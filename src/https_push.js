var firebase_config = {
    apikey: "AAAATIOk_eI:APA91bHR-NRuK-cVTc0fsdQ-N4SOAzocN7ngomFzcV7GkeCCHb6PmCFl_7MXTEPbdw-r0MTU9UmSbyxaS-DjyVZQypyNQzxUPiZt4UCZ2N7FbQqrWxi8sm4blVr-ERjt4PiUQHmSkBq4iXcNypXSuCiBin6bPKD2-A",
    messagingSenderId: "328626142690"

};

var user_subscription_detail = {
    website_id: "567",
    subscription_id: '',
    country: '',
    state: '',
    city: '',
    device_type: '',
    device_os: '',
    device_browser: '',
    ip: ''
}

var safari_push_id = "web.epush.in";
var safari_service_url = "https://api.epush.in/api/add_data";

firebase.initializeApp(firebase_config);
const firebase_messaging = firebase.messaging();

(function() {
    firebase_messaging.getToken()
        .then(function(currentToken) {
            if (currentToken) {
                console.log(currentToken, ' CT');

            } else {
                console.log('No Instance ID token available. Request permission to generate one.');
                requestPermission();
            }
        })
        .catch(function(err) {
            console.log('An error occurred while retrieving token. ', err);
        });

    function requestPermission() {
        console.log('Requesting permission...');
        // [START request_permission]
        firebase_messaging.requestPermission()
            .then(function() {
                console.log('Notification permission granted.');
                getCurrentToken();
            })
            .catch(function(err) {
                console.log('Unable to get permission to notify.', err);
                if (err.code == 'messaging/permission-default') {
                    deniedByUser('ignore');
                } else if (err.code == 'messaging/permission-blocked') {
                    deniedByUser('block');
                }
            });
        // [END request_permission]
    }

    function getCurrentToken() {
        firebase_messaging.getToken()
            .then(function(currentToken) {
                console.log('permission token ', currentToken);
                postSubscriberToken(currentToken);
            })
            .catch(function(err) {
                console.log('permission An error occurred while retrieving token. ', err);

            });
    }

    function postSubscriberToken(currentToken) {
        var user_data = {
            site_id: user_subscription_detail.website_id,
            sub_id: currentToken,
            device_type: user_subscription_detail.device_type,
            browser_type: user_subscription_detail.device_browser,
            os: user_subscription_detail.device_os,
            country: user_subscription_detail.country,
            state: user_subscription_detail.state,
            city: user_subscription_detail.city
        }
        var epush_browser_id = localStorage.getItem('epush_browser_id');
        if (epush_browser_id)
            user_data.sid = epush_browser_id;
        else
            user_data.sid = null;

        console.log("user data : " + JSON.stringify(user_data));

        var xmlhttp = new XMLHttpRequest();
        var url = 'https://api.epush.in/api/add_data/add_sub/?version=v&format=json';
        xmlhttp.open("POST", url, true);
        xmlhttp.setRequestHeader("Content-type", "application/json");
        xmlhttp.onreadystatechange = function() { //Call a function when the state changes.
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                console.log(JSON.parse(xmlhttp.responseText).sid);
                localStorage.setItem('epush_browser_id', JSON.parse(xmlhttp.responseText).sid);
            }
        }
        xmlhttp.send(JSON.stringify(user_data));

    }

    function deniedByUser(key) {

        var user_data = {
            status: 'https',
            site_id: user_subscription_detail.website_id,
            s_status: key,
            ip_adds: user_subscription_detail.ip,
            browser_type: user_subscription_detail.device_browser,
        }

        var xmlhttp = new XMLHttpRequest();
        var url = 'https://api.epush.in/api/mgtpush/add_basic_report/';
        xmlhttp.open("POST", url, true);
        xmlhttp.setRequestHeader("Content-type", "application/json");
        xmlhttp.onreadystatechange = function() { //Call a function when the state changes.
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

            }
        }
        xmlhttp.send(JSON.stringify(user_data));
    }

})();

(function() {
    var xhl = new XMLHttpRequest();
    var urll = 'https://freegeoip.net/json/';
    xhl.open("GET", urll, true);

    xhl.onreadystatechange = function() {
        if (xhl.readyState == 4 && xhl.status == 200) {
            var data = JSON.parse(xhl.responseText);
            console.log(data);
            user_subscription_detail.country = data.country_name;
            user_subscription_detail.state = data.region_name;
            user_subscription_detail.city = data.city;
            user_subscription_detail.ip = data.ip
        } else if (xhl.status == 404) {

        }
    }
    xhl.send();
})();

/**
 * JavaScript Client Detection
 * (C) viazenetti GmbH (Christian Ludwig)
 */
(function() {
    {
        var unknown = '-';

        // screen
        var screenSize = '';
        if (screen.width) {
            width = (screen.width) ? screen.width : '';
            height = (screen.height) ? screen.height : '';
            screenSize += '' + width + " x " + height;
        }

        // browser
        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browser = navigator.appName;
        var version = '' + parseFloat(navigator.appVersion);
        var majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;

        // Opera
        if ((verOffset = nAgt.indexOf('Opera')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 6);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // Opera Next
        if ((verOffset = nAgt.indexOf('OPR')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 4);
        }
        // MSIE
        else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(verOffset + 5);
        }
        // Chrome
        else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
            browser = 'Chrome';
            version = nAgt.substring(verOffset + 7);
        }
        // Safari
        else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
            browser = 'Safari';
            version = nAgt.substring(verOffset + 7);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // Firefox
        else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
            browser = 'Firefox';
            version = nAgt.substring(verOffset + 8);
        }
        // MSIE 11+
        else if (nAgt.indexOf('Trident/') != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(nAgt.indexOf('rv:') + 3);
        }
        // Other browsers
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
            browser = nAgt.substring(nameOffset, verOffset);
            version = nAgt.substring(verOffset + 1);
            if (browser.toLowerCase() == browser.toUpperCase()) {
                browser = navigator.appName;
            }
        }
        // trim the version string
        if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

        majorVersion = parseInt('' + version, 10);
        if (isNaN(majorVersion)) {
            version = '' + parseFloat(navigator.appVersion);
            majorVersion = parseInt(navigator.appVersion, 10);
        }

        // mobile version
        var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);

        // cookie
        var cookieEnabled = (navigator.cookieEnabled) ? true : false;

        if (typeof navigator.cookieEnabled == 'undefined' && !cookieEnabled) {
            document.cookie = 'testcookie';
            cookieEnabled = (document.cookie.indexOf('testcookie') != -1) ? true : false;
        }

        // system
        var os = unknown;
        var clientStrings = [
            { s: 'Windows 10', r: /(Windows 10.0|Windows NT 10.0)/ },
            { s: 'Windows 8.1', r: /(Windows 8.1|Windows NT 6.3)/ },
            { s: 'Windows 8', r: /(Windows 8|Windows NT 6.2)/ },
            { s: 'Windows 7', r: /(Windows 7|Windows NT 6.1)/ },
            { s: 'Windows Vista', r: /Windows NT 6.0/ },
            { s: 'Windows Server 2003', r: /Windows NT 5.2/ },
            { s: 'Windows XP', r: /(Windows NT 5.1|Windows XP)/ },
            { s: 'Windows 2000', r: /(Windows NT 5.0|Windows 2000)/ },
            { s: 'Windows ME', r: /(Win 9x 4.90|Windows ME)/ },
            { s: 'Windows 98', r: /(Windows 98|Win98)/ },
            { s: 'Windows 95', r: /(Windows 95|Win95|Windows_95)/ },
            { s: 'Windows NT 4.0', r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/ },
            { s: 'Windows CE', r: /Windows CE/ },
            { s: 'Windows 3.11', r: /Win16/ },
            { s: 'Android', r: /Android/ },
            { s: 'Open BSD', r: /OpenBSD/ },
            { s: 'Sun OS', r: /SunOS/ },
            { s: 'Linux', r: /(Linux|X11)/ },
            { s: 'iOS', r: /(iPhone|iPad|iPod)/ },
            { s: 'Mac OS X', r: /Mac OS X/ },
            { s: 'Mac OS', r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/ },
            { s: 'QNX', r: /QNX/ },
            { s: 'UNIX', r: /UNIX/ },
            { s: 'BeOS', r: /BeOS/ },
            { s: 'OS/2', r: /OS\/2/ },
            { s: 'Search Bot', r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/ }
        ];
        for (var id in clientStrings) {
            var cs = clientStrings[id];
            if (cs.r.test(nAgt)) {
                os = cs.s;
                break;
            }
        }

        var osVersion = unknown;

        if (/Windows/.test(os)) {
            osVersion = /Windows (.*)/.exec(os)[1];
            os = 'Windows';
        }

        switch (os) {
            case 'Mac OS X':
                osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                break;

            case 'Android':
                osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                break;

            case 'iOS':
                osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                break;
        }

        // flash (you'll need to include swfobject)
        /* script src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js" */
        var flashVersion = 'no check';
        if (typeof swfobject != 'undefined') {
            var fv = swfobject.getFlashPlayerVersion();
            if (fv.major > 0) {
                flashVersion = fv.major + '.' + fv.minor + ' r' + fv.release;
            } else {
                flashVersion = unknown;
            }
        }
    }

    window.jscd = {
        screen: screenSize,
        browser: browser,
        browserVersion: version,
        browserMajorVersion: majorVersion,
        mobile: mobile,
        os: os,
        osVersion: osVersion,
        cookies: cookieEnabled,
        flashVersion: flashVersion
    };


    var device_detail = {
        'OS': jscd.os + ' ' + jscd.osVersion,
        'Browser': jscd.browser + ' ' + jscd.browserMajorVersion +
            ' (' + jscd.browserVersion + ')',
        'Mobile': +jscd.mobile,
        'Flash': jscd.flashVersion,
        'Cookies': +jscd.cookies,
        'Screen_Size': jscd.screen,
        'Full User Agent ': navigator.userAgent
    };


    if (device_detail.OS.indexOf(' ') == -1) {
        user_subscription_detail.device_os = device_detail.OS;
    } else {
        user_subscription_detail.device_os = device_detail.OS.substring(0, device_detail.OS.indexOf(' '));
    }

    if (device_detail.Browser.indexOf(' ') == -1) {
        user_subscription_detail.device_browser = device_detail.Browser;
    } else {
        user_subscription_detail.device_browser = device_detail.Browser.substr(0, device_detail.Browser.indexOf(" "));
    }

    if (device_detail.Mobile == 0 || device_detail.Mobile == false) {
        user_subscription_detail.device_type = "Desktop";
    } else {
        var uwidth = Number.parseInt(device_detail.Screen_Size.substr(0, device_detail.Screen_Size.indexOf(" ")));
        var dwidth = 700;
        if (uwidth < dwidth) {
            user_subscription_detail.device_type = "Mobile";
        } else {
            user_subscription_detail.device_type = "Tablet";
        }
    }

    if (user_subscription_detail.device_os == 'Mac' && user_subscription_detail.device_browser == 'Safari') {
        inMac();
    }

    // Safari Notification
    function inMac() {
        // console.log('i am in mac safari')
        if ('safari' in window && 'pushNotification' in window.safari) {
            console.log('safari supported');
            var permissionData = window.safari.pushNotification.permission(safari_push_id);
            checkRemotePermission(permissionData);
            // console.log("permission data  "+permissionData);
        } else {
            console.log('safari not supported');
        }
    }

    function checkRemotePermission(permissionData) {
        if (permissionData.permission === 'default') {
            console.log('safari default');
            // This is a new web service URL and its validity is unknown.
            window.safari.pushNotification.requestPermission(
                safari_service_url, // The web service URL.
                safari_push_id, // The Website Push ID.
                {
                    site_id: user_subscription_detail.website_id,
                    domain_name: window.location.href,
                    country: user_subscription_detail.country,
                    state: user_subscription_detail.state,
                    city: user_subscription_detail.city
                }, // Data that you choose to send to your server to help you identify the user.
                function(a) {
                    // console.log("callback  "+JSON.stringify(a));
                }
            );
        } else if (permissionData.permission === 'denied') {
            console.log('safari denied');
            // The user said no.
        } else if (permissionData.permission === 'granted') {
            console.log('safari granted');
            // The web service URL is a valid push provider, and the user said yes.
            // permissionData.deviceToken is now available to use.
        }
    };
    // End Safari Notification

})();


// Callback fired if Instance ID token is updated.
firebase_messaging.onTokenRefresh(function() {
    firebase_messaging.getToken()
        .then(function(refreshedToken) {
            console.log('Token refreshed.');
            postRefreshedToken(refreshedToken);

        })
        .catch(function(err) {
            console.log('Unable to retrieve refreshed token ', err);

        });

    function postRefreshedToken(currentToken) {
        var user_data = {
            site_id: user_subscription_detail.website_id,
            sub_id: currentToken,
            device_type: user_subscription_detail.device_type,
            browser_type: user_subscription_detail.device_browser,
            os: user_subscription_detail.device_os,
            country: user_subscription_detail.country,
            state: user_subscription_detail.state,
            city: user_subscription_detail.city
        }
        var epush_browser_id = localStorage.getItem('epush_browser_id');
        if (epush_browser_id)
            user_data.sid = epush_browser_id;
        else
            user_data.sid = null;

        console.log("user data : " + JSON.stringify(user_data));

        var xmlhttp = new XMLHttpRequest();
        var url = 'https://api.epush.in/api/add_data/add_sub/?version=v&format=json';
        xmlhttp.open("POST", url, true);
        xmlhttp.setRequestHeader("Content-type", "application/json");
        xmlhttp.onreadystatechange = function() { //Call a function when the state changes.
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                console.log(JSON.parse(xmlhttp.responseText).sid);
                localStorage.setItem('epush_browser_id', JSON.parse(xmlhttp.responseText).sid);
            }
        }
        xmlhttp.send(JSON.stringify(user_data));

    }
});
// [END refresh_token]

firebase_messaging.onMessage(function(payload) {
    console.log("Message received. ", payload);

});