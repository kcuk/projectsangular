import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { GlobalService } from "./GlobalService";
import { Angulartics2GoogleAnalytics } from "angulartics2";
import { TranslateService } from "ng2-translate";
declare const $: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  delay: boolean = false;
  show_footer: boolean = true;
  constructor(
    private translate: TranslateService,
    public global: GlobalService,
    public router: Router
  ) {
    
    console.log(this.router.url,"urlto match=================>>>>>>>>>>>>1111111111111");
    if (localStorage.getItem("userInfo")) {
      let url1 =
        this.global.base_path_api() +
        "user/setLanguage/?language=" +
        localStorage.getItem("language");
      this.global.GetRequest(url1).subscribe(res => {});
    } 
    // else {
    //   let url1 = this.global.base_path_api() + "user/setLanguage/?language=es";
    //   this.global.GetRequest(url1).subscribe(res => {});
    // }

    let url = window.location.href;
    let check_string = url.split("/")[5];
    if (check_string === "addcard") {
      this.show_footer = false;
    }
    translate.addLangs(["en", "pt", "es"]);
    if (localStorage.getItem("language")) {
      if (
        localStorage.getItem("language") == null ||
        localStorage.getItem("language") == "null"
      ) {
        localStorage.setItem("language", "en");
        console.log(
          "nullllllllllllllllllllllllllll",
          localStorage.getItem("language")
        );
        translate.use("en");
      } else {
        translate.use(localStorage.getItem("language"));
      }
    } else {
      translate.setDefaultLang("es");
    }

    document.addEventListener("click", event => {
      if (event.target["id"] == "eng") {
        this.changeLanguage("en");
      } else if (event.target["id"] == "span") {
        this.changeLanguage("es");
      } else if (event.target["id"] == "port") {
        this.changeLanguage("pt");
      }
    });
    setTimeout(() => {
      this.delay = true;
    }, 2000);
  }

  tokenRefresh() {
    if (localStorage.getItem("timeout")) {
      let a = localStorage.getItem("timeout");
      let token = JSON.parse(localStorage.getItem("userInfo"));
      let currentTime = new Date().getTime() + 240000;
      let timeout = new Date(a).getTime();
      let c = timeout - currentTime;
      if (c < 0) {
        let url =
          this.global.base_path +
          "o/token/?grant_type=refresh_token&client_id=" +
          token[0].info.client_id +
          "&client_secret=" +
          token[0].info.client_secret +
          "&refresh_token=" +
          token[0].token.refresh_token +
          "&format=json";
        this.global.PostRequest(url, "data").subscribe(
          res => {
            token.token.access_token = res[0].json.access_token;
            token.token.refresh_token = res[0].json.refresh_token;
            let timeNow = new Date();
            timeNow.setHours(
              timeNow.getHours() + res[0].json.expires_in / 3600
            );
            localStorage.setItem("userInfo", JSON.stringify(token));
            localStorage.setItem("timeout", timeNow.toString());
            let myVar = setTimeout(
              this.tokenRefresh,
              res[0].json.expires_in * 1000 - 180000
            );
          },
          err => {
            this.router.navigateByUrl("login");
          }
        );
      } else {
        console.log("time remaining", c);
        let myVar = setTimeout(r => {
          this.tokenRefresh();
        }, c - 120000);
      }
    }
  }

  changeLanguage(lang) {
    if (lang == "en") {
      this.translate.use("en");
      localStorage.setItem("language", "en");
      location.reload();
    } else if (lang == "es") {
      this.translate.use("es");
      localStorage.setItem("language", "es");
      location.reload();
    } else if (lang == "pt") {
      this.translate.use("pt");
      localStorage.setItem("language", "pt");
      location.reload();
    }
  }
}
