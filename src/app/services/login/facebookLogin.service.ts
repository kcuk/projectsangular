import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from "./../../GlobalService";

import { LoginService } from './login.service';

declare const FB: any;

@Injectable()

export class FacebookService {
    public key: string = null;
    public obj: any = null;
    private login: boolean = false;
    public email: string = "";
    public name: string = "";
    public facebook_id = "";
    public profilePic: string = "";

    constructor(public router: Router, private login_service: LoginService, public global: GlobalService) {
        try {
            FB.init({
                appId: this.global.fb_id,
                //localhost  298764690458377 
                //peloteando.co  1679314905721132 // testing 1813124638915635
                cookie: false,
                xfbml: true,
                version: 'v2.5'
            });
        } catch (e) {
        }
    }

    onFacebookLoginClick() {
        FB.login((response) => {
            if (this.login)
                this.getAccessToken1();
            else
                this.getInformation();

        }, { scope: 'email,user_birthday' });

    }

    getProfilePic() {
        let userdata: any;
        FB.api('/me?fields=birthday,first_name,last_name,gender', response => {
            return response;
        });
    }


    getAccessToken1(): any {
        this.obj = FB.getAuthResponse();
        let url = this.login_service.baseUrl + "/peloteando/fb_login/";
        if (this.obj != null) {
            this.key = this.obj.accessToken;
            let x: any = { "access_token": this.key };
            this.hitApi(x, url);
        }
    }

    getInformation(): any {
        let a: any;
        let url: string = this.login_service.baseUrl + "/peloteando/signup/?format=json";
        FB.api('me?fields=id,email,name', response => {
            this.name = response.name;
            this.email = response.email;
            this.facebook_id = response.id;
        });

    }

    statusChangeCallback(resp) {
        if (resp.status === 'connected') {
            if (this.login)
                this.getAccessToken1();
            else
                this.getInformation();
        } else if (resp.status === 'not_authorized') {

        } else {
            this.onFacebookLoginClick();

        }
    };

    ngOnInit() {
        FB.getLoginStatus(response => {
            this.statusChangeCallback(response);
        });
    }
    fbLoginStatus() {
        FB.getLoginStatus(response => {
            this.statusChangeCallback(response);
        });

    }


    hitApi(data: any, url: string): any {

        this.login_service.PostRequest(url, data)
            .subscribe(res => {
                if (this.login) {
                    localStorage.setItem("userInfo", res);
                    this.router.navigate(["/home"]);
                }


            })
    }
    signin() {
        this.login = true;
        this.signup();
    }
    signup() {
        this.fbLoginStatus();
    }
}


