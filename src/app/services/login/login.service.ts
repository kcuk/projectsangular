import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { GlobalService } from "./../../GlobalService";

import { Http, Response, RequestOptions, Headers, Request, RequestMethod } from '@angular/http';

@Injectable()

export class LoginService {

    public requestoptions: RequestOptions;
    public res: Response;
    public headers: Headers;

    // public url: string = 'https://api.peloteando.co/api/peloteando/login/?format=json';
    // public baseUrl = 'https://api.peloteando.co/api';
    // public url: string = 'https://football.sia.co.in/api/peloteando/login/?format=json';
    // public baseUrl = 'https://football.sia.co.in/api';
    // public baseUrl = 'https://test.iamvinay.me/api';
    public baseUrl = this.global.base_path_api();
    public urlFb: string = this.baseUrl + "/peloteando/fb_login/?format=json";
    public nameAndPass: string;
    public encode: string;

    constructor(public http: Http,public global:GlobalService) { }
    loginRequest(data: any) {
        this.headers = new Headers();
        this.nameAndPass = data.name + ":" + data.password;
        this.encode = btoa(this.nameAndPass);
        this.headers.append('Authorization', 'Basic ' + this.encode);
        this.headers.append("Content-Type", "application/json");
        this.requestoptions = new RequestOptions({
            method: RequestMethod.Get,
            url: "http://139.59.29.6/api/peloteando/login/?format=json",
            headers: this.headers
        })
        return this.http.request(new Request(this.requestoptions))
            .map((res: Response) => {
                return [{ status: res.status, json: res.json() }]
            })
            .catch(error => {
                if (error.status === 401) {
                    return Observable.throw(error);
                }
            });

    }

    public PostRequest(url: string, data: any): any {
        this.headers = new Headers();
        this.headers.append("Content-Type", 'application/json');

        this.requestoptions = new RequestOptions({
            method: RequestMethod.Post,
            url: url,
            headers: this.headers,
            body: JSON.stringify(data)
        })
        return this.http.request(new Request(this.requestoptions))
            .map((res: Response) => {
                return res;
            })

    }

    public PostRequest1(url: string, data: any): any {
        this.headers = new Headers();
        this.headers.append("Content-Type", 'application/json');

        this.requestoptions = new RequestOptions({
            method: RequestMethod.Post,
            url: url,
            headers: this.headers,
            body: JSON.stringify(data)
        })
        return this.http.request(new Request(this.requestoptions))
            .map((res: Response) => {
                return res.json();
            })

    }

    //end of post req

    checkLoginStatus(): boolean {
        if (localStorage.getItem("userInfo") !== null)
            return true;
        else
            return false;
    }

    // fb login req func
    fbLoginRequest(data: any, url: string) {

        this.headers = new Headers();
        this.requestoptions = new RequestOptions({
            method: RequestMethod.Post,
            url: this.baseUrl + url,
            headers: this.headers,
            body: JSON.stringify(data)
        })
        return this.http.request(new Request(this.requestoptions))
            .map((res: Response) => {
                return res.json();
            })

    }
    //end of fb login req func
    public forgetPasswordRequest(data: any, url: string): any {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.requestoptions = new RequestOptions({
            method: RequestMethod.Post,
            url: url,
            headers: this.headers,
            body: JSON.stringify(data)
        })

        return this.http.request(new Request(this.requestoptions))
            .map((res: Response) => {
                return res.json();
            })
    }
    public setPasswordRequest(data: any, myurl: string, aToken: string): any {
        this.headers = new Headers();
        this.headers.append("Content-Type", 'application/json');
        this.headers.append("Authorization", 'Bearer ' + aToken)

        this.requestoptions = new RequestOptions({
            method: RequestMethod.Post,
            url: myurl,
            headers: this.headers,
            body: JSON.stringify(data)
        })
        return this.http.request(new Request(this.requestoptions))
            .map((res: Response) => {
                return res;
            })

    }
}
