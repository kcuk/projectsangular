import { Injectable, EventEmitter } from "@angular/core";
import {
  Http,
  Response,
  RequestOptions,
  Headers,
  Request,
  RequestMethod
} from "@angular/http";
import { Route, Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/Rx";

declare var jQuery: any;
declare const toastr: any;

@Injectable()
export class GlobalService {
  params: URLSearchParams;
  ws_path: string;
  /*For create Group and Round Details Relation only*/
  demoCheck: EventEmitter<any> = new EventEmitter<any>();
  profileCheck: EventEmitter<any> = new EventEmitter<any>();
  navCheck: EventEmitter<any> = new EventEmitter<any>();
  public playerRequestRefresh: EventEmitter<any> = new EventEmitter<any>();
  public groundProfile: EventEmitter<any> = new EventEmitter<any>();
  public tournamentProfile: EventEmitter<any> = new EventEmitter<any>();
  public playerProfile: EventEmitter<any> = new EventEmitter<any>();
  public teamProfile: EventEmitter<any> = new EventEmitter<any>();
  public pageRefresh: EventEmitter<any> = new EventEmitter<any>();
  public roundPageRefresh: EventEmitter<any> = new EventEmitter<any>();
  counter: number = 0;
  /*For create Group and Round Details Relation Only*/
  user_info: any;
  image_url: string;
  public playerId: any;
  public base_path: string = "";
  public headers: Headers;
  public requestoptions: RequestOptions;
  public res: Response;
  public loggedIn: boolean = false;
  public globalTournametId: number = 0;
  public base_path_image: string;
  public signUpObj: any = {};
  public shoutType: string = "";
  public tournamentPageRefresh: EventEmitter<any> = new EventEmitter<any>();
  public domainName: string = "pelotea.com";
  public shareTwitterInfo = "";
  public shareImageInfo = "";
  public pageChange: EventEmitter<any> = new EventEmitter<any>();
  public closeMenu: EventEmitter<any> = new EventEmitter<any>();
  public changeName: EventEmitter<any> = new EventEmitter<any>();

  public downloadProfile: boolean = false;
  payment_path: any;
  // public fb_id: any = "782314702158822"; //local host key
  public fb_id: any = "226002471334135"; //final and tetsing key

  public peloteaid = "20";
  constructor(public http: Http, public router: Router) {
    // final path
    // this.ws_path = 'wss://api.pelotea.com/ws/';
    // this.base_path = "https://api.pelotea.com/";
    // this.image_url = "https://api.pelotea.com";
    // this.payment_path = "https://pelotea.com";

    // testing path
    // Change base path 23  dec, 2017
    // this.base_path = "https://football.sia.co.in/";
    // this.image_url = "https://football.sia.co.in/";
    // this.ws_path = "wss://football.sia.co.in/ws/";
    // this.payment_path = "https://testing.peloteando.co";
    // this.payment_path = "http://localhost:4200";

    // this.base_path = "https://test.iamvinay.me/";
    // this.image_url = "https://test.iamvinay.me/";
    // this.ws_path = 'wss://test.iamvinay.me/ws/';
    // this.payment_path = "https://testing.peloteando.co";
    // this.payment_path = "http://localhost:4200";

    // new path
    this.base_path = "https://webapi.pelotea.com/";
    this.image_url = "https://webapi.pelotea.com/";
    this.ws_path = "wss://webapi.pelotea.com/ws/";
  }

  public base_path_api() {
    return this.base_path + "api/";
  }

  /*For create Group and Round Details Relation only*/
  public parentChildFun() {
    this.counter = this.counter + 1;
    this.demoCheck.next(this.counter);
  }

  public parentChildFunForProfileOnly() {
    this.counter = this.counter + 1;
    this.profileCheck.next(this.counter);
  }

  public childParentFun() {
    this.counter = this.counter + 1;
    this.navCheck.next(this.counter);
  }
  /*For create Group and Round Details Relation Only*/

  public getRequsetOptions(url: string): RequestOptions {
    if (localStorage.getItem("userInfo")) {
      this.headers = new Headers();
      this.headers.append("Content-Type", "application/json");
      this.headers.append(
        "Authorization",
        "Bearer " +
          JSON.parse(localStorage.getItem("userInfo")).token.access_token
      );
      this.headers.append(
        "accept-language",
        localStorage.getItem("language") == "en" ? "en" : "es"
      );

      this.params = new URLSearchParams();
      if (localStorage.getItem("language") == "en") {
        this.params.set("language", "en");
      } else {
        this.params.set("language", "es");
      }
    } else {
      // this.router.navigate(['../home']);
    }

    this.requestoptions = new RequestOptions({
      method: RequestMethod.Get,
      url: url,
      headers: this.headers,
      params: this.params
    });

    return this.requestoptions;
  }

  public getRequsetOptionsUnauthorised(url: string): RequestOptions {
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.params = new URLSearchParams();
    if (localStorage.getItem("language") == "en") {
      this.params.set("language", "en");
    } else {
      this.params.set("language", "es");
    }
    this.headers.append(
      "accept-language",
      localStorage.getItem("language") == "en" ? "en" : "es"
    );

    this.requestoptions = new RequestOptions({
      method: RequestMethod.Get,
      url: url,
      headers: this.headers,
      params: this.params
    });

    return this.requestoptions;
  }

  public PostRequest(url: string, data: any): any {
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.headers.append(
      "Authorization",
      "Bearer " +
        JSON.parse(localStorage.getItem("userInfo")).token.access_token
    );
    this.headers.append(
      "accept-language",
      localStorage.getItem("language") == "en" ? "en" : "es"
    );
    this.params = new URLSearchParams();
    if (localStorage.getItem("language") == "en") {
      this.params.set("language", "en");
    } else {
      this.params.set("language", "es");
    }
    this.requestoptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: this.headers,
      body: JSON.stringify(data),
      params: this.params
    });

    return this.http
      .request(new Request(this.requestoptions))
      .map((res: Response) => {
        if (res.status === 201) {
          return [{ status: res.status, json: res.json() }];
        } else if (res.status === 205) {
          return [{ status: res.status, json: res.json() }];
        } else if (res.status === 200) {
          return [{ status: res.status, json: res.json() }];
        }
      })
      .catch((error: any) => {
        return Observable.throw(error);
        // if (error.status === 500) {
        //   return Observable.throw(error);
        // } else if (error.status === 400) {
        //   return Observable.throw(error);
        // } else if (error.status === 409) {
        //   return Observable.throw(error);
        // } else if (error.status === 406) {
        //   return Observable.throw(error);
        // } else if (error.status === 404) {
        //   return Observable.throw(error);
        // } else if (error.status === 401) {
        //   this.router.navigate(["../home"]);
        //   localStorage.clear();
        //   return Observable.throw(error);
        // }
      });
  }

  public PostRequestUnauthorised(url: string, data: any): any {
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.headers.append(
      "accept-language",
      localStorage.getItem("language") == "en" ? "en" : "es"
    );
    this.params = new URLSearchParams();
    if (localStorage.getItem("language") == "en") {
      this.params.set("language", "en");
    } else {
      this.params.set("language", "es");
    }
    this.requestoptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: this.headers,
      body: JSON.stringify(data),
      params: this.params
    });

    return this.http
      .request(new Request(this.requestoptions))
      .map((res: Response) => {
        if (res.status === 201) {
          return [{ status: res.status, json: res.json() }];
        } else if (res.status === 205) {
          return [{ status: res.status, json: res.json() }];
        } else if (res.status === 200) {
          return [{ status: res.status, json: res.json() }];
        }
      })
      .catch((error: any) => {
        console.log(error, "global error==============>>>>>>>>>>");
        return Observable.throw(error);
        // if (error.status === 500) {
        //   return Observable.throw(error);
        // } else if (error.status === 400) {
        //   return Observable.throw(error);
        // } else if (error.status === 403) {
        //   return Observable.throw(error);
        // } else if (error.status === 400) {
        //   return Observable.throw(error);
        // } else if (error.status === 409) {
        //   return Observable.throw(error);
        // } else if (error.status === 406) {
        //   return Observable.throw(error);
        // } else if (error.status === 404) {
        //   return Observable.throw(error);
        // }
      });
  }

  public GetRequest(url: string): any {
    return this.http
      .request(new Request(this.getRequsetOptions(url)))
      .map((res: Response) => {
        let jsonObj: any;
        if (res.status === 204) {
          jsonObj = null;
        } else if (res.status === 500) {
          jsonObj = null;
        } else if (res.status !== 204) {
          jsonObj = res.json();
        }
        return [{ status: res.status, json: jsonObj }];
      })
      .catch(error => {
        if (error.status === 403) {
          return Observable.throw(error);
        } else if (error.status === 500) {
          return Observable.throw(error);
        } else if (error.status === 401) {
          // this.router.navigate(["../home"]);
          // localStorage.clear();
          return Observable.throw(error);
        } else if (error.status === 400) {
          return Observable.throw(error);
        } else if (error.status === 409) {
          return Observable.throw(error);
        }
        if (error.status === 404) {
          return Observable.throw(error);
        }
      });
  }

  public GetRequestUnauthorised(url: string): any {
    return this.http
      .request(new Request(this.getRequsetOptionsUnauthorised(url)))
      .map((res: Response) => {
        let jsonObj: any;
        if (res.status === 204) {
          jsonObj = null;
        } else if (res.status === 500) {
          jsonObj = null;
        } else if (res.status !== 204) {
          jsonObj = res.json();
        }
        return [{ status: res.status, json: jsonObj }];
      })
      .catch(error => {
        if (error.status === 403) {
          return Observable.throw(error);
        } else if (error.status === 400) {
          return Observable.throw(error);
        } else {
          return Observable.throw(error);
        }
      });
  }

  public DeleteRequest(url: string): any {
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.headers.append(
      "Authorization",
      "Bearer " +
        JSON.parse(localStorage.getItem("userInfo")).token.access_token
    );
    this.headers.append(
      "accept-language",
      localStorage.getItem("language") == "en" ? "en" : "es"
    );
    this.params = new URLSearchParams();
    if (localStorage.getItem("language") == "en") {
      this.params.append("language", "en");
    } else {
      this.params.append("language", "es");
    }
    this.requestoptions = new RequestOptions({
      method: RequestMethod.Delete,
      url: url,
      headers: this.headers,
      params: this.params
    });

    return this.http
      .request(new Request(this.requestoptions))
      .map((res: Response) => {
        if (res) {
          return [{ status: res.status, json: res }];
        }
      })
      .catch((error: any) => {
        if (error.status === 500) {
          return Observable.throw(error);
        } else if (error.status === 400) {
          return Observable.throw(error);
        } else if (error.status === 405) {
          return Observable.throw(error);
        } else if (error.status === 409) {
          return Observable.throw(error);
        } else if (error.status === 401) {
          this.router.navigate(["../home"]);
          return Observable.throw(error);
        }
      });
  }

  public PutRequest(url: string, data: any): any {
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.headers.append(
      "Authorization",
      "Bearer " +
        JSON.parse(localStorage.getItem("userInfo")).token.access_token
    );
    this.headers.append(
      "accept-language",
      localStorage.getItem("language") == "en" ? "en" : "es"
    );
    this.params = new URLSearchParams();
    if (localStorage.getItem("language") == "en") {
      this.params.set("language", "en");
    } else {
      this.params.set("language", "es");
    }
    this.requestoptions = new RequestOptions({
      method: RequestMethod.Put,
      url: url,
      headers: this.headers,
      body: JSON.stringify(data),
      params: this.params
    });

    return this.http
      .request(new Request(this.requestoptions))
      .map((res: Response) => {
        if (res) {
          return [{ status: res.status, json: res }];
        }
      })
      .catch((error: any) => {
        if (error.status === 500) {
          return Observable.throw(error);
        } else if (error.status === 400) {
          return Observable.throw(error);
        } else if (error.status === 405) {
          return Observable.throw(error);
        } else if (error.status === 409) {
          return Observable.throw(error);
        } else if (error.status === 401) {
          this.router.navigate(["../home"]);
          return Observable.throw(error);
        }
      });
  }

  getVarification(url: string) {
    return this.http
      .request(new Request(this.getRequsetOptions(url)))
      .map(res => {
        if (res) {
          if (res.status === 200) {
            return [{ status: res.status, json: null }];
          }
        }
      })
      .catch((error: any) => {
        if (error.status === 409) {
          return Observable.throw(error.status);
        }
      });
  }

  print(msg?: any, b?, c?, d?, e?) {
    console.log(msg, b, c, d, e);
  }

  redirect(str: string) {
    this.router.navigate([str]);
  }

  loginRequest(data: any, x) {
    let requestoptions: RequestOptions;
    this.headers = new Headers();
    let nameAndPass = data.name + ":" + data.password;
    let encode = btoa(nameAndPass);
    this.headers.append("Authorization", "Basic " + encode);
    this.headers.append(
      "accept-language",
      localStorage.getItem("language") == "en" ? "en" : "es"
    );
    this.params = new URLSearchParams();
    if (localStorage.getItem("language") == "en") {
      this.params.set("language", "en");
    } else {
      this.params.set("language", "es");
    }
    requestoptions = new RequestOptions({
      method: RequestMethod.Get,
      url: this.base_path + x,
      headers: this.headers,
      params: this.params
    });
    return this.http
      .request(new Request(requestoptions))
      .map((res: Response) => {
        return [{ status: res.status, json: res.json() }];
      })
      .catch(error => {
        if (error.status === 401) {
          return Observable.throw(error);
        }
      });
  }

  getLocationDefault() {
    let headers = new Headers();
    headers.append("Accept", "application/json");
    this.headers.append(
      "accept-language",
      localStorage.getItem("language") == "en" ? "en" : "es"
    );
    this.params = new URLSearchParams();
    if (localStorage.getItem("language") == "en") {
      this.params.set("language", "en");
    } else {
      this.params.set("language", "es");
    }
    let requestoptions: RequestOptions = new RequestOptions({
      method: RequestMethod.Get,
      url: "https://freegeoip.net/json/?format=json",
      headers: headers,
      params: this.params
    });
    return this.http
      .request(new Request(requestoptions))
      .map((res: Response) => {
        let jsonObj: any;
        if (res.status === 204) {
          jsonObj = null;
        } else if (res.status === 500) {
          jsonObj = null;
        } else if (res.status !== 204) {
          jsonObj = res.json();
        }
        return [{ status: res.status, json: jsonObj }];
      })
      .catch(error => {
        if (
          error.status === 403 ||
          error.status === 500 ||
          error.status === 401 ||
          error.status === 400 ||
          error.status === 409 ||
          error.status === 404
        ) {
          return Observable.throw(error);
        }
      });
  }

  isLoggedIn() {
    if (localStorage.getItem("userInfo")) {
      var x = localStorage.getItem("timeout");
      var newDate = new Date(x);
      var current_date = new Date();
      if (newDate.getTime() > current_date.getTime()) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  currentLocation() {
    // https://api.ipstack.com/check?access_key=99d744f6caa196a24239c24877526bf3
    return this.http
      .get("https://geoip.nekudo.com/api", {})
      .map(res => {
        localStorage.setItem("distance", "15");
        return res;
      })
      .catch(err => {
        return Observable.throw(err.status);
      });
  }

  loginValid(url: string, userinfo, data: any) {
    let header = new Headers();
    let nameandpass = userinfo.user + ":" + userinfo.pass;
    let encode = btoa(nameandpass);
    header.append("Authorization", "Basic " + encode);
    return this.http
      .post(url, data, {
        headers: header
      })
      .map(res => {
        return [{ status: res.status, json: res.json() }];
      })
      .catch(err => {
        console.log("global service=>>>>>>>>>>", err);
        return Observable.throw(err.status);
      });
  }

  convertUTCDateToLocalDate(date) {
    console.log("date time ===============", date);
    var newDate = new Date(
      date.getTime() + date.getTimezoneOffset() * 60 * 1000
    );
    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();
    newDate.setHours(hours - offset);
    console.log("new date=========", newDate);
    return newDate;
  }
}
