import { Component, NgZone } from "@angular/core";
import { Http } from "@angular/http";
import { GlobalService } from "../../GlobalService";
import { Router } from "@angular/router";
declare var google;

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent {
  msgs:any;
  distance: any = (localStorage.getItem('distance') != 'undefined' && localStorage.getItem('distance')) ? localStorage.getItem('distance') : 15;
  loader: boolean;
  autocompleteItems: any[];
  GoogleAutocomplete: any;
  constructor(
    public zone: NgZone,
    public global: GlobalService,
    public router: Router
  ) {
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
  }


  search(event) {
    console.log("valussssssssss", event)
    this.GoogleAutocomplete.getPlacePredictions({ input: event.query, types: ['(regions)'] },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
          console.log("list", this.autocompleteItems)
        });
      });
  }

  setLoaction(item) {
    console.log("item=>>>>>>>>>>>>>>>>", item);
    this.loader = true;
    localStorage.setItem('city', item.terms[0].value);
    localStorage.setItem('country_name', item.terms.pop().value);
    localStorage.setItem('distance', this.distance);
    this.geoCode((item.description));

  }

  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      localStorage.setItem('lat', results[0].geometry.location.lat());
      localStorage.setItem('long', results[0].geometry.location.lng());
      this.zone.run(() => {
        // this.router.navigateByUrl('dashboard')
        // this.router.navigate(['/dashboard'])
        let comdata = JSON.parse(localStorage.getItem("community_info"));
        console.log(comdata, "fgewguigewewrgwue");
        let comname = comdata.community_name;
        console.log(comname, "community name we get======================");
        this.router.navigate([comname]);
      });
    })
  }

  currentPosition() {
    this.loader = true;
    this.global.currentLocation()
      .subscribe(res => {
        let data = res.json();
        localStorage.setItem('lat', data.location.latitude);
        localStorage.setItem('long', data.location.longitude);
        localStorage.setItem('city', data.city);
        localStorage.setItem('country_name', data.country.name);
        this.zone.run(() => {
          // this.router.navigateByUrl('dashboard')
          // this.router.navigate(['/dashboard'])
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          console.log(comdata, "fgewguigewewrgwue");
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.router.navigate([comname]);
        });
      })
  }

}
