import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from 'ng2-translate';
import { GlobalService } from './../../GlobalService';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.css']
})
export class PackageComponent implements OnInit {
  check_package: any;
  id: any
  msgs: any[];
  loader: boolean = false;
  results: any;
  redirect: any = '';
  status: any;
  skip: boolean = false;
  selected_payment_option: any = 'monthly';

  constructor(public router: Router, public route: ActivatedRoute, private translate: TranslateService, public base_path_service: GlobalService) {
    this.route.params.subscribe(param => {
      this.id = param['id'];
    })
    this.route.queryParams.subscribe(param => {
      this.status = param['status'];
      if (param['option'] !== undefined) {
        console.log("okkkkkkkkkkkkk", param['option'])
        this.selected_payment_option = param['option']
      }
    })
  }

  ngOnInit() {
    if (this.status === 'ground') {
      let url = this.base_path_service.base_path_api() + 'ground/field_package/?ground=' + this.id;
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.results = res[0].json;
  
        })
    }
    else if (this.status === 'tournament') {
      let url = this.base_path_service.base_path_api() + 'tournament/tour_package/?tournament=' + this.id;
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.results = res[0].json;
          if (res[0].json.subscription_type != null) {
            this.selected_payment_option = res[0].json.subscription_type;
          }
          console.log("select optionnnnnnnnnnnnnnnnnnn",this.selected_payment_option)

        })
    }
  }

  select_package() {
    this.loader = true;
    localStorage.setItem("package_detail", JSON.stringify(this.results));
    if (this.status === 'ground') {
      let url = this.base_path_service.base_path_api() + "ground/groundData/?ground_id=" + this.id;
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.loader = false;

          let courts = {
            ground_info: res[0].json
          }
          localStorage.setItem('courts', JSON.stringify(courts));
          this.router.navigateByUrl('dashboard/payment/' + this.id + "?ground=" + true + "&package=" + this.selected_payment_option);
        }, err => {
          this.loader = false
        })
    }
    else if (this.status = 'tournament') {
      let url = this.base_path_service.base_path_api() + "tournament/tournamentData/?tournament_id=" + this.id;
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.loader = false
          let tournament = {
            tounament_info: res[0].json
          }
          localStorage.setItem('tournament', JSON.stringify(tournament));
          this.router.navigateByUrl('dashboard/payment/' + this.id + "?tournament=" + true + "&package=" + this.selected_payment_option);
        }, err => {
          this.loader = false
        })
    }
  }

  skipPackage() {
    if (this.status == 'ground') {
      this.router.navigateByUrl('dashboard/ground/ground-profile/' + this.id);
    }
    else if (this.status == 'tournament') {
      this.router.navigateByUrl('dashboard/tournaments/tournament-profile/' + this.id);

    }
  }

}
