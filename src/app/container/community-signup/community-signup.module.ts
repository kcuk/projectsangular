import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "./../../shared/shared.module";
import { Routes, RouterModule } from "@angular/router";
import { CommunitySignupComponent } from "./community-signup.component";
import { FindCommunityComponent } from "./find-community/find-community.component";
import { CheckboxModule } from "primeng/primeng";
import { SelectTeamComponent } from "./select-team/select-team.component";
import { RadioButtonModule } from "primeng/primeng";
import { SelectRoleComponent } from './select-role/select-role.component';


export const community_routes: Routes = [
  { path: "", component: FindCommunityComponent },
  { path: "selectteam", component: SelectTeamComponent },
  { path: "selectrole", component: SelectRoleComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(community_routes),
    SharedModule.forRoot(),
    CheckboxModule,
    RadioButtonModule
  ],
  declarations: [
    CommunitySignupComponent,
    FindCommunityComponent,
    SelectTeamComponent,
    SelectRoleComponent
  ]
})
export class CommunitysignupModule {}
