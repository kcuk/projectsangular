import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunitySignupComponent } from './community-signup.component';

describe('CommunitySignupComponent', () => {
  let component: CommunitySignupComponent;
  let fixture: ComponentFixture<CommunitySignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunitySignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunitySignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
