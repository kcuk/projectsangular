import { Component, OnInit, NgZone } from "@angular/core";
import { Router } from "@angular/router";
import { GlobalService } from "../../GlobalService";
import { TranslateService } from "ng2-translate";
declare var google;

@Component({
  selector: "app-community-signup",
  templateUrl: "./community-signup.component.html",
  styleUrls: ["./community-signup.component.css"]
})
export class CommunitySignupComponent implements OnInit {
  GoogleAutocomplete: any;
  distance: any =
    localStorage.getItem("distance") != "undefined" &&
    localStorage.getItem("distance")
      ? localStorage.getItem("distance")
      : 15;

  constructor(
    public router: Router,
    public global: GlobalService,
    public zone: NgZone,
    public translate: TranslateService
  ) {
    
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
  }

  ngOnInit() {
    // localStorage.clear();
    this.currentPosition();
  }
  setLoaction(item) {
    console.log("item=>>>>>>>>>>>>>>>>", item);
    localStorage.setItem("city", item.terms[0].value);
    localStorage.setItem("country_name", item.terms.pop().value);
    localStorage.setItem("distance", this.distance);
    this.geoCode(item.description);
  }
  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ address: address }, (results, status) => {
      localStorage.setItem("lat", results[0].geometry.location.lat());
      localStorage.setItem("long", results[0].geometry.location.lng());
      this.zone.run(() => {});
    });
  }
  currentPosition() {
    this.global.currentLocation().subscribe(res => {
      let data = res.json();
      localStorage.setItem("lat", data.location.latitude);
      localStorage.setItem("long", data.location.longitude);
      localStorage.setItem("city", data.city);
      localStorage.setItem("country_name", data.country.name);
    });
  }
  next() {
    console.log("bfkbdfskbskfbskdfkdsbfkdsbkfjbsd");
    this.router.navigate(["/home/community/findcommunity"]);
  }
}
