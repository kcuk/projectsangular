import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindCommunityComponent } from './find-community.component';

describe('FindCommunityComponent', () => {
  let component: FindCommunityComponent;
  let fixture: ComponentFixture<FindCommunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindCommunityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindCommunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
