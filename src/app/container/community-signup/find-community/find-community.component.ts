import { Component, NgZone, OnInit } from "@angular/core";
import { Http } from "@angular/http";
import { GlobalService } from "../../../GlobalService";
import { Router } from "@angular/router";
import { TranslateService } from "ng2-translate";

declare var google;

@Component({
  selector: "app-find-community",
  templateUrl: "./find-community.component.html",
  styleUrls: ["./find-community.component.css"]
})
export class FindCommunityComponent implements OnInit {
  // nearcommunity: any = [];
  selectedValues: any;
  msgs: any;
  distance: any =
    localStorage.getItem("distance") != "undefined" &&
    localStorage.getItem("distance")
      ? localStorage.getItem("distance")
      : 15;
  loader: boolean;
  autocompleteItems: any[];
  GoogleAutocomplete: any;

  public nameSelected: String = "";
  public from_filter: string = "";
  teams: any;
  public page: number = 1;
  public isError: boolean = false;
  public isdata: boolean = false;
  public selectedCityId: any = "";
  logo = this.global.image_url;

  nearbycommunity: any;
  communitysearch: any = [];
  nodata: boolean = false;
  activateLoader: boolean = false;
  loadr: boolean = false;
  // nosearch: boolean = true;
  keyname: any;
  noneardata: boolean = false;
  searcharea: boolean = false;
  joinpeloteacommunity: boolean = false;
  peloteaid = 20;

  constructor(
    public zone: NgZone,
    public global: GlobalService,
    public router: Router,
    public translate: TranslateService
  ) {
    if (localStorage.getItem("language") != null) {
      translate.use(localStorage.getItem("language"));
    }
    // this.nearcommunity = [{ name: "Royal city" }, { name: "Black hawk" }];
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
  }

  ngOnInit() {
    this.currentPosition();
  }

  joinpelotea() {
    this.joinpeloteacommunity = true;
  }
  closejoin() {
    this.joinpeloteacommunity = false;
  }
  getnearbycommunity() {
    this.loadr = true;
    let lat = localStorage.getItem("lat");
    let long = localStorage.getItem("long");
    var url =
      this.global.base_path +
      "api/peloteando/communityList/?longitude=" +
      long +
      "&latitude=" +
      lat;
    this.global.GetRequest(url).subscribe(
      res => {
        console.log(res, "res=================");
        this.loadr = false;
        this.nearbycommunity = res[0].json;
        // this.nearbycommunity = [];
        if (this.nearbycommunity.length == 0) {
          this.noneardata = true;
        } else {
          this.noneardata = false;
        }
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }

  communityfilter(name, val?) {
    this.searcharea = true;
    this.communitysearch = [];
    this.getcommunitybyname(name);
  }

  getcommunitybyname(name) {
    this.activateLoader = true;
    let lat = localStorage.getItem("lat");
    let long = localStorage.getItem("long");

    var url =
      this.global.base_path +
      "api/peloteando/communityList/?longitude=" +
      long +
      "&latitude=" +
      lat +
      "&search_key=" +
      name;
    this.global.GetRequest(url).subscribe(
      res => {
        console.log(res, "res=================");
        this.communitysearch = res[0].json;
        this.activateLoader = false;
        // this.nosearch = false;
        if (this.communitysearch.length == 0) {
          this.nodata = true;
        } else {
          this.nodata = false;
        }
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }
  search(event) {
    console.log("valussssssssss", event);
    this.GoogleAutocomplete.getPlacePredictions(
      { input: event.query, types: ["(regions)"] },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach(prediction => {
            this.autocompleteItems.push(prediction);
          });
          console.log("list", this.autocompleteItems);
        });
      }
    );
  }

  setLoaction(item) {
    console.log("item=>>>>>>>>>>>>>>>>", item);
    this.loader = true;
    localStorage.setItem("city", item.terms[0].value);
    localStorage.setItem("country_name", item.terms.pop().value);
    localStorage.setItem("distance", this.distance);
    this.geoCode(item.description);
  }

  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ address: address }, (results, status) => {
      localStorage.setItem("lat", results[0].geometry.location.lat());
      localStorage.setItem("long", results[0].geometry.location.lng());
      // this.zone.run(() => {});
      this.getnearbycommunity();
    });
  }

  currentPosition() {
    this.global.currentLocation().subscribe(res => {
      let data = res.json();
      localStorage.setItem("lat", data.location.latitude);
      localStorage.setItem("long", data.location.longitude);
      localStorage.setItem("city", data.city);
      localStorage.setItem("country_name", data.country.name);
      this.getnearbycommunity();
    });
  }

  joinnow(data) {
    localStorage.setItem("communityid", data.community_id);
    // this.router.navigate(["/home/community/selectrole"]);
    this.getcommunityinfo(data.community_id);
  }
  getcommunityinfo(id) {
    let url = this.global.base_path + "api/user/communities/" + id + "/";
    this.global.GetRequest(url).subscribe(
      res => {
        let data = res[0].json;
        console.log(data, "data==============");
        localStorage.setItem("community_info", JSON.stringify(data));
        // for the redirection
        if (localStorage.getItem("community_info")) {
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          let comname = comdata.community_name;
          console.log(
            comname,
            "community name in the join cmmunity page new section======================"
          );
          this.zone.run(() => {
            this.router.navigate([comname]);
            // window.location.reload();
            // this.valueChanged();
          });
        } else {
        }
      },
      err => {}
    );
  }

  continue() {
    localStorage.setItem("communityid", this.global.peloteaid);
    this.router.navigate(["/home/community/selectrole"]);
    localStorage.setItem("communitynav_name", "Pelotea");
  }
}
