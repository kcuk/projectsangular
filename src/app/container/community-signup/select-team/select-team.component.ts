import { Component, NgZone, OnInit, ViewChild } from "@angular/core";
import { Http } from "@angular/http";
import { GlobalService } from "../../../GlobalService";
import { Router } from "@angular/router";
import { ImageCropperComponent, CropperSettings } from "ng2-img-cropper";
import { TranslateService } from "ng2-translate";

@Component({
  selector: "app-select-team",
  templateUrl: "./select-team.component.html",
  styleUrls: ["./select-team.component.css"]
})
export class SelectTeamComponent implements OnInit {
  selectedcatag: any;
  teams: any;
  teamsearch: any = [];
  msgs: any;
  activateLoader: boolean = false;
  nodata: boolean = false;
  nosearch: boolean = true;
  loadr: boolean = false;
  errord: boolean = false;
  alreadysenterr: boolean = false;

  emptydata: boolean = false;
  searcharea: boolean = false;
  createteamview: boolean = false;
  image_name: string = "";
  enable: boolean = false;
  coverPic: "";
  circleImg: boolean = false;
  squareImg: boolean = false;
  display: boolean = false;
  data: any;
  cropperSettings: CropperSettings;
  data2: any;
  cropperSettings2: CropperSettings;
  notification_logo: string = "";
  file_name: string;
  fileselected: boolean = false;
  teamname: any;
  successteam: boolean = false;
  teamnameerror: boolean = false;
  oneteamonly: boolean = false;
  teamlogo: any;
  logo = this.global.image_url;
  imglogo = this.global.image_url;
  requestsend: boolean = false;

  @ViewChild("cropper", undefined)
  cropper: ImageCropperComponent;

  public nameSelected: String = "";
  createteamoption: boolean = false;
  constructor(
    public zone: NgZone,
    public global: GlobalService,
    public router: Router,
    public translate: TranslateService
  ) {
    if (localStorage.getItem("language") != null) {
      translate.use(localStorage.getItem("language"));
    }
    this.rectangleCropImage();
    this.circleCropImage();
    // this.teams = [
    //   { name: "team name 1", id: 1 },
    //   { name: "team name 2", id: 2 },
    //   { name: "team name 3", id: 3 },
    //   { name: "team name 4", id: 4 }
    // ];
  }

  ngOnInit() {
    let catag = localStorage.getItem("signupcategory");
    console.log(catag, "pring the catag");
    if (catag == "teammanager") {
      this.createteamoption = true;
    } else {
      this.createteamoption = false;
    }
    this.getteams();
  }
  backtodash() {
    let comdata = JSON.parse(localStorage.getItem("community_info"));
    console.log(comdata, "fgewguigewewrgwue");
    let comname = comdata.community_name;
    console.log(comname, "community name we get======================");
    this.router.navigate([comname]);
  }
  getteams() {
    this.searcharea = false;
    this.loadr = true;
    let communityid = localStorage.getItem("communityid");
    var url =
      this.global.base_path +
      "api/team/communityTeams/?community_id=" +
      communityid;

    this.global.GetRequest(url).subscribe(
      res => {
        console.log(res, "res=================");
        this.loadr = false;
        this.teams = res[0].json;
        console.log("teams",this.teams)
        if (this.teams.length == 0) {
          this.emptydata = true;
        } else {
          this.emptydata = false;
        }
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }

  teamfilter(name, val?) {
    this.searcharea = true;
    this.teamsearch = [];
    this.getteambyname(name);
  }
  getteambyname(name) {
    this.activateLoader = true;
    let communityid = localStorage.getItem("communityid");

    var url =
      this.global.base_path +
      "api/team/communityTeams/?community_id=" +
      communityid +
      "&search_key=" +
      name;
    this.global.GetRequest(url).subscribe(
      res => {
        console.log(res, "res=================");
        this.teamsearch = res[0].json;
        this.activateLoader = false;
        this.nosearch = false;
        if (this.teamsearch.length == 0) {
          this.nodata = true;
        } else {
          this.nodata = false;
        }
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }
  joinnowwithnoteams() {
    let data = JSON.parse(localStorage.getItem("user_info"));
    if (data) {
      this.joinmultiplecommunity();
      console.log("user is valid to create a new community");
    } else {
      console.log("join community without any team");
      this.router.navigate(["/home/signup"]);
    }
  }
  joinnow(id) {
    let data = JSON.parse(localStorage.getItem("user_info"));
    if (data) {
      this.joinmultiplecommunity(id);
      console.log("user is valid to create a new community");
    } else {
      localStorage.setItem("signupteamid", id);
      this.router.navigate(["/home/signup"]);
    }
  }
  tocommunity() {
    this.router.navigate(["/home/community"]);
  }

  joinmultiplecommunity(id?) {
    let communityid = localStorage.getItem("communityid");
    let data = {
      player_type: localStorage.getItem("signupcategory"),
      created_team: localStorage.getItem("createdteamid"),
      team_id: id,
      community_id: communityid
    };
    var url = this.global.base_path + "api/user/join_community/";
    this.global.PostRequest(url, data).subscribe(
      res => {
        console.log(res, "res=================");
        let respons = res[0].json;
        this.requestsend = true;
      },
      err => {
        console.log(err, "error in the communiyy");
        if (err.status == 403) {
          this.alreadysenterr = true;
        } else if (err.status == 400) {
          this.errord = true;
        }
      }
    );
  }

  createteam() {
    this.createteamview = true;
  }
  joinanother() {
    this.successteam = false;
    this.createteamoption = false;
    this.oneteamonly = true;
  }
  continue() {
    this.router.navigate(["/home/signup"]);
  }
  createteamsave() {
    console.log(this.teamname, "teamname");
    if (this.teamname != "" || this.teamname != undefined) {
      console.log(" team name exist");
      let lat = localStorage.getItem("lat");
      let long = localStorage.getItem("long");
      let communityid = localStorage.getItem("communityid");
      let country_name = localStorage.getItem("country_name");
      let city = localStorage.getItem("city");

      let data = {
        team_name: this.teamname,
        latitude: lat,
        longitude: long,
        country_name: country_name,
        city_name: city,
        community_id: communityid,
        team_logo: this.notification_logo
      };
      var url = this.global.base_path + "api/team/createTeam/";
      this.global.PostRequestUnauthorised(url, data).subscribe(
        res => {
          console.log(res, "res=================");
          let respons = res[0].json;
          this.createteamview = false;
          this.successteam = true;
          localStorage.setItem("createdteamid", respons.team_id);
          this.teamlogo = respons.team_logo;
        },
        err => {
          if (err.status == 404) {
            this.msgs = [];
            this.msgs.push({
              severity: "error",
              summary: "",
              detail: "Network problem"
            });
          } else {
            this.msgs = [];
            this.msgs.push({
              severity: "error",
              summary: " ",
              detail: "some error, try again..."
            });
          }
        }
      );
    } else {
      this.teamnameerror = true;
    }
  }

  closeDialog() {
    this.createteamview = false;
  }
  fileChangeListener(event, cover) {
    this.enable = true;
    this.image_name = event.target.files[0].name;
    this.coverPic = cover;
    if (cover == "coverPic") {
      this.circleImg = false;
      this.squareImg = true;
    } else {
      this.circleImg = true;
      this.squareImg = false;
    }
    var image: any = new Image();
    var file: File = event.target.files[0];
    var myReader: FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function(loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
      that.logo = image.src.split(",")[1];
    };
    myReader.readAsDataURL(file);
    this.showDialog();
  }
  showDialog() {
    this.display = true;
    this.enable = true;
  }
  disable() {
    this.display = false;
    this.enable = false;
  }
  save() {
    this.notification_logo = this.logo;
    this.image_name = this.file_name;
    this.display = false;
    this.enable = false;
    console.log(this.notification_logo, "file name selected");
    if (this.notification_logo != "") this.fileselected = true;
    else {
      this.fileselected = false;
    }
  }

  public rectangleCropImage() {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 1200;
    this.cropperSettings.height = 250;
    this.cropperSettings.keepAspect = false;

    this.cropperSettings.croppedWidth = 1200;
    this.cropperSettings.croppedHeight = 250;

    this.cropperSettings.canvasWidth = 350;
    this.cropperSettings.canvasHeight = 300;

    this.cropperSettings.minWidth = 100;
    this.cropperSettings.minHeight = 100;

    this.cropperSettings.rounded = false;
    this.cropperSettings.minWithRelativeToResolution = false;

    this.cropperSettings.cropperDrawSettings.strokeColor =
      "rgba(255,255,255,1)";
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings.noFileInput = true;

    this.data = {};
  }
  public circleCropImage() {
    this.cropperSettings2 = new CropperSettings();
    this.cropperSettings2.width = 200;
    this.cropperSettings2.height = 200;
    this.cropperSettings2.keepAspect = false;

    this.cropperSettings2.croppedWidth = 200;
    this.cropperSettings2.croppedHeight = 200;

    this.cropperSettings2.canvasWidth = 350;
    this.cropperSettings2.canvasHeight = 300;

    this.cropperSettings2.minWidth = 100;
    this.cropperSettings2.minHeight = 100;

    this.cropperSettings2.rounded = true;
    this.cropperSettings2.minWithRelativeToResolution = true;

    this.cropperSettings2.cropperDrawSettings.strokeColor =
      "rgba(255,255,255,1)";
    this.cropperSettings2.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings2.noFileInput = true;

    this.data2 = {};
  }
}
