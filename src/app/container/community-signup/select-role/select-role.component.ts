import { Component, NgZone, OnInit } from "@angular/core";
import { Http } from "@angular/http";
import { GlobalService } from "../../../GlobalService";
import { Router } from "@angular/router";
import { TranslateService } from "ng2-translate";

@Component({
  selector: "app-select-role",
  templateUrl: "./select-role.component.html",
  styleUrls: ["./select-role.component.css"]
})
export class SelectRoleComponent implements OnInit {
  nocateg: boolean = false;
  selectedcatag: any;
  constructor(
    public zone: NgZone,
    public global: GlobalService,
    public router: Router,
    public translate: TranslateService
    
  ) {
    if (localStorage.getItem("language") != null) {
      translate.use(localStorage.getItem("language"));
    }
  }

  ngOnInit() {
   
  }
  asmanager() {
    localStorage.setItem("signupcategory", "teammanager");
    this.router.navigate(["/home/community/selectteam"]);
  }

  asplayer() {
    localStorage.setItem("signupcategory", "player");
    this.router.navigate(["/home/community/selectteam"]);
  }
}
