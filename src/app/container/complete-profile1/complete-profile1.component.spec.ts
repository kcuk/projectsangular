import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteProfile1Component } from './complete-profile1.component';

describe('CompleteProfile1Component', () => {
  let component: CompleteProfile1Component;
  let fixture: ComponentFixture<CompleteProfile1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteProfile1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteProfile1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
