import { Component, OnInit } from '@angular/core';
import { Message, SelectItem } from 'primeng/primeng';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { FacebookService } from './../../services/login/facebookLogin.service';
import { LoginService } from '../../services/login/login.service';
import { GlobalService } from './../../GlobalService';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { TranslateService } from 'ng2-translate';
declare var FB: any;


@Component({
  selector: 'app-complete-profile1',
  templateUrl: './complete-profile1.component.html',
  styleUrls: ['./complete-profile1.component.css']
})
export class CompleteProfile1Component implements OnInit {
  cityValid: boolean;

  msgs: Message[] = [];
  lang = localStorage.getItem('language')
  loader: boolean = false;
  zoneName: any;
  zoneList: SelectItem[];
  filteredLocation: Array<any> = [{}];
  completeProfileForm: FormGroup;
  profileDataIns;
  genderList;
  positionList: Array<any> = [];
  prefferedLegList: Array<any> = [];
  prefLeg: string = "derecho";
  countryList: Array<any> = [];
  country;
  country_required: boolean = false;
  positionName: string;
  prefLegName: string;
  genderName: string;
  countryName: string;
  city_id: number;
  filteredTown: any[];
  selectedGender: any = ""
  selectedCountry: any = ""
  selectedLeg: any = ""
  city_name: string = '';
  city_required: boolean = false;
  referee;
  date: Date;
  birthdate: string;
  Email: string;
  refereeName;
  es: any;
  country_code: string;
  code: any = [
    { value: '+593', label: '+593' },
    { value: '+1', label: '+1' }
  ];
  length: any;

  constructor(private translate: TranslateService, private fb: FormBuilder, private facebook: FacebookService, private base_path_service: GlobalService, private router: Router) {

    this.profileDataIns = new profileData();
    this.completeProfileForm = new FormGroup({
      dob: new FormControl('', Validators.compose([])),
      Nombre: new FormControl('', Validators.compose([Validators.required])),
      Apellido: new FormControl('', Validators.compose([])),
      Mobile: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      Email: new FormControl('', Validators.compose([])),
      gender: new FormControl('', Validators.compose([Validators.required])),
      name: new FormControl('', Validators.compose([])),
      position: new FormControl('', Validators.compose([Validators.required])),
      bio: new FormControl('', Validators.compose([])),
      country: new FormControl('', Validators.compose([Validators.required])),
      city: new FormControl('', Validators.compose([Validators.required])),
      price: new FormControl(this.profileDataIns.price, Validators.compose([Validators.maxLength(6), Validators.pattern('[0-9]+')])),
      // referee_price: new FormControl(this.profileDataIns.referee_price, Validators.compose([Validators.maxLength(6), Validators.pattern('[0-9]+')])),
      pref_leg: new FormControl('', Validators.compose([Validators.required])),
      is_professional: new FormControl('', Validators.compose([])),
      country_code: new FormControl('')

    })

    this.genderList = [
      {
        "value": "",
        "label": localStorage.getItem('language') == 'en' ? "Select Gender" : "Seleccionar Género"
      },
      {
        "value": "Masculino",
        "label": localStorage.getItem('language') == 'en' ? "Male" : "Masculino"
      },
      {
        "value": "Femenino",
        "label": localStorage.getItem('language') == 'en' ? "Female" : "Femenino"
      }
    ]

    this.referee = [
      {
        "value": "",
        "label": localStorage.getItem('language') == 'en' ? "Select" : "Seleccione"
      },
      {
        "value": "true",
        "label": "SÃ­"
      },
      {
        "value": "false",
        "label": "No"
      }
    ]


    this.prefferedLegList = [
      {
        "value": "",
        "label": localStorage.getItem('language') == 'en' ? "Select Leg Pref" : "Seleccionar Pierna Pref "
      },
      {
        "value": "Izquierda",
        "label": localStorage.getItem('language') == 'en' ? "Left footed" : "Izquierda"
      },
      {
        "value": "Derecha",
        "label": localStorage.getItem('language') == 'en' ? "Right footed" : "Derecha"
      },
      {
        "value": "Ambidiestro",
        "label": localStorage.getItem('language') == 'en' ? "Ambidextrous" : "Ambidiestro"
      }
    ]
    this.positionList.push({ label: localStorage.getItem('language') == 'en' ? "Select position" : "Seleccionar posición", value: '' });
    this.countryList.push({ label: localStorage.getItem('language') == 'en' ? "Select country" : "Seleccionar país", value: '' });
  }

  ngOnInit() {
    this.es = {
      closeText: "Cerrar",
      prevText: "",
      currentText: "Hoy",
      monthNames: [
        this.lang == 'en' ? 'January' : "enero",
        this.lang == 'en' ? 'February' : "febrero",
        this.lang == 'en' ? 'March' : "marzo",
        this.lang == 'en' ? 'April' : "abril",
        this.lang == 'en' ? 'May' : "mayo",
        this.lang == 'en' ? 'June' : "junio",
        this.lang == 'en' ? 'July' : "julio",
        this.lang == 'en' ? 'August' : "agosto",
        this.lang == 'en' ? 'September' : "septiembre",
        this.lang == 'en' ? 'October' : "octubre",
        this.lang == 'en' ? 'November' : "noviembre",
        this.lang == 'en' ? 'December' : "diciembre"
      ], monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
      dayNames: ["domingo", "lunes", "martes", "miÃƒÂ©rcoles", "jueves", "viernes", "sÃƒÂ¡bado"],
      dayNamesShort: ["dom", "lun", "mar", "miÃƒÂ©", "jue", "vie", "sÃƒÂ¡b"],
      dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
      weekHeader: "Sm", dateFormat: "yy-mm-dd",
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ""
    };

    this.API_getCountryList();
    this.API_getPositionList();
    if (localStorage.getItem("fb_status")) {
      this.loader = true;
      this.getFbData();
    } else {
      this.loader = true;
      this.profileDataIns.name = JSON.parse(localStorage.getItem('userInfo'))[0].info.username;
      this.Email = JSON.parse(localStorage.getItem('userInfo'))[0].info.email;
      this.profileDataIns.last_name = JSON.parse(localStorage.getItem('userInfo'))[0].info.last_name;
      if (JSON.parse(localStorage.getItem('userInfo'))[0].info.mobile != null) {
        this.profileDataIns.Mobile = JSON.parse(localStorage.getItem('userInfo'))[0].info.mobile;
        this.country_code = JSON.parse(localStorage.getItem('userInfo'))[0].info.country_code;

      }
      else {
        this.country_code = '+593';
      }
      if (this.country_code == '+593') {
        this.length = 9
      }
      else {
        this.length = 10;
      }
      this.loader = false;
    }

    this.positionName = "Seleccione";
    this.genderName = "Seleccione";
    this.prefLegName = "Seleccione";
    this.countryName = "Seleccione";
    this.refereeName = "Seleccione";
  }


  API_getPositionList() {

    var url = this.base_path_service.base_path_api() + "user/position/?format=json"
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        for (let i = 0; i < res[0].json.length; i++) {
          this.positionList.push({
            "label": this.lang == 'en' ? res[0].json[i].position_name_en : res[0].json[i].position_name,
            "value": res[0].json[i].id
          })
        }
      },
        err => {
        })
  }

  API_getCountryList() {
    var url = this.base_path_service.base_path_api() + "team/country/?format=json";
    this.base_path_service.GetRequestUnauthorised(url)
      .subscribe(
        res => {
          for (let i = 0; i < res[0].json.length; i++) {
            this.countryList.push({ "label": this.lang == 'en' ? res[0].json[i].country_name : res[0].json[i].country_spanish, "value": res[0].json[i].id })
          }
        },
        err => {
        })
  }

  filterLocation(event) {
    this.cityValid = false;
    let query = event.query;
    if (this.country != undefined) {
      this.country_required = false;
      var url = this.base_path_service.base_path_api() + "team/location/?country=" + this.country + "&city=" + query + '&format=json';
      this.base_path_service.GetRequest(url)
        .subscribe(
          res => {
            this.filteredLocation = this.filterCountry(query, res[0].json);
          },
          err => {
          });
    } else {
      this.country_required = true;
    }
  }

  filterCountry(query, location: any[]): any[] {
    let filtered: any[] = [];
    for (let i = 0; i < location.length; i++) {
      let city = location[i];
      if (city.location.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(city);
      }
    }
    return filtered;
  }

  filterTown(event) {
    let query = event.query;
    if (this.city_id != undefined) {
      this.city_required = true;
      if (query.length >= 2) {
        var url = this.base_path_service.base_path_api() + "team/location/?form_type=town&city_pk=" + this.city_id + "&town=" + query + '&format=json';
        this.base_path_service.GetRequest(url)
          .subscribe(
            res => {
              this.filteredTown = res[0].json;
            },
            err => {
            });
      }
    } else {
      this.city_required = true;
    }
  }

  datepicker(event) {
    this.profileDataIns.dob = event;
  }

  selectedGenderFun(event) {
    this.profileDataIns.gender = event;
  }

  selectedPositionFun(event) {
    this.profileDataIns.position = event.value;
  }

  selectedPrefLegFun(event) {
    this.profileDataIns.leg = event.value;
  }

  selectedCountryFun(event) {
    this.completeProfileForm.patchValue({ 'city': '' })
    this.country_required = false;
    this.country = event.value;
  }

  selectedRefereeFun(event) {
    this.profileDataIns.is_referee = event.value;
  }

  getCheckboxValue(event) {
  }

  getLocation(event) {
    this.cityValid = true
    this.profileDataIns.city = event.id
    this.city_id = event.id;
  }

  getTown(event) {
    this.profileDataIns.town = event.id
  }
  formatDate(event) {
    var validDate;
    if (event) {
      let month = parseInt(event.getMonth());
      month = month + 1
      if (event.getMonth() > 8) {
        if (event.getDate() > 9) {
          validDate = event.getFullYear() + "-" + month + "-" + event.getDate();
        } else {
          validDate = event.getFullYear() + "-" + month + "-" + '0' + event.getDate();
        }

      } else {
        if (event.getDate() > 9) {
          validDate = event.getFullYear() + "-" + '0' + month + "-" + event.getDate();
        } else {
          validDate = event.getFullYear() + "-" + '0' + month + "-" + '0' + event.getDate();
        }
      }
      return validDate;
    }
  }
  saveDetails(data: any) {
    if (this.completeProfileForm.valid && this.cityValid) {
      data.country_code = this.country_code;
      data.price = 0;
      if (this.profileDataIns.is_referee == null || this.profileDataIns.is_referee == false)
        data.referee_price = 0;
      data.dob = this.formatDate(this.profileDataIns.dob);
      let city = data.city;
      let flag = false;
      for (let i = 0; i < this.filteredLocation.length; i++) {
        if (city == this.filteredLocation[i].id)
          flag = true;
      }
      if (flag) {
        this.loader = true;
        var url = this.base_path_service.base_path_api() + "user/profile/?format=json";
        this.base_path_service.PostRequest(url, data).subscribe(res => {
          this.loader = false;
          if (res[0].status == 205 || res[0].status == 200 || res[0].status == 201) {
            localStorage.setItem("username", JSON.stringify(res[0].json.name));
            localStorage.setItem("nick_name", JSON.stringify(res[0].json.nick_name));
            if (JSON.parse(localStorage.getItem("userInfo"))[0].info.second_page == true) {
              this.router.navigateByUrl('/complete-profile2')
            } else {
              this.router.navigate(["/dashboard"]);
            }
          }
        },
          err => {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: err.json(), detail: 'Try again !!' });

          })

      }
      else {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '', detail: 'Invalid Ciudad' });
        this.loader = false;
      }
    } else {
      this.msgs = [];
      this.msgs.push({ severity: 'error', detail: 'Ingresa todos los campos obligatorios' });
    }
  }

  skipForm() {
    this.loader = true;
    let obj = {
      form_type: "skip"
    }
    var url = this.base_path_service.base_path_api() + "user/profile/?format=json";
    this.base_path_service.PostRequest(url, obj)
      .subscribe(
        res => {
          if (res[0].status == 205 || res[0].status == 200 || res[0].status == 201) {
            this.loader = false;
            if (JSON.parse(localStorage.getItem('userInfo'))[0].info.second_page == true) {
              this.router.navigateByUrl('/complete-profile2')
            } else {
              this.router.navigate(["/dashboard"]);
            }
          }
          else {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: 'Some Error!', detail: 'Try again !!' });
          }
        },
        err => {
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: err.json(), detail: 'Try again !!' });
        })
  }

  getFbData() {
    FB.api('/me?fields=gender,first_name,last_name,birthday,email', response => {
      this.profileDataIns.name = response.first_name;
      this.country_code = '+593';
      this.length = 9;
      this.profileDataIns.last_name = response.last_name;
      if (response.gender == 'male') {
        this.selectedGender = 'Masculino';
      } else if (response.gender == 'male') {
        this.selectedGender = 'Femenino';
      }
      let x = new Date(response.birtday);
      if (x.toDateString() != 'Invalid Date') {
        this.profileDataIns.dob = x.getFullYear() + '-' + x.getMonth() + '-' + x.getDate();
      }
      this.Email = response.email;
      this.loader = false;
    })
  }

  validMobile(event) {
    console.log("event=>>>>>>>>>>>>>>>>>>>>>>>", event);
    this.completeProfileForm.patchValue({ 'Mobile': "" })
    if (event.value == '+1') {
      this.length = 10
    }
    else {
      this.length = 9
    }
  }
}

class profileData {
  "form_type": string = "on_boarding1";
  "dob": number = null;
  "gender": number = null;
  "name": string = "";
  "position": string = "";
  "bio": string = "";
  "nick_name": string = "";
  "is_professional": boolean = false;
  "last_name": string = "";
  "price": number = null;
  "referee_price": number = null;
  "leg": string = "";
  "is_referee": boolean = false;
  "city": number = null;

}