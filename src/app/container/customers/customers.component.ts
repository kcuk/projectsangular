import { GlobalService } from './../../GlobalService';
import { Router,NavigationEnd } from '@angular/router';
import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Http } from '@angular/http';
import { DOCUMENT } from "@angular/platform-browser";
declare const $: any;

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css'],
  animations: [
    trigger(
      'myAnimation',
      [
        transition(
          ':enter', [
            style({ transform: 'translateY(-100%)', opacity: 0 }),
            animate('500ms', style({ transform: 'translateY(0)', 'opacity': 1 }))
          ]
        ),
        transition(
          ':leave', [
            style({ transform: 'translateY(0)', 'opacity': 1 }),
            animate('500ms', style({ transform: 'translateY(-100%)', 'opacity': 0 }))
          ]
        )]
    )
  ]
})
export class CustomersComponent implements OnInit {
  public page: number = 1;
  menuState: string = 'out';
  show: boolean = false;
  public storyCollection: Array<any> = [];
  public imageUrl: string = "";
  public cInfo: any;
  public status: number = 0;
  public total_page: number = 0;
  public increment: number = 0;
  constructor( @Inject(DOCUMENT) public document: Document, public router: Router, public global: GlobalService, public http: Http) {

  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
    this.imageUrl = this.global.image_url;
    this.customers();
  }
  customers() {
    let url = this.global.base_path_api() + "user/cstory/?page=" + this.page + "&format=json";
    this.http.get(url)
      .map(res => {
        return res.json();
      }).subscribe(res => {
        this.total_page = res.total_num_of_pages;
        this.storyCollection.push(res.data);


      }, err => {

      })
  }
  customerInfo(id) {
    if (!this.show) {
      let url = this.global.base_path_api() + "user/cstory/" + id + "/?format=json";
      this.http.get(url)
        .map(res => {
          return res.json();
        }).subscribe(res => {
          this.cInfo = res;
          this.show = this.show ? false : true;
        }, err => {

        })
    } else {
      this.show = false;
    }
  }
  toggleMenu() {
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
  }

  @HostListener('window:scroll', ['$event'])
  track(event: any) {
    let number = this.document.body.scrollHeight;
    let totalNumber = window.pageYOffset + window.innerHeight;
    if (number == totalNumber) {
      this.page = this.page + 1;
      if (this.page <= this.total_page) {
        this.customers();
      }
    }
  }
}
