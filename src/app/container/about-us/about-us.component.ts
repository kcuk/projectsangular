import { Router,NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from "ng2-translate";
declare const google;
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  constructor(private translate: TranslateService, public router:Router) {
     if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
      console.log("lang=>>>>>>>>>>>>>>>>>>>", localStorage.getItem('language'));
    }
   }

  ngOnInit() {
  
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }

}
