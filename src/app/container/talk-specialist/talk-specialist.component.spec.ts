import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TalkSpecialistComponent } from './talk-specialist.component';

describe('TalkSpecialistComponent', () => {
  let component: TalkSpecialistComponent;
  let fixture: ComponentFixture<TalkSpecialistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TalkSpecialistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TalkSpecialistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
