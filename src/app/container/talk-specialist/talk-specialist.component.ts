import { Router, NavigationEnd } from '@angular/router';
import { Http } from '@angular/http';
import { GlobalService } from './../../GlobalService';
import { FormGroup, FormBuilder, Validator, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SelectItem, Message } from 'primeng/primeng';
@Component({
  selector: 'app-talk-specialist',
  templateUrl: './talk-specialist.component.html',
  styleUrls: ['./talk-specialist.component.css']
})
export class TalkSpecialistComponent implements OnInit {
  city_valid: boolean;
  isMobValid: boolean = true;
  public msgs: Message[] = [];
  public specialist_form: FormGroup;
  public filteredLocation: any;
  public countryList: SelectItem[] = []
  lang = localStorage.getItem('language')
  public quote_options: SelectItem[] = [
    { label: this.lang == 'en' ? 'I am interested in learning more about…' : 'Me interesa conocer más de…', value: null },
    { label: this.lang == 'en' ? 'Manage leagues or tournaments' : 'Manejo de torneos y ligas', value: 'Tournaments' },
    { label: this.lang == 'en' ? 'Manage field reservations' : 'Manejo de canchas y reservas', value: 'Fields' },
    { label: this.lang == 'en' ? 'Manage teams' : 'Manejo de equipos', value: 'Teams' },
    { label: this.lang == 'en' ? 'Being a player in Pelotea ' : 'Ser un jugador de Pelotea', value: 'Players' },
    { label: this.lang == 'en' ? 'Scouting' : 'Reclutamiento de jugadores', value: 'Scouting' },
    { label: this.lang == 'en' ? 'Becoming a sponsor' : 'Ser un auspiciante', value: 'Other' },
  ];
  code: any = [
    { value: '+1', label: '+1' },
        { value: '+502', label: '+502' },

    { value: '+593', label: '+593' }
  ];
  length: any = 9;

  constructor(public router: Router, public fb: FormBuilder, public global_services: GlobalService, public http: Http) { }

  ngOnInit() {
    this.specialist_form = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+')])],
      contact_number: ['', Validators.compose([])],
      country: ['', Validators.required],
      city: ['', Validators.required],
      company_name: [],
      learn_more_about: [],
      country_code: ['+593', Validators.compose([])]
    })

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });

    let url = this.global_services.base_path_api() + "team/country/?format=json";
    this.global_services.GetRequestUnauthorised(url)
      .subscribe(
      res => {
        for (let i = 0; i < res[0].json.length; i++) {
          this.countryList.push({ "label": this.lang=='en'? res[0].json[i].country_name:res[0].json[i].country_spanish, "value": res[0].json[i].id })
        }
      },
      err => {
      })
  }
  public filterLocation(event) {
    let query = event.query;
    var url = this.global_services.base_path_api() + "team/location/?country=" + this.specialist_form.controls['country'].value + "&city=" + query + '&format=json';
    this.http.get(url)
      .map(res => {
        return res.json();
      })
      .subscribe(res => {
        this.filteredLocation = res;
      },
      err => {
      });

  }
  public contactMe() {
    console.log(this.specialist_form.controls['city'].value.id, "idddddddddddddddddd")
    let url = this.global_services.base_path_api() + 'user/contectAs/';
    if (this.specialist_form.controls['city'].value.id) {
      this.specialist_form.value.city = this.specialist_form.controls['city'].value.id;
    }
    if (this.specialist_form.controls['city'].value.id != null) {
      this.http.post(url, this.specialist_form.value)
        .map(res => {
          return res.json();
        })
        .subscribe(res => {
          this.specialist_form.reset();
          this.length = 9;
          this.specialist_form.patchValue({'country_code':'+593'})
          this.msgs = []
          this.msgs.push({ severity: 'info', summary: 'Thanks You', detail: '' });
          console.log("contact me", res);
        })
    }

  }

  validMobile(event) {
    console.log("event=>>>>>>>>>>>>>>>>>>>>>>>", event);
    this.specialist_form.patchValue({ 'contact_number': "" })
    if (event.value == '+1') {
      this.length = 10
    }
        else if(event.value == '+502'){
      this.length = 8
    }
    else {
      this.length = 9
    }
  }

  mobileVerify(value) {
    if (value.length != this.length || value < 0) {
      console.log("mobileeeeeeeeeeeeeeee")
      this.isMobValid = false;

    }
    else
      this.isMobValid = true;
    console.log("mobileeeeeeeeeeeeeeee =>>>>>>>>>>>>>>", this.isMobValid)

  }

  emptycity() {
    this.specialist_form.patchValue({ 'city': "" })
  }
}
