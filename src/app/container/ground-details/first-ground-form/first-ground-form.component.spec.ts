import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstGroundFormComponent } from './first-ground-form.component';

describe('FirstGroundFormComponent', () => {
  let component: FirstGroundFormComponent;
  let fixture: ComponentFixture<FirstGroundFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstGroundFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstGroundFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
