import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalService } from './../../../GlobalService';
import { SelectItem } from 'primeng/primeng';
import { TranslateService } from "ng2-translate";

@Component({
  selector: 'app-first-ground-form',
  templateUrl: './first-ground-form.component.html',
  styleUrls: ['./first-ground-form.component.css']
})
export class FirstGroundFormComponent implements OnInit {
  isMobValid: boolean;
  public groundCreateForm: FormGroup;
  public cityValidation: boolean = false;
  public filteredCity: Array<any> = [];
  public loader: boolean = false;
  public city_list: Array<any> = [];
  public filteredCountry: Array<any> = [];
  public country_list: Array<any> = [];
  public time: any;
  public costPerGame: number = 0;
  public numberOfCancha: number = 1;
  public citySelectValidation: boolean = false;
  public country_code: string = "+593";
  public country_valid: boolean = true;
  public ground_levels: any[] = [
    { label: 'Seleccionar Modality', value: null },
    { label: '5V5', value: 'V5' },
    { label: '6V6', value: 'V6' },
    { label: '7V7', value: 'V7' },
    { label: '8V8', value: 'V8' },
    { label: '9V9', value: 'V9' },
    { label: '11V11', value: 'V11' },
  ];
  public groundType: any[] = [
    { label: 'Seleccionar Type', value: null },
    { label: 'Salón', value: 'Salón' },
    { label: 'Césped sintético', value: 'Césped sintético' },
    { label: 'Césped natural', value: 'Césped natural' }
  ];
  public paymentMethodLabels: SelectItem[];
  public payment: Array<any> = [];
  public cost_per_game_popup: boolean = false;
  public msgs: Array<any> = [];
  public daysInfo: Array<any> = [];
  public priceListOfGround: Array<any> = [];
  public priceOfGround: any;
  public list: Array<any> = [];
  public connectGroundList: Array<any> = [];
  public connectGround: Array<any> = [{}];
  public dayPrice: Array<any> = [];
  public slot: Array<any> = [];
  public file: File;
  public file_name: string = '';
  public costOfGround: boolean = false;
  public costOfGroundValidation: boolean = false;
  public paymentMethodValidation: boolean = false;
  public defaultGround: any;
  public tempStatus: Array<any> = [];
  public defaultGroundstatus: Array<any> = [];
  public imageBasePath: string = '';
  public connect_cancha_popup: boolean = false;
  code: any = [
    { value: '+1', label: '+1' },
        { value: '+502', label: '+502' },

    { value: '+593', label: '+593' },
  ];
  length: any = 9;
  constructor(private translate:TranslateService, public _fb: FormBuilder, public base_path_service: GlobalService, public router: Router) {
    if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
    }
   }
  ngOnInit() {
    this.imageBasePath = this.base_path_service.image_url;
    this.buildForm();
    this.cityList();
    this.countryList();
    this.paymentMethods();
    this.daysList();
  }

  public cityList() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'team/location/?form_type=city';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.city_list = [];
        this.city_list.push({ label: 'Seleccionar City', value: null });
        let city = res[0].json;
        for (var index = 0; index < res[0].json.length; index++) {
          this.city_list.push({ label: city[index].location, value: city[index].id });
        }
      }, err => {
        console.log(err);
      })
  }
  public daysList() {
    var url = this.base_path_service.base_path_api() + "ground/days" + "/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.daysInfo = res[0].json;
        this.slot = res[0].json[0].price;
      }, err => {

      });
  }
  public filterCity(event) {
    let query = event.query;
    let country = 1;
    if (country != undefined) {
      var url = this.base_path_service.base_path_api() + "team/location/?country=" + country + "&city=" + query + "&format=json";
      this.base_path_service.GetRequest(url)
        .subscribe(
        res => {
          this.filteredCity = res[0].json;

          if (res[0].json.length == 0) {
            this.cityValidation = true;
            this.citySelectValidation = false;
          } else {
            this.cityValidation = false;
            this.citySelectValidation = false;
          }
        },
        err => {
          console.log("error")
        });
    }
    else {

    }
  }

  public countryList() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'team/country/';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.country_list = [];
        this.country_list.push({ label: 'Seleccionar País', value: null });
        let country = res[0].json;
        for (var index = 0; index < res[0].json.length; index++) {
          this.country_list.push({ label: country[index].country_name, value: country[index].id });
        }
      }, err => {
        console.log(err);
      })
  }

  public paymentMethods() {
    let url = this.base_path_service.base_path_api() + "peloteando/payment_mode/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        let paymentInfo = res[0].json;
        this.paymentMethodLabels = [];
        for (var i = 0; i < res[0].json.length; i++) {
          this.paymentMethodLabels.push({ label: paymentInfo[i].option, value: paymentInfo[i].id });
        }
      }, err => {
        this.base_path_service.print(err);
      })
  }


  public buildForm() {
    this.groundCreateForm = this._fb.group({
      groundName: ['', Validators.compose([Validators.maxLength(50), Validators.required])],
      telephone: [null, Validators.compose([Validators.required, Validators.maxLength(15), Validators.pattern('[0-9]+')])],
      mobileNo: ['', Validators.required],
      direction: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],
      country: ['', Validators.compose([Validators.required])],
      gameMode: [[], Validators.compose([Validators.required])],
      groundTypes: [[], Validators.compose([Validators.required])],
      numberOfGround: ['', []],
      days: ['', []],
      cost_per_game: ['', []],
      paymentMethod: [[], Validators.compose([Validators.required])],
      bio: ['', []],
      country_code:"+593"
    })
  }

  public submitGroundForm(form_Data) {
    if (this.groundCreateForm.valid) {
      if (form_Data.ciudad != "" && this.cityValidation != true && form_Data.city.id !== undefined && this.costOfGround && !this.paymentMethodValidation) {
        this.paymentMethodValidation = false;
        this.costOfGroundValidation = false;
        this.loader = true;
        let obj = JSON.parse(localStorage.getItem("userInfo")).token.access_token;
        let url = this.base_path_service.base_path_api() + 'ground/groundProfile' + '/?format=json';
        return new Promise((resolve, reject) => {
          var formData: any = new FormData();
          var xhr = new XMLHttpRequest();
          formData.append("ground_name", form_Data.groundName);
          formData.append("address", form_Data.direction);
          formData.append("contact", parseInt(form_Data.mobileNo));
          formData.append("other_contact", form_Data.telephone ? parseInt(form_Data.telephone) : '');
          formData.append("total_field", JSON.stringify(this.defaultGroundstatus));
          formData.append("payment_method", JSON.stringify(form_Data.paymentMethod));
          formData.append("bio", form_Data.bio);
          formData.append("country", form_Data.country);
          formData.append("city", form_Data.city.id);
          formData.append("days", JSON.stringify(this.list));
          formData.append("type_of_field", form_Data.groundTypes);
          formData.append("field_format", form_Data.gameMode);
          formData.append("country_code", this.country_code);
          formData.append("rule", this.file);
          console.log('formdata', formData)
          xhr.onreadystatechange = () => {
            if (xhr.readyState == 4) {
              if (xhr.status == 205 || xhr.status == 200 || xhr.status == 201) {
                this.loader = false;
                console.log('ground_response', xhr.response)
                let groundId = xhr.response;
                this.router.navigateByUrl('dashboard/ground/create-ground/' + groundId);
              } else {
                this.loader = false;
                console.log('else', xhr.status)
                reject(xhr.response);
              }
            }
          }, error => {
            console.log(error)
            this.msgs = [];
            this.msgs.push({ severity: 'error', detail: "El terreno con la misma información existe" });
            this.base_path_service.print(error);
            this.loader = false;
          }
          xhr.open("POST", url, true);
          xhr.setRequestHeader("Authorization", 'Bearer ' + obj);
          xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
          xhr.send(formData);
        });

      } else {
        if (form_Data.city.id === undefined) {
          this.citySelectValidation = true;
        }
        if (!this.costOfGround) {
          this.costOfGroundValidation = true;
        }
      }
    } else {
      this.msgs = [];
      this.msgs.push({ severity: 'error', detail: "Ingresa todos los campos obligatorios" });
    }
  }
  public cancelButton() {
    this.router.navigateByUrl('dashboard/ground')
  }
  public countryValid(value: any) {
    if (value) {
      this.country_valid = false;
    } else {
      this.country_valid = true;
    }
  }
  public setGroundPrice(dInfo, event) {
    dInfo.amount = event.target.value;
  }
  public saveCostOfGround() {
    this.priceOfGround = [];
    for (var index = 0; index < 7; index++) {
      for (var i = 0; i < 4; i++) {
        if (this.daysInfo[index].price[i].amount) {
          this.priceOfGround.push({ 'id': this.daysInfo[index].price[i].id, 'amount': this.daysInfo[index].price[i].amount });
        }
      }
      if (this.priceOfGround.length > 0) {
        this.list.push({ id: this.daysInfo[index].pk, price: this.priceOfGround })
      }
      this.priceOfGround = [];
    }

    if (this.list.length > 0) {
      this.costOfGround = true;
      this.costOfGroundValidation = false;
    } else {
      this.costOfGround = false;
      this.costOfGroundValidation = true;
    }
  }
  public connectGroundInfo() {
    this.connectGround = [];
    let url = this.base_path_service.base_path_api() + 'ground/connectGround/?form_type=profile&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.connectGroundList = res[0].json;
        this.defaultGround = null;
      }, err => {

      })
  }
  public Ground() {
    for (let index = 0; index < this.connectGround.length; index++) {
      if (this.connectGround[index] == this.defaultGround) {
        this.defaultGroundstatus.push({ is_default: 'True', id: parseInt(this.connectGround[index]) })
      } else {
        this.defaultGroundstatus.push({ is_default: 'False', id: parseInt(this.connectGround[index]) })
      }
    }
  }
  public resetField() {
    this.list = [];
  }
  fileChangeListener(event) {
    console.log(event.target.files[0])
    this.file = event.target.files[0];
    this.file_name = event.target.files[0].name;
  }
  public validation(value: any) {
    if (value.length != 0) {

      this.paymentMethodValidation = false;
    } else {
      this.paymentMethodValidation = true;
    }
  }
  public checkStatus(info) {
    if (info.is_default) {
      info.is_default = false;
      this.defaultGround = null;
    } else {
      info.is_default = true;

    }
  }

  validMobile(event) {
    console.log("event=>>>>>>>>>>>>>>>>>>>>>>>", event);
    this.groundCreateForm.patchValue({ 'mobileNo': "" })
    if (event.value == '+1') {
      this.length = 10
    }
    else    if (event.value == '+502') {
      this.length = 8
    }
    else {
      this.length = 9
    }
  }

  mobileVerify(value) {
    if (value.toString().length != this.length || value < 0) {
      console.log("mobileeeeeeeeeeeeeeee")
      this.isMobValid = true;

    }
    else
      this.isMobValid = false;
    console.log("mobileeeeeeeeeeeeeeee =>>>>>>>>>>>>>>", this.isMobValid)

  }
}
