import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroundDetailsComponent } from './ground-details.component';

describe('GroundDetailsComponent', () => {
  let component: GroundDetailsComponent;
  let fixture: ComponentFixture<GroundDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroundDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroundDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
