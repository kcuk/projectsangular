import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondGroundFormComponent } from './second-ground-form.component';

describe('SecondGroundFormComponent', () => {
  let component: SecondGroundFormComponent;
  let fixture: ComponentFixture<SecondGroundFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondGroundFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondGroundFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
