import { Component, OnInit,ViewChild } from '@angular/core';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import {FormGroup,FormBuilder,Validators} from  '@angular/forms';
import {Router,ActivatedRoute} from '@angular/router'
import { GlobalService } from './../../../GlobalService';
import { TranslateService } from "ng2-translate";


@Component({
  selector: 'app-second-ground-form',
  templateUrl: './second-ground-form.component.html',
  styleUrls: ['./second-ground-form.component.css']
})
export class SecondGroundFormComponent implements OnInit {

  public canchaImageInfo: Array<number> = [];
  public organizationImage: any;
  public uploadImgSrc: string = '';
  public groundId: number = 0;
  public msgs: Array<any> = [];
  public loader: boolean = false;
  public profile_pic: string = "";
  public cover_pic: string = "";
  public ground_image: "";
  public organization_image: string = "";
  public organization_name: string = "";
  public sponser_list: Array<any> = [];
  public groundCreateInfo: any;
  public sposerField: Array<number> = [];
  public file_srcs: string[] = [];
  public file_src_profile: string = "";
  public file_src_cover: string = "";
  public file_src_organization: string = "";
  public orgForm: FormGroup;
  public data: any;
  public data2: any;
  public display: boolean = false;
  public backdrop: boolean = false;
  public cropperSettings: CropperSettings;
  public cropperSettings2: CropperSettings;
  public picArgs: any;
  public cropPic: any;
  public circleImg: boolean = false;
  public squareImg: boolean = false;
  public groundInput: any;
  public orgNameStatus: boolean = false;
  public groundImageInfo: Array<any> = [{ url: '', pos: 0 }, { url: '', pos: 1 }, { url: '', pos: 2 }, { url: '', pos: 3 }, { url: '', pos: 4 }]
  public imageBasePath: string = "";
  public groundImageIndex: number;
  public groundImageUploaded: any;

  @ViewChild('auto') auto: any;
  @ViewChild('cropper', undefined) cropper: ImageCropperComponent;

  constructor(private translate:TranslateService, public router: Router, public base_path_service: GlobalService, public route: ActivatedRoute, public _fb: FormBuilder) {
    this.rectangleCropImage();
    this.circleCropImage();
     if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
    }
  }
  ngOnInit() {
    let tempValue = this.route.params.subscribe(params => {
      this.groundId = parseInt(params['id']);
    });
    this.imageBasePath = this.base_path_service.image_url;
  }
  public uploadImage(formType, index) {
    console.log("test", index)
    this.makeFileUploadRequest(formType, index).then(
      (result) => {

      },
      (error) => {
        console.error(error);
      }
    );
  }

  public makeFileUploadRequest(formType, index) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'ground/image/';
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      /*
       *-----------Append Whole Data of Ground Create Page--------------------
       */
      if (formType == "profile") {
        formData.append("form_type", "profile");
        formData.append("ground", this.groundId);
        formData.append("profile_pic", this.profile_pic);
      }
      if (formType == "cover") {
        formData.append("form_type", "cover");
        formData.append("ground", this.groundId);
        formData.append("cover_pic", this.cover_pic);
      }
      if (formType == "ground") {
        formData.append("form_type", "ground");
        formData.append("ground", this.groundId);
        formData.append("ground_image", this.ground_image);
        formData.append("position", index);
      }
      if (formType == "organization") {
        formData.append("form_type", "organization");
        formData.append("ground", this.groundId);
        formData.append("organization_image", this.organization_image);
        formData.append("organization_name", this.organization_name);
      }

      if (formType == "sponser") {
        formData.append("form_type", "sponser");
        formData.append("ground", this.groundId);
        formData.append("sponser_name", this.sponser_list[index].sponser_name);
        formData.append("sponser_image", this.sponser_list[index].sponser_image);
        formData.append("priority", this.sponser_list[index].priority);
      }
      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            this.loader = false;
            this.groundImageUploaded = JSON.parse(xhr.response);
            this.groundImageInfo[index].url = this.groundImageUploaded.image
            resolve(JSON.parse(xhr.response));
          } else if (xhr.status == 201) {
            this.loader = false;
            this.msgs.push({ severity: 'Info', detail: "Sube con éxito" });

          } else {
            reject(xhr.response);
            this.msgs.push({ severity: 'error', detail: "Error" });
            this.loader = false;
          }

        }
      }
      xhr.open("POST", url, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + JSON.parse(localStorage.getItem('userInfo')).token.access_token);
      xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
      xhr.send(formData);
    });
  }
  /*
   *---------------- Add Sponser List---------------------
   */
  public addSponserField() {
    this.sponser_list.push([]);
  }
  public deleteSponserField(index: number) {
    this.sponser_list.splice(index, 1);
  }
  public fileChange(input, args, index) {
    for (var i = 0; i < input.files.length; i++) {
      var img = document.createElement("img");
      img.src = window.URL.createObjectURL(input.files[i]);
      var reader: any, target: EventTarget;
      var reader: any = new FileReader();
      reader.addEventListener("load", (event) => {
        img.src = event.target.result;
        if (args == 'ground') {
          this.file_srcs.push(img.src);
        } else if (args == 'organization') {
          this.file_src_organization = img.src;
        } else if (args == 'sponser') {
          this.sponser_list[index].sponserImage = img.src;
        }

      }, false);
      reader.readAsDataURL(input.files[i]);
    }
  }
  public formValidation() {
    this.orgForm = this._fb.group({
      orgName: ['', Validators.compose([Validators.maxLength(50), Validators.required])],
    });

  }
  public createGround() {
    this.router.navigateByUrl('dashboard/ground/ground-profile/' + this.groundId)
  }
  public fileChangeListener($event: any, args: any, picArgs: any) {
    this.picArgs = picArgs;
    if (this.picArgs == "profile") {
      this.circleImg = true;
      this.squareImg = false;
    } else {
      this.circleImg = false;
      this.squareImg = true;
    }
    var image: any = new Image();
    var file: File = $event.target.files[0];
    var myReader: FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
      if (that.picArgs == "profile") {
      that.cropPic = image.src.split(",")[1];
      that.file_src_profile = image.src
    } else {
      that.cropPic = image.src.split(",")[1];
      that.file_src_cover = image.src
    }

    };
    myReader.readAsDataURL(file);
    this.showDialog();
  }

  public showDialog() {
    this.display = true;
    this.backdrop = true;
  }
 
  
  public uploadCropImage() {

    if (this.picArgs == "profile") {
      this.profile_pic = this.cropPic;
      this.uploadImage("profile", 0);
      this.display = false;
      this.backdrop = false;
    } else if (this.picArgs == "cover") {
      this.cover_pic = this.cropPic;
      this.uploadImage("cover", 0);
      this.display = false;
      this.backdrop = false;

    } else if (this.picArgs == "organization") {
      this.organization_image = this.cropPic;
      this.uploadImage("organization", 0);
    }


  }
  public groundPicChange($event: any, args: any, picArgs: any, index: number) {
    this.groundImageIndex = index;
    this.groundInput = args;
    this.ground_image = $event.target.files[0];
    console.log("testinfo", index)
    this.uploadImage("ground", index);

  }
  public sponserPicChange($event: any, args: any, picArgs: any, input: any) {
    if (this.sponser_list[args].sponser_name === undefined) {
      this.sponser_list[args].sponserNameValid = true;
    } else {
      this.sponser_list[args].sponser_image = $event.target.files[0];
      this.fileChange(input, 'sponser', args);
      this.uploadImage("sponser", args);
      this.sponser_list[args].sponserNameValid = false;
    }

  }
  public organizationPicChange($event: any, args: any, picArgs: any) {
    if (this.organization_name == "") {
      this.orgNameStatus = true;
    } else {
      this.orgNameStatus = false;
      this.organization_image = $event.target.files[0];
      this.fileChange(args, 'organization', 0)
      this.uploadImage("organization", 0);
    }
  }

  public rectangleCropImage() {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 1200;
    this.cropperSettings.height = 250;
    this.cropperSettings.keepAspect = false;

    this.cropperSettings.croppedWidth = 1200;
    this.cropperSettings.croppedHeight = 250;

    this.cropperSettings.canvasWidth = 350;
    this.cropperSettings.canvasHeight = 300;

    this.cropperSettings.minWidth = 100;
    this.cropperSettings.minHeight = 100;

    this.cropperSettings.rounded = false;
    this.cropperSettings.minWithRelativeToResolution = false;

    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings.noFileInput = true;

    this.data = {};
  }
  public circleCropImage() {
    this.cropperSettings2 = new CropperSettings();
    this.cropperSettings2.width = 200;
    this.cropperSettings2.height = 200;
    this.cropperSettings2.keepAspect = false;

    this.cropperSettings2.croppedWidth = 200;
    this.cropperSettings2.croppedHeight = 200;

    this.cropperSettings2.canvasWidth = 350;
    this.cropperSettings2.canvasHeight = 300;

    this.cropperSettings2.minWidth = 100;
    this.cropperSettings2.minHeight = 100;

    this.cropperSettings2.rounded = true;
    this.cropperSettings2.minWithRelativeToResolution = true;

    this.cropperSettings2.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings2.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings2.noFileInput = true;

    this.data2 = {};
  }
  public cancelButton() {
    this.router.navigateByUrl('dashboard/ground');
  }
  public orgNameValidation(value: any) {
    if (value) {
      this.orgNameStatus = false;
    } else {
      this.orgNameStatus = true;
    }

  }
}


