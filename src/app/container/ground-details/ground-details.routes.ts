import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GroundDetailsComponent } from './ground-details.component'
import { GroundProfileModule } from './ground-profile/ground-profile.module';
import { FirstGroundFormComponent } from './first-ground-form/first-ground-form.component';
import { SecondGroundFormComponent } from './second-ground-form/second-ground-form.component';
import { NewFirstGroundFormComponent } from './new-first-ground-form/new-first-ground-form.component';
import { NewSecondGroundComponent } from './new-second-ground/new-second-ground.component';
// import { GroundPriceComponent } from './ground-price/ground-price.component';
// import {AddCardRedirectComponent} from './add-card-redirect/add-card-redirect.component';

export const groundRoutes: Routes = [
    { path: '', component: GroundDetailsComponent },
    { path: 'ground-profile/:id', loadChildren: './ground-profile/ground-profile.module#GroundProfileModule' },
    { path: 'create-ground', component: NewFirstGroundFormComponent },
    { path: 'create-ground/:id', component: NewSecondGroundComponent },
    // { path: 'ground-payment/:id', component: GroundPriceComponent },
    // { path: 'payment', component: AddCardRedirectComponent }

];

export const groundRouting: ModuleWithProviders = RouterModule.forChild(groundRoutes);