import { Component, OnInit, HostListener, Inject } from "@angular/core";
import { Message, ConfirmationService } from "primeng/primeng";
import { Router } from "@angular/router";
import { GlobalService } from "./../../GlobalService";
import { DOCUMENT } from "@angular/platform-browser";
import { TranslateService } from "ng2-translate";

declare const $;
@Component({
  selector: "app-ground-details",
  templateUrl: "./ground-details.component.html",
  styleUrls: ["./ground-details.component.css"]
})
export class GroundDetailsComponent implements OnInit {
  payment: any;

  show: boolean = false;
  len: number = 0;
  showBtn: boolean = false;
  cards: boolean = false;
  disable1: boolean = false;
  msgs: Message[] = [];
  myGround: boolean = false;
  basePath: string = this.base_path_service.image_url;
  loader: boolean = false;
  isdata: boolean = false;
  isError: boolean = false;
  cityPlaceholder: string = "Ciudad";
  page: number = 1;
  type: Array<any> = [];
  modality: Array<any> = [];
  cost: Array<number> = [0, 300];
  cities: Array<any> = [];
  selectedCity: any = null;
  nameSelected: string = "";
  random: number = 0;
  rating: Array<any> = [];
  groundData: Array<any> = [];
  scrolling_array: Array<any> = [0];
  from_filter: string = "";
  backdrop: boolean = false;
  comdata: any;
  comname: any;

  constructor(
    private confirmationService: ConfirmationService,
    private translate: TranslateService,
    @Inject(DOCUMENT) public document: Document,
    public base_path_service: GlobalService,
    public router: Router
  ) {
    this.comdata = JSON.parse(localStorage.getItem("community_info"));
    this.comname = this.comdata.community_name;

    localStorage.removeItem("court_id");
    localStorage.removeItem("courts");
    localStorage.removeItem("court_information");
    localStorage.removeItem("field_info");
    this.cities.push({ label: "Ciudad", value: "" });
    this.isdata = false;
    if (localStorage.getItem("language") != null) {
      translate.use(localStorage.getItem("language"));
    }
  }
  ngOnInit() {
    this.trackGroundstate();
    this.mob();
  }
  @HostListener("window:scroll", ["$event"])
  track(event: any) {
    let number = this.document.body.scrollHeight;
    let totalNumber = window.pageYOffset + window.innerHeight;
    if (number == Math.ceil(totalNumber)) {
      this.from_filter = "";
      this.page = this.page + 1;
      if (this.myGround) {
        if (this.page <= this.len) {
          this.myGrounds(this.page);
        }
      } else {
        if (this.page <= this.len) {
          this.getGrounds(this.page);
        }
      }
    }
  }

  mob() {
    let mq = window.matchMedia("screen and (min-width:640px)");
    if (mq.matches) {
      this.show = false;
    } else {
      this.show = true;
    }
  }
  trackGroundstate() {
    // if (localStorage.getItem("city"))
    //   this.selectedCity = localStorage.getItem("city");
    // if (localStorage.getItem("myGround")) {
    //   let temp = JSON.parse(localStorage.getItem("myGround"));
    //   this.myGround = true;
    //   this.len = temp.total_pages;
    //   temp = undefined;
    //   this.myGrounds(this.page);
    // }
    // else if (localStorage.getItem("ground_state")) {
    //   if (localStorage.getItem("city"))
    //     this.selectedCity = localStorage.getItem("city");
    //   let data = JSON.parse(localStorage.getItem("ground_state"));

    //   this.nameSelected = data.data.ground_name;
    //   this.type = data.data.type_of_field;
    //   this.cost[0] = data.data.min_cost;
    //   this.cost[1] = data.data.max_cost;
    //   this.modality = data.data.modalities;
    //   this.len = data.total_pages;
    //   data = undefined;
    //   this.getGrounds(this.page);
    // }
    // else
    this.getGrounds(1);
  }

  paginate(event): any {
    let page = event.page;
    this.page = page + 1;
    if (this.myGround) this.myGrounds(this.page);
    else this.getGrounds(this.page);
  }

  getGrounds(page) {
    this.page = page;
    this.msgs = [];
    this.isdata = false;
    this.isError = false;
    this.loader = true;

    let url =
      this.base_path_service.base_path + "api/ground/filterGround/?format=json";
    let data = {
      rating: this.rating,
      ground_name: this.nameSelected,
      city: this.selectedCity,
      type_of_field: this.type,
      modalities: this.modality,
      page: this.page,
      rand: this.random,
      latitude: localStorage.getItem("lat"),
      longitude: localStorage.getItem("long"),
      distance: parseInt(localStorage.getItem("distance"))
    };
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        this.mob();
        this.cards = true;
        this.payment = res[0].json;
        if (res[0].json.data.length) {
          if (this.from_filter == "filter") {
            this.groundData = [];
          }
          this.groundData.push(res[0].json.data);
        }
        if (this.groundData.length < 1) {
          this.isdata = true;
        }

        this.len = res[0].json.total_pages;
        this.random = res[0].json.rand;
        this.loader = false;
      },
      err => {
        this.isError = true;
        this.loader = false;
      }
    );
  }

  filter() {
    if (!this.myGround) {
      this.groundData = [];
      this.getGrounds(1);
      this.from_filter = "filter";
    }
  }
  fun() {
    this.showBtn = true;
    this.cards = false;
  }
  cross() {
    this.showBtn = false;
  }
  apply() {
    this.showBtn = false;
    this.cards = true;
    this.show = true;
  }
  clear() {
    this.showBtn = false;
  }

  myGrounds(x: number) {
    if (this.myGround) {
      this.disable1 = true;
      let url =
        this.base_path_service.base_path +
        "api/ground/createGround/?form_type=my_ground&page=" +
        x +
        "&format=json";
      this.base_path_service.GetRequest(url).subscribe(
        res => {
          this.mob();
          this.cards = true;
          this.groundData.push(res[0].json.data);
          this.len = res[0].json.total_pages;
          if (res[0].json.data.length < 1) this.isdata = true;
        },
        err => {}
      );
    } else {
      this.getGrounds(1);
      this.disable1 = false;
    }
  }

  searchCity(event) {
    console.log("hello", event);
    this.cities = [];
    let query = event.query;
    if (query) {
      let url =
        this.base_path_service.base_path +
        "api/team/location/?country=1&city=" +
        query +
        "&format=json";
      this.base_path_service.GetRequest(url).subscribe(res => {
        let x = res[0].json;
        this.loader = false;
        this.cities = res[0].json;
      });
    } else {
      this.groundData = [];
      this.selectedCity = "";
      localStorage.removeItem("city");
      this.getGrounds(1);
    }
  }

  cityFilter(event) {
    this.groundData = [];
    this.selectedCity = event.location;
    this.getGrounds(1);
  }

  follow(row) {
    if (row.follow) {
      let url =
        this.base_path_service.base_path_api() +
        "ground/follow/" +
        row.id +
        "/";
      this.base_path_service.DeleteRequest(url).subscribe(
        res => {
          row.follow = false;
          this.msgs.push({ severity: "info", detail: "Seguir" });
        },
        err => {
          this.loader = false;
          this.msgs.push({ severity: "error", detail: "Error" });
        }
      );
    } else {
      let url = this.base_path_service.base_path + "api/user/profile/";
      let followInfo = {
        form_type: "follow_ground",
        follow_ground: row.id
      };
      this.base_path_service.PostRequest(url, followInfo).subscribe(
        res => {
          row.follow = true;
          this.msgs.push({ severity: "info", detail: "Dejar de seguir" });
        },
        err => {
          this.loader = false;
          this.base_path_service.print(err);
          this.msgs.push({ severity: "error", detail: "Error" });
        }
      );
    }
  }

  confirm(check) {
    if (check === "yes") {
      let demo = {
        is_demo: true
      };
      localStorage.setItem("is_demo", JSON.stringify(demo));
      this.router.navigateByUrl(this.comname + "/ground/create-ground");
    } else {
      let demo = {
        is_demo: false
      };
      localStorage.setItem("is_demo", JSON.stringify(demo));
      this.router.navigateByUrl(this.comname + "/ground/create-ground");
    }
  }

  reserve(court_id, ground_id, payment) {
    let court_information = {
      discount: 0,
      minimum_fee: 0,
      vat: 0,
      payment_method: payment
    };
    localStorage.setItem(
      "court_information",
      JSON.stringify(court_information)
    );

    localStorage.setItem("court_id", court_id);
    this.router.navigateByUrl(
      this.comname + "/ground/ground-profile/" + ground_id + "/schedule"
    );
  }
}
