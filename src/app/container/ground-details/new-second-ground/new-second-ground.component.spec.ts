import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSecondGroundComponent } from './new-second-ground.component';

describe('NewSecondGroundComponent', () => {
  let component: NewSecondGroundComponent;
  let fixture: ComponentFixture<NewSecondGroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewSecondGroundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSecondGroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
