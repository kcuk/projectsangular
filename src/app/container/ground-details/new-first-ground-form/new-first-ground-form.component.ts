import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../GlobalService';
import { SelectItem } from 'primeng/primeng';
import { TranslateService } from "ng2-translate";
import { countryCode } from "../../registration/onboading2/country_code"
declare var google;


@Component({
  selector: 'app-new-first-ground-form',
  templateUrl: './new-first-ground-form.component.html',
  styleUrls: ['./new-first-ground-form.component.css']
})
export class NewFirstGroundFormComponent implements OnInit {
  autocompleteItems: any[];
  GoogleAutocomplete: any;
  checkCity: boolean=true;
  country_name: any;
  city_name: any;
  long: any;
  lat: any;
  isMobValid: boolean;
  public groundCreateForm: FormGroup;
  public formArray: any
  public cityValidation: boolean = false;
  public filteredCity: Array<any> = [];
  public loader: boolean = false;
  public city_list: Array<any> = [];
  public filteredCountry: Array<any> = [];
  public country_list: Array<any> = [];
  public time: any;
  public costPerGame: number = 0;
  public numberOfCancha: number = 1;
  public citySelectValidation: boolean = false;
  public country_code: any;
  public country_valid: boolean = true;
  lang = localStorage.getItem('language')
  public ground_levels: any[] = [
    { label: 'Modality', value: null },
    { label: '5V5', value: 'V5' },
    { label: '6V6', value: 'V6' },
    { label: '7V7', value: 'V7' },
    { label: '8V8', value: 'V8' },
    { label: '9V9', value: 'V9' },
    { label: '11V11', value: 'V11' },
  ];
  public groundType: any[] = [
    { label: 'Type', value: null },
    { label: this.lang == 'en' ? "Futsal" : 'Salón', value: 'Salón' },
    { label: this.lang == 'en' ? "Turf" : 'Césped sintético', value: 'Césped sintético' },
    { label: this.lang == 'en' ? "Natural grass" : 'Césped natural', value: 'Césped natural' }
  ];
  public paymentMethodLabels: SelectItem[];
  public payment: Array<any> = [];
  public cost_per_game_popup: boolean = false;
  public msgs: Array<any> = [];
  public daysInfo: Array<any> = [];
  public priceListOfGround: Array<any> = [];
  public priceOfGround: any;
  public list: Array<any> = [];
  public connectGroundList: Array<any> = [];
  public connectGround: Array<any> = [{}];
  public dayPrice: Array<any> = [];
  public slot: Array<any> = [];
  public file: File;
  public file_name: string = '';
  public costOfGround: boolean = false;
  public costOfGroundValidation: any[0] = [false];
  public paymentMethodValidation: boolean = false;
  payment_show_msg: boolean = false
  public defaultGround: any;
  public tempStatus: Array<any> = [];
  public defaultGroundstatus: Array<any> = [];
  public imageBasePath: string = '';
  public connect_cancha_popup: boolean = false;
  items: FormArray;
  remove_items: boolean = true;
  remove: any[] = [];
  is_demo: boolean = false;
  court_index: any;
  stage: boolean = true;
  ground_day_cost: any[] = [];
  check_primary: any[];
  check_primary_on_submit: boolean = true;
  country_id: any;
  userObj: User;
  count: number = 0;
  codes: SelectItem[] = [];
  mask: any = '999999999999999'
  constructor(
    public zone: NgZone,
    private route: ActivatedRoute, private translate: TranslateService, public _fb: FormBuilder, public base_path_service: GlobalService, public router: Router) {
    this.userObj = new User();

    if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
    }
    if (localStorage.getItem('is_demo')) {
      this.is_demo = JSON.parse(localStorage.getItem('is_demo')).is_demo;
    }
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();

    countryCode.map((val) => {
      this.codes.push({
        label: val.name + " " + "(" + val.dial_code + ")",
        value: val.dial_code
      })
    })

  }
  ngOnInit() {
    this.imageBasePath = this.base_path_service.image_url;
    this.buildForm();
    // this.cityList();
    // this.countryList();
    this.paymentMethods();
    this.daysList();
  }
  // check(form) {
  //   console.log("formmmmmmmmmmmmmmmmmmmm", form)
  //   console.log("valueeeeeeeeeeeeeeeee", form.value)
  //   console.log("length", form.value.length)
  // }
  // public cityList() {
  //   this.loader = true;
  //   let url = this.base_path_service.base_path_api() + 'team/location/?form_type=city';
  //   this.base_path_service.GetRequest(url)
  //     .subscribe(res => {
  //       this.loader = false;
  //       this.city_list = [];
  //       this.city_list.push({ label: 'Seleccionar City', value: null });
  //       let city = res[0].json;
  //       for (var index = 0; index < res[0].json.length; index++) {
  //         this.city_list.push({ label: city[index].location, value: city[index].id });
  //       }
  //     }, err => {
  //     })
  // }
  public daysList() {
    this.loader = true;
    var url = this.base_path_service.base_path_api() + "ground/days" + "/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.daysInfo = res[0].json;
        this.slot = res[0].json[0].price;
      }, err => {
        this.loader = false;
      });
  }
  // public filterCity(event) {
  //   let query = event.query;
  //   if (this.country_id != undefined) {
  //     var url = this.base_path_service.base_path_api() + "team/location/?country=" + this.country_id + "&city=" + query + "&format=json";
  //     this.base_path_service.GetRequest(url)
  //       .subscribe(
  //         res => {
  //           this.filteredCity = res[0].json;

  //           if (res[0].json.length == 0) {
  //             this.cityValidation = true;
  //             this.citySelectValidation = false;
  //           } else {
  //             this.cityValidation = false;
  //             this.citySelectValidation = false;
  //           }
  //         },
  //         err => {
  //         });
  //   }
  //   else {

  //   }
  // }

  // public countryList() {
  //   this.loader = true;
  //   let url = this.base_path_service.base_path_api() + 'team/country/';
  //   this.base_path_service.GetRequest(url)
  //     .subscribe(res => {
  //       this.loader = false;
  //       this.country_list = [];
  //       this.country_list.push({ label: this.lang == 'en' ? "Select country" : 'Seleccionar País', value: null });
  //       let country = res[0].json;
  //       for (var index = 0; index < res[0].json.length; index++) {
  //         this.country_list.push({ label: this.lang == 'en' ? res[0].json[index].country_name : res[0].json[index].country_spanish, value: country[index].id });
  //       }
  //     }, err => {
  //     })
  // }

  public paymentMethods() {
    let url = this.base_path_service.base_path_api() + "peloteando/payment_mode/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        let paymentInfo = res[0].json;
        this.paymentMethodLabels = [];
        for (var i = 0; i < res[0].json.length; i++) {
          this.paymentMethodLabels.push({ label: paymentInfo[i].option, value: paymentInfo[i].id });
        }
      }, err => {
        this.base_path_service.print(err);
      })
  }

  // hi(event, i) {
  //   console.log("event=>>>>>>>>>>>>>>>>>>>", event, "value=>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", i)
  // }
  public buildForm() {
    this.lat = localStorage.getItem('lat');
    this.long = localStorage.getItem('long');
    let city = {
      description: localStorage.getItem('city')
    }
    this.country_name = localStorage.getItem('country_name')
    this.city_name = localStorage.getItem('city');
    this.groundCreateForm = this._fb.group({
      groundName: ['', Validators.compose([Validators.maxLength(50), Validators.required])],
      telephone: ['', Validators.compose([Validators.required])],
      mobileNo: ['', Validators.compose([Validators.required])],
      direction: ['', Validators.compose([Validators.required])],
      // city: ['', Validators.compose([Validators.required])],
      // country: ['', Validators.compose([Validators.required])],
      vat: [0,],
      paymentMethod: [],
      bio: ['', Validators.compose([Validators.required])],
      items: this._fb.array([this.createItems()]),
      country_code: localStorage.getItem('country_dial_code'),
      selectCity: [city]


    })

  }

  createItems(): FormGroup {
    return this._fb.group({
      court_name: ['', Validators.compose([Validators.maxLength(50), Validators.required])],
      game_mode: [[], Validators.compose([Validators.required])],
      type_grass: [[], Validators.compose([Validators.required])],
      minimum_fee: 0,
      discount: 0,
      is_primary: "false",
    });
  }

  addItem(): void {
    this.count++;
    this.items = this.groundCreateForm.get('items') as FormArray;
    let len = this.items.length;
    // this.remove[len - 1] = true;
    // this.remove[len] = false
    this.items.push(this.createItems());
  }

  check_primary_box(ind) {
    this.items = this.groundCreateForm.get('items') as FormArray;
    console.log("item=>>>>>>>>>>>>>>", this.items)
    for (let i = 0; i < this.items.length; i++) {
      if (i === ind) {
        this.items.controls[i].value.is_primary = "true";
        console.log("matchhhhhhh=>>>>>>>>>>>>>>", this.items)
      }
      else {
        this.items.controls[i].value.is_primary = "false";
        console.log("noyt matchhhhhhh=>>>>>>>>>>>>>>", this.items)


      }
    }
  }

  removeItem(i) {
    this.count--;
    this.items.removeAt(i);
    // let len = this.items.length;
    // this.remove[len - 1] = false
  }


  get item(): FormGroup {
    return this.groundCreateForm.get('items') as FormGroup;
  }


  public submitGroundForm(form_Data) {
    this.items = this.groundCreateForm.get('items') as FormArray;
    for (let court_cost = 0; court_cost < this.ground_day_cost.length; court_cost++) {
      this.items.value[court_cost].days = this.ground_day_cost[court_cost];
    }
    for (let court_cost = 0; court_cost < this.ground_day_cost.length; court_cost++) {
      if (this.items.value[court_cost].is_primary === 'true') {
        this.check_primary_on_submit = true;
        break;
      }
      else {
        this.check_primary_on_submit = false;
      }
    }
    console.log("file=>>>>>>>>>>>>>>>>>>>>>>", this.file)
    console.log("form", form_Data)
    // this.items = this.groundCreateForm.get('items') as FormArray;
    // this.items.value[this.court_index].days = this.list
    let data = {
      "ground_name": form_Data.groundName,
      "country_code": form_Data.country_code,
      "contact": parseInt(form_Data.mobileNo),
      "other_contact": form_Data.telephone ? parseInt(form_Data.telephone) : '',
      // "city": form_Data.city.id,
      "payment_method": form_Data.paymentMethod,
      "address": form_Data.direction,
      "bio": form_Data.bio,
      "vat": form_Data.vat,
      "rule": (this.file),
      "is_dummy": this.is_demo,
      "court_data": this.items.value,

    }

    if (this.check_primary_on_submit) {

      if (this.groundCreateForm.valid && this.checkCity) {
        console.log("trrrrrrrrrrrrrrrrrrrr")
        if (this.costOfGround && this.paymentMethodValidation) {
          console.log("trrrrrrrrrrrrrrrrrrrr11111111111111111")

          let check_valid: boolean = true
          for (let len = 0; len < form_Data.items.length; len++) {
            if (this.costOfGroundValidation[len] == undefined) {
              check_valid = false;
              this.costOfGroundValidation[len] = true
            }
            else {
              this.costOfGroundValidation[len] = false;
            }
          }
          if (check_valid) {
            console.log("trrrrrrrrrrrrrrrrrrrr check_valid")

            this.loader = true;
            let url = this.base_path_service.base_path_api() + 'ground/createGround/' + '/?format=json';
            return new Promise((resolve, reject) => {
              var append_data: any = new FormData();
              var xhr = new XMLHttpRequest();
              append_data.append("ground_name", form_Data.groundName)
              append_data.append("country_code", form_Data.country_code);
              append_data.append("contact", parseInt(form_Data.mobileNo));
              append_data.append("other_contact", form_Data.telephone ? parseInt(form_Data.telephone) : '');
              append_data.append("payment_method", JSON.stringify(form_Data.paymentMethod));
              append_data.append("address", form_Data.direction);
              append_data.append("bio", form_Data.bio);
              append_data.append("vat", form_Data.vat);
              append_data.append("rule", (this.file));
              append_data.append("is_dummy", this.is_demo);
              append_data.append("court_data", JSON.stringify(form_Data.items));
              append_data.append('country_name', this.country_name);
              append_data.append('city_name', this.city_name);
              append_data.append('latitude', this.lat);
              append_data.append('longitude', this.long);
              xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {
                  if (xhr.status == 200 || xhr.status == 201) {
                    this.loader = false;
                    console.log("response=>>>>>>>>>>>>>>", xhr.response, "idddddddddd", JSON.parse(xhr.response).ground_id);
                    let groundId = JSON.parse(xhr.response).ground_id
                    let courts = {
                      ground_info: JSON.parse(xhr.response)
                    }
                    localStorage.setItem('courts', JSON.stringify(courts))
                    this.router.navigateByUrl('dashboard/ground/create-ground/' + groundId);
                  }
                  else {
                    this.msgs = [];
                    this.msgs.push({ severity: 'error', detail: "Error" });
                    this.loader = false;
                  }
                }
              }
              xhr.open("POST", url, true);
              xhr.setRequestHeader("Authorization", 'Bearer ' + JSON.parse(localStorage.getItem('userInfo')).token.access_token);
              xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
              xhr.send(append_data);
            });
          }

          // this.base_path_service.PostRequest(url, data)
          //   .subscribe(res => {
          //     this.loader = false;
          //     let groundId = res[0].json.ground_id;
          //     let courts = {
          //       ground_info: res[0].json
          //     }
          //     localStorage.setItem('courts', JSON.stringify(courts))
          //     this.router.navigateByUrl('dashboard/ground/create-ground/' + groundId);
          //   }, err => {
          //     this.msgs = [];
          //     this.msgs.push({ severity: 'error', detail: "El terreno con la misma información existe" });
          //     this.loader = false;
          //   })

          else {

            if (!this.costOfGround) {
              this.costOfGroundValidation[0] = true;
            }
            this.msgs = [];
            this.msgs.push({ severity: 'error', detail: "Ingresa todos los campos obligatorios" });
          }
        }

        else {

          if (!this.costOfGround) {
            this.costOfGroundValidation[0] = true;
          }
          this.msgs = [];
          this.msgs.push({ severity: 'error', detail: "Ingresa todos los campos obligatorios" });
        }
      }
    }
    else {
      this.msgs = [];
      this.msgs.push({ severity: 'error', detail: "Please select  primary court" });
    }
  }
  public cancelButton() {
    this.router.navigateByUrl('dashboard/ground')
  }
  // public countryValid(value: any) {
  //   if (value) {
  //     this.country_id = value
  //     this.country_valid = false;
  //   } else {
  //     this.country_valid = true;
  //   }
  // }
  public setGroundPrice(dInfo, event) {
    dInfo.amount = event.target.value;
  }
  public saveCostOfGround() {
    this.priceOfGround = [];
    for (var index = 0; index < 7; index++) {
      for (var i = 0; i < 4; i++) {
        if (this.daysInfo[index].price[i].amount) {
          this.priceOfGround.push({ 'time_id': this.daysInfo[index].price[i].id, 'amount': this.daysInfo[index].price[i].amount });
        }
      }
      if (this.priceOfGround.length > 0) {
        this.list.push({ day_id: this.daysInfo[index].pk, price: this.priceOfGround })
      }
      this.priceOfGround = [];
    }

    if (this.list.length > 0) {
      this.costOfGround = true;
      this.costOfGroundValidation[this.court_index] = false;
    } else {
      this.costOfGround = false;
      this.costOfGroundValidation[this.court_index] = true;
    }
    this.ground_day_cost[this.court_index] = this.list;

  }
  public connectGroundInfo() {
    this.connectGround = [];
    let url = this.base_path_service.base_path_api() + 'ground/connectGround/?form_type=profile&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.connectGroundList = res[0].json;
        this.defaultGround = null;
      }, err => {

      })
  }
  public Ground() {
    for (let index = 0; index < this.connectGround.length; index++) {
      if (this.connectGround[index] == this.defaultGround) {
        this.defaultGroundstatus.push({ is_default: 'True', id: parseInt(this.connectGround[index]) })
      } else {
        this.defaultGroundstatus.push({ is_default: 'False', id: parseInt(this.connectGround[index]) })
      }
    }
  }
  public resetField(index) {
    this.list = [];
    this.daysInfo = [];
    this.daysList();
    this.court_index = index
  }
  fileChangeListener(event) {
    this.file = event.target.files[0];
    console.log("upload=>>>>>>>>>>>>>>>>>", this.file)
    this.file_name = event.target.files[0].name;
  }
  public validation(value: any) {
    if (value.length != 0) {
      this.payment_show_msg = false
      this.paymentMethodValidation = true;
    } else {
      this.paymentMethodValidation = false;
      this.payment_show_msg = true
    }
  }
  public checkStatus(info) {
    if (info.is_default) {
      info.is_default = false;
      this.defaultGround = null;
    } else {
      info.is_default = true;

    }
  }




  // validMobile(event) {
  //   console.log("event=>>>>>>>>>>>>>>>>>>>>>>>", event);
  //   this.groundCreateForm.patchValue({ 'mobileNo': "" })
  //   if (event.value == '+1') {
  //     this.length = 10;
  //     this.mask = '9999999999';
  //   }
  //   else if (event.value == '+502') {
  //     this.length = 8;
  //     this.mask = '99999999';
  //   }
  //   else {
  //     this.length = 9;
  //     this.mask = '999999999';
  //   }
  // }

  // mobileVerify(value) {
  //   if (value.toString().length != this.length || value < 0) {
  //     console.log("mobileeeeeeeeeeeeeeee")
  //     this.isMobValid = true;

  //   }
  //   else
  //     this.isMobValid = false;
  //   console.log("mobileeeeeeeeeeeeeeee =>>>>>>>>>>>>>>", this.isMobValid)

  // }


  // mobileVerify(event) {
  //   // console.log("value=>>>>>>>>>>>>>>>>>>>",event.target.value.toString().includes('_'))
  //   // this.userObj.mobile = value;
  //   if (event.target.value.toString().includes('_')) {
  //     console.log("mobileeeeeeeeeeeeeeee")
  //     this.isMobValid = true;

  //   }
  //   else
  //     this.isMobValid = false;
  //   console.log("mobileeeeeeeeeeeeeeee =>>>>>>>>>>>>>>", this.isMobValid)

  // }

  search(event) {
    this.checkCity = false;
    console.log("valussssssssss", event)
    this.GoogleAutocomplete.getPlacePredictions({ input: event.query, types: ['(regions)'] },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
          console.log("list", this.autocompleteItems)
        });
      });
  }

  setLoaction(item) {
    console.log("item=>>>>>>>>>>>>>>>>", item);
    this.checkCity = true;
    this.country_name = item.terms.pop().value;
    this.city_name = item.terms[0].value;
    this.geoCode((item.description));

  }

  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.lat = results[0].geometry.location.lat();
      this.long = results[0].geometry.location.lng()
    })
  }

}

class User {
  public password: string = "";
  public email: string = "";
  public name: string = "";
  public last_name: string = "";
  public mobile: string = "";
  public facebook_id = "";
  public country_code: string = "+593"

}

