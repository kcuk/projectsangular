import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFirstGroundFormComponent } from './new-first-ground-form.component';

describe('NewFirstGroundFormComponent', () => {
  let component: NewFirstGroundFormComponent;
  let fixture: ComponentFixture<NewFirstGroundFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFirstGroundFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFirstGroundFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
