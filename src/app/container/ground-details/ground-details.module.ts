import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { groundRouting } from './ground-details.routes';
import { GroundDetailsComponent } from './ground-details.component'
import { SharedModule } from './../../shared/shared.module';
import { FirstGroundFormComponent } from './first-ground-form/first-ground-form.component';
import { SecondGroundFormComponent } from './second-ground-form/second-ground-form.component';
import { NewFirstGroundFormComponent } from './new-first-ground-form/new-first-ground-form.component';
import { NewSecondGroundComponent } from './new-second-ground/new-second-ground.component';
// import { GroundPriceComponent } from './ground-price/ground-price.component';

@NgModule({
  imports: [
    CommonModule,
    groundRouting,
    SharedModule.forRoot()
  ],
  declarations: [
    GroundDetailsComponent,
    FirstGroundFormComponent,
    SecondGroundFormComponent,
    NewSecondGroundComponent,
    NewFirstGroundFormComponent,
    NewSecondGroundComponent,
    // GroundPriceComponent,
  ]
})
export class GroundDetailsModule { }
