import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from 'ng2-translate';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { GlobalService } from '../../../../GlobalService';

@Component({
  selector: 'app-edit-court',
  templateUrl: './edit-court.component.html',
  styleUrls: ['./edit-court.component.css']
})
export class EditCourtComponent implements OnInit {
  dummy: any;
  imageUrl: string;
  court_list: any;
  loader: boolean;
  daysInfo: any;
  slot: any;
  day_info: any;
  msgs: any[] = []
  ground_id: string;
  results: any;
  form: FormGroup;
  items: FormArray;
  index: any;
  court_popup: boolean = false;
  primary: any;
  primary_id: any;
  check_new_court: any[] = [];
  ground_day_cost: any[] = [];
  deleteTournament: boolean = false;
  court_remove_index: any;
  court_remove_data: any;
  is_paid: boolean = false;
  court_length: any = 0;
  public ground_levels: any[] = [
    { label: localStorage.getItem('language') == 'en' ? "Select modality" : 'Seleccionar modality', value: null },
    { label: '5V5', value: 'V5' },
    { label: '6V6', value: 'V6' },
    { label: '7V7', value: 'V7' },
    { label: '8V8', value: 'V8' },
    { label: '9V9', value: 'V9' },
    { label: '11V11', value: 'V11' },
  ];
  public groundType: any[] = [
    { label: localStorage.getItem('language') == 'en' ? "Select type" : 'Seleccionar type', value: null },
    { label: localStorage.getItem('language') == 'en' ? "Fustal" : 'Salón', value: 'Salón' },
    { label: localStorage.getItem('language') == 'en' ? "" : 'Turf', value: 'Césped sintético' },
    { label: localStorage.getItem('language') == 'en' ? "" : 'Natural grass', value: 'Césped natural' }
  ];
  cost_per_game_popup: boolean = false;
  constructor(private route?: ActivatedRoute, private translate?: TranslateService, public _fb?: FormBuilder, public base_path_service?: GlobalService, public router?: Router) {
  }
  ngOnInit() {
    this.route.queryParams.subscribe(res => {
      this.dummy = res['certified'];
    })
    this.form = this._fb.group({
      items: this._fb.array([])
    })
    this.route.params.subscribe(res => {
      this.ground_id = res['id'];
    })
    this.getCourtList();
  }

  getCourtList(): any {
    this.items = this.form.get('items') as FormArray;
    let url = this.base_path_service.base_path_api() + "ground/courtData/?ground_id=" + this.ground_id;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.results = res[0].json.list;
        this.court_length = this.results.length;
        this.is_paid = res[0].json.is_paid
        for (let i = 0; i < this.results.length; i++) {
          this.addItem(this.results[i], i);
        }
      })
  }

  addItem(data?, index?) {
    this.items = this.form.get('items') as FormArray;
    if (data === true) {
      let ind = this.items.length
      console.log("length=>>>>>>>>>>>>>>>>", ind)
      this.check_new_court[ind] = data
      this.items.push(this.createItems());
    }
    else {
      this.check_new_court[index] = false;
      this.items.push(this.createItems(data));
    }
  }

  createItems(data?): FormGroup {
    return this._fb.group({
      ground_name: [data ? data.court_name : '', Validators.required],
      field_format: [data ? data.field_format : '', Validators.compose([Validators.required])],
      type_of_field: [data ? data.type_of_field : '', Validators.compose([Validators.required])],
      minimum_fee: data ? data.minimum_fee : 0,
      discount: data ? data.discount : 0,
      is_default: data ? data.is_primary : false,
    });
  }

  get item(): FormGroup {
    return this.form.get('items') as FormGroup;
  }

  removeItem(data, ind) {
    this.court_remove_index = ind;
    this.deleteTournament = true;
    this.court_remove_data = data;

  }
  deleteCourt() {
    this.loader = true
    if (this.court_remove_data === undefined) {
      this.items.removeAt(this.court_remove_index)
      this.loader = false;
      // this.ngOnInit();
    }
    else {
      this.items = this.form.get('items') as FormArray;
      let url = this.base_path_service.base_path_api() + 'ground/courtEdit/' + this.court_remove_data.court_id + '/';
      this.base_path_service.DeleteRequest(url)
        .subscribe(res => {
          this.msgs = [];
          this.msgs.push({ 'severity': 'success', detail: 'Court deleted' })
          this.loader = false;
          this.ngOnInit();
        }, err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ 'severity': 'Error', detail: 'Court not deleted' })
        })
    }
  }

  showCourtCost(index) {
    this.index = index
    if (this.check_new_court[index] === true) {
      var url = this.base_path_service.base_path_api() + "ground/days" + "/?format=json";
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.cost_per_game_popup = true;
          this.daysInfo = res[0].json;
          this.slot = res[0].json[0].price;
        }, err => {

        });
    }
    else {
      console.log("days cost showwwwwwww+>>>>>>>>>>>>>>>>>>", this.results[this.index].days)
      this.daysInfo = this.results[this.index].days
      this.slot = this.daysInfo[0].price
      this.cost_per_game_popup = true;
    }
  }

  save(ind, data) {

    if (this.check_new_court[ind] === true && this.results[ind] === undefined && data.valid) {
      this.loader = true;
      this.items = this.form.get('items') as FormArray;
      this.item.value[ind].days = this.daysInfo;
      let temp = this.item.value[ind]
      let data = {
        "ground_id": this.ground_id,
        "court_name": temp.ground_name,
        "game_mode": temp.field_format,
        "minimum_fee": temp.minimum_fee,
        "discount": temp.discount,
        "type_grass": temp.type_of_field,
        "days": temp.days
      }
      let url = this.base_path_service.base_path_api() + 'ground/createCourt/'
      this.base_path_service.PostRequest(url, data)
        .subscribe(res => {
          this.check_new_court[ind] = false;
          this.msgs = [];
          this.msgs.push({ 'severity': 'success', detail: 'Court Added Succesfully' })
          this.loader = false;
          this.ngOnInit();
        }, err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ 'severity': 'error', detail: err.json().error })
        })
    }
    // else if (data.valid) {
    //   this.loader = true;
    //   this.items = this.form.get('items') as FormArray;
    //   this.item.value[ind].days = []
    //   let temp = this.item.value[ind]
    //   let url = this.base_path_service.base_path_api() + 'ground/courtEdit/' + this.results[ind].court_id + '/';
    //   this.base_path_service.PutRequest(url, temp)
    //     .subscribe(res => {
    //       this.msgs = [];
    //       this.msgs.push({ 'severity': 'success', detail: 'Edit Succesfully' })
    //       this.loader = false;
    //     }, err => {
    //       this.loader = false;
    //       this.msgs = [];
    //       this.msgs.push({ 'severity': 'Error', detail: 'Edit Error' })
    //     })
    // }
    else if (data.valid) {
      console.log("days cost+>>>>>>>>>>>>>>>>>>", this.results[ind].days)
      this.loader = true;
      this.items = this.form.get('items') as FormArray;
      this.item.value[ind].days = this.results[ind].days
      let temp = this.item.value[ind]
      let url = this.base_path_service.base_path_api() + 'ground/courtEdit/' + this.results[ind].court_id + '/';
      this.base_path_service.PutRequest(url, temp)
        .subscribe(res => {
          this.msgs = [];
          this.msgs.push({ 'severity': 'success', detail: 'Edit Succesfully' })
          this.loader = false;
        }, err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ 'severity': 'Error', detail: 'Edit Error' })
        })
    }
  }

  back() {
    this.router.navigateByUrl('dashboard/ground/ground-profile/' + this.ground_id + '/edit-ground?certified=' + this.dummy)
  }

  goProfile() {
    this.router.navigateByUrl('dashboard/ground/ground-profile/' + this.ground_id)
  }

  courtList() {
    this.loader = true;
    this.court_popup = true;
    let url = this.base_path_service.base_path_api() + "ground/courtList/?ground_id=" + this.ground_id;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.court_list = res[0].json;
        this.imageUrl = this.base_path_service.image_url;
        this.primary = true;
      }, err => {
        this.loader = false;
      })
  }

  submitPrimary() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "ground/courtList/"
    let data = {
      "ground_id": this.ground_id,
      "court_id": this.primary_id
    }
    localStorage.setItem("court_id", this.primary_id)
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.court_popup = false
        this.msgs = [];
        this.loader = false;
        this.msgs.push({ 'severity': 'Success', 'detail': 'Set primary successfully' })
      }, err => {
        this.loader = false;
        this.msgs.push({ 'severity': 'Eerror', 'detail': 'Set primary error' })
      })
  }

  setPrimary(id) {
    this.primary_id = this.court_list[id].id;
  }

  saveCourtCost() {
    for (var index = 0; index < 7; index++) {
      for (var i = 0; i < 4; i++) {
        if (this.daysInfo[index].price[i].amount === undefined) {
          this.daysInfo[index].price[i].amount = 0;
        }
      }
    }
  }

  continue() {
    this.router.navigateByUrl('dashboard/package/' + this.ground_id + "?status=ground");

  }
}
