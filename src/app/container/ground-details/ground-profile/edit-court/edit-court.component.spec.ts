import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCourtComponent } from './edit-court.component';

describe('EditCourtComponent', () => {
  let component: EditCourtComponent;
  let fixture: ComponentFixture<EditCourtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCourtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCourtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
