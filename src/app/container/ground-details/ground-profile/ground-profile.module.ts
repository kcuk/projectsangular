import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { groundProfileRouting } from './ground-profile.routes';
import { GroundProfileComponent } from './ground-profile.component';
import { SharedModule } from './../../../shared/shared.module';
import { GroundScheduleComponent } from './ground-schedule/ground-schedule.component';
import { GroundRequestComponent } from './ground-request/ground-request.component';
import { GroundInfoComponent } from './ground-info/ground-info.component';
import { EditGroundComponent } from './edit-ground/edit-ground.component';
import { EditSponserComponent } from './edit-sponser/edit-sponser.component';
import { ShareButtonsModule } from 'ngx-sharebuttons';
import { ReservationInfoComponent } from './reservation-info/reservation-info.component';
import { EditCourtComponent } from './edit-court/edit-court.component';
import { EarningComponent } from './earning/earning.component';
import { EarningTransactionComponent } from './earning-transaction/earning-transaction.component';

@NgModule({
  imports: [
    SharedModule.forRoot(),
    ShareButtonsModule.forRoot(),
    groundProfileRouting
  ],
  declarations: [EarningTransactionComponent,EarningComponent, GroundProfileComponent, GroundScheduleComponent, GroundRequestComponent, GroundInfoComponent, EditGroundComponent, EditSponserComponent, ReservationInfoComponent, EditCourtComponent, EarningComponent, EarningTransactionComponent]
})
export class GroundProfileModule { }
