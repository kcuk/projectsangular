import { GlobalService } from './../../../../GlobalService';
import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from "ng2-translate";
@Component({
  selector: 'app-reservation-info',
  templateUrl: './reservation-info.component.html',
  styleUrls: ['./reservation-info.component.css']
})
export class ReservationInfoComponent implements OnInit {
  public connectGroundDropdown: SelectItem[] = [];
  public monthOption: SelectItem[] = [{ label: 'enero', value: 1 }, { label: 'febrero', value: 2 }, { label: 'marzo', value: 3 }, { label: 'abril', value: 4 }, { label: 'mayo', value: 5 }, { label: 'junio', value: 6 }, { label: 'julio', value: 7 }, { label: 'agosto', value: 8 }, { label: 'septiembre', value: 9 }, { label: 'octubre', value: 10 }, { label: 'noviembre', value: 11 }, { label: 'diciembre', value: 12 }];
  public yearOption: SelectItem[] = [{ label: '2016', value: 2016 }, { label: '2017', value: 2017 },{ label: '2018', value: 2018 }];
  public connectGroundId: any;
  public groundId: number;
  public imageUrl: string = "";
  public date: string = "";
  public reserveInfoByUser: Array<any> = []
  public selectedMonth: any = 1;
  public selectedYear: string = "2018";
  public loader: boolean = false;
  public reserveInfoByTournament: Array<any> = [];
  public no_of_reservation:any;
  constructor(private translate: TranslateService,public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute) {
    this.imageUrl = this.base_path_service.image_url;
    let tempValue = this.route.params.subscribe(params => {
      this.groundId = params['id'];
    });
     if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
    }
  }

  ngOnInit() {

    this.connectGroundInfo();
  }
  public connectGroundInfo() {
    let url = this.base_path_service.base_path_api() + `ground/connectGround/?ground=${this.groundId}&form_type=connected&format=json`;
    this.connectGroundDropdown = [];
    var temp;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        temp = res[0].json;
        this.connectGroundId = { id: temp[0].id, img: temp[0].profile_pic };
        for (var index = 0; index < temp.length; index++) {
          this.connectGroundDropdown.push({ label: temp[index].ground_name, value: { id: temp[index].id, img: temp[index].profile_pic } });
        }
        this.reserveInfo();
      }, err => {

      })
  }
  public connectGroundChangeInfo() {

  }
  public reserveInfo() {
    this.loader = true;
    let temp = this.monthOption[this.selectedMonth - 1].value
    if (temp < 10) {
      this.date = this.selectedYear + "-" + "0" + temp + "-" + "01"
    } else {
      this.date = this.selectedYear + "-" + temp + "-" + "01"
    }

    let url = this.base_path_service.base_path_api() + "scorecard/groundReservations/?ground=" + this.connectGroundId.id + "&date=" + this.date;

    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.no_of_reservation=res[0].json;
        this.reserveInfoByUser = res[0].json.and_someone_total_reservation_detail;
        this.reserveInfoByTournament = res[0].json.total_tournament_reservations;
      }, err => {
        this.loader = false
      });
  }
  public back() {
    this.router.navigateByUrl("dashboard/ground/ground-profile/" + this.groundId + "/schedule")
  }
}
