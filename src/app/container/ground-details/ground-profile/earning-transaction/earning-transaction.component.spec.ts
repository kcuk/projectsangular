import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarningTransactionComponent } from './earning-transaction.component';

describe('EarningTransactionComponent', () => {
  let component: EarningTransactionComponent;
  let fixture: ComponentFixture<EarningTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarningTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarningTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
