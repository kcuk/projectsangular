import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../../GlobalService';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from "ng2-translate";
@Component({
  selector: 'app-edit-sponser',
  templateUrl: './edit-sponser.component.html',
  styleUrls: ['./edit-sponser.component.css']
})
export class EditSponserComponent implements OnInit {
  public sponserInfo: Array<any> = [{}]
  public groundId: number = 0;
  public loader: boolean = false;
  public message: Array<any> = [];
  public file_srcs: string[] = [];
  public file_srcs_sponser: string[] = [];
  public sponser_image: string = "";
  public sponser_name: string = "";
  public priority: string = "";
  public sponserId: number = 0;
  public sponserInfoList: Array<any> = [];
  public imageBasePath: string = "";
  public addSponserStatus: boolean = false;
  public data: any;
  public display: boolean = false;
  public backdrop: boolean = false;
  public sponserInput: any;
  public sponserPic: any;
  public index: any;
  public addObj: any;
  public cropImage: any;
  public file_src_sponser: string[] = [];
  public hoverTrue: boolean = false;
  public sponserImages: Array<any> = [];

  constructor(private translate: TranslateService, public base_path_service: GlobalService, public route: ActivatedRoute, public router: Router) {
    if (localStorage.getItem('language') != null) {
      console.log("chalalalallala")
      translate.use(localStorage.getItem('language'));
    }
    console.log("kishanknajkghysa")
  }
  ngOnInit() {
    this.sponserInfo = [];
    this.imageBasePath = this.base_path_service.image_url;
    let tempValue = this.route.params.subscribe(params => {
      this.groundId = params['id'];
    });
    this.sponserList();
  }

  public fileChange(input: any, args: any, index: number) {
    for (var i = 0; i < input.files.length; i++) {
      var img = document.createElement("img");
      img.src = window.URL.createObjectURL(input.files[i]);
      var reader: any, target: EventTarget;
      var reader: any = new FileReader();
      reader.addEventListener("load", (event) => {
        img.src = event.target.result;
        if (args == 'ground') {
          if (this.file_srcs.length < 5) {
            this.file_srcs.push(img.src);
          }
        } else {
          this.file_src_sponser[index] = img.src;
        }


      }, false);
      reader.readAsDataURL(input.files[i]);
    }
  }

  public updateSposerInfo(index, addObj) {
    this.loader = true;
    this.makeFileUploadRequest(index, addObj).then(
      (result) => {

      },
      (error) => {
        console.error(error);
      }
    );
  }

  public makeFileUploadRequest(index, addObj) {
    var url;
    var method;
    if (addObj.pk != null) {
      method = "PUT";
      url = this.base_path_service.base_path_api() + 'ground/groundImages/' + this.groundId + "/";
    } else {
      method = "POST";
      url = this.base_path_service.base_path_api() + 'ground/groundImages/';
    }
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      /*
       *-----------Append Whole Data of Ground Create Page--------------------
       */
      if (addObj.pk != null) {
        formData.append("form_type", "sponser");
        formData.append("sponser_name", this.sponser_name);
        formData.append("pk", this.sponserId);
        formData.append("sponser_image", this.sponserInfo[index].sponser_image);
        formData.append("priority", this.priority);
      } else {
        formData.append("form_type", "sponser");
        formData.append("ground_id", this.groundId);
        formData.append("sponser_name", this.sponserInfo[index].sponser_name);
        formData.append("sponser_image", this.sponserInfo[index].sponser_image);
        formData.append("priority", this.sponserInfo[index].priority);
      }



      xhr.onreadystatechange = () => {
        this.loader = false;
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else if (xhr.status == 201) {
            this.loader = false;
            this.message.push({ severity: 'info', detail: "Añadir con éxitoy" });
          } else if (xhr.status == 205) {
            this.sponserList();
            this.loader = false;
            this.message.push({ severity: 'info', detail: "Éxito" });
          } else if (xhr.status == 406) {
            this.loader = false;
            this.message.push({ severity: 'error', detail: "La prioridad debe ser único" });
          } else {
            this.loader = false;
            reject(xhr.response);
          }

        }
      }

      xhr.open(method, url, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + JSON.parse(localStorage.getItem('userInfo')).token.access_token);
      xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
      xhr.send(formData);
    });
  }
  /*
   *---------------- Add Sponser List---------------------
   */
  public addSponserField() {
    this.sponserInfo.push({ pk: null });
  }
  public deleteSponserField(index: number, id: number) {
    this.sponserInfo.splice(index, 1);
    if (id != null) {
      let url = this.base_path_service.base_path_api() + 'ground/sponser/' + id + '/?format=json';
      this.base_path_service.DeleteRequest(url)
        .subscribe(res => {
          this.message.push({ severity: 'info', detail: "Borrado exitosamente" });
        }, err => {
          this.base_path_service.print(err);
          this.message.push({ severity: 'error', detail: "Algo mal" });
        })
    }
  }
  public backToProfile() {
    this.router.navigateByUrl('dashboard/ground/ground-profile/' + this.groundId)
  }
  public sponserList() {
    let url = this.base_path_service.base_path_api() + "ground/sponser/" + this.groundId + "/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.sponserInfo = res[0].json.sponsor;
      }, err => {

      })
  }
  public sponserPicChange(event: any, index: any, input: any) {
    this.sponserImages[index] = event.target.files[0];
    this.fileChange(input, 'sponser', index);
  }

  public fileChangeListener(index: number, addObj, pic) {
    this.sponser_name = this.sponserInfo[index].sponser_name;
    this.priority = this.sponserInfo[index].priority;
    this.sponserId = addObj.pk;
    this.sponserPic = pic;
    this.index = index;
    this.addObj = addObj;
    this.cropImage = this.sponserImages[index];
    this.sponserInfo[index].valid = false;
    this.sponserInfo[index].sponser_image = this.cropImage;
    if (this.priority === undefined) {
      this.priority = this.sponserInfo[index].priority;
    }
    if (this.cropImage === undefined) {
      this.cropImage = this.sponserInfo[index].image;
      this.sponserInfo[index].sponser_image = this.sponserInfo[index].image;
    }
    if (this.sponserInfo[index].sponser_name === undefined || this.sponserInfo[index].sponser_name == "" || this.cropImage === undefined || this.priority == undefined) {
      alert("Todos los Informeshns Hey Mandetri")
    } else {
      this.updateSposerInfo(this.index, this.addObj);
    }
  }

  public showDialog() {
    this.display = true;
    this.backdrop = true;
  }

}

