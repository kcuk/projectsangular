import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';

@Component({
  selector: 'app-ground-request',
  templateUrl: './ground-request.component.html',
  styleUrls: ['./ground-request.component.css']
})
export class GroundRequestComponent implements OnInit {
  court_id: any;
  public requestInfo: Array<any> = [];
  public imageUrl: string = '';
  public tournamentMatchList: Array<any> = [];
  public statusLebel: any = '';
  public bookingStatusLabel: any = '';
  public tournamentBookingList: Array<any> = [];
  public loader: boolean = false;
  public msgs: Array<any> = [];
  public statusInfo: any;
  public groundId: any;
  public statusLevel: string = "";
  public admin: boolean = false;
  public noData: boolean = false;
  public status_lebels: any[] = [
    { label: localStorage.getItem('language') == 'en' ? 'Action' : 'ACCIÓN', value: '' },
    { label: localStorage.getItem('language') == 'en' ? 'Accept' : 'Aceptar', value: 'accepted' },
    { label:  localStorage.getItem('language') == 'en' ? 'Deny' :'Negar', value: 'deny' }
  ];
  public bookingReq: any[];
  obj: any;

  constructor(public base_path_service: GlobalService, public route: ActivatedRoute, public router: Router) {
    console.log("request=>>>>>>>>>>>")
    this.imageUrl = this.base_path_service.image_url;
    this.obj = this.base_path_service.groundProfile.subscribe(res => {
      if (res['court_id'] !== undefined) {
        this.court_id = res['court_id'];
        var tempUrl = window.location.href;
        var arrUrl = tempUrl.split('/');
        var temp = arrUrl[arrUrl.length - 1];
        this.court_id = res['court_id']
        if (temp === 'request') {
          this.groundRequestInfo();
        }
      }
    })

  }

  ngOnDestroy() {
    this.obj.unsubscribe();
  }

  ngOnInit() {
    let court;
    this.route.queryParams.subscribe(res => {
      court = res['court_id'];
    })
    if (court !== undefined) {
      this.court_id = court;
      localStorage.setItem('court_id',this.court_id)
      let id: any;
      this.route.params.subscribe(res => {
        id = res['id'];
      })
      this.groundRequestInfo();
    }
    else if (localStorage.getItem('court_id')) {
      this.court_id = localStorage.getItem('court_id')
      this.groundRequestInfo();
    }
    // this.base_path_service.groundProfile.next("request");
  }


  public groundRequestInfo() {
    this.loader = true;
    this.noData = false;
    let url = this.base_path_service.base_path_api() + "scorecard/ground_request/?ground=" + this.court_id + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.requestInfo = res[0].json.normal_match_list;
        this.tournamentMatchList = res[0].json.tournament_match_list;
        this.tournamentBookingList = res[0].json.simple_booking;
        this.admin = res[0].json.admin;
        if (this.requestInfo.length == 0 && this.tournamentMatchList.length == 0 && this.tournamentBookingList.length == 0) {
          this.noData = true;
        }
      }, err => {
        this.loader = false;
        this.msgs.push({ severity: 'error', detail: "Error" });
        this.base_path_service.print(err);
      })
  }
  public postStatus(id: number, labelValue: string) {
    this.statusLevel = "";
    if (labelValue == "accepted") {
      this.statusInfoObj(id, labelValue);
    } else {
      this.statusInfoObj(id, labelValue);
    }
    if (labelValue != "") {
      this.loader = true;
      let url = this.base_path_service.base_path_api() + "scorecard/ground_request/"
      this.base_path_service.PostRequest(url, this.statusInfo)
        .subscribe(res => {
          this.loader = false;
          this.msgs.push({ severity: 'info', detail: "Exitosamente" });
          this.groundRequestInfo();
        }, err => {
          this.loader = false;
          this.base_path_service.print(err);
          this.msgs.push({ severity: 'error', detail: "Error" });
        })
    }
  }
  public statusInfoObj(id: number, labelValue: string) {
    this.statusInfo = {
      "status": labelValue,
      "request": id,
      "form_type": 'status'
    }
  }
  public postReqStatus(id: number, labelValue) {
    this.postStatus(id, labelValue);
    this.bookingStatusLabel = ''
  }
  public redirectTeamProfile(id) {
    this.router.navigateByUrl('dashboard/team-profile/' + id)
  }
  public redirectPlayerProfile(id) {
    this.router.navigateByUrl('dashboard/player/player-profile/' + id)
  }
  public redirectTournamentProfile(id) {
    this.router.navigateByUrl('dashboard/tournament-profile/' + id)
  }
}

