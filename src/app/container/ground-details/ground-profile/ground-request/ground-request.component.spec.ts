import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroundRequestComponent } from './ground-request.component';

describe('GroundRequestComponent', () => {
  let component: GroundRequestComponent;
  let fixture: ComponentFixture<GroundRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroundRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroundRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
