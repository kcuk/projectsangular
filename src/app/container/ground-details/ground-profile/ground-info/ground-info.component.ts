import { GlobalService } from './../../../../GlobalService';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, HostListener, keyframes, trigger, state, animate, transition, style } from '@angular/core';
import { TranslateService } from "ng2-translate";

@Component({
  selector: 'app-ground-info',
  templateUrl: './ground-info.component.html',
  styleUrls: ['./ground-info.component.css'],
  animations: [
    trigger(
      'myAnimation',
      [
        transition(
          ':enter', [
            style({ transform: 'translateY(-100%)', opacity: 0 }),
            animate('500ms', style({ transform: 'translateY(0)', 'opacity': 1 }))
          ]
        ),
        transition(
          ':leave', [
            style({ transform: 'translateY(0)', 'opacity': 1 }),
            animate('500ms', style({ transform: 'translateY(-100%)', 'opacity': 0 }))
          ]
        )]
    )
    , trigger('dialog', [
      transition('void => *', [
        style({ transform: 'scale3d(.3, .3, .3)' }),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
      ])
    ])
  ]
})
export class GroundInfoComponent implements OnInit {

  public tech_rating: number = 0;
  public groundInformation: any;
  public shoutout: Array<any> = [];
  public loader: boolean = false;
  public msgs: Array<any> = [];
  public showRatingPopup: boolean = false;
  public groundId: any;
  public backdrop1: boolean = false;
  public ratingPopupBackdrop: boolean = false;
  public user_rating: number = 0;
  public total_rating: number = 0;
  public see_more_bttn: boolean = false;
  public timeSlot: Array<any> = [];
  public imageBasePath: any;
  public tempGroundImage: any;
  public groundImageInfo: Array<any> = [{ url: '', pos: 0 }, { url: '', pos: 1 }, { url: '', pos: 2 }, { url: '', pos: 3 }, { url: '', pos: 4 }]
  public ground_levels: any[] = [
    { label: '5V5', value: 'V5' },
    { label: '6V6', value: 'V6' },
    { label: '7V7', value: 'V7' },
    { label: '8V8', value: 'V8' },
    { label: '9V9', value: 'V9' },
    { label: '11V11', value: 'V11' },
  ];
  public ground_image: Array<any> = [];
  public filestatus: boolean = true;
  public textchangeoffile: string =localStorage.getItem('language')=='es'? 'Políticas de reservas':"Reservation policies";
  public admin: boolean = false;
  public previewUrl: string = '';
  public preview: boolean = false;
  public deleteConfirmationMessage: boolean = false;
  public delImgPos: any;
  public cost_per_game_popup: boolean = false;
  court_id: any = '';
  data: any;
  obj: any;
  constructor(private translate: TranslateService, public base_path_service: GlobalService, public route: ActivatedRoute, public router: Router) {
    if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
    }

    this.obj = this.base_path_service.groundProfile.subscribe(res => {
      if (res['court_id'] !== undefined) {
        this.court_id = res['court_id'];
        var tempUrl = window.location.href;
        var arrUrl = tempUrl.split('/');
        var temp = arrUrl[arrUrl.length - 1];
        if (temp !== 'schedule' && temp !== 'request') {
          this.groundInfo();
        }
      }
    })
  }

  ngOnDestroy() {
    this.obj.unsubscribe();
  }

  ngOnInit() {
    let id = this.route.parent.params.subscribe(params => {
      this.groundId = +params['id'];
      setTimeout(res => {
        if (localStorage.getItem('court_id')) {
          this.court_id = localStorage.getItem('court_id');
        }
        else {
          this.court_id = ''
        }
        this.groundInfo();
      }, 1000)
    });
    this.imageBasePath = this.base_path_service.image_url;
  }

  groundInfo() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'ground/createGround/?ground=' + this.groundId + "&form_type=information" + "&court_id=" + this.court_id + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        let court_information = {
          discount: res[0].json.court_serializer.discount,
          vat:res[0].json.ground_information.vat,
          minimum_fee: res[0].json.court_serializer.minimum_fee,
          payment_method: res[0].json.ground_information.payment_method
        }
        localStorage.setItem('court_information', JSON.stringify(court_information));
        this.loader = false;
        this.data = res[0].json.court_serializer
        this.groundInformation = res[0].json.ground_information;
        this.admin = res[0].json.admin;
        this.timeSlot = this.data.days[0] ? this.data.days[0].time_slot : [];
        this.ground_image = res[0].json.ground_image;
        for (var i = 0; i < this.groundImageInfo.length; i++) {
          for (var index = 0; index < this.ground_image.length; index++) {
            if (this.groundImageInfo[i].pos == this.ground_image[index].position) {
              this.groundImageInfo[i].url = this.ground_image[index].image;
              this.groundImageInfo[i].pos = this.ground_image[index].position;
              break;
            }
          }
        }
        var filecheck = this.groundInformation.rule;
        if (filecheck === '/media/undefined' || filecheck == null) {
          this.filestatus = false;
          this.textchangeoffile =localStorage.getItem('language')=='es'? 'Sin archivo adjunto':"No attachment"
        }
        if (this.ground_levels) {
          for (var index = 0; index < this.ground_levels.length; index++) {
            if (this.ground_levels[index].value == this.groundInformation.field_format) {
              this.groundInformation.field_format = this.ground_levels[index].label
            }

          }
        }
        this.tech_rating = res[0].json.customer_service;
        this.user_rating = res[0].json.user_rating;
        this.total_rating = res[0].json.total_rating;
        this.shoutout = res[0].json.shoutout;
        if (this.shoutout) {
          if (this.shoutout.length >= 5) {
            this.see_more_bttn = true;
          }
        }
      }, err => {
        this.loader = false;
        this.msgs.push({ severity: 'error', detail: "Error" });
        this.base_path_service.print(err);
      })
  }
  public ratingPopup() {
    this.showRatingPopup = true;

  }
  public updateRatingInfo() {
    this.backdrop1 = false;
    this.showRatingPopup = false;
    let ratingInfoData = {
      "ground": this.groundId,
      "rating": this.user_rating
    }
    this.showRatingPopup = false;
    let url = this.base_path_service.base_path_api() + "ground/rating/";
    this.base_path_service.PostRequest(url, ratingInfoData)
      .subscribe(res => {
        this.groundInfo();
        this.msgs.push({ severity: 'info', detail: "Tasa de éxito" });
      }, err => {
        this.base_path_service.print(err);
        this.msgs.push({ severity: 'error', detail: "Error" });
      })
  }
  public shoutoutPage() {
    this.router.navigateByUrl('dashboard/shoutouts/' + this.groundId + '/' + 'ground')

  }


  public groundPicChange($event: any, args: any, picArgs: any, index: number) {
    this.ground_image = $event.target.files[0];
    this.groundImageUpload(index).then(
      (result) => {

      },
      (error) => {
      }
    );
  }
  public groundImageUpload(index) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'ground/groundImages/';
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      formData.append("form_type", "ground");
      formData.append("ground_id", this.groundId);
      formData.append("ground_image", this.ground_image);
      formData.append("position", index);
      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            this.loader = false;
            this.msgs.push({ severity: 'info', detail: "La imagen se ha cargado correctamente" });
            this.tempGroundImage = JSON.parse(xhr.response);
            this.groundImageInfo[index].url = this.tempGroundImage.image;
            resolve(JSON.parse(xhr.response));
          } else if (xhr.status == 201) {
            this.loader = false;
          } else if (xhr.status == 205) {
            this.loader = false;

          } else if (xhr.status == 406) {
            this.loader = false;

          } else {
            this.loader = false;
            reject(xhr.response);
          }

        }
      }

      xhr.open("POST", url, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + JSON.parse(localStorage.getItem('userInfo')).token.access_token);
      xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
      xhr.send(formData);
    });
  }
  public deleteGroundImage(position) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "ground/image/" + this.groundId + "/?position=" + position + "&format=json";
    this.base_path_service.DeleteRequest(url)
      .subscribe(res => {
        this.groundImageInfo[position].url = "";
        this.groundImageInfo[position].pos = position
        this.msgs.push({ severity: 'info', detail: "La imagen ha sido eliminada" });
        this.loader = false;
      })
  }

}
