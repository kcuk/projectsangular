import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroundInfoComponent } from './ground-info.component';

describe('GroundInfoComponent', () => {
  let component: GroundInfoComponent;
  let fixture: ComponentFixture<GroundInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroundInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroundInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
