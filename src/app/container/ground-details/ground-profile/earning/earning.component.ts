import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'ng2-translate';
import { GlobalService } from '../../../../GlobalService';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-earning',
  templateUrl: './earning.component.html',
  styleUrls: ['./earning.component.css']
})
export class EarningComponent implements OnInit {
  lang: string;
  transaction: any;
  refund_popup: boolean;
  result: any;
  regDateValidation: boolean;
  reg_end_date: any;
  reg_start_date: string;
  minDate: Date;
  data_not_found: boolean;
  es: any;
  loader: boolean;
  msgs: any[] = [];
  form: FormGroup;
  court_id: any;
  ground_id: any;
  package_details: any;
  show_package: boolean = false;
  payment_success_msg: boolean = false;

  constructor(public route: ActivatedRoute, private translate: TranslateService, public base_path_service: GlobalService, public router: Router, public fb: FormBuilder) {
    this.minDate = new Date();
    this.lang = localStorage.getItem('language');

    this.es = {
      firstDayOfWeek: 1,
      dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
      dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
      dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
      monthNames: [
        this.lang == 'en' ? 'January' : "enero",
        this.lang == 'en' ? 'February' : "febrero",
        this.lang == 'en' ? 'March' : "marzo",
        this.lang == 'en' ? 'April' : "abril",
        this.lang == 'en' ? 'May' : "mayo",
        this.lang == 'en' ? 'June' : "junio",
        this.lang == 'en' ? 'July' : "julio",
        this.lang == 'en' ? 'August' : "agosto",
        this.lang == 'en' ? 'September' : "septiembre",
        this.lang == 'en' ? 'October' : "octubre",
        this.lang == 'en' ? 'November' : "noviembre",
        this.lang == 'en' ? 'December' : "diciembre"
      ],
      monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
      today: 'Hoy',
      clear: 'Borrar'
    }
    this.route.parent.params.subscribe(res => {
      this.ground_id = +res['id']
      console.log("idddddddddddd", this.ground_id);
    })
  }

  ngOnInit() {
    this.form = this.fb.group({
      'start_date': ['', Validators.compose([Validators.required])],
      'end_date': ['', Validators.compose([Validators.required])]
    })
  }


  regStartDate(event) {
    let newDate = new Date(event).toISOString();
    this.reg_start_date = newDate;
    if (typeof this.reg_end_date !== "undefined") {
      if (this.reg_start_date <= this.reg_end_date) {
        this.regDateValidation = false;
      } else {
        this.regDateValidation = true;
      }
    }
  }

  regEndDate(event) {
    let newDate = new Date(event).toISOString();
    this.reg_end_date = newDate;
    if (typeof this.reg_start_date !== "undefined") {
      if (this.reg_start_date <= this.reg_end_date) {
        this.regDateValidation = false;
      } else {
        this.regDateValidation = true;
      }
    }
  }

  showTransaction(value?) {
    if (!this.regDateValidation) {
      this.loader = true;
      this.court_id = localStorage.getItem('court_id');
      let url = this.base_path_service.base_path_api() + "ground/transactions/";
      let data = {
        "start_date": value.start_date.toISOString().substring(0, 10),
        "end_date": value.end_date.toISOString().substring(0, 10),
        "court": this.court_id
      }
      this.base_path_service.PostRequest(url, data)
        .subscribe(res => {
          this.loader = false;
          this.result = res[0].json;
          if (this.result.length == 0) {
            this.data_not_found = true;
          }
          else {
            this.data_not_found = false;
          }
        }, err => {
          this.loader = false;
        })
    }
  }

  showDetails() {
    let url = this.base_path_service.base_path_api() + "user/packageDetails/?form_type=ground&ground=" + this.ground_id
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.package_details = res[0].json.data;
        this.show_package = true;
      })
  }

  refund(id) {
    this.transaction = id;
    this.refund_popup = true;

  }

  refunded() {
    this.loader = true
    let url = this.base_path_service.base_path_api() + "user/refundAmount/?form_type=ground&trans_id=" + this.transaction;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.showTransaction(this.form.value)
        this.msgs = [];
        this.msgs.push({ severity: 'success', detail: 'Refund Successful!' });
        this.payment_success_msg = true;
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: 'Refund Unsuccessful!' });
      })
  }

  xyz() {

  }
}
