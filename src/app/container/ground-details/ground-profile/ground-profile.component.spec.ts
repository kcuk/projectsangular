import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroundProfileComponent } from './ground-profile.component';

describe('GroundProfileComponent', () => {
  let component: GroundProfileComponent;
  let fixture: ComponentFixture<GroundProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroundProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroundProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
