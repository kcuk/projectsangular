import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';

import { GlobalService } from './../../../../GlobalService';
import { TranslateService } from "ng2-translate";
import { countryCode } from "../../../registration/onboading2/country_code"
declare var google;

@Component({
  selector: 'app-edit-ground',
  templateUrl: './edit-ground.component.html',
  styleUrls: ['./edit-ground.component.css']
})
export class EditGroundComponent implements OnInit {


  long: any;
  lat: any;
  city_name: any;
  country_name: any;
  autocompleteItems: any[];
  checkCity: boolean = true;
  country_dial_code: string;
  GoogleAutocomplete: any;
  isMobValid: boolean;
  public groundCreateForm: FormGroup;
  public costGameForm: FormGroup;
  public cityValidation: boolean = false;
  public filteredCity: Array<any> = [];
  public loader: boolean = false;
  public city_list: Array<any> = [];
  public filteredCountry: Array<any> = [];
  public country_list: Array<any> = [];
  public time: any;
  public paymentMethodLebals: SelectItem[] = [];
  public paymentMethod: Array<any> = [];
  public msgs: Array<any> = [];
  public groundInfoObj: any;
  public daysLabels: Array<any> = [];
  public days: Array<any> = [];
  public groundId: number = 0;
  public imageBasePath: string = "";
  public pay: any;
  public cityValid: boolean = false;
  public citySelectValidation: boolean = false;
  public country_code: string = "+593";
  public file: File;
  public file_name: string = '';
  downloadURL: string = '';
  downloads: boolean = true;
  downloadss: boolean = false;
  deleteTournament: boolean = false;
  public ground_levels: any[] = [
    { label: 'Seleccionar Modality', value: null },
    { label: '5V5', value: 'V5' },
    { label: '6V6', value: 'V6' },
    { label: '7V7', value: 'V7' },
    { label: '8V8', value: 'V8' },
    { label: '9V9', value: 'V9' },
    { label: '11V11', value: 'V11' },
  ];
  public groundTypeInfo: any[] = [
    { label: 'Seleccionar Type', value: null },
    { label: 'Salón', value: 'Salón' },
    { label: 'Césped sintético', value: 'Césped sintético' },
    { label: 'Césped natural', value: 'Césped natural' }
  ];
  public cost_per_game_popup: boolean = false;
  public daysInfo: Array<any> = [];
  public daysPrice: Array<any> = [];
  public connect_cancha_popup: boolean = false;
  public connect_ground_list: Array<any> = [];
  public connectGroundList: Array<any> = [];
  public UnConnectGroundList: Array<any> = [];
  public temp: Array<number> = [];
  public priceInfo: Array<any> = [];
  public timeSlot: Array<any> = [];
  public priceOfGround: Array<any> = [];
  public deleteGround: boolean = false;
  public costOfGround: boolean = false;
  public costOfGroundValidation: boolean = false;
  public paymentMethodValidation: boolean = false;
  public updateFileInfo: any;
  public defaultGround: any;
  public defaultGroundstatus: Array<any> = [];
  public location: any;
  dummy: any;
  code: any = [
    { value: '+1', label: '+1' },
    { value: '+502', label: '+502' },

    { value: '+593', label: '+593' },
  ];
  length: any;
  rule_file_change: boolean = false;
  lang = localStorage.getItem('language')
  number: any = '';
  codes: SelectItem[] = [];
  mask: any = '999999999999999'
  constructor(
    public zone:NgZone,
    private translate: TranslateService, public _fb: FormBuilder, public router: Router, public base_path_service: GlobalService, public route: ActivatedRoute) {

    let tempValue = this.route.params.subscribe(params => {
      this.groundId = +params['id'];
    });
    this.imageBasePath = this.base_path_service.image_url;
    if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
    }

    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    // this.country_dial_code = localStorage.getItem('country_dial_code')
    countryCode.map((val) => {
      this.codes.push({
        label: val.name + " " + "(" + val.dial_code + ")",
        value: val.dial_code
      })
    })

  }
  ngOnInit() {
    this.paymentMethods();
    this.buildFormForCost();
    this.countryList();
    this.cityList();
    this.buildForm();
    this.groundInfo();

  }

  public cityList() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'team/location/?form_type=city';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.city_list = [];
        this.city_list.push({ label: 'Seleccionar City', value: null });
        let city = res[0].json;
        for (var index = 0; index < res[0].json.length; index++) {
          this.city_list.push({ label: city[index].location, value: city[index].id });
        }
      }, err => {
      })
  }

  public filterCity(event) {
    let query = event.query;
    let country = 1;
    if (country != undefined) {
      var url = this.base_path_service.base_path_api() + "team/location/?country=" + this.groundInfoObj.city.country_pk + "&city=" + query + "&format=json";
      this.base_path_service.GetRequest(url)
        .subscribe(
          res => {
            this.filteredCity = res[0].json;

            if (res[0].json.length == 0) {
              this.cityValidation = true;
              this.citySelectValidation = false;
            } else {
              this.cityValidation = false;
              this.citySelectValidation = false;
            }
          },
          err => {
          });
    }
    else {

    }
  }

  public countryList() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'team/country/';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.country_list = [];
        this.country_list.push({ label: localStorage.getItem('language') == 'en' ? "Select country" : 'Seleccionar País', value: null });
        let country = res[0].json;
        for (var index = 0; index < res[0].json.length; index++) {
          this.country_list.push({ label: this.lang == 'en' ? res[0].json[index].country_name : res[0].json[index].country_spanish, value: country[index].id });
        }
      }, err => {
      })
  }


  public buildFormForCost() {
    this.costGameForm = this._fb.group({
      monday: ['', []],
      tuesday: ['', []],
      wedensday: ['', []],
      thursday: ['', []],
      friday: ['', []],
      saturday: ['', []],
      sunday: ['', []]
    })
  }
  public paymentMethods() {
    let url = this.base_path_service.base_path_api() + "peloteando/payment_mode/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.paymentMethodLebals = [];
        let paymentInfo = res[0].json;
        if (res[0].json) {
          for (var i = 0; i < res[0].json.length; i++) {
            this.paymentMethodLebals.push({ label: paymentInfo[i].option, value: paymentInfo[i].id });
          }
        }
      }, err => {
        this.base_path_service.print(err);
      })
  }

  public groundInfo() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "ground/createGround/?ground=" + this.groundId + "&form_type=information&court_id=&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.groundInfoObj = res[0].json.ground_information;
        let file_path = this.groundInfoObj.rule
        if (file_path !== null) {
          this.file_name = file_path.split('/')[2]
        }
        this.updategroundFormInfo();
      }, err => {
        this.loader = false;
        this.base_path_service.print(err);
        this.msgs.push({ severity: 'error', detail: "Error" });
      })
  }
  public updategroundFormInfo() {

    this.groundCreateForm.patchValue({
      // 'country': this.groundInfoObj.city.country_pk,
      'groundName': this.groundInfoObj.ground_name,
      'telephone': this.groundInfoObj.other_contact,
      'mobileNo': this.groundInfoObj.contact,
      'direction': this.groundInfoObj.address,
      'city': this.groundInfoObj.city ,
      'country_code': this.groundInfoObj.country_code,
      'bio': this.groundInfoObj.bio,
      'vat': this.groundInfoObj.vat,

    }
     );

     this.lat = this.groundInfoObj.latitude;
     this.long = this.groundInfoObj.longitude;
     this.country_name = this.groundInfoObj.city.country_name;
     this.city_name = this.groundInfoObj.city.description;

    if (this.groundInfoObj.country_code == '+593') {
      this.length = 9;
    }
    else if (this.groundInfoObj.country_code == '+502') {
      this.length = 8;
    }
    else {
      this.length = 10
    }
    // this.days = this.groundInfoObj.days;
    if (this.groundInfoObj.payment_method) {
      for (var index = 0; index < this.groundInfoObj.payment_method.length; index++) {
        this.paymentMethod.push(this.groundInfoObj.payment_method[index].id);
        this.paymentMethodValidation = false;
      }
    }
    this.groundCreateForm.patchValue({ 'paymentMethod': this.paymentMethod })
  }

  public buildForm() {
    this.groundCreateForm = this._fb.group({
      groundName: ['', Validators.compose([Validators.maxLength(50), Validators.required])],
      telephone: ['', Validators.compose([Validators.required, Validators.maxLength(15), Validators.pattern('[0-9]+')])],
      mobileNo: ['', Validators.compose([Validators.required])],
      direction: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],
      // country: ['', [Validators.required]],
      bio: ['', []],
      paymentMethod: [[], Validators.compose([Validators.required])],
      country_code: ['', []],
      vat: [[], Validators.compose([Validators.required])],

    })
  }

  public GroundUpdate(form_Data, check) {
    console.log("dataaaaaaaaaaaaaaaaaa", form_Data)
    if (this.groundCreateForm.valid && this.checkCity) {
      this.loader = true;
      let url = this.base_path_service.base_path_api() + 'ground/createGround/' + this.groundId + '/';
      return new Promise((resolve, reject) => {
        var append_data: any = new FormData();
        var xhr = new XMLHttpRequest();
        append_data.append("ground_name", form_Data.groundName)
        append_data.append("country_code", form_Data.country_code);
        append_data.append("contact", parseInt(form_Data.mobileNo));
        append_data.append("other_contact", form_Data.telephone ? parseInt(form_Data.telephone) : '');
        append_data.append("city_name", this.city_name);
        append_data.append("country_name", this.city_name);
        append_data.append("latitude", this.lat);
        append_data.append("longitude", this.long);
        append_data.append("payment_method", JSON.stringify(form_Data.paymentMethod));
        append_data.append("address", form_Data.direction);
        append_data.append("bio", form_Data.bio);
        append_data.append("vat", form_Data.vat);
        append_data.append("rule", this.rule_file_change ? this.file : null);
        xhr.onreadystatechange = () => {
          if (xhr.readyState == 4) {
            if (xhr.status == 200 || xhr.status == 201 || xhr.status == 205) {
              this.loader = false;
              this.loader = false;
              if (check === 'close') {
                this.router.navigateByUrl('dashboard/ground/ground-profile/' + this.groundId)
              }
              else {
                this.router.navigateByUrl('dashboard/ground/ground-profile/' + this.groundId + "/edit_court")
              }
            }
            else {
              this.msgs = [];
              this.msgs.push({ severity: 'error', detail: "Error" });
              this.loader = false;
            }
          }
        }
        xhr.open("PUT", url, true);
        xhr.setRequestHeader("Authorization", 'Bearer ' + JSON.parse(localStorage.getItem('userInfo')).token.access_token);
        xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
        xhr.send(append_data);
      });
    }
  }

  public backToProfile() {
    this.router.navigateByUrl('/dashboard/ground/ground-profile/' + this.groundId);
  }

  public finalUpdateGroundPrice() {

    this.priceOfGround = [];
    this.daysPrice = [];
    for (var index = 0; index < 7; index++) {
      for (var i = 0; i < 4; i++) {
        if (this.priceInfo[index].time_slot[i].amount) {
          this.priceOfGround.push({ 'id': this.priceInfo[index].time_slot[i].id, 'amount': this.priceInfo[index].time_slot[i].amount });
        }
      }
      if (this.priceOfGround.length > 0) {
        this.daysPrice.push({ id: this.priceInfo[index].day_id, price: this.priceOfGround })
      }
      this.priceOfGround = [];
    }

    if (this.daysPrice.length > 0) {
      this.costOfGround = true;
      this.costOfGroundValidation = false;
    } else {
      this.costOfGround = false;
      this.costOfGroundValidation = true;
    }
  }
  public connectGroundInfo() {
    this.connect_ground_list = [];
    let url = this.base_path_service.base_path_api() + "ground/connectGround/" + this.groundId + "/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.UnConnectGroundList = res[0].json;
        for (var index = 0; index < this.UnConnectGroundList.length; index++) {
          if (this.UnConnectGroundList[index].is_default) {
            this.defaultGround = this.UnConnectGroundList[index].id;
            break;
          }
        }
      }, err => {

      })

  }
  public checkStatus(info) {
    if (this.defaultGround == info.id) {
      this.defaultGround = "";
    }
    if (info.is_default) {
      info.is_default = false;
    } else {
      info.is_default = true;

    }

  }
  public connectGround() {
    this.temp = [];
    this.defaultGroundstatus = [];
    for (var i = 0; i < this.UnConnectGroundList.length; i++) {
      if (this.UnConnectGroundList[i].status == true) {
        this.temp[i] = parseInt(this.UnConnectGroundList[i].id, 10);
      }

    }
    for (let index = 0; index < this.temp.length; index++) {
      if (this.temp[index] == this.defaultGround) {
        this.defaultGroundstatus.push({ is_default: 'True', id: this.temp[index] })
      } else {
        if (this.temp[index]) {
          this.defaultGroundstatus.push({ is_default: 'False', id: this.temp[index] })
        }
      }
    }
    let url = this.base_path_service.base_path_api() + "ground/connectGround/?format=json";
    let data = {
      "soccer_ground": this.defaultGroundstatus,
      "ground": this.groundId
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {

      }, err => {

      })
  }

  fileChangeListener(event) {
    this.file = event.target.files[0];
    this.file_name = event.target.files[0].name;
    this.rule_file_change = true;
    if (this.file_name) {
      this.downloadss = true;
    }
  }

  // public deleteGroundInfo() {
  //   let url = this.base_path_service.base_path_api() + "ground/groundProfile/" + this.groundId + "/?format=json";
  //   this.base_path_service.DeleteRequest(url)
  //     .subscribe(res => {
  //       this.router.navigateByUrl('dashboard/ground');
  //     }, err => {
  //       this.msgs.push({ severity: 'error', detail: "error" });
  //     })
  // }

  public validation() {
    if (this.groundCreateForm.value.paymentMethod != 0) {
      this.paymentMethodValidation = false;
    } else {
      this.paymentMethodValidation = true;
    }
  }

  close() {
    this.router.navigateByUrl('dashboard/ground/ground-profile/' + this.groundId)
  }

  deleteMainGround() {
    let url = this.base_path_service.base_path_api() + "ground/createGround/" + this.groundId + "/?format=json";
    this.base_path_service.DeleteRequest(url)
      .subscribe(res => {
        this.router.navigateByUrl('dashboard/ground');
      }, err => {
        this.msgs.push({ severity: 'error', detail: "error" });
      })
  }


  validMobile(event) {
    console.log("event=>>>>>>>>>>>>>>>>>>>>>>>", event);
    this.groundCreateForm.patchValue({ 'mobileNo': "" })
    if (event.value == '+1') {
      this.length = 10
    }
    else if (event.value == '+502') {
      this.length = 8
    }
    else {
      this.length = 9
    }
  }

  mobileVerify(value) {
    this.number = value
    if (value.toString().length != this.length || value < 0) {
      console.log("mobileeeeeeeeeeeeeeee")
      this.isMobValid = true;

    }
    else
      this.isMobValid = false;
    console.log("mobileeeeeeeeeeeeeeee =>>>>>>>>>>>>>>", this.isMobValid)

  }

  search(event) {
    this.checkCity = false;
    console.log("valussssssssss", event)
    this.GoogleAutocomplete.getPlacePredictions({ input: event.query, types: ['(regions)'] },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
          console.log("list", this.autocompleteItems)
        });
      });
  }

  setLoaction(item) {
    this.checkCity = true;
    this.country_name = item.terms.pop().value;
    this.city_name = item.terms[0].value;
    console.log("jhsjhgjhgsssssssssssssssssssssss",this.city_name)
    this.geoCode((item.description));

  }

  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.lat = results[0].geometry.location.lat();
      this.long = results[0].geometry.location.lng()
    })
  }

}

