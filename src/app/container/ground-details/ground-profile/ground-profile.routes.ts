import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GroundProfileComponent } from './ground-profile.component';
import { GroundInfoComponent } from './ground-info/ground-info.component';
import { GroundScheduleComponent } from './ground-schedule/ground-schedule.component';
import { GroundRequestComponent } from './ground-request/ground-request.component';
import { EditGroundComponent } from './edit-ground/edit-ground.component';
import { EditSponserComponent } from './edit-sponser/edit-sponser.component';
import { ReservationInfoComponent } from './reservation-info/reservation-info.component';
import { EditCourtComponent } from './edit-court/edit-court.component';
import { EarningComponent } from './earning/earning.component';
import { EarningTransactionComponent } from './earning-transaction/earning-transaction.component';


export const groundProfileRoutes: Routes = [
        {
                path: '', component: GroundProfileComponent,
                children: [
                        { path: '', component: GroundInfoComponent },
                        { path: 'information', component: GroundInfoComponent },
                        { path: 'request', component: GroundRequestComponent },
                        { path: 'schedule', component: GroundScheduleComponent },
                        { path: 'earning', component: EarningComponent },
                        { path: 'earning-transaction', component: EarningTransactionComponent }
                        
                        
                ]

        },
        { path: 'edit-ground', component: EditGroundComponent },
        { path: 'edit-sponser', component: EditSponserComponent },
        { path: 'reservation-info', component: ReservationInfoComponent },
        { path: 'edit_court', component: EditCourtComponent }
        

]

export const groundProfileRouting: ModuleWithProviders = RouterModule.forChild(groundProfileRoutes);