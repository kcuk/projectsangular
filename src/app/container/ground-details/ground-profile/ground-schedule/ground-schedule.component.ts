
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';
import { SelectItem, Message } from 'primeng/primeng';
import { DatePipe } from '@angular/common';
import { FormControl, Validators } from '@angular/forms';
import { Angulartics2 } from 'angulartics2';


declare var $: any;
declare var moment: any;
@Component({
  selector: 'app-ground-schedule',
  templateUrl: './ground-schedule.component.html',
  styleUrls: ['./ground-schedule.component.css']
})
export class GroundScheduleComponent implements OnInit {
  langauge: string;
  lang: string;
  request_sent_msg: boolean;
  check_attended: any;
  date_obj: string;
  price_nill: boolean;
  obj: any;
  public guestEmail = new FormControl();
  public connectGroundId: any;
  public connectGround: Array<any> = [];
  public groundId: number;
  public events: any[];
  public reservarPopup: boolean = false;
  public header: any;
  public locale = "es";
  public loader: boolean = false;
  public challengingTournamentInfo: Array<any> = [];
  public simpleBooking: Array<any> = [];
  public imageUrl: string = '';
  msgs: any[] = [];
  public validDate: any;
  public selected_sort_by: string = 'week';
  public week_popup: boolean = false;
  public mobiles: any;
  public reservePopup: boolean = false;
  public matchDate: string = "";
  public fromTime: string = "";
  public toTime: string = "";
  public message = "";
  public reserveValidation: boolean = false;
  public updateCost: string = '';
  public Property: string = ""
  public connectGroundDropdown: SelectItem[] = [];
  public reserve_options: SelectItem[] = [{ label: "Seleccionar opción", value: null },
  { label: "Por nombre", value: 'name' },
  { label: "Por email", value: 'email' }
  ];
  public source_options: SelectItem[] = [{ label: "Seleccionar opción", value: null },
  { label: "WhatsApp", value: 'whatsapp' },
  { label: "Phone", value: 'Phone' },
  { label: "Email", value: 'email' },
  { label: "In person", value: 'in_person' }]
  public profileInformation: any;
  public priceInfo: Array<any> = [];
  public price: number = 0;
  public selectedValues: boolean = false;
  public selectedRadioValue: String = 'everyday'
  public tillDate: any;
  public current_date: any;
  public tillDateStatus: boolean = true;
  public defaultView: string = '';
  public timeFormat: String = '';
  public selected_link: string = 'semana';
  public es_lang: any;
  public allDaySlot: boolean = false;
  public price_list: any;
  public startDate: any;
  public daysArray = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
  public globalDate: any;
  public globalEvent: any;
  public current_price: any
  public displayEventTime: boolean = false
  @ViewChild('ref') newInfo;
  public timezone: boolean = true;
  public guestCheckbox: string = "";
  public minDate: any;
  public admin: boolean = false
  public cancel_reservation_status: boolean = false;
  public select_option: any;
  public player_list: Array<any> = [];
  public email: any;
  public name_valid: boolean = false;
  public is_existed: boolean = false;
  public user_id: number = 0;
  public playerStatus: string = "false";
  public name = new FormControl();
  public reservePopupMobile: boolean = false;
  public phone: any;
  public existing_name: string = "";
  public slotLabelFormat: any;
  public status: any;
  court_name: any;
  court_id: any = '';
  main_ground: any;
  ground_reserve_admin: boolean = false;
  paid_with: any = '';
  recurrent_queue_popup: boolean = false;

  constructor(public angulartics2: Angulartics2, public base_path_service: GlobalService, public route: ActivatedRoute, public router: Router) {
    this.lang = localStorage.getItem('language');
    if (this.lang == 'es') {
      this.langauge = 'es';
    }
    else {
      this.langauge = 'en-gb'
    }
    console.log("langaaaaaaaaaaaaa", this.langauge)
    if (localStorage.getItem('court_id')) {
      this.court_id = localStorage.getItem('court_id')
    }
    this.lang = localStorage.getItem('language');
    this.obj = this.base_path_service.groundProfile.subscribe(res => {
      if (res['court_id'] !== undefined) {
        this.court_id = res['court_id'];
        var tempUrl = window.location.href;
        var arrUrl = tempUrl.split('/');
        var temp = arrUrl[arrUrl.length - 1];
        if (temp === 'schedule') {
          this.events = []
          this.groundScheduleInfo();
        }
      }
    })
  }

  ngOnDestroy() {
    this.obj.unsubscribe();
  }

  ngOnInit() {
    this.guestEmail = new FormControl('', Validators.compose([Validators.required, Validators.pattern('[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+')]));
    this.name = new FormControl('', [Validators.required]);
    let id = this.route.parent.params.subscribe(params => {
      this.groundId = +params['id'];
    });
    this.imageUrl = this.base_path_service.image_url;
    let a = new Date();
    let day = a.getDay();
    if (day == 1) {
      this.validDate = a.toISOString().substring(0, 10);
    }
    else if (day == 0) {
      this.validDate = new Date(new Date().setDate(a.getDate() - 6)).toISOString().substring(0, 10);
    }
    else {
      let sub_day = (day - 1);
      this.validDate = new Date(new Date().setDate(a.getDate() - sub_day)).toISOString().substring(0, 10);
    }
    this.header = {
      left: 'prev,next',
      center: 'title',
      right: 'agendaWeek,agendaDay'

    };
    let court;
    this.route.queryParams.subscribe(res => {
      court = res['court_id'];
    })
    if (court !== undefined) {
      this.court_id = court;
      localStorage.setItem('court_id', this.court_id)
    }
    this.court_id = localStorage.getItem('court_id');
    this.defaultView = 'agendaWeek';
    this.events = [];
    this.groundScheduleInfo();
    this.es_lang = {
      closeText: "Cerrar",
      prevText: "",
      currentText: "Hoy",
      monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
      monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
      dayNames: ["domingo", "lunes", "martes", "miÃ©rcoles", "jueves", "viernes", "sÃ¡bado"],
      dayNamesShort: ["dom", "lun", "mar", "miÃ©", "jue", "vie", "sÃ¡b"],
      dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
      weekHeader: "Sm",
      dateFormat: "yy-mm-dd",
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ""
    };

  }
  public formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  public groundScheduleInfo() {
    let url = this.base_path_service.base_path_api() + "ground/schedule/?date=" + this.validDate + "&ground=" + this.court_id + "&form_type=" + this.selected_sort_by + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        let update_court_info = JSON.parse(localStorage.getItem('court_information'))
        setTimeout(re => {
          update_court_info.discount = res[0].json.discount;
          update_court_info.minimum_fee = res[0].json.minimum_fee;
          update_court_info.vat = res[0].json.vat;
          localStorage.setItem('court_information', JSON.stringify(update_court_info));
        }, 2000)
        this.admin = res[0].json.admin;
        this.simpleBooking = res[0].json.simple_booking;
        this.price_list = res[0].json.price;
        this.court_name = res[0].json.ground_name;
        this.main_ground = res[0].json.main_ground;
        this.challengingTournamentInfo = res[0].json.tournament;
        if (this.simpleBooking) {
          for (var index = 0; index < this.simpleBooking.length; index++) {
            if (this.simpleBooking[index].is_guest) {
              this.events.push({
                "title": "Reserva",
                "start": this.simpleBooking[index].date,
                "end": this.simpleBooking[index].end_date_time,
                "backgroundColor": "##EF6262",
                "className": "moreBorder",
                "id": this.simpleBooking[index].reservation_pk,
                "form_type": 'player',
                "ground_admin": this.simpleBooking[index].is_admin,
              })
            } else {
              let date_obj = new Date().toISOString().substring(0, 10);
              let slot_date = this.simpleBooking[index].date.substring(0, 10);
              let color: any;
              if ((this.simpleBooking[index].paid_with === 'cash' ||
                this.simpleBooking[index].paid_with === 'minimum_fee') &&
                this.simpleBooking[index].queue_count < 2 &&
                slot_date >= date_obj
              ) {
                color = '#8ec63f';
              }
              else {
                color = "##EF6262";
              }

              this.events.push({
                "title": "Reserva",
                "start": this.simpleBooking[index].date,
                "end": this.simpleBooking[index].end_date_time,
                "backgroundColor": color,
                "className": "moreBorder",
                "id": this.simpleBooking[index].reservation_pk,
                "form_type": 'player',
                "ground_admin": this.simpleBooking[index].is_admin,
                "paid_with": this.simpleBooking[index].paid_with

              })
            }
          }
        }
        if (this.challengingTournamentInfo) {
          for (var index = 0; index < this.challengingTournamentInfo.length; index++) {
            this.events.push({
              "title": "Torneo",
              "start": this.challengingTournamentInfo[index].date_modify,
              "end": this.challengingTournamentInfo[index].end_date_time,
              "backgroundColor": "#9FDDF1",
              "className": "moreBorder",
              "id": this.challengingTournamentInfo[index].fixture_pk,
              "form_type": 'tournament'
            })

          }
        }

        var tempEvent = this.events;
        var temp_slot = [];
        for (var index = 0; index < this.price_list.length; index++) {
          var fromTime = parseInt(this.price_list[index].time_slot.from_time.split(':')[0])
          var toTime = parseInt(this.price_list[index].time_slot.to_time.split(':')[0])
          for (fromTime = fromTime; fromTime <= toTime; fromTime++) {
            if (this.price_list[index].time_slot.amount > 0) {
              if (fromTime >= 10) {
                temp_slot.push({ day: this.price_list[index].day, fromTime: fromTime + ':00', toTime: fromTime + 1 + ':00', amount: this.price_list[index].time_slot.amount })
              } else {
                if (fromTime == 9) {
                  temp_slot.push({ day: this.price_list[index].day, fromTime: '0' + fromTime + ':00', toTime: (fromTime + 1) + ':00', amount: this.price_list[index].time_slot.amount })
                } else {
                  temp_slot.push({ day: this.price_list[index].day, fromTime: '0' + fromTime + ':00', toTime: '0' + (fromTime + 1) + ':00', amount: this.price_list[index].time_slot.amount })
                }

              }
            } else {
              if (fromTime >= 10) {
                temp_slot.push({ day: this.price_list[index].day, fromTime: fromTime + ':00', toTime: fromTime + 1 + ':00', amount: '--' })
              } else {
                if (fromTime == 9) {
                  temp_slot.push({ day: this.price_list[index].day, fromTime: '0' + fromTime + ':00', toTime: (fromTime + 1) + ':00', amount: '--' })
                } else {
                  temp_slot.push({ day: this.price_list[index].day, fromTime: '0' + fromTime + ':00', toTime: '0' + (fromTime + 1) + ':00', amount: '--' })
                }

              }
            }
          }
        }
        console.log("slot=>>>>>>>>>>>>>>>>>", temp_slot)
        var tempDate = new Date();
        if (this.globalEvent == 'next') {
          tempDate = this.globalDate;
        } else if (this.globalEvent == 'prev') {
          tempDate = this.globalDate;
        }

        for (let k = 0; k < this.daysArray.length; k++) {
          let counter = 0;

          if (this.globalEvent == "next" || this.globalEvent == 'prev') {
            tempDate = this.globalDate;
          } else {
            tempDate = new Date();
          }


          for (let j = 0; j < temp_slot.length; j++) {
            if (temp_slot[j].day == this.daysArray[k]) {
              if (counter != 1) {
                if (this.globalEvent == "next" || this.globalEvent == 'prev') {
                  this.startDate = this.formatDate(tempDate.setDate(tempDate.getDate() - (tempDate.getDay() - k - 1)));
                } else {
                  if (new Date().getDay()) {
                    this.startDate = this.formatDate(tempDate.setDate(tempDate.getDate() - (new Date().getDay() - k - 1)));
                  } else {
                    this.startDate = this.formatDate(tempDate.setDate(tempDate.getDate() - (7 - k - 1)));
                  }
                }
                counter = 1;
              }

              let counterCheck = 0;
              for (let b = 0; b < tempEvent.length; b++) {
                if (tempEvent[b].start == (this.startDate + 'T' + temp_slot[j].fromTime)) {
                  counterCheck = 1;
                }
              }
              if (counterCheck == 0) {
                if (temp_slot[j].amount != '--') {
                  this.events.push({
                    "title": '$' + temp_slot[j].amount,
                    "start": this.startDate + 'T' + temp_slot[j].fromTime,
                    "end": this.startDate + 'T' + temp_slot[j].toTime,
                    "backgroundColor": "transparent",
                    "textColor": "#373a3c",
                    "index": j,
                    "price": temp_slot[j].amount
                  })
                } else {
                  this.events.push({
                    "title": temp_slot[j].amount,
                    "start": this.startDate + 'T' + temp_slot[j].fromTime,
                    "end": this.startDate + 'T' + temp_slot[j].toTime,
                    "backgroundColor": "transparent",
                    "textColor": "#373a3c",
                    "index": j,
                    "price": temp_slot[j].amount
                  })

                }
              }
            }
          }
        }

        this.loader = false;
      }, err => {


      })
  }

  public connectGroundInfo() {
    let url = this.base_path_service.base_path_api() + `ground/connectGround/?ground=${this.groundId}&form_type=connected&format=json`;
    this.connectGroundDropdown = [];
    var temp;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        temp = res[0].json;
        this.connectGroundId = { id: temp[0].id, img: temp[0].profile_pic };
        for (var index = 0; index < temp.length; index++) {
          this.connectGroundDropdown.push({ label: temp[index].ground_name, value: { id: temp[index].id, img: temp[index].profile_pic } });
        }
        this.groundScheduleInfo();
      }, err => {

      })
  }




  handleDayClickByPrice(event) {
    this.guestEmail.reset();
    this.selectedValues = false;
    this.tillDate = '';
    this.select_option = null;
    this.phone = "";
    this.name.reset();
    this.playerStatus = "false";
    this.existing_name = "";
    this.message = "";
    var obj = event;
    this.minDate = new Date(event);
    this.matchDate = obj.substring(0, 10);
    let today = obj.split('T')[1].split(':')[0];
    let actualTime = parseInt(today);
    let valid_time;

    if (actualTime < 0) {
      actualTime = actualTime + 24;
      valid_time = actualTime + ':' + '00';
    } else if (actualTime < 10) {
      valid_time = '0' + actualTime + ':' + '00'
    } else {
      valid_time = actualTime + ':' + '00'
    }
    this.fromTime = valid_time;
    let tTime = valid_time.split(':');
    let fTime = parseInt(tTime[0]) + 1;

    if (fTime < 9) {
      this.toTime = '0' + fTime + ':00';
    } else {
      this.toTime = fTime + ':00';
      if (fTime == 24) {
        this.toTime = '00' + ':00';
      }
    }
    if (this.paid_with == '') {
      this.reservarPopup = true;
    }
    else {
      this.recurrent_queue_popup = true;
    }

  }
  check_validation() {
    this.loader = true;
    if (this.tillDate) {
      this.tillDate = this.formatDateFun(this.tillDate);
    }
    let infoObj = {
      "ground": this.court_id,
      "form_type": "reserve",
      "date": this.matchDate,
      "from_time": this.fromTime,
      "to_time": this.toTime,
      "message": this.message,
      "is_recurrent_reservations": this.selectedValues,
      "till_date": this.tillDate,
      "recurrent_reservations_type": this.selectedRadioValue,
      "is_guest": true,
      "email": this.guestEmail.value,
      "is_existed": this.playerStatus,
      "user_id": this.user_id,
      "reserved_through": this.select_option,
      "phone": this.phone,
      "guest_name": this.name.value,
      'price': this.price,
      'court_name': this.court_name,
      "main_ground": this.main_ground,
      paid_with: ""

    }
    if (this.admin) {
      console.log("adminsssssssssssssssss")
      if ((this.name.valid && this.guestEmail.valid) || (this.existing_name)) {
        if (this.selectedValues) {
          if (this.tillDate) this.pop();
        } else {
          this.pop();
        }
      }
    }
    else if (this.price_nill) {
      console.log("priceeeeeeeeeeeeeeeeeeee")

      this.pop();
    }
    else {

      if (!this.ground_reserve_admin && (this.paid_with === 'minimum_fee' || this.paid_with === 'cash')) {
        console.log("cashhhhhhhhhhhhhhhhhhhhh")

        this.loader = false
        this.reservarPopup = false;
        let url = this.base_path_service.base_path_api() + 'scorecard/addQueue/?format=json';
        this.base_path_service.PostRequest(url, infoObj)
          .subscribe(res => {
            console.log("resposeeeeeeeeeeeee", res[0].json);
            this.msgs = [];
            this.msgs.push({ severity: 'success', detail: "You have added in queue successfully!" });
          }, err => {
            console.log("errrrrrrrrrrrrrrr", err.json);
            this.msgs = [];
            this.msgs.push({ severity: 'error', detail: err.json().error });
          })
      }
      else {
        console.log("elseeeeeeeeeeeeeeeeeeeee")

        this.reservarPopup = false;
        // localStorage.setItem('field_info', JSON.stringify(infoObj));
        // this.router.navigateByUrl('dashboard/payment-option/' + this.groundId + '?reserve=true' + "&recurrent=" + this.selectedValues);
        infoObj.paid_with = 'cash';
        let url = this.base_path_service.base_path_api() + 'scorecard/ground_request/?format=json';
        this.base_path_service.PostRequest(url, infoObj)
          .subscribe(res => {
            let data_obj = res[0].json;
            let reserve = {
              "ground_id": this.groundId,
              "court_id": parseInt(infoObj.ground),
              "reservation_id": data_obj.reservation_id,
              "request_type": 'cash'
            }
            let reservation_url = this.base_path_service.base_path_api() + 'ground/reserveFieldPay/';
            this.base_path_service.PostRequest(reservation_url, reserve)
              .subscribe(res => {
                this.pop('cash')
                // this.router.navigateByUrl('dashboard/ground/ground-profile/' + this.groundId + '?status=3');
              }, err => {
                this.loader = false;
                this.msgs = [];
                this.msgs.push({ severity: 'error', detail: "Server error please try again!" });
              })
          }, err => {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'error', detail: "Server error please try again!" });
          })
      }
      // this.router.navigateByUrl('dashboard/payment/' + this.groundId + "?reserve=" + true)
    }
  }

  pop(check?) {
    let url = this.base_path_service.base_path_api() + 'ground/mobile/' + this.court_id + '/?format=json';
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.loader = false;
      this.mobiles = res[0].json.mobile;
      if (check == 'cash') {
        this.request_sent_msg = true;
      }
      else {
        this.reservarPopup = false;
        this.reservePopupMobile = true;
      }

    }, err => {
      this.loader = false;

    })
  }
  formatDateFun(event) {
    var validDate;
    let month = parseInt(event.getMonth());
    month = month + 1
    if (event.getMonth() > 8) {
      if (event.getDate() > 9) {
        validDate = event.getFullYear() + "-" + month + "-" + event.getDate();
      } else {
        validDate = event.getFullYear() + "-" + month + "-" + '0' + event.getDate();
      }

    } else {
      if (event.getDate() > 9) {
        validDate = event.getFullYear() + "-" + '0' + month + "-" + event.getDate();
      } else {
        validDate = event.getFullYear() + "-" + '0' + month + "-" + '0' + event.getDate();
      }
    }
    return validDate;
  }

  public reserveCanchaInfo() {

    this.reservePopup = false;
    this.loader = true;
    var temp_email;
    var temp_admin = true;

    let infoObj = {
      "ground": this.court_id,
      "form_type": "reserve",
      "date": this.matchDate,
      "from_time": this.fromTime,
      "to_time": this.toTime,
      "message": this.message,
      "is_recurrent_reservations": this.selectedValues,
      "till_date": this.tillDate,
      "recurrent_reservations_type": this.selectedRadioValue,
      "is_guest": temp_admin,
      "email": this.guestEmail.value,
      "is_existed": this.playerStatus,
      "user_id": this.user_id,
      "reserved_through": this.select_option,
      "phone": this.phone,
      "guest_name": this.name.value
    }
    console.log("recurrent reservation", infoObj);
    let url = this.base_path_service.base_path_api() + 'scorecard/ground_request/?format=json';
    this.base_path_service.PostRequest(url, infoObj)
      .subscribe(res => {
        // this.events = [];
        this.reservePopupMobile = false;
        // this.groundScheduleInfo();
        this.message = '';
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'info', detail: "Reserva cancha con éxito" });
      }, err => {
        this.loader = false;
        this.reservePopupMobile = false;
        this.base_path_service.print(err);
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: "Error" });
      })
  }

  handleEventClick(event) {
    console.log("event=>>>>>>>>>>>>>>>>>>>>>>", event)
    this.select_option = null;
    this.guestCheckbox = "";
    let id = event.calEvent._id;
    let info = event.calEvent.form_type;
    let dateObj = event.calEvent._start._d;
    let date = dateObj.toISOString().substring(0, 10);
    let today = new Date(dateObj)
    let time = today.getTime();
    let current_date_obj = new Date().toISOString().substring(0, 10);


    if (event.calEvent.price && date >= current_date_obj) {
      if (event.calEvent.price === '--') {
        this.price = 0;
        this.price_nill = true;
      }
      else {
        this.price = event.calEvent.price;
      }
      this.paid_with = '';
      this.handleDayClickByPrice(dateObj.toISOString())
    } else if ((this.admin || event.calEvent.ground_admin) && info !== undefined) {
      this.profileInfo(date, time, info, id);
    }
    else if (event.calEvent.backgroundColor == '#8ec63f' && (event.calEvent.paid_with === 'minimum_fee' || event.calEvent.paid_with === 'cash')) {
      this.ground_reserve_admin = event.calEvent.ground_admin
      this.paid_with = event.calEvent.paid_with;
      this.handleDayClickByPrice(dateObj.toISOString())
    }
  }
  public profileInfo(date, time, info, id) {
    this.loader = true;
    this.check_attended = false;
    let url = this.base_path_service.base_path_api() + "ground/reserve/" + id + "/?form_type=" + info + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.profileInformation = res[0].json;
        if (this.profileInformation.is_attended) {
          this.check_attended = true
        }
        this.date_obj = new Date().toISOString().substring(0, 10);
        this.week_popup = true;
        if (info == 'player') {
          this.Property = 'player'
        } else {
          this.Property = 'tournament'
        }

      }, err => {
        this.loader = false;
      })
  }
  public redirectPlayerProfile(id) {
    this.router.navigateByUrl('dashboard/player/player-profile/' + id);
  }
  public tournamentProfile(id: number) {
    this.router.navigateByUrl('dashboard/tournaments/tournament-profile/' + id);
  }
  public cancelSimpleBooking(id) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'ground/schedule/' + id + '/?form_type=reservation'
    this.base_path_service.DeleteRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.status = JSON.parse(res[0].json._body);

        if (this.status.status) {
          this.events = [];
          this.groundScheduleInfo();
          this.msgs = [];
          this.msgs.push({ severity: 'info', detail: "Borrado exitosamente" });
        } else {
          this.cancel_reservation_status = true;
        }
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: "Error" });
      })
  }
  public deleteScheduleWithTour(id: number) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "ground/schedule/" + id + "/?form_type=match"
    this.base_path_service.DeleteRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.events = [];
        this.groundScheduleInfo();
        this.msgs = [];
        this.msgs.push({ severity: 'info', detail: "Borrado exitosamente" });
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: "Error" });
      })

  }
  public connectGroundChangeInfo() {
    this.events = [];
    this.groundScheduleInfo();
  }
  public previousDate(ref) {
    ref.prev();
    this.validDate = ref.getDate()._d.toISOString().substring(0, 10);
    this.events = [];
    this.globalDate = ref.getDate()._d;
    this.globalEvent = "prev";
    this.groundScheduleInfo();
  }
  public nextDate(ref) {
    ref.next();
    this.validDate = ref.getDate()._d.toISOString().substring(0, 10);
    this.events = [];
    this.globalDate = ref.getDate()._d;
    this.globalEvent = "next";
    this.groundScheduleInfo();

  }
  public sortBy(day: string, ref) {
    if (day == 'day') {
      ref.changeView('agendaDay');
      this.selected_sort_by = 'day';
      this.events = [];
      this.groundScheduleInfo();
    } else {
      ref.changeView('agendaWeek');
      this.selected_sort_by = 'week';
      this.events = [];
      this.groundScheduleInfo();
    }
  }
  public getReserveOption() {
    this.email = null;
    this.guestCheckbox = this.select_option;
    this.is_existed = false;
  }
  public filterBrands(event) {
    let query = event.query;
    let url = this.base_path_service.base_path_api() + "user/existingUser/?form_type=manage_profie&value=" + query + '&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.player_list = res[0].json;
      }, err => {
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: 'Algunos errores!' });
      });
  }
  public handleDropdownClick(event) {
    this.name_valid = false;
    this.email = event.email;
    this.user_id = event.user_id
    this.is_existed = true;
    this.existing_name = event.name + " " + event.last_name

  }

  attended(id) {
    console.log("helo+>>>>>>>>>>>>>>>>>>", this.check_attended);
    let url = this.base_path_service.base_path_api() + "ground/addPayment/"
    let data = {
      "reservation_id": id
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        console.log("ok=>>>>>>>>>>>>>>>>>", res[0].json);
      })
  }
}