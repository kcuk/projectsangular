import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroundScheduleComponent } from './ground-schedule.component';

describe('GroundScheduleComponent', () => {
  let component: GroundScheduleComponent;
  let fixture: ComponentFixture<GroundScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroundScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroundScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
