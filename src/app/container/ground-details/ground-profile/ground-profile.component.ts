import { Component, OnInit, ViewChild } from '@angular/core';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../GlobalService';
import { Angulartics2 } from 'angulartics2';
import * as html2canvas from 'html2canvas';
import { TranslateService } from "ng2-translate";

@Component({
  selector: 'app-ground-profile',
  templateUrl: './ground-profile.component.html',
  styleUrls: ['./ground-profile.component.css']
})
export class GroundProfileComponent implements OnInit {
  destroy: any;
  loader1: boolean;

  public groundprofileInfo: any;
  public profilePic: string = '';
  public loader: boolean = false;
  public msgs: Array<any> = [];
  public reservarPopup: boolean = false;
  public imageBasePath: string = '';
  public sponserList: Array<any> = [];
  public followersPopup: boolean = false;
  public follower_list: Array<any> = [];
  public matchDate: any;
  public fromTime: any;
  public toTime: any;
  public cost: any;
  public message: any;
  public cost_per_hour: number = 0;
  public myForm: FormGroup;
  public follow: string = "SEGUIR"
  public groundId: number = 0;
  public reserveValidation: boolean = false;
  public display_image_cropper: boolean = false;
  public backdrop: boolean = false;
  public profile_pic: string = "";
  public coverPic: string = "";
  public data: any;
  public data2: any;
  public updateCost: string = '$';
  public costField: boolean = true;
  public circleImg: boolean = false;
  public squareImg: boolean = false;
  public messageModel: boolean = false;
  public messageInfo: string = "";
  public messagePopup: boolean = false;
  public currentDate: any;
  public info: boolean = true;
  public es: any;
  public reservePopup: boolean = false;
  court_list_data: any[] = [];
  cropperSettings: CropperSettings;
  cropperSettings2: CropperSettings;
  @ViewChild('auto') auto: any;
  @ViewChild('cropper', undefined) cropper: ImageCropperComponent;
  @ViewChild('layout') canvasRef;
  url: string = "";
  twitterUrl: string = "";
  shareimgUrl: string = "";
  share_profole_image: string = "";
  downloadProfile: boolean = false;
  downloadDataInfo: any;
  imageUrl: string = "";
  public rating: number = 0;
  payment_success_msg: any = false;
  payment_failed_msg: any = false;
  court_popup: boolean = false;
  court_list: any[] = [];
  court_id: any = '';
  show_default_court_name: any;
  request_sent_msg: boolean = false;
  reservation_payment: boolean = false;
  constructor(private translate: TranslateService, public angulartics2: Angulartics2, public base_path_service: GlobalService, public _fb: FormBuilder, public route: ActivatedRoute, public router: Router) {

    let url = window.location.href
    this.route.queryParams.subscribe(param => {
      let status = param['status'];
      if (status != undefined) {
        this.loader = true;

        if (status == '1') {
          this.payment_success_msg = true;
          this.loader1 = false;
        }
        else if (status == 2) {
          this.payment_failed_msg = true;
          this.loader1 = false;
        }
        else if (status === '3') {
          this.request_sent_msg = true;
          this.loader1 = false;
        }
        else if (status === '11') {
          this.reservation_payment = true;
          this.loader1 = false;
        }
        else if (status === '12') {
          this.payment_failed_msg = true;
          this.loader1 = false;
        }
        // this.loader1 = true;
        // if (status === '1') {
        //   let redirect = url.replace('status=1', 'status=true');
        //   window.parent.location.href = redirect;
        // }
        // else if (status === '2') {
        //   let redirect = url.replace('status=2', 'status=false');
        //   window.parent.location.href = redirect;
        // }
        // else if (status == 'true') {
        //   this.payment_success_msg = true;
        //   this.loader1 = false;
        // }
        // else if (status == 'false') {
        //   this.payment_failed_msg = true;
        //   this.loader1 = false;
        // }
        // else if (status === '3') {
        //   this.request_sent_msg = true;
        //   this.loader1 = false;
        // }
        // else if (status === '11') {
        //   this.reservation_payment = true;
        //   this.loader1 = false;
        // }
        // else if (status === '12') {
        //   this.payment_failed_msg = true;
        //   this.loader1 = false;
        // }
      }
    })
    if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
    }
    this.currentDate = new Date();
    this.rectangleCropImage();
    this.circleCropImage();
    this.imageBasePath = this.base_path_service.image_url;
    this.es = {
      closeText: "Cerrar",
      prevText: "",
      currentText: "Hoy",
      monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
      monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
      dayNames: ["domingo", "lunes", "martes", "miÃ©rcoles", "jueves", "viernes", "sÃ¡bado"],
      dayNamesShort: ["dom", "lun", "mar", "miÃ©", "jue", "vie", "sÃ¡b"],
      dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
      weekHeader: "Sm",
      dateFormat: "yy-mm-dd",
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ""
    };
  }
 
  ngOnInit() {
    this.router.events.subscribe((evt) => {
      document.body.scrollTop = 0;
    });
    var tempUrl = window.location.href;
    var arrUrl = tempUrl.split('/');
    this.shareimgUrl = tempUrl;
    var temp = arrUrl[arrUrl.length - 1].split('?');
    var currentUrl = temp[0];
    if (currentUrl == 'request' || currentUrl == 'schedule' || currentUrl == 'earning') {
      this.info = false;
    }
    this.route.params.subscribe(params => {
      this.groundId = params['id'];
    });
    if (localStorage.getItem('court_id')) this.court_id = localStorage.getItem('court_id')
    this.groundAndPlayerInfo();
    this.buildForm();
    this.destroy = this.base_path_service.pageChange.subscribe(res => {
      this.groundId = res[3];
      this.info = false;
      this.groundAndPlayerInfo();
    })

  }

  public groundAndPlayerInfo() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'ground/createGround/?ground=' + this.groundId + "&form_type=profile&format=json";
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.loader = false;
      this.groundprofileInfo = res[0].json;
      this.sponserList = res[0].json.sponser_list;
      this.cost_per_hour = res[0].json.cost_per_hour;
      this.court_list_data = res[0].json.court_data;
      if (localStorage.getItem('court_id')) {
        this.court_id = (localStorage.getItem('court_id'));
      }
      else {
        this.court_id = res[0].json.court_id
        localStorage.setItem("court_id", this.court_id)
      }
      for (let i = 0; i < this.court_list_data.length; i++) {
        if (this.court_list_data[i].id == this.court_id) {
          this.show_default_court_name = this.court_list_data[i].ground_name;
        }
      }
      this.share_profole_image = this.base_path_service.image_url + "/" + res[0].json.share_ground_link;
      this.profilePic = this.base_path_service.image_url + this.groundprofileInfo.profile_pic;
      if (this.groundprofileInfo.profile_pic == null || this.groundprofileInfo.profile_pic == "media/null") {
        this.profilePic = "assets/images/ground-profile-logo/default.png"
      }
      if (this.groundprofileInfo.follow == false) {
        this.follow = 'SEGUIR'
      } else {
        this.follow = 'Dejar de seguir'
      }

    }, err => {
      this.msgs.push({ severity: 'error', detail: "Error" });
      this.base_path_service.print(err);
      this.loader = false;
    });
  }
  public showReservarPopup() {
    this.router.navigateByUrl(`dashboard/ground/ground-profile/${this.groundId}/schedule`)

  }
  public showFollowerPopup() {
    if (this.groundprofileInfo.follower) {
      this.router.navigateByUrl('/dashboard/followers?id=' + this.groundId + '&status=ground');
    }
  }
  public reserveCanchaInfo(formData) {
    this.reservePopup = false
    this.reservarPopup = false;
    this.loader = true;
    let infoObj = {
      "ground": this.groundId,
      "form_type": "reserve",
      "date": this.matchDate,
      "from_time": this.fromTime,
      "to_time": this.toTime,
      "message": this.message
    }
    let url = this.base_path_service.base_path_api() + 'scorecard/ground_request/?format=json';
    this.base_path_service.PostRequest(url, infoObj).subscribe(res => {
      this.loader = false;
      this.msgs.push({ severity: 'info', detail: "Reserva cancha con éxito" });
    }, err => {
      this.loader = false;
      this.base_path_service.print(err);
      this.msgs.push({ severity: 'error', detail: "Error" });
    })
  }

  public buildForm() {
    this.myForm = this._fb.group({
      cost: ['', []],
      selectedDate: ['', Validators.compose([Validators.required])],
      selectedTime: ['', Validators.compose([Validators.required])]
    })
  }

  public changeStatusOfFollow(followStatus: string) {
    this.loader = true;
    let followInfo = {
      "form_type": "follow_ground",
      "follow_ground": this.groundId
    }
    if (followStatus == "SEGUIR" || followStatus == "seguir") {
      this.angulartics2.eventTrack.next({ action: '10', properties: { category: 'Ground Followers' } });
      let url = this.base_path_service.base_path_api() + "user/profile/?format=json"
      this.base_path_service.PostRequest(url, followInfo).subscribe(res => {
        this.groundAndPlayerInfo();
        this.loader = false;
        this.msgs.push({ severity: 'info', detail: "Seguir" });
      }, err => {
        this.loader = false;
        this.base_path_service.print(err);
        this.msgs.push({ severity: 'error', detail: "Error" });
      })
    } else {
      let url = this.base_path_service.base_path_api() + "ground/follow/" + this.groundId + "/"
      this.base_path_service.DeleteRequest(url).subscribe(res => {
        this.loader = false;
        this.groundAndPlayerInfo();
        this.msgs.push({ severity: 'info', detail: "Dejar de seguir" });
      }, err => {
        this.loader = false;
        this.msgs.push({ severity: 'error', detail: "Error" });
      })
    }
  }
  public changeStatusOfFollowers(followStatus: string, playerId: number) {
    this.loader = true;
    let followInfo = {
      "form_type": "follow_player",
      "follow_player": playerId
    }
    if (followStatus == "FOLLOW" || followStatus == "follow") {
      let url = this.base_path_service.base_path_api() + "user/profile/?format=json"
      this.base_path_service.PostRequest(url, followInfo).subscribe(res => {
        this.showFollowerPopup();
        this.loader = false;
        this.msgs.push({ severity: 'info', detail: "Actualizar correctamente" });
      }, err => {
        this.loader = false;
        this.base_path_service.print(err);
        this.msgs.push({ severity: 'error', detail: "Error" });
      })
    } else {
      let url = this.base_path_service.base_path_api() + "user/player_follow/" + playerId + "/?format=json"
      this.base_path_service.DeleteRequest(url).subscribe(res => {
        this.loader = false;
        this.showFollowerPopup();
        this.msgs.push({ severity: 'info', detail: "Actualizar correctamente" });
      }, err => {
        this.loader = false;
        this.msgs.push({ severity: 'error', detail: "Error" });
      })
    }
  }
  public setTime() {
    this.toTime = parseInt(this.fromTime) + 1 + ':00';
    this.updateCost = '$' + this.cost_per_hour;
  }
  public keyUpEventForSetTime() {
    this.toTime = parseInt(this.fromTime) + 1;
    this.updateCost = '$' + this.cost_per_hour;
  }
  public calCostPerHours() {
    let from_time = this.fromTime.split(':');
    let to_time = this.toTime.split(':');
    var diffHours = to_time[0] - from_time[0];
    var diffMinute = to_time[1] - from_time[1];

    if (diffMinute > 0 && diffHours > 0) {
      this.cost = this.cost_per_hour * (diffHours + 1);
    } else if (diffMinute == 0 && diffHours > 0) {
      this.cost = this.cost_per_hour * diffHours;
    } else if (diffHours == 0 && diffMinute > 0) {
      this.cost = this.cost_per_hour * 1;
    }

    if (diffHours < 0 || diffHours < 0) {
      this.reserveValidation = true;
      this.updateCost = '$';
    } else {
      this.reserveValidation = false;
      this.updateCost = this.cost;
    }

  }

  /*** image cropper ***/

  public fileChangeListener($event, coverPic) {
    var image: any = new Image();
    this.coverPic = coverPic;
    if (this.coverPic == "coverPic") {
      this.circleImg = false;
      this.squareImg = true;
    } else {
      this.circleImg = true;
      this.squareImg = false;
    }

    var image: any = new Image();
    var file: File = $event.target.files[0];
    var myReader: FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
      if (that.coverPic == "coverPic") {
        that.profile_pic = image.src.split(",")[1];
      } else {
        that.profile_pic = image.src.split(",")[1];
      }
    };
    myReader.readAsDataURL(file);
    this.showDialog();
  }

  public showDialog() {
    this.display_image_cropper = true;

  }

  public updateGroundProfile() {
    this.loader = true;
    this.display_image_cropper = false;
    let obj = JSON.parse(localStorage.getItem("userInfo")).token.access_token;
    let url = this.base_path_service.base_path_api() + 'ground/groundImages/' + this.groundId + '/?format=json';
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      if (this.coverPic == "coverPic") {
        formData.append("form_type", "cover");
        formData.append("cover_pic", this.profile_pic);
        this.coverPic = "";
      } else {
        formData.append("form_type", "profile");
        formData.append("profile_pic", this.profile_pic);
      }

      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 205) {
            this.loader = false;
            this.groundAndPlayerInfo();
            this.msgs = [];
            this.msgs.push({ severity: 'info', summary: 'Actualizado Exitosamente', detail: 'Actualizar correctamente' });

          } else if (xhr.status == 400) {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'error', detail: 'Algo mal' });
            this.backdrop = false;
            this.display_image_cropper = false;
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("PUT", url, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + obj);
      xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
      xhr.send(formData);
    });
  }

  public remove_img($event) {

  }

  /*** image cropper ***/

  public rectangleCropImage() {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 1200;
    this.cropperSettings.height = 250;
    this.cropperSettings.keepAspect = false;

    this.cropperSettings.croppedWidth = 1200;
    this.cropperSettings.croppedHeight = 250;

    this.cropperSettings.canvasWidth = 350;
    this.cropperSettings.canvasHeight = 300;

    this.cropperSettings.minWidth = 100;
    this.cropperSettings.minHeight = 100;

    this.cropperSettings.rounded = false;
    this.cropperSettings.minWithRelativeToResolution = false;

    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings.noFileInput = true;

    this.data = {};
  }
  public circleCropImage() {
    this.cropperSettings2 = new CropperSettings();
    this.cropperSettings2.width = 300;
    this.cropperSettings2.height = 300;
    this.cropperSettings2.keepAspect = false;

    this.cropperSettings2.croppedWidth = 200;
    this.cropperSettings2.croppedHeight = 200;

    this.cropperSettings2.canvasWidth = 300;
    this.cropperSettings2.canvasHeight = 300;

    this.cropperSettings2.minWidth = 100;
    this.cropperSettings2.minHeight = 100;

    this.cropperSettings2.rounded = true;
    this.cropperSettings2.minWithRelativeToResolution = true;

    this.cropperSettings2.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings2.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings2.noFileInput = true;

    this.data2 = {};
  }

  public showMessagePopup() {
    this.messageModel = true;
  }
  public messageSentInfo() {
    this.messageModel = false;
    this.backdrop = false;
    var url = this.base_path_service.base_path_api() + 'user/message/';
    let data = {
      form_type: "p2g",
      message: this.messageInfo,
      ground: this.groundId

    }
    this.base_path_service.PostRequest(url, data).subscribe(res => {
      this.msgs.push({ severity: 'info', detail: "Enviado correctamente" });
    }, err => { })
  }
  downloadImage() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + `ground/soccerGroundProfile/?id=${this.groundId}&format=json`;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.downloadProfile = true;
        this.downloadDataInfo = res[0].json;
        this.rating = res[0].json.rating;

        setTimeout(() => {
          this.drawImage();
        }, 1000)

      }, err => {

      })
  }
  downloadAsImage() {
    this.downloadImage();

  }

  drawImage() {
    let canvas = this.canvasRef.nativeElement;
    html2canvas(canvas, {
      onrendered: (canvas) => {
        this.imageUrl = canvas.toDataURL("image/png");
      }
    })

  }

  courtList() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "ground/courtList/?ground_id=" + this.groundId;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.court_list = res[0].json;
        this.imageUrl = this.base_path_service.image_url;
      }, err => {
        this.loader = false;
      })
  }

  selectCourt(id) {
    this.court_popup = false;
    this.base_path_service.groundProfile.next({
      court_id: id
    });
    localStorage.setItem("court_id", id)
    for (let i = 0; i < this.court_list_data.length; i++) {
      if (this.court_list_data[i].id === id) {
        this.show_default_court_name = this.court_list_data[i].ground_name;
      }
    }
  }

  goLive() {
    this.router.navigateByUrl('dashboard/package/' + this.groundId + "?status=ground");
  }
}

