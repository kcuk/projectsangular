import { Router, NavigationEnd } from '@angular/router';
import { Http } from '@angular/http';
import { GlobalService } from './../../GlobalService';
import { Component, OnInit } from '@angular/core';
import { SelectItem, Message } from 'primeng/primeng';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import 'rxjs/add/operator/map';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent implements OnInit {
  isMobValid: boolean = true;
  public msgs: Message[] = [];
  public total_plateform_count: any;
  public quote_form: FormGroup;
  public filteredLocation: any;
  public countryList: SelectItem[] = [{ label: '*País', value: null }];
  lang = localStorage.getItem('language')
  public quote_options: SelectItem[] = [
    { label: this.lang == 'en' ? 'I am interested in learning more about…' : 'Me interesa conocer más de…', value: null },
    { label: this.lang == 'en' ? 'Manage leagues or tournaments' : 'Manejo de torneos y ligas', value: 'Tournaments' },
    { label: this.lang == 'en' ? 'Manage field reservations' : 'Manejo de canchas y reservas', value: 'Fields' },
    { label: this.lang == 'en' ? 'Manage teams' : 'Manejo de equipos', value: 'Teams' },
    { label: this.lang == 'en' ? 'Being a player in Pelotea ' : 'Ser un jugador de Pelotea', value: 'Players' },
    { label: this.lang == 'en' ? 'Scouting' : 'Reclutamiento de jugadores', value: 'Scouting' },
    { label: this.lang == 'en' ? 'Becoming a sponsor' : 'Ser un auspiciante', value: 'Other' },
  ];
  code: any = [
    { value: '+1', label: '+1' },
    { value: '+502', label: '+502' },
    { value: '+593', label: '+593' }
  ];
  length: any = 9;
  constructor(public router: Router, public global_services: GlobalService, public http: Http, public fb: FormBuilder) {


  }

  ngOnInit() {

    this.quote_form = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+')])],
      contact_number: ['',],
      country: ['', Validators.required],
      city: ['', Validators.required],
      company_name: [],
      learn_more_about: [],
      country_code: ['+593', Validators.compose([])]

    })
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
    let url = this.global_services.base_path_api() + 'user/totalCount/';
    this.http.get(url)
      .map(res => {
        return res.json();
      }).
      subscribe(res => {
        this.total_plateform_count = res
      })


    url = this.global_services.base_path_api() + "team/country/?format=json";
    this.global_services.GetRequestUnauthorised(url)
      .subscribe(
      res => {
        for (let i = 0; i < res[0].json.length; i++) {
          this.countryList.push({ "label": this.lang=='en'? res[0].json[i].country_name:res[0].json[i].country_spanish, "value": res[0].json[i].id })
        }
      },
      err => {
      })

  }
  public filterLocation(event) {
    let query = event.query;
    var url = this.global_services.base_path_api() + "team/location/?country=" + this.quote_form.controls['country'].value + "&city=" + query + '&format=json';
    this.http.get(url)
      .map(res => {
        return res.json();
      })
      .subscribe(res => {
        this.filteredLocation = res;
      },
      err => {
      });

  }
  public submitForm() {
    let url = this.global_services.base_path_api() + "user/contectAs/";
    if (this.quote_form.controls['city'].value) {
      this.quote_form.value.city = this.quote_form.controls['city'].value.id;
    }
    if (this.quote_form.controls['city'].value.id != null) {

      this.http.post(url, this.quote_form.value)
        .map(res => {
          return res.json();
        })
        .subscribe(res => {
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: 'Thanks You', detail: '' });
          this.quote_form.reset();
                    this.quote_form.patchValue({'country_code':'+593'})

          this.length = 9;
        }, err => {
        })
    }
  }
  numberFormat(_number, _sep) {
    _number = typeof _number != "undefined" && _number > 0 ? String(_number) : "";
    _number = _number.replace(new RegExp("^(\\d{" + (_number.length % 3 ? _number.length % 3 : 0) + "})(\\d{3})", "g"), "$1 $2").replace(/(\d{3})+?/gi, "$1 ").trim();
    if (typeof _sep != "undefined" && _sep != " ") {
      _number = _number.replace(/\s/g, _sep);
    }
    return _number;
  }

  validMobile(event) {
    console.log("event=>>>>>>>>>>>>>>>>>>>>>>>", event);
    this.quote_form.patchValue({ 'contact_number': "" })
    if (event.value == '+1') {
      this.length = 10
    }
    else if(event.value == '+502'){
      this.length = 8
    }
    else {
      this.length = 9
    }
  }

  mobileVerify(value) {
    if (value.length != this.length || value < 0) {
      console.log("mobileeeeeeeeeeeeeeee")
      this.isMobValid = false;

    }
    else
      this.isMobValid = true;
    console.log("mobileeeeeeeeeeeeeeee =>>>>>>>>>>>>>>", this.isMobValid)

  }

  emptycity() {
    this.quote_form.patchValue({ 'city': "" })
  }
}
