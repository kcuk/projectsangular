import { NgModule } from "@angular/core";
import { SharedModule } from "./../../shared/shared.module";
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./../../container/login/login.component";
import { SignupComponent } from "./../../container/signup/signup.component";
import { HomeComponent } from "./../../container/home/home.component";

export const changePasswordRoutes = [
  {
    path: "",
    component: HomeComponent
    // children: [
    //     // { path: '', component: LoginComponent },
    //     // { path: 'login', component: LoginComponent },
    //     // { path: 'signup', component: SignupComponent },
    // ]
  },

];

@NgModule({
  imports: [
    RouterModule.forChild(changePasswordRoutes),
    SharedModule.forRoot()
  ],
  declarations: [
    HomeComponent
    // LoginComponent,
    // SignupComponent,
  ]
})
export class HomeComponentModule {}
