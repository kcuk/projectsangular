import { MetadataService } from "ng2-metadata";
import { SelectItem } from "primeng/primeng";
import { GlobalService } from "./../../GlobalService";
import {
  Component,
  OnInit,
  Directive,
  ElementRef,
  Renderer,
  HostListener,
  animate,
  transition,
  trigger,
  state,
  style
} from "@angular/core";
import { Router } from "@angular/router";
import { Http } from "@angular/http";
import * as html2canvas from "html2canvas";
import { TranslateService } from "ng2-translate";

declare const FB: any;
declare const $: any;
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
  animations: [
    trigger("rotatedState", [
      state("default", style({ transform: "rotate(0)" })),
      state("rotated", style({ transform: "rotate(-180deg)" })),
      transition("rotated => default", animate("400ms ease-out")),
      transition("default => rotated", animate("400ms ease-in"))
    ])
  ]
})
export class HomeComponent implements OnInit {
  public total_plateform_count;
  any;
  public imageUrl: String = "";
  public feedback_images: Array<any>;
  public images: Array<any> = [];
  public url: any;
  public videoPlay: boolean = false;
  public videoPlayStatus: boolean = false;
  public state: string = "default";
  show_popup: boolean = true;
  url1: any = window.location.href;
  uinfo:any;

  constructor(
    private translate: TranslateService,
    public el: ElementRef,
    public renderer: Renderer,
    public router: Router,
    public http: Http,
    public services: GlobalService
  ) {
    if (localStorage.getItem("language")) {
      translate.use(localStorage.getItem("language"));
      this.show_popup = false;
    }
  }

  ngOnInit() {
    this.imageUrl = this.services.image_url;
    this.platformInformation();
    this.feedbackInformation();

    this.images = [];
    this.images.push({ image: "assets/images/shoes.png" });
    this.images.push({ image: "assets/images/Own_field.png" });
    this.images.push({ image: "assets/images/talk.jpg" });
    this.router.events.subscribe(evt => {
      document.body.scrollTop = 0;
    });
    $(document).click(event => {
      if (this.videoPlay) {
        this.videoPlay = false;
      } else if (this.videoPlayStatus) {
        this.videoPlay = true;
        this.videoPlayStatus = false;
      }
    });
    $(document).keyup(function(e) {
      if (e.keyCode == 27) {
        this.videoPlay = false;
      }
    });
  }

  ngAfterViewInit() {
    if (localStorage.getItem("user_info")) {
      this.uinfo = JSON.parse(localStorage.getItem("user_info"));
    }
    if (localStorage.getItem("user_info") && this.uinfo.approved) {
      let comdata = JSON.parse(localStorage.getItem("community_info"));
      console.log(comdata, "fgewguigewewrgwue");
      let comname = comdata.community_name;
      console.log(comname, "community name we get======================");
      this.router.navigate([comname]);
    }
    if (this.url1.includes("signup")) {
      console.log("offfffffffffffffffffffffff");
      this.router.navigateByUrl("home/signup");
    }
  }
  public scroll() {
    document.body.scrollTop = 0;
  }

  rotate() {
    this.state = this.state === "default" ? "rotated" : "default";
  }
  public platformInformation() {
    let url = this.services.base_path_api() + "user/totalCount/";
    this.services.GetRequest(url).subscribe(
      res => {
        this.total_plateform_count = res[0].json;
      },
      err => {
        console.log("error");
      }
    );
  }

  feedbackInformation() {
    let feedback_url =
      this.services.base_path_api() + `user/testimonial/?format=json`;
    this.services.GetRequest(feedback_url).subscribe(
      res => {
        this.feedback_images = res[0].json;
      },
      err => {}
    );
  }
  numberFormat(_number, _sep) {
    _number =
      typeof _number != "undefined" && _number > 0 ? String(_number) : "";
    _number = _number
      .replace(
        new RegExp(
          "^(\\d{" +
            (_number.length % 3 ? _number.length % 3 : 0) +
            "})(\\d{3})",
          "g"
        ),
        "$1 $2"
      )
      .replace(/(\d{3})+?/gi, "$1 ")
      .trim();
    if (typeof _sep != "undefined" && _sep != " ") {
      _number = _number.replace(/\s/g, _sep);
    }
    return _number;
  }

  loginStatus() {
    FB.getLoginStatus(function(response) {
      if (response.status === "connected") {
        var uid = response.authResponse.userID;
        var accessToken = response.authResponse.accessToken;
      } else if (response.status === "not_authorized") {
        console.log("not authorize");
      } else {
        console.log("login");
      }
    });
  }
  dataURItoBlob() {
    var canvas = document.getElementById("demo");
    html2canvas(canvas, {
      onrendered: canvas => {
        var imageData = canvas.toDataURL("image/png");
        this.url = canvas.toDataURL("image/png");
        var byteString = atob(imageData.split(",")[1]);
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }
      }
    });
  }
  @HostListener("click", ["$event"])
  onClick(e) {
    e.preventDefault();
  }
  backdropChange() {
    this.videoPlay = true;
  }

  goToLink(link) {
    window.open(link);
  }
}
