import { Component, OnInit, AfterViewInit } from "@angular/core";
import { Router } from "@angular/router";
import { GlobalService } from "./../../GlobalService";
import { MessagesModule, SelectItem } from "primeng/primeng";
import { Http } from "@angular/http";
import { LoaderComponent } from "./../loader/loader.component";
import { TranslateService } from "ng2-translate";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit, AfterViewInit {
  Players: Array<any> = [{}];
  Teams: Array<any> = [{}];
  selectedstatus: any;
  tournamentList: Array<any> = [];
  loader: boolean = false;
  rankingList: any;
  tournamentLink: any;
  tournamentImage: any;
  jugador: boolean = true;
  equipo: boolean = false;
  cancha: boolean = false;
  torneo: boolean = false;
  msgs: MessagesModule[] = [];
  images: Array<any> = [{}];
  shoutoutList: Array<any> = [];
  count: number = 0;
  topPlayerInfo: any;
  peloteandoPlayerInfo: any;
  tournamentInfo: boolean = false;
  peloteandoPlayer: boolean = true;
  tournamentDropdown: SelectItem[] = [];
  tournamentDropdownId: number = 0;
  type: string = "";
  player_count: number = 0;
  team_count: number = 0;
  tournament_count: number = 0;
  ground_count: number = 0;
  player_page: number = 0;
  team_page: number = 1;
  tournament_page: number = 1;
  ground_page: number = 1;
  page: number = 1;
  date: any;
  matchList: Array<any> = [];
  basePath: any;
  currentDate: any;
  Tournamentimages: Array<any> = [];
  Groundimages: Array<any> = [];
  responce: Array<any> = [];
  activeValue: string = "peloteando";
  image_url: string;
  uinfo: any;
  communitycredential: any;
  userinfo: boolean = false;
  displaydialog: boolean = false;
  communitynotexist: boolean = false;
  requestsent: boolean = false;

  collaps: boolean = false;
  showallcon: boolean = false;
  colindex: any;

  comdata: any;
  comname: any;
  upcommingleagues: any;
  topteams: any;
  topscorer: any;
  shoutouts: any;

  historydata: any;
  rateteams: any;
  logo = this.base_path_service.image_url;
  lang = localStorage.getItem("language");

  // maindata
  availabledata: any;
  tabledata: any = [];
  results: any = [];
  fixturedata: any;
  ongoingleagues: any;
  fixtures: any = [];
  userstatdata: any;
  newsdataimage: any;
  newsdatalatest: any;
  newsdatavideo: any;
  sponsers: any;
  gallerydata: any;
  shoutoutdata: any;
  biggallarydata: any;
  playerrankings: any;
  previousstats: any;
  tablealldata: any;
  nofixtures: boolean = false;
  noresuts: boolean = false;
  resultsection: boolean = false;
  notables: boolean = false;
  nosponser: boolean = false;
  nogallery: boolean = false;
  noleague: boolean = false;
  noshoutouts: boolean = false;
  notopplayers: boolean = false;
  notopteams: boolean = false;
  novideonews: boolean = false;
  noimagenews: boolean = false;
  public player_detail: any;
  usercommunities: any;
  shoutoutsection: boolean = false;
  filterdataarray = [];
  tournament_name: any;

  // maincommunityid
  communityapiid: any;
  leaguefilter: any;
  tournamentidfixture: any = "";
  resultidfixture: any = "";
  tableidfixture: any = "";

  // maincommunityid

  constructor(
    private translate: TranslateService,
    private base_path_service: GlobalService,
    private router: Router
  ) {
    if (localStorage.getItem("language")) {
      translate.use(localStorage.getItem("language"));
    }

    if (localStorage.getItem("community_info")) {
      this.comdata = JSON.parse(localStorage.getItem("community_info"));
      this.comname = this.comdata.community_name;
      console.log("name we get from local storage", this.comname)
      this.communityapiid = this.comdata.community_id;
    }
    else {
      this.comname = localStorage.getItem("communitynav_name")
      console.log(this.comname, "name we get from the url");
      this.communityapiid = localStorage.getItem("communityid");
      console.log(this.communityapiid,"communityapiid===========")

    }
    this.getcomunitycredential();

    this.availabledata = [
      { label: "Available", value: "available" },
      { label: "Not Going", value: "notgoing" },
      { label: "May be", value: "maybe" }
    ];
  }
  ngAfterViewInit() {
    if (localStorage.getItem("user_info")) {
      this.API_getTopPlayers();
      this.API_getTopTeams();
    }
  }
  open(index) {
    console.log(index, "index");
    this.colindex = index;
    this.showallcon = true;
    this.collaps = true;
  }
  close() {
    this.collaps = false;
    console.log(this.collaps, "coloa[se");
    this.showallcon = false;
  }

  ngOnInit() {
    this.getleaguefilter();
    if (localStorage.getItem("user_info")) {
      this.userinfo = true;
      this.date = new Date();
      var month = this.date.getMonth();
      month = month + 1;
      this.currentDate =
        this.date.getFullYear() + "-" + month + "-" + this.date.getDate();
      this.date.toString("yyyy-mmmm-dddd");
      this.shoutoutList = [];
      this.getShoutout("player");
      this.basePath = this.base_path_service.image_url;
      this.image_url = this.basePath;
      this.peloteandoScoreInfo();
      // new apis according to the communities with user info
      this.getuserstats();
      this.getnewscontent();
      this.getopenleagues();
      this.getsponsers();
      this.getgallery();
      this.getshoutouts();
      this.getpreviousstats();
      this.getfixtures();
      this.getresults();
      this.getplayerranking();
      this.gettables();
      this.gettopteams();
      this.shoutoutsection = true;
    } else {
      this.userinfo=false
      // api for the community without useinfo
      this.shoutoutsection = false;
      this.getshoutouts();
      this.peloteandoScoreInfo();
      this.getresults();
      this.gettables();
      this.getsponsers();
      this.gettopteams();
      this.getnewscontent();
      this.getopenleagues();
      this.getsponsers();
      this.getgallery();
      this.getfixtures();

      console.log("insideelse dashboarsd==========================");
      console.log(this.userstatdata, "userstatdata==================");
    }
  }

  getcomunitycredential() {
    if (localStorage.getItem("user_info")) {
      this.uinfo = JSON.parse(localStorage.getItem("user_info"));
    }
    this.loader = true;
    let url =
      this.base_path_service.base_path +
      "api/peloteando/communityDetail/?community_name=" +
      this.comname;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.loader = false;
        this.communitycredential = res[0].json;
        console.log(this.communitycredential,"all community credentials========")
        localStorage.setItem("community_info", JSON.stringify(this.communitycredential));
        localStorage.setItem("communityid", res[0].json.community_id);
      },
      err => {
        console.log("inside the error");
        localStorage.removeItem("communitynav_name");
        this.loader = false;
        this.displaydialog = false;
        this.communitynotexist = true;
      }
    );
  }
  redirect() {
    window.location.href = "http://" + this.tournamentLink;
  }
  filterfixtures(event) {
    console.log(event, "event===================");
    this.tournamentidfixture = event.value.value;
    this.getfixtures();
  }
  filterresults(event) {
    console.log(event, "event===================");
    this.resultidfixture = event.value.value;
    this.getresults();
  }
  filtertables(event) {
    console.log(event, "event===================");
    this.tableidfixture = event.value.value;
    this.gettables();
  }
  // api for getting user stats
  getleaguefilter() {
    let url =
      this.base_path_service.base_path +
      "api/user/league_filter/?community_id=" +
      this.communityapiid;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        let data = res[0].json;
        data.forEach(element => {
          this.filterdataarray.push({
            name: element.tournament_name,
            value: element.tournament_id
          });
        });
        if (this.filterdataarray.length > 0) {
          this.leaguefilter = true;
        }
        console.log(this.filterdataarray, "filterdataarray");
      },
      err => { }
    );
  }
  getuserstats() {
    let url =
      this.base_path_service.base_path +
      "api/user/user_stats/?community_id=" +
      this.communityapiid;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.userstatdata = res[0].json.data;
      },
      err => { }
    );
  }
  getnewscontent() {
    let url =
      this.base_path_service.base_path +
      "api/user/news/?community_id=" +
      this.communityapiid;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.newsdataimage = res[0].json.image_news;
        if (this.newsdataimage.length == 0) {
          this.noimagenews = true;
        }
        this.newsdatalatest = res[0].json.latest_news;
        this.newsdatavideo = res[0].json.video_news;
        if (this.newsdataimage.length == 0) {
          this.novideonews = true;
        }
      },
      err => { }
    );
  }
  getopenleagues() {
    let url =
      this.base_path_service.base_path +
      "api/user/open_league/?community_id=" +
      this.communityapiid;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.ongoingleagues = res[0].json.ongoing_league;
        this.upcommingleagues = res[0].json.reg_open_league;
        if (this.upcommingleagues.length == 0) {
          this.noleague = true;
        }
        console.log(
          this.upcommingleagues,
          "upcommingleagues=========================>>>>>>"
        );
      },
      err => {
        // this.loader = false;
      }
    );
  }
  getsponsers() {
    let url =
      this.base_path_service.base_path +
      "api/user/sponsors/?community_id=" +
      this.communityapiid;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.sponsers = res[0].json;
        if (this.sponsers == []) {
          this.nosponser = true;
        }
      },
      err => {
        // this.loader = false;
      }
    );
  }
  getgallery() {
    let url =
      this.base_path_service.base_path +
      "api/user/gallery/?community_id=" +
      this.communityapiid;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.gallerydata = res[0].json.gallery_picture;
        this.biggallarydata = res[0].json.featured_picture;
        if (this.biggallarydata == []) {
          this.nogallery = true;
        }
      },
      err => {
        // this.loader = false;
      }
    );
  }
  getshoutouts() {
    let url =
      this.base_path_service.base_path +
      "api/user/shoutOut/?community_id=" +
      this.communityapiid;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.shoutoutdata = res[0].json;
        if (this.shoutoutdata.length < 1) {
          this.noshoutouts = true;
        }
      },
      err => {
        // this.loader = false;
      }
    );
  }
  getpreviousstats() {
    let url =
      this.base_path_service.base_path +
      "api/user/user_stats/?form_type=last_match&community_id=" +
      this.communityapiid;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.previousstats = res[0].json.data;
        console.log(this.previousstats, "this.previousstats");
      },
      err => {
        // this.loader = false;
      }
    );
  }
  statusupdate(id,status) {
    let data = {
      fixture_id: id,
      status: status,
      community_id: this.communityapiid
    };
    var url = this.base_path_service.base_path + "api/user/fixtures/";
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        console.log(res, "res=================");
        let respons = res[0].json;
        this.getfixtures();
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }
  getfixtures() {
    let url =
      this.base_path_service.base_path +
      "api/user/fixtures/?community_id=" +
      this.communityapiid +
      "&tournament_id=" +
      this.tournamentidfixture +
      "&format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.fixturedata = res[0].json.latest_match;
        this.tournament_name = res[0].json.tournament_name;
        this.fixtures = res[0].json.upcoming_matches;
        console.log(this.fixtures.length, "fixtures length");
        if (this.fixtures.length == 0) {
          this.nofixtures = true;
        }
      },
      err => {
        // this.loader = false;
      }
    );
  }
  getresults() {
    let url =
      this.base_path_service.base_path +
      "api/user/results/?community_id=" +
      this.communityapiid +
      this.resultidfixture +
      "&format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.results = res[0].json.data;
        if (this.results.length < 1) {
          this.noresuts = true;
        }
      },
      err => {
        // this.loader = false;
      }
    );
  }
  gettables() {
    let url =
      this.base_path_service.base_path +
      "api/tournament/result/?community_id=" +
      this.communityapiid +
      this.tableidfixture +
      "&format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.tablealldata = res[0].json;
        this.tabledata = this.tablealldata.group.team;
        if (this.tabledata.length < 1) {
          this.notables = true;
        }
      },
      err => {
        // this.loader = false;
      }
    );
  }

  getplayerranking() {
    let url =
      this.base_path_service.base_path +
      "api/user/rankings/?community_id=" +
      this.communityapiid;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.playerrankings = res[0].json.data;
        console.log(this.playerrankings, "player rankings");
      },
      err => {
        // this.loader = false;
      }
    );
  }
  gettopteams() {
    let url =
      this.base_path_service.base_path +
      "api/user/top_teams/?community_id=" +
      this.communityapiid;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.topteams = res[0].json;
        if (this.topteams.length == 0) {
          this.notopteams = true;
        }
      },
      err => {
        // this.loader = false;
      }
    );
  }

  rateval1(event, team) {
    console.log(event, "rateevent");

    let data = {
      fixture_id: this.playerrankings.fixture_id,
      team_id: this.playerrankings.team1_id,
      player_id: team.player_id,
      rating: event.value,
      community_id: this.communityapiid
    };
    var url = this.base_path_service.base_path + "api/user/rankings/";
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        console.log(res, "res=================");
        let respons = res[0].json;
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }

  rateval2(event, team) {
    console.log(event, "rateevent");

    let data = {
      fixture_id: this.playerrankings.fixture_id,
      team_id: this.playerrankings.team2_id,
      player_id: team.player_id,
      rating: event.value,
      community_id: this.communityapiid
    };
    var url = this.base_path_service.base_path + "api/user/rankings/";
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        console.log(res, "res=================");
        let respons = res[0].json;
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }

  joinothercommunity() {
    this.router.navigateByUrl("home/community");
  }

  completeProfile() {
    this.loader = true;
    let url =
      this.base_path_service.base_path_api() +
      "user/completeProfile/?format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.loader = false;
        localStorage.setItem("checkoptionalpage", "complete_profile");
        localStorage.setItem("user_info", JSON.stringify(res[0].json.data));
        let user_info = JSON.parse(localStorage.getItem("user_info"));
        if (!user_info.is_phone_no) {
          this.router.navigate(["/home/signup/set-phone"]);
        } else if (!user_info.is_email) {
          this.router.navigate(["/home/signup/set-email"]);
        } else if (!user_info.is_username) {
          this.router.navigate(["/home/signup/set-user"]);
        } else if (!user_info.is_gender) {
          this.router.navigate(["/home/signup/set-gender"]);
        } else if (!user_info.is_height) {
          this.router.navigate(["/home/signup/set-height"]);
        } else if (!user_info.is_dob) {
          this.router.navigate(["/home/signup/set-dob"]);
        } else if (!user_info.is_leg) {
          this.router.navigate(["/home/signup/set-leg"]);
        } else if (!user_info.is_position) {
          this.router.navigate(["/home/signup/set-position"]);
        } else if (!user_info.is_profile_pic) {
          this.router.navigate(["/home/signup/set-profile-pic"]);
        }
        // else{
        //   this.router.navigateByUrl('dashboard/player/player-profile/' + this.player_id + '/information');
        // }
      },
      err => {
        this.loader = false;
      }
    );
  }

  API_getTopPlayers() {
    this.loader = true;
    let url =
      this.base_path_service.base_path +
      "api/user/player/?form_type=signup&format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.loader = false;
        this.Players = res[0].json;
        this.image_url = this.base_path_service.image_url;
      },
      err => {
        this.loader = false;
      }
    );
  }

  API_getTopTeams() {
    this.loader = true;
    let url =
      this.base_path_service.base_path + "api/team/profile/?format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.loader = false;
        this.Teams = res[0].json;
        this.image_url = this.base_path_service.image_url;
      },
      err => {
        this.loader = false;
      }
    );
  }

  terms() {
    this.router.navigateByUrl("terms");
    localStorage.setItem("term", "dashboard");
  }

  follow_player(player: any) {
    this.loader = true;
    let url =
      this.base_path_service.base_path + "api/user/profile/?format=json";
    let player_data = {
      form_type: "follow_player",
      follow_player: player.id
    };
    this.base_path_service.PostRequest(url, player_data).subscribe(
      res => {
        this.loader = false;
        this.get_shoutout("player");
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: res[0].json,
            detail: ""
          });
          this.API_getTopPlayers();
        }
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: "info", summary: err.json(), detail: "" });
      }
    );
  }

  follow_team(team: any) {
    this.loader = true;
    let url =
      this.base_path_service.base_path + "api/user/profile/?format=json";
    let team_data = {
      form_type: "follow_team",
      follow_team: team.id
    };
    this.base_path_service.PostRequest(url, team_data).subscribe(
      res => {
        this.loader = false;
        this.get_shoutout("team");
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: res[0].json,
            detail: ""
          });
          this.API_getTopTeams();
        }
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: "info", summary: err.json(), detail: "" });
      }
    );
  }

  get_shoutout(type) {
    this.page = 0;
    this.shoutoutList = [];
    if (type == "player") {
      this.player_page = 0;
      this.equipo = false;
      this.cancha = false;
      this.torneo = false;
      this.jugador = true;
    } else if (type == "team") {
      this.team_page = 0;
      this.jugador = false;
      this.cancha = false;
      this.torneo = false;
      this.equipo = true;
    } else if (type == "ground") {
      this.ground_page = 0;
      this.torneo = false;
      this.jugador = false;
      this.equipo = false;
      this.cancha = true;
    } else if (type == "tournament") {
      this.tournament_page = 0;
      this.jugador = false;
      this.equipo = false;
      this.cancha = false;
      this.torneo = true;
    }
    this.getShoutout(type);
  }

  /****************anoop***************/
  getShoutout(value) {
    this.type = value;
    if (this.type == "player") {
      this.player_page = this.player_page + 1;
      let url =
        this.base_path_service.base_path_api() +
        "user/shoutout/?form_type=player&page=" +
        this.player_page +
        "&format=json";

      this.base_path_service.GetRequest(url).subscribe(res => {
        this.page = res[0].json.total_page;
        this.shoutoutList = this.shoutoutList.concat(res[0].json.shoutout_list);
      });
    }
    if (this.type == "team") {
      this.team_page = this.team_page + 1;

      let url =
        this.base_path_service.base_path_api() +
        "user/shoutout/?form_type=team&page=" +
        this.team_page +
        "&format=json";
      this.base_path_service.GetRequest(url).subscribe(res => {
        this.shoutoutList = this.shoutoutList.concat(res[0].json.shoutout_list);
        this.page = res[0].json.total_page;
      });
    }
    if (this.type == "ground") {
      this.ground_page = this.ground_page + 1;

      let url =
        this.base_path_service.base_path_api() +
        "user/shoutout/?form_type=ground&page=" +
        this.ground_page +
        "&format=json";
      this.base_path_service.GetRequest(url).subscribe(res => {
        this.shoutoutList = this.shoutoutList.concat(res[0].json.shoutout_list);
        this.page = res[0].json.total_page;
      });
    }
    if (this.type == "tournament") {
      this.tournament_page = this.tournament_page + 1;

      let url =
        this.base_path_service.base_path_api() +
        "user/shoutout/?form_type=tournament&page=" +
        this.tournament_page +
        "&format=json";
      this.base_path_service.GetRequest(url).subscribe(res => {
        this.shoutoutList = this.shoutoutList.concat(res[0].json.shoutout_list);
        this.page = res[0].json.total_page;
      });
    }
  }

  seemore(value) {
    this.getShoutout(value);
  }

  navigate(id, profile) {
    switch (profile) {
      case "player":
        this.router.navigateByUrl("/dashboard/player/player-profile/" + id);
        break;
      case "team":
        this.router.navigateByUrl("/dashboard/team/team-profile/" + id);
        break;
      case "ground":
        this.router.navigateByUrl("/dashboard/ground/ground-profile/" + id);
        break;
      case "tournament":
        this.router.navigateByUrl(
          "/dashboard/tournaments/tournament-profile/" + id
        );
        break;
    }
  }

  peloteandoScoreInfo() {
    this.tournamentInfo = false;
    let url =
      this.base_path_service.base_path_api() +
      "scorecard/topScorerPlayer/?community_id=" +
      this.communityapiid +
      "&format=json";
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.peloteandoPlayerInfo = res[0].json;
      if (this.peloteandoPlayerInfo.length == 0) {
        this.notopplayers = true;
      }
    });
  }
  goToPlayerProfile(id: number) {
    this.router.navigateByUrl("dashboard/player/player-profile/" + id);
  }
  tosignup() {
    this.displaydialog = false;
    this.router.navigateByUrl("/home/community/selectrole");
  }
  tologin() {
    this.displaydialog = false;
    this.router.navigateByUrl("/home/login");
  }
  joincommunity(){
    this.router.navigateByUrl("/home/community/selectrole");
  }
  tocommunnity() {
    this.displaydialog = false;
    this.communitynotexist = false;
    this.router.navigateByUrl("/home/community");
  }
}
