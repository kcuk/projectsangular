import { GlobalService } from "./../../GlobalService";
import { SelectItem } from "primeng/primeng";
import { Router } from "@angular/router";
import { Angulartics2 } from "angulartics2";
import {
  trigger,
  state,
  style,
  transition,
  animate
} from "@angular/animations";
import {
  OnInit,
  EventEmitter,
  HostListener,
  Component,
  Output,
  ElementRef,
  Inject,
  NgZone
} from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";
import { TranslateService } from "ng2-translate";

declare const $: any;

@Component({
  selector: "home-navbar",
  templateUrl: "./home-navbar.component.html",
  styleUrls: ["./home-navbar.component.css"],
  animations: [
    trigger("myAnimation", [
      transition(":enter", [
        style({ transform: "translateY(-100%)", opacity: 0 }),
        animate("500ms", style({ transform: "translateY(0)", opacity: 1 }))
      ]),
      transition(":leave", [
        style({ transform: "translateY(0)", opacity: 1 }),
        animate("500ms", style({ transform: "translateY(-100%)", opacity: 0 }))
      ])
    ])
  ]
})
export class HomeNavbarComponent implements OnInit {
  public info_drop_down: string = "Company";
  public dropDown: boolean = false;
  public unSubscribe: any;
  public navIsFixed: boolean = true;
  public eventCheck: number = 0;
  public direction: string = "";
  display: boolean = false;
  lang: any;
  displayFlag: any = "es";
  comoptions: any;
  usercommunities: any;
  dropdata = [];

  @Output()
  pageYPositionChange: EventEmitter<any> = new EventEmitter();

  constructor(
    private translate: TranslateService,
    public lc: NgZone,
    @Inject(DOCUMENT) public document: Document,
    public angulartics2: Angulartics2,
    public router: Router,
    public global: GlobalService,
    public element: ElementRef
  ) {

    if (localStorage.getItem("language")) {
      this.displayFlag = localStorage.getItem("language");
    }
  }

  ngOnInit() {
    let data = JSON.parse(localStorage.getItem("user_info"));
    if (data) {
      // this.comoptions = true;
      this.getcommunitylist();
      console.log("user is valid to create a new community");
    } else {
      this.comoptions = false;
    }

    this.currentPosition();
    $(document).click(event => {
      if (event.target.id == "check") {
        this.dropDown = this.dropDown ? false : true;
      } else {
        this.dropDown = false;
      }
      if (event.target.id == "imglang") {
        this.display = this.display ? false : true;
      } else {
        this.display = false;
      }
    });
    this.angulartics2.eventTrack.next({
      action: "myAction",
      properties: { category: "myCategory" }
    });
  }
  getcommunitylist() {
    // this.loader = true;
    let url = this.global.base_path + "api/user/communities/";
    this.global.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.usercommunities = res[0].json;
        this.usercommunities.forEach(element => {
          this.dropdata.push({ 'name': element.community_name, 'value': element.community_id })
        });
        this.comoptions = true;
      },
      err => {
        // this.loader = false;
      }
    );
  }

  // toselectedcommunity(event) {
  //   console.log(event, "event===================");
  // }

  toselectedcommunity(event) {
    console.log("erewr")
    let community_id = event.value.value;
    console.log(community_id, "community id for the selection");
    this.getcommunityinfo(community_id);
  }
  getcommunityinfo(id) {
    let url = this.global.base_path + "api/user/communities/" + id + "/";
    this.global.GetRequest(url).subscribe(
      res => {
        let data = res[0].json;
        console.log(data, "data==============");
        localStorage.setItem(
          "community_info",
          JSON.stringify(data)
        );
        // for the redirection
        if (localStorage.getItem("community_info")) {
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.router.navigate([comname]);
        }
        else { }
      },
      err => {
      }
    );
  }





  currentPosition() {
    this.global.currentLocation().subscribe(res => {
      let data = res.json();
      localStorage.setItem("lat", data.location.latitude);
      localStorage.setItem("long", data.location.longitude);
      localStorage.setItem("city", data.city);
      localStorage.setItem("country_name", data.country.name);
      // this.zone.run(() => {
      //   this.router.navigateByUrl("dashboard");
      // });
    });
  }

  public redirectPage() {
    window.open("https://peloteando.zendesk.com/hc/es");
  }
  public dropDownCheck(res?: any) {
    if (res == "check") {
      this.dropDown = this.dropDown ? false : true;
    } else {
      this.dropDown = false;
    }
  }

  @HostListener("window:scroll", ["$event"])
  track(event: any) {
    // let number = this.document.body.scrollHeight;
    // console.log("bottom", number, window.pageYOffset + window.innerHeight);

    let st = window.pageYOffset;
    let dir = "";
    if (st > this.eventCheck) {
      dir = "down";
    } else {
      dir = "up";
    }
    this.eventCheck = st;
    this.lc.run(() => {
      this.direction = dir;
    });
    if (this.direction === "up") {
      this.navIsFixed = true;
    } else {
      this.navIsFixed = false;
    }
  }

  routing() {
    this.router.navigateByUrl("./home/signup");
  }
}
