import { Component, OnInit, HostListener, Inject } from "@angular/core";
import { GlobalService } from "./../../GlobalService";
import { Message } from "primeng/primeng";
import { DOCUMENT } from "@angular/platform-browser";
import { TranslateService } from "ng2-translate";

declare const $;
@Component({
  selector: "app-player-details",
  templateUrl: "./player-details.component.html",
  styleUrls: ["./player-details.component.css"]
})
export class PlayerDetailsComponent implements OnInit {
  public players: Array<any> = [];
  public show: boolean = false;
  public checked: boolean = true;
  public positions: Array<number> = [];
  public positionObj: any[] = [];
  public townSelected: string = "";
  public refree: boolean = false;
  public pro: boolean = false;
  public towns: Array<any> = [];
  public len;
  public showBtn: boolean = false;
  public cards: boolean = false;
  public basePath: string;
  public msgs: Message[] = [];
  public activateLoader: boolean;
  public isdata: boolean = false;
  public isError: boolean = false;
  public nameSelected: string = "";
  public cityPlaceholder = "Ciudad";
  public page: number = 1;
  public gender: Array<any> = [];
  public att: Array<number> = [];
  public tech: Array<number> = [];
  public from_filter: string = "";
  comdata: any;
  communityapiid: any;
  userexist: boolean = false;

  constructor(
    private translate: TranslateService,
    @Inject(DOCUMENT) public document: Document,
    public global: GlobalService
  ) {
    localStorage.removeItem("court_id");
    localStorage.removeItem("courts");
    localStorage.removeItem("court_information");
    localStorage.removeItem("field_info");
    this.basePath = this.global.image_url;
    this.activateLoader = false;
    if (localStorage.getItem("language")) {
      translate.use(localStorage.getItem("language"));
    }
    if (localStorage.getItem("user_info")) {
      this.userexist = true;
    } else {
      this.userexist = false;
    }
  }
  ngOnInit() {
    if (localStorage.getItem("community_info")) {
      this.comdata = JSON.parse(localStorage.getItem("community_info"));
      this.communityapiid = this.comdata.community_id;
    } else {
      this.communityapiid = localStorage.getItem("communityid");
      console.log(this.communityapiid, "communityapiid===========");
    }
    this.trackState();
    this.getPosition();
    this.mob();
  }

  @HostListener("window:scroll", ["$event"])
  track(event: any) {
    let number = this.document.body.scrollHeight;
    let totalNumber = window.pageYOffset + window.innerHeight;
    if (number == Math.ceil(totalNumber)) {
      this.from_filter = "";
      this.page = this.page + 1;
      if (this.page <= this.len) {
        this.trackState();
      }
    }
  }
  mob() {
    let mq = window.matchMedia("screen and (min-width:640px)");
    if (mq.matches) {
      this.show = false;
    } else {
      this.show = true;
    }
  }

  trackState() {
    this.getPlayers(this.page);
  }

  getPlayers(pg: any, id?) {
    console.log("position", this.positionObj);
    this.page = pg;
    this.isError = false;
    this.isdata = false;
    this.msgs = [];
    this.activateLoader = true;
    for (let i: number = 0; i < this.positionObj.length; i++)
      this.positionObj[i] = parseInt(this.positionObj[i]);
    let url = this.global.base_path + "api/user/player/?format=json";
    let data = {
      player_name: this.nameSelected.trim(),
      form_type: "filter",
      city: this.townSelected,
      gender: this.gender,
      position: this.positionObj,
      attitude: this.att,
      technique: this.tech,
      referee: this.refree,
      is_pro: this.pro,
      page: this.page,
      latitude: localStorage.getItem("lat"),
      longitude: localStorage.getItem("long"),
      distance: parseInt(localStorage.getItem("distance")),
      community_id: this.communityapiid
    };

    if (this.userexist) {
      this.global.PostRequest(url, data).subscribe(
        res => {
          this.mob();
          this.cards = true;
          this.activateLoader = false;
          if (this.from_filter != "") {
            this.players = [];
            this.players.push(res[0].json.data);
          } else {
            this.players.push(res[0].json.data);
          }

          this.len = res[0].json.total_pages;
          if (res[0].json.data < 1) this.isdata = true;
        },
        err => {
          this.msgs.push({
            severity: "error",
            summary: "Error",
            detail: "No se pudo obtener la lista de jugador"
          });
          this.activateLoader = false;
          this.isError = true;
        }
      );
    } else {
      this.global.PostRequestUnauthorised(url, data).subscribe(
        res => {
          this.mob();
          this.cards = true;
          this.activateLoader = false;
          if (this.from_filter != "") {
            this.players = [];
            this.players.push(res[0].json.data);
          } else {
            this.players.push(res[0].json.data);
          }

          this.len = res[0].json.total_pages;
          if (res[0].json.data < 1) this.isdata = true;
        },
        err => {
          this.msgs.push({
            severity: "error",
            summary: "Error",
            detail: "No se pudo obtener la lista de jugador"
          });
          this.activateLoader = false;
          this.isError = true;
        }
      );
    }
  }

  getPosition() {
    this.msgs = [];
    this.activateLoader = true;
    let url = this.global.base_path + "api/user/position/?format=json";
    this.global.GetRequest(url).subscribe(
      res => {
        this.activateLoader = false;
        this.positions = res[0].json;
      },
      err => {
        this.msgs.push({
          severity: "error",
          summary: "Error",
          detail: "algunos errores"
        });
        this.activateLoader = false;
      }
    );
  }

  paginate(event) {
    this.players = [];
    let page = event.page + 1;
    this.page = page;
    this.getPlayers(page);
  }
  fun() {
    this.showBtn = true;
    this.cards = false;
  }

  searchCity(event) {
    let query = event.query;
    if (query) {
      let url =
        this.global.base_path +
        "api/team/location/?country=1&city=" +
        query +
        "&format=json";
      this.global.GetRequest(url).subscribe(
        res => {
          this.towns = [];
          this.activateLoader = false;
          this.towns = res[0].json;
        },
        err => {
          this.activateLoader = false;
        }
      );
    } else {
      this.townSelected = "";
      this.getPlayers(1);
    }
  }

  cityFilter(event) {
    this.players = [];
    this.townSelected = event.location;
    this.getPlayers(1);
  }

  nameFilter(event) {
    this.players = [];
    this.from_filter = "filter";
    this.getPlayers(1);
  }

  follow(player) {
    if (player.follow) {
      let url =
        this.global.base_path +
        "api/user/player_follow/" +
        player.id +
        "/?format=json";
      this.global.DeleteRequest(url).subscribe(
        res => {
          player.follow = false;
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: "",
            detail: "unfollowed con éxito!"
          });
          this.getPlayers(this.page);
        },
        err => {
          this.msgs = [];
          this.msgs.push({ severity: "error", summary: "", detail: "Error" });
        }
      );
    } else {
      let url = this.global.base_path + "api/user/profile/?format=json";
      let data = { form_type: "follow_player", follow_player: player.id };
      this.global.PostRequest(url, data).subscribe(
        res => {
          player.follow = true;
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: "",
            detail: "seguido con éxito!"
          });
          this.getPlayers(this.page);
        },
        err => {
          this.msgs = [];
          this.msgs.push({ severity: "error", summary: "", detail: "Error" });
        }
      );
    }
  }
}
