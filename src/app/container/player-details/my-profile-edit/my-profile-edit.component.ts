import { Component, OnInit, ViewChild, NgZone, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Message, SelectItem } from 'primeng/primeng';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../GlobalService';
import { TranslateService } from "ng2-translate";
import { setTimeout } from 'timers';
import { countryCode } from "../../registration/onboading2/country_code"
import { Http } from '@angular/http';
declare var google;

@Component({
  selector: 'app-my-profile-edit',
  templateUrl: './my-profile-edit.component.html',
  styleUrls: ['./my-profile-edit.component.css']
})
export class MyProfileEditComponent implements OnInit {
  mask: any = '9';
  mask1: any = '999999999999999';
  @ViewChild('input1') first: any;
  @ViewChild('input2') second: any;
  @ViewChild('input3') third: any;
  @ViewChild('input4') fourth: any;
  d1;
  d2;
  d3;
  d4;
  mobile_verify: any = false;
  email_verify: any = false;
  country_name: any = "";
  loader: boolean = false;
  player_id: string;
  msgs: any;
  player_profile: any;
  player_bio: any;
  position_list: any[] = [];
  leg_list: any[] = [];
  height: any = '';
  height_list: any[] = [];
  gender_list: any[] = [];
  selected_position: any = null;
  selected_leg: any;
  date: any = new Date();

  en = {
    dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
  };
  es = {
    dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
    dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
    monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
    monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
  }
  langauge: any;
  dob: any;
  gender: any;
  citizenship: SelectItem[] = [];
  country_id: any;
  form: FormGroup;
  full_name_error: boolean;
  full_name_valid: boolean = true;
  check_user: boolean = true;
  user_exist: boolean;
  user_short_length: boolean;
  tagname_valid1: boolean = true;
  currentTagName: any = "";
  mobile: any = "";
  city: any = "";
  country: any = "";
  password: any = "";
  country_code: any;
  emailDisplay: boolean;
  locationDisplay: boolean;
  mobileDisplay: boolean;
  check_email: boolean;
  email_exist: boolean;
  email: any;
  otpDisplay: boolean;
  emailPasswordDisplay: boolean;
  show_password: boolean = false;
  password1: any;
  checkNumber: boolean;
  checkNumberErrorMsg: boolean;
  whichOtpPage: string;
  mobile1: any;
  codes: any[] = [];
  GoogleAutocomplete: any;
  autocompleteItems: any[];
  location: any;
  form1: FormGroup;
  count: number;
  constructor(
    private translate1: TranslateService,
    public router: Router,
    public http: Http,
    public ngzone: NgZone,
    public base_path_service: GlobalService,
    public fb: FormBuilder,
    public route: ActivatedRoute) {
    setInterval(() => {
      this.count--;
    }, 1000)
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.player_id = route.snapshot.paramMap.get('id');
    this.leg_list.push({ label: localStorage.getItem('language') == 'es' ? 'Izquierda' : 'Left footed', value: 'Izquierda' });
    this.leg_list.push({ label: localStorage.getItem('language') == 'es' ? 'Derecha' : 'Right footed', value: 'Derecha' });
    this.leg_list.push({ label: localStorage.getItem('language') == 'es' ? 'Ambidiestro' : 'Ambidextrous', value: 'Ambidiestro' });
    for (let i = 150; i <= 230; i++) {
      this.height_list.push({
        label: i + " " + "cm",
        value: i
      });
    }
    this.gender_list.push({ label: localStorage.getItem('language') == 'es' ? 'Masculino' : 'Male', value: 'Masculino' });
    this.gender_list.push({ label: localStorage.getItem('language') == 'es' ? 'Femenino' : 'Female', value: 'Femenino' });
    if (localStorage.getItem('language') == 'es') {
      this.langauge = this.es;
    }
    else {
      this.langauge = this.en
    }

    this.form = this.fb.group({
      fullname: [''],
      username: [''],
    })
    this.form1 = this.fb.group({
      about: [''],
      national_id: [''],
    })

    countryCode.map((val) => {
      this.codes.push({
        label: val.name + " " + "(" + val.dial_code + ")",
        value: val.dial_code
      })
    })
  }

  ngOnInit() {
    this.profileData();
    this.profileBioData();
    if (localStorage.getItem('language') != null) {
      this.translate1.use(localStorage.getItem('language'))
    }

  }

  ngAfterViewInit() {
    this.getPosition();
    this.getCitizenship();
  }

  profileFormEdit() {
    this.form.patchValue({
      fullname: this.player_profile.full_name,
      username: this.player_profile.tag_name,
      // location:this.player_profile.full_country
    })
    console.log("this form", this.form)
  }

  public profileData() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "user/viewProfile/?form_type=general&id=" + this.player_id + "&format=json";
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.player_profile = res[0].json.data;
      console.log("data in method =>>>>>>>>>>>>>>>>>>>>", this.player_profile)
      this.loader = false;
      this.profileFormEdit();
      this.currentTagName = this.player_profile.tag_name;
      // this.mobile = this.player_profile.mobile;
      this.country_code = this.player_profile.country_code;
      this.city = this.player_profile.city;
      this.country_name = this.player_profile.country;
      this.location = {
        description: this.player_profile.full_country
      }

    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', detail: 'Algunos errores!' });
    });
  }

  profileBioEdit() {
    this.form1.patchValue({
      about: this.player_bio.bio,
      national_id: this.player_bio.national_id,
    })
  }

  public profileBioData() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "user/viewProfile/?form_type=biography&id=" + this.player_id + "&format=json";
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.loader = false;
      this.player_bio = res[0].json.data;
      this.selected_position = this.player_bio.position_id;
      this.selected_leg = this.player_bio.leg;
      this.dob = this.player_bio.dob;
      this.height = this.player_bio.height;
      this.gender = this.player_bio.gender;
      this.country = this.player_bio.country_name;
      this.profileBioEdit();
      console.log("iddddddddddddddddddd", this.country_id, this.country_name);
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores!' });
    });
  }

  getPosition() {
    let url = this.base_path_service.base_path_api() + "user/position/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        let position_array = res[0].json;
        position_array.forEach(element => {
          this.position_list.push({
            label: localStorage.getItem('language') == 'es' ? element.position_name : element.position_name_en,
            value: element.id
          })
        });
        console.log("positions", this.position_list)
      });
  }

  getCitizenship() {
    let url = this.base_path_service.base_path_api() + "team/nationality?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        let data = res[0].json;
        new Promise((res, rej) => {
          data.forEach(element => {
            this.citizenship.push({
              label: element.country_name,
              value: element.id
            })
          });
          res();
        }).then(res => {
          this.country_id = this.player_bio.country_id;
        })
        console.log("city =>>>>>>>>>>>>>>>>>>>", this.citizenship)
      }, err => {
      });
  }

  validateFullName(event) {
    this.full_name_error = false;
    if (event.target.value.length > 40) {
      this.form.patchValue({
        fullname: event.target.value.slice(0, 40)
      })
    }
    if (/^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$/.test(event.target.value)) {
      this.full_name_valid = true;
    }
    else {
      this.full_name_valid = false;
    }
  }

  validateuser(event) {
    console.log("hiiiiiiiiiiiiii")
    let user = event.target.value;
    if (user.length > 5) {
      this.user_exist = false;
      this.user_short_length = false;
      if (/^(\@)?[A-Za-z0-9_]+$/i.test(user)) {
        this.tagname_valid1 = true
        this.check_user = true;
      }
      else {
        this.check_user = false;
        this.tagname_valid1 = false
      }
    }
    else {
      this.tagname_valid1 = true
      this.user_exist = false;
      this.check_user = false;
      this.user_short_length = true
    }
  }

  checkusernameexist() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "peloteando/verification/?tag_name=" + this.form.controls.username.value + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.updateProfileNow();
      }, err => {
        this.loader = false;
        this.user_exist = true;
        this.check_user = false;
      })
  }

  updateProfile() {
    if (this.full_name_valid) {
      if (this.check_user) {
        if (this.currentTagName == this.form.controls.username.value) {
          this.updateProfileNow();
        }
        else {
          this.checkusernameexist();
        }
      }
    }
    else {
      this.full_name_error = true;
    }
  }

  updateProfileNow() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "user/editMyProfile/?format=json";
    let data = {
      "form_type": "general",
      "full_name": this.form.controls.fullname.value,
      "tag_name": this.form.controls.username.value,
      "email": this.player_profile.email,
      "email_is_verified": this.email_verify, // GIVE TRUE WHEN USER HAS VERIFIED THE OTP
      "password": this.password1, //GIVE WHEN USER IS FILLING THE PASSWORD AND EMAIL FOR FIRST TIME.
      "mobile": this.mobile1,
      "mobile_is_verified": this.mobile_verify, //GIVE TRUE WHEN USER HAS VERIFIED THE OTP
      "latitude": this.player_profile.latitude,
      "longitude": this.player_profile.longitude,
      "city": this.city,
      "country": this.country_name,
      "country_code": this.country_code,
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        console.log("resposne after edited", res[0].json);
        this.msgs = [];
        this.msgs.push({
          severity: 'info',
          summary: localStorage.getItem('language') == 'es' ? "Actualización exitosa" : "Successfuly updated",
        });
        this.base_path_service.changeName.next(this.form.controls.fullname.value)
        localStorage.setItem('username', this.form.controls.fullname.value);
        localStorage.setItem('last_name', '');


      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

  back() {
    this.router.navigateByUrl('/dashboard/player/player-profile/' + this.player_id + '/information')
  }

  showDialog(check) {
    if (check == 'email') {
      this.emailDisplay = true;
    }
    else if (check == 'mobile') {
      this.mobileDisplay = true;
    }
    else if (check == 'location') {
      this.locationDisplay = true;
    }

  }

  validateEmail(event) {
    if (/[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+$/.test(this.email)) {
      this.check_email = true;
      this.email_exist = false;
    }
    else {
      this.check_email = false;
      this.email_exist = false;
    }
  }

  validateEmailExist() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "peloteando/verification/?email=" + this.email + "&format=json";
    this.http.get(url)
      .subscribe(res => {
        this.sendEmailOtp();
      }, err => {
        this.loader = false;
        this.email_exist = true;
        this.check_email = false;
      })
  }

  next() {
    this.validateEmailExist();
  }


  sendEmailOtp() {
    let url = this.base_path_service.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
    let data = {
      "form_type": "send_otp",
      "email": this.email,
      "register_type": "email",
    }
    this.base_path_service.PostRequestUnauthorised(url, data)
      .subscribe(res => {
        this.loader = false;
        // let data = {
        //   "page": "optional",
        //   "form_type": "email",
        //   "email": this.email,
        //   "skipped": false
        // }
        this.msgs = [];
        this.msgs.push({
          severity: 'success',
          summary: localStorage.getItem('language') == 'es' ? "OTP enviado" : "OTP sent",
        });
        this.emailDisplay = false;
        this.otpDisplay = true;
        this.count = 30;
        this.whichOtpPage = 'email'
        setTimeout(() => {
          this.first.inputViewChild.nativeElement.focus();
        }, 500)
      }, error => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }




  checkOtp(check, event) {
    console.log("event", event)
    this.ngzone.run(() => {
      if (event.keyCode >= 48 && event.keyCode <= 57) {
        if (check == 1) {
          this.second.inputViewChild.nativeElement.focus();
        }
        else if (check == 2) {
          this.third.inputViewChild.nativeElement.focus();
        }
        else if (check == 3) {
          this.fourth.inputViewChild.nativeElement.focus();
        }
      }
      else {
        return false
      }
    })
  }

  verifyOtp() {
    if (this.d1 && this.d2 && this.d3 && this.d4) {
      this.loader = true;
      let otp = this.d1 + "" + this.d2 + "" + this.d3 + "" + this.d4;
      let url = this.base_path_service.base_path_api() + "peloteando/mobile_otp/?format=json";
      let data: any;
      if (this.whichOtpPage == 'email') {
        data = {
          "email": this.email,
          "form_type": "verify_otp",
          "register_type": "email",
          "otp": parseInt(otp)
        }
      }
      else {
        data = {
          "phone": this.mobile,
          "country_code": this.country_code,
          "form_type": "verify_otp",
          "register_type": "phone",
          "otp": parseInt(otp)
        }
      }
      console.log("data", data)
      this.base_path_service.PostRequestUnauthorised(url, data)
        .subscribe(res => {
          console.log("otp verify=>>>>>>>>>>>>>>>>>");
          this.otpDisplay = false;
          this.loader = false;
          if (this.whichOtpPage == 'email') {
            if (this.player_profile.password) {
              this.email_verify = true;
              this.player_profile.email = this.email;
              this.email = '';
              this.d1 = '';
              this.d2 = '';
              this.d3 = '';
              this.d4 = '';
              this.check_email = false;
            }
            else {
              this.emailPasswordDisplay = true;
            }
          }
          else {
            this.d1 = '';
            this.d2 = '';
            this.d3 = '';
            this.d4 = '';
            this.checkNumber = false;
            this.mobile_verify = true;
            this.player_profile.full_mobile = this.country_code + "-" + this.mobile;
            this.mobile1 = this.mobile;
            this.mobile = ''
          }
        }, err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({
            severity: 'error',
            summary: (localStorage.getItem('language') == 'es' ? "Ingrese OTP correcta" : "Enter correct OTP"),
          });
        })
    }
  }




  resendOtp() {
    if (this.count <= 0) {
      this.loader = true;
      let url = this.base_path_service.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
      let data: any;
      if (this.whichOtpPage == 'email') {
        data = {
          "form_type": "send_otp",
          "email": this.email,
          "register_type": "email",
        }
      }
      else {
        data = {
          "phone": this.mobile,
          "country_code": this.country_code,
          "form_type": "send_otp",
          "register_type": "phone",
        }
      }


      this.base_path_service.PostRequestUnauthorised(url, data)
        .subscribe(res => {
          this.loader = false;
          this.count = 30;
          this.msgs = [];
          this.msgs.push({
            severity: 'success',
            summary: localStorage.getItem('language') == 'es' ? "OTP enviado" : "OTP sent",
          });
        }, error => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({
            severity: 'error',
            summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
          });

        })
    }
  }

  setPassword() {
    if (this.password.length >= 6) {
      this.getToken();
    }
    else {
      this.msgs = [];
      this.msgs.push({
        severity: 'error',
        summary: (localStorage.getItem('language') == 'es' ? "La contraseña debe ser mayor a 5 caracteres" : "Password must be greater than 5 character"),
      });

    }
  }

  getToken() {
    this.loader = true;
    let data = {
      "page": "optional",
      "form_type": "email",
      "email": this.email,
      "skipped": false,
      'password': this.password,
    }
    let url1 = this.base_path_service.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.base_path_service.PostRequest(url1, data)
      .subscribe(res => {
        this.ngzone.run(() => {
          this.loader = false;
          this.emailPasswordDisplay = false;
          this.email_verify = true;
          this.player_profile.email = this.email;
          this.email = '';
          this.password1 = this.password;
          this.password = '';
          this.d1 = '';
          this.d2 = '';
          this.d3 = '';
          this.d4 = '';
          this.check_email = false;

        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

  validatePhone(event) {
    let result = event.target.value;
    if (result.length >= 8) {
      this.checkNumber = true;
      this.checkNumberErrorMsg = false;
    }
    else {
      this.checkNumber = false;
      this.checkNumberErrorMsg = false;
    }
  }

  checkNumberExist() {
    this.loader = true
    let url = this.base_path_service.base_path_api() + "peloteando/verification/?mobile=" + parseInt(this.mobile) + "&country_code=" + this.country_code + "&format=json";
    this.http.get(url)
      .subscribe(res => {
        this.checkNumberErrorMsg = false;
        this.sendMobileOtp();
      }, err => {
        this.loader = false;
        this.checkNumberErrorMsg = true;
        this.checkNumber = false;
      })
  }

  sendMobileOtp() {
    let url = this.base_path_service.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
    let data = {
      "phone": this.mobile,
      "country_code": this.country_code,
      "form_type": "send_otp",
      "register_type": "phone",
    }
    this.base_path_service.PostRequestUnauthorised(url, data)
      .subscribe(res => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'success',
          summary: localStorage.getItem('language') == 'es' ? "OTP enviado" : "OTP sent",
        });
        this.mobileDisplay = false;
        this.otpDisplay = true;
        this.count = 30;
        this.whichOtpPage = 'mobile';
        setTimeout(() => {
          this.first.inputViewChild.nativeElement.focus();
        }, 500)
      }, error => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

  search(event) {
    console.log("valussssssssss", event)
    this.GoogleAutocomplete.getPlacePredictions({ input: event.query, types: ['(regions)'] },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.ngzone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
          console.log("list", this.autocompleteItems)
        });
      });
  }

  setLoaction(item) {
    console.log("item=>>>>>>>>>>>>>>>>", item);
    this.locationDisplay = false;
    this.city = item.terms[0].value;
    this.country_name = item.terms.pop().value;
    // this.player_profile.full_country = item.description;
    this.geoCode((item.description));

  }

  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.player_profile.latitude = results[0].geometry.location.lat()
      this.player_profile.longitude = results[0].geometry.location.lng()
    })
  }

  updateBio() {
    this.loader = true;
    let date = new Date(this.dob).toISOString().slice(0, 10)
    console.log(" new Date(this.dob).toISOString().slice(0,10)", new Date(this.dob).toISOString().slice(0, 10))
    let url = this.base_path_service.base_path_api() + "user/editMyProfile/?format=json";
    let data = {
      "form_type": "biography",
      "nationality_id": this.country_id,
      "leg": this.selected_leg,
      "position_id": this.selected_position,
      "height": this.height,
      "dob": new Date(this.dob).toISOString().slice(0, 10),
      "gender": this.gender,
      "bio": (this.form1.controls.about.value).trim(),
      "national_id": (this.form1.controls.national_id.value).trim(),
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'info',
          summary: localStorage.getItem('language') == 'es' ? "Actualización exitosa" : "Successfuly updated",
        });
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error,Try again later",
        });
      })

  }

}
