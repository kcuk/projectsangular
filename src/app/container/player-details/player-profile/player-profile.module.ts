import { NgModule } from '@angular/core';
import { PlayerProfileComponent } from './player-profile.component';
import { playerProfileRouting } from './player-profile.routes';
import { SharedModule } from './../../../shared/shared.module';
import { InformationComponent } from './information/information.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { RequestComponent } from './request/request.component';
import { CanchasComponent } from './canchas/canchas.component';
import { EquiposComponent } from './equipos/equipos.component';
import { ReserveinfoComponent } from './reserveinfo/reserveinfo.component';
import { TorneosComponent } from './torneos/torneos.component';
import { ShareButtonsModule } from 'ngx-sharebuttons';
import { UserPaymentComponent } from './user-payment/user-payment.component';
import { SavedCardsComponent } from './saved-cards/saved-cards.component';
import { UserPaymentDetailsComponent } from './user-payment-details/user-payment-details.component';
import { StatisticComponent } from './statistic/statistic.component';
import { MyScheduleComponent } from './my-schedule/my-schedule.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ProfilehistoryComponent } from "./profilehistory/profilehistory.component";



@NgModule({
  imports: [
    playerProfileRouting, ImageCropperModule,
    ShareButtonsModule.forRoot(),
    SharedModule.forRoot()
  ],
  declarations: [PlayerProfileComponent,ProfilehistoryComponent, InformationComponent,ScheduleComponent, RequestComponent, CanchasComponent, EquiposComponent, ReserveinfoComponent, TorneosComponent, UserPaymentComponent, SavedCardsComponent, UserPaymentDetailsComponent,
    StatisticComponent,
    MyScheduleComponent,
     ]
})
export class PlayerProfileModule { }
