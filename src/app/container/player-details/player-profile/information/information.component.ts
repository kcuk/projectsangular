import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';
@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {

  public player_detail: any[];
  public cover_pic: any[];
  public profile_pic: string;
  public shoutouts: any[];
  public players;
  public age: number = null;
  public attitute: number = 0;
  public technique: number = 0;
  public attitute1: number = 0;
  public technique1: number = 0;
  public player_id: any;
  public date: Date;
  public match_played: number;
  public total_goal: number;
  public tournament_played: number;
  public shoutout;
  public backdrop: boolean = false;
  public loader: boolean = false;
  public admin: boolean = false;
  public ratingpopup: boolean = false;
  public attitute_data: any;
  public technique_data: any;
  public msgs: Message[] = [];
  public total_attitute: number = 0;
  public total_technique: number = 0;
  data: any;
  logo = this.base_path_service.image_url;
  lang = localStorage.getItem('language');
  constructor(public router: Router, public base_path_service: GlobalService, public route: ActivatedRoute) {
    this.date = new Date();
  }

  ngOnInit() {

    let sub = this.route.parent.params.subscribe(params => {
      this.player_id = +params['id'];
    });
    console.log("information id =>>>>>>>>>>>>>>", this.player_id)
    this.loader = true;
    this.get_data();
  }
  public get_data() {
    let url = this.base_path_service.base_path_api() + "user/viewProfile/?form_type=biography&id=" + this.player_id + "&format=json";
    // let url1 = this.base_path_service.base_path_api() + 'user/player/?form_type=information&player=' + this.player_id + '&format=json';
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.loader = false;
      this.data = res[0].json.data;
      // this.admin = res[0].json.admin;
      // this.attitute = res[0].json.average_attitude;
      // this.technique = res[0].json.average_technique;
      // this.match_played = res[0].json.match_played;
      // this.tournament_played = res[0].json.tournament_played;
      // this.total_goal = res[0].json.total_goal;
      // this.shoutout = res[0].json.player_shoutout;
      // this.players = res[0].json.basic_detail;
      // this.total_attitute = res[0].json.total_attitude_rating;
      // this.total_technique = res[0].json.total_technique_rating;
      // this.attitute1 = res[0].json.user_attitude;
      // this.technique1 = res[0].json.user_technique;
      // if (res[0].json.basic_detail.dob != null)
      //   this.age = this.date.getFullYear() - new Date(res[0].json.basic_detail.dob).getFullYear();
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores!' });
    });
  }
  public rating_popup() {
    this.backdrop = true;
    this.ratingpopup = true;
  }
  public cancel() {
    this.backdrop = false;
    this.ratingpopup = false;
  }
  public technique_rating() {
    this.technique_data = {
      "form_type": "technique",
      "player": this.player_id,
      "technique": this.technique1
    }
    let url = this.base_path_service.base_path_api() + 'user/rating/?format=json';
    this.base_path_service.PostRequest(url, this.technique_data)
      .subscribe(res => {
        if (res[0].status == 200 || res[0].status == 201 || res[0].status == 205) {
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: '', detail: 'Técnica Actualizado con éxito!' });
          this.get_data();
        }
      }, err => {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores!' });
      });
  }
  public attitute_rating() {
    this.attitute_data = {
      "form_type": "attitude",
      "player": this.player_id,
      "attitude": this.attitute1
    }
    let url = this.base_path_service.base_path_api() + 'user/rating/?format=json';
    this.base_path_service.PostRequest(url, this.attitute_data)
      .subscribe(res => {
        if (res[0].status == 200 || res[0].status == 201 || res[0].status == 205) {
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: '', detail: 'Attitute Actualizado con éxito!' });
          this.technique_rating();
        }
      }, err => {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores!' });
      });
  }
  public rating() {
    this.loader = true;
    this.attitute_rating();
    this.backdrop = false;
    this.ratingpopup = false;
  }

  navigate_to_shoutout() {
    this.router.navigateByUrl('/dashboard/shoutouts/' + this.player_id + '/player')
  }
}