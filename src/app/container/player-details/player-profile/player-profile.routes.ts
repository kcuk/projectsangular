import { ModuleWithProviders } from "@angular/core";
import { RouterModule } from "@angular/router";
import { InformationComponent } from "./information/information.component";
import { PlayerProfileComponent } from "./player-profile.component";
// import { ScheduleComponent } from './schedule/schedule.component';
import { RequestComponent } from "./request/request.component";
import { CanchasComponent } from "./canchas/canchas.component";
import { EquiposComponent } from "./equipos/equipos.component";
import { ReserveinfoComponent } from "./reserveinfo/reserveinfo.component";
import { TorneosComponent } from "./torneos/torneos.component";
import { UserPaymentComponent } from "./user-payment/user-payment.component";
import { SavedCardsComponent } from "./saved-cards/saved-cards.component";
import { UserPaymentDetailsComponent } from "./user-payment-details/user-payment-details.component";
import { StatisticComponent } from "./statistic/statistic.component";
import { MyScheduleComponent } from "./my-schedule/my-schedule.component";
import { ProfilehistoryComponent } from "./profilehistory/profilehistory.component";

export const playerProfileRoutes = [
  {
    path: "",
    component: PlayerProfileComponent,
    children: [
      { path: "", component: InformationComponent },
      { path: "information", component: InformationComponent },
      { path: "schedule", component: MyScheduleComponent },
      { path: "requests", component: RequestComponent },
      { path: "torneos", component: TorneosComponent },
      { path: "canchas", component: CanchasComponent },
      { path: "equipos", component: EquiposComponent },
      { path: "reserve", component: ReserveinfoComponent },
      { path: "user-payments", component: UserPaymentComponent },
      { path: "saved-card", component: SavedCardsComponent },
      {
        path: "user-payment-details/:id",
        component: UserPaymentDetailsComponent
      },
      { path: "statistic", component: StatisticComponent },
      { path: "history", component: ProfilehistoryComponent }
    ]
  }
];

export const playerProfileRouting: ModuleWithProviders = RouterModule.forChild(
  playerProfileRoutes
);
