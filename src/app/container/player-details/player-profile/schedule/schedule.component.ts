import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../../GlobalService';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  events: any[];
  header: any;
  public playerId: number = 0;
  public imageUrl: string = '';
  public validDate: any;
  public timeFormat: any;
  public defaultView: any;
  public selected_link: string = 'semana';
  public selected_sort_by: string = 'week';
  public player_scheduleInfo: any;
  public match_Info_popup: boolean = false;
  public reservationPopup: boolean = false;
  public match_info: any;
  public match_list: any;
  public matchPopup: boolean = false;
  public allDaySlot: boolean = false;
  lang: any

  constructor(public base_path_service: GlobalService, public route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    if (localStorage.getItem('language')) {
      this.lang = localStorage.getItem('language');
      console.log("ptttttttttttt",this.lang)

    }

    let id = this.route.parent.params.subscribe(params => {
      this.playerId = +params['id'];
    });
    this.imageUrl = this.base_path_service.image_url;
    let a = new Date();
    this.validDate = a.toISOString().substring(0, 10);
    this.header = {
      left: 'prev,next',
      center: 'title',
      right: 'agendaWeek,agendaDay'
    };
    this.timeFormat = 'hh:mm A',
      this.defaultView = 'agendaWeek';
    this.events = [];
    this.playerScheduleInfo();
  }
  public sortBy(day: string, ref) {
    if (day == 'day') {
      ref.changeView('agendaDay');
      this.selected_sort_by = 'day';
      this.events = [];
    } else {
      ref.changeView('agendaWeek');
      this.selected_sort_by = 'week';
      this.events = [];
    }
    this.playerScheduleInfo();
  }
  public previousDate(ref) {
    ref.prev();
    this.validDate = ref.getDate()._d.toISOString().substring(0, 10);
    this.events = [];
    this.playerScheduleInfo();

  }
  public nextDate(ref) {
    ref.next();
    this.validDate = ref.getDate()._d.toISOString().substring(0, 10);
    this.events = [];
    this.playerScheduleInfo();
  }
  public playerScheduleInfo() {
    this.events = [];
    let url = this.base_path_service.base_path_api() + `user/playerSchedule/?date=${this.validDate}&form_type=${this.selected_sort_by}&format=json`;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.player_scheduleInfo = res[0].json;
        if (this.player_scheduleInfo) {
          for (var index = 0; index < this.player_scheduleInfo.length; index++) {
            if (this.player_scheduleInfo[index].form_type == 'reservation') {
              this.events.push({
                "title": "Reservas",
                "start": this.player_scheduleInfo[index].match_date_time,
                "end": this.player_scheduleInfo[index].end_date_time,
                "backgroundColor": "darkcyan",
                "className": "moreBorder",
                "id": this.player_scheduleInfo[index].id,
                "form_type": 'reservation'
              })
            } else if (this.player_scheduleInfo[index].form_type == 'tournament') {
              this.events.push({
                "title": "Torneo",
                "start": this.player_scheduleInfo[index].match_date_time,
                "end": this.player_scheduleInfo[index].end_date_time,
                "backgroundColor": "#FFDC7A",
                "className": "moreBorder",
                "id": this.player_scheduleInfo[index].id,
                "form_type": 'tournament'
              })

            } else if (this.player_scheduleInfo[index].form_type == 'tournament_referee') {
              this.events.push({
                "title": "Torneo arbitro",
                "start": this.player_scheduleInfo[index].match_date_time,
                "end": this.player_scheduleInfo[index].end_date_time,
                "backgroundColor": "#9FDDF1",
                "className": "moreBorder",
                "id": this.player_scheduleInfo[index].id,
                "form_type": 'tournament_referee'
              })
            } else {
              this.events.push({
                "title": "Partido",
                "start": this.player_scheduleInfo[index].match_date_time,
                "end": this.player_scheduleInfo[index].end_date_time,
                "backgroundColor": "##EF6262",
                "className": "moreBorder",
                "id": this.player_scheduleInfo[index].id,
                "form_type": 'match'
              })

            }
          }
        }
      }, err => {
        this.base_path_service.print(err);
      })
  }

  handleEventClick(event) {
    let id = event.calEvent._id;
    let form_type = event.calEvent.form_type;
    this.playerProfileInfo(id, form_type)
  }
  public playerProfileInfo(id: number, form_type: any) {
    this.match_info = [];
    this.match_list = [];
    this.reservationPopup = false;
    this.matchPopup = false;
    let url = this.base_path_service.base_path_api() + `user/matchdetail/?form_type=${form_type}&id=${id}&format=json`;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.match_info = res[0].json[0];
        this.match_list = res[0].json;
        this.match_Info_popup = true;
        if (form_type === 'tournament') {
          this.matchPopup = true;
        } else if (form_type === 'reservation') {
          this.reservationPopup = true;
        }
      })
  }
  public redirectProfile(id: number) {
    this.router.navigateByUrl('dashboard/ground/ground-profile/' + id);
  }
  public redirectProfileTeam(id: number) {
    this.router.navigateByUrl('dashboard/team/team-profile/' + id);
  }
}


