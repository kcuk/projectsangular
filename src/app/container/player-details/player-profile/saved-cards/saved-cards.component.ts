import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'ng2-translate';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../../../GlobalService';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-saved-cards',
  templateUrl: './saved-cards.component.html',
  styleUrls: ['./saved-cards.component.css']
})
export class SavedCardsComponent implements OnInit {
  card_failed: boolean;
  card_added: boolean;
  loader1: boolean;
  redirect: any;
  card_data: { "success_url": string; "failure_url": string; };
  msgs: any[];
  results: any[];
  loader: boolean=false;
  id: any;

  constructor(private translate: TranslateService, public router: Router, public base_path_service: GlobalService,
    public route: ActivatedRoute, public _fb: FormBuilder) {
    let url1 = window.location.href
    this.route.queryParams.subscribe(param => {
      let status = param['status'];
      if (status != undefined) {
        this.loader = true;
        this.loader1 = true;
        if (status === '1') {
          let redirect = url1.replace('status=1', 'status=true');
          window.parent.location.href = redirect;
        }
        else if (status === '2') {
          let redirect = url1.replace('status=2', 'status=false');
          window.parent.location.href = redirect;
        }
        else if (status === 'true') {
          this.card_added = true;
          this.loader1 = false;
        }
        else if (status === 'false') {
          this.card_failed = true;
          this.loader1 = false;
        }
      }
    })
    let url = this.base_path_service.base_path_api() + "ground/saveCardDetails/";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
      })
  }

  ngOnInit() {
    this.route.parent.params.subscribe(res => {
      this.id = res['id'];
    })
    this.getCardList();
  }

  getCardList() {
		this.loader = true;
		let url = this.base_path_service.base_path_api() + 'user/listCards/';
		this.base_path_service.GetRequest(url)
			.subscribe(res => {
				this.loader = false;
				this.results = [];
				console.log("carddddddddddddddd", res[0].json.data)
				this.results = res[0].json.data;
			}, err => {
				this.loader = false
			})
	}
 
  removeCard(card, ind) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'user/deleteCard/';
    let data = {
      "card": card
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.msgs = [];
        this.getCardList();
        this.msgs.push({ severity: 'info', detail: "Card removed succefully" });
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: err.json().error });
      })
  }



  addCard() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'ground/field_add_card/';
    this.card_data = {
      "success_url": this.base_path_service.payment_path + "/dashboard/player/player-profile/" + this.id + "/saved-card?status=1",
      "failure_url": this.base_path_service.payment_path + "/dashboard/player/player-profile/" + this.id + "/saved-card?status=2"
    }
    this.base_path_service.PostRequest(url, this.card_data)
      .subscribe(res => {
        this.redirect = res[0].json;
        // window.location.href = this.redirect;
        localStorage.setItem('url', this.redirect);
        this.router.navigateByUrl('dashboard/ground/payment');
      }, err => {
        this.loader = false;
      })
  }

}
