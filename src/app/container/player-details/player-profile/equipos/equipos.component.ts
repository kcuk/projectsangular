import { Component, OnInit } from '@angular/core';
import {Message,SelectItem} from 'primeng/primeng';
import {Route,Router,ActivatedRoute} from '@angular/router';
import { GlobalService } from './../../../../GlobalService';


@Component({
  selector: 'app-equipos',
  templateUrl: './equipos.component.html',
  styleUrls: ['./equipos.component.css']
})
export class EquiposComponent implements OnInit {
  team: Array<any> = [];
  tournament: Array<any> = [];
  ground: Array<any> = [];
  player_id: number;
  status: string = "pending";
  referee: any;
  requestList: Array<any> = [];
  invitedList: any;
  loader: boolean = false;
  team1: boolean = false;
  tournament1: boolean = false;
  ground1: boolean = false;
  msgs: Message[] = [];
  list: SelectItem[];
  cancelPopup: boolean = false;
  imageBasepath: string = "";

  constructor(public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {

    this.imageBasepath = this.base_path_service.image_url;
    this.list = [];
    this.list.push({ label: 'seleccionar', value: null });
    this.list.push({ label: 'cancelar', value: 'delete' });
    this.loader = true;
    let sub = this.route.parent.params.subscribe(params => {
      this.player_id = +params['id'];
    });
    this.pendingRequest();
  }

  pendingRequest() {
    this.loader = true;
    this.tournament = []
    this.ground = []
    this.status = "pending";

    let url = this.base_path_service.base_path + "api/user/posesion/?form_type=team&player=" + this.player_id + '&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.team = res[0].json;
        this.loader = false;
      })
  }
  edit(pk) {
    if (this.status == 'pending') {
      this.router.navigateByUrl('/dashboard/team/edit-team/' + pk)
    }
    else if (this.status == 'approved') {
      this.router.navigateByUrl('/dashboard/tournaments/round-details/' + pk)
    }
    else if (this.status == 'invited') {
      this.router.navigateByUrl('/dashboard/ground/ground-profile/edit-ground/' + pk)
    }
  }

  see_more(id) {
    this.router.navigateByUrl('/dashboard/team/team-profile/' + id);
  }

}
