import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'ng2-translate';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../../../GlobalService';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-user-payment-details',
  templateUrl: './user-payment-details.component.html',
  styleUrls: ['./user-payment-details.component.css']
})
export class UserPaymentDetailsComponent implements OnInit {
  data_not_found: boolean;
  results: any;
  id: number;

  constructor(private translate: TranslateService, public router: Router, public base_path_service: GlobalService,
    public route: ActivatedRoute, public _fb: FormBuilder) { }

  ngOnInit() {
    this.route.params.subscribe(res => {
      console.log("ressssssssssssssss",res)
      this.id = res.id;
    })
    this.getPaymentDetails(this.id);
  }

  getPaymentDetails(id) {
    let url = this.base_path_service.base_path_api() + "user/myPayments/" + id+"/";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.results = res[0].json;
        console.log("results=>>>>>>>>>>", this.results)
      })
  }

  
}
