import {
  Component,
  OnInit,
  HostListener,
  keyframes,
  trigger,
  state,
  animate,
  transition,
  style
} from "@angular/core";
import {
  Validators,
  FormBuilder,
  FormControl,
  FormGroup,
  NgForm,
  AbstractControl
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { GlobalService } from "../../../../GlobalService";
import {
  Button,
  Growl,
  Message,
  InputTextarea,
  Dropdown,
  SelectItem,
  AutoComplete,
  InputText
} from "primeng/primeng";
import { FooterComponent } from "../../../../container/footer/footer.component";
import { TranslateService } from "ng2-translate";

@Component({
  selector: "app-profilehistory",
  templateUrl: "./profilehistory.component.html",
  styleUrls: ["./profilehistory.component.css"]
})
export class ProfilehistoryComponent implements OnInit {
  leaguearray: any = [];
  leaguedata: any;
  msgs: Message[] = [];
  basePath: string = "";
  playerId: number;
  league: any = "";
  seasonsection: boolean = false;
  historydata: any;
  nodta: boolean = false;

  seasondata: any;
  sessonarray: any = [];
  season: any = "";
  logo = this.base_path_service.image_url;
  constructor(
    private translate: TranslateService,
    public router: Router,
    public base_path_service: GlobalService,
    form: FormBuilder,
    public route: ActivatedRoute,
    public activatedRoute: ActivatedRoute
  ) {
    this.route.parent.params.subscribe(params => {
      this.playerId = +params["id"];
    });
  }

  ngOnInit() {
    this.getleagues();
    this.gethistory();
    this.basePath = this.base_path_service.image_url;
  }
  getleagues() {
    var url =
      this.base_path_service.base_path +
      "api/user/history_filter/?player_id=" +
      this.playerId +
      "&form_type=player&sort_type=league";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        console.log(res, "res=================");
        this.leaguedata = res[0].json.leagues;
        this.leaguedata.forEach(element => {
          this.leaguearray.push({
            label: element.league_name,
            value: element.league_id
          });
        });
        console.log(this.leaguearray, "leaguearray==============leaguedata===");
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }

  leagueselect(event) {
    this.sessonarray = [];
    console.log("leagure event", event);
    this.getseasons();
    this.seasonsection = true;
  }
  seasonselect(event) {
    this.gethistory();
  }
  getseasons() {
    var url =
      this.base_path_service.base_path +
      "api/user/history_filter/?player_id=" +
      this.playerId +
      "&form_type=player&sort_type=season&league_id=" +
      this.league;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        console.log(res, "res=================");
        this.seasondata = res[0].json.seasons;
        this.seasondata.forEach(element => {
          this.sessonarray.push({
            label: element.season,
            value: element.season
          });
        });
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }

  gethistory() {
    let data = {
      player_id: this.playerId,
      league: this.league,
      season: this.season
    };
    console.log(data, "ddtato send===========================");

    var url = this.base_path_service.base_path + "api/user/player_history/";
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        console.log(res, "res for history faya=================");
        this.historydata = res[0].json;
        console.log(this.historydata, "this.historydata");
        if (this.historydata.length == 0) {
          this.nodta = true;
        } else {
          this.nodta = false;
        }
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }
  reset() {
    this.seasonsection = false;
    this.league = "";
    this.season = "";
    this.sessonarray = [];
    this.gethistory();
  }
}
