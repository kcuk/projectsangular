import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilehistoryComponent } from './profilehistory.component';

describe('ProfilehistoryComponent', () => {
  let component: ProfilehistoryComponent;
  let fixture: ComponentFixture<ProfilehistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilehistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilehistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
