import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../../GlobalService';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-my-schedule',
  templateUrl: './my-schedule.component.html',
  styleUrls: ['./my-schedule.component.css']
})
export class MyScheduleComponent implements OnInit {
  matches: any[] = [];
  reservations: any[] = [];
  lang: string;
  logo: any = this.base_path_service.image_url;
  loader: boolean = true;
  show: any = "matches";
  msgs:any;
  constructor(public base_path_service: GlobalService, public route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    let url = this.base_path_service.base_path_api() + "user/viewProfile/?form_type=schedule&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.matches = res[0].json.matches;
        this.reservations = res[0].json.reservations;
        console.log("schedule =>>>>>>>>>>>>>>>>..", res[0].json);
      }, err => {
        this.loader = false;
      })
  }

  gotoReservation(id) {

  }


}
