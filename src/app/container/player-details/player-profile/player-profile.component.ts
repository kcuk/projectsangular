import { Component, OnInit, ViewChild, NgZone } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { Http } from "@angular/http";
import { Router, ActivatedRoute } from "@angular/router";
import { ImageCropperComponent, CropperSettings } from "ng2-img-cropper";
import { Message } from "primeng/primeng";
import { GlobalService } from "./../../../GlobalService";
import * as html2canvas from "html2canvas";
import { TranslateService } from "ng2-translate";

declare const Canvas2Image: any;
@Component({
  selector: "app-player-profile",
  templateUrl: "./player-profile.component.html",
  styleUrls: ["./player-profile.component.css"]
})
export class PlayerProfileComponent implements OnInit {
  public loader: boolean = false;
  public info: boolean = false;
  public requestoptions: any;
  public players: any[];
  public player_detail: any;
  public cover_pic: string;
  public profile_pic: string;
  public shoutouts: any[];
  public admin: boolean = true;
  public player_id: any;
  public logo: string = "";
  public player_logo: any;
  public age: number;
  public enable: boolean = false;
  public display: boolean = false;
  public mssge: FormGroup;

  public page: number;
  public team_id: number = null;
  public team_page: number;
  public msgs: Message[] = [];
  public backdrop: boolean = false;
  public player_data: any;
  public creat_selection: string = "";
  public create: any[];
  public regDetails;
  public invitePopup: boolean = false;
  public registerPopup: boolean = false;
  public basepath: string;
  public confirmpopup: boolean = false;
  public file_name: string;
  public image_name: string = "";

  public coverPic: any;
  public cover_popup: boolean = false;
  public coverPicList: any[] = [];
  public cover_url: string;
  public cover_id: number = null;
  public count: number = 0;
  public reply_msgs: string;
  public mesgs: boolean = false;
  public circleImg: any;
  public squareImg: any;
  public data: any;
  public data2: any;
  public twitterUrl: string = "";
  public url: string = "";
  public shareimgURL: string = "";

  public cropperSettings: CropperSettings;
  public cropperSettings2: CropperSettings;
  public share_profole_image: string = "";
  public downloadProfile: boolean = false;
  public downloadDataInfo: any;
  public imageUrl: any;
  public attitude: number = 0;
  public techniq: number = 0;
  payment_team: boolean = false;

  @ViewChild("cropper", undefined)
  cropper: ImageCropperComponent;
  @ViewChild("layout")
  canvasRef;
  imageChangedEvent: any;
  croppedImage: string;
  constructor(
    public zone: NgZone,
    private translate: TranslateService,
    public fb: FormBuilder,
    public router: Router,
    public http: Http,
    public base_path_service: GlobalService,
    public route: ActivatedRoute
  ) {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.width = 200;
    this.cropperSettings.height = 200;
    this.cropperSettings.keepAspect = false;
    this.cropperSettings.croppedWidth = 200;
    this.cropperSettings.croppedHeight = 200;
    this.cropperSettings.canvasWidth = 300;
    this.cropperSettings.canvasHeight = 300;
    this.cropperSettings.minWidth = 100;
    this.cropperSettings.minHeight = 100;
    this.cropperSettings.rounded = true;
    this.cropperSettings.minWithRelativeToResolution = false;
    this.cropperSettings.cropperDrawSettings.strokeColor =
      "rgba(255,255,255,1)";
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;

    this.data = {};

    this.mssge = fb.group({
      replymsgs: new FormControl("", Validators.compose([Validators.required]))
    });

    this.create = [];
    this.create.push({ label: "CREAR", value: "" });
    this.create.push({ label: "EQUIPO", value: "team" });
    this.create.push({ label: "TORNEO", value: "tournament" });
    this.create.push({ label: "CANCHA", value: "soccer_ground" });
    if (localStorage.getItem("language")) {
      translate.use(localStorage.getItem("language"));
    }
  }
  ngOnInit() {
    this.router.events.subscribe(evt => {
      document.body.scrollTop = 0;
    });
    this.basepath = this.base_path_service.image_url;
    let sub = this.route.params.subscribe(params => {
      this.player_id = +params["id"];
      console.log("player_id ", this.player_id);
      this.get_data();
    });
    var tempUrl = window.location.href;
    this.shareimgURL = tempUrl;
    var arrUrl = tempUrl.split("/");
    var temp = arrUrl[arrUrl.length - 1];
    var currentUrl = temp;
    console.log("currentUrl", currentUrl);
    console.log("information id =>>>>>>>>>>>>>>", this.player_id);
    if (Number.isInteger(parseInt(currentUrl))) {
      this.info = true;
    }
    this.team_page = 1;
    this.page = 1;
  }
  public get_data() {
    if (this.count != 0) {
      this.count = this.count + 1;
    } else {
      this.loader = true;
    }
    let url =
      this.base_path_service.base_path_api() +
      "user/viewProfile/?form_type=general&id=" +
      this.player_id +
      "&format=json";
    // let url = this.base_path_service.base_path_api() + 'user/player/?form_type=profile&player=' + this.player_id + '&format=json';
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.player_detail = res[0].json.data;
        console.log("data =>>>>>>>>>>>>>>>>>>>>", this.player_detail.data);
        this.profile_pic = this.player_detail.profile_pic;
        this.cover_pic = res[0].json.cover_pic;
        this.admin = this.player_detail.admin;
        this.share_profole_image =
          this.basepath + "/" + res[0].json.share_profile_link;
        console.log("hello ", this.share_profole_image);
        this.loader = false;
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: "error", detail: "Algunos errores!" });
      }
    );
  }

  togallery() {
    let comdata = JSON.parse(localStorage.getItem("community_info"));
    console.log(comdata, "fgewguigewewrgwue");
    let comname = comdata.community_name;
    console.log(comname, "community name we get======================");
    // this.router.navigate([comname]);
    this.router.navigateByUrl(comname + "/gallery");
  }

  public routing() {
    this.loader = true;
    this.router.navigate([
      "/dashboard/player/edit-profile",
      { id: this.player_id }
    ]);
    this.loader = false;
  }

  public player_followers() {
    if (this.player_detail.follower > 0) {
      this.router.navigateByUrl(
        "/dashboard/followers?id=" + this.player_id + "&status=player"
      );
    }
  }

  public player_followings() {
    if (this.player_detail.following > 0) {
      this.router.navigateByUrl(
        "/dashboard/followings?id=" + this.player_id + "&status=player"
      );
    }
  }

  public follow_player_profile() {
    this.loader = true;
    let url =
      this.base_path_service.base_path + "api/user/profile/?format=json";
    this.player_data = {
      form_type: "follow_player",
      follow_player: this.player_id
    };
    this.base_path_service.PostRequest(url, this.player_data).subscribe(
      res => {
        this.loader = false;
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: "",
            detail: "Seguido con Ã©xito!"
          });
          this.get_data();
        }
      },
      err => {
        this.msgs = [];
        this.msgs.push({ severity: "error", detail: "Algunos errores!" });
        this.loader = false;
      }
    );
  }

  public unfollow_player_profile() {
    this.loader = true;
    var url =
      this.base_path_service.base_path_api() +
      "user/player_follow/" +
      this.player_id +
      "/?format=json";
    this.base_path_service.DeleteRequest(url).subscribe(
      res => {
        this.loader = false;
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: "",
            detail: "no seguir con Ã©xito!"
          });
          this.get_data();
        }
      },
      err => {
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Some error!"
        });
        this.loader = false;
      }
    );
  }

  public invite_player() {
    this.loader = true;
    this.invitePopup = true;
    this.enable = true;
    this.loader = false;
  }
  public registerPopupWindow() {
    this.loader = true;
    let url =
      this.base_path_service.base_path_api() +
      "team/profile/?value=myteam&page=" +
      this.team_page +
      "&player=" +
      this.player_id +
      "&format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.regDetails = res[0].json.teams;
        this.registerPopup = true;
        this.loader = false;
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: "error", detail: "Algunos errores!" });
      }
    );
  }

  public getChallengeId(id) {
    this.team_id = id;
  }
  public confirm() {
    if (this.team_id != null) {
      this.registerPopup = false;
      this.confirmpopup = true;
    } else {
      this.msgs = [];
      this.msgs.push({
        severity: "error",
        summary: "",
        detail: "Â¡SelecciÃ³n invalida!"
      });
    }
  }

  public invite() {
    this.loader = true;
    this.confirmpopup = false;
    this.backdrop = false;
    let url =
      this.base_path_service.base_path_api() +
      "user/player_request/?format=json";
    let data = {
      player: this.player_id,
      team: this.team_id
    };
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        this.loader = false;
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          this.team_id = null;
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: "",
            detail: "Invite con Ã©xito!"
          });
        } else {
          if (res[0].status == 400) {
            this.msgs = [];
            this.msgs.push({
              severity: "error",
              summary: "",
              detail: "Equipo mal!"
            });
          } else {
            this.msgs = [];
            this.msgs.push({
              severity: "error",
              summary: "",
              detail: "Algunos errores!"
            });
          }
        }
      },
      err => {
        this.msgs = [];
        this.msgs.push({ severity: "error", detail: "Algunos errores!" });
        this.loader = false;
      }
    );
  }

  public regPopupClose() {
    this.loader = true;
    this.backdrop = false;
    this.team_id = null;
    this.registerPopup = false;
    this.enable = false;
    this.loader = false;
  }

  public creat() {
    if (this.creat_selection == "team") {
      this.router.navigateByUrl("/dashboard/team/create-team");
    } else if (this.creat_selection == "tournament") {
      this.router.navigateByUrl("/dashboard/tournaments/create-tournament");
    } else if (this.creat_selection == "soccer_ground") {
      this.router.navigateByUrl("/dashboard/ground/create-ground");
    }
  }

  public cover_dialog() {
    this.cover_popup = true;
    this.API_getCoverImages();
  }

  public API_getCoverImages() {
    this.loader = true;
    let url =
      this.base_path_service.base_path + "api/user/cover_pic/?format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.coverPicList = res[0].json;
        this.cover_url = this.base_path_service.image_url;
        this.loader = false;
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Algunos errores!"
        });
      }
    );
  }

  get_cover(id) {
    if (this.cover_id == id) {
      this.cover_id = 0;
    }
    {
      this.cover_id = id;
    }
  }

  public disable_cover() {
    this.cover_popup = false;
    this.backdrop = false;
  }

  public save_cover() {
    this.loader = true;
    let data = {
      form_type: "cover_pic",
      cover_pic: this.cover_id
    };
    let url =
      this.base_path_service.base_path_api() +
      "user/player/" +
      this.player_id +
      "/?format=json";
    this.base_path_service.PutRequest(url, data).subscribe(
      res => {
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          this.cover_id = null;
          this.cover_popup = false;
          this.backdrop = false;
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: "Actualizado Exitosamente",
            detail: ""
          });
          this.get_data();
        }
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Por favor seleccione Imagen de portada!"
        });
      }
    );
  }

  /*** image cropper ***/

  public fileChangeListener($event) {
    var image: any = new Image();
    var file: File = $event.target.files[0];
    var myReader: FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function(loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
      that.logo = image.src.split("data:image/jpeg;base64,")[1];
    };
    myReader.readAsDataURL(file);
    this.showDialog();
  }

  public showDialog() {
    this.display = true;
  }

  // public save() {
  //   this.backdrop = false;
  //   this.display = false;
  //   this.loader = true;
  //   let obj = JSON.parse(localStorage.getItem("userInfo")).token.access_token;
  //   let url = this.base_path_service.base_path_api() + 'user/player/' + this.player_id + '/?format=json';
  //   return new Promise((resolve, reject) => {
  //     var formData: any = new FormData();
  //     var xhr = new XMLHttpRequest();
  //     formData.append("form_type", "profile_pic");
  //     formData.append("profile_pic", this.logo);
  //     xhr.onreadystatechange = () => {
  //       if (xhr.readyState == 4) {
  //         if (xhr.status == 205 || xhr.status == 200 || xhr.status == 201) {
  //           this.msgs = [];
  //           this.msgs.push({ severity: 'info', summary: 'Actualizado Exitosamente', detail: '' });
  //           localStorage.setItem("profile_pic", JSON.parse(xhr.response)['profile_pic']);
  //           this.base_path_service.parentChildFun();
  //           this.get_data();
  //         } else {
  //           if (xhr.status == 400) {
  //             this.loader = false;
  //             this.msgs = [];
  //             this.msgs.push({ severity: 'error', summary: '', detail: 'pic no vÃ¡lido' });
  //           } else {
  //             this.loader = false;
  //             this.msgs = [];
  //             this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores!' });
  //           }
  //           reject(xhr.response);
  //         }
  //       }
  //     }
  //     xhr.open("PUT", url, true);
  //     xhr.setRequestHeader("Authorization", 'Bearer ' + obj);
  //     xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
  //     xhr.send(formData);
  //   });
  // }

  // public disable() {
  //   this.backdrop = false;
  //   this.logo = '';
  //   this.display = false;
  // }

  public remove_img($event) {}
  /*** image cropper ***/
  public showmsgs() {
    this.mesgs = true;
    this.reply_msgs = "";
  }

  public save_msgs() {
    this.mesgs = false;
    this.backdrop = false;
  }

  public navigate_msgs() {
    console.log("go to message page!");
    this.router.navigateByUrl("/dashboard/messages/" + this.player_id);
  }

  public send_msg() {
    this.loader = true;
    let data = {
      form_type: "p2p",
      message: this.reply_msgs,
      player: this.player_id
    };
    let url = this.base_path_service.base_path_api() + "user/message/";
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: "",
            detail: "Â¡Mensaje enviado exitosamente!"
          });
          this.mesgs = false;
          this.backdrop = false;
          this.loader = false;
        }
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Algunos errores para Enviar!"
        });
      }
    );
  }
  downloadImage() {
    this.loader = true;
    let url =
      this.base_path_service.base_path_api() +
      `user/playerProfile/?id=${this.player_id}&format=json`;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.downloadDataInfo = res[0].json;
        this.attitude = res[0].json.attitude;
        this.techniq = res[0].json.technique;
        this.downloadProfile = true;
        this.loader = false;
        setTimeout(() => {
          this.drawImage();
        }, 1000);
      },
      err => {
        this.loader = false;
      }
    );
  }
  downloadAsImage() {
    this.downloadImage();
  }

  drawImage() {
    let canvas = this.canvasRef.nativeElement;
    html2canvas(canvas, {
      onrendered: canvas => {
        this.imageUrl = canvas.toDataURL("image/png");
      }
    });
  }
  convertImageToDataUri(url, callback) {
    var xhr = new XMLHttpRequest();
    this.http.get(url).subscribe(res => {
      console.log("response", res);
    });
    xhr.onload = function() {
      var reader = new FileReader();
      reader.onloadend = function() {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };

    xhr.open("GET", url, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader(
      "Access-Control-Allow-Origin",
      "http://localhost:4200/"
    );
    xhr.setRequestHeader(
      "accept-language",
      localStorage.getItem("language") == "en" ? "en" : "es"
    );
    xhr.responseType = "blob";
    xhr.send();
  }

  completeProfile() {
    this.loader = true;
    let url =
      this.base_path_service.base_path_api() +
      "user/completeProfile/?format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.loader = false;
        localStorage.setItem("checkoptionalpage", "complete_profile");
        localStorage.setItem("user_info", JSON.stringify(res[0].json.data));
        let user_info = JSON.parse(localStorage.getItem("user_info"));
        if (!user_info.is_phone_no) {
          this.router.navigate(["/home/signup/set-phone"]);
        } else if (!user_info.is_email) {
          this.router.navigate(["/home/signup/set-email"]);
        } else if (!user_info.is_username) {
          this.router.navigate(["/home/signup/set-user"]);
        } else if (!user_info.is_gender) {
          this.router.navigate(["/home/signup/set-gender"]);
        } else if (!user_info.is_height) {
          this.router.navigate(["/home/signup/set-height"]);
        } else if (!user_info.is_dob) {
          this.router.navigate(["/home/signup/set-dob"]);
        } else if (!user_info.is_leg) {
          this.router.navigate(["/home/signup/set-leg"]);
        } else if (!user_info.is_position) {
          this.router.navigate(["/home/signup/set-position"]);
        } else if (!user_info.is_profile_pic) {
          this.router.navigate(["/home/signup/set-profile-pic"]);
        }
        // else{
        //   this.router.navigateByUrl('dashboard/player/player-profile/' + this.player_id + '/information');
        // }
      },
      err => {
        this.loader = false;
      }
    );
  }

  fileChangeEvent(event: any): void {
    this.display = true;
    console.log("display=>>>>>>>>>>>>>>>>>>", this.display);
    this.imageChangedEvent = event;
    console.log("cropeed", this.imageChangedEvent);
  }

  imageCropped(image: string) {
    this.croppedImage = image;
    console.log("cropeed", this.croppedImage);
  }

  disable() {
    this.display = false;
    this.croppedImage = "";
    // this.backdrop = false;
  }

  save() {
    this.display = false;
    // this.crop_img = this.croppedImage;
    this.setProfile();
  }

  setProfile() {
    this.loader = true;
    let data = {
      profile_raw_data: this.croppedImage,
      page: "profile_pic",
      skipped: false,
      request_from: "edit_profile"
    };
    let url =
      this.base_path_service.base_path_api() +
      "peloteando/mobile_signup/?format=json";
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        this.loader = false;
        this.zone.run(() => {
          this.player_detail.profile_complete =
            res[0].json.data.profile_percentage;
          this.profile_pic = res[0].json.data.profile_pic;
        });
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary:
            localStorage.getItem("language") == "es"
              ? "Error, inténtalo de nuevo más tarde"
              : "Error, Try again later"
        });
      }
    );
  }
}
