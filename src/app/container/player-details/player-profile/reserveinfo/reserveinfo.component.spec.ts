import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReserveinfoComponent } from './reserveinfo.component';

describe('ReserveinfoComponent', () => {
  let component: ReserveinfoComponent;
  let fixture: ComponentFixture<ReserveinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReserveinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReserveinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
