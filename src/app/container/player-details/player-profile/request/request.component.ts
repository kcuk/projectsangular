import { Component, OnInit } from "@angular/core";
import { Message } from "primeng/primeng";
import { Router, Route, ActivatedRoute } from "@angular/router";
import { GlobalService } from "./../../../../GlobalService";

class selectedItem {
  label;
  value;
}

@Component({
  selector: "app-request",
  templateUrl: "./request.component.html",
  styleUrls: ["./request.component.css"]
})
export class RequestComponent implements OnInit {
  public activeLink: boolean = true;
  public loader: boolean = false;
  public request: boolean;
  public accept: boolean = true;
  public basePath;
  public flag: boolean;
  public admin: any;
  public tid: number;
  public url: any;
  public msgs: Message[];
  public type: string = "accepted";
  public actions: selectedItem[];
  public selectedAction: number = null;
  public playerlist: any[];
  public player_team_list: any[];
  public team_player_list: any[];
  public team_manager_list: any[];
  public team_referee_list: any[];
  public tournament_list: any[];
  public ground_booking: any[];

  public data: any;
  public action: string = "";
  cities1: any = [];
  teamfilterdata: any;
  teamarray: any = [];
  selectedid: any='';
  constructor(
    public router: Router,
    public base_path_service: GlobalService,
    public route: ActivatedRoute
  ) {
    //   this.teamfilterdata = [
    //     {label:'Select City', value:null},
    //     {label:'New York', value:{id:1, name: 'New York', code: 'NY'}},
    //     {label:'Rome', value:{id:2, name: 'Rome', code: 'RM'}},
    //     {label:'London', value:{id:3, name: 'London', code: 'LDN'}},
    //     {label:'Istanbul', value:{id:4, name: 'Istanbul', code: 'IST'}},
    //     {label:'Paris', value:{id:5, name: 'Paris', code: 'PRS'}}
    // ];

    this.actions = [];
    this.actions.push({ label: "ACCIÓN", value: null }),
      this.actions.push({ label: "Aceptar", value: 1 }),
      this.actions.push({ label: "Negar", value: 0 });
  }

  ngOnInit() {
    // this.base_path_service.playerProfile.next("requests");
    let sub = this.route.parent.params.subscribe(params => {
      this.tid = +params["id"];
    });
    this.basePath = this.base_path_service.image_url;
    this.approved();
    this.getteamlist();
  }
  getteamlist() {
    let url = this.base_path_service.base_path + "api/user/team_list/";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.teamfilterdata = res[0].json;
        console.log(this.teamfilterdata);

        this.teamfilterdata.forEach(element => {
          this.teamarray.push({
            label: element.team_name,
            value: element.team_id
          });
        });
      },
      err => {
        // this.loader = false;
      }
    );
  }
  teamselectfilter(event) {
    this.selectedid = event.value;
    console.log(event, "event value============>>>>>>>>>");
    this.getTeam("accepted");
  }

  public getTeam(type: string) {
    this.loader = true;
    this.request = false;
    this.accept = false;
    this.tournament_list = [];
    if (type == "accepted") {
      this.accept = true;
      this.url =
        this.base_path_service.base_path +
        "api/user/player_request/?form_type=request_received&team_id=" +
        this.selectedid +
        "&format=json";
      this.base_path_service.GetRequest(this.url).subscribe(
        res => {
          this.loader = false;
          this.playerlist = res[0].json.player_list;
          this.team_player_list = res[0].json.team_player_list;
          this.team_manager_list = res[0].json.manager_list;
          this.team_referee_list = res[0].json.team_referee_list;
          this.tournament_list = res[0].json.tournament_list;
        },
        err => {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Algunos errores!"
          });
          this.loader = false;
          this.base_path_service.print("some error in getting details");
        }
      );
    } else if (type == "requested") {
      this.request = true;
      this.url =
        this.base_path_service.base_path +
        "api/user/player_request/?form_type=request_sent&format=json";
      this.base_path_service.GetRequest(this.url).subscribe(
        res => {
          this.loader = false;
          this.player_team_list = res[0].json.player_team_list;
          this.tournament_list = res[0].json.tournament_list;
          this.ground_booking = res[0].json.ground_booking;
        },
        err => {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Algunos errores!"
          });
          this.loader = false;
          this.base_path_service.print("some error in getting details");
        }
      );
    }
  }
  public requested() {
    this.type = "requested";
    this.activeLink = false;
    this.flag = false;
    this.getTeam(this.type);
  }
  public approved() {
    this.type = "accepted";
    this.activeLink = true;
    this.flag = true;
    this.getTeam(this.type);
  }

  public team_player_post(id) {
    if (this.selectedAction == 1) this.action = "accepted";
    else if (this.selectedAction == 0) this.action = "deny";
    this.selectedAction = null;
    let url = this.base_path_service.base_path + "api/user/status/?format=json";
    this.data = {
      form_type: "team",
      team: id,
      status: this.action
    };
    this.base_path_service.PostRequest(url, this.data).subscribe(
      res => {
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          if (this.action == "accepted") {
            this.msgs = [];
            this.msgs.push({
              severity: "info",
              summary: "",
              detail: "Successfully accepted"
            });
          } else if (this.action == "deny") {
            this.msgs = [];
            this.msgs.push({
              severity: "info",
              summary: "",
              detail: "Successfully denied"
            });
          }
          this.loader = false;
          this.approved();
        }
      },
      err => {
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Algunos errores!"
        });
        this.loader = false;
        this.base_path_service.print("some error in deleting details");
      }
    );
  }

  public player_post(comid, playerid) {
    if (this.selectedAction == 1) this.action = "accepted";
    else if (this.selectedAction == 0) this.action = "deny";
    this.selectedAction = null;
    let url =
      this.base_path_service.base_path +
      "api/user/community_request/?format=json";
    this.data = {
      community_id: comid,
      player_id: playerid,
      status: this.action
    };
    this.base_path_service.PostRequest(url, this.data).subscribe(
      res => {
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          if (this.action == "accepted") {
            this.msgs = [];
            this.msgs.push({
              severity: "info",
              summary: "",
              detail: "Successfully accepted"
            });
          } else if (this.action == "deny") {
            this.msgs = [];
            this.msgs.push({
              severity: "info",
              summary: "",
              detail: "Successfully denied"
            });
          }
          this.loader = false;
          this.approved();
        }
      },
      err => {
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Algunos errores!"
        });
        this.loader = false;
        this.base_path_service.print("some error in deleting details");
      }
    );
  }

  public team_referee_post(id) {
    if (this.selectedAction == 1) this.action = "accepted";
    else if (this.selectedAction == 0) this.action = "deny";
    this.selectedAction = null;
    let url = this.base_path_service.base_path + "api/user/status/?format=json";
    this.data = {
      form_type: "match",
      match: id,
      status: this.action
    };
    this.base_path_service.PostRequest(url, this.data).subscribe(
      res => {
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          if (this.action == "accepted") {
            this.msgs = [];
            this.msgs.push({
              severity: "info",
              summary: "",
              detail: "Successfully accepted"
            });
          } else if (this.action == "deny") {
            this.msgs = [];
            this.msgs.push({
              severity: "info",
              summary: "",
              detail: "Successfully denied"
            });
          }
          this.loader = false;
          this.approved();
        }
      },
      err => {
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Algunos errores!"
        });
        this.loader = false;
        this.base_path_service.print("some error in deleting details");
      }
    );
  }
  public tournament_post(id) {
    if (this.selectedAction == 1) this.action = "approved";
    else if (this.selectedAction == 0) this.action = "remove";
    this.selectedAction = null;
    let url = this.base_path_service.base_path + "api/user/status/?format=json";
    this.data = {
      form_type: "tournament",
      tournament: id,
      status: this.action
    };
    this.base_path_service.PostRequest(url, this.data).subscribe(
      res => {
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          if (this.action == "approved") {
            this.msgs = [];
            this.msgs.push({
              severity: "info",
              summary: "",
              detail: "Successfully apprroved"
            });
          } else if (this.action == "remove") {
            this.msgs = [];
            this.msgs.push({
              severity: "info",
              summary: "",
              detail: "Successfully removed"
            });
          }
          this.loader = false;
          this.approved();
        }
      },
      err => {
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Algunos errores!"
        });
        this.loader = false;
        this.base_path_service.print("some error in deleting details");
      }
    );
  }

  public team(id) {
    this.router.navigateByUrl("/dashboard/team/team-profile/" + id);
  }

  public tournament(id) {
    this.router.navigateByUrl(
      "/dashboard/tournaments/tournament-profile/" + id
    );
  }

  public cancel_tournament(id) {
    this.loader = true;
    this.data = {
      form_type: "cancel",
      tournament: id
    };
    let url = this.base_path_service.base_path + "api/user/status/?format=json";
    this.base_path_service.PostRequest(url, this.data).subscribe(
      res => {
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: "",
            detail: "Successfully Cancelled"
          });
          this.get_team();
        }
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Algunos errores!"
        });
      }
    );
  }

  public cancel_team(id) {
    this.loader = true;
    this.data = {
      form_type: "cancel",
      team: id
    };
    let url = this.base_path_service.base_path + "api/user/status/?format=json";
    this.base_path_service.PostRequest(url, this.data).subscribe(
      res => {
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: "",
            detail: "Successfully Cancelled"
          });
          this.get_team();
        }
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Algunos errores!"
        });
      }
    );
  }

  public ground(court_id, id) {
    localStorage.setItem("court_id", court_id);
    this.router.navigateByUrl("/dashboard/ground/ground-profile/" + id);
  }

  public cancel_ground(id) {
    this.loader = true;
    this.data = {
      form_type: "cancel",
      ground: id
    };
    let url = this.base_path_service.base_path + "api/user/status/?format=json";
    this.base_path_service.PostRequest(url, this.data).subscribe(
      res => {
        if (
          res[0].status == 200 ||
          res[0].status == 201 ||
          res[0].status == 205
        ) {
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            summary: "",
            detail: "Successfully Cancelled"
          });
          this.get_team();
        }
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Algunos errores!"
        });
      }
    );
  }

  public get_team() {
    this.url =
      this.base_path_service.base_path +
      "api/user/player_request/?form_type=request_sent&format=json";
    this.base_path_service.GetRequest(this.url).subscribe(
      res => {
        this.loader = false;
        this.player_team_list = res[0].json.player_team_list;
        this.tournament_list = res[0].json.tournament_list;
        this.ground_booking = res[0].json.ground_booking;
      },
      err => {
        this.loader = false;
        this.base_path_service.print("some error in getting details");
      }
    );
  }
}
