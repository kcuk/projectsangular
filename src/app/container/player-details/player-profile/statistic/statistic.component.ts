import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../../GlobalService';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})
export class StatisticComponent implements OnInit {
  loader: boolean;
  data: any;
  msgs:any;
  playerId: number;

  constructor(public base_path_service: GlobalService, public route: ActivatedRoute, public router: Router) { 
    this.route.parent.params.subscribe(params => {
      this.playerId = +params['id'];
    });
  }

  ngOnInit() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "user/viewProfile/?form_type=stats&id="+this.playerId+"&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.data = res[0].json.data;
        console.log("statistics", this.data);
      }, err => {
        this.loader = false;
      })
  }

}
