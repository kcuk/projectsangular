import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'ng2-translate';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../../../GlobalService';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-user-payment',
  templateUrl: './user-payment.component.html',
  styleUrls: ['./user-payment.component.css']
})
export class UserPaymentComponent implements OnInit {
  data_not_found: boolean;
  total_pages: any;
  results: any=[];
  id: any;
  page: any = 1;

  constructor(private translate: TranslateService, public router: Router, public base_path_service: GlobalService,
    public route: ActivatedRoute, public _fb: FormBuilder) { }

  ngOnInit() {
    this.route.parent.params.subscribe(res => {
      this.id = res['id'];
    })
    this.getPaymentList(this.page);
  }

  getPaymentList(page) {
    let url = this.base_path_service.base_path_api() + "user/myPayments/?page=" + page;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        Array.prototype.push.apply(this.results, res[0].json.data);
        this.total_pages = res[0].json.total_pages
        if (this.results.length == 0) {
          this.data_not_found = true;
        }
        else {
          this.data_not_found = false;
        }
      })
  }

  public onScroll() {
    this.page = this.page + 1;
    if (this.page <= this.total_pages) {
      this.getPaymentList(this.page);
    }

  }

}
