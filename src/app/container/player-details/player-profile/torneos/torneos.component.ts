import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/primeng';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';

@Component({
  selector: 'app-torneos',
  templateUrl: './torneos.component.html',
  styleUrls: ['./torneos.component.css']
})
export class TorneosComponent implements OnInit {

  team: Array<any> = [];
  tournament: Array<any> = [];
  ground: Array<any> = [];
  player_id: number;
  status: string = "pending";
  referee: any;
  requestList: Array<any> = [];
  invitedList: any;
  loader: boolean = false;
  team1: boolean = false;
  tournament1: boolean = false;
  ground1: boolean = false;
  msgs: Message[] = [];
  imageBasePath: string = "";
  constructor(public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute) {
  }



  ngOnInit() {
    this.base_path_service.playerProfile.next("torneos");
    this.imageBasePath = this.base_path_service.image_url;
    this.loader = true;
    let sub = this.route.parent.params.subscribe(params => {
      this.player_id = +params['id'];

    });
    this.approvedRequest();
  }

  approvedRequest() {
    this.loader = true;
    this.team = []
    this.ground = []
    this.status = "approved";

    let url = this.base_path_service.base_path + "api/user/posesion/?form_type=tournament&player=" + this.player_id + '&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.tournament = res[0].json;
        this.loader = false;
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores!' });
      })
  }
  public redirectProfile(id: number) {
    this.router.navigateByUrl('dashboard/tournaments/tournament-profile/' + id);
  }
}
