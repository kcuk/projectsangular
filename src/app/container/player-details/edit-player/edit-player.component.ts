import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Message, SelectItem } from 'primeng/primeng';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../GlobalService';
import { TranslateService } from "ng2-translate";

@Component({
    selector: 'app-edit-player',
    templateUrl: './edit-player.component.html',
    styleUrls: ['./edit-player.component.css']
})
export class EditPlayerComponent implements OnInit {
    check_city: boolean = true;

    public loader: boolean = false;
    public player_id;
    public msgs: Message[] = [];
    public zoneName: any;
    public zoneList: SelectItem[];
    public filteredLocation: any[];
    public completeProfileForm: FormGroup;
    public profileDataIns;
    public genderList;
    public positionList: Array<any> = [];
    public prefferedLegList;
    public prefLeg: string = "derecho";
    public countryList;
    public country: number;
    public country_required: boolean = false;
    public positionName: string;
    public prefLegName: string;
    public genderName: string;
    public countryName: string;
    public city_id: number;
    public filteredTown: any[];
    public city_required: boolean = false;
    public referee;
    public refereeName;
    public detail: any;
    public es: any;
    public city_empty: boolean = false;
    public emailExists: boolean = false;
    public error: boolean = false;
    public country_code: string = "+593";
    public initialEmail: string = "";
    public deactiveAccount: boolean = false;
    public refree_value: boolean = false;
    public location: any;
    code: any = [
        { value: '+593', label: '+593' },
        { value: '+1', label: '+1' }
    ];
    length: any;
    lang = localStorage.getItem('language')

    constructor(private translate1: TranslateService, public router: Router, public base_path_service: GlobalService, public route: ActivatedRoute) {
        if (localStorage.getItem('language') != null) {
            translate1.use(localStorage.getItem('language'))
        }
        this.profileDataIns = new profileData();
        this.completeProfileForm = new FormGroup({
            dob: new FormControl('', Validators.compose([])),
            gender: new FormControl('', Validators.compose([Validators.required])),
            Apellido: new FormControl('', Validators.compose([Validators.maxLength(50)])),
            nick_name: new FormControl('', Validators.compose([Validators.maxLength(50)])),
            name: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(50)])),
            mobile: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
            price: new FormControl('', Validators.compose([Validators.maxLength(6), Validators.pattern('[0-9]+')])),
            position: new FormControl('', Validators.compose([Validators.required])),
            bio: new FormControl('', Validators.compose([])),
            country: new FormControl('', Validators.compose([Validators.required])),
            city: new FormControl('', Validators.compose([Validators.required])),
            pref_leg: new FormControl('', Validators.compose([Validators.required])),
            is_professional: new FormControl('', Validators.compose([])),
            email: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+')])),
            country_code: new FormControl('')
        })

        this.genderList = [
            {
                "value": "",
                "label": "seleccionar género"
            },
            {
                "value": "Masculino",
                "label": "Masculino"
            },
            {
                "value": "Femenino",
                "label": "Femenino"
            }
        ]

        this.referee = [
            {
                "value": null,
                "label": "seleccionar Refree"
            },
            {
                "value": true,
                "label": "SÃƒÂ­"
            },
            {
                "value": false,
                "label": "No"
            }
        ]


        this.prefferedLegList = [
            {
                "value": null,
                "label": "seleccionar pref"
            },
            {
                "value": 'Izquierda',
                "label": "Izquierda"
            },
            {
                "value": 'Derecha',
                "label": "Derecha"
            },
            {
                "value": "Ambidiestro",
                "label": "Ambidiestro"
            }
        ]
    }

    ngOnInit() {
        this.es = { closeText: "Cerrar", prevText: "", currentText: "Hoy", monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"], monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"], dayNames: ["domingo", "lunes", "martes", "miÃƒÂ©rcoles", "jueves", "viernes", "sÃƒÂ¡bado"], dayNamesShort: ["dom", "lun", "mar", "miÃƒÂ©", "jue", "vie", "sÃƒÂ¡b"], dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"], weekHeader: "Sm", dateFormat: "yy-mm-dd", firstDay: 1, isRTL: false, showMonthAfterYear: false, yearSuffix: "" };

        let sub = this.route.params.subscribe(params => {
            this.player_id = +params['id'];
        });
        this.loader = true;
        this.API_getCountryList();
        this.API_getPositionList();
        this.positionName = "Seleccione";
        this.genderName = "Seleccione";
        this.prefLegName = "Seleccione";
        this.countryName = "Seleccione";
        this.refereeName = "Seleccione";

        let url3 = this.base_path_service.base_path_api() + 'user/player/' + this.player_id + '/?format=json';
        this.base_path_service.GetRequest(url3)
            .subscribe(res => {
                this.completeProfileForm.patchValue({
                    'name': res[0].json.username.name,
                    'dob': res[0].json.dob,
                    'bio': res[0].json.bio,
                    'Apellido': res[0].json.username.last_name,
                    'nick_name': res[0].json.nick_name,
                    'gender': res[0].json.gender,
                    'is_professional': res[0].json.is_professional,
                    'pref_leg': res[0].json.leg,
                    'position': res[0].json.username.position_pk,
                    'country': res[0].json.city.country_pk,
                    'city': { id: res[0].json.city.city_pk, location: res[0].json.city.city },
                    'mobile': res[0].json.username.mobile,
                    'email': res[0].json.username.email,
                    'country_code': res[0].json.username.country_code,
                    'price': res[0].json.price
                })
                if (res[0].json.username.country_code == '+593') {
                    this.length = 9
                }
                else {
                    this.length = 10;
                }
                this.country = res[0].json.city.country_pk;
                this.city_id = res[0].json.city.city_pk;
                this.loader = false;
            });
    }

    public API_getPositionList() {
        this.positionList.push({ "label": "seleccionar posición", "value": null })
        var url = this.base_path_service.base_path_api() + "user/position/?format=json"
        this.base_path_service.GetRequest(url)
            .subscribe(res => {
                for (let i = 0; i < res[0].json.length; i++) {
                    this.positionList.push({ "label": res[0].json[i].position_name, "value": res[0].json[i].id })
                }
            },
            err => {
            })
    }

    public API_getCountryList() {
        this.countryList = [];
        this.countryList.push({ "label": "seleccionar Pias", "value": null });
        var url = this.base_path_service.base_path_api() + "team/country/?format=json";
        this.base_path_service.GetRequestUnauthorised(url)
            .subscribe(
            res => {
                for (let i = 0; i < res[0].json.length; i++) {
                    this.countryList.push({ "label": this.lang == 'en' ? res[0].json[i].country_name : res[0].json[i].country_spanish, "value": res[0].json[i].id })
                }
            },
            err => {
            })
    }

    public filterLocation(event) {
        let query = event.query;
        this.check_city = false;

        if (this.country != undefined || this.country != null) {
            this.country_required = false;
            var url = this.base_path_service.base_path_api() + "team/location/?country=" + this.country + "&city=" + query + '&format=json';
            this.base_path_service.GetRequest(url)
                .subscribe(
                res => {
                    this.filteredLocation = this.filterCountry(query, res[0].json);
                },
                err => {
                    this.msgs = [];
                    this.msgs.push({ severity: 'error', summary: '', detail: 'algunos de error' });
                });
        } else {
            this.country_required = true;
        }
    }

    public filterCountry(query, location: any[]): any[] {
        let filtered: any[] = [];
        for (let i = 0; i < location.length; i++) {
            let city = location[i];
            if (city.location.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(city);
            }
        }
        return filtered;
    }

    public filterTown(event) {
        let query = event.query;
        if (this.city_id != undefined) {
            this.city_required = false;
            if (query.length >= 2) {
                var url = this.base_path_service.base_path_api() + "team/location/?form_type=town&city_pk=" + this.city_id + "&town=" + query + '&format=json';
                this.base_path_service.GetRequest(url)
                    .subscribe(
                    res => {
                        this.filteredTown = res[0].json;
                    },
                    err => {
                        this.msgs = [];
                        this.msgs.push({ severity: 'error', summary: '', detail: 'algunos de error' });
                    });
            }
        } else {
            this.city_required = true;
        }
    }

    public datepicker(event) {
        this.profileDataIns.dob = event;
    }

    public selectedGenderFun(event) {
        this.profileDataIns.gender = event.value;
    }

    public selectedPositionFun(event) {
        this.profileDataIns.position = event.value;
    }

    public selectedPrefLegFun(event) {
        this.profileDataIns.pref_leg = event.value;
    }

    public selectedCountryFun(event) {
        this.completeProfileForm.patchValue({ 'city': '' })
        if (this.country != event.value) {
            this.country = event.value;
            this.country_required = false;
            this.profileDataIns.city = null;
            this.city_id = null;
        }
    }

    public selectedRefereeFun(event) {
        this.profileDataIns.is_referee = event.value;
    }

    public getCheckboxValue(event) {
    }

    public getLocation(event) {
        this.check_city = true
        if (this.city_id != event.id) {
            this.profileDataIns.city = event.location;
            this.city_id = event.id;
            this.city_required = false;
            this.profileDataIns.town = null;
        }
    }

    public getTown(event) {
        this.profileDataIns.town = event.town_name;
        this.profileDataIns.town_id = event.id;
    }
    public getStyle() {
    }
    formatDate(event) {
        if (event && event.toString().length > 10) {
            var validDate;
            let month = parseInt(event.getMonth());
            month = month + 1
            if (event.getMonth() > 8) {
                if (event.getDate() > 9) {
                    validDate = event.getFullYear() + "-" + month + "-" + event.getDate();
                } else {
                    validDate = event.getFullYear() + "-" + month + "-" + '0' + event.getDate();
                }

            } else {
                if (event.getDate() > 9) {
                    validDate = event.getFullYear() + "-" + '0' + month + "-" + event.getDate();
                } else {
                    validDate = event.getFullYear() + "-" + '0' + month + "-" + '0' + event.getDate();
                }
            }
            return validDate;
        } else {
            return event;
        }
    }
    public saveDetails(data) {
        if (this.completeProfileForm.valid && this.check_city) {
            this.loader = true;
            if (data.is_professional == false || data.price == null) {
                data.price = 0;
            }
            if (data.is_referee == false || data.referee_price == null) {
                data.referee_price = 0;
            }
            this.detail = {
                "ssn": 'PRAVEEN',
                "name": data.name,
                "last_name": data.Apellido,
                "position": data.position,
                "mobile": data.mobile,
                "price": 0,
                "nick_name": data.nick_name,
                "dob": this.formatDate(data.dob),
                "email": data.email,
                "gender": data.gender,
                "city": this.city_id,
                "bio": data.bio,
                "leg": data.pref_leg,
                "is_professional": false,
                "country_code": data.country_code
            }
            var url = this.base_path_service.base_path_api() + "user/player/" + this.player_id + "/?format=json";
            this.base_path_service.PutRequest(url, this.detail)
                .subscribe(
                res => {
                    if (res[0].status == 205 || res[0].status == 200 || res[0].status == 201) {
                        this.loader = false;
                        this.msgs = [];
                        this.msgs.push({ severity: 'info', summary: '', detail: 'Actualizado exitosamente!' });
                        this.base_path_service.childParentFun();
                        localStorage.setItem("username", JSON.stringify(res[0].json.json().name));
                        localStorage.setItem("nick_name", JSON.stringify(res[0].json.json().nick_name));
                        localStorage.setItem("last_name", JSON.stringify(res[0].json.json().last_name));
                        this.base_path_service.childParentFun();
                        this.router.navigateByUrl('dashboard/player/player-profile/' + this.player_id)
                    }
                },
                err => {
                    this.loader = false;
                    this.msgs = [];
                    this.msgs.push({ severity: 'error', detail: 'algunos de error' });
                })
        } else {
            this.msgs = [];
            this.msgs.push({ severity: 'error', detail: 'Ingresa todos los campos obligatorios' });
        }

    }
    public cancel() {
        this.loader = true;
        this.router.navigateByUrl('dashboard/player/player-profile/' + this.player_id);
        this.loader = false;
    }

    public get_email(event) {
        this.emailExists = false;
        this.error = false;
        if (this.completeProfileForm.controls['email'].valid && this.profileDataIns.email != this.initialEmail) {
            let val = event.target.value;
            if (val.length == 0) {
                this.error = true;
            } else {
                let url = this.base_path_service.base_path + "api/peloteando/verification/?email=" + val + "&format=json";
                this.base_path_service.GetRequest(url)
                    .subscribe(res => {

                    }, err => {
                        if (err.status == 404 || err.status == 403 || err.status == 400) {
                            this.emailExists = false;
                            this.error = true;
                            this.msgs = [];
                            this.msgs.push({ severity: 'error', detail: 'some error' });
                        }
                        else {
                            this.emailExists = true;
                        }
                    });
            }
        }

    }
    public deactiveAccountInfo() {
        let url = this.base_path_service.base_path + "api/user/account/?format=json";
        this.base_path_service.PostRequest(url, '')
            .subscribe(res => {
                this.msgs.push({ severity: 'info', detail: 'Tu cuenta se ha desactivado correctamente.' });
                localStorage.clear();
                this.router.navigate(['../home/login']);
            })
    }
    public refreeValidation() {
        var temp = this.completeProfileForm.value;
    }


    validMobile(event) {
        console.log("event=>>>>>>>>>>>>>>>>>>>>>>>", event);
        this.completeProfileForm.patchValue({ 'mobile': "" })
        if (event.value == '+1') {
            this.length = 10
        }
        else {
            this.length = 9
        }
    }
}

class profileData {
    "dob": number = null;
    "mobile": number = null;
    "gender": number = null;
    "name": string = "";
    "nick_name": string = "";
    "position": string = "";
    "bio": string = "";
    "town": number = null;
    "city": string = '';
    "Apellido": string = '';
    "is_professional": boolean = false;
    "price": number = null;
    "leg": string = '';
    "is_referee": boolean = false;
    referee_price: number = null;
}