import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PlayerRouting } from "./player-details.routes";
import { SharedModule } from "./../../shared/shared.module";
import { PlayerDetailsComponent } from "./player-details.component";
import { NavbarModule } from "./../navbar/navbar.module";
import { EditPlayerComponent } from "./edit-player/edit-player.component";
import { MyProfileEditComponent } from "./my-profile-edit/my-profile-edit.component";

@NgModule({
  imports: [CommonModule, PlayerRouting, SharedModule.forRoot()],
  declarations: [
    PlayerDetailsComponent,
    EditPlayerComponent,
    MyProfileEditComponent
  ]
})
export class PlayerDetailsModule {}
