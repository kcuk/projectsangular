import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PlayerDetailsComponent } from './player-details.component';
import {PlayerProfileComponent} from './player-profile/player-profile.component';
import {EditPlayerComponent} from './edit-player/edit-player.component';
import { MyProfileEditComponent } from './my-profile-edit/my-profile-edit.component';

export const playerRoutes = [
    { path: '', component: PlayerDetailsComponent },
    { path: 'player-profile/:id', loadChildren:'./player-profile/player-profile.module#PlayerProfileModule'},
    // { path: 'edit-player/:id', component:EditPlayerComponent}
    { path: 'edit-profile', component: MyProfileEditComponent }



];

export const PlayerRouting: ModuleWithProviders = RouterModule.forChild(playerRoutes);