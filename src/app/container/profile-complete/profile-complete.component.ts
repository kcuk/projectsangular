import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-profile-complete',
  templateUrl: './profile-complete.component.html',
  styleUrls: ['./profile-complete.component.css']
})
export class ProfileCompleteComponent  {
  @Input('progress') progress;

  constructor() { }

  ngOnInit() {
  }

}