import { Http } from '@angular/http';
import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalService } from './../../GlobalService';
import { Message } from 'primeng/primeng'
import { LoginService } from '../../services/login/login.service';
import { FacebookService } from '../../services/login/facebookLogin.service';
import { TranslateService } from "ng2-translate";
declare var gapi: any;
declare var $: any;
declare var FB: any;
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [LoginService, FacebookService]
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  userObj: User;
  loader: boolean = false;
  checked: boolean = false;
  msgs: Message[] = [];
  emailExists: boolean;
  ssnExists: boolean = false;
  public emailMsg: string = '';
  public emailExistmsg: string = '';
  public email: string;
  isMobValid: boolean = true;
  fbemail: string = '';
  public country_code_validation: boolean = false;
  public passStatus: boolean = false;
  public signup_status: boolean = false;
  public email_name: string = "";
  code: any = [
    { value: '+1', label: '+1' },
    { value: '+502', label: '+502' },

    { value: '+593', label: '+593' },
  ];
  length: any = 9;

  constructor(private _zone: NgZone,
    private translate: TranslateService, public http: Http, public login_service: LoginService, public facebook: FacebookService, public base_path_service: GlobalService, fb: FormBuilder, public router: Router) {
    this.userObj = new User();
    console.log("aaaaaaaaaaaaaaaaaaaaa", this.userObj)
    this.signupForm = new FormGroup({
      first_name: new FormControl(this.userObj.name, Validators.compose([Validators.required, Validators.maxLength[80], Validators.minLength[3]])),
      last_name: new FormControl(this.userObj.last_name),
      email: new FormControl(this.userObj.email, Validators.compose([Validators.required, Validators.pattern('[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+')])),
      password: new FormControl(this.userObj.password, Validators.compose([Validators.required, Validators.maxLength[50], Validators.minLength[6]])),
      mobile: new FormControl(this.userObj.mobile, Validators.compose([Validators.required])),
      code: new FormControl(this.userObj.country_code, Validators.compose([Validators.required]))
    });
    this.emailMsg = 'Ingresar email con el formato correcto';
    this.emailExistmsg = "correo electrónico ya registrado";
    if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
    }
  }

  ngOnInit() {
    this.userObj = this.base_path_service.signUpObj;
    this.base_path_service.signUpObj = {};
  }

  signup() {
    this.userObj.country_code = this.signupForm.value.code
    if (this.checked) {
      this.loader = true;
      // localStorage.clear();
      let timeNow = new Date();
      this.msgs = [];
      this.loader = true;
      this.email_name = this.signupForm.value.email;
      var url = this.login_service.baseUrl + "/peloteando/signup/?format=json";
      this.login_service.PostRequest1(url, this.userObj)
        .subscribe(
          res => {
            this.signup_status = true;
            this.loader = false;

          },
          err => {
            this.loader = false;
            if (err.status == 404) {
              this.msgs = [];
              this.msgs.push({ severity: 'error', summary: '', detail: 'Network problem' });
            }
            else {
              this.msgs = [];
              this.msgs.push({ severity: 'error', summary: " ", detail: "some error, try again..." });
            }
          });
    }
    else {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: "Checkbox", detail: "Por favor, acepta los términos y condiciones para continuar" });
    }
  }

  googleLogin() {
    let timeNow = new Date();
    this.loader = true;
    let googleAuth = gapi.auth2.getAuthInstance();
    googleAuth.signIn()
      .then(res => {
        this._zone.run(() => {
          let data = {
            id_token: res.getAuthResponse().id_token
          }
          let url = this.base_path_service.base_path_api() + "peloteando/gmail_login/?format=json";
          this.base_path_service.PostRequestUnauthorised(url, data)
            .subscribe(res => {
              this.loader = false;
              localStorage.setItem("google_status", "true");
              let arr = [];
              arr.push(res[0].json)
              timeNow.setHours(timeNow.getHours() + (res[0].json.token.expires_in) / 3600);
              localStorage.setItem("userInfo", JSON.stringify(arr));
              localStorage.setItem("nick_name", JSON.stringify(res[0].json.info.nick_name));
              localStorage.setItem("last_name", JSON.stringify(res[0].json.info.last_name));
              localStorage.setItem("username", JSON.stringify(res[0].json.info.username));
              localStorage.setItem("profile_pic", JSON.parse(JSON.stringify(res[0].json.info.image)));
              localStorage.setItem("timeout", timeNow.toString());
              if (res[0].json.info.first_page == true) {
                this.router.navigate(["/complete-profile1"]);
              } else if (res[0].json.info.second_page == true) {
                this.router.navigate(["/complete-profile2"]);
              } else if (res[0].json.info.first_page == false && res[0].json.info.second_page == false) {
                this.router.navigate(["/dashboard"]);
              }
            }, err => {
              this.loader = false;
              this.msgs = [];
              this.msgs.push({
                severity: 'error',
                summary: localStorage.getItem('language') == 'en' ? "Identification email does not exist" : "Correo electrónico de identificación no existe"
              });
            })
        })
      })
      .catch(err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde"
        });
      })
  }



  facebookLogin() {
    this.loader = true;
    let timeNow = new Date();
    this.loader = true;
    FB.login((response) => {
      let data: any;
      console.log(response, "fb response");
      if (response) {
        FB.api('/me?fields=email', res => {
          localStorage.setItem("em", res.email);
          this.fbemail = res.email;
          console.log(res.email);

        });
        let demo = this;
        let thisa = demo;
        let url = this.base_path_service.base_path + 'api/peloteando/fb_login/?format=json';
        if (localStorage.getItem("em"))
          this.fbemail = localStorage.getItem("em");
        localStorage.removeItem("em");
        data = { email: this.fbemail, access_token: response.authResponse.accessToken };

        this.base_path_service.PostRequestUnauthorised(url, data)
          .subscribe((res) => {
            this.loader = false;
            localStorage.setItem("fb_status", "true");
            timeNow.setHours(timeNow.getHours() + (res[0].json[0].token.expires_in) / 3600);
            localStorage.setItem("userInfo", JSON.stringify(res[0].json));
            localStorage.setItem("nick_name", JSON.stringify(res[0].json[0].info.nick_name));
            localStorage.setItem("username", JSON.stringify(res[0].json[0].info.username));
            localStorage.setItem("last_name", JSON.stringify(res[0].json[0].info.last_name));
            localStorage.setItem("profile_pic", JSON.parse(JSON.stringify(res[0].json[0].info.image)));
            localStorage.setItem("timeout", timeNow.toString());
            if (localStorage.getItem("userInfo")) {
              if (localStorage.getItem("fb_status"))
                localStorage.removeItem("fb_status");
              if (res[0].json[0].info.first_page == true) {
                this.router.navigate(["/complete-profile1"]);
              } else if (res[0].json[0].info.second_page == true) {
                this.router.navigate(["/complete-profile2"]);
              } else if (res[0].json[0].info.first_page == false && res[0].json[0].info.second_page == false) {
                this.router.navigate(["/dashboard"]);
              }
              this.base_path_service.loggedIn = true;
            }
            else {
              this.router.navigate(['/signup']);
            }
          }, err => {
            this.loader = false;
            if (err.status == 404) {
              this.loader = false;
              this.msgs = [];
              this.msgs.push({ severity: 'error', summary: "", detail: "Network problem" });
            }
            else {
              this.loader = false;
              this.msgs = [];
              this.msgs.push({ severity: 'error', summary: "Correo electrónico de identificación no existe" });
            }
          });
        FB.api('/me', function (response) {
          console.log('Good to see you, ' + response.name + '.');
        });
      }
      else {
        this.loader = false;
      }
    }, { scope: 'email,user_birthday' });
    this.loader = false;
  }


  emailVerify(event) {

    if (this.signupForm.controls["email"].valid && this.userObj.email != this.email) {
      this.loader = true;
      this.email = this.userObj.email;
      let url = this.base_path_service.base_path_api() + "peloteando/verification/?email=" + this.email + "&format=json";
      this.base_path_service.GetRequestUnauthorised(url)
        .subscribe(res => {
          this.loader = false;
          this.emailExists = false;
          console.log("does not exist");
        }, err => {
          this.loader = false;
          if (err.status == 404 || err.status == 403 || err.status == 400) {
            this.msgs = [];
            this.msgs.push({ severity: 'error', detail: 'some error' });
          }
          else {
            this.emailExists = true;
            this.msgs = [];
            this.msgs.push({ severity: 'error', detail: 'correo electrónico ya registrado' });
          }
        });
    }
  }

  mobileVerify(value) {
    this.userObj.mobile = value;
    if (this.userObj.mobile.toString().length != this.length || value < 0) {
      console.log("mobileeeeeeeeeeeeeeee")
      this.isMobValid = false;

    }
    else
      this.isMobValid = true;
    console.log("mobileeeeeeeeeeeeeeee =>>>>>>>>>>>>>>", this.isMobValid)

  }
  checkLength(passwordValue) {
    if (passwordValue && passwordValue.length < 6) {
      if (passwordValue.length != 0) {
        this.passStatus = true;
      } else {
        this.passStatus = false;
      }
    } else {
      this.passStatus = false;
    }
  }
  termsandcondition() {
    this.base_path_service.signUpObj = this.userObj;
    this.router.navigate(['/home/terms']);
  }
  privacyPolicy() {
    this.base_path_service.signUpObj = this.userObj;
    this.router.navigate(['/home/privacy-policy']);
  }
  routing() {
    this.router.navigateByUrl("home")
  }

  resendEmail() {

    this.loader = true;
    let url = this.base_path_service.base_path_api() + `peloteando/resendMail/?email=${this.email_name}&format=json`;
    this.http.get(url).subscribe(res => {
      this.loader = false;
      console.log("response");
    }, err => {
      this.loader = false;
    })
  }

  validMobile(event) {
    console.log("event=>>>>>>>>>>>>>>>>>>>>>>>", event);
    this.signupForm.patchValue({ 'mobile': "" })
    if (event.value == '+1') {
      this.length = 10
    }
    else if (event.value == '+502') {
      this.length = 8
    }
    else {
      this.length = 9
    }
  }
}
class User {
  public password: string = "";
  public email: string = "";
  public name: string = "";
  public last_name: string = "";
  public mobile: string = "";
  public facebook_id = "";
  public country_code: string = "+593"

}

