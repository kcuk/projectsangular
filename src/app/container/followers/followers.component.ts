import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../GlobalService';
import {Message} from 'primeng/primeng'
@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.css']
})
export class FollowersComponent implements OnInit {

  loader: boolean = false;
  id: number;
  follower: Array<any> = [];
  followersList: Array<any> = [];
  status: any;
  paramsSub: any;
  basePath: string;
  page: number = 1;
  hide: boolean = false;
  total_pages: number;
  seemore: boolean = false;
  msgs: Message[] = [];
  showSeeMore: boolean = false;
  total_follower: any;
  page_no: number;
  index: number;
  followers: Array<any> = [];
  no_of_player:number;
   groundFollowerList:Array<any>=[];

  constructor(private router: Router, private base_path_service: GlobalService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.paramsSub = this.route.queryParams
      .subscribe(params => {
        this.id = params['id'];
        this.status = params['status'];
      });

    if (this.status == 'team') {
      this.API_getTeamFollowers();
    } else if (this.status == 'tournament') {
      this.API_getTournamentFollowers();
    } else if (this.status == 'player') {
      this.follower = [];
      this.API_getPlayerFollowers();
    } else if(this.status == 'ground'){
      this.groundFollowersList();
    }
    this.basePath = this.base_path_service.base_path;
  }

  API_getTeamFollowers() {
    this.loader = true;
    this.follower = []
    let url = this.base_path_service.base_path + "api/user/team_follow/?team=" + this.id + '&page=' + this.page + '&format=json'
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.followersList = this.follower.concat(res[0].json.data);
        this.total_pages = res[0].json.total_pages;
        this.follower = res[0].json.follower;
        if (this.total_pages > this.page) {
          this.showSeeMore = true;
        }
      }, err => {
        this.loader = false;
      })
  }

  follow(id: any, follow_status, i: number) {
    this.loader = true;
    this.index = i;
    if (this.status == 'team') {
      if (follow_status) {
        let url = this.base_path_service.base_path + 'api/user/player_follow/' + id + '/?format=json'
        this.base_path_service.DeleteRequest(url)
          .subscribe(res => {
            this.loader = false;
            this.msgs.push({ severity: 'info', summary: 'Some info Here', detail: "unfollowed Ã©xito" });
            this.API_getTeamFollowers();
          },
          err => {
            this.loader = false;
            this.msgs.push({ severity: 'error', summary: 'Some info Here', detail: "Error" });
          });
      }
      else {
        let url = this.base_path_service.base_path + 'api/user/profile/'
        let data = { "form_type": "follow_player", "follow_player": id };
        this.base_path_service.PostRequest(url, data)
          .subscribe(res => {
            this.loader = false;
            this.API_getTeamFollowers();
            this.msgs.push({ severity: 'info', summary: 'Some info Here', detail: "seguido con Ã©xito" });

          },
          err => {
            this.loader = false;
            this.msgs.push({ severity: 'error', summary: 'Some info Here', detail: "Error" });

          });
      }
    } else if (this.status == 'tournament') {
      if (follow_status) {
        var url = this.base_path_service.base_path_api() + "user/player_follow/" + id + "/";
        this.base_path_service.DeleteRequest(url)
          .subscribe(res => {
            this.loader = false;
            if (res[0].status == 205) {
              this.API_getTournamentFollowers();
            }
          },
          err => {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: err.json(), detail: 'algunos errores' });
          })
      } else {
        let obj = {
          "form_type": "follow_player",
          "follow_player": id
        }
        var url = this.base_path_service.base_path_api() + "user/profile/";
        this.base_path_service.PostRequest(url, obj)
          .subscribe(res => {
            this.loader = false;
            if (res[0].status == 200) {
              this.API_getTournamentFollowers();
            }
          },
          err => {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: err.json(), detail: 'algunos errores' });
          })
      }

    } else if (this.status == 'player') {

      if (follow_status) {
        let url = this.base_path_service.base_path + 'api/user/player_follow/' + id + '/?format=json'
        this.base_path_service.DeleteRequest(url)
          .subscribe(res => {
            this.msgs = [];
            this.msgs.push({ severity: 'info', summary: '', detail: 'no seguir con Ãxito!' });
            this.API_getPlayerFollowers();
          },
          err => {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores!' });
          });

      }
      else {
        let url = this.base_path_service.base_path + 'api/user/profile/'
        let data = { "form_type": "follow_player", "follow_player": id };
        this.base_path_service.PostRequest(url, data)
          .subscribe(res => {
            this.msgs = [];
            this.msgs.push({ severity: 'info', summary: '', detail: 'Seguido con Ãxito!' })
            this.API_getPlayerFollowers();
          },
          err => {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores!' });
          });
      }
    }else if(this.status == 'ground'){
      let followInfo = {
        "form_type": "follow_player",
        "follow_player": id
      }
      if (!follow_status ) {
          let url = this.base_path_service.base_path_api() + "user/profile/"
          this.base_path_service.PostRequest(url, followInfo)
            .subscribe(res => {
                this.loader = false;
                this.groundFollowersList();
                this.msgs.push({ severity: 'info',  detail: " Actualizado" });
              }, err => {
                this.loader = false;
                this.base_path_service.print(err);
                this.msgs.push({ severity: 'error',  detail: "Error" });
              })
        } else {
            let url = this.base_path_service.base_path_api() + "user/player_follow/" + id + "/?format=json"
            this.base_path_service.DeleteRequest(url)
                .subscribe(res => {
                      this.loader = false;
                      this.groundFollowersList();
                      this.msgs.push({ severity: 'info',  detail: " Actualizado" });
                    }, err => {
                      this.loader = false;
                      this.msgs.push({ severity: 'error', detail: "Error" });
                    })
        }
    }

  }

  seeMore() {
    this.page = this.page + 1;
    if (this.status == 'team') {
      this.API_getTeamFollowers();
    } else if (this.status == 'tournament') {
      this.API_getTournamentFollowers();
    } else if (this.status == 'player') {
      this.follower = [];
      this.API_getPlayerFollowers();
    }
   
  }
  API_getplayerpage(i: number) {
    this.page_no = i / 10 + 1;
    this.API_getPlayerFollowers();
  }

  API_getTournamentFollowers() {
    this.loader = true;
    var url = this.base_path_service.base_path_api() + "user/followTournament/?tournament=" + this.id + "&page=" + this.page + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.base_path_service.print(res[0].json, "followers")
        this.followersList = res[0].json.data;
        this.follower = res[0].json.total_follower;
        this.total_pages = res[0].json.total_pages;
        if (this.total_pages > this.page) {
          this.showSeeMore = true;
        }
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: err.json(), detail: 'algunos errores' });
      })
  }
 
  API_getPlayerFollowers() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'user/player_follow/?form_type=follower&player=' + this.id + '&page=' + this.page;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.followersList = res[0].json.follower;
        this.total_pages = res[0].json.total_pages;
        this.follower = res[0].json.total;
        if (this.total_pages > this.page) {
          this.showSeeMore = true;
        }
        if (res[0].status == 200 || res[0].status == 201 || res[0].status == 205) {
          this.loader = false;
        }
      }, err => {
        this.loader = false;
      });
  }
   groundFollowersList(){
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'ground/groundProfile/?form_type=follower_list&ground=' + this.id + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.followersList = res[0].json.player_list;
        this.follower = res[0].json.total;
        this.msgs.push({ severity: 'info', detail: " Actualizado" });
      }, err => {
        this.base_path_service.print(err);
        this.loader = false;
        this.msgs.push({ severity: 'error',  detail: "Error" });
      })
  }

  routing() {
    if (this.status == "tournament") {
      this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + this.id)
    } else if (this.status == "team") {
      this.router.navigateByUrl('/dashboard/team/team-profile/' + this.id)
    } else if (this.status == "player") {
      this.router.navigateByUrl('/dashboard/player/player-profile/' + this.id)
    }else if (this.status == "ground") {
      this.router.navigateByUrl('/dashboard/ground/ground-profile/' + this.id)
    }
  }
  redirectPlayerprofile(id : number){
    this.router.navigateByUrl('/dashboard/player/player-profile/'+id)
  }
}
