import { Component, OnInit, ViewContainerRef, NgZone } from '@angular/core';
import { FormBuilder, Validator, AbstractControl, Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalService } from './../../GlobalService';
import { Button, Messages, InputText, Growl, Message } from 'primeng/primeng';
import { LoginService } from './../../services/login/login.service';
import { FacebookService } from './../../services/login/facebookLogin.service'

import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { TranslateService } from "ng2-translate";

class loginData {
    "name": string;
    "password": string;
}

declare var $: any;
declare var FB: any;
declare var gapi: any;



@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [FacebookService, LoginService, GlobalService]
})
export class LoginComponent implements OnInit {

    auth2: any;
    public API_hitting: boolean = false;
    public loginForm: FormGroup;
    public name1: boolean = false;
    public pass1: boolean = false;
    public visibility: boolean = false;
    public summary: string;
    public detail: string;
    public token: string;
    public msgs: Message[] = [];
    public uData: any;
    public accessToken: string = null;
    public loginDataIns;
    public fbApi: string = '/peloteando/fb_login/';
    public fbResponse: any;
    public first_time: boolean = true;
    public obj;
    public key;
    public email: string = '';
    public isLoggedIn: boolean = false;

    constructor(
        private _zone: NgZone,
        private translate: TranslateService, public fb: FormBuilder, public facebook_service: FacebookService, public router: Router, public base_path_service: GlobalService, public login_service: LoginService) {


        if (localStorage.getItem("timeout")) {
            let timeout = (localStorage.getItem("timeout"));
            router.navigateByUrl('/dashboard');
        }
        else
            this.isLoggedIn = true;
        this.loginDataIns = new loginData();
        this.loginForm = new FormGroup({
            name: new FormControl('', Validators.compose([Validators.required, Validators.pattern("[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+")])),
            pass: new FormControl('', Validators.compose([Validators.minLength(6), Validators.required,]))
        })

    }

    ngOnInit() {
        document.getElementById("pass1")
            .addEventListener("keyup", function (event) {
                event.preventDefault();
                if (event.keyCode == 13) {
                    document.getElementById("logInButton").click();
                }
            });

    }

    ngAfterViewInit() {
        this.googleInit();
    }

    public googleInit() {
        gapi.load('auth2', () => {
            this.auth2 = gapi.auth2.init();
        })
    }


    loginFun(data: any) {
        let timeNow = new Date();
        this.API_hitting = true;
        this.base_path_service.loginRequest(data, "api/peloteando/login/?format=json")
            .subscribe(res => {
                let lang = localStorage.getItem('language')
                localStorage.clear();
                localStorage.setItem('language', lang)

                this.API_hitting = false;
                timeNow.setHours(timeNow.getHours() + (res[0].json[0].token.expires_in) / 3600);
                localStorage.setItem("userInfo", JSON.stringify(res[0].json));
                localStorage.setItem("nick_name", JSON.stringify(res[0].json[0].info.nick_name));
                localStorage.setItem("username", JSON.stringify(res[0].json[0].info.username));
                localStorage.setItem("last_name", JSON.stringify(res[0].json[0].info.last_name));
                localStorage.setItem("profile_pic", JSON.parse(JSON.stringify(res[0].json[0].info.image)));
                localStorage.setItem("timeout", timeNow.toString());
                let url = this.base_path_service.base_path_api() + "user/setLanguage/?language=" + localStorage.getItem('language');
                this.base_path_service.GetRequest(url)
                    .subscribe(res => {
                    })
                if (localStorage.getItem("userInfo")) {
                    if (localStorage.getItem("fb_status"))
                        localStorage.removeItem("fb_status");
                    if (res[0].json[0].info.first_page == true) {
                        this.router.navigate(["/complete-profile1"]);
                    } else if (res[0].json[0].info.second_page == true) {
                        this.router.navigate(["/complete-profile2"]);
                    } else if (res[0].json[0].info.first_page == false && res[0].json[0].info.second_page == false) {
                        this.router.navigate(["/dashboard"]);
                    }
                    this.base_path_service.loggedIn = true;
                }
                else {
                    this.router.navigate(['/login']);
                }

            },
                err => {
                    this.msgs = [];
                    this.msgs.push({ severity: 'error', detail: "El correo electrónico o la contraseña son incorrectos" });
                    this.API_hitting = false;
                })
    }

    getStyle(ele: string) {
        if (!this.loginForm.controls[ele].valid) {
            this.msgs = [];
            if (ele === 'name') {
                this.name1 = true;
                this.summary = "Name";
                this.detail = "can't be Empty";
            }
            else if (ele === 'pass') {
                this.pass1 = true;
                this.summary = "Password";
                this.detail = `Can't be empty
                   * Must have at least 6 characters`;
            }
            this.msgs.push({ severity: 'error', summary: this.summary, detail: this.detail });

        }
    }

    setVisibilityFalse(ele: string) {
        this.visibility = false;
        if (ele === 'name')
            this.name1 = false;
        else if (ele === 'pass')
            this.pass1 = false;
        this.msgs = [];
    }

    googleLogin() {
        let timeNow = new Date();
        this.API_hitting = true;
        let googleAuth = gapi.auth2.getAuthInstance();
        googleAuth.signIn()
            .then(res => {
                this._zone.run(() => {         
                let data = {
                    id_token: res.getAuthResponse().id_token
                }
                let url = this.base_path_service.base_path_api() + "peloteando/gmail_login/?format=json";
                this.base_path_service.PostRequestUnauthorised(url, data)
                    .subscribe(res => {
                        this.API_hitting = false;
                        localStorage.setItem("google_status", "true");
                        let arr = [];
                        arr.push(res[0].json)
                        timeNow.setHours(timeNow.getHours() + (res[0].json.token.expires_in) / 3600);
                        localStorage.setItem("userInfo", JSON.stringify(arr));
                        localStorage.setItem("nick_name", JSON.stringify(res[0].json.info.nick_name));
                        localStorage.setItem("last_name", JSON.stringify(res[0].json.info.last_name));
                        localStorage.setItem("username", JSON.stringify(res[0].json.info.username));
                        localStorage.setItem("profile_pic", JSON.parse(JSON.stringify(res[0].json.info.image)));
                        localStorage.setItem("timeout", timeNow.toString());
                        if (res[0].json.info.first_page == true) {
                            this.router.navigate(["/complete-profile1"]);
                        } else if (res[0].json.info.second_page == true) {
                            this.router.navigate(["/complete-profile2"]);
                        } else if (res[0].json.info.first_page == false && res[0].json.info.second_page == false) {
                            this.router.navigate(["/dashboard"]);
                        }
                    }, err => {
                        this.API_hitting = false;
                        this.msgs = [];
                        this.msgs.push({
                            severity: 'error',
                            summary: localStorage.getItem('language') == 'en' ? "Identification email does not exist" : "Correo electrónico de identificación no existe"
                        });
                    })
                })
            })
            .catch(err => {
                this.API_hitting = false;
                this.msgs = [];
                this.msgs.push({
                    severity: 'error',
                    summary: localStorage.getItem('language') == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde"
                });
            })
    }

    facebookLogin(x: any) {
        this.API_hitting = true;
        let timeNow = new Date();
        FB.login((response) => {
            let data: any;
            if (response) {
                FB.api('/me?fields=email', res => {
                    this.email = res.email;
                    localStorage.setItem("em", this.email);
                    return this.email;
                });
                let url = this.base_path_service.base_path + 'api/peloteando/fb_login/?format=json';
                if (localStorage.getItem("em"))
                    this.email = localStorage.getItem("em");
                localStorage.removeItem("em");
                data = { email: this.email, access_token: response.authResponse.accessToken };
                this.base_path_service.PostRequestUnauthorised(url, data)
                    .subscribe((res) => {
                        this.API_hitting = false;
                        localStorage.setItem("fb_status", "true");
                        timeNow.setHours(timeNow.getHours() + (res[0].json[0].token.expires_in) / 3600);
                        localStorage.setItem("userInfo", JSON.stringify(res[0].json));
                        localStorage.setItem("nick_name", JSON.stringify(res[0].json[0].info.nick_name));
                        localStorage.setItem("last_name", JSON.stringify(res[0].json[0].info.last_name));
                        localStorage.setItem("username", JSON.stringify(res[0].json[0].info.username));
                        localStorage.setItem("profile_pic", JSON.parse(JSON.stringify(res[0].json[0].info.image)));
                        localStorage.setItem("timeout", timeNow.toString());

                        if (localStorage.getItem("userInfo")) {
                            if (res[0].json[0].info.first_page == true) {
                                this.router.navigate(["/complete-profile1"]);
                            } else if (res[0].json[0].info.second_page == true) {
                                this.router.navigate(["/complete-profile2"]);
                            } else if (res[0].json[0].info.first_page == false && res[0].json[0].info.second_page == false) {
                                this.router.navigate(["/dashboard"]);
                            }
                            this.base_path_service.loggedIn = true;
                        }
                        else {
                            this.router.navigate(['/login']);
                        }
                    }, err => {

                        if (err.status == 404) {
                            this.API_hitting = false;
                            this.msgs = [];
                            this.msgs.push({ severity: 'error', summary: this.summary, detail: "Network problem" });
                        }
                        else {
                            this.API_hitting = false;
                            this.msgs = [];
                            this.msgs.push({ severity: 'error', summary: "Correo electrónico de identificación no existe" });
                        }
                    });

                FB.api('/me', function (response) {

                });
            }
            else {
                this.API_hitting = false;
            }
        }, { scope: 'email,user_birthday' });
        this.API_hitting = false;
    }

    passwordReset(x: any) {
        let url = this.login_service.baseUrl + '/peloteando/forgot/?format=json'
        if (x != null && x != undefined) {
            let email: any = { "email": x };
            this.login_service.forgetPasswordRequest(email, url)
                .subscribe(
                    res => {
                    },
                    err => {

                    })
        }
    }

    routing() {
        this.router.navigateByUrl('home/user')
    }
}
