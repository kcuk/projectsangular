import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';


@Component({
  selector: 'app-team-jugadores',
  templateUrl: './team-jugadores.component.html',
  styleUrls: ['./team-jugadores.component.css']
})
export class TeamJugadoresComponent implements OnInit {
  status: string = "player";
  referee: any;
  team_id;
  isAdmin: boolean;
  player: Array<any> = [];
  tournament: Array<any> = [];
  msgs: Message[] = [];
  loader: boolean = false;
  basePath: any;
  constructor(public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    let sub = this.route.parent.params.subscribe(params => {
      this.team_id = +params['id'];
    });
    this.basePath = this.base_path_service.image_url;
    this.getPlayers();
  }

  getPlayers() {
    this.loader = true;


    this.status = "player";
    let url = this.base_path_service.base_path + "api/team/profile/" + this.team_id + "/?form_type=posesiones&option=jugador&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.player = [];
        this.loader = false;
        this.tournament = []
        this.player = res[0].json.data;
        this.isAdmin = res[0].json.admin;
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: "error", summary: "", detail: "Error en dejar a sus jugadores" });
      });
  }

  followPlayer(id: any, follow_status) {
    this.loader = true;
    if (follow_status) {
      let url = this.base_path_service.base_path + "api/user/player_follow/" + id + "/?format=json";
      this.base_path_service.DeleteRequest(url)
        .subscribe(res => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: "info", summary: "", detail: "Unfollowed éxito" });
          this.getPlayers();
        },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: "error", summary: "", detail: "no pudo Dejar de seguir" });
        });

    }
    else {
      let url = this.base_path_service.base_path + "api/user/profile/?format=json";
      let data = { "form_type": "follow_player", "follow_player": id };
      this.base_path_service.PostRequest(url, data)
        .subscribe(res => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: "info", summary: "", detail: "seguido con éxito" });
          this.getPlayers();
        },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: "error", summary: "", detail: "no podía seguir" });
        });
    }
  }
}
