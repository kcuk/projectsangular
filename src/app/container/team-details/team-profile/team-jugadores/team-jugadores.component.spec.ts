import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamJugadoresComponent } from './team-jugadores.component';

describe('TeamJugadoresComponent', () => {
  let component: TeamJugadoresComponent;
  let fixture: ComponentFixture<TeamJugadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamJugadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamJugadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
