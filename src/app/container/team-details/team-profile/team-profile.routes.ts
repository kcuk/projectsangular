import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TeamProfileComponent } from "./team-profile.component";
import { TorneosComponent } from "./torneos/torneos.component";
import { TeamScheduleComponent } from "./team-schedule/team-schedule.component";
import { TeamRequestComponent } from "./team-request/team-request.component";
import { TeamInformationComponent } from "./team-information/team-information.component";
import { TeamJugadoresComponent } from "./team-jugadores/team-jugadores.component";
import { EditTeamComponent } from "./../edit-team/edit-team.component";
import { ManagePlayerComponent } from "./manage-player/manage-player.component";
import { TeamhistoryComponent } from "./teamhistory/teamhistory.component";

export const teamProfileRoutes: Routes = [
  {
    path: "",
    component: TeamProfileComponent,
    children: [
      { path: "", component: TeamInformationComponent },
      { path: "information", component: TeamInformationComponent },
      { path: "schedule", component: TeamScheduleComponent },
      { path: "request", component: TeamRequestComponent },
      { path: "torneos", component: TorneosComponent },
      { path: "jugadores", component: TeamJugadoresComponent },
      { path: "history", component: TeamhistoryComponent }
    ]
  },
  { path: "edit-team", component: EditTeamComponent },
  { path: "manage-player", component: ManagePlayerComponent }
];

export const teamProfileRouting: ModuleWithProviders = RouterModule.forChild(
  teamProfileRoutes
);
