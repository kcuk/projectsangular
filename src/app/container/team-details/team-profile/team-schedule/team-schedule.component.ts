import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../../GlobalService';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-team-schedule',
  templateUrl: './team-schedule.component.html',
  styleUrls: ['./team-schedule.component.css']
})
export class TeamScheduleComponent implements OnInit {

  public team_id:any;
  public team_scheduleInfo: any[] = [];
  public basePath: any;
  league_id: string = "";
  session_id: string = "";
  sessonarray: any[] = [];
  msgs: any[]=[];
  leaguearray: any[]=[];
  loader: boolean;
  seasonsection: boolean;
  nodata: boolean;

  constructor(public router: Router, public route: ActivatedRoute, public base_path_service: GlobalService) { }

  ngOnInit() {
    this.basePath = this.base_path_service.image_url;
    this.route.parent.params.subscribe(params => {
      this.team_id = +params['id'];
    });
    this.teamScheduleInfo();
    this.getLeaguesList();
  }

  public teamScheduleInfo() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "team/profile/" + this.team_id + "/?form_type=schedule&league=" + this.league_id + "&season=" + this.session_id + "&format=json"
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.team_scheduleInfo = res[0].json;
        if(this.team_scheduleInfo.length==0){
          this.nodata = true;
        }
        else{
          this.nodata = false;
        }
        this.loader = false;
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error,Try again later"
        });
      })
  }

  getseasons() {
    var url = this.base_path_service.base_path + "api/user/history_filter/?team_id=" + this.team_id + "&form_type=team&sort_type=season&league_id=" + this.league_id;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        let seasondata = res[0].json.seasons;
        console.log(seasondata, "res=================");
        seasondata.forEach(element => {
          this.sessonarray.push({
            label: element.season,
            value: element.season
          });
        });
        this.loader = false;
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error,Try again later"
        });
      }
    );
  }

  getLeaguesList() {
    var url = this.base_path_service.base_path + "api/user/history_filter/?team_id=" + this.team_id + "&form_type=team&sort_type=league";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        let leaguedata = res[0].json.leagues;
        console.log(res, "res=================");
        leaguedata.forEach(element => {
          this.leaguearray.push({
            label: element.league_name,
            value: element.league_id
          });
        });
        console.log(this.leaguearray, "leaguearray==============leaguedata===");
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: 'error',
            summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error,Try again later"
          });
        }
      }
    );
  }

  reset() {
    this.seasonsection = false;
    this.league_id = "";
    this.session_id = "";
    this.sessonarray = [];
    this.teamScheduleInfo();
  }

  leagueselect(event) {
    this.loader = true;
    this.sessonarray = [];
    this.session_id=''
    console.log("leagure event", event);
    this.getseasons();
    this.seasonsection = true;
  }

  seasonselect(event) {
    this.teamScheduleInfo();
  }

}

