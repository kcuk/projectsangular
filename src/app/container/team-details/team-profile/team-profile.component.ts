import { Component, OnInit, ViewChild } from '@angular/core';
import { Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { GlobalService } from './../../../GlobalService';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import * as html2canvas from 'html2canvas';
import { TranslateService } from "ng2-translate";
@Component({
  selector: 'app-team-profile',
  templateUrl: './team-profile.component.html',
  styleUrls: ['./team-profile.component.css']
})
export class TeamProfileComponent implements OnInit {

  public color: boolean = false;
  public results: string[];
  public es: any;
  public auto: any;
  public backdrop: boolean = false;
  public joins: any;
  public invitePopup: boolean = false;
  public loader: boolean = false;
  public team_details: any = [{}];
  public follower: Array<any> = [{}];
  public displayDialog: boolean = false;
  public val1 = 3;
  public pop: Array<any> = [{}];
  public followersPopup: boolean = false;
  public logo: File;
  public teams: boolean = false;
  public msgs: Message[] = [];
  public teams1: Array<any> = [];
  public mesg: boolean = false;
  public date: Date;
  public team_id: string = '';
  public challengeDate: any = '';
  public dateValid: boolean = false;
  public getId: number = 0;
  public disablebtn: boolean = false;
  public invitedTeamId: number;
  public registerPopup: boolean = false;
  public enable: boolean = false;
  public display: boolean = false;
  public isAdmin: boolean;
  public basePath: string = "";
  public data: any;
  public team_logo: any;
  public redirection: any;
  public cropperSettings: CropperSettings;
  public changeCoverPic: boolean = false;
  public coverPic: Array<any> = [];
  public coverPicId: number;
  public msg: string = '';
  public info: boolean = true;
  public value: boolean = false;
  public invitePl: string = '';
  public query: string = '';
  public challengeTime: any;
  public url: string = "";
  public twitterUrl: string = "";
  public tour_info: Array<any> = [];
  public inviteTournamentPopup: boolean = false;
  public shareimgUrl: string = "";
  @ViewChild('cropper', undefined) cropper: ImageCropperComponent;
  @ViewChild('layout') canvasRef;
  public share_profole_image: string = "";
  downloadProfile: boolean = false;
  downloadDataInfo: any;
  imageUrlDownload: string = "";
  public attitude: number = 0;
  public techniq: number = 0


  constructor(private translate:TranslateService, public router: Router, public http: Http, public global: GlobalService, public route: ActivatedRoute) {

    this.cropperSettings = new CropperSettings();
    this.cropperSettings.minWithRelativeToResolution = false;
    this.cropperSettings.width = 300;
    this.cropperSettings.height = 300;
    this.cropperSettings.keepAspect = false;

    this.cropperSettings.croppedWidth = 210;
    this.cropperSettings.croppedHeight = 210;

    this.cropperSettings.canvasWidth = 350;
    this.cropperSettings.canvasHeight = 300;

    this.cropperSettings.minWidth = 100;
    this.cropperSettings.minHeight = 100;

    this.cropperSettings.rounded = true;


    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings.noFileInput = true;

    this.data = {};

    if(localStorage.getItem('language')){
      translate.use(localStorage.getItem('language'));
    }
  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      document.body.scrollTop = 0;
    });

    var tempUrl = window.location.href;
    this.shareimgUrl = tempUrl;
    var arrUrl = tempUrl.split('/');
    var temp = arrUrl[arrUrl.length - 1].split('?');
    var currentUrl = temp[0];
    if (currentUrl == 'request' || currentUrl == 'schedule' || currentUrl == 'jugadores' || currentUrl == 'torneos') {
      this.info = false;
    }
    this.es = {
      closeText: "Cerrar",
      prevText: "<Ant",
      nextText: "Sig>",
      currentText: "Hoy",
      monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
      ],
      monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
      dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
      dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
      dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
      weekHeader: "Sm",
      dateFormat: "yy-mm-dd",
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ""
    };
    this.loader = true;

    let sub = this.route.params.subscribe(params => {
      this.team_id = params['id'];
      this.getBasicInformation();
    });

    if (localStorage.getItem("create_status")) {
      this.redirection = localStorage.getItem("create_status");
      localStorage.removeItem("create_status");
    }
    this.loader = false;
    this.basePath = this.global.image_url;

  }

  message() {
    let url = this.global.base_path + "api/team/profile/" + this.team_id + "/?form_type=basic&format=json";
    this.global.GetRequest(url).subscribe(res => {
      this.team_details = res[0].json;
      this.team_logo = res[0].json.team_logo
      this.isAdmin = res[0].json.admin;
      this.mesg = true;
    })
  }

  getBasicInformation() {
    let url = this.global.base_path + "api/team/profile/" + this.team_id + "/?form_type=basic&format=json";
    this.global.GetRequest(url).subscribe(res => {
      this.team_details = res[0].json;
      this.team_logo = res[0].json.team_logo
      this.isAdmin = res[0].json.admin;
      this.joins = res[0].json.join;
      this.share_profole_image = this.basePath + "/" + res[0].json.share_team_link;
      console.log("hello", this.share_profole_image)
    }, err => {

    });
  }

  routing() {
    this.loader = true;
    this.loader = false;
  }
  follows() {
    if (this.follower.length > 0) {
      this
        .router
        .navigateByUrl("/dashboard/followers?id=" + this.team_id + "&status=team")
    }
  }
  myTeamsModal(pg) {
    let url = this.global.base_path + "api/team/profile/?value=myteam&format=json";
    this.global.GetRequest(url).subscribe(res => {
      this.teams1 = res[0].json.teams;
      if (this.teams1.length == 0)
        this.disablebtn = true;
    }
      , err => {

      })
  }

  redirectSuccess() {
    this.backdrop = false;
    this.redirection = false;
  }

  submitChallenge() {
    let cdate = new Date(this.challengeDate).getTime();
    let currentDate = new Date().getTime();

    if (this.challengeDate == undefined || (cdate < currentDate) || !this.challengeDate || !this.getId) {

    } else {
      let myJson = {
        "challenger_team": this.getId,
        "invited_team": parseInt(this.team_id),
        "date_time": this.challengeDate,
        "time": this.challengeTime
      }

      let url = this.global.base_path + "api/scorecard/challenge/";
      this.global.PostRequest(url, myJson).subscribe(res => {
        this.getId = null;
        this.challengeDate = "";
        myJson = undefined;
        this.displayDialog = false;
        this.teams1 = res[0].json.teams;
        this.msgs = [];
        this.msgs.push({ severity: 'info', summary: '', detail: 'Solicitud de reto a sido enviada' });
        this.backdrop = false;
      }, err => {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'No se pudo desafiar' });
      })
    }
  }

  showReg(id) {
    this.invitedTeamId = id;
    this.dateValid = false;
    this.displayDialog = true;
    this.myTeamsModal(1);
  }

  cancel() {
    this.displayDialog = false;
    this.backdrop = false;
    this.getId = null;
    this.challengeDate = null;
  }

  accept() {
    this.teams = false;
    this.backdrop = false;
    let url = this.global.base_path + "api/user/applyTeam/?format=json"
    let data = {
      team: parseInt(this.team_id)
    }
    this.global.PostRequest(url, data).subscribe(res => {
      this.getBasicInformation();
      if (res[0].status == 201)
        this.msgs = [];
      this.msgs.push({ severity: 'info', summary: 'Info Message', detail: 'solicitud enviada' });
    }, err => {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'No solicitar enviado' });
    });
  }

  team() {
    this.teams = true;
    this.backdrop = true;
    this.getBasicInformation();
  }
  cancl() {
    this.teams = false;
    this.backdrop = false;
  }
  getChallengeId(id) {
    this.getId = id;
    this.color = true;
  }

  fileChangeListener(event) {
    var image: any = new Image();
    var file: File = event.target.files[0];
    var myReader: FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
      that.logo = image.src.split(',')[1];
    };
    myReader.readAsDataURL(file);
    this.showDialog();
  }

  showDialog() {
    this.display = true;
  }
  save() {
    this.loader = true;
    this.display = false;
    this.backdrop = false;
    let obj = JSON.parse(localStorage.getItem("userInfo")).token.access_token;
    let url = this.global.base_path_api() + 'team/image/';
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      formData.append("team", this.team_id);
      formData.append("team_logo", this.logo);
      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 205) {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'info', summary: 'Actualizado Exitosamente', detail: '' });
            this.getBasicInformation();
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + obj);
      xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
      xhr.send(formData);
    });
  }

  disable() {
    this.backdrop = false;
    this.mesg = false;
    this.display = false;
  }

  remove_img($event) { }


  follow(status) {
    if (!status) {
      let url = this.global.base_path + 'api/user/profile/'
      let player_data = {
        form_type: "follow_team",
        follow_team: this.team_id
      }
      this.global.PostRequest(url, player_data).subscribe(res => {
        this.getBasicInformation();
        this.msgs.push({ severity: 'info', detail: "seguir con éxito" });
      }, err => {
        this.msgs.push({ severity: 'error', detail: "Error" });
      });
    } else
      this.unfollow();
  }

  unfollow() {
    let url = this.global.base_path + "api/user/team_follow/" + this.team_id + "/?format=json"
    let player_data = {
      form_type: "follow_team",
      follow_team: this.team_id
    }
    this.global.DeleteRequest(url).subscribe(res => {
      this.getBasicInformation();
      this.msgs.push({ severity: 'info', detail: "seguir sin éxito" });
    }, err => {
      this.msgs.push({ severity: 'error', detail: "Error" });
    });

  }

  search(event) {
    this.query = event.query;
    this.invitePopupOpen(this.query);
  }

  invitePopupOpen(str) {

    if (str == 'default') {
      this.auto = '';
      str = null;
    }
    let url = this.global.base_path + 'api/user/profile/?form_type=player&value=' + str + '&team=' + this.team_id;
    this.global.GetRequest(url).subscribe(res => {
      this.pop = res[0].json;
      if (this.pop.length == 0) {
        this.value = true;
      } else {
        this.value = false;
      }
      this.invitePopup = true;
    })
  }
  inviteTournamentInfo() {
    let url = this.global.base_path + `api/tournament/tournamentInvaitTeam/?team=${this.team_id}`
    this.global.GetRequest(url).subscribe(res => {
      this.tour_info = res[0].json;
      this.inviteTournamentPopup = true;
    })
  }
  backdrops() {
    this.followersPopup = false;
    this.backdrop = false;
  }

  invitePlayer(id) {
    let url = this.global.base_path + "api/user/player_request/";
    let data = {
      "team": this.team_id,
      "player": id
    };
    this.global.PostRequest(url, data).subscribe(res => {
      this.invitePopupOpen(this.query);
      this.msgs.push({ severity: 'info', summary: 'Info Message', detail: 'InvitaciÃ³n enviada con Ã©xito' });
    }, err => {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'No solicitar enviado' });
    });
  }

  checkRetarDate() {
    if (new Date() > new Date(this.challengeDate))
      this.dateValid = true;
    else
      this.dateValid = false;

  }

  coverPicListener() {
    this.changeCoverPic = true;
    let url = this.global.base_path + "api/user/cover_pic/?format=json";
    this.global.GetRequest(url).subscribe(res => {
      this.coverPic = res[0].json;
    }, err => {
    });
  }

  selectCoverPic(id) {
    if (this.coverPicId != id)
      this.coverPicId = id;
    else
      this.coverPicId = null;

  }
  updateCoverPic() {
    let url = this.global.base_path + "api/team/image/?format=json";
    let data = {
      team: this.team_id,
      cover_image: this.coverPicId
    };
    this.global.PostRequest(url, data).subscribe(res => {
      this.changeCoverPic = false;
      this.backdrop = false;
      this.getBasicInformation();
    }, err => {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: '', detail: 'Some error' });
    });

  }

  postMsg() {
    this.backdrop = false;
    this.mesg = false;
    let url = this.global.base_path + "api/user/message/?format=json";
    let data = {
      form_type: 'p2t',
      message: this.msg,
      team: this.team_id
    };
    this.global.PostRequest(url, data).subscribe(res => {
      this.msgs = [];
      this.msgs.push({ severity: "info", summary: '', detail: 'Mensaje enviado con éxito' });
    }, err => {
      this.msgs = [];
      this.msgs.push({ severity: "error", summary: "", detail: "error" });
    });

  }

  inviteTournament(id: number) {
    this.loader = true;
    let url = this.global.base_path_api() + "tournament/team_request/?format=json";
    let data = {
      tournament: id,
      team: this.team_id
    };
    this.global.PostRequest(url, data).subscribe(res => {
      this.loader = false;
      this.msgs = [];
      this
        .msgs
        .push({ severity: "info", summary: '', detail: 'Invitar correctamente' });
      this.inviteTournamentInfo();
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: "error", summary: '', detail: 'Invitar sin éxito' });
    })
  }
  downloadImage() {
    this.loader = true;
    let url = this.global.base_path_api() + `team/teamProfile/?id=${this.team_id}&format=json`;
    this.global.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.downloadProfile = true;
        this.downloadDataInfo = res[0].json;
        this.attitude = res[0].json.attitude;
        this.techniq = res[0].json.technique;

        setTimeout(() => {
          this.drawImage();
        }, 1000)

      }, err => {
        this.loader = false;
      })
  }
  downloadAsImage() {
    this.downloadImage();

  }

  drawImage() {
    let canvas = this.canvasRef.nativeElement;
    html2canvas(canvas, {
      onrendered: (canvas) => {
        this.imageUrlDownload = canvas.toDataURL("image/png");
      }
    })

  }

}

