import { Component, OnInit } from '@angular/core';
import {Message} from 'primeng/primeng';
import {Router,ActivatedRoute} from '@angular/router';
import { GlobalService } from './../../../../GlobalService';

@Component({
  selector: 'app-team-information',
  templateUrl: './team-information.component.html',
  styleUrls: ['./team-information.component.css']
})
export class TeamInformationComponent implements OnInit {

  public val1: number = 3;
  public team_id: number;
  public ratings: number;
  public team: any = {};
  public shoutout: Array<any> = [{}];
  public shows: any = {};
  public isAdmin: boolean;
  public attitude: any;
  public technique: any;
  public backdrop: boolean = false;
  public backdrop1: boolean = false;
  public visble: boolean = false;
  public paramsSub: any;
  public msgs: Message[] = [];

  constructor(public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute) {
  }

  ngOnInit() {
    this.base_path_service.teamProfile.next("information");
    let sub = this.route.parent.params.subscribe(params => {
      this.team_id = +params['id'];
      this.getTeamInformation();
    });
  }
  showModal() {
    let url = this.base_path_service.base_path + "api/team/rating/" + this.team_id + "/?format=json"
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.shows = res[0].json;
        this.visble = true;
        this.backdrop1 = true;
      })
  }

  mores() {
    this.router.navigateByUrl('/dashboard/shoutouts/' + this.team_id + '/team');
  }

  rating() {

    let ratings = {
      "attitude": this.shows.attitude,
      "technique": this.shows.technique,
      "team": this.team_id
    }
    let url = this.base_path_service.base_path + "api/team/rating/?format=json";
    this.base_path_service.PostRequest(url, ratings)
      .subscribe(res => {

        this.getTeamInformation();
        this.visble = false;
        this.backdrop1 = false;
        this.msgs = [];
        this.msgs.push({ severity: 'info', summary: 'Info Message', detail: 'solicitud enviada' });
      },
      err => {
        this.visble = false;
        this.backdrop1 = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'No solicitar enviado' });
      })
  }

  cancle() {
    this.visble = false;
    this.backdrop1 = false;
  }
  getTeamInformation() {
    let url = this.base_path_service.base_path + "api/team/profile/" + this.team_id + "/?form_type=information&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {

        this.team = res[0].json.team;
        this.shoutout = res[0].json.shoutout;
        this.isAdmin = res[0].json.admin;
        this.attitude = res[0].json.team.attitude;
        this.technique = res[0].json.team.technique;
      },
      err => {
      });
  }
}


