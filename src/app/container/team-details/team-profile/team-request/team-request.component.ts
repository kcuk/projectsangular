import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';
import { Message } from 'primeng/primeng';

@Component({
  selector: 'app-team-request',
  templateUrl: './team-request.component.html',
  styleUrls: ['./team-request.component.css']
})
export class TeamRequestComponent implements OnInit {

  basePath: string;
  status: string = "invite";
  team_id;
  open_match: Array<any> = [{}];
  player_request: Array<any> = [{}];
  referee_request: Array<any> = [{}];
  tournament_request: Array<any>;
  player: Array<any> = [{}];
  match: Array<any> = [{}];
  sentData: Array<any> = [{}];
  recievedData: Array<any> = [{}];
  dropdownOptions: Array<any> = []
  actionType: String = '';
  msgs: Message[] = [];
  showLoader: boolean = false;
  openMatch;
  openChallengeSent: Array<any> = [];
  actionTypeTournament: string = '';

  constructor(public global: GlobalService, public router: Router, public route: ActivatedRoute) {
    this.basePath = this.global.image_url;
    this.dropdownOptions = []
    this.dropdownOptions.push({ label: "ACCIÓN", value: null });
    this.dropdownOptions.push({ label: "Aceptar", value: "accepted" });
    this.dropdownOptions.push({ label: "Negar", value: "deny" });
  }

  ngOnInit() {
    this.global.teamProfile.next("request");
    let sub = this.route.parent.params.subscribe(params => {
      this.team_id = +params['id'];
    });
    this.getRequest("received");
  }

  getRequest(type: string) {
    this.showLoader = true;
    this.sentData = [];
    this.recievedData = [];
    this.openMatch = [];
    this.openChallengeSent = [];
    let url = this.global.base_path + "api/team/profile/" + this.team_id + "/?form_type=request&option=" + type + "&format=json";
    this.global.GetRequest(url)
      .subscribe(res => {
        this.showLoader = false;
        if (type == "sent") {
          this.open_match = res[0].json.open_match;
          this.player_request = res[0].json.player_request;
          this.referee_request = res[0].json.referee_request;
          this.tournament_request = res[0].json.tournament_request;
          this.openChallengeSent = res[0].json.team_open_match_request;
          if (this.open_match || this.player_request || this.referee_request || this.tournament_request)
            this.addSentDetailField()
        }
        else {
          this.player = res[0].json.player;
          this.match = res[0].json.match;
          this.openMatch = res[0].json.open_match;
          this.tournament_request = res[0].json.tournament_request;
          this.addRecievedDetailField();
        }
      },
        err => {
          this.showLoader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores en la obtención de las solicitudes' });
        });

  }

  addSentDetailField() {
    if (this.open_match) {
      let len = this.open_match.length;
      for (let i = 0; i < len; i++) {
        this.open_match[i].type = "open";
        this.open_match[i].redirect_id = this.open_match[i].id;
      }
    }
    if (this.player_request) {
      let len = this.player_request.length;
      for (let i = 0; i < len; i++) {
        this.player_request[i].type = "player";
        this.player_request[i].redirect_id = this.player_request[i].id;
        this.player_request[i].id = this.player_request[i].request_id;
      }
    }
    if (this.referee_request) {
      let len = this.referee_request.length;
      for (let i = 0; i < len; i++) {
        this.referee_request[i].type = "referee";
        this.referee_request[i].redirect_id = this.referee_request[i].id;
        this.referee_request[i].id = this.referee_request[i].match_id;
      }
    }
    if (this.tournament_request) {
      let len = this.tournament_request.length;
      for (let i = 0; i < len; i++) {
        this.tournament_request[i].type = "tournament";
        this.tournament_request[i].redirect_id = this.tournament_request[i].tournament_id;
        this.tournament_request[i].name = this.tournament_request[i].tournament_name;
        this.tournament_request[i].logo = this.tournament_request[i].tournament_logo;
        this.tournament_request[i].id = this.tournament_request[i].tournament_id;
      }
    }
    this.sentData = this.open_match.concat(this.player_request, this.referee_request, this.tournament_request);
    this.open_match = [];
    this.player_request = [];
    this.referee_request = [];
    this.tournament_request = [];

  }

  addRecievedDetailField() {
    if (this.match) {
      let len = this.match.length;
      for (let i = 0; i < len; i++) {
        this.match[i].type = "open";
        this.match[i].redirect_id = this.match[i].team2.team_id;
      }
    }
    if (this.player) {
      let len = this.player.length;
      for (let i = 0; i < len; i++) {
        this.player[i].type = "player";
        this.player[i].redirect_id = this.player[i].id;
        this.player[i].id = this.player[i].apply_team_id;
      }
    }
    this.recievedData = this.match.concat(this.player);
    this.player = [];
    this.match = [];

  }

  activateSent() {
    this.status = "sent";
    this.getRequest("sent");
  }

  activateInvite() {
    this.status = "invite";
    this.getRequest("received");

  }

  actionRequest(type: string, id: any, action: string) {
    this.showLoader = true;
    this.actionType = "";
    if (action != null) {
      let form_type: string = '';
      switch (type) {
        case "open":
          form_type = "open_match";
          break;
        case "player":
          form_type = "player_request";
          break;
        case "referee":
          form_type = "referee_request";
          break;
        case "tournament":
          form_type = "tournament_request";
          break;
        case "match":
          form_type = "match"
          break;
        case "player1":
          form_type = "player";
      }

      let url = this.global.base_path + "api/team/teamAction/" + id + "/?format=json";
      let data = { id: this.team_id, option: form_type, action: action };
      this.global.PutRequest(url, data)
        .subscribe(res => {
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: 'Request', detail: 'exitosamente!' });
          if (action == 'cancel') {
            this.getRequest("sent");
          } else {
            this.getRequest('received');
          }
        },
          err => {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: 'Request', detail: 'Algunos errores no podía cancelar!' });
          });
    }
    this.showLoader = false;

  }


  delete(type, id, action) {
  }

  redirectToProfile(type, id) {
    let redirectUrl = "";
    if (type == "player" || type == "referee")
      this.router.navigate(["/dashboard/player/player-profile/" + id]);
    else if (type == "tournament")
      this.router.navigate(["/dashboard/tournaments/tournament-profile/" + id]);
    else if (type == "open")
      this.router.navigate(["/dashboard/team/team-profile/" + id]);

  }

  redirectToOpenMatch(id) {
    this.router.navigate(["/dashboard/match-profile/" + id, "open_match"]);

  }
  redirectToTournament(id) {
    this.router.navigateByUrl("dashboard/tournaments/tournament-profile/" + id);
  }
  openMatchAction(id, action) {
    this.actionType = '';
    let url = this.global.base_path + "api/team/teamAction/" + id + "/?format=json";
    let data = { id: this.team_id, option: 'team_request', action: action };
    this.global.PutRequest(url, data)
      .subscribe(res => {
        this.msgs = [];
        this.msgs.push({ severity: "info", summary: '', detail: 'exitosamente' });
        this.getRequest('received');
      }, err => {
        this.msgs = [];
        this.msgs.push({ severity: "error", summary: '', detail: 'Error' });
      });
  }
  cancelOpenMatch(id) {
    let url = this.global.base_path + "api/team/teamAction/" + id + "/?format=json";
    let data = { "option": "team_request", "action": "cancel" };
    this.global.PutRequest(url, data)
      .subscribe(res => {
        this.msgs = [];
        this.msgs.push({ severity: "info", summary: "", detail: "exitosamente" });
        this.getRequest("sent");
      }, err => {
        this.msgs = [];
        this.msgs.push({ severity: "error", summary: "", detail: "Error" });
      })
  }
  actionRequestTournament(id, labelValue: string) {
    this.showLoader = true
    this.actionTypeTournament = "";
    let url = this.global.base_path + "api/team/teamAction/" + id + "/";
    let data = {
      "option": "tournament_to_team",
      "action": labelValue
    }
    this.global.PutRequest(url, data)
      .subscribe(res => {
        this.showLoader = false;
        if (labelValue == "accepted") {
          this.getRequest("received");
        } else {
          this.getRequest("sent");
        }

        this.msgs.push({ severity: "info", detail: "Actualizado exitosamente" });
      }, err => {
        this.showLoader = false;
        this.msgs.push({ severity: "error", detail: "Error" });
      })
  }
}

