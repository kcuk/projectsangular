import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamhistoryComponent } from './teamhistory.component';

describe('TeamhistoryComponent', () => {
  let component: TeamhistoryComponent;
  let fixture: ComponentFixture<TeamhistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamhistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamhistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
