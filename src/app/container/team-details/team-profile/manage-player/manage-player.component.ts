import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  Validators,
  FormControl,
  FormBuilder
} from "@angular/forms";
import { Message } from "primeng/primeng";
import { ActivatedRoute, Router } from "@angular/router";
import { GlobalService } from "./../../../../GlobalService";
import { TranslateService } from "ng2-translate";
import { Http } from "@angular/http";

@Component({
  selector: "app-manage-player",
  templateUrl: "./manage-player.component.html",
  styleUrls: ["./manage-player.component.css"]
})
export class ManagePlayerComponent implements OnInit {
  mask: any = "999999";
  national_id: string = "";
  email: any;
  email_exist: boolean;
  check_email: boolean;
  fake_name_valid: boolean;
  fakename: any;
  fake_name_error: boolean;
  fullname: any;
  full_name_valid: boolean;
  full_name_error: boolean;
  isMobValid: boolean;
  public title = "Manage Player!";
  public msgs: Message[] = [];
  public managedata;
  public player_list: any[];
  public emailExists: boolean = false;
  public error: boolean = false;
  public data: any;
  public team_player: any;
  public team_id: number;
  public shirt_no: number;
  public id: any;
  public Manage_player: FormGroup;
  public loader: boolean = false;
  public filteredBrands: any[];
  public brand: string;
  public basePath: string;
  public teamid: number;
  public country_code: string = "+593";
  public statusCheck: boolean = false;
  public selectedValue: string = "Email";
  public selectedName: string = "";
  public fakeNameTest: boolean = false;
  public fakeName_Test: boolean = false;
  public fakelnameTest: boolean = false;
  code: any = [
    { value: "+1", label: "+1" },
    { value: "+502", label: "+502" },

    { value: "+593", label: "+593" }
  ];
  length: any = 9;

  constructor(
    public http: Http,
    public global: GlobalService,
    public translate: TranslateService,
    public router: Router,
    public route: ActivatedRoute,
    public base_path_service: GlobalService,
    public frm: FormBuilder
  ) {
    this.managedata = new manageData();

    this.Manage_player = frm.group({
      email: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(
            "[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+"
          )
        ])
      ),
      // mobile: new FormControl('', Validators.compose([Validators.required])),
      name: new FormControl("", Validators.compose([Validators.required])),
      // lname: new FormControl('', Validators.compose([Validators.required])),
      // country_code: new FormControl('+593', Validators.compose([])),
      FakeName: new FormControl("", Validators.compose([])),
      national_id: new FormControl("")

      // Fakelname: new FormControl('', Validators.compose([]))
    });
    if (localStorage.getItem("language")) {
      translate.use(localStorage.getItem("language"));
    }
  }

  ngOnInit() {
    this.loader = true;
    this.basePath = this.base_path_service.image_url;
    let sub = this.route.params.subscribe(params => {
      this.team_id = +params["id"];
    });
    this.getPlayerInfo();
  }
  public getPlayerInfo() {
    let url =
      this.base_path_service.base_path_api() +
      "user/profile/?form_type=team_player&team=" +
      this.team_id +
      "&format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.loader = false;
        this.team_player = res[0].json;
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: "error", detail: "Algunos errores!" });
      }
    );
  }
  public filterBrands(event) {
    let query = event.query;
    let url =
      this.base_path_service.base_path_api() +
      "user/profile/?form_type=manage_profie&value=" +
      query +
      "&team=" +
      this.team_id +
      "&format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.player_list = res[0].json;
      },
      err => {
        this.msgs = [];
        this.msgs.push({ severity: "error", detail: "Algunos errores!" });
      }
    );
  }

  public get_player() {
    let url =
      this.base_path_service.base_path_api() +
      "user/profile/?form_type=team_player&team=" +
      this.team_id +
      "&format=json";
    this.base_path_service.GetRequest(url).subscribe(res => {
      console.log(res);
      this.team_player = res[0].json;
      this.full_name_valid = false;
      this.check_email = false;
      this.fullname = "";
      this.email = "";
      this.national_id = "";
      this.fakename = "";
      this.fake_name_error = false;
      this.fake_name_valid = false;
    });
  }

  public invite(id) {
    console.log(id);
    this.loader = true;
    let url =
      this.base_path_service.base_path_api() +
      "user/player_request/?format=json";
    let data = {
      player: id,
      team: this.team_id
    };
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        this.get_player();
        this.msgs = [];
        this.msgs.push({
          severity: "info",
          summary: "",
          detail: "Con éxito reproductor invitado"
        });
        this.loader = false;
        console.log("response", res.response);
      },
      err => {
        this.msgs = [];
        this.msgs.push({ severity: "error", detail: "Algunos errores!" });
        this.loader = false;
      }
    );
  }

  public delete(id) {
    this.loader = true;
    let url =
      this.base_path_service.base_path_api() +
      "user/player/" +
      id +
      "/?team=" +
      this.team_id +
      "&format=json";
    this.base_path_service.DeleteRequest(url).subscribe(
      res => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "info",
          summary: "",
          detail: "Jugador eliminado con éxito"
        });
        this.get_player();
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: "error", detail: "Algunos errores!" });
      }
    );
  }
  public update(id, shirt, national) {
    this.loader = true;
    let url =
      this.base_path_service.base_path_api() +
      "user/player/" +
      id +
      "/?format=json";
    let data = {
      form_type: "shirt_no",
      shirt_no: shirt,
      team: this.team_id,
      national_id: national.trim()
    };
    this.base_path_service.PutRequest(url, data).subscribe(
      res => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "info",
          summary: "",
          detail: "Actualizado con éxito reproductor"
        });
      },
      err => {
        this.loader = false;
      }
    );
  }
  public enable(event) {
    this.shirt_no = event.target.value;
  }

  public get_id(id) {
    this.id = id;
  }
  public terminate() {
    this.router.navigateByUrl("/dashboard/team/team-profile/" + this.team_id);
  }
  public fakeNameTesting(fakeName) {
    if (fakeName.FakeName && fakeName.FakeName.length == 0) {
      this.fakeNameTest = true;
    } else {
      this.fakeNameTest = false;
    }

    if (fakeName.Fakelname && fakeName.Fakelname.length == 0) {
      this.fakelnameTest = true;
    } else {
      this.fakelnameTest = false;
    }

    if (
      fakeName.FakeName &&
      fakeName.FakeName.length >= 1 &&
      fakeName.Fakelname &&
      fakeName.Fakelname.length >= 1
    ) {
      this.fakeName_Test = true;
    } else {
      this.fakeName_Test = false;
    }
  }

  validateFullName(event) {
    this.full_name_error = false;
    if (event.target.value.length > 40) {
      this.fullname = event.target.value.slice(0, 40);
    }
    if (
      /^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$/.test(
        this.fullname
      )
    ) {
      this.full_name_valid = true;
    } else {
      this.full_name_valid = false;
    }
  }

  validateFakeName(event) {
    this.fake_name_error = false;
    if (event.target.value.length > 40) {
      this.fakename = event.target.value.slice(0, 40);
    }
    if (
      /^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$/.test(
        this.fakename
      )
    ) {
      this.fake_name_valid = true;
    } else {
      this.fake_name_valid = false;
    }
  }

  validate(event) {
    if (
      /[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+$/.test(
        this.email
      )
    ) {
      this.check_email = true;
      this.email_exist = false;
    } else {
      this.check_email = false;
      this.email_exist = false;
    }
  }

  public createdata() {
    if (
      /^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$/.test(
        this.fullname
      )
    ) {
      this.validateEmail();
    } else {
      this.full_name_error = true;
    }
  }

  validateEmail() {
    this.loader = true;
    let url =
      this.global.base_path_api() +
      "peloteando/verification/?email=" +
      this.email +
      "&form_type=invite" +
      "&format=json";
    this.http.get(url).subscribe(
      res => {
        this.sendData();
      },
      err => {
        this.loader = false;
        this.email_exist = true;
        this.check_email = false;
      }
    );
  }

  sendData() {
    this.data = {
      form_type: "create",
      name: this.fullname,
      email: this.email,
      team: this.team_id,
      national_id: this.national_id.trim()
    };
    this.loader = true;
    var url =
      this.base_path_service.base_path_api() + "user/player/?format=json";
    this.base_path_service.PostRequest(url, this.data).subscribe(
      res => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "info",
          summary:
            localStorage.getItem("language") == "es"
              ? "Solicitud enviada"
              : "Request sent"
        });
        this.get_player();
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary:
            localStorage.getItem("language") == "es"
              ? "Error, inténtalo de nuevo más tarde"
              : "Error,Try again later"
        });
      }
    );
  }

  public postFakePlayerData() {
    if (
      /^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$/.test(
        this.fakename
      )
    ) {
      this.sendFakeData();
    } else {
      this.fake_name_error = true;
    }
  }

  sendFakeData() {
    var data = {
      form_type: "fake",
      name: this.fakename,
      team: this.team_id,
      national_id: this.national_id.trim()
    };
    this.loader = true;
    var url =
      this.base_path_service.base_path_api() + "user/dummyplayer/?format=json";
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        this.loader = false;
        this.get_player();
      },
      err => {
        this.loader = false;
      }
    );
  }
}
class manageData {
  "form_type": string = "create";
  "email": string = "";
  "name": string = "";
  "mobile": number = null;
  "lname": string = "";
}

class User {
  public password: string = "";
  public email: string = "";
  public name: string = "";
  public last_name: string = "";
  public mobile: string = "";
  public facebook_id = "";
  public country_code: string = "+593";
}
