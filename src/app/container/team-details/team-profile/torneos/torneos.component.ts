import { Component, OnInit } from '@angular/core';
import {Message} from 'primeng/primeng';
import {Router,ActivatedRoute} from '@angular/router';
import { GlobalService } from './../../../../GlobalService';


@Component({
  selector: 'app-torneos',
  templateUrl: './torneos.component.html',
  styleUrls: ['./torneos.component.css']
})
export class TorneosComponent implements OnInit {
  status: string = "player";
  referee: any;
  team_id;
  isAdmin: boolean;
  player: Array<any> = [];
  tournament: Array<any> = [];
  msgs: Message[] = [];
  loader: boolean = false;
  basePath: any;

  constructor(public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute) {
  }



  ngOnInit() {
    let sub = this.route.parent.params.subscribe(params => {
      this.team_id = +params['id'];
    });
    this.basePath = this.base_path_service.image_url;
    this.getTournament();
  }

  getTournament() {
    this.loader = true;
    this.tournament = [];
    this.player = [];
    this.status = "tour";
    let url = this.base_path_service.base_path + "api/team/profile/" + this.team_id + "/?form_type=posesiones&option=torneos&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;

        this.tournament = res[0].json;
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: "error", summary: "", detail: "Error in getting tournaments" });
      });

  }

}

