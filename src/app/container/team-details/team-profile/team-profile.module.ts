import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { teamProfileRouting } from "./team-profile.routes";
import { TeamProfileComponent } from "./team-profile.component";
import { SharedModule } from "./../../../shared/shared.module";
import { TorneosComponent } from "./torneos/torneos.component";
import { TeamScheduleComponent } from "./team-schedule/team-schedule.component";
import { TeamRequestComponent } from "./team-request/team-request.component";
import { TeamInformationComponent } from "./team-information/team-information.component";
import { TeamJugadoresComponent } from "./team-jugadores/team-jugadores.component";
import { EditTeamComponent } from "./../edit-team/edit-team.component";
import { ManagePlayerComponent } from "./manage-player/manage-player.component";
import { ShareButtonsModule } from "ngx-sharebuttons";
import { TeamhistoryComponent } from "./teamhistory/teamhistory.component";

import {AccordionModule} from 'primeng/primeng';

@NgModule({
  imports: [
    teamProfileRouting,
    ShareButtonsModule.forRoot(),
    SharedModule.forRoot(),
    AccordionModule
  ],
  declarations: [
    EditTeamComponent,
    TeamhistoryComponent,
    TeamProfileComponent,
    TorneosComponent,
    TeamScheduleComponent,
    TeamRequestComponent,
    TeamInformationComponent,
    TeamJugadoresComponent,
    ManagePlayerComponent
  ]
})
export class TeamProfileModule {}
