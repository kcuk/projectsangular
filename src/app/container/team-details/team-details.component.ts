import { Component, OnInit, HostListener, Inject } from "@angular/core";
import { Message } from "primeng/primeng";
import { Router } from "@angular/router";
import { GlobalService } from "./../../GlobalService";
import { DOCUMENT } from "@angular/platform-browser";
import { TranslateService } from "ng2-translate";
declare const $;
@Component({
  selector: "app-team-details",
  templateUrl: "./team-details.component.html",
  styleUrls: ["./team-details.component.css"]
})
export class TeamDetailsComponent implements OnInit {
  display: boolean = false;
  backdrop: boolean = false;
  challengeDate: any;
  getId: number;
  cards: boolean = true;
  showBtn: boolean = false;
  disablebtn: boolean = false;
  invitedTeamId: number;
  registerPopup: boolean = false;
  public msgs: Message[] = [];
  public teams: Array<any> = [];
  dateValid: boolean = false;
  public teams1: any;
  public len: number;
  public selectedCityId: any = "";
  public format: Array<string> = [];
  public formatDefault: boolean = true;
  public location: Array<any> = [];
  public locality: Array<any> = [];
  public locationDefault: boolean = true;
  public rating: Array<any> = [];
  public cities: Array<any> = [{ label: "Select City", value: "" }];
  public myTeam: boolean = false;
  public counter: number = 1;
  public activateLoader: boolean = false;
  public basePath: string = this.global.image_url;
  public isdata: boolean = false;
  public disable1: boolean = false;
  public isError: boolean = false;
  public nameSelected: String = "";
  public cityPlaceholder: string = "Ciudad";
  public show: boolean = true;
  public page: number = 1;
  public paramsSub: any;
  public attitu: Array<any> = [];
  public tech: Array<any> = [];
  public cityFlag: boolean = false;
  public gender: Array<any> = [];
  public from_filter: string = "";
  comdata: any;
  communityapiid: any;
  userexist: boolean = false;

  constructor(
    public translate: TranslateService,
    @Inject(DOCUMENT) public document: Document,
    public global: GlobalService,
    public router: Router
  ) {
    localStorage.removeItem("court_id");
    localStorage.removeItem("courts");
    localStorage.removeItem("court_information");
    localStorage.removeItem("field_info");
    if (localStorage.getItem("language")) {
      translate.use(localStorage.getItem("language"));
    }
    if (localStorage.getItem("user_info")) {
      this.userexist = true;
    } else {
      this.userexist = false;
    }
  }

  ngOnInit() {
    if (localStorage.getItem("community_info")) {
      this.comdata = JSON.parse(localStorage.getItem("community_info"));
      this.communityapiid = this.comdata.community_id;
    } else {
      this.communityapiid = localStorage.getItem("communityid");
      console.log(this.communityapiid, "communityapiid===========");
    }
    this.trackstatus();
    this.mob();
  }
  @HostListener("window:scroll", ["$event"])
  track(event: any) {
    let number = this.document.body.scrollHeight;
    let totalNumber = window.pageYOffset + window.innerHeight;
    if (number == Math.ceil(totalNumber)) {
      this.from_filter = "";
      this.page = this.page + 1;
      if (this.myTeam) {
        if (this.page <= this.len) {
          this.from_filter = "";
          this.myTeams(this.page);
        }
      } else {
        if (this.page <= this.len) {
          this.from_filter = "";
          this.getTeam(this.page);
        }
      }
    }
  }

  mob() {
    let mq = window.matchMedia("screen and (min-width:640px)");
    if (mq.matches) {
      this.show = false;
    } else {
      this.show = true;
    }
  }

  trackstatus() {
    this.getTeam(1);
  }

  showDialog(id) {
    this.invitedTeamId = id;
    this.display = true;
    this.backdrop = true;
  }

  paginate(event) {
    this.teams = [];
    let page = event.page + 1;
    this.page = page;
    if (this.myTeam) this.myTeams(page);
    else this.getTeam(page);
  }
  reset() {
    if (!this.myTeam) {
      this.teams = [];
    }
  }
  getTeam(page: any) {
    this.page = page;
    this.msgs = [];
    this.activateLoader = true;
    this.isError = false;
    this.isdata = false;
    let x = this.format;
    let y = this.location;
    let z = this.rating;

    let data = {
      form_type: "filter",
      team_name: this.nameSelected,
      attitude: this.attitu,
      technique: this.tech,
      city: this.selectedCityId,
      page: page,
      gender: this.gender,
      latitude: localStorage.getItem("lat"),
      longitude: localStorage.getItem("long"),
      distance: parseInt(localStorage.getItem("distance")),
      community_id: this.communityapiid
    };
    let url = this.global.base_path + "api/team/profile/?format=json";

    if (this.userexist) {
      this.global.PostRequest(url, data).subscribe(
        res => {
          this.mob();
          this.cards = true;
          this.activateLoader = false;
          if (this.from_filter != "") {
            this.teams = [];
            this.teams.push(res[0].json.teams);
          } else {
            this.teams.push(res[0].json.teams);
          }
          this.len = res[0].json.total_pages;
          if (this.teams[0].length < 1) this.isdata = true;
        },
        err => {
          this.isError = true;
          this.msgs.push({
            severity: "error",
            summary: "Error",
            detail: "No se pudo obtener equipos Actualiza la página .."
          });
          this.activateLoader = false;
        }
      );
    } else {
      this.global.PostRequestUnauthorised(url, data).subscribe(
        res => {
          this.mob();
          this.cards = true;
          this.activateLoader = false;
          if (this.from_filter != "") {
            this.teams = [];
            this.teams.push(res[0].json.teams);
          } else {
            this.teams.push(res[0].json.teams);
          }
          this.len = res[0].json.total_pages;
          if (this.teams[0].length < 1) this.isdata = true;
        },
        err => {
          this.isError = true;
          this.msgs.push({
            severity: "error",
            summary: "Error",
            detail: "No se pudo obtener equipos Actualiza la página .."
          });
          this.activateLoader = false;
        }
      );
    }
  }

  modalityFilter(x: boolean): any {
    if (!this.myTeam) {
      if (x) {
        this.format = [];
        this.formatDefault = true;
      } else {
        if (this.format.length == 6) {
          this.formatDefault = true;
          this.format = [];
        } else if (this.format.length == 0) {
          this.formatDefault = true;
          this.format = [];
        } else if (this.format.length == 1) this.formatDefault = false;
      }
      this.getTeam(1);
    }
  }
  fun() {
    this.showBtn = true;
    this.cards = false;
  }

  searchCity(event) {
    this.msgs = [];

    this.cities = [];
    let query = event.query;
    if (!query) {
      this.selectedCityId = "";
      this.getTeam(1);
    } else {
      let url =
        this.global.base_path +
        "api/team/location/?country=1&city=" +
        query +
        "&format=json";
      this.global.GetRequest(url).subscribe(
        res => {
          this.cities = [{}];
          this.activateLoader = false;
          let x = res[0].json;
          for (let i = 0; i < x.length; i++) {
            this.cities.push({ label: x[i].location, value: x[i].location });
          }
        },
        err => {
          this.activateLoader = false;
        }
      );
    }
  }

  locationFilter(event) {
    this.teams = [];
    this.selectedCityId = event.value;
    this.cityFlag = true;
    this.getTeam(1);
  }

  myTeams(pg) {
    this.page = pg;
    this.isdata = false;
    this.activateLoader = true;
    this.isError = false;
    if (this.myTeam) {
      this.disable1 = true;
      let url =
        this.global.base_path +
        "api/team/profile/?value=myteam&page=" +
        this.page +
        "&format=json";
      this.global.GetRequest(url).subscribe(
        res => {
          this.activateLoader = false;
          this.mob();
          this.cards = true;
          this.teams.push(res[0].json.teams);
          this.len = res[0].json.total_pages;
          if (res[0].json.teams.length < 1) this.isdata = true;
        },
        err => {
          this.isError = true;
          this.msgs.push({
            severity: "error",
            summary: "Error",
            detail: "No se pudo obtener Mis equipos Actualiza la página .."
          });
          this.activateLoader = false;
        }
      );
    } else {
      this.disable1 = false;
      this.getTeam(1);
    }
  }

  myTeamsModal(pg) {
    let url =
      this.global.base_path +
      "api/team/profile/?value=myteam&page=" +
      pg +
      "&format=json";
    this.global.GetRequest(url).subscribe(
      res => {
        this.teams1 = res[0].json.teams;
        if (this.teams1.length == 0) this.disablebtn = true;
      },
      err => {}
    );
  }

  submitChallenge() {
    if (typeof this.challengeDate == undefined) {
      this.dateValid = true;
    } else {
      let myJson = {
        challenger_team: this.getId,
        invited_team: this.invitedTeamId,
        date_time: this.challengeDate
      };

      let url = this.global.base_path + "api/scorecard/challenge/";
      this.global.PostRequest(url, myJson).subscribe(
        res => {
          this.teams1 = res[0].json.teams;
          this.msgs.push({
            severity: "info",
            summary: "Info Message",
            detail: "desafiar con éxito"
          });
          this.display = false;
          this.backdrop = false;
        },
        err => {
          this.msgs.push({
            severity: "error",
            summary: "Error Message",
            detail: "No se pudo desafiar"
          });
        }
      );
    }
  }
  cancel() {
    this.backdrop = false;
    this.display = false;
  }

  getChallengeId(id) {
    this.getId = id;
  }

  nameFilter(name) {
    this.teams = [];
    this.from_filter = "filter";
    this.getTeam(1);
  }

  follow(team: any) {
    this.activateLoader = true;
    let url = this.global.base_path + "api/user/profile/";
    let player_data = {
      form_type: "follow_team",
      follow_team: team.id
    };
    this.global.PostRequest(url, player_data).subscribe(
      res => {
        this.msgs = [];
        this.msgs.push({
          severity: "info",
          summary: "Followed",
          detail: "Successfully"
        });
        team.follow = !team.follow;
        team.flag = true;
      },
      err => {
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "Could not follow"
        });
      }
    );
    this.activateLoader = false;
  }

  unfollow(team: any) {
    console.log("hello ", team);
    this.activateLoader = true;
    let url =
      this.global.base_path +
      "api/user/team_follow/" +
      team.id +
      "/?format=json";

    this.global.DeleteRequest(url).subscribe(
      res => {
        this.msgs = [];
        this.msgs.push({
          severity: "info",
          summary: "Un Followed",
          detail: "Successfully"
        });
        team.follow = !team.follow;
        team.flag = true;
      },
      err => {
        this.msgs = [];
        this.msgs.push({
          severity: "info",
          summary: "",
          detail: "Some error in unfollow"
        });
      }
    );
    this.activateLoader = false;
  }

  profileRedirect(teamid: string) {
    // this.router.navigate(['/dashboard'])
    let comdata = JSON.parse(localStorage.getItem("community_info"));
    let comname = comdata.community_name;
    this.router.navigateByUrl(comname + "/team/team-profile/" + teamid);
  }
}
