import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTeamFirstComponent } from './create-team-first.component';

describe('CreateTeamFirstComponent', () => {
  let component: CreateTeamFirstComponent;
  let fixture: ComponentFixture<CreateTeamFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTeamFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTeamFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
