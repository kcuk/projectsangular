import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Message } from 'primeng/primeng';
import { GlobalService } from './../../../GlobalService';
import { Router } from '@angular/router';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { TranslateService } from "ng2-translate";
declare var google;


@Component({
  selector: 'app-create-team-first',
  templateUrl: './create-team-first.component.html',
  styleUrls: ['./create-team-first.component.css']
})
export class CreateTeamFirstComponent implements OnInit {

  checkCity: boolean=true;
  long: any;
  lat: any;
  country_name: any;
  autocompleteItems: any[];
  GoogleAutocomplete: any;
  loader: boolean = false;
  CreateTeamForm: FormGroup;
  coverPicList: string;
  cover_url: string;
  coverPic: "";

  circleImg: boolean = false;
  squareImg: boolean = false;
  cover_id: number = 0;
  msgs: Message[];
  brand: string;
  notification_logo: string = '';
  url: string = '';
  team_id: number = null;
  country_id: number = null;
  country: any[];
  countries: any[];
  num: number;
  filteredCountriesSingle: any[];
  filteredCountries: any[] = [];
  city: any[];
  City: string;
  city_id: number = null;
  city_name: string;
  town: any[];
  filteredtown: any[];
  country_required: boolean = false;
  city_required: boolean = false;
  gender: any[];
  player_list: any[];
  data: any;
  cropperSettings: CropperSettings;
  data2: any;
  cropperSettings2: CropperSettings;
  display: boolean = false;
  enable: boolean = false;
  logo: string;
  file_name: string;
  image_name: string = '';
  lang = localStorage.getItem('language')

  @ViewChild('cropper', undefined) cropper: ImageCropperComponent;


  constructor(public zone: NgZone,
    public translate: TranslateService, frm: FormBuilder, public base_path_service: GlobalService, public router: Router) {
    this.lat = localStorage.getItem('lat');
    this.long = localStorage.getItem('long');
    let city = {
      description: localStorage.getItem('city')
    }
    this.country_name = localStorage.getItem('country_name')
    this.city_name = localStorage.getItem('city');
    this.rectangleCropImage();
    this.circleCropImage();
    this.gender = []
    this.gender.push({ label: 'Seleccionar tipo', value: null });
    this.gender.push({ label: 'Masculino', value: 'Masculino' });
    this.gender.push({ label: 'Femenino', value: 'Femenino' });
    this.gender.push({ label: 'Mixto', value: 'Mixto' });

    this.CreateTeamForm = frm.group({
      Name: ['', Validators.compose([Validators.required])],
      Motto: ['', Validators.compose([])],
      // City: ['', Validators.compose([Validators.required])],
      // country: ['', Validators.compose([Validators.required])],
      Bio: ['', Validators.compose([])],
      // Parish: ['', Validators.compose([])],
      // brands: ['', Validators.compose([])],
      team_type: ['', Validators.compose([Validators.required])],
      selectCity: [city]

    })
    if (localStorage.getItem('language')) {
      translate.use(localStorage.getItem('language'))
    }
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();

  }

  ngOnInit() {
    this.loader = true;
    let url = this.base_path_service.base_path + "api/user/cover_pic/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.coverPicList = res[0].json;
        this.cover_url = this.base_path_service.image_url;
      });
    // let url1 = this.base_path_service.base_path + "api/team/country/?format=json";
    // this.base_path_service.GetRequest(url1)
    //   .subscribe(res => {
    //     this.country = res[0].json;
    //     this.num = res[0].json.length;
    //     this.fun();
    //   });
    this.loader = false;
  }
  // fun() {
  //   this.countries = [];
  //   this.countries.push({ label: 'Seleccionar pais', value: null });
  //   for (let i = 0; i < this.country.length; i++) {
  //     this.countries.push({ label: this.lang == 'en' ? this.country[i].country_name : this.country[i].country_spanish, value: this.country[i].id });
  //   }
  // }

  // selectedcountryFun(event) {
  //   this.country_required = false;
  //   this.country_id = event.value;
  // }

  // filterCountrySingle(event) {
  //   if (this.country_id != null) {
  //     let query = event.query;
  //     if (query.length >= 2) {
  //       let url2 = this.base_path_service.base_path + "api/team/location/?country=" + this.country_id + "&city=" + query + '&format=json';
  //       this.base_path_service.GetRequest(url2)
  //         .subscribe(res => {
  //           this.city = res[0].json;
  //           this.filteredCountries = this.filterCountry(query, this.city);
  //         });
  //     }
  //   }
  //   else {
  //     this.country_required = true;
  //   }
  // }

  // filterCountry(query, countries: any[]): any[] {
  //   let filtered: any[] = [];
  //   for (let i = 0; i < countries.length; i++) {
  //     let country = countries[i];
  //     if (country.location.toLowerCase().indexOf(query.toLowerCase()) == 0) {
  //       filtered.push({ label: country.location, value: country.id });
  //     }
  //   }
  //   return filtered;
  // }
  // selectedcityFun(event) {
  //   if (event.value != 'undefined') {
  //     this.city_name = event.label;
  //     this.city_id = event.value;
  //   }
  // }

  // filtertownSingle(event) {
  //   let query = event.query;
  //   if (query.length >= 2) {
  //     let url3 = this.base_path_service.base_path + "api/team/location/?form_type=town&city_pk=" + this.city_id + "&town=" + query + '&format=json';
  //     this.base_path_service.GetRequest(url3)
  //       .subscribe(res => {
  //         this.filteredtown = res[0].json;
  //       });
  //   }
  // }

  fileChangeEvent(fileInput: any) {
    this.notification_logo = fileInput.target.files[0];
  }

  get_image(pic, id) {
    if (this.cover_id == id) {
      this.cover_id = 0;
    } else {
      this.cover_id = id;
      this.url = pic;
    }
  }

  cancel() {
    this.router.navigateByUrl('/dashboard/team');
  }

  onSubmitXhr(value) {
    this.loader = true;
    if (this.CreateTeamForm.valid && this.checkCity) {
      let flag = true;
      // if (this.country_id !== null) {
      //   flag = false;
      //   if (this.filteredCountries != null) {
      //     for (let i = 0; i < this.filteredCountries.length; i++) {
      //       if (this.city_name == this.filteredCountries[i].label)
      //         flag = true;
      //     }
      //   }
      // }
      // if (flag) {
      let token = JSON.parse(localStorage.getItem('userInfo'));
      let url = this.base_path_service.base_path + 'api/team/profile/?format=json'
      return new Promise((resolve, reject) => {
        var formData: any = new FormData();
        var xhr = new XMLHttpRequest();
        formData.append("form_type", "newpeloteando");
        formData.append("team_name", value.Name);
        formData.append("team_type", value.team_type);
        formData.append("moto", value.Motto);
        formData.append("bio", value.Bio);
        formData.append("city_name", this.city_name);
        formData.append("country_name", this.country_name);
        formData.append("latitude", this.lat);
        formData.append("longitude", this.long);
        formData.append("cover_image", this.cover_id);
        formData.append("team_logo", this.notification_logo);
        xhr.onreadystatechange = () => {
          if (xhr.readyState == 4) {
            if (xhr.status == 200 || xhr.status == 201 || xhr.status == 205) {
              resolve(JSON.parse(xhr.response));
              this.loader = false;
              this.team_id = JSON.parse(xhr.response).teamId;
              this.msgs = [];
              this.msgs.push({ severity: 'info', summary: '', detail: 'Creado con éxito!' });
              this.router.navigateByUrl('/dashboard/team/create-team/' + this.team_id)
            } else {
              this.msgs = [];
              this.msgs.push({ severity: 'error', summary: '', detail: 'algún error!' });
              this.loader = false;
              reject(xhr.response);

            }
          }
        }
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Authorization", 'Bearer ' + token.token.access_token);
        xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
        xhr.send(formData);
      });
      // }
      // else {
      //   if (this.CreateTeamForm.controls['City'].value == '') {
      //     this.loader = false;
      //     this.msgs = [];
      //     this.msgs.push({ severity: 'error', summary: '', detail: 'Ciudad requerido' });
      //   }
      //   else {
      //     this.loader = false;
      //     this.msgs = [];
      //     this.msgs.push({ severity: 'error', summary: '', detail: 'ciudad inavalid' });
      //   }
      // }
    }
    else {

      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: '', detail: 'Ingresa todos los campos obligatorios' });

    }
  }


  fileChangeListener(event, cover) {
    this.enable = true;
    this.image_name = event.target.files[0].name;
    this.coverPic = cover;
    if (cover == 'coverPic') {
      this.circleImg = false;
      this.squareImg = true;
    } else {
      this.circleImg = true;
      this.squareImg = false;
    }
    var image: any = new Image();
    var file: File = event.target.files[0];
    var myReader: FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
      that.logo = image.src.split(",")[1];
    };
    myReader.readAsDataURL(file);
    this.showDialog();
  }
  showDialog() { this.display = true; this.enable = true; }
  disable() { this.display = false; this.enable = false; }
  save() { this.notification_logo = this.logo; this.image_name = this.file_name; this.display = false; this.enable = false; }

  public rectangleCropImage() {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 1200;
    this.cropperSettings.height = 250;
    this.cropperSettings.keepAspect = false;

    this.cropperSettings.croppedWidth = 1200;
    this.cropperSettings.croppedHeight = 250;

    this.cropperSettings.canvasWidth = 350;
    this.cropperSettings.canvasHeight = 300;

    this.cropperSettings.minWidth = 100;
    this.cropperSettings.minHeight = 100;

    this.cropperSettings.rounded = false;
    this.cropperSettings.minWithRelativeToResolution = false;

    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings.noFileInput = true;

    this.data = {};
  }
  public circleCropImage() {
    this.cropperSettings2 = new CropperSettings();
    this.cropperSettings2.width = 200;
    this.cropperSettings2.height = 200;
    this.cropperSettings2.keepAspect = false;

    this.cropperSettings2.croppedWidth = 200;
    this.cropperSettings2.croppedHeight = 200;

    this.cropperSettings2.canvasWidth = 350;
    this.cropperSettings2.canvasHeight = 300;

    this.cropperSettings2.minWidth = 100;
    this.cropperSettings2.minHeight = 100;

    this.cropperSettings2.rounded = true;
    this.cropperSettings2.minWithRelativeToResolution = true;

    this.cropperSettings2.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings2.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings2.noFileInput = true;

    this.data2 = {};
  }

  search(event) {
    this.checkCity = false;
    console.log("valussssssssss", event)
    this.GoogleAutocomplete.getPlacePredictions({ input: event.query, types: ['(regions)'] },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
          console.log("list", this.autocompleteItems)
        });
      });
  }

  setLoaction(item) {
    console.log("item=>>>>>>>>>>>>>>>>", item);
    this.checkCity = true;
    this.country_name = item.terms.pop().value;
    this.city_name = item.terms[0].value;
    this.geoCode((item.description));

  }

  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.lat = results[0].geometry.location.lat();
      this.long = results[0].geometry.location.lng()
    })
  }

}





