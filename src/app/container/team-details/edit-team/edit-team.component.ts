import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from './../../../GlobalService';
import { Message } from 'primeng/primeng';
import { TranslateService } from "ng2-translate";
declare var google;

@Component({
  selector: 'app-edit-team',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.css']
})
export class EditTeamComponent implements OnInit {


 
  long: any;
  lat: any;
  city_name: any;
  country_name: any;
  autocompleteItems: any[];
  GoogleAutocomplete: any;
  checkCity: boolean=true;
  loader: boolean = false;
  team_id;
  playerList: any;
  msgs: Message[];
  cities: any[];
  Edit_Form: FormGroup;
  country_id: number = null;
  country: any[];
  countries: any[];
  num: number;
  filteredCities: any[];
  city: any[];
  city_id: number = null;
  town_id: number = null;
  town: any[];
  name: any;
  filteredtown: any[];
  data: any;
  gender: any[];
  tipo: any;
  country_required: boolean = false;

  Name: any = '';
  Motto: any = '';
  Country: any;
  City: any = '';
  Bio: any = '';
  deleteTeamPopup: boolean = false;
  label: any;
  lang = localStorage.getItem('language')

  constructor(
    public translate: TranslateService,
    frm: FormBuilder,
    public router: Router,
    public base_path_service: GlobalService, public route: ActivatedRoute, public zone: NgZone, ) {
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.gender = []
    this.gender.push({ label: 'Seleccionar tipo', value: null });
    this.gender.push({ label: 'Masculino', value: 'Masculino' });
    this.gender.push({ label: 'Femenino', value: 'Femenino' });
    this.gender.push({ label: 'Mixto', value: 'Mixto' });
    this.Edit_Form = frm.group({
      name: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(50)])),
      Motto: new FormControl('', Validators.compose([Validators.maxLength(50)])),
      // Country: new FormControl('', Validators.compose([Validators.required])),
      City: new FormControl('', Validators.compose([Validators.required])),
      team_type: new FormControl('', Validators.compose([Validators.required]))
    })
    if (localStorage.getItem('language')) {
      translate.use(localStorage.getItem('language'))
    }
  }

  ngOnInit() {
    this.loader = true;
    let sub = this.route.params.subscribe(params => {
      this.team_id = +params['id'];
      this.getTeam();
    });
    // let url1 = this.base_path_service.base_path + "api/team/country/?format=json";
    // this.base_path_service.GetRequest(url1)
    //   .subscribe(res => {
    //     this.country = res[0].json;
    //     this.num = res[0].json.length;
    //     if (res[0].status == 400) {
    //       this.msgs = [];
    //       this.msgs.push({ severity: 'error', summary: '', detail: 'Perfil del equipo no existe!' });
    //     }
    //     this.fun();
    //   }, err => {
    //     this.msgs = [];
    //     this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos Error!' });
    //   });
    this.loader = false;
  }
  // fun() {
  //   this.countries = [];
  //   this.countries.push({ label: 'Select Country', value: null });
  //   for (let i = 0; i < this.country.length; i++) {
  //     this.countries.push({ label:this.lang=='en'?this.country[i].country_name:this.country[i].country_spanish, value: this.country[i].id });
  //   }
  // }

  getTeam() {
    this.filteredCities = [];
    let url = this.base_path_service.base_path_api() + 'team/profile/?team=' + this.team_id + '&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.playerList = res[0].json.Team;
        this.Name = this.playerList.team_name
        this.Motto = this.playerList.moto;
        this.Bio = this.playerList.bio;
        // this.Country = this.playerList.city.country_id;
        // this.country_id = this.playerList.city.country_id;
        this.City = this.playerList.city;
        this.lat = this.playerList.latitude;
        this.long = this.playerList.longitude;
        // this.city_name = this.playerList.city.city_name;
        // this.city_id = this.playerList.city.city_id;
        this.tipo = this.playerList.team_type;
        this.country_name = this.playerList.city.country_name;
        this.city_name = this.playerList.city.description;
      },
        err => {
        });
  }
  // getcity() {
  //   this.filteredCities = [];
  //   if (this.country_id != null) {
  //     if (this.City.length >= 2) {
  //       let url2 = this.base_path_service.base_path + "api/team/location/?country=" + this.country_id + "&city=" + this.City + "&format=json";
  //       this.base_path_service.GetRequest(url2)
  //         .subscribe(res => {
  //           this.city = res[0].json;
  //           this.filteredCities = this.filterCity(this.City, this.city);
  //         });
  //     }
  //   }
  // }
  // selectedcountryFun(event) {
  //   if (!(event.value === this.country_id)) {
  //     this.country_id = event.value;
  //     this.City = '';
  //   }
  // }

  // filterCitySingle(event) {
  //   let query = event.query;
  //   if (this.country_id != null) {
  //     if (query.length >= 2) {
  //       let url2 = this.base_path_service.base_path + "api/team/location/?country=" + this.country_id + "&city=" + query + "&format=json";
  //       this.base_path_service.GetRequest(url2)
  //         .subscribe(res => {
  //           this.city = res[0].json;
  //           this.filteredCities = this.filterCity(query, this.city);
  //         });
  //     }
  //   }
  //   else {
  //     this.country_required = true;
  //   }
  // }
  // filterCity(query, countries: any[]): any[] {
  //   let filtered: any[] = [];
  //   for (let i = 0; i < countries.length; i++) {
  //     let country = countries[i];
  //     filtered.push({ label: country.location, value: country.id });
  //   }
  //   return filtered;
  // }
  // selectedcityFun(event) {

  //   if (!(typeof event.value === 'undefined')) {
  //     this.city_id = event.value;
  //     this.City = {id: event.value,label:event.label};
  //   }
  // }
  post() {
    if (this.Edit_Form.valid && this.checkCity) {
      // let flag = true;
      // if (this.country_id !== null) {
      //   flag = false;
      //   if (this.city_name != '') {
      //     flag = true;
      //   }
      //   else if (this.City != '') {
      //     for (let i = 0; i < this.filteredCities.length; i++) {
      //       if (this.filteredCities[i].label === this.City) {
      //         flag = true;
      //       }
      //     }
      //   }
      // }
      // if (flag) {
        this.loader = true;
        let url = this.base_path_service.base_path + 'api/team/profile/' + this.team_id + '/?format=json';
        this.data = {
          "form_type": "team_edit",
          "team_name": this.Name,
          "moto": this.Motto,
          "team_type": this.Edit_Form.controls['team_type'].value,
          "bio": this.Bio,
          "city_name": this.city_name,
          "country_name": this.country_name,
          "latitude": this.lat,
          "longitude": this.long

          // "city": this.city_id
        }
        this.base_path_service.PutRequest(url, this.data)
          .subscribe(
            res => {
              if (res[0].status == 200 || res[0].status == 201 || res[0].status == 205) {
                this.loader = false;
                this.msgs = [];
                this.msgs.push({ severity: 'info', summary: '', detail: '¡Actualizado exitosamente!' });
                this.router.navigateByUrl('/dashboard/team/team-profile/' + this.team_id)

              }
            },
            err => {
            })
      // }
      // else {
      //   if (this.City == '') {
      //     this.msgs = [];
      //     this.msgs.push({ severity: 'error', summary: '', detail: 'ciudad required' });
      //   }
      //   else {
      //     this.msgs = [];
      //     this.msgs.push({ severity: 'error', summary: '', detail: 'ciudad inavalid' });
      //   }
      // }
    } else {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: '', detail: 'Ingresa todos los campos obligatorios' });
    }
  }
  public backToProfile() {
    this.router.navigateByUrl('dashboard/team/team-profile/' + this.team_id)
  }
  public deleteTeam() {
    let url = this.base_path_service.base_path_api() + "team/profile/" + this.team_id + '/?format=json'
    this.base_path_service.DeleteRequest(url)
      .subscribe(res => {
        this.router.navigateByUrl('dashboard/team');
        this.msgs.push({ severity: 'error', summary: '', detail: 'Equipo eliminado correctamente' });
      }, err => {
        this.msgs.push({ severity: 'error', summary: '', detail: 'error' });
      })
  }

  search(event) {
    this.checkCity = false;
    console.log("valussssssssss", event)
    this.GoogleAutocomplete.getPlacePredictions({ input: event.query, types: ['(regions)'] },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
          console.log("list", this.autocompleteItems)
        });
      });
  }

  setLoaction(item) {
    this
    this.checkCity = true;
    this.country_name = item.terms.pop().value;
    this.city_name = item.terms[0].value;
    console.log("jhsjhgjhgsssssssssssssssssssssss", this.city_name)
    this.geoCode((item.description));

  }

  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.lat = results[0].geometry.location.lat();
      this.long = results[0].geometry.location.lng()
    })
  }
}
