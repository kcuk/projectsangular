import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Message } from 'primeng/primeng';
import { GlobalService } from './../../../GlobalService';
import { TranslateService } from "ng2-translate";
import { Http } from '@angular/http';

@Component({
  selector: 'app-create-team-second',
  templateUrl: './create-team-second.component.html',
  styleUrls: ['./create-team-second.component.css']
})
export class CreateTeamSecondComponent implements OnInit {
  mask: any = '999999'
  national_id: any = ""
  fakename: any = "";
  fake_name_valid: boolean;
  fake_name_error: boolean;
  fullname: any = ""
  email_exist: boolean;
  check_email: boolean;
  email: any = ""
  full_name_valid: boolean;
  full_name_error: boolean;
  public title = 'create team2!'
  public player_list: any[];
  public team_id: number;
  public sub: any;
  public data: any;
  public loader: boolean = false;
  public creatdata;
  public players: any[];
  public emailExists: boolean = false;
  public error: boolean = false;
  public display: boolean = false;
  public enable: boolean = false;
  public basePath: string;
  public player_invitation: string;
  public Creat_team: FormGroup;
  public msgs: Message[] = [];
  public country_code: string = "+593";
  public selectedValue: String = 'Email';
  public fakeNameTest: boolean = false;
  public fakeName_Test: boolean = false;
  public fakelnameTest: boolean = false;
  public shirt_no: number;
  public id;
  public team_player: Array<any> = [];
  code: any = [
    { value: '+593', label: '+593' },
    { value: '+1', label: '+1' }
  ];
  length: any = 9;
  constructor
    (
    public http: Http,
    public global: GlobalService,
    public translate: TranslateService, frm: FormBuilder, public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute) {
    this.creatdata = new CreatData();

    // this.Creat_team = frm.group({
    //   email: new FormControl('', Validators.compose([Validators.pattern("[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+")])),
    //   name: new FormControl('', Validators.compose([Validators.required])),
    //   FakeName: new FormControl('', Validators.compose([])),
    //   national_id: new FormControl('')
    // })
    if (localStorage.getItem('language')) {
      translate.use(localStorage.getItem('language'))
    }
  }
  ngOnInit() {
    this.loader = true;
    this.sub = this.route.params.subscribe(params => {
      this.team_id = parseInt(params['id']);
    });
    this.basePath = this.base_path_service.image_url;
    let url = this.base_path_service.base_path_api() + "user/player_request/?team=" + this.team_id + '&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.players = res[0].json;
      }, err => {
        this.loader = false;
      });
    this.loader = false;
    this.get_player();

  }
  handleDropdownClick() {
    this.player_list = [];
  }

  get_playerList(event) {
    let query = event.query;
    if (query.length >= 2) {
      let url = this.base_path_service.base_path_api() + "user/profile/?form_type=manage_profie&value=" + query + '&team=' + this.team_id + '&format=json';
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.player_list = res[0].json;
          if (this.player_list.length == 0) {
            this.msgs = [];
            this.msgs.push({ severity: 'error', detail: 'Jugador no existe!' });
          }
        }, err => {
          this.msgs = [];
          this.msgs.push({ severity: 'error', detail: 'Algunos errores!' });
        });
    }
  }
  invite(id) {
    let url = this.base_path_service.base_path_api() + 'user/player_request/?format=json'
    let data = {
      "player": id,
      "team": this.team_id,
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(
        res => {
          this.player_invitation = '';
          this.msgs = [];
          this.msgs.push({ severity: 'info', detail: 'Éxito invitados!' });
          this.get_players();
        }, err => {
          this.player_invitation = '';
          this.msgs = [];
          this.msgs.push({ severity: 'error', detail: 'Algunos errores!' });
        });
  }
  continue() {
    localStorage.setItem("create_status", "true");
    this.router.navigateByUrl('dashboard/team/team-profile/' + this.team_id + '?status=true')
  }
  showDialog() { this.display = true; this.enable = true; }
  Accept() {
    this.display = false; this.enable = false;
  }


  // public get_email(event) {
  //   this.emailExists = false;
  //   this.error = false;
  //   if (/[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+$/.test(this.email)) {
  //     let val = event.target.value;
  //     if (val.length == 0) {
  //       this.error = true;
  //       console.log(val.length == 0);
  //     } else {
  //       console.log(val.length)
  //       let url = this.base_path_service.base_path + "api/peloteando/verification/?email=" + val + "&format=json";
  //       this.base_path_service.GetRequest(url)
  //         .subscribe(res => {

  //         }, err => {
  //           console.log("error", err);
  //           if (err.status == 404 || err.status == 403 || err.status == 400) {
  //             this.emailExists = false;
  //             this.error = true;
  //             this.msgs = [];
  //             this.msgs.push({ severity: 'error', detail: 'some error' });
  //           }
  //           else {
  //             this.emailExists = true;
  //           }
  //         });
  //     }
  //   }

  // }

  validate(event) {
    if (/[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+$/.test(this.email)) {
      this.check_email = true;
      this.email_exist = false;
    }
    else {
      this.check_email = false;
      this.email_exist = false;
    }
  }

  validateEmail() {
    this.loader = true;
    let url = this.global.base_path_api() + "peloteando/verification/?email=" + this.email + "&format=json";
    this.http.get(url)
      .subscribe(res => {
        this.sendData();
      }, err => {
        this.loader = false;
        this.email_exist = true;
        this.check_email = false;
      })
  }


  public createdata() {
    if (/^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$/.test(this.fullname)) {
      this.validateEmail()
    }
    else {
      this.full_name_error = true;
    }

  }

  sendData() {
    this.data = {
      "form_type": 'create',
      "name": this.fullname,
      "email": this.email,
      "team": this.team_id,
      "national_id": (this.national_id).trim()

    }
    var url = this.base_path_service.base_path_api() + "user/player/?format=json";
    this.base_path_service.PostRequest(url, this.data)
      .subscribe(
        res => {
          if (res[0].status == 205 || res[0].status == 200 || res[0].status == 201) {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({
              severity: 'info',
              summary: localStorage.getItem('language') == 'es' ? "Solicitud enviada" : "Request sent"
            });
            this.get_player();
            this.creatdata.email = '';
            this.creatdata.name = '';
            this.creatdata.lname = '';
            this.creatdata.mobile = '';
          }
        },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({
            severity: 'error',
            summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error,Try again later"
          });
        })
  }

  delete(id) {
    let url = this.base_path_service.base_path_api() + 'user/player_request/' + this.team_id + '/?player=' + id + '&format=json';
    this.base_path_service.DeleteRequest(url)
      .subscribe(
        res => {
          if (res[0].status == 200 || res[0].status == 201 || res[0].status == 205) {
            this.msgs = [];
            this.msgs.push({ severity: 'info', detail: 'Se ha eliminado el!' });
            this.get_players();
          }
        });
  }
  public delete_player(id) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'user/player/' + id + '/?team=' + this.team_id + '&format=json';
    this.base_path_service.DeleteRequest(url)
      .subscribe(
        res => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: '', detail: 'Jugador eliminado con éxito' });
          this.get_player();
        }, err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', detail: 'Algunos errores!' });
        });
  }
  public update(id, shirt, national) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'user/player/' + id + '/?format=json';
    let data = {
      "form_type": "shirt_no",
      "shirt_no": shirt,
      "team": this.team_id,
      "national_id": (national).trim()
    }
    this.base_path_service.PutRequest(url, data)
      .subscribe(
        res => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: '', detail: 'Actualizado con éxito reproductor' });
        }, err => {
          this.loader = false;
        });
  }
  public get_id(id) {
    this.id = id;
  }
  public enable_bttn(event) {
    this.shirt_no = event.target.value;
  }

  get_players() {
    let url = this.base_path_service.base_path_api() + "user/player_request/?team=" + this.team_id + '&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.players = res[0].json;
      });
  }

  public get_player() {
    let url = this.base_path_service.base_path_api() + 'user/profile/?form_type=team_player&team=' + this.team_id + '&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.team_player = res[0].json;
        this.full_name_valid = false;
        this.check_email = false;
        this.fullname = '';
        this.email = '';
        this.national_id = '';
        this.fakename = '';
        this.fake_name_error = false;
        this.fake_name_valid = false;
      });
  }

  public postFakePlayerData() {
    if (/^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$/.test(this.fakename)) {
      this.sendFaleData()
    }
    else {
      this.fake_name_error = true;
    }
  }

  sendFaleData() {
    var data = {
      form_type: "fake",
      name: this.fakename,
      team: this.team_id,
      national_id: (this.national_id).trim()
    }
    this.loader = true;
    var url = this.base_path_service.base_path_api() + "user/dummyplayer/?format=json";
    this.base_path_service.PostRequest(url, data)
      .subscribe(
        res => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'info', detail: 'Éxito invitados!' });
          // (<FormControl>this.Creat_team.controls['FakeName']).updateValue(null);
          // (<FormControl>this.Creat_team.controls['Fakelname']).updateValue(null);
          this.get_player();
        }, err => {
          this.loader = false;
        })
  }
  public fakeNameTesting(fakeName) {
    if (fakeName.FakeName && fakeName.FakeName.length == 0) {
      this.fakeNameTest = true;
    } else {
      this.fakeNameTest = false;
    }

    if (fakeName.Fakelname && fakeName.Fakelname.length == 0) {
      this.fakelnameTest = true;
    } else {
      this.fakelnameTest = false;
    }

    if (fakeName.FakeName && fakeName.FakeName.length >= 1 && fakeName.Fakelname && fakeName.Fakelname.length >= 1) {
      this.fakeName_Test = true;
    } else {
      this.fakeName_Test = false;
    }


  }



  validateFullName(event) {
    this.full_name_error = false;
    if (event.target.value.length > 40) {
      this.fullname = event.target.value.slice(0, 40)
    }
    if (/^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$/.test(this.fullname)) {
      this.full_name_valid = true;
    }
    else {
      this.full_name_valid = false;
    }
  }


  validateFakeName(event) {
    this.fake_name_error = false;
    if (event.target.value.length > 40) {
      this.fakename = event.target.value.slice(0, 40)
    }
    if (/^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$/.test(this.fakename)) {
      this.fake_name_valid = true;
    }
    else {
      this.fake_name_valid = false;
    }
  }
}


class CreatData {
  "form_type": string = "create";
  "email": string = "";
  "name": string = "";
  "mobile": number = null;
  "lname": string = "";
}
