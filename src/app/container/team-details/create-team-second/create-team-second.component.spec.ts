import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTeamSecondComponent } from './create-team-second.component';

describe('CreateTeamSecondComponent', () => {
  let component: CreateTeamSecondComponent;
  let fixture: ComponentFixture<CreateTeamSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTeamSecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTeamSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
