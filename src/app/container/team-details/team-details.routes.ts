import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamDetailsComponent } from './team-details.component';
import { TeamProfileModule } from './team-profile/team-profile.module';
import { CreateTeamFirstComponent } from './create-team-first/create-team-first.component';
import { CreateTeamSecondComponent } from './create-team-second/create-team-second.component';
export const teamDetailsRoutes: Routes = [
    { path: '', component: TeamDetailsComponent },
    { path: 'team-profile/:id', loadChildren: './team-profile/team-profile.module#TeamProfileModule' },
    { path: 'create-team', component: CreateTeamFirstComponent },
    { path: 'create-team/:id', component: CreateTeamSecondComponent }

]

export const teamDetailsRouting: ModuleWithProviders = RouterModule.forChild(teamDetailsRoutes);