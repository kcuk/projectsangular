import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TeamDetailsComponent } from "./team-details.component";
import { teamDetailsRouting } from "./team-details.routes";
import { SharedModule } from "./../../shared/shared.module";
import { CreateTeamFirstComponent } from "./create-team-first/create-team-first.component";
import { CreateTeamSecondComponent } from "./create-team-second/create-team-second.component";

@NgModule({
  imports: [CommonModule, teamDetailsRouting, SharedModule.forRoot()],
  declarations: [
    TeamDetailsComponent,
    CreateTeamFirstComponent,
    CreateTeamSecondComponent
  ]
})
export class TeamDetailsModule {}
