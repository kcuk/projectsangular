import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCardRedirectComponent } from './add-card-redirect.component';

describe('AddCardRedirectComponent', () => {
  let component: AddCardRedirectComponent;
  let fixture: ComponentFixture<AddCardRedirectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCardRedirectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCardRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
