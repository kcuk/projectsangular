import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-add-card-redirect',
  templateUrl: './add-card-redirect.component.html',
  styleUrls: ['./add-card-redirect.component.css']
})
export class AddCardRedirectComponent implements OnInit {
  url: any;
  loader: boolean = true;
  constructor() {
    this.url = localStorage.getItem('url');
  }

  ngOnInit() {
    // window.open(this.url, 'kis')
    console.log()
    let data = document.getElementById("myFrame")
    data['src'] = this.url;
    setTimeout(res => {
      this.loader = false;
    }, 7000)
  }

}
