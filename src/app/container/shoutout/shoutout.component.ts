import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { GlobalService } from './../../GlobalService';

@Component({
  selector: 'app-shoutout',
  templateUrl: './shoutout.component.html',
  styleUrls: ['./shoutout.component.css']
})
export class ShoutoutComponent implements OnInit {

  title = "shoutout component!"

  public type: string;
  public player_page: number = 0;
  public team_page: number = 0;
  public ground_page: number = 0;
  public tournament_page: number = 0;
  public page: number;
  public user_id: number;
  public shoutoutList: any[] = [];

  constructor(public route: ActivatedRoute, public router: Router, public base_path_service: GlobalService) {

  }
  ngOnInit() {
    let sub = this.route.params.subscribe(params => {
      this.user_id = +params['id'];
      this.type = params['type'];
    });
    this.shoutoutList = [];
    this.getShoutout(this.type)

    this.base_path_service.demoCheck.subscribe(res => {
      this.player_page = 0;
      this.ground_page = 0;
      this.team_page = 0;
      this.tournament_page = 0;
      this.shoutoutList = [];
      this.getShoutout(this.type);
    })
  }

  getShoutout(value) {
    this.type = value;
    if (this.type == 'player') {

      this.player_page = this.player_page + 1;
      let url = this.base_path_service.base_path_api() + 'user/shoutout/' + this.user_id + '/?form_type=player&page=' + this.player_page + '&format=json';

      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.page = res[0].json.total_page;
          this.shoutoutList = this.shoutoutList.concat(res[0].json.shoutout_list);
        });
    }
    if (this.type == 'team') {

      this.team_page = this.team_page + 1;

      let url = this.base_path_service.base_path_api() + 'user/shoutout/' + this.user_id + '/?form_type=team&page=' + this.team_page + '&format=json';
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.shoutoutList = this.shoutoutList.concat(res[0].json.shoutout_list);
          this.page = res[0].json.total_page;
        });
    }
    if (this.type == 'ground') {

      this.ground_page = this.ground_page + 1;

      let url = this.base_path_service.base_path_api() + 'user/shoutout/' + this.user_id + '/?form_type=ground&page=' + this.ground_page + '&format=json';
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.shoutoutList = this.shoutoutList.concat(res[0].json.shoutout_list);
          this.page = res[0].json.total_page;
        });
    }
    if (this.type == 'tournament') {

      this.tournament_page = this.tournament_page + 1;

      let url = this.base_path_service.base_path_api() + 'user/shoutout/' + this.user_id + '?form_type=tournament&page=' + this.tournament_page + '&format=json';
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.shoutoutList = this.shoutoutList.concat(res[0].json.shoutout_list);
          this.page = res[0].json.total_page;
        });
    }
  }

  navigate(id, profile) {
   
  }

  back_to_profile(id, type) {
    switch (type) {
      case 'player': this.router.navigateByUrl('/dashboard/player/player-profile/' + id);
        break;
      case 'team': this.router.navigateByUrl('/dashboard/team/team-profile/' + id);
        break;
      case 'ground': this.router.navigateByUrl('/dashboard/ground/ground-profile/' + id);
        break;
      case 'tournament': this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + id);
        break;
    }
  }
}
