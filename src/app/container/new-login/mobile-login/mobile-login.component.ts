import { Component, OnInit } from '@angular/core';
import { countryCode } from "../../registration/onboading2/country_code"
import { GlobalService } from '../../../GlobalService';
import { Router, Routes, ActivatedRoute } from '@angular/router';
import { SelectItem, Message } from 'primeng/primeng';
import { Http } from '@angular/http';
@Component({
  selector: 'app-mobile-login',
  templateUrl: './mobile-login.component.html',
  styleUrls: ['./mobile-login.component.css']
})
export class MobileLoginComponent implements OnInit {
  codes: SelectItem[] = [];
  mobile: any;
  mask: any = '999999999999999'
  country_code: string;
  number: any;
  checkNumber: boolean = false;
  checkNumberErrorMsg: boolean = false;
  loader: boolean;
  public msgs: any;

  constructor(
    public router: Router,
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http
  ) {
  }

  ngOnInit() {
    this.country_code = localStorage.getItem("country_dial_code");
    countryCode.map((val) => {
      this.codes.push({
        label: val.name + " " + "(" + val.dial_code + ")",
        value: val.dial_code
      })
    })
  }

  validatePhone(event) {
    if (event.keyCode == 13) {
      if (this.checkNumber) {
        this.checkMobile()
      }
    }
    else {
      let result = event.target.value;
      this.number = result;
      if (this.number.length >= 8) {
        this.checkNumber = true;
        this.checkNumberErrorMsg = false;
      }
      else {
        this.checkNumber = false;
        this.checkNumberErrorMsg = false;
      }
    }
  }

   
  checkMobile() {
    this.loader = true
    let url = this.global.base_path_api() + "peloteando/verification/?mobile=" + parseInt(this.mobile) + "&country_code=" + this.country_code + "&format=json";
    this.http.get(url)
      .subscribe(res => {
        this.checkNumberErrorMsg = true;
        this.checkNumber = false;
        this.loader = false;
      }, err => {
        this.checkNumberErrorMsg = false;
        this.sendOtp();
      })
  }

  sendOtp() {
    let url = this.global.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
    let data = {
      "phone": this.mobile,
      "country_code": this.country_code,
      "form_type": "send_otp",
      "register_type": "phone",
    }
    this.global.PostRequestUnauthorised(url, data)
      .subscribe(res => {
        this.loader = false;
        console.log("otppppppppppppppppppppp");
        let data = {
          "form_type": "mobile",
          "mobile": this.number,
          "country_code": this.country_code,
        }
        this.msgs = [];
        this.msgs.push({
          severity: 'success',
          summary: localStorage.getItem('language') == 'es' ? "OTP enviado" : "OTP sent",
        });
        this.router.navigate(['/home/login/mobile-login-otp', { data: JSON.stringify(data) }])
      }, error => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }


}
