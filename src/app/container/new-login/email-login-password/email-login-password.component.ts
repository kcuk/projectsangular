import { Component, OnInit, NgZone } from "@angular/core";
import { GlobalService } from "../../../GlobalService";
import { Router, Routes, ActivatedRoute } from "@angular/router";
import { SelectItem, Message } from "primeng/primeng";
import { Http } from "@angular/http";

@Component({
  selector: "app-email-login-password",
  templateUrl: "./email-login-password.component.html",
  styleUrls: ["./email-login-password.component.css"]
})
export class EmailLoginPasswordComponent {
  data: any;
  check_password: boolean;
  password = "";
  loader: boolean;
  public msgs: any;
  show_password: boolean = false;
  requestsent: boolean = false;
  passerr: boolean = false;
  selectcommunity: boolean = false;
  usercommunities: any;
  logo = this.global.image_url;
  notpart: boolean = false;

  constructor(
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    public ngzone: NgZone,
    public router: Router,

  ) {
    this.data = JSON.parse(routes.snapshot.paramMap.get("data"));
  }

  validate(event) {
    let password_length = event.target.value.length;
    if (password_length >= 6) {
      this.check_password = true;
    } else {
      this.check_password = false;
    }
  }

  next() {
    if (this.password.length >= 6) {
      this.getToken();
    } else {
      this.msgs = [];
      this.msgs.push({
        severity: "error",
        summary:
        localStorage.getItem("language") == "es"
          ? "La contraseña debe ser mayor a 5 caracteres"
          : "Password must be greater than 5 character"
      });
    }
  }

  getToken() {
    this.loader = true;
    let userinfo = {
      user: this.data.email,
      pass: this.password
    };
    let data = {
      form_type: "email"
    };
    let url =
      this.global.base_path_api() + "peloteando/mobile_login/?format=json";
    this.global.loginValid(url, userinfo, data).subscribe(
      res => {
        this.loader = false;
        let lang = localStorage.getItem("language");
        let country = localStorage.getItem("country_code_with_name");
        let country_code = localStorage.getItem("country_dial_code");
        let distance = localStorage.getItem("distance");
        localStorage.clear();
        localStorage.setItem("country_code_with_name", country);
        localStorage.setItem("country_dial_code", country_code);
        localStorage.setItem("distance", distance);
        localStorage.setItem("language", lang);
        let data = res[0].json;
        let expire = new Date().getTime() / 1000 + data.token.expires_in;
        localStorage.setItem("userInfo", JSON.stringify(data));
        localStorage.setItem("user_info", JSON.stringify(data.info));
        localStorage.setItem("timeout", JSON.stringify(expire));
        localStorage.setItem(
          "nick_name",
          JSON.stringify(res[0].json.info.nick_name)
        );
        localStorage.setItem("username", data.info.username);
        localStorage.setItem(
          "profile_percentage",
          data.info.profile_percentage
        );
        localStorage.setItem("checkoptionalpage", "signup");
        localStorage.setItem("last_name", res[0].json.info.last_name);
        localStorage.setItem("profile_pic", res[0].json.info.profile_pic);

        let url =
          this.global.base_path_api() +
          "user/setLanguage/?language=" +
          localStorage.getItem("language");
        this.global.GetRequest(url).subscribe(res => {
          this.global.currentLocation().subscribe(res => {
            this.loader = false;
            let data = res.json();
            localStorage.setItem("lat", data.location.latitude);
            localStorage.setItem("long", data.location.longitude);
            localStorage.setItem("city", data.city);
            localStorage.setItem("country_name", data.country.name);


            this.selectcommunity = true;
            this.getcommunitylist();



          });
        });
      },

      err => {
        if (err == 401) {
          this.loader = false;
          this.passerr = true;
          console.log("password error==================");
        } else if (err == 403) {
          this.requestsent = true;
          this.loader = false;
        } else {
          this.loader = false;
          console.log(err, "error message");
        }

      }
    );
  }
  toselectedcommunity(community_id) {
    this.getcommunityinfo(community_id);
  }
  getcommunityinfo(id) {
    let url = this.global.base_path + "api/user/communities/" + id + "/";
    this.global.GetRequest(url).subscribe(
      res => {
        let data = res[0].json;
        console.log(data, "data==============");
        localStorage.setItem(
          "community_info",
          JSON.stringify(data)
        );
        // for the redirection
        if (localStorage.getItem("community_info")) {
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.router.navigate([comname]);
        }
        else { }
      },
      err => {
        // this.loader = false;
      }
    );
  }
  getcommunitylist() {
    // this.loader = true;
    let url = this.global.base_path + "api/user/communities/";
    this.global.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.usercommunities = res[0].json;
        console.log(this.usercommunities, "usercommunities")
        console.log(this.usercommunities.length, "length of the community")
        if (this.usercommunities.length == 0) {
          console.log("you are not the part of any community");
          this.notpart = true;
          localStorage.removeItem("userInfo");
          localStorage.removeItem("username");
          localStorage.removeItem("user_info");
        }
        else {
          this.notpart = false;
        }
      },
      err => {
        // this.loader = false;
      }
    );
  }
  cleardata() {
    localStorage.removeItem("userInfo");
    localStorage.removeItem("username");
    localStorage.removeItem("user_info");
    this.router.navigateByUrl("/home");
  }



  closeDialog() {
    this.requestsent = false;
    this.router.navigate(["/home"]);
  }
  tocommunnity() {
    this.requestsent = false;
    this.router.navigateByUrl("/home");
  }
}
