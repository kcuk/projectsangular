import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailLoginPasswordComponent } from './email-login-password.component';

describe('EmailLoginPasswordComponent', () => {
  let component: EmailLoginPasswordComponent;
  let fixture: ComponentFixture<EmailLoginPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailLoginPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailLoginPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
