import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../GlobalService';
import { Router, Routes, ActivatedRoute } from '@angular/router';
import { SelectItem, Message } from 'primeng/primeng';
import { Http } from '@angular/http';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent   {

  email: any;
  loader: boolean;
  email_exist: boolean;
  check_email: boolean;
  public msgs:any;

  constructor(public router: Router,
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http) { }

  validate(event) {
    if (event.keyCode == 13) {
      if (this.check_email) {
        this.validateEmail()
      }
    }
    else {
      if (/[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+$/.test(this.email)) {
        this.check_email = true;
        this.email_exist = false;
      }
      else {
        this.check_email = false;
        this.email_exist = false;
      }
    }
  }

  validateEmail() {
    this.loader = true;
    let url = this.global.base_path_api() + "peloteando/verification/?email=" + this.email + "&format=json";
    this.http.get(url)
    .subscribe(res => {
      this.loader = false;
      this.email_exist = true;
      this.check_email = false;
      }, err => {
        this.setEmail();
      })
  }

 


  setEmail() {
    let url = this.global.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
    let data1 = {
      "form_type": "send_otp",
      "email": this.email,
      "register_type": "email",
    }
    this.global.PostRequestUnauthorised(url, data1)
    .subscribe(res => {
      let data = {
        "form_type": "email",
        "email": this.email,
      }
      this.msgs = [];
      this.msgs.push({
        severity: 'success',
        summary: localStorage.getItem('language') == 'es' ? "OTP enviado" : "OTP sent",
      });
      this.router.navigate(['/home/login/forgot-password-otp', { data: JSON.stringify(data) }])
    }, error => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({
        severity: 'error',
        summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
      });
    })
  }

}
