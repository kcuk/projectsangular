import { Component, ViewChild, Renderer2, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../GlobalService';
import { Http } from '@angular/http';
@Component({
  selector: 'app-forgot-password-reset',
  templateUrl: './forgot-password-reset.component.html',
  styleUrls: ['./forgot-password-reset.component.css']
})
export class ForgotPasswordResetComponent {
  data: any;
  check_password: boolean;
  password = "";
  loader: boolean;
  public msgs: any;
  show_password: boolean = false;
  constructor(public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    private renderer: Renderer2,
    public ngzone: NgZone,
    public router: Router, ) {
    this.data = JSON.parse(routes.snapshot.paramMap.get('data'));

  }


  validate(event) {
    let password_length = event.target.value.length;
    if (password_length >= 6) {
      this.check_password = true;
    }
    else {
      this.check_password = false;
    }
  }


  setPassword() {
    this.loader = true;
    let url = this.global.base_path_api() + "peloteando/forget_password/?format=json";
    let data = {
      "email": this.data.email,
      "password": this.password
    }
    this.global.PostRequestUnauthorised(url, data)
      .subscribe(res => {
        this.ngzone.run(() => {
          this.loader = false;
          this.router.navigate(['/home/login'])
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

}
