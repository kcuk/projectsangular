import { Component, OnInit, NgZone } from "@angular/core";
import { GlobalService } from "../../GlobalService";
import { Router } from "@angular/router";
import { countryCode } from "../registration/onboading2/country_code";
import { TranslateService } from "ng2-translate";
declare var gapi: any;
declare var $: any;
declare var FB: any;

@Component({
  selector: "app-new-login",
  templateUrl: "./new-login.component.html",
  styleUrls: ["./new-login.component.css"]
})
export class NewLoginComponent implements OnInit {
  auth2: any;
  msgs: any[];
  loader: boolean;
  requestsent: boolean = false;
  passerr: boolean = false;
  selectcommunity: boolean = false;
  usercommunities: any;
  logo = this.global.image_url;
  notpart: boolean = false;

  constructor(
    public router: Router,
    public global: GlobalService,
    private _zone: NgZone,
    public translate: TranslateService
  ) {
    FB.init({
      appId: this.global.fb_id,
      //localhost  298764690458377
      //peloteando.co  1679314905721132 // testing 1813124638915635
      cookie: false,
      xfbml: true,
      version: "v2.5"
    });
    if (localStorage.getItem("language") != null) {
      console.log("language1111111111111");
      translate.use(localStorage.getItem("language"));
    }
  }

  ngOnInit() {
    this.global.currentLocation().subscribe(res => {
      let code = res.json().country.code;
      countryCode.map(val => {
        if (val.code == code) {
          localStorage.setItem(
            "country_code_with_name",
            val.name + " " + "(" + val.dial_code + ")"
          );
          localStorage.setItem("country_dial_code", val.dial_code);
        }
      });
    });
  }

  ngAfterViewInit() {
    this.googleInit();
  }

  public googleInit() {
    gapi.load("auth2", () => {
      this.auth2 = gapi.auth2.init();
    });
  }

  googleLogin() {
    this.loader = true;
    let googleAuth = gapi.auth2.getAuthInstance();
    googleAuth
      .signIn()
      .then(res => {
        this._zone.run(() => {
          let data = {
            id_token: res.getAuthResponse().id_token,
            form_type: "gmail",
            login_type: "login"
          };
          let url =
            this.global.base_path_api() +
            "peloteando/social_login/?format=json";
          this.global.PostRequestUnauthorised(url, data).subscribe(
            res => {
              this.loader = false;
              let lang = localStorage.getItem("language");
              let country = localStorage.getItem("country_code_with_name");
              let country_code = localStorage.getItem("country_dial_code");
              let distance = localStorage.getItem("distance");
              // localStorage.clear();
              localStorage.setItem("google_status", "true");
              localStorage.setItem("country_code_with_name", country);
              localStorage.setItem("country_dial_code", country_code);
              localStorage.setItem("distance", distance);
              localStorage.setItem("language", lang);
              let data = res[0].json;
              let expire = new Date().getTime() / 1000 + data.token.expires_in;
              localStorage.setItem("userInfo", JSON.stringify(data));
              localStorage.setItem("user_info", JSON.stringify(data.info));
              localStorage.setItem("timeout", JSON.stringify(expire));
              localStorage.setItem(
                "nick_name",
                JSON.stringify(res[0].json.info.nick_name)
              );
              localStorage.setItem("username", data.info.username);
              localStorage.setItem(
                "profile_percentage",
                data.info.profile_percentage
              );
              localStorage.setItem("checkoptionalpage", "signup");
              localStorage.setItem("last_name", res[0].json.info.last_name);
              localStorage.setItem("profile_pic", res[0].json.info.profile_pic);

              let url =
                this.global.base_path_api() +
                "user/setLanguage/?language=" +
                localStorage.getItem("language");
              this.global.GetRequest(url).subscribe(res => {
                this.global.currentLocation().subscribe(res => {
                  let data = res.json();
                  localStorage.setItem("lat", data.location.latitude);
                  localStorage.setItem("long", data.location.longitude);
                  localStorage.setItem("city", data.city);
                  localStorage.setItem("country_name", data.country.name);
                  localStorage.setItem("checkoptionalpage", "signup");
                  // let user_info = JSON.parse(localStorage.getItem("user_info"));
                  // if (!user_info.is_location) {
                  //   this.router.navigate(["/home/signup/set-location"]);
                  // } else if (!user_info.is_phone_no) {
                  //   this.router.navigate(["/home/signup/set-phone"]);
                  // } else if (!user_info.is_username) {
                  //   this.router.navigate(["/home/signup/set-user"]);
                  // } else if (!user_info.is_gender) {
                  //   this.router.navigate(["/home/signup/set-gender"]);
                  // } else if (!user_info.is_height) {
                  //   this.router.navigate(["/home/signup/set-height"]);
                  // } else if (!user_info.is_dob) {
                  //   this.router.navigate(["/home/signup/set-dob"]);
                  // } else if (!user_info.is_leg) {
                  //   this.router.navigate(["/home/signup/set-leg"]);
                  // } else if (!user_info.is_position) {
                  //   this.router.navigate(["/home/signup/set-position"]);
                  // } else if (!user_info.is_profile_pic) {
                  //   this.router.navigate(["/home/signup/set-profile-pic"]);
                  // } else {
                  //   localStorage.setItem(
                  //     "profile_percentage",
                  //     JSON.parse(localStorage.getItem("user_info"))
                  //       .profile_percentage
                  //   );
                  //   let comdata = JSON.parse(localStorage.getItem("community_info"));
                  //   let comname = comdata.community_name;
                  //   console.log(comname, "community name we get======================");
                  //   this.router.navigate([comname]);
                  // }
                  this.selectcommunity = true;
                  this.getcommunitylist();
                });
              });
            },
            err => {
              console.log(err.status, "err======>>>>>>>>");

              if (err.status == 401) {
                this.loader = false;
                this.passerr = true;
                console.log("password error==================");
              } else if (err.status == 403) {
                this.requestsent = true;
                this.loader = false;
                console.log("error===========>>>>>>>");
              } else {
                this.loader = false;
                console.log(err, "error message");
              }
            }
          );
        });
      })
      .catch(err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary:
          localStorage.getItem("language") == "es"
            ? "Error, inténtalo de nuevo más tarde"
            : "Error,Try again later"
        });
      });
  }


  toselectedcommunity(community_id) {
    this.selectcommunity=false;
    this.getcommunityinfo(community_id);
  }
  getcommunityinfo(id) {
    let url = this.global.base_path + "api/user/communities/" + id + "/";
    this.global.GetRequest(url).subscribe(
      res => {
        let data = res[0].json;
        console.log(data, "data==============");
        localStorage.setItem(
          "community_info",
          JSON.stringify(data)
        );
        // for the redirection
        if (localStorage.getItem("community_info")) {
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.router.navigate([comname]);
        }
        else { }
      },
      err => {
      }
    );
  }
  getcommunitylist() {
    let url = this.global.base_path + "api/user/communities/";
    this.global.GetRequest(url).subscribe(
      res => {
        this.usercommunities = res[0].json;
        console.log(this.usercommunities, "usercommunities")
        console.log(this.usercommunities.length, "length of the community")
        if (this.usercommunities.length == 0) {
          console.log("you are not the part of any community");
          this.notpart = true;
          localStorage.removeItem("userInfo");
          localStorage.removeItem("username");
          localStorage.removeItem("user_info");
          localStorage.removeItem("last_name");
          localStorage.removeItem("profile_pic");
        }
        else {
          this.notpart = false;
        }
      },
      err => {
      }
    );
  }

  cleardata() {
    localStorage.removeItem("userInfo");
    localStorage.removeItem("username");
    localStorage.removeItem("user_info");
    this.router.navigateByUrl("/home");
  }


  fbLogin() {
    this.loader = true;
    FB.login(response => {
      console.log(response, "fb response");
      this._zone.run(() => {
        let data = {
          form_type: "fb",
          login_type: "login",
          access_token: response.authResponse.accessToken
        };
        let url =
          this.global.base_path_api() + "peloteando/social_login/?format=json";
        this.global.PostRequestUnauthorised(url, data).subscribe(
          res => {
            this.loader = false;
            let lang = localStorage.getItem("language");
            let country = localStorage.getItem("country_code_with_name");
            let country_code = localStorage.getItem("country_dial_code");
            let distance = localStorage.getItem("distance");
            // localStorage.clear();
            localStorage.setItem("google_status", "true");
            localStorage.setItem("country_code_with_name", country);
            localStorage.setItem("country_dial_code", country_code);
            localStorage.setItem("distance", distance);
            localStorage.setItem("language", lang);
            let data = res[0].json;
            let expire = new Date().getTime() / 1000 + data.token.expires_in;
            localStorage.setItem("userInfo", JSON.stringify(data));
            localStorage.setItem("user_info", JSON.stringify(data.info));
            localStorage.setItem("timeout", JSON.stringify(expire));
            localStorage.setItem(
              "nick_name",
              JSON.stringify(res[0].json.info.nick_name)
            );
            localStorage.setItem("username", data.info.username);
            localStorage.setItem(
              "profile_percentage",
              data.info.profile_percentage
            );
            localStorage.setItem("checkoptionalpage", "signup");
            localStorage.setItem("last_name", res[0].json.info.last_name);
            localStorage.setItem("profile_pic", res[0].json.info.profile_pic);

            let url =
              this.global.base_path_api() +
              "user/setLanguage/?language=" +
              localStorage.getItem("language");
            this.global.GetRequest(url).subscribe(res => {
              this.global.currentLocation().subscribe(res => {
                let data = res.json();
                localStorage.setItem("lat", data.location.latitude);
                localStorage.setItem("long", data.location.longitude);
                localStorage.setItem("city", data.city);
                localStorage.setItem("country_name", data.country.name);
                localStorage.setItem("checkoptionalpage", "signup");
                // for the redirection
                // let comdata = JSON.parse(
                //   localStorage.getItem("community_info")
                // );
                // let comname = comdata.community_name;
                // console.log(
                //   comname,
                //   "community name we get======================"
                // );
                // this.router.navigate([comname]);

                // let user_info = JSON.parse(localStorage.getItem("user_info"));
                // if (!user_info.is_location) {
                //   this.router.navigate(["/home/signup/set-location"]);
                // } else if (!user_info.is_phone_no) {
                //   this.router.navigate(["/home/signup/set-phone"]);
                // } else if (!user_info.is_username) {
                //   this.router.navigate(["/home/signup/set-user"]);
                // } else if (!user_info.is_gender) {
                //   this.router.navigate(["/home/signup/set-gender"]);
                // } else if (!user_info.is_height) {
                //   this.router.navigate(["/home/signup/set-height"]);
                // } else if (!user_info.is_dob) {
                //   this.router.navigate(["/home/signup/set-dob"]);
                // } else if (!user_info.is_leg) {
                //   this.router.navigate(["/home/signup/set-leg"]);
                // } else if (!user_info.is_position) {
                //   this.router.navigate(["/home/signup/set-position"]);
                // } else if (!user_info.is_profile_pic) {
                //   this.router.navigate(["/home/signup/set-profile-pic"]);
                // } else {
                //   localStorage.setItem(
                //     "profile_percentage",
                //     JSON.parse(localStorage.getItem("user_info"))
                //       .profile_percentage
                //   );
                //   let comdata = JSON.parse(localStorage.getItem("community_info"));
                //   let comname = comdata.community_name;
                //   console.log(comname, "community name we get======================");
                //   this.router.navigate([comname]);

                // }
                this.selectcommunity = true;
                this.getcommunitylist();
              });
            });
          },
          err => {
            console.log(err.status, "err======>>>>>>>>");

            if (err.status == 401) {
              this.loader = false;
              this.passerr = true;
              console.log("password error==================");
            } else if (err.status == 403) {
              this.requestsent = true;
              this.loader = false;
              console.log("error===========>>>>>>>");
            } else {
              this.loader = false;
              console.log(err, "error message");
            }
          }
        );
      });
    });
  }

  tocommunnity() {
    this.requestsent = false;
    this.router.navigateByUrl("/home");
  }
}
