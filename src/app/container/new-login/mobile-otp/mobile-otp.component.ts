import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2,
  Renderer,
  NgZone
} from "@angular/core";
import { Route, ActivatedRoute, Router } from "@angular/router";
import { ActionSequence } from "protractor";
import { GlobalService } from "../../../GlobalService";
import { Http } from "@angular/http";
import { Message } from "primeng/primeng";
@Component({
  selector: "app-mobile-otp",
  templateUrl: "./mobile-otp.component.html",
  styleUrls: ["./mobile-otp.component.css"]
})
export class MobileOtpComponent {
  data: any;
  mask: any = "9";
  @ViewChild("input1")
  first: any;
  @ViewChild("input2")
  second: any;
  @ViewChild("input3")
  third: any;
  @ViewChild("input4")
  fourth: any;
  d1;
  d2;
  d3;
  d4;
  loader: boolean;
  count: number = 30;
  public msgs: Message[] = [];
  requestsent: boolean = false;
  passerr: any;
  selectcommunity: boolean = false;
  usercommunities: any;
  logo = this.global.image_url;
  notpart: boolean = false;

  constructor(
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    private renderer: Renderer2,
    public ngzone: NgZone,
    public router: Router
  ) {
    this.data = JSON.parse(routes.snapshot.paramMap.get("data"));
    setInterval(() => {
      this.count--;
    }, 1000);

  }

  getcommunitylist() {
    // this.loader = true;
    let url = this.global.base_path + "api/user/communities/";
    this.global.GetRequest(url).subscribe(
      res => {
        // this.loader = false;
        this.usercommunities = res[0].json;
        if (this.usercommunities.length == 0) {
          console.log("you are not the part of any community");
          this.notpart = true;
          localStorage.removeItem("userInfo");
          localStorage.removeItem("username");
          localStorage.removeItem("user_info");
        }
        else {
          this.notpart = false;
        }
      },
      err => {
        // this.loader = false;
      }
    );
  }
  cleardata() {
    localStorage.removeItem("userInfo");
    localStorage.removeItem("username");
    localStorage.removeItem("user_info");
    this.router.navigateByUrl("/home");
  }



  ngAfterViewInit() {
    setTimeout(() => {
      this.first.inputViewChild.nativeElement.focus();
    }, 500);
  }

  checkOtp(check, event) {
    console.log("event", event);
    this.ngzone.run(() => {
      if (event.keyCode >= 48 && event.keyCode <= 57) {
        if (check == 1) {
          this.second.inputViewChild.nativeElement.focus();
        } else if (check == 2) {
          this.third.inputViewChild.nativeElement.focus();
        } else if (check == 3) {
          this.fourth.inputViewChild.nativeElement.focus();
        }
      } else {
        return false;
      }
    });
  }

  verifyOtp() {
    if (this.d1 && this.d2 && this.d3 && this.d4) {
      this.loader = true;
      let otp = this.d1 + "" + this.d2 + "" + this.d3 + "" + this.d4;
      let url =
        this.global.base_path_api() + "peloteando/mobile_otp/?format=json";
      let data = {
        phone: this.data.mobile,
        country_code: this.data.country_code,
        form_type: "verify_otp",
        register_type: "phone",
        otp: parseInt(otp)
      };
      this.global.PostRequestUnauthorised(url, data).subscribe(
        res => {
          console.log("otp verify=>>>>>>>>>>>>>>>>>");
          this.getToken();
        },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary:
            localStorage.getItem("language") == "es"
              ? "Ingrese OTP correcta"
              : "Enter correct OTP"
          });
        }
      );
    }
  }

  getToken() {
    let url1 =
      this.global.base_path_api() + "peloteando/mobile_login/?format=json";
    this.global.PostRequestUnauthorised(url1, this.data).subscribe(
      res => {
        this.loader = false;
        let lang = localStorage.getItem("language");
        let country = localStorage.getItem("country_code_with_name");
        let country_code = localStorage.getItem("country_dial_code");
        let distance = localStorage.getItem("distance");
        localStorage.clear();
        localStorage.setItem("country_code_with_name", country);
        localStorage.setItem("country_dial_code", country_code);
        localStorage.setItem("distance", distance);
        localStorage.setItem("language", lang);
        let data = res[0].json;
        let expire = new Date().getTime() / 1000 + data.token.expires_in;
        localStorage.setItem("userInfo", JSON.stringify(data));
        localStorage.setItem("user_info", JSON.stringify(data.info));
        localStorage.setItem("timeout", JSON.stringify(expire));
        localStorage.setItem(
          "nick_name",
          JSON.stringify(res[0].json.info.nick_name)
        );
        localStorage.setItem("username", data.info.username);
        localStorage.setItem(
          "profile_percentage",
          data.info.profile_percentage
        );
        localStorage.setItem("checkoptionalpage", "signup");
        localStorage.setItem("last_name", res[0].json.info.last_name);
        localStorage.setItem("profile_pic", res[0].json.info.profile_pic);

        let url =
          this.global.base_path_api() +
          "user/setLanguage/?language=" +
          localStorage.getItem("language");
        this.global.GetRequest(url).subscribe(res => {
          this.loader = false;
          this.global.currentLocation().subscribe(res => {
            let data = res.json();
            localStorage.setItem("lat", data.location.latitude);
            localStorage.setItem("long", data.location.longitude);
            localStorage.setItem("city", data.city);
            localStorage.setItem("country_name", data.country.name);
            this.selectcommunity = true;
            this.getcommunitylist();

          });
        });
      },
      err => {
        if (err == 401) {
          this.loader = false;
          this.passerr = true;
          console.log("password error==================");
        } else if (err == 403) {
          this.requestsent = true;
          this.loader = false;
        } else {
          this.loader = false;
          console.log(err, "error message");
        }
      }
    );
  }

  toselectedcommunity(community_id) {
    this.getcommunityinfo(community_id);
  }
  getcommunityinfo(id) {
    let url = this.global.base_path + "api/user/communities/" + id + "/";
    this.global.GetRequest(url).subscribe(
      res => {
        let data = res[0].json;
        console.log(data, "data==============");
        localStorage.setItem(
          "community_info",
          JSON.stringify(data)
        );
        // for the redirection
        if (localStorage.getItem("community_info")) {
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.router.navigate([comname]);
        }
        else { }

      },
      err => {
      }
    );
  }

  resendOtp() {
    if (this.count <= 0) {
      this.loader = true;
      let url =
        this.global.base_path_api() +
        "peloteando/mobile_otp/?language=" +
        localStorage.getItem("language") +
        "&format=json";
      let data = {
        form_type: "send_otp",
        phone: this.data.mobile,
        country_code: this.data.country_code,
        register_type: "phone"
      };
      this.global.PostRequestUnauthorised(url, data).subscribe(
        res => {
          this.loader = false;
          this.count = 30;
          this.msgs = [];
          this.msgs.push({
            severity: "success",
            summary:
            localStorage.getItem("language") == "es"
              ? "OTP enviado"
              : "OTP sent"
          });
        },
        error => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary:
            localStorage.getItem("language") == "es"
              ? "Error, inténtalo de nuevo más tarde"
              : "Error, Try again later"
          });
        }
      );
    }
  }
  closeDialog() {
    this.requestsent = false;
    this.router.navigate(["/home"]);
  }
  tocommunnity() {
    this.requestsent = false;
    this.router.navigateByUrl("/home");
  }
}
