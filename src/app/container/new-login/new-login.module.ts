import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from './../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { NewLoginComponent } from './new-login.component';
import { EmailLoginComponent } from './email-login/email-login.component';
import { MobileLoginComponent } from './mobile-login/mobile-login.component';
import { MobileOtpComponent } from './mobile-otp/mobile-otp.component';
import { EmailLoginPasswordComponent } from './email-login-password/email-login-password.component';
import { ForgotPasswordOtpComponent } from './forgot-password-otp/forgot-password-otp.component';
import { ForgotPasswordResetComponent } from './forgot-password-reset/forgot-password-reset.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';



export const login_routes: Routes = [
  { path: "", component: NewLoginComponent },
  { path: 'email-login', component: EmailLoginComponent },
  { path: 'email-login-password', component: EmailLoginPasswordComponent },
  { path: 'mobile-login', component: MobileLoginComponent },
  { path: 'mobile-login-otp', component: MobileOtpComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'forgot-password-otp', component: ForgotPasswordOtpComponent },
  { path: 'forgot-password-reset', component: ForgotPasswordResetComponent },

]

  @NgModule({
    imports: [
    CommonModule,
    RouterModule.forChild(login_routes),
    SharedModule.forRoot()
  ],
  declarations: [
    NewLoginComponent, EmailLoginComponent,  MobileLoginComponent, MobileOtpComponent, EmailLoginPasswordComponent, ForgotPasswordComponent, ForgotPasswordOtpComponent, ForgotPasswordResetComponent
  ]
})

export class NewLoginModule { }
