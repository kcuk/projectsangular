import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../GlobalService';
import { Router, Routes, ActivatedRoute } from '@angular/router';
import { SelectItem, Message } from 'primeng/primeng';
import { Http } from '@angular/http';

@Component({
  selector: 'app-email-login',
  templateUrl: './email-login.component.html',
  styleUrls: ['./email-login.component.css']
})
export class EmailLoginComponent {

  email: any;
  loader: boolean;
  email_exist: boolean;
  check_email: boolean;
  public msgs:any;

  constructor(public router: Router,
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http) { }

  validate(event) {
    if (event.keyCode == 13) {
      if (this.check_email) {
        this.validateEmail()
      }
    }
    else {
      if (/[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+$/.test(this.email)) {
        this.check_email = true;
        this.email_exist = false;
      }
      else {
        this.check_email = false;
        this.email_exist = false;
      }
    }
  }

  validateEmail() {
    this.loader = true;
    let url = this.global.base_path_api() + "peloteando/verification/?email=" + this.email + "&format=json";
    this.http.get(url)
    .subscribe(res => {
      this.loader = false;
      this.email_exist = true;
      this.check_email = false;
      }, err => {
        this.loader = false;
        this.setEmail();
      })
  }


  setEmail() {
    let url = this.global.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
    let data = {
      "form_type": "email",
      "email": this.email,
    }
    this.router.navigate(['/home/login/email-login-password', { data: JSON.stringify(data) }])
  }

}
