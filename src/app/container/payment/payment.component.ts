import { Component, OnInit } from '@angular/core';
import { TranslateService } from "ng2-translate";
import { Router, ActivatedRoute } from "@angular/router";
import { GlobalService } from "./../../GlobalService";
import { FormBuilder } from "@angular/forms";

@Component({
	selector: 'app-payment',
	templateUrl: './payment.component.html',
	styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
	tour_second_pay: boolean;
	no_of_teams: any;
	cost_per_team: any;
	paid_amount: any;
	tournament_vat: any;
	ground_vat: any;
	sur_charge: string;
	sub_total: any;
	gateway_vat: string;
	payment_vat: any;
	loader1: boolean;
	getId: any;
	card_added: boolean;
	card_failed: boolean;
	selected_payment_option: string = 'card';
	selected_card_option: string = '';
	id: any;
	package_details: any;
	redirect: any = '';
	results: any[] = [];
	loader: boolean = false;
	msgs: any[] = [];
	team_id: any;
	team: boolean = false;
	ground: boolean = false;
	team_pay_info: any;
	reservation: boolean = false;
	reservation_info: any;
	gateway_charge: any;
	total_charge: any;
	option: any;
	amount: any;
	pending_amount: any;
	groun_detail: any;
	extra_court: number = 0;
	card_data: any;
	package: any = 'yearly';
	recurrent: any = 'false';
	tournament_detail: any;
	tournament: boolean = false;
	additional_fee: any;
	paid: boolean = false;
	backdrop: boolean = false;
	new_amount_to_pay: any;
	constructor(private translate: TranslateService, public router: Router, public base_path_service: GlobalService, public route: ActivatedRoute, public _fb: FormBuilder) {
		// let url1 = window.location.href
		// this.route.queryParams.subscribe(param => {
		// 	let status = param['status'];
		// 	if (status != undefined) {
		// 		this.loader = true;
		// 		this.loader1 = true;
		// 		if (status === '1') {
		// 			let redirect = url1.replace('status=1', 'status=true');
		// 			window.parent.location.href = redirect;
		// 		}
		// 		else if (status === '2') {
		// 			let redirect = url1.replace('status=2', 'status=false');
		// 			window.parent.location.href = redirect;
		// 		}
		// 		else if (status === 'true') {
		// 			this.card_added = true;
		// 			this.loader1 = false;
		// 		}
		// 		else if (status === 'false') {
		// 			this.card_failed = true;
		// 			this.loader1 = false;
		// 		}
		// 	}
		// })
		// let url = this.base_path_service.base_path_api() + "ground/saveCardDetails/";
		// this.base_path_service.GetRequest(url)
		// 	.subscribe(res => {
		// 	})
	}
	ngOnInit() {
		this.getCardList();
		this.route.queryParams.subscribe(param => {
			if (param['ground']) {
				localStorage.removeItem('team_payment');
				localStorage.removeItem('field_info')
				localStorage.removeItem('court_information');
				localStorage.removeItem('reservation');
				localStorage.removeItem('teamregister');
				localStorage.removeItem('tournamentcreation');
				this.package = param['package']
				this.package_details = JSON.parse(localStorage.getItem('package_detail'));
				this.groun_detail = JSON.parse(localStorage.getItem('courts')).ground_info;
				//if (this.groun_detail.no_of_courts > this.package_details.no_of_courts) {
				// this.extra_court = this.groun_detail.no_of_courts - this.package_details.no_of_courts;
				// let price = parseFloat(this.package == 'monthly' ? this.package_details.monthly_sub : this.package_details.yearly_sub)
				// console.log("price extraaaaaaaaaaaa",price)
				// let total_charge = price + (this.extra_court * this.package_details.additional_fee);
				// // let vat: any = (total_charge * 12) / 100;
				// this.sub_total = (parseFloat(total_charge)).toFixed(2);
				// this.gateway_charge = (((this.sub_total * 2.9) / 100) + .30).toFixed(2);
				// // this.payment_vat = parseFloat(vat).toFixed(2);
				// this.total_charge = (parseFloat(this.sub_total) + parseFloat(this.gateway_charge)).toFixed(2);
				// }
				// else {
				let price = parseFloat((this.package == 'monthly' ? this.package_details.monthly_sub : this.package_details.yearly_sub))
				// let vat: any = (price * 12) / 100;
				// this.sub_total = (parseFloat(price)).toFixed(2);
				let commition_on_amount = (price * 5) / 100;
				this.gateway_charge = (commition_on_amount + .30).toFixed(2);
				// this.payment_vat = parseFloat(vat).toFixed(2);
				this.total_charge = (price + parseFloat(this.gateway_charge)).toFixed(2);
				// }
				this.ground = true;
				this.route.params.subscribe(params => {
					this.id = parseInt(params['id']);
				});
			}
			else if (param['team']) {
				localStorage.removeItem('package_detail')
				localStorage.removeItem('field_info');
				localStorage.removeItem('court_information');
				localStorage.removeItem('reservation');
				localStorage.removeItem('groundcreation');
				localStorage.removeItem('tournamentcreation');
				this.option = param['option']
				this.team_pay_info = JSON.parse(localStorage.getItem('team_payment')).payment_info;
				this.tournament_vat = this.team_pay_info.vat;
				if (this.option === 'discount') {
					let obj: any = this.team_pay_info.amount - ((this.team_pay_info.amount * this.team_pay_info.discount) / 100);
					this.amount = parseFloat(obj).toFixed(2);
					// let vat: any = (this.amount * 12) / 100;
					// this.gateway_vat = parseFloat(vat).toFixed(2);
					let tour_vat: any = (this.amount * this.tournament_vat) / 100;
					this.sur_charge = parseFloat(tour_vat).toFixed(2);
					this.sub_total = parseFloat(obj + tour_vat).toFixed(2);
					let commition_on_amount = (this.sub_total * 2.1) / 100;
					console.log("commition_on_amount =>>>>>>>>>>>>>>", commition_on_amount);
					let amount_with_commition = parseFloat(this.sub_total) + commition_on_amount;
					let charge: any = (((amount_with_commition * 2.9) / 100) + .30 + commition_on_amount);
					this.gateway_charge = parseFloat(charge).toFixed(2)
					this.total_charge = (parseFloat(this.sub_total) + parseFloat(this.gateway_charge)).toFixed(2);
					this.pending_amount = '00.00';
				}
				else if (this.option === 'minimum_fee') {
					this.amount = parseFloat(this.team_pay_info.minimum_fee ? this.team_pay_info.minimum_fee : this.team_pay_info.amount).toFixed(2);
					// let vat: any = (this.amount * 12) / 100;
					// this.gateway_vat = parseFloat(vat).toFixed(2);
					let tour_vat: any = (this.amount * this.tournament_vat) / 100;
					this.sur_charge = parseFloat(tour_vat).toFixed(2);
					this.sub_total = parseFloat(parseFloat(this.amount) + tour_vat).toFixed(2);
					let commition_on_amount = (this.sub_total * 2.1) / 100;
					console.log("commition_on_amount =>>>>>>>>>>>>>>", commition_on_amount);
					let amount_with_commition = parseFloat(this.sub_total) + commition_on_amount;
					let charge: any = (((amount_with_commition * 2.9) / 100) + .30 + commition_on_amount);
					this.gateway_charge = parseFloat(charge).toFixed(2)
					this.total_charge = (parseFloat(this.sub_total) + parseFloat(this.gateway_charge)).toFixed(2);
					this.pending_amount = (parseFloat(this.team_pay_info.amount) - parseFloat(this.amount)).toFixed(2);
				}

				this.team = true;
				this.route.params.subscribe(params => {
					this.id = parseInt(params['id']);
				});
				this.team_id = param['team_id']
			}
			else if (param['reserve']) {
				localStorage.removeItem('team_payment');
				localStorage.removeItem('package_detail');
				this.option = param['option'];
				this.recurrent = param['recurrent'];
				localStorage.removeItem('teamregister');
				localStorage.removeItem('groundcreation');
				localStorage.removeItem('tournamentcreation');


				this.reservation_info = JSON.parse(localStorage.getItem('field_info'));
				let court_information = JSON.parse(localStorage.getItem('court_information'));
				this.ground_vat = court_information.vat;

				if (this.option === 'discount') {
					let obj: any = this.reservation_info.price - ((this.reservation_info.price * court_information.discount) / 100);
					this.amount = parseFloat(obj).toFixed(2);
					// let vat: any = (this.amount * 12) / 100;
					// this.gateway_vat = parseFloat(vat).toFixed(2);
					let ground_vat: any = (this.amount * this.ground_vat) / 100;
					this.sur_charge = parseFloat(ground_vat).toFixed(2);
					this.sub_total = parseFloat(obj + ground_vat).toFixed(2);
					let commition_on_amount = (this.sub_total * 2.1) / 100;
					console.log("commition_on_amount =>>>>>>>>>>>>>>", commition_on_amount);
					let amount_with_commition = parseFloat(this.sub_total) + commition_on_amount;
					let charge: any = (((amount_with_commition * 2.9) / 100) + .30 + commition_on_amount);
					this.gateway_charge = parseFloat(charge).toFixed(2)
					this.total_charge = (parseFloat(this.sub_total) + parseFloat(this.gateway_charge)).toFixed(2);
					this.pending_amount = '00.00';
				}
				else if (this.option === 'minimum_fee') {
					this.amount = parseFloat((court_information.minimum_fee && (court_information.minimum_fee <= this.reservation_info['price'])) ? court_information.minimum_fee : this.reservation_info['price']).toFixed(2);
					// let vat: any = (this.amount * 12) / 100;
					// this.gateway_vat = parseFloat(vat).toFixed(2);
					let ground_vat: any = (this.amount * this.ground_vat) / 100;
					this.sur_charge = parseFloat(ground_vat).toFixed(2);
					this.sub_total = parseFloat(parseFloat(this.amount) + ground_vat).toFixed(2);
					let commition_on_amount = (this.sub_total * 2.1) / 100;
					console.log("commition_on_amount =>>>>>>>>>>>>>>", commition_on_amount)
					let amount_with_commition = parseFloat(this.sub_total) + commition_on_amount
					let charge: any = (((amount_with_commition * 2.9) / 100) + .30 + commition_on_amount);
					this.gateway_charge = parseFloat(charge).toFixed(2)
					this.total_charge = (parseFloat(this.sub_total) + parseFloat(this.gateway_charge)).toFixed(2);
					this.pending_amount = (parseFloat(this.reservation_info.price) - parseFloat(this.amount)).toFixed(2);
				}
				this.reservation = true;
				this.route.params.subscribe(params => {
					this.id = parseInt(params['id']);
				});
			}
			else if (param['tournament']) {
				console.log("params=>>>>>>>>>>>>>>>>>", param)
				localStorage.removeItem('team_payment');
				localStorage.removeItem('field_info');
				localStorage.removeItem('court_information');
				localStorage.removeItem('reservation');
				localStorage.removeItem('teamregister');
				localStorage.removeItem('groundcreation');

				// this.package = param['package']
				// this.package_details = JSON.parse(localStorage.getItem('package_detail'));
				// this.paid_amount = parseFloat(this.package_details.paid_amount).toFixed(2);
				if (param['second']) {
					this.tour_second_pay = true;
					this.tournament_detail = JSON.parse(localStorage.getItem('tour_second_payment_data'));
					this.no_of_teams = this.tournament_detail.no_of_teams;
					this.cost_per_team = parseFloat(this.tournament_detail.cost_per_team).toFixed(2);
					this.amount = (this.cost_per_team * this.no_of_teams).toFixed(2);
					let commition_on_amount = (this.amount * 5) / 100;
					let charge: any = (.30 + commition_on_amount);
					this.gateway_charge = parseFloat(charge).toFixed(2);
					this.total_charge = (parseFloat(this.amount) + parseFloat(this.gateway_charge)).toFixed(2);

				}
				else {
					this.tour_second_pay = false;
					this.tournament_detail = JSON.parse(localStorage.getItem('tournament')).tounament_info;
					let commition_on_amount = (10 * 5) / 100;
					let charge: any = (.30 + commition_on_amount);
					this.gateway_charge = parseFloat(charge).toFixed(2);
					this.total_charge = (10 + parseFloat(this.gateway_charge)).toFixed(2);
				}

				// if (this.tournament_detail.no_of_admins > this.package_details.no_of_admins) {
				// 	this.amount = parseFloat(this.package == 'monthly' ? this.package_details.monthly_sub : this.package_details.yearly_sub).toFixed(2);
				// 	this.extra_court = this.tournament_detail.no_of_admins - this.package_details.no_of_admins;
				// 	this.additional_fee = (this.package == 'monthly' ? this.extra_court * this.package_details.additional_fee : (12 * this.extra_court * this.package_details.additional_fee));
				// 	let total_charge = parseFloat(this.amount) + this.additional_fee;
				// 	// let charge: any = (total_charge * 1.5) / 100;
				// 	// this.gateway_charge = parseFloat(charge).toFixed(2);
				// 	// let total_float = total_charge + charge;
				// 	// this.total_charge = parseFloat(total_float).toFixed(2);
				// 	// let vat: any = (parseFloat(total_charge) * 12) / 100;
				// 	// this.sub_total = (parseFloat(total_charge)).toFixed(2);
				// 	this.new_amount_to_pay = (parseFloat(total_charge) - parseFloat(this.paid_amount)).toFixed(2)
				// 	// let vat: any = (parseFloat(this.new_amount_to_pay) * 12) / 100;
				// 	this.sub_total = (parseFloat(this.new_amount_to_pay)).toFixed(2);
				// 	this.gateway_charge = (((this.sub_total * 2.9) / 100) + .30).toFixed(2);
				// 	// this.payment_vat = parseFloat(vat).toFixed(2);
				// 	this.total_charge = (parseFloat(this.sub_total) + parseFloat(this.gateway_charge)).toFixed(2);
				// }
				// else {
				// 	this.amount = parseFloat(this.package == 'monthly' ? this.package_details.monthly_sub : this.package_details.yearly_sub).toFixed(2);
				// 	// let vat: any = (parseFloat(this.amount) * 12) / 100;
				// 	// this.sub_total = (parseFloat(this.amount)).toFixed(2);
				// 	this.new_amount_to_pay = (parseFloat(this.amount) - parseFloat(this.paid_amount)).toFixed(2)
				// 	// this.total_pay_amount
				// 	// let vat: any = (parseFloat(this.new_amount_to_pay) * 12) / 100;
				// 	this.sub_total = (parseFloat(this.new_amount_to_pay)).toFixed(2);
				// 	this.gateway_charge = (((this.sub_total * 2.9) / 100) + .30).toFixed(2);
				// 	// this.payment_vat = parseFloat(vat).toFixed(2);
				// 	this.total_charge = (parseFloat(this.sub_total) + parseFloat(this.gateway_charge)).toFixed(2);
				// }
				this.tournament = true;
				this.route.params.subscribe(params => {
					this.id = parseInt(params['id']);
				});
			}
		})

	}

	pay() {
		if (this.ground) {
			this.doStripeGroundCreation();
		}
		else if (this.team) {
			this.doStripeTeamRegister();
		}
		else if (this.reservation) {
			//	this.doReservation();
			this.doStripeReservation();
		}
		if (this.tournament) {
			//this.doCreateTour()
			this.doStripeCreateTour()

		}

	}


	doStripeCreateTour() {
		this.paid = true;
		if (this.selected_payment_option === 'online') {
			this.loader = true;
			this.backdrop = true;
			let data = {
				"tournament_id": this.id,
				"form_type": "direct_pay",
			}
			localStorage.setItem("tournamentcreation", JSON.stringify(data))
			if (this.tour_second_pay) {
				this.router.navigateByUrl('dashboard/payment-form?tour_first_pay=' + false);
			}
			else {
				this.router.navigateByUrl('dashboard/payment-form?tour_first_pay=' + true);

			}


		}
		else if (this.selected_payment_option === 'card') {
			if (this.selected_card_option !== '') {
				this.loader = true;
				this.backdrop = true;
				let field_success = 'dashboard/tournaments/tournament-profile/' + this.id + '?status=1';
				let field_failed = 'dashboard/tournaments/tournament-profile/' + this.id + '?status=2';
				let url: any;
				if (this.tour_second_pay) {
					url = this.base_path_service.base_path_api() + 'tournament/createTournamentFinalPay/';
				}
				else {
					url = this.base_path_service.base_path_api() + 'tournament/createTournamentInitialPay/';
				}
				let data = {
					"tournament_id": this.id,
					"form_type": "card_pay",
					"card_id": this.selected_card_option,
				}
				this.base_path_service.PostRequest(url, data)
					.subscribe(res => {
						this.loader = false;
						this.backdrop = false;
						let status = res[0].json.success;
						if (status) {
							this.router.navigateByUrl(field_success)
						}
						else {
							this.router.navigateByUrl(field_failed)
						}
					}, err => {
						this.msgs = [];
						this.loader = false;
						this.backdrop = false;
						this.router.navigateByUrl(field_failed)


					})
			}
			else {
				this.paid = false;
				this.msgs = [];
				this.msgs.push({ severity: 'error', detail: "Please select card" });
			}
		}
		else {
			this.msgs = [];
			this.msgs.push({ severity: 'error', detail: "Please select payment option" });
		}
	}


	doStripeGroundCreation() {
		this.paid = true
		if (this.selected_payment_option === 'online') {
			this.loader = true;
			this.backdrop = true;
			// let url = this.base_path_service.base_path_api() + 'ground/field_pay_now/';
			let data = {
				ground_id: parseInt(this.id),
				sub_type: this.package,
				"form_type": "direct_pay",
			}
			localStorage.setItem("groundcreation", JSON.stringify(data))
			this.router.navigateByUrl('dashboard/payment-form');

		}
		else if (this.selected_payment_option === 'card') {
			if (this.selected_card_option !== '') {
				this.loader = true;
				this.backdrop = true;
				let field_success = 'dashboard/ground/ground-profile/' + this.id + '?status=1';
				let field_failed = 'dashboard/ground/ground-profile/' + this.id + '?status=2';
				let url = this.base_path_service.base_path_api() + 'ground/createGroundPay/';
				let data = {
					"card_id": this.selected_card_option,
					ground_id: parseInt(this.id),
					sub_type: this.package,
					"form_type": "card_pay",

				}
				this.base_path_service.PostRequest(url, data)
					.subscribe(res => {
						console.log("response by card=>>>>>>>>>>>>>>>>>>>>>", res)
						let status = res[0].json.success;
						console.log("status by card=>>>>>>>>>>>>>>>>>>>>>", status)

						if (status) {
							this.loader = false;
							this.backdrop = false;
							this.router.navigateByUrl(field_success)
						}
						else {
							this.router.navigateByUrl(field_failed)
						}
					}, err => {
						this.msgs = [];
						this.loader = false;
						this.backdrop = false;
						this.router.navigateByUrl(field_failed)


					})
			}
			else {
				this.paid = false
				this.msgs = [];
				this.msgs.push({ severity: 'error', detail: "Please select card" });
			}
		}
		else {
			this.msgs = [];
			this.msgs.push({ severity: 'error', detail: "Please select payment option" });
		}
	}


	doStripeReservation() {
		this.paid = true
		if (this.selected_payment_option === 'online') {
			this.loader = true;
			this.backdrop = true;
			let url = this.base_path_service.base_path_api() + 'scorecard/ground_request/?format=json';
			this.reservation_info.paid_with = this.option;
			this.base_path_service.PostRequest(url, this.reservation_info)
				.subscribe(res => {
					let data_obj = res[0].json;
					let reserve = {
						"ground_id": this.id,
						"court_id": parseInt(this.reservation_info.ground),
						"reservation_id": data_obj.reservation_id,
						"request_type": this.option
					}
					localStorage.setItem("reservation", JSON.stringify(reserve))
					this.router.navigateByUrl('dashboard/payment-form');
				})
		}
		else if (this.selected_payment_option === 'card') {
			if (this.selected_card_option !== '') {
				this.loader = true;
				this.backdrop = true;
				// let field_success = 'dashboard/ground/ground-profile/' + this.id + '?status=11';
				// let field_failed = 'dashboard/ground/ground-profile/' + this.id + '?status=12';
				this.reservation_info.paid_with = this.option;
				let url = this.base_path_service.base_path_api() + 'scorecard/ground_request/?format=json';
				this.base_path_service.PostRequest(url, this.reservation_info)
					.subscribe(res => {
						let data_obj = res[0].json;
						let reserve = {
							"ground_id": this.id,
							"court_id": parseInt(this.reservation_info.ground),
							"reservation_id": data_obj.reservation_id,
							"card_id": this.selected_card_option,
							"request_type": this.option,
							"save_card": true,
							"form_type": "card_pay",


						}
						let reservation_url = this.base_path_service.base_path_api() + 'ground/reserveFieldPay/';
						this.base_path_service.PostRequest(reservation_url, reserve)
							.subscribe(res => {
								if (res[0].json.success) {
									console.log("successssssssssssssss")
									this.router.navigateByUrl('/dashboard/ground/ground-profile/' + reserve.ground_id + '/schedule?status=11')
								}
								else {
									this.router.navigateByUrl('/dashboard/ground/ground-profile/' + reserve.ground_id + '/schedule?status=12')
								}
							}, err => {
								this.router.navigateByUrl('/dashboard/ground/ground-profile/' + reserve.ground_id + '/schedule?status=12')
							})

					}, err => {
						this.msgs = [];
						this.loader = false;
						this.backdrop = false;
						this.router.navigateByUrl('/dashboard/ground/ground-profile/' + this.id + '/schedule?status=2')


					})
			}
			else {
				this.paid = false;
				this.msgs = [];
				this.msgs.push({ severity: 'error', detail: "Please select card" });
			}

		}
		else {
			this.msgs = [];
			this.msgs.push({ severity: 'error', detail: "Please select payment option" });
		}
	}

	doStripeTeamRegister() {
		this.paid = true;
		if (this.selected_payment_option === 'online') {
			this.loader = true;
			this.backdrop = true;
			let data = {
				tournament_id: this.id,
				team_id: parseInt(this.team_id),
				request_type: this.option,
				"form_type": "direct_pay",
				"save_card": true,
				registration_id: this.team_pay_info.team_registration_id
			}
			localStorage.setItem("teamregister", JSON.stringify(data))
			this.router.navigateByUrl('dashboard/payment-form');
		}
		else if (this.selected_payment_option === 'card') {
			if (this.selected_card_option !== '') {
				this.loader = true;
				this.backdrop = true;
				// let field_success = 'dashboard/tournaments/tournament-profile/' + this.id + '?status=11';
				// let field_failed = 'dashboard/tournaments/tournament-profile/' + this.id + '?status=12';
				let url = this.base_path_service.base_path_api() + 'tournament/registerTeamPay/';
				let data = {
					card_id: this.selected_card_option,
					tournament_id: this.id,
					team_id: this.team_id,
					request_type: this.option,
					"form_type": "card_pay",
					"save_card": true,
					registration_id: this.team_pay_info.team_registration_id
				}
				this.base_path_service.PostRequest(url, data)
					.subscribe(res => {
						if (res[0].json.success) {
							console.log("successssssssssssssss")
							this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + data.tournament_id + '?status=1')
						}
						else {
							this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + data.tournament_id + '?status=2')
						}
					}, err => {
						this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + data.tournament_id + '?status=2')
					})
			}
			else {
				this.paid = false
				this.msgs = [];
				this.msgs.push({ severity: 'error', detail: "Please select card" });
			}
		}
		else {
			this.msgs = [];
			this.msgs.push({ severity: 'error', detail: "Please select payment option" });
		}
	}
	doTeamRegister() {
		this.paid = true;
		if (this.selected_payment_option === 'online') {
			this.loader = true;
			this.backdrop = true;

			let field_success = this.base_path_service.payment_path + '/dashboard/tournaments/tournament-profile/' + this.id + '?status=1';
			let field_failed = this.base_path_service.payment_path + '/dashboard/tournaments/tournament-profile/' + this.id + '?status=2';
			let url = this.base_path_service.base_path_api() + 'tournament/team_pay_now/';
			let data = {
				tournament_id: this.id,
				team_id: this.team_id,
				request_type: this.option
			}
			this.base_path_service.PostRequest(url, data)
				.subscribe(res => {
					this.loader = false;
					this.backdrop = false;

					let data = res[0].json;
					this.redirect = data.header + '?application_code=' + data.application_code + '&uid=' + data.uid +
						'&auth_timestamp=' + data.auth_timestamp + '&auth_token=' + data.auth_token + '&dev_reference=' + data.dev_reference +
						'&product_description=' + data.product_description + '&product_code=' + data.product_code + '&product_amount=' + data.product_amount +
						'&success_url=' + field_success + '&failure_url=' + field_failed + '&review_url=' + data.review_url + '&vat=' + data.vat +
						'&installments_type=' + data.installments_type + '&tax_percentage=' + data.tax_percentage + '&taxable_amount=' + data.taxable_amount;;
					// window.location.href = this.redirect;
					localStorage.setItem('url', this.redirect);
					this.router.navigateByUrl('dashboard/ground/payment');
				}, err => {
					this.loader = false;
					this.backdrop = false;

				})
		}
		else if (this.selected_payment_option === 'card') {
			if (this.selected_card_option !== '') {
				this.loader = true;
				this.backdrop = true;

				let field_success = 'dashboard/tournaments/tournament-profile/' + this.id + '?status=11';
				let field_failed = 'dashboard/tournaments/tournament-profile/' + this.id + '?status=12';

				let url = this.base_path_service.base_path_api() + 'tournament/teamDebitSavedCard/';
				let data = {
					card_reference: this.selected_card_option,
					tournament_id: this.id,
					team_id: this.team_id,
					request_type: this.option
				}
				this.base_path_service.PostRequest(url, data)
					.subscribe(res => {
						this.loader = false;
						this.backdrop = false;

						let status = JSON.parse(res[0].json).status;
						if (status === 'success') {
							this.router.navigateByUrl(field_success)
						}
						else {
							this.router.navigateByUrl(field_failed)

						}
					}, err => {
						this.msgs = [];
						this.loader = false;
						this.backdrop = false;
						this.router.navigateByUrl(field_failed)


					})
			}
			else {
				this.paid = false
				this.msgs = [];
				this.msgs.push({ severity: 'error', detail: "Please select card" });
			}
		}
		else {
			this.msgs = [];
			this.msgs.push({ severity: 'error', detail: "Please select payment option" });
		}

	}
	doReservation() {
		this.paid = true;
		if (this.selected_payment_option === 'online') {
			this.loader = true;
			this.backdrop = true;
			let field_success = this.base_path_service.payment_path + '/dashboard/ground/ground-profile/' + this.id + '/schedule?status=1';
			let field_failed = this.base_path_service.payment_path + '/dashboard/ground/ground-profile/' + this.id + '/schedule?status=2';
			let url = this.base_path_service.base_path_api() + 'scorecard/ground_request/?format=json';
			this.reservation_info.paid_with = this.option;
			this.base_path_service.PostRequest(url, this.reservation_info)
				.subscribe(res => {
					let data_obj = res[0].json;
					let reserve = {
						"ground_id": this.id,
						"court_id": parseInt(this.reservation_info.ground),
						"reservation_id": data_obj.reservation_id,
						"request_type": this.option
					}
					let reservation_url = this.base_path_service.base_path_api() + 'ground/field_reserve_pay/';
					this.base_path_service.PostRequest(reservation_url, reserve)
						.subscribe(res => {
							let data = res[0].json;
							this.redirect = data.header + '?application_code=' + data.application_code + '&uid=' + data.uid +
								'&auth_timestamp=' + data.auth_timestamp + '&auth_token=' + data.auth_token + '&dev_reference=' + data.dev_reference +
								'&product_description=' + data.product_description + '&product_code=' + data.product_code + '&product_amount=' + data.product_amount +
								'&success_url=' + field_success + '&failure_url=' + field_failed + '&review_url=' + data.review_url + '&vat=' + data.vat +
								'&installments_type=' + data.installments_type + '&tax_percentage=' + data.tax_percentage + '&taxable_amount=' + data.taxable_amount;;
							// window.location.href = this.redirect;
							localStorage.setItem('url', this.redirect);
							this.router.navigateByUrl('dashboard/ground/payment');
						})
				}, err => {
					this.loader = false;
					this.backdrop = false;

				})
		}
		else if (this.selected_payment_option === 'card') {
			if (this.selected_card_option !== '') {
				this.loader = true;
				this.backdrop = true;
				let field_success = 'dashboard/ground/ground-profile/' + this.id + '?status=11';
				let field_failed = 'dashboard/ground/ground-profile/' + this.id + '?status=12';
				this.reservation_info.paid_with = this.option;
				let url = this.base_path_service.base_path_api() + 'scorecard/ground_request/?format=json';
				this.base_path_service.PostRequest(url, this.reservation_info)
					.subscribe(res => {
						let data_obj = res[0].json;
						let reserve = {
							"ground_id": this.id,
							"court_id": parseInt(this.reservation_info.ground),
							"reservation_id": data_obj.reservation_id,
							"card_reference": this.selected_card_option,
							"request_type": this.option
						}
						let reservation_url = this.base_path_service.base_path_api() + 'ground/field_reserve_card_pay/';
						this.base_path_service.PostRequest(reservation_url, reserve)
							.subscribe(res => {
								let status = JSON.parse(res[0].json).status;
								if (status === 'success') {
									this.router.navigateByUrl(field_success)
								}
								else {
									this.router.navigateByUrl(field_failed)
								}
							}, err => {
								this.router.navigateByUrl(field_failed)
							})

					}, err => {
						this.msgs = [];
						this.loader = false;
						this.backdrop = false;
						this.router.navigateByUrl(field_failed)


					})
			}
			else {
				this.paid = false;
				this.msgs = [];
				this.msgs.push({ severity: 'error', detail: "Please select card" });
			}

		}
		else {
			this.msgs = [];
			this.msgs.push({ severity: 'error', detail: "Please select payment option" });
		}
	}
	doGroundCreation() {
		this.paid = true
		if (this.selected_payment_option === 'online') {
			this.loader = true;
			this.backdrop = true;
			let field_success = this.base_path_service.payment_path + '/dashboard/ground/ground-profile/' + this.id + '?status=1';
			let field_failed = this.base_path_service.payment_path + '/dashboard/ground/ground-profile/' + this.id + '?status=2';
			let url = this.base_path_service.base_path_api() + 'ground/field_pay_now/';
			let data = {
				ground_id: this.id,
				sub_type: this.package
			}
			this.base_path_service.PostRequest(url, data)
				.subscribe(res => {
					this.loader = false;
					this.backdrop = false;
					let data = res[0].json;
					this.redirect = data.header + '?application_code=' + data.application_code + '&uid=' + data.uid +
						'&auth_timestamp=' + data.auth_timestamp + '&auth_token=' + data.auth_token + '&dev_reference=' + data.dev_reference +
						'&product_description=' + data.product_description + '&product_code=' + data.product_code + '&product_amount=' + data.product_amount +
						'&success_url=' + field_success + '&failure_url=' + field_failed + '&review_url=' + data.review_url + '&vat=' + data.vat +
						'&installments_type=' + data.installments_type + '&tax_percentage=' + data.tax_percentage + '&taxable_amount=' + data.taxable_amount;
					localStorage.removeItem('package_detail');
					// window.location.href = this.redirect;
					localStorage.setItem('url', this.redirect);
					this.router.navigateByUrl('dashboard/ground/payment');
				}, err => {
					this.loader = false;
					this.backdrop = false;

				})
		}
		else if (this.selected_payment_option === 'card') {
			if (this.selected_card_option !== '') {
				this.loader = true;
				this.backdrop = true;

				let field_success = 'dashboard/ground/ground-profile/' + this.id + '?status=true';
				let field_failed = 'dashboard/ground/ground-profile/' + this.id + '?status=false';
				let url = this.base_path_service.base_path_api() + 'ground/debitSavedCard/';
				let data = {
					"card_reference": this.selected_card_option,
					ground_id: this.id,
					sub_type: this.package
				}
				this.base_path_service.PostRequest(url, data)
					.subscribe(res => {
						let status = JSON.parse(res[0].json).status;
						if (status === 'success') {
							this.loader = false;
							this.backdrop = false;

							this.router.navigateByUrl(field_success)
						}
						else {
							this.router.navigateByUrl(field_failed)
						}
					}, err => {
						this.msgs = [];
						this.loader = false;
						this.backdrop = false;
						this.router.navigateByUrl(field_failed)


					})
			}
			else {
				this.paid = false
				this.msgs = [];
				this.msgs.push({ severity: 'error', detail: "Please select card" });
			}
		}
		else {
			this.msgs = [];
			this.msgs.push({ severity: 'error', detail: "Please select payment option" });
		}
	}


	doCreateTour() {
		this.paid = true;
		if (this.selected_payment_option === 'online') {
			this.loader = true;
			this.backdrop = true;

			let field_success = this.base_path_service.payment_path + '/dashboard/tournaments/tournament-profile/' + this.id + '?status=1';
			let field_failed = this.base_path_service.payment_path + '/dashboard/tournaments/tournament-profile/' + this.id + '?status=2';
			let url = this.base_path_service.base_path_api() + 'tournament/tournament_pay_now/';
			let data = {
				"tournament_id": this.id,
				'sub_type': this.package
			}
			this.base_path_service.PostRequest(url, data)
				.subscribe(res => {
					this.loader = false;
					this.backdrop = false;

					let data = res[0].json;
					this.redirect = data.header + '?application_code=' + data.application_code + '&uid=' + data.uid +
						'&auth_timestamp=' + data.auth_timestamp + '&auth_token=' + data.auth_token + '&dev_reference=' + data.dev_reference +
						'&product_description=' + data.product_description + '&product_code=' + data.product_code + '&product_amount=' + data.product_amount +
						'&success_url=' + field_success + '&failure_url=' + field_failed + '&review_url=' + data.review_url + '&vat=' + data.vat +
						'&installments_type=' + data.installments_type + '&tax_percentage=' + data.tax_percentage + '&taxable_amount=' + data.taxable_amount;;
					localStorage.removeItem('package_detail');
					// window.location.href = this.redirect;
					// window.open(this.redirect)
					localStorage.setItem('url', this.redirect);
					this.router.navigateByUrl('dashboard/payment');
				}, err => {
					this.loader = false;
					this.backdrop = false;
					this.paid = false

				})
		}
		else if (this.selected_payment_option === 'card') {
			if (this.selected_card_option !== '') {
				this.loader = true;
				this.backdrop = true;
				let field_success = 'dashboard/tournaments/tournament-profile/' + this.id + '?status=11';
				let field_failed = 'dashboard/tournaments/tournament-profile/' + this.id + '?status=12';
				let url = this.base_path_service.base_path_api() + 'tournament/tournamentSavedCard/';
				let data = {
					"card_reference": this.selected_card_option,
					"tournament_id": this.id,
					'sub_type': this.package
				}
				this.base_path_service.PostRequest(url, data)
					.subscribe(res => {
						this.loader = false;
						this.backdrop = false;

						let status = JSON.parse(res[0].json).status;
						if (status === 'success') {
							this.router.navigateByUrl(field_success)
						}
						else {
							this.router.navigateByUrl(field_failed)
						}
					}, err => {
						this.msgs = [];
						this.loader = false;
						this.backdrop = false;
						this.router.navigateByUrl(field_failed)


					})
			}
			else {
				this.paid = false;
				this.msgs = [];
				this.msgs.push({ severity: 'error', detail: "Please select card" });
			}
		}
		else {
			this.msgs = [];
			this.msgs.push({ severity: 'error', detail: "Please select payment option" });
		}
	}

	getCardList() {
		this.loader = true;
		let url = this.base_path_service.base_path_api() + 'user/listCards/';
		this.base_path_service.GetRequest(url)
			.subscribe(res => {
				this.loader = false;
				this.results = [];
				console.log("carddddddddddddddd", res[0].json.data)
				this.results = res[0].json.data;
			}, err => {
				this.loader = false
			})
	}

	addCard() {
		this.loader = true;
		let url = this.base_path_service.base_path_api() + 'ground/field_add_card/';
		if (this.team) {
			this.card_data = {
				"success_url": this.base_path_service.payment_path + "/dashboard/payment/" + this.id + "?team_id=" + this.team_id + "&team=true&option=" + this.option + "&status=1",
				"failure_url": this.base_path_service.payment_path + "/dashboard/payment/" + this.id + "?team_id=" + this.team_id + "&team=true&option=" + this.option + "&status=2",
			}
		}
		else if (this.ground) {
			this.card_data = {
				"success_url": this.base_path_service.payment_path + "/dashboard/payment/" + this.id + "?ground=true&package=" + this.package + "&status=1",
				"failure_url": this.base_path_service.payment_path + "/dashboard/payment/" + this.id + "?ground=true&package=" + this.package + "&status=2",
			}
		}
		else if (this.reservation) {
			this.card_data = {
				"success_url": this.base_path_service.payment_path + "/dashboard/payment/" + this.id + "?reserve=true&option=" + this.option + "&recurrent=" + this.recurrent + "&status=1",
				"failure_url": this.base_path_service.payment_path + "/dashboard/payment/" + this.id + "?reserve=true&option=" + this.option + "&recurrent=" + this.recurrent + "&status=2",
			}
		}
		else if (this.tournament) {
			this.card_data = {
				"success_url": this.base_path_service.payment_path + "/dashboard/payment/" + this.id + "?tournament=true&package=" + this.package + "&status=1",
				"failure_url": this.base_path_service.payment_path + "/dashboard/payment/" + this.id + "?tournament=true&package=" + this.package + "&status=2",
			}
		}
		this.base_path_service.PostRequest(url, this.card_data)
			.subscribe(res => {
				this.loader = false;
				this.redirect = res[0].json;
				// window.location.href = this.redirect;
				localStorage.setItem('url', this.redirect);
				this.router.navigateByUrl('dashboard/payment');
			}, err => {
				this.loader = false;
			})
	}

	removeCard(card, ind) {
		this.loader = true;
		let url = this.base_path_service.base_path_api() + 'ground/field_delete_card/';
		let data = {
			"card_reference": card
		}
		this.base_path_service.PostRequest(url, data)
			.subscribe(res => {
				this.loader = false;
				this.msgs = [];
				this.getCardList();
				this.msgs.push({ severity: 'info', detail: "Card removed succefully" });
			}, err => {
				this.loader = false;
				this.msgs = [];
				this.msgs.push({ severity: 'error', detail: err.json().error });
			})
	}

	back() {
		this.loader = true;
		if (this.team) {
			this.loader = false;
			this.router.navigateByUrl('dashboard/tournaments/tournament-profile/' + this.id);
		}
		else if (this.ground) {
			this.loader = false;
			this.router.navigateByUrl('dashboard/package/' + this.id + "?status=ground&option=" + this.package);

		}
		else if (this.reservation) {
			this.loader = false;
			this.router.navigateByUrl('dashboard/payment-option/' + this.id + '?reserve=true&recurrent=' + this.recurrent + "&option=" + this.option);

		}
		else if (this.tournament) {
			this.loader = false;
			this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + this.id + '?status=2')
			// this.router.navigateByUrl('dashboard/package/' + this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + data.tournament_id + '?status=1')
			// 	+ '?status=tournament&option=' + this.package);

		}
	}
}
