import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroundPaymentComponent } from './ground-payment.component';

describe('GroundPaymentComponent', () => {
  let component: GroundPaymentComponent;
  let fixture: ComponentFixture<GroundPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroundPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroundPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
