import { NavbarComponent } from "./../navbar/navbar.component";
import { ModuleWithProviders } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MessagesModule } from "./../messages/messages.module";
import { PlayerDetailsModule } from "./../player-details/player-details.module";
import { DashboardComponent } from "./../dashboard/dashboard.component";
import { GroundDetailsModule } from "./../ground-details/ground-details.module";
import { TeamDetailsModule } from "./../team-details/team-details.module";
import { TournamentDetailsModule } from "./../tournament-details/tournament-details.module";
import { FollowersComponent } from "./../followers/followers.component";
import { FollowingsComponent } from "./../followings/followings.component";
import { NotificationComponent } from "./../notification/notification.component";
import { ShoutoutComponent } from "./../shoutout/shoutout.component";
import { PaymentComponent } from "./../payment/payment.component";
import { PaymentOptionComponent } from "./../payment-option/payment-option.component";
import { PackageComponent } from "./../package/package.component";
import { PaymentformComponent } from "./../paymentform/paymentform.component";
import { AddCardRedirectComponent } from "./../add-card-redirect/add-card-redirect.component";
import { GalleryuploadComponent } from "../galleryupload/galleryupload.component";

export const NavbarRoutes = [
  {
    path: "",
    component: NavbarComponent,
    children: [
      { path: "", component: DashboardComponent },
      {
        path: "player",
        loadChildren:
          "./../player-details/player-details.module#PlayerDetailsModule"
      },
      {
        path: "messages/:id",
        loadChildren: "./../messages/messages.module#MessagesModule"
      },
      {
        path: "ground",
        loadChildren:
          "./../ground-details/ground-details.module#GroundDetailsModule"
      },
      {
        path: "team",
        loadChildren: "./../team-details/team-details.module#TeamDetailsModule"
      },
      {
        path: "tournaments",
        loadChildren:
          "./../tournament-details/tournament-details.module#TournamentDetailsModule"
      },
      { path: "followers", component: FollowersComponent },
      { path: "followings", component: FollowingsComponent },
      { path: "notification", component: NotificationComponent },
      { path: "shoutouts/:id/:type", component: ShoutoutComponent },
      { path: "payment/:id", component: PaymentComponent },
      { path: "payment-option/:id", component: PaymentOptionComponent },
      { path: "package/:id", component: PackageComponent },
      { path: "payment-form", component: PaymentformComponent },
      { path: "payment", component: AddCardRedirectComponent },
      { path: "gallery", component: GalleryuploadComponent },
      {
        path: "chat",
        loadChildren:
          "./../communitychat/communitychat.module#CommunitychatModule"
      }
    ]
  }
];

export const NavbarRouting: ModuleWithProviders = RouterModule.forChild(
  NavbarRoutes
);
