import {
  Component,
  OnInit,
  HostListener,
  keyframes,
  trigger,
  state,
  animate,
  transition,
  style,
  NgZone,
  EventEmitter,
  Output
} from "@angular/core";
import {
  Validators,
  FormBuilder,
  FormControl,
  FormGroup,
  NgForm,
  AbstractControl
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { GlobalService } from "./../../GlobalService";
import {
  Button,
  Growl,
  Message,
  InputTextarea,
  Dropdown,
  SelectItem,
  AutoComplete,
  InputText
} from "primeng/primeng";
import { FooterComponent } from "../../container/footer/footer.component";
import { TranslateService } from "ng2-translate";
declare var gapi: any;
declare var $: any;
declare const FB: any;
declare var ReconnectingWebSocket: any;

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"],
  animations: [
    trigger("myAnimation", [
      transition(":enter", [
        style({ transform: "translateY(-100%)", opacity: 0 }),
        animate("500ms", style({ transform: "translateY(0)", opacity: 1 }))
      ]),
      transition(":leave", [
        style({ transform: "translateY(0)", opacity: 1 }),
        animate("500ms", style({ transform: "translateY(-100%)", opacity: 0 }))
      ])
    ]),
    trigger("dialog", [
      transition("void => *", [
        style({ transform: "scale3d(.3, .3, .3)" }),
        animate(100)
      ]),
      transition("* => void", [
        animate(100, style({ transform: "scale3d(.0, .0, .0)" }))
      ])
    ])
  ]
})
export class NavbarComponent implements OnInit {
  location: string;
  show_footer: boolean = true;
  show1: boolean = false;
  username: string;
  not: boolean = false;
  last_name: string;
  nick_name: string;
  profile: string;
  times: boolean = false;
  display: boolean = false;
  outlet: boolean = false;
  shoutout: Array<any>;
  notification: Array<any>;
  text: any = "";
  team: any;
  counts: number = 0;
  datas: boolean = false;
  notifications: boolean = false;
  ground: any;
  ground_id: number = null;
  tournament: any;
  team_id: number = null;
  tournament_id: number = null;
  type: string = "player";
  data: any;
  basePath: string = "";
  Shoutout_Form: FormGroup;
  AS: any = { id: 111, type: "player" };
  backdrop: boolean = false;
  invitePopup: boolean = false;
  userId: number = null;
  msgs: Message[] = [];
  click: boolean = false;
  link: any;
  apiPath: string = "";
  public logoutSet: boolean = false;
  languages: SelectItem[];
  selectedLang: any;
  state: string = "inactive";
  displayDiv: any = false;
  displayFlag: any = "es";
  leagues: any[] = [];
  leaguearray: any = [];
  clubsdisplay: boolean = false;
  leaguedata: any;
  teamsdata: any[] = [];
  teamselected: any;
  membertype: any = "manager";
  shoutoutmsg: any;
  errormsg: any;
  selectedValues: any;
  communityurl_name: any;
  communityalldata: any;
  uinfo: any;
  communitydata: any;
  logo = this.base_path_service.image_url;
  comdata: any;
  comname: any;
  loader: boolean;
  user_email: any;
  usercommunities: any;
  userpart: boolean = false;
  chatpop: boolean = false;
  communitycredential: any;
  communitynotexist:boolean=false;

  @Output()
  valueChange = new EventEmitter();
  Counter: any = 0;

  constructor(
    private translate: TranslateService,
    public router: Router,
    public base_path_service: GlobalService,
    form: FormBuilder,
    public activatedRoute: ActivatedRoute,
    public zone: NgZone
  ) {

    if (localStorage.getItem("language")) {
      if (
        localStorage.getItem("language") == null ||
        localStorage.getItem("language") == "null"
      ) {
        localStorage.setItem("language", "en");
        console.log(
          "nullllllllllllllllllllllllllll",
          localStorage.getItem("language")
        );
        translate.use("en");
      } else {
        translate.use(localStorage.getItem("language"));
      }
    } else {
      translate.setDefaultLang("es");
    }

    
    this.communityurl_name = this.router.url.split("/");
    console.log(
      this.communityurl_name[1],
      "this.communityurl_name after split"
    );
    this.getcomunitycredential();
    localStorage.setItem("communitynav_name", this.communityurl_name[1]);
    this.base_path_service.changeName.subscribe(res => {
      this.username = res;
    });

    let url = window.location.href;
    this.location = localStorage.getItem("city");
    let check_string = url.split("/")[5];
    if (check_string === "addcard") {
      this.show_footer = false;
    }
    if (localStorage.getItem("language")) {
      this.displayFlag = localStorage.getItem("language");
    }

    this.languages = [];
    this.languages.push({ label: "Select languages", value: null });
    this.languages.push({ label: "Spanish", value: "es" });
    this.languages.push({ label: "English", value: "en" });
    this.Shoutout_Form = form.group({
      text: new FormControl("", Validators.compose([Validators.required]))
    });
    if (localStorage.getItem("language") != null) {
      translate.use(localStorage.getItem("language"));
    }

    $(document).click(event => {
      if (event.target.id == "flag") {
        this.displayDiv = this.displayDiv ? false : true;
      } else {
        this.displayDiv = false;
      }
    });
  }
  getcomunitycredential() {
    if (localStorage.getItem("user_info")) {
      this.uinfo = JSON.parse(localStorage.getItem("user_info"));
    }
    this.loader = true;
    let url =
      this.base_path_service.base_path +
      "api/peloteando/communityDetail/?community_name=" +
      this.communityurl_name[1];
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.loader = false;
        this.communitycredential = res[0].json;
        console.log(
          this.communitycredential,
          "all community credentials========"
        );
        localStorage.setItem(
          "community_info",
          JSON.stringify(this.communitycredential)
        );
        localStorage.setItem("communityid", res[0].json.community_id);
      },
      err => {
        console.log("inside the error for nonexisting community======");
        localStorage.removeItem("communitynav_name");
        this.loader = false;
        localStorage.removeItem("community_info");
        this.communitynotexist = true;
      }
    );
  }
  tocommunnity(){
    this.communitynotexist = false;
    this.router.navigateByUrl("/home/community");
  }

  ngOnInit() {
    if (localStorage.getItem("community_info")) {
      this.comdata = JSON.parse(localStorage.getItem("community_info"));
      this.communitydata = this.comdata;
      this.comname = this.comdata.community_name;
    } else {
      this.comname = localStorage.getItem("communitynav_name");
      console.log(this.comname, "name we get from the url");
    }

    if (localStorage.getItem("user_info")) {
      this.userpart = true;
      this.uinfo = JSON.parse(localStorage.getItem("user_info"));
      this.getcommunitylist();
    } else {
      this.userpart = false;
    }

    this.basePath = this.base_path_service.image_url;
    this.apiPath = this.base_path_service.base_path;
    this.getProfileInfo();
    this.base_path_service.demoCheck.subscribe(res => {
      this.getProfileInfo();
    });

    this.base_path_service.navCheck.subscribe(res => {
      this.getProfileInfo();
    });
    try {
      $(window).bind("scroll", function() {
        if ($(window).scrollTop() > 70) {
          $(".header").addClass("fixed");
        } else {
          $(".header").removeClass("fixed");
        }
      });

      $(".shoutout").click(function($event) {
        $(".backdrop").css({ display: "block" });
      });

      $(document).click(f => {
        if ($(f.target).is("#logoutModal,#logoutModal *")) {
          return;
        } else {
          this.show1 = false;
          this.notifications = false;
        }
      });

      $(document).keyup(e => {
        if (e.which == 27) {
          this.show1 = false;
          this.notifications = false;
        }
      });
    } catch (exception) {}
    this.notificationConnection();
  }

  joinanothercommunity() {
    this.router.navigateByUrl("home/community");
  }
  toregister() {
    this.router.navigateByUrl("home/community/selectrole");
  }
  getcommunitylist() {
    let url = this.base_path_service.base_path + "api/user/communities/";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.usercommunities = res[0].json;
        console.log(this.usercommunities, "usercommunities==================");
      },
      err => {}
    );
  }
  chatpopup() {
    this.chatpop = true;
  }
  closechatpop() {
    this.chatpop = false;
  }

  toselectedcommunity(id) {
    let community_id = id;
    console.log(
      community_id,
      "community id for the selection in navar dashboard"
    );
    this.getcommunityinfo(community_id);
  }
  getcommunityinfo(id) {
    let url =
      this.base_path_service.base_path + "api/user/communities/" + id + "/";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        let data = res[0].json;
        console.log(data, "data==============");
        localStorage.setItem("community_info", JSON.stringify(data));
        // for the redirection
        if (localStorage.getItem("community_info")) {
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.zone.run(() => {
            this.router.navigate([comname]);
            window.location.reload();
          });
        } else {
        }
      },
      err => {}
    );
  }

  completeProfile() {
    this.loader = true;
    let url =
      this.base_path_service.base_path_api() +
      "user/completeProfile/?format=json";
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.loader = false;
        localStorage.setItem("checkoptionalpage", "complete_profile");
        localStorage.setItem("user_info", JSON.stringify(res[0].json.data));
        let user_info = JSON.parse(localStorage.getItem("user_info"));
        if (!user_info.is_phone_no) {
          this.router.navigate(["/home/signup/set-phone"]);
        } else if (!user_info.is_email) {
          this.router.navigate(["/home/signup/set-email"]);
        } else if (!user_info.is_username) {
          this.router.navigate(["/home/signup/set-user"]);
        } else if (!user_info.is_gender) {
          this.router.navigate(["/home/signup/set-gender"]);
        } else if (!user_info.is_height) {
          this.router.navigate(["/home/signup/set-height"]);
        } else if (!user_info.is_dob) {
          this.router.navigate(["/home/signup/set-dob"]);
        } else if (!user_info.is_leg) {
          this.router.navigate(["/home/signup/set-leg"]);
        } else if (!user_info.is_position) {
          this.router.navigate(["/home/signup/set-position"]);
        } else if (!user_info.is_profile_pic) {
          this.router.navigate(["/home/signup/set-profile-pic"]);
        }
        // else{
        //   this.router.navigateByUrl('dashboard/player/player-profile/' + this.player_id + '/information');
        // }
      },
      err => {
        this.loader = false;
      }
    );
  }

  togallery() {
    let comdata = JSON.parse(localStorage.getItem("community_info"));
    console.log(comdata, "fgewguigewewrgwue");
    let comname = comdata.community_name;
    console.log(comname, "community name we get======================");
    this.router.navigateByUrl(comname + "/gallery");
  }
  notificationConnection() {
    if (localStorage.getItem("user_info") && this.uinfo.approved) {
      let ws_path = this.base_path_service.ws_path;
      let user_id = JSON.parse(localStorage.getItem("userInfo"));
      let socket = new ReconnectingWebSocket(ws_path);
      let data_to_be_send = {
        action: "notification",
        user: user_id.info.user_id
      };
      setTimeout(res => {
        socket.send(JSON.stringify(data_to_be_send));
        console.log(data_to_be_send, "data AT the time of connection create");
      }, 2000);
      socket.onclose(err => {
        console.log("socked Closed due to some reason ");
        let socket = new ReconnectingWebSocket(ws_path);
        socket.send(JSON.stringify(data_to_be_send));
      });
      socket.onerror(close => {
        console.log("socked Error due to some reason");
        let socket = new ReconnectingWebSocket(ws_path);
        socket.send(JSON.stringify(data_to_be_send));
      });
      socket.onmessage = message => {
        let data = JSON.parse(message.data);
        this.counts = data.total_notification;
        if (this.counts) {
          this.not = true;
        } else {
          this.not = false;
        }
        console.log(
          "Got Notification count: ",
          this.counts,
          message.data,
          ", For user Id:",
          data_to_be_send
        );
      };
    }
  }
  toggleMove() {
    this.state = this.state === "inactive" ? "active" : "inactive";
  }

  notify() {
    if (this.notifications) {
      this.notifications = false;
    } else {
      this.notifications = true;
      let url = this.apiPath + "api/user/notification/?format=json";
      this.base_path_service.GetRequest(url).subscribe(res => {
        this.notification = res[0].json.data;
        this.counts = 0;
      });
    }
  }

  getProfileInfo() {
    if (localStorage.getItem("user_info") && this.uinfo.approved) {
      let data = JSON.parse(localStorage.getItem("user_info"));
      this.user_email = data.email;
      console.log(this.user_email, "name===============");

      this.last_name = localStorage.getItem("last_name");
      this.username = localStorage.getItem("username");
      // this.nick_name = JSON.parse(localStorage.getItem("nick_name"));

      this.userId = JSON.parse(
        localStorage.getItem("userInfo")
      ).info.profile_id;
      if (localStorage.getItem("profile_pic")) {
        console.log("local");
        if (localStorage.getItem("profile_pic") == "null") {
          console.log("local if 1111111");
          this.profile = "";
        } else {
          console.log(
            "local if 1111111 else",
            typeof localStorage.getItem("profile_pic")
          );
          this.profile =
            this.base_path_service.image_url +
            "/" +
            localStorage.getItem("profile_pic");
        }
      } else {
        console.log("profile else", this.profile);
        this.profile = "";
      }
    }
  }
  myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }

  showDialog() {
    this.display = true;
    this.getleagues();
    // let url = this.base_path_service.base_path + "api/user/shoutout_user/";
    // this.base_path_service.GetRequest(url).subscribe(res => {
    //   this.ground = res[0].json.ground;
    //   this.team = res[0].json.team;
    //   this.tournament = res[0].json.tournament;
    //   this.pushdata();
    // });
  }

  closeDialog() {
    // $(".backdrop").css({ display: "none" });
    console.log("close dialog");
    this.display = false;
    this.text = "";
    this.type = "player";
    this.ground_id = null;
    this.tournament_id = null;
    this.team_id = null;
    this.AS = { id: 111, type: "player" };
    this.leaguearray = [];
    this.leaguedata = [];
    this.leagues = [];
    this.teamselected = [];
    this.shoutoutmsg = "";
    this.membertype = "manager";
    this.selectedValues = "";
    this.teamsdata = [];
    this.clubsdisplay = false;
  }

  pushdata() {
    this.shoutout = [];
    this.shoutout.push({
      label: "jugador",
      value: { id: 111, type: "player" }
    });
    for (let i = 0; i < this.team.length; i++) {
      this.shoutout.push({
        label: this.team[i].team_name,
        value: { id: this.team[i].id, type: "team" }
      });
    }
    for (let i = 0; i < this.tournament.length; i++) {
      this.shoutout.push({
        label: this.tournament[i].tournament_name,
        value: { id: this.tournament[i].id, type: "tournament" }
      });
    }
    for (let i = 0; i < this.ground.length; i++) {
      this.shoutout.push({
        label: this.ground[i].ground_name,
        value: { id: this.ground[i].id, type: "ground" }
      });
    }
  }

  post() {
    let url = this.base_path_service.base_path + "api/user/shoutout/";
    this.data = {
      user_type: this.type,
      team: this.team_id,
      tournament: this.tournament_id,
      shoutout: this.text,
      ground: this.ground_id
    };
    this.base_path_service.PostRequest(url, this.data).subscribe(res => {
      this.base_path_service.shoutType = this.data.user_type;
      this.msgs = [];
      this.msgs.push({
        severity: "info",
        summary: "",
        detail: "Publicado con éxito!"
      });
      this.base_path_service.parentChildFun();
    });
    this.AS = { id: 111, type: "player" };
    this.type = "player";
    this.text = "";
    this.tournament_id = null;
    this.team_id = null;
    this.ground_id = null;
    this.display = false;
    $(".backdrop").css({ display: "none" });
  }
  ver() {
    this.notifications = false;
  }

  show(event) {
    this.type = event.value.type;
    if (this.type == "team") {
      this.team_id = event.value.id;
      this.tournament_id = null;
      this.ground_id = null;
    } else if (this.type == "tournament") {
      this.tournament_id = event.value.id;
      this.team_id = null;
      this.ground_id = null;
    } else if (this.type == "ground") {
      this.ground_id = event.value.id;
      this.tournament_id = null;
      this.team_id = null;
    } else if (this.type == "player") {
      this.ground_id = null;
      this.tournament_id = null;
      this.team_id = null;
    }
  }
  disable() {
    this.display = false;
    this.outlet = false;
  }

  showInfo() {
    if (this.show1) {
      this.show1 = false;
    } else {
      this.show1 = true;
    }
    this.notifications = false;
  }
  logout() {
    let lang = localStorage.getItem("language");
    localStorage.clear();
    localStorage.setItem("language", lang);
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.disconnect();
  }
  opennav() {
    $(".sidenav").animate({ left: "0px" });
    $(".backdrop").css({ display: "block" });
  }
  closeNav() {
    $(".sidenav").animate({ left: "-260px" });
    $(".backdrop").css({ display: "none" });
    this.notifications = false;
  }
  close() {
    $(".sidenav").animate({ left: "-260px" });
    $(".backdrop").css({ display: "none" });
    this.notifications = false;
  }
  tocommunity() {
    this.router.navigate([this.comname]);
  }
  joincommunity() {
    this.router.navigateByUrl("/home/community/selectrole");
  }
  routing() {
    console.log(this.comname, "w45r454354543");
    this.router.navigateByUrl(
      this.comname + "/player/player-profile/" + this.userId + "/information"
    );
    this.base_path_service.parentChildFunForProfileOnly();
  }
  navigate_to_link(link: string) {
    var temp = link.split("/");
    this.router.navigateByUrl(link);
    this.base_path_service.pageChange.next(temp);
  }

  saveCurrentRoute() {
    let cr: string = window.location.hash;
    cr = cr.slice(1);
    localStorage.setItem("cr", cr);
    this.router.navigateByUrl("change-password");
  }
  public requestPlayer() {
    this.base_path_service.playerRequestRefresh.next("rerfresh");
    this.router.navigateByUrl(
      this.comname + "/player/player-profile/" + this.userId + "/requests"
    );
  }
  public messages() {
    this.router.navigateByUrl(this.comname + "/messages/" + this.userId);
  }
  public setLanguage() {
    localStorage.setItem("lang", this.selectedLang);
    location.reload();
  }

  gotToLocation() {
    this.router.navigateByUrl("home/location");
  }
  showclubs() {
    this.clubsdisplay = true;
    console.log(this.leagues, "ngmode;lleagues============");
    this.getteams();
  }

  getleagues() {
    this.leaguearray = [];
    this.leaguedata = [];
    let data;
    var url =
      this.base_path_service.base_path + "api/user/shoutOut/?form_type=league";
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        console.log(res, "res=================");
        this.leaguedata = res[0].json.data;
        this.leaguedata.forEach(element => {
          this.leaguearray.push({
            label: element.tournament_name,
            value: element.id
          });
        });
        console.log(this.leaguearray, "leaguearray==============leaguedata===");
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }

  getteams() {
    let data = {
      leagues: this.leagues
    };
    var url =
      this.base_path_service.base_path + "api/user/shoutOut/?form_type=teams";
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        console.log(res, "res=================");
        this.teamsdata = res[0].json.data;
        console.log(this.teamsdata, "teamsdata==============leaguedata===");
      },
      err => {
        if (err.status == 404) {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: "",
            detail: "Network problem"
          });
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary: " ",
            detail: "some error, try again..."
          });
        }
      }
    );
  }
  selectall(event) {
    console.log(event, "evnent===");
    if (event == true) {
      this.teamselected = [];
      this.teamsdata.forEach(element => {
        this.teamselected.push(element.id);
      });
    } else {
      this.teamselected = [];
    }
    console.log(this.teamselected, "all select-================");
  }
  postshoutout() {
    if (
      this.leagues == [] ||
      this.leagues == undefined ||
      this.teamselected == [] ||
      this.teamselected == undefined ||
      this.shoutoutmsg == "" ||
      this.shoutoutmsg == undefined ||
      this.membertype == "" ||
      this.membertype == undefined
    ) {
      $("#err").show();
      this.errormsg = true;
      setTimeout(function() {
        $("#err").fadeOut();
      }, 3000);
    } else {
      this.loader = true;
      this.errormsg = false;
      let data = {
        leagues: this.leagues,
        teams: this.teamselected,
        shoutout: this.shoutoutmsg,
        member_type: this.membertype
      };

      console.log(data, "data to send");

      var url =
        this.base_path_service.base_path +
        "api/user/shoutOut/?form_type=shoutout";
      this.base_path_service.PostRequest(url, data).subscribe(
        res => {
          this.display = false;
          console.log(res, "res=================");
          this.msgs = [];
          this.msgs.push({
            severity: "info",
            detail: "shoutout sent successfully"
          });
          this.leagues = [];
          this.teamselected = [];
          this.shoutoutmsg = "";
          this.membertype = "manager";
          this.selectedValues = "";
          this.teamsdata = [];
          this.clubsdisplay = false;
          this.loader = false;
        },
        err => {
          this.loader = false;
          if (err.status == 404) {
            this.msgs = [];
            this.msgs.push({
              severity: "error",
              detail: "Network problem"
            });
          } else {
            this.msgs = [];
            this.msgs.push({
              severity: "error",
              detail: "some error, try again..."
            });
          }
        }
      );
    }
  }
}
