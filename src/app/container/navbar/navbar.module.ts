import { NgModule } from "@angular/core";
import { NavbarComponent } from "./navbar.component";
import { NavbarRouting } from "./navbar.routes";
import { DashboardComponent } from "./../dashboard/dashboard.component";
import { SharedModule } from "./../../shared/shared.module";
import { FollowersComponent } from "./../followers/followers.component";
import { FollowingsComponent } from "./../followings/followings.component";
import { NotificationComponent } from "./../notification/notification.component";
import { ShoutoutComponent } from "./../shoutout/shoutout.component";
import { PaymentComponent } from "./../payment/payment.component";
import { PaymentOptionComponent } from "./../payment-option/payment-option.component";
import { PackageComponent } from "./../package/package.component";
import { PaymentformComponent } from "./../paymentform/paymentform.component";
import { AddCardRedirectComponent } from "./../add-card-redirect/add-card-redirect.component";
import { GalleryuploadComponent } from '../galleryupload/galleryupload.component';

import { AuthGuard } from "./../../auth.gurd";
import { TranslateService } from "ng2-translate";
@NgModule({
  imports: [NavbarRouting, SharedModule.forRoot()],
  providers: [AuthGuard],
  declarations: [
    AddCardRedirectComponent,
    PaymentformComponent,
    PackageComponent,
    PaymentComponent,
    PaymentOptionComponent,
    ShoutoutComponent,
    NotificationComponent,
    NavbarComponent,
    DashboardComponent,
    FollowersComponent,
    FollowingsComponent,
    GalleryuploadComponent
  ]
})
export class NavbarModule {
  constructor() {}
}
