import { Component, OnInit } from '@angular/core';
import { TranslateService } from "ng2-translate";
import { Router, ActivatedRoute } from "@angular/router";
import { GlobalService } from "./../../GlobalService";
import { FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-payment-option',
  templateUrl: './payment-option.component.html',
  styleUrls: ['./payment-option.component.css']
})
export class PaymentOptionComponent implements OnInit {
  team_registration: boolean;
  loader: boolean;
  reservation: boolean;
  court_information: any;
  team_id: any;
  id: number;
  team: boolean;
  team_pay_info: any;
  selected_payment_option: any = 'minimum_fee';
  discount_money: any;
  msgs: any[] = [];
  payment_option: any;
  show_min_and_discount: boolean = false;
  show_cash: boolean = false;
  pay_min_fee: any;
  cash: any;
  discount: any;
  reservation_info: any;
  recurrent: any = 'false';
  check: any;

  constructor(private translate: TranslateService, public router: Router, public base_path_service: GlobalService, public route: ActivatedRoute, public _fb: FormBuilder) {

  }

  ngOnInit() {
    window.scrollTo(0, 10);
    this.route.queryParams.subscribe(param => {
      if (param['team']) {
        this.team_registration = true
        localStorage.removeItem('package_detail');
        localStorage.removeItem('field_info');
        this.team_pay_info = JSON.parse(localStorage.getItem('team_payment')).payment_info;
        this.payment_option = this.team_pay_info.payment_mode;
        for (let i = 0; i < this.payment_option.length; i++) {
          if (this.payment_option[i].abbreviation === 'online' || this.payment_option[i].abbreviation === 'card') {
            this.show_min_and_discount = true;
          }
          else if (this.payment_option[i].abbreviation === 'cash') {
            this.show_cash = true;
          }
        }
        this.team = true;
        let data: any = this.team_pay_info.amount - (this.team_pay_info.amount * this.team_pay_info.discount) / 100;
        this.discount_money = parseFloat(data).toFixed(2);
        this.pay_min_fee = parseFloat(this.team_pay_info.minimum_fee ? this.team_pay_info.minimum_fee : this.team_pay_info.amount).toFixed(2);
        this.route.params.subscribe(params => {
          this.id = parseInt(params['id']);
        });
        this.team_id = param['team_id'];
        this.cash = parseFloat(this.team_pay_info.amount).toFixed(2);
        this.discount = this.team_pay_info.discount;
      }
      else if (param['reserve']) {
        this.team_registration = false;
        if (param['option'] !== undefined) {
          this.selected_payment_option = param['option']
        }
        localStorage.removeItem('team_payment');
        localStorage.removeItem('package_detail');
        this.recurrent = param['recurrent'];
        console.log("recurent", this.recurrent);
        this.court_information = JSON.parse(localStorage.getItem('court_information'));
        this.reservation_info = JSON.parse(localStorage.getItem('field_info'));
        this.payment_option = this.court_information['payment_method'];
        for (let i = 0; i < this.payment_option.length; i++) {
          if (this.payment_option[i].abbreviation === 'online' || this.payment_option[i].abbreviation === 'card') {
            this.show_min_and_discount = true;
          }
          else if (this.payment_option[i].abbreviation === 'cash') {
            this.show_cash = true;
          }
        }
        console.log(this.show_cash, "cash", this.show_min_and_discount, "onlineeeeeeeeeeeee")
        this.reservation = true;
        this.discount = this.court_information.discount;
        this.cash = parseFloat(this.reservation_info['price']).toFixed(2);
        let data: any = this.reservation_info['price'] - ((this.reservation_info['price'] * this.court_information.discount) / 100);
        this.discount_money = parseFloat(data).toFixed(2);
        this.pay_min_fee = parseFloat(((this.court_information.minimum_fee && (this.court_information.minimum_fee <= this.reservation_info['price'])) ? this.court_information.minimum_fee : this.reservation_info['price'])).toFixed(2);
        console.log("minnnnnnnnnnnnnnn", this.pay_min_fee)
        this.route.params.subscribe(params => {
          this.id = parseInt(params['id']);
        });
      }
    })
  }

  back() {
    if (this.team) {
      this.router.navigateByUrl('dashboard/tournaments/tournament-profile/' + this.id);
    }

    else if (this.reservation) {
      this.router.navigateByUrl('dashboard/ground/ground-profile/' + this.id + '/schedule');

    }
  }

  pay() {
    if (this.team) {
      this.loader = true;
      console.log(this.selected_payment_option, "option=>>>>>>>>>>>>>...")
      if (this.selected_payment_option === 'cash') {
        let url = this.base_path_service.base_path_api() + 'tournament/team_pay_now/';
        let data = {
          tournament_id: this.id,
          team_id: this.team_id,
          request_type: 'cash',
          registration_id: this.team_pay_info.team_registration_id
        }
        this.base_path_service.PostRequest(url, data)
          .subscribe(res => {
            this.loader = false;
            this.router.navigateByUrl('dashboard/tournaments/tournament-profile/' + this.id + '?status=3');
          }, err => {
            this.loader = false;
          })
      }
      else if (this.selected_payment_option === 'discount' || this.selected_payment_option === 'minimum_fee') {
        console.log("okkkkkkkkkkkkkkk")
        this.loader = false;
        this.router.navigateByUrl('dashboard/payment/' + this.id + '?team_id=' + this.team_id + "&team=" + true + "&option=" + this.selected_payment_option);
      }

    }
    else if (this.reservation) {
      this.loader = true;
      console.log(this.selected_payment_option, "option=>>>>>>>>>>>>>...")
      if (this.selected_payment_option === 'cash') {
        this.reservation_info.paid_with = 'cash';
        let url = this.base_path_service.base_path_api() + 'scorecard/ground_request/?format=json';
        this.base_path_service.PostRequest(url, this.reservation_info)
          .subscribe(res => {
            this.loader = false;
            let data_obj = res[0].json;
            let reserve = {
              "ground_id": this.id,
              "court_id": parseInt(this.reservation_info.ground),
              "reservation_id": data_obj.reservation_id,
              "request_type": 'cash'
            }
            let reservation_url = this.base_path_service.base_path_api() + 'ground/field_reserve_pay/';
            this.base_path_service.PostRequest(reservation_url, reserve)
              .subscribe(res => {
                this.loader = false;
                this.router.navigateByUrl('dashboard/ground/ground-profile/' + this.id + '?status=3');
              })
          })
      }
      else if (this.selected_payment_option === 'discount' || this.selected_payment_option === 'minimum_fee') {
        console.log("okkkkkkkkkkkkkkk")
        this.loader = false;
        this.router.navigateByUrl('dashboard/payment/' + this.id + "?reserve=" + true + "&option=" + this.selected_payment_option + "&recurrent=" + this.recurrent);
      }
    }

  }


}
