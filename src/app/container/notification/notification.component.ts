import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { GlobalService } from './../../GlobalService';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
 notification: Array<any>;
  basePath: string = '';
  page: number = 1;
  total_page: number;
  msgs: Message[] = [];
  showSeeMore: boolean = false;


  constructor(public router: Router, public base_path_service: GlobalService) {

  }

  ngOnInit() {
    this.basePath = this.base_path_service.image_url;
    this.notify();
  }

  notify() {
    let url = this.base_path_service.base_path + "api/user/notification/?page=" + this.page + '&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        console.log(res);
        this.notification = res[0].json.data;
        this.total_page = res[0].json.total_pages;
        if (this.total_page > this.page) {
          this.showSeeMore = true;
        }
      }, err => {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores!' });
      })
  }

  seeMore() {
    this.page = this.page + 1;
    this.notify();
  }

  routing() {
    this.router.navigateByUrl('/dashboard')
  }
  routingOfLink(link: string) {
    this.router.navigateByUrl(link)
    
    // this.router.navigateByUrl('/dashboard/ground/ground-profile/337/request');
  }
}


