import { MessagesComponent } from './../messages/messages.component';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InboxComponent } from './inbox/inbox.component';
import { SentComponent } from './sent/sent.component';

export const messagesRoutes = [
    {
        path: '', component: MessagesComponent,
        children: [
            { path: '', component: InboxComponent },
            { path: 'inbox', component: InboxComponent },
            { path: 'sent', component: SentComponent },
        ]
    }
];

export const messagesRouting: ModuleWithProviders = RouterModule.forChild(messagesRoutes);