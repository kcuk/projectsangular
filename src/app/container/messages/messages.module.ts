import { NgModule } from '@angular/core';
import {MessagesComponent} from './messages.component'
import {messagesRouting} from './messages.routes';
import {SharedModule} from './../../shared/shared.module';
import { SentComponent } from './sent/sent.component';
import { InboxComponent } from './inbox/inbox.component';

@NgModule({
  imports: [
    messagesRouting,
    SharedModule.forRoot()
  ],
  declarations: [MessagesComponent, SentComponent, InboxComponent]
  
})
export class MessagesModule { }
