import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { GlobalService } from './../../../GlobalService';
import {Http } from '@angular/http';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-sent',
  templateUrl: './sent.component.html',
  styleUrls: ['./sent.component.css']
})
export class SentComponent implements OnInit {

  public page: number = 1;
  public loader: boolean = false;
  public msgs_detail: boolean = false;
  public list: any[];
  public msgs: Message[];
  public user_msgs: any;
  public profile_pic: string;
  public basePath: string;
  public Sent_form: FormGroup;
  public message_id: string[] = [];
  public reply_msgs: string;
  public delete_msgs: number[] = [];
  public player_id: number;
  public profilepic: string;
  public total_page: number;

  constructor(public fb: FormBuilder, public router: Router, public http: Http, public base_path_service: GlobalService, public route: ActivatedRoute) {
    this.Sent_form = fb.group({
      replymsgs: new FormControl('', Validators.compose([Validators.required]))
    });
  }

  ngOnInit() {
    let sub = this.route.parent.params.subscribe(params => {
      this.player_id = +params['id'];
    });
    this.basePath = this.base_path_service.image_url;
    this.get_message();
  }

  get_user_message(id) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'user/message/' + id + '/?form_type=sentbox';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.msgs_detail = true;
        this.user_msgs = res[0].json;
        this.profile_pic = res[0].json.from_player.profile_pic;
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '', detail: 'algunos de error!' });
      });

    let url1 = this.base_path_service.base_path_api() + 'user/player/?form_type=profile&player=' + this.player_id + '&format=json';
    this.base_path_service.GetRequest(url1)
      .subscribe(res => {
        this.profilepic = res[0].json.profile_pic;
      });
  }

  previous() {
    if (this.page > 1) {
      this.page = this.page - 1;
      this.get_message();
    }
  }
  next() {
    if (this.page < this.total_page) {
      this.page = this.page + 1;
      this.get_message();
    }
  }

  get_message() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'user/message/?page=' + this.page + '&form_type=sent';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.msgs_detail = false;
        this.loader = false;
        this.list = res[0].json.data;
        this.total_page = res[0].json.total_pages;
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '', detail: 'algunos de error!' });
      });
  }

  delete_msg(id) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'user/message/';
    let data = {
      "form_type": "delete",
      "option": "sentbox",
      "delete_list": [id]
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.get_message();
        this.msgs = [];
        this.msgs.push({ severity: 'info', summary: '', detail: '¡Borrado exitosamente!' });
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '', detail: 'algunos de error!' });
      });
  }

  reply(id) {
    this.loader = true;
    let data = {
      "form_type": "reply",
      "msg_id": id,
      "message": this.reply_msgs
    }
    let url = this.base_path_service.base_path_api() + 'user/message/';
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.reply_msgs = '';
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'info', summary: '', detail: 'Respondió con éxito!' });
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '', detail: 'algunos de error!' });
      });
  }

  delete() {
    this.loader = true;
    for (let i = 0; i < this.message_id.length; i++) {
      this.delete_msgs[i] = parseInt(this.message_id[i]);
    }
    let url = this.base_path_service.base_path_api() + 'user/message/';
    let data = {
      "form_type": "delete",
      "option": "sentbox",
      "delete_list": this.delete_msgs
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.get_message();
        this.msgs = [];
        this.msgs.push({ severity: 'info', summary: '', detail: '¡Borrado exitosamente!' });
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '', detail: 'algunos de error!' });
      });
  }

}