import { Component, OnInit } from '@angular/core';
import {Message} from 'primeng/primeng' 
import {Router,ActivatedRoute,RouterState} from '@angular/router';
import {Http} from '@angular/http';
import { GlobalService } from './../../GlobalService';
import {LoaderComponent} from './../loader/loader.component';
import { TranslateService } from "ng2-translate";
declare const $: any;

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent extends LoaderComponent implements OnInit {
  public loader: boolean = false;
  public cover_pic: string;
  public profile_pic: string;
  public player_detail: any;
  public admin: boolean;
  public msgs: Message[];
  public basepath: string;
  public player_id: number;
  public paramsSub: any;
  public send: boolean = false;
  public inbox: boolean = false;

  constructor( private translate: TranslateService,public router: Router, public http: Http, public base_path_service: GlobalService, public route: ActivatedRoute) {
super();
  if (localStorage.getItem('language') != null) {
            translate.use(localStorage.getItem('language'))
        }

  }

  ngOnInit() {
    let sub = this.route.params.subscribe(params => {
      this.player_id = +params['id'];
    });
    this.basepath = this.base_path_service.image_url;
    this.get_data();
  }

  public get_data() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'user/player/?form_type=profile&player=' + this.player_id + '&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.player_detail = res[0].json;
        this.profile_pic = res[0].json.profile_pic;
        this.cover_pic = res[0].json.cover_pic;
        this.admin = res[0].json.admin;
        this.loader = false;
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos de errores!' });
      });
  }

  public back_to_profile() {
    this.router.navigateByUrl('/dashboard/player/player-profile/' + this.player_id)
  }
}