import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunitychatComponent } from './communitychat.component';

describe('CommunitychatComponent', () => {
  let component: CommunitychatComponent;
  let fixture: ComponentFixture<CommunitychatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunitychatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunitychatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
