import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "./../../shared/shared.module";
import { Routes, RouterModule } from "@angular/router";
import { CommunitychatComponent } from "./communitychat.component";
import { CheckboxModule } from "primeng/primeng";
import { RadioButtonModule } from "primeng/primeng";


export const community_routes: Routes = [
  { path: "", component: CommunitychatComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(community_routes),
    SharedModule.forRoot(),
    CheckboxModule,
    RadioButtonModule
  ],
  declarations: [
    CommunitychatComponent,
  ]
})
export class CommunitychatModule {}
