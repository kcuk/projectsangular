import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-communitychat",
  templateUrl: "./communitychat.component.html",
  styleUrls: ["./communitychat.component.css"]
})
export class CommunitychatComponent implements OnInit {
  rightnav: boolean = false;
  collaps: boolean = false;
  showallcon: boolean = false;
  colindex: any;
  leagues: any=[];
  constructor() {
    this.leagues = [
      { name: "League A" },
      { name: "league B" },
      { name: "league C" }
    ];
  }

  ngOnInit() {}
  moredetails() {
    this.rightnav = !this.rightnav;
  }
  open(index) {
    console.log(index, "index");
    this.colindex = index;
    this.showallcon = true;
    this.collaps = true;
  }
  close() {
    this.collaps = false;
    console.log(this.collaps, "coloa[se");
    this.showallcon = false;
  }
}
