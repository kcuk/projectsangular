import { Component, OnInit } from '@angular/core';
import { LoginService } from './../../services/login/login.service';
import { GlobalService } from './../../GlobalService';
import { Router, ActivatedRoute } from '@angular/router'
import { Message } from 'primeng/primeng'
class userDetails {
  "new_password": "";
  "confirm_password": "";
}

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  userDetailsIns;

  public paramsSub: any;
  public user: string;
  public key: string;
  public aToken: string;
  public uid: number;
  public newPass: string;
  resetBlock: boolean = false;
  apiHitting: boolean = false;
  btn: boolean = false;
  userKey;
  msgs: Message[] = [];
  public isVerified: boolean = false;
  public isError: boolean = false;

  constructor(public login_service: LoginService, public router: Router, public _route: ActivatedRoute, public base_path_service: GlobalService) {
    this.userDetailsIns = new userDetails();
    this.userKey = {
      "user": "",
      "key": ""
    }
  }

  ngOnInit() {
    this.paramsSub = this._route.queryParams.subscribe(params => {
      this.user = params['user'];
      this.key = params['key']; 
      this.verifyKey();
    });
  }

  verifyKey() {
    this.userKey.user = this.user;
    this.userKey.key = this.key;

    let url: string = this.login_service.baseUrl + '/peloteando/reset_password/';
    this.login_service.forgetPasswordRequest(this.userKey, url)
      .subscribe(
      res => {
        this.apiHitting = false;
        this.isVerified = true;
        localStorage.setItem("forget", JSON.stringify(res));
        this.aToken = res.token.access_token;
        this.uid = res.info.account_id;
        this.resetBlock = true;
      },
      err => {
        this.apiHitting = false;
        this.isError = true;
        setTimeout(this.redirect(), 5000);
        if (err.status == 401) {
          this.msgs = [];
          this.msgs.push({ severity: "error", summary: "Something is not good", detail: "Mail has been expired or not valid" })
        }
        console.log("error");
      })
  }

  password() {
    if (this.userDetailsIns.new_password.length >= 6 && this.userDetailsIns.confirm_password.length >= 6) {
      this.btn = true;
    } else {
      this.btn = false;
    }

  }

  resetPassword() {
    if (this.userDetailsIns.confirm_password.length >= 6 && this.userDetailsIns.new_password.length >= 6) {
      this.btn = true;
    } else {
      this.btn = false;
    }
  }

  setPassword(data) {
    this.apiHitting = true;
    let passwordVerified = this.verifyPassword();
    if (passwordVerified) {
      let a: any = {
        "user_id": this.uid,
        "new_password": data.new_password
      };
      let url = this.base_path_service.base_path_api() + "peloteando/set_password/";

      this.login_service.setPasswordRequest(a, url, this.aToken)
        .subscribe(
        res => {
          console.log(res.status);
          if (localStorage.getItem("forget") != null)
            localStorage.removeItem("forget");
          if (res.status == 205) {
            this.msgs = [];
            this.msgs.push({ severity: 'info', summary: 'Password has been reset!!', detail: '' });
            this.router.navigate(['/'])
          }
        },
        err => {
          console.log("error")
        })
      this.apiHitting = false;
    }
  }
  verifyPassword() {
    if (this.userDetailsIns.new_password === this.userDetailsIns.confirm_password)
      return true;
    else {
      this.msgs = []
      this.msgs.push({ severity: 'error', summary: 'pasword nueva y Confirmar contraseña no son los mismos', detail: '' });
      this.userDetailsIns.new_password = '';
      this.userDetailsIns.confirm_password = '';
      this.apiHitting = false;
      return false;
    }
  }
  redirect() {
    this.router.navigate(['/']);
  }

}
