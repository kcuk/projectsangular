import { Component, OnInit } from '@angular/core';
import { Router ,NavigationEnd} from '@angular/router'
import { GlobalService } from './../../GlobalService';
class userDetails {
  "new_password": "";
  "confirm_password": "";
}

@Component({
  moduleId: module.id,
  selector: 'pricing_info',
  templateUrl: 'pricing_info.component.html',
  styleUrls: ['pricing_info.component.css'],
  providers: []
})
export class PricingInfo implements OnInit {
  public tabChange:string="first";
  constructor(public router:Router) {

  }
  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }

}