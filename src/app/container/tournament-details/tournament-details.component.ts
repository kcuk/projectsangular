import { Component, OnInit, HostListener, Inject } from "@angular/core";
import { Message } from "primeng/primeng";
import { GlobalService } from "./../../GlobalService";
import { DOCUMENT } from "@angular/platform-browser";
import { TranslateService } from "ng2-translate";
declare const $;
@Component({
  selector: "app-tournament-details",
  templateUrl: "./tournament-details.component.html",
  styleUrls: ["./tournament-details.component.css"]
})
export class TournamentDetailsComponent implements OnInit {
  public show: boolean = false;
  public obj: Array<any> = [];
  public showBtn: boolean = false;
  public cards: boolean = false;
  public len: number = 0;
  public disable1: boolean = false;
  public format: Array<string> = [];
  public cost: Array<any> = [];
  public formatDefault: boolean = true;
  public costDefault: boolean = true;
  public costObj: Array<any> = [];
  public msgs: Message[] = [];
  public myTour: boolean = false;
  public counter: number = 1;
  public basePath: string = this.base_path_service.image_url;
  public loader: boolean = false;
  public level: Array<any> = [];
  public cities: Array<any> = [];
  public selectedCityId: any = null;
  public category: Array<any> = [];
  public status: Array<any> = ["register", "play"];
  public isdata: boolean = false;
  public isError: boolean = false;
  public date: Date;
  public dateString: any;
  public nameSelected: string = "";
  public cityPlaceholder: string = "Ciudad";
  public page: number = 1;
  public random: number = 0;
  public from_filter: string = "";
  comdata: any;
  communityapiid: any;
  userexist: boolean = false;

  constructor(
    private translate: TranslateService,
    @Inject(DOCUMENT) public document: Document,
    public base_path_service: GlobalService
  ) {
    localStorage.removeItem("court_id");
    localStorage.removeItem("courts");
    localStorage.removeItem("court_information");
    localStorage.removeItem("field_info");
    this.isdata = false;
    if (localStorage.getItem("language") != null) {
      translate.use(localStorage.getItem("language"));
    }
    if (localStorage.getItem("user_info")) {
      this.userexist = true;
    } else {
      this.userexist = false;
    }
  }
  ngOnInit() {
    if (localStorage.getItem("community_info")) {
      this.comdata = JSON.parse(localStorage.getItem("community_info"));
      this.communityapiid = this.comdata.community_id;
    } else {
      this.communityapiid = localStorage.getItem("communityid");
      console.log(this.communityapiid, "communityapiid===========");
    }
    this.mob();
    this.getTournament(this.page);
  }

  @HostListener("window:scroll", ["$event"])
  track(event: any) {
    let number = this.document.body.scrollHeight;
    let totalNumber = window.pageYOffset + window.innerHeight;
    if (number == Math.ceil(totalNumber)) {
      this.from_filter = "";
      this.page = this.page + 1;
      if (this.page <= this.len) {
        if (this.myTour) {
          this.myTournament(this.page);
        } else {
          this.getTournament(this.page);
        }
      }
    }
  }
  mob() {
    let mq = window.matchMedia("screen and (min-width:640px)");
    if (mq.matches) {
      this.show = false;
    } else {
      this.show = true;
    }
  }

  reset() {
    if (!this.myTour) {
      this.obj = [];
    }
  }

  getTournament(page) {
    this.page = page;
    this.msgs = [];
    this.isdata = false;
    this.isError = false;
    this.loader = true;
    this.costObj = this.getObjectData(this.cost);
    let filterObj = {
      category: this.category,
      tournament_name: this.nameSelected,
      city: this.selectedCityId,
      page: page,
      format: this.format,
      cost: this.costObj,
      status: this.status,
      tournament_type: this.category,
      tournament_division: this.level,
      rand: this.random,
      latitude: localStorage.getItem("lat"),
      longitude: localStorage.getItem("long"),
      distance: parseInt(localStorage.getItem("distance")),
      community_id: this.communityapiid
    };

    let url =
      this.base_path_service.base_path + "api/tournament/filter/?format=json";

    if (this.userexist) {
      this.base_path_service.PostRequest(url, filterObj).subscribe(
        res => {
          this.mob();
          this.cards = true;
          if (this.from_filter != "") {
            this.obj = [];
            this.obj.push(res[0].json.data);
          } else {
            this.obj.push(res[0].json.data);
          }

          if (res[0].json.data.length < 1) {
            this.isdata = true;
          }
          this.len = res[0].json.total_pages;
          this.random = res[0].json.rand;
          this.loader = false;
        },
        err => {
          this.isError = true;
          this.loader = false;
          this.msgs.push({
            severity: "error",
            summary: "Error",
            detail: "algunos errores"
          });
        }
      );
    } else {
      this.base_path_service.PostRequestUnauthorised(url, filterObj).subscribe(
        res => {
          this.mob();
          this.cards = true;
          if (this.from_filter != "") {
            this.obj = [];
            this.obj.push(res[0].json.data);
          } else {
            this.obj.push(res[0].json.data);
          }

          if (res[0].json.data.length < 1) {
            this.isdata = true;
          }
          this.len = res[0].json.total_pages;
          this.random = res[0].json.rand;
          this.loader = false;
        },
        err => {
          this.isError = true;
          this.loader = false;
          this.msgs.push({
            severity: "error",
            summary: "Error",
            detail: "algunos errores"
          });
        }
      );
    }
  }

  getObjectData(b: any): any {
    let a: Array<any> = [];
    for (let x = 0; x < b.length; x++) {
      let str: string = b[x];
      let costArray = str.split("-");
      a[x] = { min: costArray[0], max: costArray[1] };
    }
    return a;
  }

  fun() {
    this.cards = false;
  }

  modalityFilter(x: boolean) {
    if (!this.myTour) {
      if (x) {
        this.format = [];
        this.formatDefault = true;
      } else {
        if (this.format.length == 1) {
          this.formatDefault = false;
        } else if (this.format.length == 6) {
          this.format = [];
          this.formatDefault = true;
        }
      }
      this.getTournament(1);
    }
  }

  Filter() {
    if (!this.myTour) {
      this.getTournament(1);
    }
  }

  myTournament(x) {
    this.isError = false;
    this.loader = true;
    this.isdata = false;
    this.msgs = [];
    if (this.myTour) {
      this.disable1 = true;

      this.base_path_service
        .GetRequest(
          this.base_path_service.base_path +
            "api/tournament/tournamentDetail/?page=" +
            x +
            "&format=json"
        )

        .subscribe(
          res => {
            this.obj.push(res[0].json.data);
            this.len = res[0].json.total_pages;
            this.mob();
            this.cards = true;
            this.loader = false;
            if (res[0].json.data.length < 1) this.isdata = true;
          },
          err => {
            this.isError = true;
            this.loader = false;
            this.msgs.push({
              severity: "error",
              summary: err.json(),
              detail: "algunos errores"
            });
            this.base_path_service.print("some error");
          }
        );
    } else {
      this.disable1 = false;
      this.getTournament(1);
    }
  }

  follow(tour) {
    this.loader = true;
    this.msgs = [];
    let url =
      this.base_path_service.base_path +
      "api/user/followTournament/?format=json";
    let data = { tournament: tour.id };
    this.base_path_service.PostRequest(url, data).subscribe(
      res => {
        this.loader = false;
        this.msgs.push({
          severity: "info",
          summary: "",
          detail: "seguido con éxito"
        });
        tour.follow = !tour.follow;
      },
      err => {
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "No podía seguir por favor, inténtelo de nuevo ..."
        });
        this.loader = false;
      }
    );
  }

  unFollow(tour) {
    this.msgs = [];
    this.loader = true;
    let url =
      this.base_path_service.base_path +
      "api/user/followTournament/" +
      tour.id +
      "/?format=json";
    this.base_path_service.DeleteRequest(url).subscribe(
      res => {
        this.loader = false;
        this.msgs.push({
          severity: "info",
          summary: "",
          detail: "Unfollowed correctamente!"
        });
        tour.follow = !tour.follow;
      },
      err => {
        this.msgs.push({
          severity: "error",
          summary: "",
          detail: "No podría dejar de seguir por favor, inténtelo de nuevo ..."
        });
        this.loader = false;
      }
    );
  }

  searchCity(event) {
    this.cities = [];
    let query = event.query;
    if (!query) {
      this.selectedCityId = "";
      this.getTournament(1);
    } else {
      let url =
        this.base_path_service.base_path +
        "api/team/location/?country=1&city=" +
        query.trim() +
        "&format=json";
      this.base_path_service.GetRequest(url).subscribe(res => {
        this.loader = false;
        this.cities = res[0].json;
      });
    }
  }
  cityFilter(event) {
    this.obj = [];
    this.selectedCityId = event.location;
    this.getTournament(1);
  }

  nameFilter(event) {
    this.from_filter = "filter";
    this.obj = [];
    this.getTournament(1);
  }
}
