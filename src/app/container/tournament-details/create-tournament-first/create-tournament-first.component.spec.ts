import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTournamentFirstComponent } from './create-tournament-first.component';

describe('CreateTournamentFirstComponent', () => {
  let component: CreateTournamentFirstComponent;
  let fixture: ComponentFixture<CreateTournamentFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTournamentFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTournamentFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
