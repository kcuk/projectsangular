import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalService } from './../../../GlobalService';
import { TranslateService } from "ng2-translate";
import { countryCode } from "../../registration/onboading2/country_code"
import {  Routes, ActivatedRoute } from '@angular/router';
import {  Message } from 'primeng/primeng';
import { Http } from '@angular/http';

declare var google;

@Component({
  selector: 'app-create-tournament-first',
  templateUrl: './create-tournament-first.component.html',
  styleUrls: ['./create-tournament-first.component.css']
})
export class CreateTournamentFirstComponent implements OnInit {

  long: any;
  lat: any;
  city_name: any;
  country_name: any;
  autocompleteItems: any[];
  GoogleAutocomplete: any;
  checkCity: boolean=true;
  isMobValid: boolean;
  public filteredCity;
  public loader: boolean = false;
  public tournamentLebelList: SelectItem[];
  public city: SelectItem[];
  public town: SelectItem[];
  public genderList: SelectItem[];
  public gender_lebel;
  public tournament_lebel;
  public cityValue: any;
  public createTournamentFieldsIns;
  public createTournamentForm: FormGroup;
  public cityValidation = false;
  public modalityValidation = false;
  public genderValidation = false;
  public reg_start_date: any;
  public reg_end_date: any;
  public citySelect: boolean = false;
  public tournament_start_date: any;
  public msgs: Array<any> = [];
  public tournament_end_date: any;
  public regDateValidation: boolean = false;
  public tourDateValidation: boolean = false;
  public minDate: Date;
  public lang: any;
  public country_code:any;
  // code: any = [
  //   { value: '+1', label: '+1' },
  //   { value: '+502', label: '+502' },

  //   { value: '+593', label: '+593' },
  // ];
  // length: any = 9;
  userObj = new User();
  codes: SelectItem[] = [];

  mask: any = '999999999999999'

  en = {
    dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
  };
  es = {
    dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
    dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
    monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
    monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
  }
  langauge: any
  constructor(public zone: NgZone,

    private translate: TranslateService, public base_path_service: GlobalService, public router: Router, public fb: FormBuilder) {
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();

    if (localStorage.getItem('language') == 'es') {
      this.langauge = this.es;
    }
    else {
      this.langauge = this.en
    }

    this.country_code = localStorage.getItem("country_dial_code");
    countryCode.map((val) => {
      this.codes.push({
        label: val.name + " " + "(" + val.dial_code + ")",
        value: val.dial_code
      })
    })
  }

  ngOnInit() {
    this.minDate = new Date();
    this.createTournamentFieldsIns = new CreateTournamentFields();
    // this.cityList();
    this.setGenderLavel();
    this.setTournamentLevel();
    this.formValidation();
  }

  setGenderLavel() {
    this.gender_lebel = 'Seleccionar tipo';
    this.genderList = [];
    this.genderList.push({ label: localStorage.getItem('language') == "en" ? 'Select' : 'Seleccionar', value: null });
    this.genderList.push({ label: localStorage.getItem('language') == "en" ? 'Male' : 'Masculino', value: 'Masculino' });
    this.genderList.push({ label: localStorage.getItem('language') == "en" ? 'Female' : 'Femenino', value: 'Femenino' });
    this.genderList.push({ label: localStorage.getItem('language') == "en" ? 'Mix' : 'Mixto', value: 'Mixto' });
  }


  setTournamentLevel() {
    this.tournament_lebel = 'Seleccionar';
    this.tournamentLebelList = [];
    this.tournamentLebelList.push({ label: localStorage.getItem('language') == "en" ? 'Select' : 'Seleccionar', value: null });
    this.tournamentLebelList.push({ label: '5x5', value: 'V5' });
    this.tournamentLebelList.push({ label: '6x6', value: 'V6' });
    this.tournamentLebelList.push({ label: '7x7', value: 'V7' });
    this.tournamentLebelList.push({ label: '8x8', value: 'V8' });
    this.tournamentLebelList.push({ label: '9x9', value: 'V9' });
    this.tournamentLebelList.push({ label: '11x11', value: 'V11' });
  }

  formValidation() {
    this.lat = localStorage.getItem('lat');
    this.long = localStorage.getItem('long');
    let city = {
      description:localStorage.getItem('city')
    }
    this.country_name =localStorage.getItem('country_name')
    this.city_name = localStorage.getItem('city');
    this.createTournamentForm = new FormGroup({
      tournament_name: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(50)])),
      bio: new FormControl('', Validators.compose([Validators.maxLength(200)])),
      // city: new FormControl('', Validators.compose([Validators.required])),
      tournament_level: new FormControl('', Validators.compose([Validators.required])),
      tournament_type: new FormControl('', Validators.compose([Validators.required])),
      contact: new FormControl('', Validators.compose([Validators.required])),
      country_code: new FormControl(this.country_code, Validators.compose([Validators.required])),
      registration_start_date: new FormControl('', Validators.compose([Validators.required])),
      registration_end_date: new FormControl('', Validators.compose([Validators.required])),
      start_date: new FormControl('', Validators.compose([Validators.required])),
      end_date: new FormControl('', Validators.compose([Validators.required])),
      no_of_admins: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      selectCity:new FormControl(city)
    })
  }


  // filterCity(event) {
  //   let query = event.query;
  //   let country = 1;
  //   if (country != undefined) {
  //     var url = this.base_path_service.base_path_api() + "team/location/?&city=" + query + "&format=json";
  //     this.base_path_service.GetRequest(url)
  //       .subscribe(
  //         res => {
  //           this.filteredCity = res[0].json;
  //           if (this.filteredCity.length == 0) {
  //             this.cityValidation = true;
  //             this.citySelect = false;
  //           } else {
  //             this.cityValidation = false;
  //             this.citySelect = false;
  //           }
  //         },
  //         err => {
  //           console.log("error")
  //         });
  //   }
  //   else {

  //   }
  // }

  // getCity(event) {
  //   this.createTournamentFieldsIns.city = event.id;

  // }


  // cityList() {
  //   let url = this.base_path_service.base_path_api() + 'team/location/?form_type=city' + "&format=json";
  //   this.base_path_service.GetRequest(url)
  //     .subscribe(res => {
  //       this.city = [];
  //       this.city.push({ label: 'Select CIty', value: null });
  //       let city = res[0].json;
  //       for (var index = 0; index < res[0].json.length; index++) {
  //         this.city.push({ label: city[index].location, value: city[index].id });
  //       }
  //     })
  // }

  formatDate(event) {
    var validDate;
    let month = parseInt(event.getMonth());
    month = month + 1
    if (event.getMonth() > 8) {
      if (event.getDate() > 9) {
        validDate = event.getFullYear() + "-" + month + "-" + event.getDate();
      } else {
        validDate = event.getFullYear() + "-" + month + "-" + '0' + event.getDate();
      }

    } else {
      if (event.getDate() > 9) {
        validDate = event.getFullYear() + "-" + '0' + month + "-" + event.getDate();
      } else {
        validDate = event.getFullYear() + "-" + '0' + month + "-" + '0' + event.getDate();
      }
    }
    return validDate;
  }

  firstFormData(formData: any) {
    console.log("form =>>>>>>>>>>>>>>>",this.createTournamentForm)
    if (this.createTournamentForm.valid && this.checkCity) {
      // if (formData.city != '') {
        formData.page = 1;
        let start_time_reg = this.formatDate(formData.registration_start_date)
        formData.registration_start_date = this.formatDate(formData.registration_start_date);
        formData.registration_end_date = this.formatDate(formData.registration_end_date);
        formData.start_date = this.formatDate(formData.start_date)
        formData.end_date = this.formatDate(formData.end_date);
        // formData.city = formData.city.id;
        formData.city_name = this.city_name;
        formData.country_name = this.country_name;
        formData.latitude = this.lat;
        formData.longitude = this.long;

        this.loader = true;
        let url = this.base_path_service.base_path_api() + 'tournament/tournamentDetail/?format=json';
        this.base_path_service.PostRequest(url, formData)
          .subscribe(res => {
            this.loader = false;
            let tournamentInfo = res[0].json;
            localStorage.setItem("tournament_name", tournamentInfo.tournament_name);
            this.createTournamentFieldsIns.tournament_id = tournamentInfo.tournament_id;
            this.router.navigateByUrl('dashboard/tournaments/create-tournament/' + tournamentInfo.tournament_id);

          },
            err => {
              this.loader = false;
              this.base_path_service.print(err);
              this.msgs.push({ severity: 'error', detail: err.json() })
            })
      // } else {
      //   if (formData == '') {
      //     this.citySelect = true;
      //   }
      // }
    } else {
      this.msgs = []
      this.msgs.push({ severity: 'error', detail: 'Ingresa todos los campos obligatorios' })
    }
  }

  regStartDate(event) {
    let newDate = new Date(event).toISOString();
    this.reg_start_date = newDate;
    if (typeof this.reg_end_date !== "undefined") {
      if (this.reg_start_date <= this.reg_end_date) {
        this.regDateValidation = false;
      } else {
        this.regDateValidation = true;
      }
    }
    if (typeof this.reg_end_date !== "undefined" && typeof this.tournament_start_date !== "undefined" && typeof this.tournament_end_date !== "undefined") {
      if (this.reg_start_date <= this.reg_end_date && this.reg_start_date <= this.tournament_start_date && this.reg_start_date <= this.tournament_end_date) {
        this.regDateValidation = false;
      } else {
        this.regDateValidation = true;
      }
    }
  }
  regEndDate(event) {
    let newDate = new Date(event).toISOString();
    this.reg_end_date = newDate;
    if (typeof this.reg_start_date !== "undefined") {
      if (this.reg_start_date <= this.reg_end_date) {
        this.regDateValidation = false;
      } else {
        this.regDateValidation = true;
      }
    }
    if (typeof this.tournament_start_date !== "undefined" && typeof this.tournament_end_date !== "undefined" && typeof this.reg_start_date !== "undefined") {
      if (this.reg_end_date <= this.tournament_start_date && this.reg_end_date <= this.tournament_end_date) {
        this.tourDateValidation = false;
      } else {
        this.tourDateValidation = true;
      }
    }
  }
  tournamentStartDate(event) {

    let newDate = new Date(event).toISOString();
    this.tournament_start_date = newDate;
    if (typeof this.tournament_end_date !== "undefined") {
      if (this.tournament_start_date <= this.tournament_end_date) {
        this.tourDateValidation = false;
      } else {
        this.tourDateValidation = true;
      }
    }
    if (this.tournament_start_date >= this.reg_start_date && this.tournament_start_date >= this.reg_end_date) {
      this.regDateValidation = false;
    } else {
      this.regDateValidation = true;
    }
  }


  tournamentEndtDate(event) {
    let newDate = new Date(event).toISOString();
    this.tournament_end_date = newDate;
    if (typeof this.tournament_start_date !== "undefined" && typeof this.reg_start_date !== "undefined" && typeof this.reg_end_date !== "undefined") {
      if (this.tournament_end_date >= this.reg_end_date && this.tournament_end_date >= this.tournament_start_date && this.tournament_end_date >= this.reg_start_date) {
        this.tourDateValidation = false;
      } else {
        this.tourDateValidation = true;
      }
    }
  }


  public cancelButton() {
    this.router.navigateByUrl('dashboard/tournaments');
  }


  //   validMobile(event) {
  //     console.log("event=>>>>>>>>>>>>>>>>>>>>>>>", event);
  //     this.createTournamentForm.patchValue({ 'contact': "" })
  //     if (event.value == '+1') {
  //       this.length = 10;
  //       this.mask = '9999999999';
  //     }
  //     else if (event.value == '+502') {
  //       this.length = 8;
  //       this.mask = '99999999';
  //     }
  //     else {
  //       this.length = 9;
  //       this.mask = '999999999';
  //     }
  //   }

  //   mobileVerify(event) {
  //     if (event.target.value.toString().includes('_')) {
  //       this.isMobValid = true;
  //     }
  //     else
  //       this.isMobValid = false;
  //   }

  search(event) {
    this.checkCity = false;
    console.log("valussssssssss", event)
    this.GoogleAutocomplete.getPlacePredictions({ input: event.query, types: ['(regions)'] },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
          console.log("list", this.autocompleteItems)
        });
      });
  }

  setLoaction(item) {
    console.log("item=>>>>>>>>>>>>>>>>", item);
    this.checkCity = true;
    this.country_name = item.terms.pop().value;
    this.city_name = item.terms[0].value;
    this.geoCode((item.description));

  }

  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.lat = results[0].geometry.location.lat();
      this.long = results[0].geometry.location.lng()
    })
  }

}


class CreateTournamentFields {
  page: number = 1;
  tournament_name: string = '';
  tag_line: string = '';
  bio: string = '';
  registration_start_date: string = '';
  registration_end_date: string = '';
  start_date: string = '';
  end_date: string = '';
  tournament_id: number = 0;
  tournament_type: string = '';
  tournament_level: string = '';
}

class User {
  public password: string = "";
  public email: string = "";
  public name: string = "";
  public last_name: string = "";
  public mobile: string = "";
  public facebook_id = "";
  // public country_code: string = "+593"

}
