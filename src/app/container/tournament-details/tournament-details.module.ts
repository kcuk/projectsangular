import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TournamentDetailsComponent } from "./tournament-details.component";
import { SharedModule } from "./../../shared/shared.module";
import { tournamentRouting } from "./tournament-details.routes";
import { CreateTournamentFirstComponent } from "./create-tournament-first/create-tournament-first.component";
import { CreateTournamentSecondComponent } from "./create-tournament-second/create-tournament-second.component";
import { ResultsComponent } from "./results/results.component";
import { FixturesComponent } from "./fixtures/fixtures.component";
import { CustomCalendarModule } from "./../../custom-component/calender/custom-calender";
import { TabViewModule } from "primeng/primeng";

@NgModule({
  imports: [
    tournamentRouting,
    CustomCalendarModule,
    SharedModule.forRoot(),
    TabViewModule
  ],
  declarations: [
    TournamentDetailsComponent,
    CreateTournamentFirstComponent,
    CreateTournamentSecondComponent,
    ResultsComponent,
    FixturesComponent
  ]
})
export class TournamentDetailsModule {}
