import { Component, OnInit } from '@angular/core';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from './../../../GlobalService';
import { TranslateService } from "ng2-translate";


@Component({
  selector: 'app-round-details',
  templateUrl: './round-details.component.html',
  styleUrls: ['./round-details.component.css']
})
export class RoundDetailsComponent implements OnInit {
  constructor(private translate: TranslateService, public route: ActivatedRoute, public router: Router, public globalservice: GlobalService) {
    if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
      console.log("lang=>>>>>>>>>>>>>>>>>>>", localStorage.getItem('language'));
    }
  }
  public id: string;
  public roundsDetails: Array<any> = [];
  public tournament_name: any;
  public tournamentLink: any;
  public roundValidation: boolean = false;
  public noOfRegisteredTeam: number = 0;
  public roundInfo: any;
  public deleteTournament: boolean = false;
  public showProfileBttn: boolean = true;
  public is_automatic: string = '';

  ngOnInit() {
    this.tournament_name = localStorage.getItem('tournament_name');
    this.id = this.route.snapshot.params['id'];
    if (localStorage.getItem('activeLink') == "editTournament") {
      this.tournamentLink = true;
    } else {
      this.tournamentLink = false;
    }

    this.getRoundList();
    this.globalservice.roundPageRefresh.subscribe(res => {
      this.getRoundList();
    })
    var comUrl = window.location.href;
    var urlArray = comUrl.split("/");
    if (urlArray[urlArray.length - 1] === 'create-round') {
      this.showProfileBttn = false;
    }
  }
  public getRoundList() {
    let url = this.globalservice.base_path_api() + 'tournament/tournamentRound/?tournament=' + this.id + '&format=json';
    this.globalservice.GetRequest(url)
      .subscribe(res => {
        this.roundsDetails = res[0].json.data;
        this.noOfRegisteredTeam = res[0].json.registered_teams;
        this.is_automatic = res[0].json.tournament_is_automatic;

      },
      err => {
        console.log("error");
      })
  }
  public rountsRoutingFun(round) {
    this.roundInfo = round;
    if (round.round_type == 'group') {
      this.roundValidation = true;
    } else {
      this.router.navigateByUrl('dashboard/tournaments/round-details/' + this.id + '/create-round');
    }
  }

  public backToTournamentRouting() {
    this.router.navigateByUrl('dashboard/tournaments/tournament-profile/' + this.id);
  }
  public setActiveClass(id) {
    let elements = document.getElementsByClassName('activeLink');
    while (elements.length > 0) {
      elements[0].classList.remove('activeLink');
    }
    let get_id = document.getElementById(id)
    get_id.className += " activeLink";
  }
  public roundValidationRouting() {
    this.roundValidation = false;
    this.router.navigateByUrl('dashboard/tournaments/round-details/' + this.id + '/create-group?round_id=' + this.roundInfo.id + '&round_name=' + this.roundInfo.round_name + '&is_automatic=' + this.is_automatic);

  }
  public deleteTournamentInfo() {
    let url = this.globalservice.base_path_api() + 'tournament/tournamentDetail/' + this.id + '/?format=json';

    this.globalservice.DeleteRequest(url)
      .subscribe(res => {
        this.router.navigateByUrl('dashboard/tournaments');
      }, err => {

      })
  }
}
