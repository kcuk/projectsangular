import { NgModule } from '@angular/core';
import {roundDetailsRouting} from './round-details.routes';
import { RoundDetailsComponent } from './round-details.component';
import { CreateGroupComponent } from './create-group/create-group.component';
import { CreateRoundComponent } from './create-round/create-round.component';
import {SharedModule} from './../../../shared/shared.module';
import {EditTournamentComponent} from './edit-tournament/edit-tournament.component';

@NgModule({
  imports: [
    roundDetailsRouting,
    SharedModule.forRoot()
  ],
  declarations: [RoundDetailsComponent,CreateGroupComponent,CreateRoundComponent,EditTournamentComponent]
})
export class RoundDetailsModule { }
