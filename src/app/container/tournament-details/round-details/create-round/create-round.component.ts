import { roundDetailsRoutes } from './../round-details.routes';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';


@Component({
  selector: 'app-create-round',
  templateUrl: './create-round.component.html',
  styleUrls: ['./create-round.component.css']
})
export class CreateRoundComponent implements OnInit {

  public msgs: Array<any> = [];
  public loader: boolean = false;
  public lebel_of_rounds: any;
  public roudsDeatilsIns: any;
  public id: number = 0;
  public sub: any;
  public rounds: Array<any> = [];
  public roundsDetails: Array<any> = [];
  public showRounds: boolean = false;
  public showRoundsFields: boolean = false;
  public playOffListShow: boolean = false;
  public saveButtnShow: boolean = false;
  public showContinueBttn: boolean = false;
  public hideCreateNew: boolean = true;
  public round_id: number;
  public round_type;
  public round_name: any;
  public roundName: any;
  public deletePopup: boolean = false;
  public deleteRoundsId: number;
  public createRoundDisable: boolean = true;
  public noOfRegisteredTeam: number = 0;
  public is_automatic: string = '';
  public roundList: any[] = [
    { label:  localStorage.getItem('language') == 'en' ? "Select round" :'Seleccionar ronda', value: null },
    { label:  localStorage.getItem('language') == 'en' ? "Group stage" :'Fase de grupo', value: 'group' },
    { label:  localStorage.getItem('language') == 'en' ? "Simple elimination" :'Eliminación simple', value: 'SE' },
    { label:  localStorage.getItem('language') == 'en' ? "Double elimination" :'Eliminación doble', value: 'DE' }
  ];
  public regTeamPopup: boolean = false;
  public noOfRegTeamStatus: boolean = false;

  constructor(public _fb: FormBuilder, public router: Router, public route: ActivatedRoute, public base_path_service: GlobalService) {
    this.roudsDeatilsIns = new roudsDeatils();
  }

  ngOnInit() {
    let id = this.route.parent.params.subscribe(params => {
      this.roudsDeatilsIns.tournament = +params['id'];
    });
    this.getRoundList();
  }


  public getRoundList() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentRound/?tournament=' + this.roudsDeatilsIns.tournament + '&format=json'
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.roundsDetails = res[0].json.data;
        this.round_name = res[0].json.data;
        this.noOfRegisteredTeam = res[0].json.registered_teams;
        this.is_automatic = res[0].json.tournament_is_automatic;
        this.noOfRegTeamStatus = res[0].json.show;
        if (res[0].json.data && res[0].json.data.length > 0) {
          for (var i = 0; i < res[0].json.data.length; i++) {
            if (res[0].json.data[i].playoff_teams == 1) {
              this.createRoundDisable = false;
            }
          }
          this.showRounds = true;
        } else {
          this.showRounds = false;
          this.loader = false;
        }
        this.loader = false;
      },
      err => {
        console.log("error");
        this.loader = false;
      })
  }


  public createRoundData() {
    if (this.is_automatic != 'False') {
      this.loader = true;
      this.hideCreateNew = true;
      this.showRoundsFields = false;

      let url = this.base_path_service.base_path_api() + 'tournament/tournamentRound/?format=json';

      this.base_path_service.PostRequest(url, this.roudsDeatilsIns)
        .subscribe(res => {
          this.loader = false;
          if (this.round_type == "DE" || this.round_type == "SE") {
            this.roudsDeatilsIns.round_type = null;
            this.getRoundList();
            this.base_path_service.roundPageRefresh.next('refresh');
          }
          let id = res[0].json;
          id = id.round_id;
          this.round_id = id;
          if (this.round_type == "group") {
            this.router.navigateByUrl('dashboard/tournaments/round-details/' + this.roudsDeatilsIns.tournament + '/create-group?round_id=' + id + "&round_name=" + this.roundName + "&round_type=" + this.round_type + "&is_automatic=" + this.is_automatic + '&format=json');
          }
        },
        err => {
          this.loader = false;
          this.msgs.push({ severity: 'error', summary: 'Some Error Here', detail: err.json() });
          this.base_path_service.print(err.json());
        })
      this.roudsDeatilsIns.round_name = '';
      this.roudsDeatilsIns.round_type = '';
    } else {
      let url = this.base_path_service.base_path_api() + 'tournament/manuallyTornamentRound/?format=json'
      this.base_path_service.PostRequest(url, this.roudsDeatilsIns)
        .subscribe(res => {
          if (this.round_type != 'group') {
            this.getRoundList();
            this.base_path_service.roundPageRefresh.next('refresh');
          }
          this.showRoundsFields = false;
          this.hideCreateNew = true;
          this.roudsDeatilsIns.round_name = '';
          this.roudsDeatilsIns.round_type = '';
          let id = res[0].json.round_id
          if (this.round_type == 'group') {
            this.router.navigateByUrl('dashboard/tournaments/round-details/' + this.roudsDeatilsIns.tournament + '/create-group?round_id=' + id + "&round_name=" + this.roundName + "&round_type=" + this.round_type + "&is_automatic=" + this.is_automatic + '&format=json');
          }
        }, err => {


        })
    }
  }
  public createNewRoundsBttn(status: boolean) {
    if (this.roundsDetails) {
      this.showRoundsFields = true;
      this.hideCreateNew = false;
    } else {
      this.redirectPage();
    }

  }
  public redirectPage() {
    this.showRoundsFields = true;
    this.hideCreateNew = false;
  }
  public changeRounds(event) {
    this.roudsDeatilsIns.round_type = event.value;
    this.round_type = event.value;
  }
  public changeRoundName() {
    this.roundName = this.roudsDeatilsIns.round_name;
  }
  public deleteRounds() {
    this.deletePopup = false;
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentRound/' + this.deleteRoundsId + "/?format=json";
    this.base_path_service.DeleteRequest(url)
      .subscribe(res => {
        this.getRoundList();
        this.createRoundDisable = true;
        this.base_path_service.roundPageRefresh.next('refresh');
        this.msgs = [];
        this.msgs.push({ severity: 'info', detail: res[0].json.json() });
      },
      err => {
        this.msgs.push({ severity: 'error', detail: err[0].json.json() });
        this.base_path_service.print(err);
      })
  }
  public showDeletePopup(roundsInfo) {
    this.deletePopup = true;
    this.deleteRoundsId = roundsInfo.id;

  }
}

class roudsDeatils {
  tournament: number = 0;
  round_name: string = '';
  round_type: string = '';
}

