import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoundDetailsComponent } from './round-details.component';
import { CreateGroupComponent } from './create-group/create-group.component';
import { CreateRoundComponent } from './create-round/create-round.component';
import {EditTournamentComponent} from './edit-tournament/edit-tournament.component';

export const roundDetailsRoutes: Routes = [

    {
        path: '', component: RoundDetailsComponent,
        children: [
            { path: 'create-round', component: CreateRoundComponent },
            { path: 'create-group', component: CreateGroupComponent },
            { path: 'edit-tournament', component: EditTournamentComponent }
        ]
    }
]

export const roundDetailsRouting: ModuleWithProviders = RouterModule.forChild(roundDetailsRoutes)