import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';


@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent implements OnInit {

  public tournament_id: number;
  public round_name: string;
  public groupList: SelectItem[];
  public onSelectGroupList: SelectItem[];
  public playOffList: SelectItem[];
  public playOffValue: any;
  public groupDataIns: any;
  public sub: any;
  public roundId: any;
  public list1: Array<any> = [];
  public list2: any[];
  public groupValue: any;
  public base_path: string = '';
  public lengthOfList1: number = 0;
  public lengthOfList2: number = 0;
  public addInPickList: Array<any> = [];
  public removeInPickList: Array<any> = [];
  public PickListAddRemoveDataIns;
  public showPickList: boolean = false;
  public showSubmit: boolean = false;
  public ChangeRoundStatusIns: any;
  public initialList2: Array<any>;
  public showFinalSubmit: boolean = false;
  public msgs: Array<any> = [];
  public loader: boolean = false;
  public playoff: number;
  public submitClick: boolean = false;
  public form_type;
  public hideSubmit: boolean = true;
  public showInputBox: boolean = false;
  public hideCreateBttn: boolean = true;
  public showDropdown: boolean = false;
  public editGroupPopup: boolean = false;
  public groupNameForEdit: string = '';
  public editBttnShow: boolean = false;
  public editGroupIns: any;
  public edit_group_name: any;
  public picklistValidation: boolean = false;
  public changeText: string = '';
  public round_type: string = '';
  public is_automatic: string = ''
  public round_position: number = 0;
  public finalStatus: boolean = false;


  constructor(public router: Router, public route: ActivatedRoute, public base_path_service: GlobalService) {
    this.groupDataIns = new GroupData();
    this.PickListAddRemoveDataIns = new PickListAddRemoveData();
    this.ChangeRoundStatusIns = new ChangeRoundStatus();
    this.editGroupIns = new EditGroup()
  }

  ngOnInit() {

    let id = this.route.queryParams
      .subscribe(params => {
        this.groupDataIns.round = +params['round_id']
        this.round_name = params['round_name']
        this.round_type = params['round_type']
        this.is_automatic = params['is_automatic']
      });

    let tournament_id = this.route.parent.params.subscribe(params => {
      this.tournament_id = +params['id'];
    });

    this.getGroupList();

    this.base_path = this.base_path_service.base_path;

    if (typeof this.list2 !== 'undefined' && this.list2.length > 0) {
      this.lengthOfList1 = this.list1.length;
      this.lengthOfList2 = this.list2.length;
    }
  }

  public createGroup() {
    this.loader = true;
    this.groupDataIns.round = parseInt(this.groupDataIns.round);
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentGroup/?format=json';
    this.base_path_service.PostRequest(url, this.groupDataIns)
      .subscribe(res => {
        this.finalStatus = true;
        this.loader = false;
        let idObj = res[0].json;
        this.roundId = idObj.round_id;
        this.groupDataIns.group_name = '';
        this.round_position = idObj.round_position;
        this.getGroupList();
        this.msgs.push({ severity: 'info', detail: "Grupo creado con éxito" });
      },
      err => {
        this.loader = false;
        this.msgs.push({ severity: 'error', detail: '' });
      })

  }
  public getGroupList() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentGroup/?round=' + this.groupDataIns.round + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {


        if (res[0].json) {
          this.loader = false
          this.hideCreateBttn = false;
          this.showInputBox = false;
          this.showDropdown = true;

          let groupListObj = res[0].json;
          this.groupList = [];
          this.groupList.push({ label: localStorage.getItem('language') == 'en' ? "Choose group" : 'Escoger grupo', value: null });
          if (res[0].status != 204) {
            for (let index = 0; index < groupListObj.length; index++) {
              this.groupList.push({ label: groupListObj[index].group_name, value: groupListObj[index].id })
            }
          }
        } else {
          this.loader = false
        }
      })
  }

  public selectedGroupList(event, ref) {
    console.log("hello",ref.label);
    
    this.loader = true;
    if (this.groupValue != null) {
      this.editBttnShow = true;
    } else {
      this.editBttnShow = false;
    }
    let selectedGroupId = event.value;
    this.groupNameForEdit = ref.label;
    console.log("hello",this.groupNameForEdit);
    if (selectedGroupId != null) {
      let url = this.base_path_service.base_path_api() + 'tournament/tournamentGroup/' + selectedGroupId + "/?format=json";
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.loader = false;
          this.list2 = [];
          this.list2 = res[0].json.data;

          this.form_type = res[0].json.form_type;
          this.initialList2 = this.list2.slice();
          this.lengthOfList2 = this.list2.length;
          if (this.is_automatic == 'False') {
            if (this.round_position > 1) {
              this.showPickList = false;

            } else {
              this.showPickList = true;
              this.showSubmit = false;
              this.hideCreateBttn = false;
              this.showDropdown = true;
              this.PickListAddRemoveDataIns.tournament_group = this.groupValue;
              this.playoff = res[0].json.playoff;
              this.playOffValue = this.playoff;
              this.allGroupList();
            }

          } else if (this.is_automatic == 'True') {
            this.showPickList = true;
            this.showSubmit = false;
            this.hideCreateBttn = false;
            this.showDropdown = true;
            this.PickListAddRemoveDataIns.tournament_group = this.groupValue;
            this.playoff = res[0].json.playoff;
            this.playOffValue = this.playoff;
            this.allGroupList();
          }


        },
        err => {
          this.loader = false;
          this.msgs.push({ severity: 'error', detail: "Grupo creado con éxito" });
        })
    } else {
      this.loader = false;
      this.showPickList = false;
    }
  }
  public selectplayOffList(event) {
    this.showSubmit = false;
    this.compareArrays();
  }

  public allGroupList() {
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentRoundTeam/?round=' + this.groupDataIns.round + '&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        if (res[0].status != 204) {
          this.form_type = res[0].json.form_type;
          this.list1 = [];
          this.list1 = res[0].json.data;
        }
      },
      err => {
        this.base_path_service.print(err);
      })
  }

  public compareArrays() {

    let skip = false;
    let existing: Array<any> = []
    let add: Array<any> = [];
    let remove: Array<any> = [];
    if (this.form_type == 'simple') {
      for (let i = 0; i < this.list2.length; i++) {
        skip = false;
        for (let j = 0; j < this.initialList2.length; j++) {
          if (this.list2[i].team.id == this.initialList2[j].team.id) {
            existing.push(this.list2[i].id);
            skip = true;
            break;
          }
        }
        if (!skip)
          add.push(this.list2[i].team.id)
      }

      for (let i = 0; i < this.initialList2.length; i++) {
        skip = false;
        for (let j = 0; j < this.list2.length; j++) {
          if (this.list2[j].team.id == this.initialList2[i].team.id) {
            existing.push(this.initialList2[i].id);
            skip = true;
            break;
          }
        }
        if (!skip)
          remove.push(this.initialList2[i].team.id)
      }

      this.PickListAddRemoveDataIns.add = add;
      this.PickListAddRemoveDataIns.remove = remove;
    } else {
      for (let i = 0; i < this.list2.length; i++) {
        skip = false;
        for (let j = 0; j < this.initialList2.length; j++) {
          if (this.list2[i].id == this.initialList2[j].id) {
            existing.push(this.list2[i].id);
            skip = true;
            break;
          }
        }
        if (!skip)
          add.push(this.list2[i].id)
      }

      for (let i = 0; i < this.initialList2.length; i++) {
        skip = false;
        for (let j = 0; j < this.list2.length; j++) {
          if (this.list2[j].id == this.initialList2[i].id) {
            existing.push(this.initialList2[i].id);
            skip = true;
            break;
          }
        }
        if (!skip)
          remove.push(this.initialList2[i].id)
      }

      this.PickListAddRemoveDataIns.add = add;
      this.PickListAddRemoveDataIns.remove = remove;
    }

  }

  public updateGroupData() {
    if (this.is_automatic != 'False') {
      if (this.playOffValue > this.list2.length) {
        this.picklistValidation = true;
        this.changeText = "PlayOff no debe ser mayor que la lista del equipo del grupo"
      } else if (this.list2.length > 20) {
        this.picklistValidation = true;
        this.changeText = "No se puede empujar el equipo mayor de 20"
      } else {
        this.compareArrays();
        this.PickListAddRemoveDataIns.playoff = parseInt(this.playOffValue);
        let url = this.base_path_service.base_path_api() + 'tournament/assignTeam/?format=json';
        this.base_path_service.PostRequest(url, this.PickListAddRemoveDataIns)
          .subscribe(res => {
            this.msgs.push({ severity: 'info', detail: "Grupo actualizado correctamente" });
            this.showPickList = false;
            this.groupValue = null
            this.getGroupList();
            this.allGroupList();
            if (res[0].json.finish == true) {
              this.showFinalSubmit = true;
              this.showSubmit = false
            } else {
              this.showFinalSubmit = false;
              this.showSubmit = false
            }

          },
          err => {
            this.msgs.push({ severity: 'error', detail: '' });
            this.base_path_service.print(err);
          })
      }
    } else {
      this.compareArrays();
      let url = this.base_path_service.base_path_api() + 'tournament/manuallyTournamentGroup/?format=json';
      this.base_path_service.PostRequest(url, this.PickListAddRemoveDataIns)
        .subscribe(res => {
          this.msgs.push({ severity: 'info', detail: "Grupo actualizado correctamente" });
          this.showPickList = false;
          this.groupValue = null
          this.getGroupList();
          this.allGroupList();
          if (res[0].json.finish == true) {
            this.showFinalSubmit = true;
            this.showSubmit = false
          } else {
            this.showFinalSubmit = false;
            this.showSubmit = false
          }
        }, err => {

        })
    }
  }
  public finalSubmitData() {
    if (this.is_automatic != 'False') {
      this.ChangeRoundStatusIns.round = parseInt(this.groupDataIns.round);
      this.ChangeRoundStatusIns.form_type = 'status';
      let url = this.base_path_service.base_path_api() + 'tournament/tournamentRound/?format=json';
      this.base_path_service.PostRequest(url, this.ChangeRoundStatusIns)
        .subscribe(res => {
          if (res[0].status == 201) {
            this.router.navigateByUrl('dashboard/tournaments/round-details/' + this.tournament_id + '/create-round');
            this.base_path_service.roundPageRefresh.next('refresh');
          }
        })
    } else {
      let data = {
        round: parseInt(this.groupDataIns.round)
      }
      let url = this.base_path_service.base_path_api() + 'tournament/manuallyTournamentGroupSubmit/?format=json';
      this.base_path_service.PostRequest(url, data)
        .subscribe(res => {

          this.router.navigateByUrl('dashboard/tournaments/round-details/' + this.tournament_id + '/create-round');
          this.base_path_service.roundPageRefresh.next('refresh');
        }, err => {

        })
    }
  }

  public createFixtureFun() {
    this.router.navigateByUrl('/dashboard/create-fixture?id=' + this.groupDataIns.round + '&group=group');
  }
  public showCreateInputBox() {
    this.showInputBox = true;
    this.hideCreateBttn = false;
  }
  public showDropdownList() {
    this.showDropdown = true;
    this.showInputBox = false;
    this.groupValue = null
    this.createGroup();
  }
  public prevCreateInputBox() {
    this.showInputBox = true;
    this.showDropdown = false;
    this.showPickList = false;
    this.groupValue = null
  }
  public groupEditPopup() {
    this.editGroupPopup = true;
  }
  public editGroup(event) {
    this.editGroupIns.group_name = event;
    this.editGroupPopup = false;
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentGroup/' + this.groupValue + '/?format=json';
    this.base_path_service.PutRequest(url, this.editGroupIns)
      .subscribe(res => {
        this.base_path_service.print(res);
        this.getGroupList();
      },
      err => {
        this.msgs.push({ severity: 'error', detail: '' });
        this.base_path_service.print(err);
      })
  }

}

class GroupData {
  round: number = 0;
  group_name: string = '';
}
class PickListAddRemoveData {
  "tournament_group": number = 0;
  "add": Array<number> = [];
  "remove": Array<number> = [];
  "playoff": number;
}
class ChangeRoundStatus {
  'form_type': string = '';
  'round': number = 0;
}
class EditGroup {
  group_name: string = ''
}

