import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { countryCode } from "../../../registration/onboading2/country_code"
declare var google;

@Component({
  selector: 'app-edit-tournament',
  templateUrl: './edit-tournament.component.html',
  styleUrls: ['./edit-tournament.component.css']
})
export class EditTournamentComponent implements OnInit {


  edit_city_name: any;
  long: any;
  lat: any;
  city_name: any;
  country_name: any;
  autocompleteItems: any[];
  GoogleAutocomplete: any;
  checkCity: boolean=true;
  codes: SelectItem[] = [];
  checkcost: boolean;
  number: any;
  isMobValid: boolean;
  public loader: boolean = false;
  public myForm: FormGroup;
  public parentRouter: any
  public tournament_id: any;
  public city_list: any[];
  public tm_detail: any = new TournamentDetail;
  public date_format: any;
  public filteredCity;
  public cityValidation: boolean = false;
  public tournament_name: string = '';
  public genderValidation: boolean = false;
  public msgs: Array<any> = [];
  public reg_end_date: any;
  public reg_start_date: any;
  public tournament_start_date: any;
  public tournament_end_date: any;
  public disableTourStartDateField: boolean = true;
  public disableTourEndtDateField: boolean = true;
  public regStartDateValidation: boolean = false;
  public regEndDateValidation: boolean = false;
  public regEndDateDisable: boolean = true;
  public cityId: any;
  public statusOfCertPeloteando: boolean = false;
  public paymentMethodLebals: SelectItem[] = [];
  public payment_mode: Array<any> = [];
  public pdfFile: File;
  public pdfInfo;
  public es: any;
  public citySelect: boolean = false;
  public regDateValidation: boolean = false;
  public teamNumberValidation: boolean = false;
  public tourDateValidation: boolean = false;
  public cityValid: boolean = false;
  // code: any = [
  //   { value: '+1', label: '+1' },
  //   { value: '+502', label: '+502' },

  //   { value: '+593', label: '+593' },
  // ];
  length: any;
  mask: any = '999999999999999';
  @ViewChild('test') test: any;

  public gender_list: any[] = [
    { label: localStorage.getItem('language') == 'en' ? "Select type" : 'Seleccionar tipo', value: null },
    { label: localStorage.getItem('language') == 'en' ? "Male" : 'Masculino', value: 'Masculino' },
    { label: localStorage.getItem('language') == 'en' ? "Female" : 'Femenino', value: 'Femenino' },
    { label: localStorage.getItem('language') == 'en' ? "Mix" : 'Mixto', value: 'Mixto' }
  ];
  public result_type: any[] =
    [
      { label: localStorage.getItem('language') == 'en' ? "Select scoring structure" : 'Seleccionar estructura de puntaje', value: null },
      { label: 'Normal', value: 'Simple-Ponit' },
      { label: localStorage.getItem('language') == 'en' ? "Special" : 'Especial', value: 'Penalties-Point' }
    ]

  public tournament_levels: any[] = [
    { label: localStorage.getItem('language') == 'en' ? "Select modality" : 'Seleccionar modalidad', value: null },
    { label: '5V5', value: 'V5' },
    { label: '6V6', value: 'V6' },
    { label: '7V7', value: 'V7' },
    { label: '8V8', value: 'V8' },
    { label: '9V9', value: 'V9' },
    { label: '11V11', value: 'V11' },
  ];

  public tournament_divisions: any[] = [
    { label: localStorage.getItem('language') == 'en' ? "Select the tournament division" : 'Seleccione la división del torneo', value: null },
    { label: localStorage.getItem('language') == 'en' ? "Recreational" : 'Recreacional', value: 'Recreacional' },
    { label: localStorage.getItem('language') == 'en' ? "Semi Competitive" : 'Semi Competitivo', value: 'Semi-Competitivo' },
    { label: localStorage.getItem('language') == 'en' ? "Competitive" : 'Competitivo', value: 'Competitivo' }
  ];

  constructor(public _fb: FormBuilder, public base_path_service: GlobalService, public _router: Router, public _route: ActivatedRoute,
    public zone: NgZone, ) {
    this.tournament_name = localStorage.getItem('tournament_name');
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();

    this.es = { closeText: "Cerrar", prevText: "", currentText: "Hoy", monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"], monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"], dayNames: ["domingo", "lunes", "martes", "miÃ©rcoles", "jueves", "viernes", "sÃ¡bado"], dayNamesShort: ["dom", "lun", "mar", "miÃ©", "jue", "vie", "sÃ¡b"], dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"], weekHeader: "Sm", dateFormat: "yy-mm-dd", firstDay: 1, isRTL: false, showMonthAfterYear: false, yearSuffix: "" };

    this.buildForm();
    this.date_format = {
      dateFormat: "yy-mm-dd"
    }
    // this.cityList();
    this.paymentMethods();
    countryCode.map((val) => {
      this.codes.push({
        label: val.name + " " + "(" + val.dial_code + ")",
        value: val.dial_code
      })
    })

  }

  ngOnInit() {
    this.parentRouter = this._route.parent.params.subscribe(params => {
      this.tournament_id = +params["id"];
    });
    this.getTournamentDetails();
  }

  // public cityList() {
  //   this.loader = true;
  //   let url = this.base_path_service.base_path_api() + 'team/location/?form_type=city';
  //   this.base_path_service.GetRequest(url)
  //     .subscribe(res => {
  //       this.loader = false;
  //       this.city_list = [];
  //       this.city_list.push({ label: 'Select CIty', value: null });
  //       let city = res[0].json;
  //       for (var index = 0; index < res[0].json.length; index++) {
  //         this.city_list.push({ label: city[index].location, value: city[index].id });
  //       }
  //     }, err => {
  //     })
  // }

  public filterCity(event) {
    let query = event.query;
    let country = 1;
    if (country != undefined) {
      var url = this.base_path_service.base_path_api() + "team/location/?country=" + country + "&city=" + query + "&format=json";
      this.base_path_service.GetRequest(url)
        .subscribe(
          res => {
            this.filteredCity = res[0].json;

            if (res[0].json.length == 0) {
              this.cityValidation = true;
              this.citySelect = false;
            } else {
              this.cityValidation = false;
              this.citySelect = false;
            }
          },
          err => {
          });
    }
    else {

    }
  }
  public paymentMethods() {
    let url = this.base_path_service.base_path_api() + "peloteando/payment_mode/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.paymentMethodLebals = [];
        let paymentInfo = res[0].json;
        if (res[0].json) {
          for (var i = 0; i < res[0].json.length; i++) {
            this.paymentMethodLebals.push({ label: paymentInfo[i].option, value: paymentInfo[i].id });
          }
        }
      }, err => {
        this.base_path_service.print(err);
      })
  }
  public getTournamentDetails() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentDetail/' + this.tournament_id + '/?format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(
        res => {
          this.loader = false;
          this.tm_detail = res[0].json;
          this.updateFormValue();
        },
        err => {
        })
  }
  formatDateFun(event) {
    var validDate;
    if (event.toString().length > 10) {
      let month = parseInt(event.getMonth());
      month = month + 1
      if (event.getMonth() > 8) {
        if (event.getDate() > 9) {
          validDate = event.getFullYear() + "-" + month + "-" + event.getDate();
        } else {
          validDate = event.getFullYear() + "-" + month + "-" + '0' + event.getDate();
        }

      } else {
        if (event.getDate() > 9) {
          validDate = event.getFullYear() + "-" + '0' + month + "-" + event.getDate();
        } else {
          validDate = event.getFullYear() + "-" + '0' + month + "-" + '0' + event.getDate();
        }
      }
      return validDate;
    } else {
      return event;
    }

  }

  public updateFormValue() {
    // this.cityId = this.tm_detail.city.id;
 
    console.log("city=>>>>>>>>>>>>>>",this.tm_detail.city.location)
    this.edit_city_name = this.tm_detail.city.location;
    this.myForm.patchValue({
      'nombre': this.tm_detail.tournament_name,
      'ciudad': this.tm_detail.city,
      'modalidad': this.tm_detail.tournament_level,
      'fecha_one': this.tm_detail.start_date,
      'fecha_two': this.tm_detail.end_date,
      'fecha_three': this.tm_detail.registration_start_date,
      'fecha_four': this.tm_detail.registration_end_date,
      'torneo': this.tm_detail.tournament_division,
      'equipos': this.tm_detail.max_no_of_teams,
      'cuota': this.tm_detail.registration_fee,
      'premio': this.tm_detail.prize,
      'bio': this.tm_detail.description,
      'genero': this.tm_detail.tournament_type,
      'result_type': this.tm_detail.tournament_point_type,
      "country_code": this.tm_detail.country_code,
      "contact": this.tm_detail.contact,
      'min_fee': this.tm_detail.minimum_fee ? this.tm_detail.minimum_fee : 0,
      'discount': this.tm_detail.discount ? this.tm_detail.discount : 0,
      'vat': this.tm_detail.vat ? this.tm_detail.vat : 0,
      'no_of_admins': this.tm_detail.no_of_admin
    })
    // if (this.tm_detail.country_code == '+593') {
    //   this.length = 9;
    // }
    // else if (this.tm_detail.country_code == '+502') {
    //   this.length = 8;
    // }
    // else {
    //   this.length = 10
    // }
    this.lat = this.tm_detail.latitude;
    this.long = this.tm_detail.longitude;
    this.country_name = this.tm_detail.city.country_name;
    this.city_name = this.tm_detail.city.description;
    this.tournament_start_date = this.tm_detail.start_date;
    this.tournament_end_date = this.tm_detail.end_date;
    this.reg_start_date = this.tm_detail.registration_start_date;
    this.reg_end_date = this.tm_detail.registration_end_date;
    if (this.tm_detail.rule_file) {
      this.pdfInfo = this.tm_detail.rule_file.split('/media/');
    }
    if (this.tm_detail.payment_mode) {
      for (var index = 0; index < this.tm_detail.payment_mode.length; index++) {
        this.payment_mode[index] = (this.tm_detail.payment_mode[index].id);
      }

      this.myForm.patchValue({
        payment_mode: this.payment_mode
      })
    }
  }

  public submitForm(formData) {
    this.loader = true;
    if (this.myForm.valid && this.checkCity) {
      // if (this.tm_detail.city.location == formData.ciudad) {
      //   this.cityValid = true;
      // } else if (formData.ciudad.id !== undefined) {
      //   this.cityValid = true;
      // }
      if (formData.equipos > 1 && this.tourDateValidation == false && this.regDateValidation == false) {
        let url = this.base_path_service.base_path_api() + 'tournament/tournamentUpdate/' + this.tournament_id + '/?format=json';
        if (formData.cuota == "") {
          formData.cuota = 0;
        }
        if (formData.premio == "") {
          formData.premio = 0;
        }

        let myjson = {
          "tournament_name": formData.nombre,
          "bio": formData.bio,
          "registration_start_date": this.formatDateFun(formData.fecha_three),
          "registration_end_date": this.formatDateFun(formData.fecha_four),
          "start_date": this.formatDateFun(formData.fecha_one),
          "end_date": this.formatDateFun(formData.fecha_two),
          "tournament_level": formData.modalidad,
          "city_name": this.city_name,
          "country_name": this.country_name,
          "registration_fee": parseInt(formData.cuota),
          "tournament_division": formData.torneo,
          "tournament": this.tm_detail.id,
          "prize": parseInt(formData.premio),
          "rule": formData.reglas,
          "tournament_type": formData.genero,
          "max_no_of_teams": formData.equipos,
          "payment_mode": this.payment_mode,
          "result_type": formData.result_type,
          "country_code": formData.country_code,
          "contact": formData.contact,
          "minimum_fee": formData.min_fee == "" ? '0' : formData.min_fee,
          "discount": formData.discount == null ? 0 : formData.discount,
          "vat": formData.vat == null ? 0 : formData.vat,
          "latitude": this.lat,
          "longitude": this.long


        }
     
        this.base_path_service.PutRequest(url, myjson)
          .subscribe(res => {
            this.loader = false;
            this.msgs.push({ severity: 'info', summary: 'Alguna información aquí', detail: "Actualizar con éxito" });
            this._router.navigateByUrl('dashboard/tournaments/tournament-profile/' + this.tournament_id);
          },
            err => {
              this.loader = false;
              this.msgs.push({ severity: 'error', detail: "Ingresa todos los campos obligatorios" });
            })
      } else {
        this.loader = false;
        if (formData.equipos < 2) {
          this.teamNumberValidation = true;
        }
      }
    } else {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', detail: "Ingresa todos los campos obligatorios" });
    }
  }

  public buildForm() {
    this.myForm = this._fb.group({
      nombre: ['', [Validators.compose([Validators.required, Validators.maxLength(50)])]],
      ciudad: ['', [Validators.required]],
      modalidad: ['', [Validators.required]],
      genero: ['', [Validators.required]],
      fecha_one: ['', [Validators.required]],
      fecha_two: ['', [Validators.required]],
      fecha_three: ['', [Validators.required]],
      fecha_four: ['', [Validators.required]],
      torneo: ['', [Validators.required]],
      equipos: ['', [Validators.compose([Validators.required,])]],
      cuota: ['', [Validators.compose([Validators.required])]],
      premio: ['', [Validators.compose([Validators.required])]],
      bio: ['', [Validators.compose([Validators.maxLength(200)])]],
      payment: ['', []],
      is_automated: [, []],
      payment_mode: [[], []],
      result_type: [[]],
      country_code: [[Validators.required]],
      contact: [[Validators.required]],
      min_fee: "",
      discount: '',
      vat: "",
      no_of_admins: ['', [Validators.required]]
    })
  }

  public regStartDate(event) {
    let newDate = new Date(event).toISOString();
    this.reg_start_date = newDate;
    if (this.reg_start_date <= this.reg_end_date) {
      this.regDateValidation = false;
    } else {
      this.regDateValidation = true;
    }

    if (this.reg_start_date <= this.tournament_start_date && this.reg_start_date <= this.tournament_end_date) {
      this.tourDateValidation = false;
    } else {
      this.tourDateValidation = true;
    }

  }
  public regEndDate(event) {
    let newDate = new Date(event).toISOString();
    this.reg_end_date = newDate;
    if (this.reg_start_date <= this.reg_end_date) {
      this.regDateValidation = false;
    } else {
      this.regDateValidation = true;
    }

    if (this.reg_end_date <= this.tournament_start_date && this.reg_end_date <= this.tournament_end_date) {
      this.tourDateValidation = false;
    } else {
      this.tourDateValidation = true;
    }


  }
  public tournament_Start_Date(event) {
    let newDate = new Date(event).toISOString();
    this.tournament_start_date = newDate;
    if (this.tournament_start_date <= this.tournament_end_date) {
      this.tourDateValidation = false;
    } else {
      this.tourDateValidation = true;
    }

    if (this.tournament_start_date >= this.reg_start_date && this.tournament_start_date >= this.reg_end_date) {
      this.regDateValidation = false;
    } else {
      this.regDateValidation = true;
    }
  }
  public tournament_End_Date(event) {
    let newDate = new Date(event).toISOString();
    this.tournament_end_date = newDate;

    if (this.tournament_end_date >= this.reg_end_date && this.tournament_end_date >= this.tournament_start_date && this.tournament_end_date >= this.reg_start_date) {
      this.tourDateValidation = false;
    } else {
      this.tourDateValidation = true;
    }

  }
  public uploadPdf($event) {
    this.pdfFile = $event.target.files[0];
    this.pdfInfo = $event.target.files[0].name;

    this.makeFileUploadRequest().then(
      (result) => {

      },
      (error) => {

      });
  }
  public equipoValidation(value) {
    if (value > 1) {
      this.teamNumberValidation = false;
    } else {
      this.teamNumberValidation = true;
    }
  }
  public makeFileUploadRequest() {
    this.loader = true;
    var url = this.base_path_service.base_path_api() + 'tournament/image/' + this.tournament_id + '/';
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      formData.append("rule_file", this.pdfFile);
      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else if (xhr.status == 201) {
            this.loader = false;
          } else {
            this.loader = false;
            reject(xhr.response);
          }
        }
      }
      xhr.open("PUT", url, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + JSON.parse(localStorage.getItem('userInfo')).token.access_token);
      xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
      xhr.send(formData);
    });
  }
  backToProfile() {
    this._router.navigateByUrl('dashboard/tournaments/tournament-profile/' + this.tournament_id);
  }


  // validMobile(event) {
  //   console.log("event=>>>>>>>>>>>>>>>>>>>>>>>", event);
  //   this.myForm.patchValue({ 'contact': "" })
  //   if (event.value == '+1') {
  //     this.length = 10
  //   }
  //   else if (event.value == '+502') {
  //     this.length = 8
  //   }
  //   else {
  //     this.length = 9
  //   }
  // }

  // mobileVerify(value) {
  //   console.log("valueeeeeeeeeeeee", value)
  //   this.number = value
  //   if (value.toString().length != this.length || value < 0) {
  //     console.log("mobileeeeeeeeeeeeeeee")
  //     this.isMobValid = true;

  //   }
  //   else
  //     this.isMobValid = false;
  //   console.log("mobileeeeeeeeeeeeeeee =>>>>>>>>>>>>>>", this.isMobValid)

  // }

  checkCost(event) {
    console.log("event=>>>>>>>>>>>>>>>>>>>>", event.target.value);
    if (parseInt(this.myForm.controls['cuota'].value) < parseInt(this.myForm.controls['min_fee'].value)) {
      this.checkcost = true
    }
    else {
      this.checkcost = false
    }

  }

  search(event) {
    this.checkCity = false;
    console.log("valussssssssss", event)
    this.GoogleAutocomplete.getPlacePredictions({ input: event.query, types: ['(regions)'] },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
          console.log("list", this.autocompleteItems)
        });
      });
  }

  setLoaction(item) {
    console.log("item=>>>>>>>>>>>>>>>>", item,this.myForm.controls.ciudad);
    this
    this.checkCity = true;
    this.country_name = item.terms.pop().value;
    this.city_name = item.terms[0].value;
    console.log("jhsjhgjhgsssssssssssssssssssssss",this.city_name)
    this.geoCode((item.description));

  }

  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.lat = results[0].geometry.location.lat();
      this.long = results[0].geometry.location.lng()
    })
  }

}
class TournamentDetail {
  "id": number = null;
  "tournament_name": string = "";
  "description": string = "";
  "registration_start_date": string = "";
  "registration_end_date": string = "";
  "start_date": string = "";
  "end_date": string = "";
  "tournament_level": string = "";
  "tournament_format": string = "";
  "registration_fee": string = "";
  "logo": string = "";
  "cover_pic": string = "";
}
