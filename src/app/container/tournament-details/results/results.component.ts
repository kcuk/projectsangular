import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../GlobalService';
import { Message } from 'primeng/primeng'
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  lang: string;
  private cities: Array<any> = [];
  private selectedCity: string;
  val: any;
  private fixtureId: string = "";
  matchDetail: any;
  team1: any = {};
  team2: any = {};
  private basePath: string;
  desc: any = {};
  rounds: any;
  paramsSub: any;
  admins: boolean = false;
  arePenalties: boolean = false;
  isLoaderActivated: boolean = true;
  tournamentID: any;
  msgs: Message[] = [];
  private matchId: string = "";
  private teamId: string = "";
  public url: string = "";
  redCardArrayTeam1: any[] = [];
  redCardArrayTeam2: any[] = [];
  suspended: boolean = false;
  is_edit: any;
  showPopUp: boolean;
  readMsg:boolean=false;

  constructor(private translate: TranslateService, private global: GlobalService, private router: Router, private route: ActivatedRoute) {
    this.lang = localStorage.getItem('language');
    translate.use(this.lang);

    this.paramsSub = this.route.queryParams.subscribe(params => {
      this.fixtureId = params['fixtureId'];
      this.tournamentID = params['tourID'];
      this.matchId = params['matchID'];
    });
    this.basePath = this.global.base_path;
  }

  ngOnInit() {
    this.url = window.location.pathname;
    if (this.matchId) {
      this.playerMatchList();
    } else {
      this.getResult();
    }

  }

  getResult() {
    this.msgs = [];
    this.isLoaderActivated = true;
    let url = this.global.base_path + "api/scorecard/score/?fixture=" + this.fixtureId + "&format=json";
    this.global.GetRequest(url)
      .subscribe(res => {
        this.isLoaderActivated = false;
        this.matchDetail = res[0].json;
        this.admins = res[0].json.admin;
        this.is_edit = res[0].json.is_edit;
        this.rounds = res[0].json.round_name;
        this.team1 = this.matchDetail.team_list[0];
        this.team2 = this.matchDetail.team_list[1];
        this.desc = { isCancel: this.matchDetail.is_cancelled, description: this.matchDetail.description };
        this.suspended = this.matchDetail.is_suspended;
        this.redCardArrayTeam1 = this.team1.default_players;
        this.redCardArrayTeam2 = this.team2.default_players;
      },
        err => {
          this.msgs.push({ severity: 'error', summary: 'Mensaje de error', detail: 'Algunos error al obtener el resultado' });
          this.isLoaderActivated = false;
        });
  }

  updateResult() {
    if(this.is_edit){
      this.showPopUp = true;
    }
    else{
      this.saveResult();
    }
  }

  saveResult() {
    this.showPopUp = false;
    this.isLoaderActivated = true;
    if (typeof this.fixtureId == "undefined" && typeof this.tournamentID == "undefined") {
      this.fixtureId = "";
      this.tournamentID = "";
    }
    let url = this.global.base_path + "api/scorecard/score/?format=json";
    this.team1.default_players = this.redCardArrayTeam1;
    this.team2.default_players = this.redCardArrayTeam2;
    let data = {
      fixture: this.fixtureId,
      description: this.desc.description,
      is_cancelled: this.desc.isCancel,
      is_suspended: this.suspended,
      team_list: [this.team1, this.team2],
      normal_match: this.matchId
    };
    this.global.PostRequest(url, data)
      .subscribe(res => {
        this.msgs = [];
        this.msgs.push({ severity: 'info', summary: 'Mensaje existoso', detail: 'El resultado ha guardado correctamente' });
        if (this.matchId) {
          var id = JSON.parse(localStorage.getItem('userInfo'))[0].info.profile_id;
          this.router.navigateByUrl('dashboard/player/player-profile/' + id + '/schedule');
        } else {
          this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + this.tournamentID + '/schedule');
        }
        this.isLoaderActivated = false;
      },
        err => {
          console.log("elseeeeeeeeeeeeeeeeeee")
          this.msgs = []
          this.msgs.push({ severity: 'error', detail: (localStorage.getItem('language') == 'es' ? 'Por favor ingrese la sanción' : 'Please enter sanction') });
          this.isLoaderActivated = false;
        });
  }

  // **********************function to update record of a single player(function triggered by chenging values of table row) ***********************************
  updateRowData(data, value, type) {
    let val = value.target.value
    switch (type) {
      case 3:
        data.yellow_card = val;
        break;
      case 4:
        data.assist_goal = val;
        break;
      case 5:
        data.goal = val;
    }
  }
  jsonData() {
    let url1 = "/result.json";
    this.global.GetRequest(url1)
      .subscribe(res => {
        this.isLoaderActivated = false;
        this.matchDetail = res[0].json;
        this.team1 = this.matchDetail.team_list[0];
        this.team2 = this.matchDetail.team_list[1];
        this.desc = { isCancel: this.matchDetail.is_cancelled, description: this.matchDetail.description };
      },
        err => {
          this.msgs.push({ severity: 'error', summary: 'Mensaje de error', detail: 'Algunos error al obtener el resultado' });
          this.isLoaderActivated = false;
          this.jsonData();
        });

  }
  updateRowData1(data: any, value: any, type: any) {
  }

  private playerMatchList() {
    this.msgs = [];
    this.isLoaderActivated = true;
    let url = this.basePath + 'api/scorecard/score/?normal_match=' + this.matchId + '&format=json';
    this.global.GetRequest(url)
      .subscribe(res => {
        this.isLoaderActivated = false;
        this.matchDetail = res[0].json;
        this.admins = res[0].json.admin;
        this.rounds = res[0].json.round_name;
        this.team1 = this.matchDetail.team_list[0];
        this.team2 = this.matchDetail.team_list[1];
        this.desc = { isCancel: this.matchDetail.is_cancelled, description: this.matchDetail.description };
      }, err => {
        this.msgs.push({ severity: 'error', summary: 'Mensaje de error', detail: 'Algunos error al obtener el resultado' });
        this.isLoaderActivated = false;
      })

  }
  private routing() {
    this.router.navigateByUrl('dashboard/tournaments/tournament-profile/' + this.tournamentID + '/schedule')
  }

  checkTeam1RedCard(player) {
    if (player.red_card) {
      // player.yellow_card = 0;
      this.redCardArrayTeam1.push({
        name: player.player_name + " " + (player.last_name ? player.last_name : ""),
        player_pk: player.player_pk,
        sanction_games: "",
        sanction_reason: ""
      })
    }
    else {
      let index = this.redCardArrayTeam1.findIndex(x => {
        return x.player_pk == player.player_pk;
      })
      console.log("index", index);
      this.redCardArrayTeam1.splice(index, 1)
    }
  }

  checkTeam2RedCard(player) {
    if (player.red_card) {
      // player.yellow_card = 0;
      this.redCardArrayTeam2.push({
        name: player.player_name + " " + (player.last_name ? player.last_name : ""),
        player_pk: player.player_pk,
        sanction_games: "",
        sanction_reason: ""
      })
    }
    else {
      let index = this.redCardArrayTeam2.findIndex(x => {
        return x.player_pk == player.player_pk;
      })
      console.log("index", index);
      this.redCardArrayTeam2.splice(index, 1)
    }
  }

  checkCancel() {
    if (this.desc.isCancel) {
      this.suspended = false;
    }
  }

  checkSuspended() {
    if (this.suspended) {
      this.desc.isCancel = false;
      this.desc.description = ''
    }
  }
}
