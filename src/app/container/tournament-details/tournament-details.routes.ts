import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TournamentDetailsComponent } from './tournament-details.component';
import { TournamentProfileModule } from './tournament-profile/tournament-profile.module';
import { EditSponserComponent } from './tournament-profile/edit-sponser/edit-sponser.component';
import { CreateTournamentFirstComponent } from './create-tournament-first/create-tournament-first.component';
import { CreateTournamentSecondComponent } from './create-tournament-second/create-tournament-second.component';
import { RoundDetailsModule } from './round-details/round-details.module';
import { ResultsComponent } from './results/results.component';
import { FixturesComponent } from './fixtures/fixtures.component';
export const tournamentRoutes: Routes = [
    { path: '', component: TournamentDetailsComponent },
    { path: 'tournament-profile/:id', loadChildren: './tournament-profile/tournament-profile.module#TournamentProfileModule' },
    { path: 'create-tournament', component: CreateTournamentFirstComponent },
    { path: 'create-tournament/:id', component: CreateTournamentSecondComponent },
    { path: 'round-details/:id', loadChildren: './round-details/round-details.module#RoundDetailsModule' },
    { path: 'results', component: ResultsComponent },
    { path: 'create-fixture', component: FixturesComponent }


]

export const tournamentRouting: ModuleWithProviders = RouterModule.forChild(tournamentRoutes)