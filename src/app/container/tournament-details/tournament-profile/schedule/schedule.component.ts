
import { Component, OnInit, ViewChild, HostListener, Inject, Output, style } from '@angular/core';
import { Message, SelectItem } from 'primeng/primeng';
import { Routes, ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';
import { DOCUMENT } from "@angular/platform-browser";
import * as html2canvas from 'html2canvas';
import { SlicePipe } from '@angular/common';

declare const html2pdf: any;
declare const $: any;
declare const FB: any;

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {
  sponser_list_length: boolean;
  paid: any;
  ground_id: any = null;

  public round_Details: Array<any> = [];
  public matchInfo: Array<any> = [];
  public hasRounds: boolean = false;
  public hasFixture: boolean;
  public roundID: any;
  is_fixture: boolean = false;
  round_status: boolean = false;
  public roundType: string;
  public roundName;
  public tournamentId: any;
  public registeredTeam: number = 0;
  semifinal: boolean = false;
  no_rounds: boolean = false;
  msgs: Message[] = [];
  loader: boolean = false;
  is_admin: boolean = false;
  public roundData: Array<any> = [];
  public deData: Array<any> = [];
  roundList: Array<any> = [];
  sortList: Array<any> = [];
  selectedOption: any;
  DeSeFixtureData: Array<any> = [];
  GroupFixtureData: Array<any> = [];
  groupID: any;
  date: any;
  groupList: Array<any> = [];
  selectedGroup: any;
  fixtureList: Array<any> = [];
  groupType: boolean = false;
  currentDate: any;
  dd: any;
  yyyy: any;
  mm: any;
  increament: number = 0;
  groupIndex: any;
  page = 1;
  sortType: any;
  gID: any;
  dateType: boolean = false;
  total_pages: any;
  public isResult: boolean = false;
  public isResultDeclared: boolean = false;
  public imageUrl = "";
  public basePath: any;
  public confirmationResult: boolean = false;
  public type: string = '';
  public nextRoundRegistartion: boolean = false;
  public is_automatic: string = '';
  public nextRoundData: Array<any> = [];
  public nextRoundArray: number[] = [];
  public nextRoundGroup: Array<any> = [];
  public nextRoundGroupList: SelectItem[];
  public nextRoundName: string = '';
  public selectedGroupForRound: number = null;
  public nextRoundStatus: boolean = true;
  public groupCounter: number = 0;
  public dropdownStatus: boolean = false;
  public nextRoundMessage: boolean = false;
  public imagePath: string = '';
  public round_position: number = 1;
  public noRegTeamPopup: boolean = false;
  public regTeamPopup: boolean = false;
  @ViewChild('layout') canvasRef;
  @ViewChild('layoutShare') layoutShare;
  @ViewChild('tablehieght') tablehieght;
  public destroy: any;
  public dateList: SelectItem[];
  public date_info_fetcha: string = '';
  public date_counter: number = 0;
  public select_counter: number = 1;
  public GroupFixtureDataInfo: Array<any> = [];
  public DeSeFixtureDataInfo: Array<any> = [];
  public pdfShow: boolean = false;
  public mylist: any;
  public fixtureImageSharePopup: boolean = false;
  public imageUrlDownload: string = "";
  public fixturePopup: boolean = false;
  public fixtureShareData: any;
  public loadTime: boolean = false;
  display_scroll_arrow: boolean = false;
  check_team: boolean = false;
  check_date: boolean = false;
  sub_group: boolean = true;
  sponsers: any;
  constructor(@Inject(DOCUMENT) public document: Document, public base_path_service: GlobalService, public route: ActivatedRoute, public router: Router) {

  }

  ngOnInit() {

    var tempUrl = window.location.href;
    this.base_path_service.shareTwitterInfo = "";
    this.base_path_service.shareImageInfo = "";
    this.base_path_service.shareTwitterInfo = tempUrl;
    this.nextRoundArray = [];
    this.imagePath = this.base_path_service.image_url;
    this.basePath = this.base_path_service.base_path_api();
    let id = this.route.parent.params.subscribe(params => {
      this.tournamentId = +params['id'];
    });

    this.API_getRoundDetails();
    this.destroy = this.base_path_service.pageChange.subscribe(res => {
      this.tournamentId = res[3];
      if (res[4] === "schedule")
        this.API_getRoundDetails();
    })
    this.getCurrentDate(this.increament);
    this.imageUrl = this.base_path_service.image_url;

    setTimeout(() => {
      this.loadTime = true;
    }, 2000)

  }
  public onScroll() {
    this.page = this.page + 1;
    if (this.page <= this.total_pages) {
      if (this.check_team) {
        this.getFixtureByTeam();
      }
      else {
        this.paginateGroupType(this.page);
      }
    }

  }


  getCurrentDate(val: any) {
    this.currentDate = new Date();
    this.dd = this.currentDate.getDate();

    this.currentDate.setDate(this.dd + val); //set date on click
    this.dd = this.currentDate.getDate();

    this.mm = this.currentDate.getMonth() + 1; //January is 0!
    this.yyyy = this.currentDate.getFullYear();
    if (this.dd < 10) {
      this.dd = '0' + this.dd
    }
    if (this.mm < 10) {
      this.mm = '0' + this.mm
    }
    this.currentDate = this.yyyy + "-" + this.mm + "-" + this.dd;

  }


  API_getSeDeFixtures() {
    this.loader = true;
    this.sub_group = true
    this.DeSeFixtureDataInfo = [];
    this.GroupFixtureDataInfo = [];
    this.sortType = "date";
    if (this.roundType == "SE" || this.roundType == "DE") {
      this.gID = null;
    } else {
      this.gID = this.groupID;
    }
    var url = this.base_path_service.base_path_api() + "tournament/tournamentRound/" + this.roundID + "/?group=" + this.gID + "&sort=" + this.sortType + "&date=" + this.selectedGroup + "&page=1&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.groupList = []
        this.total_pages = res[0].json.total_pages
        if (res[0].json.date_list.length) {
          for (var index = 0; index < res[0].json.date_list.length; index++) {
            this.groupList.push({ label: res[0].json.date_list[index], value: res[0].json.date_list[index] })
          }
        }
        if (this.roundType == "SE" || this.roundType == "DE") {
          this.DeSeFixtureDataInfo.push(res[0].json.data);
          this.selectedGroup = res[0].json.data[0].date_time.slice(0, 10);
          this.groupList.forEach((obj,ind)=>{
            console.log("obj =>>>>>>>>>>",obj,"index =>>>>>>>>>>>>>",ind)
            if(obj.label==this.selectedGroup){
              this.groupIndex = ind;
              console.log("index =>>>>>>>>>>>>>>>>",this.groupIndex)
            }
          })
        } else {
          this.GroupFixtureDataInfo.push(res[0].json.data);
          this.selectedGroup = res[0].json.data[0].date_time.slice(0, 10);
          this.groupList.forEach((obj,ind)=>{
            console.log("obj =>>>>>>>>>>",obj,"index =>>>>>>>>>>>>>",ind)
            if(obj.label==this.selectedGroup){
              this.groupIndex = ind;
              console.log("index =>>>>>>>>>>>>>>>>",this.groupIndex)
            }
          })
          console.log("dateeeeeeeeeeeee", this.selectedGroup);
        }

      },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', detail: 'error' });
        })
  }


  API_getRoundDetails() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "tournament/tournamentRound/?tournament=" + this.tournamentId + "&form_type=profile&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        if (res[0].status != 204) {
          localStorage.removeItem('selectedRound');
          this.loader = false;
          this.is_admin = res[0].json.admin;
          this.round_Details = res[0].json.data;
          this.registeredTeam = res[0].json.registered_teams;
          this.is_automatic = res[0].json.tournament_is_automatic;
          this.noRegTeamPopup = res[0].json.show;
          this.paid = res[0].json.is_peloteando_certified;
          let tour_second_payment_data = {
            no_of_teams: res[0].json.no_of_teams,
            cost_per_team: res[0].json.cost_per_team,
            tournament_name: res[0].json.tournament_name,

          }
          if (this.paid) {
            localStorage.removeItem('tour_second_payment_data')
          }
          else {
            localStorage.setItem("tour_second_payment_data", JSON.stringify(tour_second_payment_data))
          }
          this.base_path_service.shareImageInfo = this.imageUrl + "/" + res[0].json.share_fixture_link;
          for (let i = 0; i < this.round_Details.length; i++) {
            this.roundList.push({ "label": this.round_Details[i].round_name, "value": this.round_Details[i] })
          }

          if (res[0].json.data.length === 0) {
            this.no_rounds = true;
            this.is_fixture = false;
          } else {
            // Auto Select First             
            for (let i = 0; i < res[0].json.data.length; i++) {
              if (res[0].json.data[i].group != null && res[0].json.data[i].round_position == 1) {
                this.groupList = [];
                for (let j = 0; j < res[0].json.data[i].group.length; j++) {
                  this.groupList.push({ "label": res[0].json.data[i].group[j].group_name, "value": { id: res[0].json.data[i].group[j].id, index: j } })
                }
                this.selectedGroup = this.groupList[0].value.id;
                this.groupIndex = this.groupList[0].value.index;
              }
            }
            this.no_rounds = false;
            this.is_fixture = false;
            this.API_getFixture(this.round_Details[0], this.round_Details[0].group);
            localStorage.removeItem('fb_image');
          }

        } else {
          this.loader = false;
        }
      },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', detail: 'error' });
        })
  }

  routing(x: boolean) {

    if (x) {
      if (this.paid) {
        localStorage.setItem("roundName", this.roundName);
        this.router.navigateByUrl('/dashboard/tournaments/create-fixture?id=' + this.roundID + '&group=' + this.roundType);
      }
      else {
        this.router.navigateByUrl('dashboard/payment/' + this.tournamentId + "?tournament=" + true + '&second=' + true);

      }
    }
    else {
      //if round is not created
      if (this.noRegTeamPopup) {
        localStorage.setItem('activeLink', 'roundLink');
        this.router.navigateByUrl('/dashboard/tournaments/round-details/' + this.tournamentId + '/create-round');
      } else {
        this.regTeamPopup = true;
      }

    }

  }
  routingForRound() {
    localStorage.setItem('activeLink', 'roundLink');
    this.router.navigateByUrl('/dashboard/tournaments/round-details/' + this.tournamentId + '/create-round');
  }
  groupRouting() {
    this.router.navigateByUrl('dashboard/tournaments/round-details/' + this.tournamentId + '/create-group?round_id=' + this.roundID + '&round_name=' + this.roundName);
  }

  selectedRoundFun(event) {
    this.API_getFixture(event.value, event.value.group);
  }

  API_getFixture(round, group) {
    this.loader = true;
    this.page = 1;
    this.roundType = round.round_type;
    this.roundID = round.id;
    localStorage.setItem("selectedRound", round.id);
    this.isResult = round.is_result_declared;
    this.DeSeFixtureDataInfo = [];
    this.GroupFixtureDataInfo = [];
    // *******rounds*****
    if (group == null) {
      // this.getGroupList()
      this.sortList = [];
      this.sortList.push({ "label": "Fixture", "value": "fixture" })
      this.sortList.push({ "label": localStorage.getItem('language') == 'en' ? "Date" : "Fecha", "value": "date" })
      if (localStorage.getItem('fixtureresult')) {
        console.log("fixtureresult fixtureresult")
        this.is_fixture = true;
        let obj = JSON.parse(localStorage.getItem('fixtureresult'));
        localStorage.removeItem('fixtureresult');
        this.selectedOption = obj.showFixtureType;
        this.selectedGroup = obj.showFixtureSubType;
        if (this.selectedOption === "date") {
          this.sortType = "date";
          this.check_date = true;
          this.check_team = false
          this.date = this.currentDate;
          this.groupType = true;
          this.gID = null;
          this.DeSeFixtureDataInfo = [];
          this.GroupFixtureDataInfo = [];
          this.getData(obj);
        }
        else {
          this.getFixture(round);
        }
      }
      else {
       this.getFixture(round);
      }
    } else {
      this.sortList = [];
      this.sortList.push({ "label": localStorage.getItem('language') == 'en' ? "Date" : "Fecha", "value": "date" })
      this.sortList.push({ "label": localStorage.getItem('language') == 'en' ? "Day" : "Dia", "value": "team" })
      this.sortList.push({ "label": localStorage.getItem('language') == 'en' ? "Group" : "Grupo", "value": "group" })
      this.groupType = true;
      this.dateType = false;
      this.groupID = group[0].id;
      this.gID = this.groupID;
      this.date = null;
      this.is_fixture = true;
      if (localStorage.getItem('fixtureresult')) {
        console.log("fixtureresult fixtureresult")
        let obj = JSON.parse(localStorage.getItem('fixtureresult'));
        localStorage.removeItem('fixtureresult');
        this.selectedOption = obj.showFixtureType;
        this.selectedGroup = obj.showFixtureSubType;
        if (this.selectedOption === "group") {
          this.groupType = true;
          this.check_team = false
          this.check_date = false;
          this.dateType = false;
          this.gID = this.groupID;
          this.sortType = "group";
          this.date = null;
          this.DeSeFixtureDataInfo = [];
          this.GroupFixtureDataInfo = [];
          this.getData(obj);
        } else if (this.selectedOption === "date") {
          this.sortType = "date";
          this.check_date = true;
          this.check_team = false
          this.date = this.currentDate;
          this.groupType = true;
          this.gID = null;
          this.DeSeFixtureDataInfo = [];
          this.GroupFixtureDataInfo = [];
          this.getData(obj);
        }
        else if (this.selectedOption === "team") {
          this.check_team = true;
          this.groupType = true;
          this.dateType = false
          this.DeSeFixtureDataInfo = [];
          this.GroupFixtureDataInfo = [];
          this.getData(obj);
        }
      }
      else {
        console.log("fixtureresult else else")
        this.selectedOption = "date";
        this.selectedGroup = "";
        this.sortType = "date";
        this.API_getSeDeFixtures();
        this.check_date = true;
      }

      // var url = this.base_path_service.base_path_api() + "tournament/tournamentRound/" + round.id + "/?group=" + this.groupID + "&sort=group&date=null&page=" + this.page + "&format=json";
      // this.base_path_service.GetRequest(url)
      //   .subscribe(res => {
      //     this.loader = false;
      //     if (res[0].status != 204) {
      //       this.total_pages = res[0].json.total_pages;
      //       this.is_fixture = res[0].json.is_fixture_created;
      //       if (res[0].json.data && res[0].json.data.length > 0) {
      //         this.GroupFixtureDataInfo.push(res[0].json.data);
      //       }
      //     }
      //   },
      //     err => {
      //       this.loader = false;
      //       this.msgs = [];
      //       this.msgs.push({ severity: 'error', detail: 'error' });
      //     })
    }
  }

  getFixture(round) {
    this.groupType = false;
    this.dateType = false;
    this.selectedOption = "fixture";
    this.sortType = "fixture";
    this.date = this.currentDate;
    this.gID = null;
    var url = this.base_path_service.base_path_api() + "tournament/tournamentRound/" + round.id + "/?group=null&sort=fixture&date=" + this.currentDate + "&page=" + this.page + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        if (res[0].status != 204) {
          this.total_pages = res[0].json.total_pages
          this.is_fixture = res[0].json.is_fixture_created;
          if (res[0].json.data && res[0].json.data.length > 0) {
            this.DeSeFixtureDataInfo.push(res[0].json.data);
          }
        }
      },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', detail: 'error' });
        })
  }

  getData(obj) {
    this.page = 1;
    // this.groupIndex = event.value.index;
    if (this.check_team) {
      this.ground_id = obj.round_id
      this.GroupFixtureDataInfo = [];
      this.DeSeFixtureDataInfo = [];
      this.getFixtureByTeam();
    }
    else if (this.check_date) {
      this.API_getSeDeFixtures();
    }
    else {
      let tempData = {
        value: this.selectedGroup,
        page: this.page
      }
      this.gID = obj.group_id;
      this.sortType = "group";
      this.date = null;
      this.GroupFixtureDataInfo = [];
      this.DeSeFixtureDataInfo = [];
      this.API_getGroupTypeFixture();
    }

  }

  // Edit Round Routing
  editRoundFun() {
    this.router.navigateByUrl('dashboard/tournaments/round-details/' + this.tournamentId + '/create-round')
  }
  editFixtureFun() {
    if (this.roundID != undefined && this.roundType != undefined)
      this.router.navigateByUrl('dashboard/tournaments/create-fixture?id=' + this.roundID + '&group=' + this.roundType);
  }

  selectedSortingOptionFun(event) {
    this.page = 1;
    this.selectedOption = event.value;
    if (event.value === "group") {
      this.groupType = true;
      this.check_team = false
      this.check_date = false;
      this.dateType = false;
      this.gID = this.groupID;
      this.sortType = "group";
      this.date = null;
      this.DeSeFixtureDataInfo = [];
      this.GroupFixtureDataInfo = [];
      this.API_getGroupTypeFixture();
    } else if (event.value === "date") {
      this.sortType = "date";
      this.check_date = true;
      this.check_team = false
      this.date = this.currentDate;
      this.groupType = true;
      this.gID = null;
      this.DeSeFixtureDataInfo = [];
      this.GroupFixtureDataInfo = [];
      this.selectedGroup = ''
      this.API_getSeDeFixtures();
    } else if (event.value === "result") {
      this.sortType = "result";
      this.check_team = false
      this.check_date = false;
      this.date = null;
      this.gID = null;
      this.dateType = false;
      this.groupList = [];
      this.groupType = false;
      this.DeSeFixtureDataInfo = [];
      this.GroupFixtureDataInfo = [];
      this.API_getGroupTypeFixture();
    }
    else if (event.value === "team") {
      this.check_team = true;
      this.groupType = true;
      this.dateType = false
      this.DeSeFixtureDataInfo = [];
      this.GroupFixtureDataInfo = [];
      this.getFixtureByTeam();
    }
    else {
      this.check_team = false
      this.check_date = false;
      this.sortType = "fixture";
      this.date = null;
      this.gID = null;
      this.groupList = [];
      this.dateType = false;
      this.groupType = false;
      this.DeSeFixtureDataInfo = [];
      this.GroupFixtureDataInfo = [];
      this.API_getGroupTypeFixture();
    }
  }

  selectedGroupFun(event) {
    this.page = 1;
    this.selectedGroup = event.value;
    this.groupIndex = event.value.index;
    if (this.check_team) {
      this.ground_id = event.value.id
      this.GroupFixtureDataInfo = [];
      this.DeSeFixtureDataInfo = [];
      this.getFixtureByTeam();
    }
    else if (this.check_date) {
      this.API_getSeDeFixtures();
    }
    else {
      let tempData = {
        value: this.selectedGroup,
        page: this.page
      }
      this.gID = event.value.id;
      this.sortType = "group";
      this.date = null;
      this.GroupFixtureDataInfo = [];
      this.DeSeFixtureDataInfo = [];
      this.API_getGroupTypeFixture();
    }

  }

  API_getGroupTypeFixture() {
    this.sub_group = true
    this.getGroupList();
    this.loader = true;
    var url = this.base_path_service.base_path_api() + "tournament/tournamentRound/" + this.roundID + "/?group=" + this.gID + "&sort=" + this.sortType + "&date=" + this.date + "&page=" + this.page + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        if (this.roundType == "group") {
          this.GroupFixtureDataInfo.push(res[0].json.data);
          this.total_pages = res[0].json.total_pages
        }
        if (this.roundType == "SE" || this.roundType == "DE") {
          this.DeSeFixtureDataInfo.push(res[0].json.data);
        }
      },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', detail: 'error' });
        })
  }

  previousGroup() {
    if (this.groupIndex > 0) {
      this.page = 1;
      this.GroupFixtureDataInfo = [];
      this.groupIndex = --this.groupIndex;
      if (this.check_team) {
        this.ground_id = this.groupList[this.groupIndex].value.id;
        this.selectedGroup = this.groupList[this.groupIndex].value;
        this.getFixtureByTeam();
      }
      else if (this.check_date) {
        this.selectedGroup = this.groupList[this.groupIndex].value;
        this.API_getSeDeFixtures();

      }
      else {
        for (let i = 0; i < this.groupList.length; i++) {
          if (i === this.groupIndex) {
            this.selectedGroup = this.groupList[i].value;
            this.gID = this.groupList[i].value.id;
            let tempData = {
              value: this.selectedGroup,
              page: this.page
            }
            this.API_getGroupTypeFixture();
          }
        }
      }
    }
  }

  nextGroup() {
    if (this.groupIndex < this.groupList.length - 1) {
      this.GroupFixtureDataInfo = [];
      this.page = 1;
      this.groupIndex = ++this.groupIndex;
      if (this.check_team) {
        this.ground_id = this.groupList[this.groupIndex].value.id;
        this.selectedGroup = this.groupList[this.groupIndex].value;
        this.getFixtureByTeam();
      }
      else if (this.check_date) {
        this.selectedGroup = this.groupList[this.groupIndex].value;
        this.API_getSeDeFixtures();
      }
      else {
        for (let i = 0; i < this.groupList.length; i++) {
          if (i === this.groupIndex) {
            this.selectedGroup = this.groupList[i].value;
            this.gID = this.groupList[i].value.id;
            let tempData = {
              value: this.selectedGroup,
              page: this.page
            }
            this.API_getGroupTypeFixture();
          }
        }
      }
    }
  }

  paginateGroupType(page) {
    this.page = page;
    let tempData = {
      value: this.selectedGroup,
      page: this.page
    }
    this.API_getGroupTypeFixture();
  }

  resultRouting(obj, check?) {
    let scheduleobj = {
      showFixtureType: this.selectedOption,
      showFixtureSubType: this.selectedGroup,
      group_id: this.gID,
      round_id: this.ground_id
    }
    localStorage.setItem("fixtureresult", JSON.stringify(scheduleobj));
    if (obj.is_result) {
      this.router.navigateByUrl('/dashboard/tournaments/results?fixtureId=' + obj.fixture_id + '&tourID=' + this.tournamentId);
    } else { }
  }
  public finalizeRound(roundId, form_type) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'tournament/completeRound/';
    let data = {
      round: roundId,
      form_type: form_type
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.API_getRoundDetails();
      }, err => {
        this.loader = false;
        this.base_path_service.print(err);
      })

  }
  public downloadPdf(fixture_id: number) {
    let url = this.base_path_service.base_path_api() + `tournament/teamplayerpdf/?fixture=${fixture_id}`;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
      }, err => {

      })
  }
  public nextRoundTeam(reset) {
    if (reset === 'reset') {
      this.selectedGroupForRound = null;
    }

    if (this.is_automatic == 'False') {
      this.nextRoundArray = [];
      this.nextRoundData = [];
      this.loader = true;
      this.nextRoundMessage = false;
      let url = this.base_path_service.base_path_api() + `tournament/manuallyTournamentFinalizeRound/${this.roundID}/?tournament=${this.tournamentId}&group=${this.selectedGroupForRound}&format=json`;
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          if (res[0].json.next_round == false) {
            this.nextRoundMessage = true;
          } else {
            this.nextRoundRegistartion = true;
          }
          this.loader = false;
          this.nextRoundData = res[0].json.data;
          this.nextRoundGroup = res[0].json.group;
          this.nextRoundStatus = res[0].json.next_round;
          this.nextRoundName = res[0].json.next_round_name;
          if (this.nextRoundData) {
            this.nextRoundArray = [];
            for (var index = 0; index < this.nextRoundData.length; index++) {
              if (this.nextRoundData[index].status) {
                this.nextRoundArray.push(this.nextRoundData[index].id);
              }
            }
          }
          if (!this.dropdownStatus || reset === 'reset') {
            this.dropdownStatus = false;
            this.nextRoundGroupList = [];
            if (this.nextRoundGroup) {
              for (var index = 0; index < this.nextRoundGroup.length; index++) {
                this.nextRoundGroupList.push({ label: this.nextRoundGroup[index].group_name, value: this.nextRoundGroup[index].id });
              }
              this.selectedGroupForRound = this.nextRoundGroupList[0].value;
            }

          }
        }, err => {
          this.loader = false;
        })
    } else {
      this.confirmationResult = true;
    }
  }
  public postNextRoundData() {
    let add = [];
    this.nextRoundArray.forEach(res => {
      if (res)
        add.push(res);
    })
    let data = {
      round: this.roundID,
      add: add,
      group: this.selectedGroupForRound
    }
    this.loader = false;
    let url = this.base_path_service.base_path_api() + `tournament/manuallyTournamentFinalizeRound/?format=json`;
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        if (!this.nextRoundGroup) {
          this.nextRoundRegistartion = false;
        }
        this.msgs = [];
        this.msgs.push({ severity: 'info', detail: 'Equipo para moverse en ' + this.nextRoundName });
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: 'error' });
      })
  }
  public selectedGroupFunForNextRound() {
    this.dropdownStatus = true;
    for (var index = 0; index < this.nextRoundGroupList.length; index++) {
      if (this.nextRoundGroupList[index].value == this.selectedGroupForRound) {
        this.groupCounter = index;
      }
    }
    this.nextRoundTeam('');
  }
  public previousGroupForNextRound() {
    this.dropdownStatus = true;
    var len = this.nextRoundGroupList.length;
    if (this.groupCounter >= 1) {
      this.groupCounter--;
      this.selectedGroupForRound = this.nextRoundGroupList[this.groupCounter].value;
      this.nextRoundTeam('');
    }

  }
  public nextGroupForNextRound() {
    this.dropdownStatus = true;
    var len = this.nextRoundGroupList.length;
    if (this.groupCounter < len - 1) {
      this.groupCounter++;
      this.selectedGroupForRound = this.nextRoundGroupList[this.groupCounter].value;
      this.nextRoundTeam('');
    }
  }
  public getGroupList() {
    let url = this.base_path_service.base_path_api() + "tournament/tournamentRound/?tournament=" + this.tournamentId + "&form_type=profile&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        if (res[0].status != 204) {
          for (let i = 0; i < res[0].json.data.length; i++) {
            if (res[0].json.data[i].group != null && res[0].json.data[i].round_position == this.round_position) {
              this.groupList = [];
              for (let j = 0; j < res[0].json.data[i].group.length; j++) {
                this.groupList.push({ "label": res[0].json.data[i].group[j].group_name, "value": { id: res[0].json.data[i].group[j].id, index: j } })
              }
              // this.selectedGroup = this.groupList[this.round_position - 1].value;
              // this.groupIndex = this.groupList[0].value.index;
            }

          }
        }
      }, err => {

      })
  }
  formatDate(event) {
    var validDate;
    let month = parseInt(event.getMonth());
    month = month + 1
    if (event.getMonth() > 8) {
      if (event.getDate() > 9) {
        validDate = event.getFullYear() + "-" + month + "-" + event.getDate();
      } else {
        validDate = event.getFullYear() + "-" + month + "-" + '0' + event.getDate();
      }

    } else {
      if (event.getDate() > 9) {
        validDate = event.getFullYear() + "-" + '0' + month + "-" + event.getDate();
      } else {
        validDate = event.getFullYear() + "-" + '0' + month + "-" + '0' + event.getDate();
      }
    }
    return validDate;
  }
  public pdfPreview(id: number) {

    this.loader = true;
    let url = this.base_path_service.base_path_api() + "tournament/teamplayerpdf/?fixture=" + id;
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.mylist = res[0].json;
      this.sponsers = this.mylist.sponsors;
      if (this.sponsers.length) {
        this.sponser_list_length = false;
      }
      else {
        this.sponser_list_length = true;
      }
      let player1_length = this.mylist.player_team1_json.player_team1_list.length;
      let player2_length = this.mylist.player_team2_json.player_team2_list.length;
      if (player1_length > player2_length) {
        for (let i = player2_length; i < player1_length; i++) {
          this.mylist.player_team2_json.player_team2_list.push({
            shirt_no: "-",
            goal: " ",
            name: " ",
            total_red_card: " ",
            total_yellow_card: " "
          })
        }
      }
      else if (player2_length > player1_length) {
        for (let i = player1_length; i < player2_length; i++) {
          this.mylist.player_team1_json.player_team1_list.push({
            shirt_no: "-",
            goal: " ",
            name: " ",
            total_red_card: " ",
            total_yellow_card: " "
          })
        }
      }
      this.pdfShow = true;
      this.loader = false;
    }, err => {
    })
  }


  downloadPdfBy() {
    const elementToPrint = this.canvasRef.nativeElement;
    html2pdf(elementToPrint, {
      margin: 0,
      filename: 'ploteando-fixture.pdf',
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: { dpi: 192, letterRendering: true },
      jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
    });
  }

  public tableDownload() {
    this.fixtureImageSharePopup = true;
    setTimeout(() => {
      this.downloadImageBy();
    }, 100)
  }
  public downloadImageBy() {

    const height = this.tablehieght.nativeElement.scrollHeight;
    const elementToPrint = this.layoutShare.nativeElement;
    elementToPrint.scrollTop = 0;
    html2canvas(elementToPrint, {
      height: height + 100,
      useCORS: true,
      // allowTaint: true,
      background: "white",
      overflow: "visible",
      onrendered: (canvas) => {
        this.imageUrlDownload = canvas.toDataURL("image/png");
      }
    });
  }
  public shareFixtureInfo() {
    this.loader = true;
    if (!this.groupID) {
      this.groupID = null;
    }
    let url = this.base_path_service.base_path_api() + 'tournament/shareResultDate/${this.roundID}/?group=${this.groupID}&sort=date&date=${this.date_info_fetcha}&page=1&format=json'
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.fixtureShareData = res[0].json;
        this.tableDownload();
        this.loader = false;
      }, err => {
        this.loader = false;
      })
  }

  getFixtureByTeam() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentRound/' + this.roundID + '/?sort=team_match&key_type=show&sub_group=' + this.ground_id + '&page=' + this.page + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        if (this.roundType == "group") {
          this.GroupFixtureDataInfo.push(res[0].json.data);
          this.total_pages = res[0].json.total_pages
          this.groupList = [];
          this.sub_group = res[0].json.success
          if (this.sub_group === undefined) {
            this.sub_group = true
          }
          for (let group = 0; group < res[0].json.sub_group.length; group++) {
            this.groupList.push({ "label": res[0].json.sub_group[group].name, "value": { id: res[0].json.sub_group[group].id, index: group } })
          }
        }
        if (this.roundType == "SE" || this.roundType == "DE") {
          this.DeSeFixtureDataInfo.push(res[0].json.data);
          this.total_pages = res[0].json.total_pages
        }

      },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', detail: 'error' });
        })
  }
}

