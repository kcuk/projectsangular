import { DOCUMENT } from '@angular/platform-browser';
import { Component, OnInit, HostListener, Inject, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';
import { SelectItem } from 'primeng/primeng'
import * as html2canvas from 'html2canvas';
declare const html2pdf: any;
declare const $: any;
@Component({
  selector: 'app-points',
  templateUrl: './points.component.html',
  styleUrls: ['./points.component.css']
})

export class PointsComponent implements OnInit {
  pointsDetails: Array<any> = [];
  isEmpty = true;
  tournament_id: any;
  imageUrl: any;
  loader: boolean = false;
  types: SelectItem[];
  groupList: SelectItem[];
  selectedTypes: String = 'table';
  status: String = "table";
  page: number = 1;
  totalPage: number = 0;
  scorerInfo: Array<any> = [];
  golesInfo: Array<any> = [];
  @ViewChild('layout') canvasRef;
  @ViewChild('tablehieght') tablehieght;

  public pdfShow: boolean = false;
  public tableInfo: any;
  public downloadBttnStatus: boolean = false;
  public imageUrlDownload: any;
  public groupSelect: boolean = false;
  public groupValue: string = "";
  public tableName: string = "";
  constructor(@Inject(DOCUMENT) public document: Document, public globalservice: GlobalService, public router: Router, public route: ActivatedRoute) { }
  ngOnInit() {

    this.types = [];
    this.types.push({ label: localStorage.getItem('language') == 'en' ? "Positions table" : 'Tabla De Posiciones', value: 'table' });
    this.types.push({ label: localStorage.getItem('language') == 'en' ? "Scorers" :'Goleadores', value: "goles" });
    this.types.push({ label: localStorage.getItem('language') == 'en' ? "Yellow cards" :'Tarjetas Amarillas', value: 'yellow_card' });
    this.types.push({ label:localStorage.getItem('language') == 'en' ? "Red cards" : 'Tarjetas Rojas', value: 'red_card' })
    this.tableName = localStorage.getItem('language') == 'en' ? "Positions table" :"Tabla De Posiciones";
    let id = this.route.parent.params.subscribe(params => {
      this.tournament_id = +params['id'];
    });

    this.globalservice.shareTwitterInfo = tempUrl;
    var tempUrl = window.location.href;
    this.imageUrl = this.globalservice.image_url;
    this.teamPointsDetails();

  }

  teamPointsDetails() {
    this.loader = true;
    let url = this.globalservice.base_path_api() + `tournament/result/${this.tournament_id}/`;
    this.globalservice.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.pointsDetails = res[0].json;
        this.globalservice.shareImageInfo = this.imageUrl + "/" + res[0].json.share_result_link;
        if (this.pointsDetails.length) {
          this.downloadBttnStatus = true;
        } else {
          this.downloadBttnStatus = false;
        }

      }, err => {
        this.loader = false;
      })
  }
  public topScorerView() {
    this.router.navigateByUrl('dashboard/topscorer/' + this.tournament_id)
  }
  onTypeChanged() {
    this.status = this.selectedTypes
  }

  public topScorerInfo(type) {
    this.loader = true;
    let url = this.globalservice.base_path_api() + 'tournament/tournamentTopPlayers/?tournament=' + this.tournament_id + "&form_type=" + type + "&page=" + this.page;
    this.globalservice.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        if (this.selectedTypes == 'yellow_card' || this.selectedTypes == 'red_card') {
          this.totalPage = res[0].json.total_num_of_pages;
          this.golesInfo = res[0].json.data;

          if (this.golesInfo.length) {
            this.downloadBttnStatus = true;
          } else {
            this.downloadBttnStatus = false;
          }
        } else {
          this.golesInfo = [];
          this.scorerInfo = res[0].json;
          if (this.scorerInfo.length) {
            this.downloadBttnStatus = true;
          } else {
            this.downloadBttnStatus = false;
          }
        }
      }, err => {
        this.loader = false;
      })
  }

  public changeStatus(event) {
    this.tableName = event.selectedOption.label;
    console.log("tbale name =>>>>>>>>>>>>>>>>>>>>>",this.tableName);
    console.log("tbale name =>>>>>>>>>>>>>>>>>>>>>",this.selectedTypes)
    if (this.selectedTypes == 'goles' || this.selectedTypes == 'yellow_card' || this.selectedTypes == 'red_card') {
      console.log("tbale name ifffffffffffffffff =>>>>>>>>>>>>>>>>>>>>>",this.selectedTypes)
      this.status = this.selectedTypes;
      this.pointsDetails = [];
      this.golesInfo = [];
      this.page = 1;
      this.topScorerInfo(this.selectedTypes);
    } else {
      console.log("tbale name elsee =>>>>>>>>>>>>>>>>>>>>>",this.selectedTypes)
      this.status = this.selectedTypes;
      this.scorerInfo = [];
      this.teamPointsDetails();
    }
  }
  @HostListener('window:scroll', ['$event'])
  scroll(event: any) {
    let number = this.document.body.scrollHeight;
    let totalNumber = window.pageYOffset + window.innerHeight;

    if (this.page <= this.totalPage) {
      if (this.selectedTypes == 'yellow_card' || this.selectedTypes == 'red_card') {
        if (number == totalNumber) {
          this.page = this.page + 1;
          if (this.page <= this.totalPage) {
            this.topScorerInfo(this.selectedTypes);
          }

        }
      }

    }
  }
  public tableInformation() {
    this.loader = true;
    let url;
    if (this.selectedTypes == 'table') {
      url = this.globalservice.base_path_api() + `tournament/resultPdf/${this.tournament_id}/?format=json`;
    } else {
      url = this.globalservice.base_path_api() + `tournament/tournamentTopPlayersPdf/?tournament=${this.tournament_id}&form_type=${this.selectedTypes}&format=json`
    }

    this.globalservice.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.tableInfo = res[0].json;
        if (this.selectedTypes == 'table') {
          this.groupSelect = true;
          let temp = this.tableInfo.data[0].group;
          this.groupList = []
          if (temp.length) {
            for (let index = 0; index < temp.length; index++) {
              this.groupList.push({ label: temp[index].group_name, value: temp[index].group_name });
            }
            this.groupValue = this.groupList[0].value;

          }

        } else {
          this.pdfShow = true;
          setTimeout(() => {
            this.downloadPdfBy();
          }, 100)
        }


      }, err => {
        this.loader = false;
      })
  }
  public tableDownload() {
    this.groupSelect = false;
    this.pdfShow = true;
    setTimeout(() => {
      this.downloadPdfBy();
    }, 100)
  }
  public downloadPdfBy() {

    const height = this.tablehieght.nativeElement.scrollHeight;
    const elementToPrint = this.canvasRef.nativeElement;
    elementToPrint.scrollTop = 0;
    html2canvas(elementToPrint, {
      height: height + 100,
      useCORS: true,
      // allowTaint: true,
      background: "white",
      overflow: "visible",
      onrendered: (canvas) => {
        this.imageUrlDownload = canvas.toDataURL("image/png");
      }
    });
    // html2pdf(elementToPrint, {
    //   margin: 0,
    //   filename: 'table.pdf',
    //   image: { type: 'jpeg', quality: 0.98 },
    //   html2canvas: { dpi: 192, letterRendering: true },
    //   jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
    // });

  }

}
