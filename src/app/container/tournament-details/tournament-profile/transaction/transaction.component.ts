import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
declare const html2pdf: any;

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {
  refund_id: any;
  show: boolean = true;
  tour_info: any;
  sponsers: any;
  result_pdf: any;
  pdf_popup: boolean;
  msgs: any[];
  loader: boolean;
  result: any;
  id: any;
  data_not_found: any;
  add_amount: boolean = false;
  payment_details: any;
  tour_id: any;
  show_add_bttn: boolean = false;
  refund_popup: boolean = false;
  payment_success_msg: boolean = false;
  @ViewChild('layout') canvasRef;

  constructor(public base_path_service: GlobalService, public fb: FormBuilder, public router: Router, public route: ActivatedRoute) {

  }

  ngOnInit() {
    this.route.params.subscribe(res => {
      this.id = +res['id'];
    })
    this.route.parent.params.subscribe(res => {
      this.tour_id = +res['id']
    })

    this.transactionList(this.id);
  }

  transactionList(id) {
    this.loader = true
    let url = this.base_path_service.base_path_api() + "tournament/transactionList/" + id + "/";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false
        this.result = res[0].json.data;
        this.show_add_bttn = res[0].json.is_paid;
        if (this.result.length == 0) {
          this.data_not_found = true;
        }
        else {
          this.data_not_found = false;
        }
      }, err => {
        this.loader = false;
      })
  }

  addAmount() {
    this.loader = true
    let url = this.base_path_service.base_path_api() + "tournament/addAmount/?register_id=" + this.id;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false
        this.payment_details = res[0].json;
        this.add_amount = true;
      }, err => {
        this.loader = false;
      })
  }

  receiveAmount() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "tournament/addAmount/";
    let data = {
      "tournament": this.tour_id,
      "team_id": this.id,
      "amount": this.payment_details.amount
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.add_amount = false;
        this.msgs = [];
        this.msgs.push({ severity: 'success', detail: res[0].json.message });
        this.transactionList(this.id);
        console.log("response=>>>>>>>>>>>>>>>>", res[0].json)
      })
  }


  public pdfPreview() {
    this.loader = true
    let url = this.base_path_service.base_path_api() + "tournament/registrationPdf/" + this.id + "/";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false
        this.result_pdf = res[0].json.data;
        this.sponsers = res[0].json.sponsors;
        if (this.sponsers.length == 0) {
          this.show = false;
        }
        console.log("sponser=>>>>>>>>>", this.sponsers)
        this.tour_info = res[0].json
        this.pdf_popup = true;
      }, err => {
        this.loader = false;
      })
  }



  downloadPdfBy() {
    const elementToPrint = this.canvasRef.nativeElement;
    html2pdf(elementToPrint, {
      margin: 0,
      filename: 'Tournament_transaction.pdf',
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: { dpi: 192, letterRendering: true },
      jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
    });
  }

  refund(id) {
    this.refund_id = id;
    this.refund_popup = true;

  }

  refunded() {
    this.loader = true
    let url = this.base_path_service.base_path_api() + "user/refundAmount/?form_type=team&trans_id=" + this.refund_id;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.payment_success_msg = true;
        this.transactionList(this.id)
        this.msgs = [];
        this.msgs.push({ severity: 'success', detail: 'Refund Successful!' });

      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: 'Refund Unsuccessful!' });
      })
  }

  xyz() {

  }
}
