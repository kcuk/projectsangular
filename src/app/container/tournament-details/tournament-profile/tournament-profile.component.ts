
import { ViewChild, Component, OnInit } from '@angular/core';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from './../../../GlobalService';
import { Message } from 'primeng/primeng';
import * as html2canvas from 'html2canvas';
import { TranslateService } from "ng2-translate";
declare const FB: any;
declare const $: any;
@Component({
  selector: 'app-tournament-profile',
  templateUrl: './tournament-profile.component.html',
  styleUrls: ['./tournament-profile.component.css'],
  outputs: ['myevent']
})
export class TournamentProfileComponent implements OnInit {
  loader1: boolean;
  Id: any;
  is_paid: any;
  notfound: any;
  admin_list: any;
  court_list: any;
  public sub: any;
  info: boolean = true;
  counter: any;
  public id: string;
  public tourObj: any;
  public teams: any;
  public information_Details: Array<any> = [];
  public info_best_player: Array<any> = [];
  basePath: string = ''
  profile_pic;
  cover_pic;
  rankingList;
  loader: boolean = false;
  registerPopup: boolean = false;
  admin: boolean = false;
  abc: boolean = false;
  regDetails: Array<any> = []
  DisableRegBttn: boolean = false;
  activeClass: boolean = false;
  teamID: any;
  msgs: any[] = [];
  backdrop: boolean = false;
  display: boolean = false;
  enable: boolean = false;
  logo: File;
  data: any;
  data2: any;
  cropperSettings: CropperSettings;
  cropperSettings2: CropperSettings;
  basicInfo: any;
  invitePopup: boolean = false;
  refereeList: any;
  refereeFilteredList: Array<any> = [];
  followersPopup: any;
  followersList: Array<any>;
  organization_logo: any;
  is_sponsor: any;
  addListOptions: any;
  selectedAddListOption: any;
  groundList: Array<any> = [];
  equipoList: Array<any> = [];
  is_referee: boolean = false;
  registerOptions: Array<any> = [];
  groundFilteredList: any;
  equipoFilteredList: any;
  registerRefereePopup: boolean = false;
  selectedRegisterOption: any;
  openEnrollment: boolean = false;
  cover_url: any;
  messagePopup: boolean = false
  @ViewChild('cropper', undefined) cropper: ImageCropperComponent;
  @ViewChild('message') message: any;
  @ViewChild('layout') canvasRef;
  cover: boolean = false;
  @ViewChild('auto') auto: any;
  tour: string = "";
  req: boolean = false;
  url: string = "";
  twitterUrl: string = "";
  imageUrl: string = '';
  currentRouting: string = '';
  shareimageUrl: string = '';
  share_image_Url: string = "";
  public destroy: any;
  downloadProfile: boolean = false;
  downloadDataInfo: any;
  imageUrlDownload: string = "";
  public attitude: number = 0;
  public techniq: number = 0;
  payment_success_msg: any = false;
  payment_failed_msg: any = false;
  court_popup: boolean = false;
  payment_cash: any = false;
  add_admin: boolean = false;
  addmin_list_option: any;
  selected_admin: any;
  follower_list: any;
  no_of_admins: any;
  search_value: any = "";
  payment_team: boolean = false;

  constructor(private translate: TranslateService, public base_path_service: GlobalService, public route: ActivatedRoute, public router: Router) {
    this.rectangleCropImage();
    this.circleCropImage();
    if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
    }
    let url = window.location.href
    this.route.queryParams.subscribe(param => {
      let status = param['status'];
      if (status != undefined) {
        this.loader1 = true;

        if (status === '1') {
          this.loader1 = false;
          this.payment_success_msg = true;
        }
        else if (status === '2') {
          this.loader1 = false;
          this.payment_failed_msg = true;
        }

        else if (status === '3') {
          this.loader1 = false;
          this.payment_cash = true;
        }
        // this.loader1 = true;
        // if (status === '1') {
        //   let redirect = url.replace('status=1', 'status=true');
        //   window.parent.location.href = redirect;
        // }
        // else if (status === '2') {
        //   let redirect = url.replace('status=2', 'status=false');
        //   window.parent.location.href = redirect;
        // }
        // if (status === 'true') {
        //   this.loader1 = false;
        //   this.payment_success_msg = true;
        // }
        // else if (status === 'false') {
        //   this.loader1 = false;
        //   this.payment_failed_msg = true;
        // }
        // if (status === '11') {
        //   this.loader1 = false;
        //   this.payment_success_msg = true;
        // }
        // else if (status === '12') {
        //   this.loader1 = false;
        //   this.payment_failed_msg = true;
        // }
        // else if (status === '3') {
        //   this.loader1 = false;
        //   this.payment_cash = true;
        // }
      }
    })
  }
  ngOnDestroy() {
    this.destroy.unsubscribe();
  }

  ngOnInit() {
    // document.body.scrollTop = 100;
    // document.body.click();
    this.addmin_list_option = [
      {
        "label":localStorage.getItem('language')=='en'? "Admin":"Admin",
        "value": "admin"
      }, {
        "label":localStorage.getItem('language')=='en'? "Followers":"seguidor",
        "value": "follower"
      }
    ]
    document.body.scrollTop = 0;
    this.imageUrl = this.base_path_service.image_url;
    this.id = this.route.snapshot.params['id'];
    var tempUrl = window.location.href;
    var arrUrl = tempUrl.split('/');
    var temp = arrUrl[arrUrl.length - 1].split('?');
    var currentUrl = temp[0];
    if (currentUrl == 'request' || currentUrl == 'tournament_payment' || currentUrl == 'posesiones' || currentUrl == 'schedule' || currentUrl == 'points' || currentUrl == 'goleadores') {
      this.info = false;
    }
    localStorage.setItem("tid", this.id);
    this.basePath = this.base_path_service.image_url;
    this.counter = 0;
    this.destroy = this.base_path_service.pageChange.subscribe(res => {
      this.id = res[3];
      this.info = false;
      this.API_getTournamentBasicInfo();
    })
    this.API_getTournamentBasicInfo();
    this.addListOptions = [
      {
        "label":localStorage.getItem('language')=='en'?"Refree": "Árbitro",
        "value": "Árbitro"
      }, {
        "label": localStorage.getItem('language')=='en'?"Fields":"Canchas",
        "value": "Cancha"
      }, {
        "label": localStorage.getItem('language')=='en'?"Teams":"Equipos",
        "value": "Equipos"
      }
    ]

  }


  API_getTournamentBasicInfo() {
    console.log("methoddddddddddddddddd", this.id)

    this.registerOptions = [];
    this.registerOptions.push({ "label": localStorage.getItem('language') == 'en' ? "ENROLL" : "INSCRIBIR", "value": "INSCRIBIR" });
    var url = this.base_path_service.base_path_api() + "tournament/tournamentDetail/" + this.id + "/?form_type=basic&format=json";
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.admin = res[0].json.admin;
      this.no_of_admins = res[0].json.no_of_admin;
      this.is_paid = res[0].json.is_paid;
      this.basicInfo = res[0].json;
      this.shareimageUrl = this.imageUrl + res[0].json.share_tournament_link;
      this.base_path_service.shareImageInfo = this.imageUrl + "/" + res[0].json.share_tournament_link;
      console.log("hello ", this.base_path_service.shareImageInfo)
      let endDate = res[0].json.registration_end_date;
      let date1 = new Date().getTime();
      let date2 = new Date(endDate).getTime();
      if (date1 < date2) {
        this.openEnrollment = true;
      }
      this.cover_pic = res[0].json.cover_pic
      this.cover_url = this.base_path_service.image_url + res[0].json.cover_pic;
      if (res[0].json.is_referee) {
        this
          .registerOptions
          .push({ "label": localStorage.getItem('language') == 'en' ? "REFEREE" : "ÁRBITRO", "value": "referee" });
        this
          .registerOptions
          .push({ "label": localStorage.getItem('language') == 'en' ? "Team" : "EQUIPO", "value": "team" })
      } else {
        this
          .registerOptions
          .push({ "label":  localStorage.getItem('language') == 'en' ? "Team" :"EQUIPO", "value": "team" })
      }
      if (this.basicInfo.sponsor.length > 0) {
        this.is_sponsor = true;
      } else {
        this.is_sponsor = false;
      }
      if (res[0].json.organization_logo == null || res[0].json.organization_logo == "media/null") {
        this.organization_logo = null;
      } else {
        this.organization_logo = res[0].json.organization_logo;
      }
      if (res[0].json.logo == null || res[0].json.logo == "media/null") {
        this.profile_pic = null;
      } else {
        this.profile_pic = res[0].json.logo;
      }

    }, err => {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
    })
  }

  selectedRegisterOptionFun(event) {
    this.regDetails = [];
    if (event.value == "referee") {
      this.registerRefereePopup = true;
    } else if (event.value == "team") {
      this.teamID = null;
      this.registerPopup = true;
      this.regPopupDetails();
    }

  }

  registerReferee() {
    this.loader = true;
    let obj = {
      "tournament": parseInt(this.id)
    }
    let url = this.base_path_service.base_path_api() + "tournament/refereeRequest/?format=json";
    this.base_path_service.PostRequest(url, obj).subscribe(res => {
      this.loader = false
      this.registerRefereePopup = false;
      this.backdrop = false;
      this.msgs = [];
      this.msgs.push({ severity: 'info', summary: res[0].json, detail: '' });
    }, err => {
      this.registerRefereePopup = false;
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
    })
  }

  editTournamentFun() {
    this.loader = true;
    localStorage.setItem('activeLink', 'editTournament');
    localStorage.setItem('tournament_name', this.basicInfo.tournament_name);
    this.router.navigateByUrl('/dashboard/tournaments/round-details/' + this.id + '/edit-tournament');
  }

  /**** Registration Popup Functionality ****/
  registerPopupWindow() {
    this.teamID = null;
    this.backdrop = true;
    this.registerPopup = true;
    this.regPopupDetails();
    // this.getTeam();
  }

  regPopupDetails() {
    this.loader = true;
    var url = this.base_path_service.base_path_api() + "team/profile/?value=myteam&tournament=" + this.id + "&format=json";
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.loader = false;
      this.regDetails = res[0].json.teams;
      if (this.regDetails.length == 0) {
        this.DisableRegBttn = true;
      }
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
    })
  }

  regDetailsData() {
    this.registerPopup = false;
    this.loader = true;
    let regJson = {
      tournament: parseInt(this.id),
      team: this.teamID
    }
    let url = this.base_path_service.base_path_api() + "tournament/teamRegistration/?format=json";
    this.base_path_service.PostRequest(url, regJson).subscribe(res => {
      this.backdrop = false;
      this.selectedRegisterOption = "INSCRIBIR"
      // let data = {
      //   'payment_info': res[0].json
      // }
      // localStorage.setItem('team_payment', JSON.stringify(data))
      // this.router.navigateByUrl('dashboard/payment-option/' + parseInt(this.id) + '?team_id=' + this.teamID + "&team=" + true);
      if (!this.admin) {
        let url = this.base_path_service.base_path_api() + 'tournament/registerTeamPay/';
        let data = {
          tournament_id: parseInt(this.id),
          team_id: this.teamID,
          request_type: 'cash',
          registration_id: res[0].json.team_registration_id
        }
        this.base_path_service.PostRequest(url, data)
          .subscribe(res => {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'success', detail: 'Request sent' });            // this.router.navigateByUrl('dashboard/tournaments/tournament-profile/' + this.id + '?status=3');
          }, err => {
            this.loader = false;
          })
      }
      else {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'success', detail: 'Team added successfully' });
      }
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: err.json(), detail: 'Error' });
    })
  }

  regPopupClose() {
    this.selectedRegisterOption = "INSCRIBIR"
    this.registerPopup = false;
    this.backdrop = false;
  }

  activeDivColor(id) {
    this.teamID = id;
  }

  /**** Registration Popup Functionality ****/

  /*** followers btn***/
  followers() {
    this.router.navigateByUrl('dashboard/followers?id=' + this.id + '&status=tournament')
  }
  /*** followers btn***/

  follow() {
    this.loader = true;
    let obj = {
      "tournament": this.id
    }

    var url = this.base_path_service.base_path_api() + "user/followTournament/?format=json";
    this.base_path_service.PostRequest(url, obj).subscribe(res => {
      this.loader = false;
      this.API_getTournamentBasicInfo();
    }, err => {
      this.loader = false;
      this.msgs = [];
      this
        .msgs
        .push({
          severity: 'error',
          summary: err.json(),
          detail: ''
        });
    })
  }

  unfollow() {
    this.loader = true;
    var url = this.base_path_service.base_path_api() + "user/followTournament/" + this.id + "/?format=json";
    this.base_path_service.DeleteRequest(url).subscribe(res => {
      this.loader = false;
      this.API_getTournamentBasicInfo();
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
    })
  }

  /*** Invitation Poup code****/
  showInvitationPopup() {
    this.selectedAddListOption = "Árbitro";
    if (this.refereeFilteredList.length > 0) {
      this.auto.value = "";
    }

    this.invitePopup = true;
    this.API_getRefereeList();
  }

  API_getRefereeList() {
    this.loader = true;
    var url = this.base_path_service.base_path_api() + "user/profile/?form_type=referee&tournament=" + this.id + "&format=json";
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.loader = false;
      // this.base_path_service.print(res[0].json, "referee");
      this.refereeList = res[0].json.data;
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
    })
  }

  getRefereeList(event) {
    let query = event.query;
    if (event.query.length > 1) {
      this.loader = true;
      var url = this.base_path_service.base_path_api() + "user/profile/?form_type=referee&value=" + event.query + "&tournament=" + this.id + "&format=json";
      this.base_path_service.GetRequest(url).subscribe(res => {
        this.loader = false;
        this.refereeFilteredList = res[0].json.data;
        this.refereeList = [];
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
      });
    }
  }

  getReferee(event) {
    this.refereeList.push(event);
  }

  API_getGroundList() {
    var url = this.base_path_service.base_path_api() + "ground/filterGround/?tournament=" + this.id + "&format=json";
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.loader = false;
      this.groundList = res[0].json.data;
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({
        severity: 'error', summary: err.json(), detail: ''
      });
    })
  }

  getGroundList(event) {
    let query = event.query;
    if (event.query.length > 1) {
      this.loader = true;
      var url = this.base_path_service.base_path_api() + "ground/filterGround/?tournament=" + this.id + "&value=" + event.query + "&format=json";
      this.base_path_service.GetRequest(url).subscribe(res => {
        this.loader = false;
        this.groundList = [];
        this.groundFilteredList = res[0].json.data;
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
      });
    }
  }

  getGround(event) {
    this.groundList.push(event);
  }

  addReferee(referee_id) {
    this.loader = true;
    let obj = {
      "referee": referee_id,
      "tournament": parseInt(this.id)
    }
    let url = this
      .base_path_service
      .base_path_api() + "tournament/referee/?format=json"
    this.base_path_service.PostRequest(url, obj).subscribe(res => {
      this.loader = false;
      if (res[0].status == 201) {
        this.msgs = [];
        this.msgs.push({ severity: 'info', summary: res[0].json, detail: '' });
        this.API_getRefereeList();
      }
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
    })
  }

  addGround(ground_id) {
    this.court_popup = true;
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "ground/filterCourt/?tournament=" + this.id + "&ground=" + ground_id + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.court_list = res[0].json;
        console.log("court=>>>>>>>>>>>>>>>>>>>>>>>.", this.court_list)
      }, err => {
        this.loader = false;
      })

  }

  select_court(court_id) {
    this.loader = true;
    let obj = {
      "ground": court_id,
      "tournament": parseInt(this.id)
    }
    let url = this.base_path_service.base_path_api() + "tournament/ground/?format=json"
    this.base_path_service.PostRequest(url, obj).subscribe(res => {
      this.loader = false;
      if (res[0].status == 201) {
        this.msgs = [];
        this.court_popup = false;
        this.msgs.push({ severity: 'info', summary: res[0].json, detail: '' });
        this.API_getGroundList();
      }
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
    })
  }

  selectedAddListOptionFun(event) {
    this.selectedAddListOption = event.value;
    if (this.selectedAddListOption == "Árbitro") {
      this.API_getRefereeList();
    } else if (this.selectedAddListOption == "Cancha") {
      this.API_getGroundList();
    } else {
      this.API_getEquipoList();
    }
  }
  API_getEquipoList() {
    var url = this.base_path_service.base_path_api() + "team/team_search/?value=&tournament=" + this.id + "&format=json";
    this.base_path_service.GetRequest(url).subscribe(res => {
      this.loader = false;
      this.equipoList = res[0].json;
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
    })

  }
  getEquipoList(event) {
    let query = event.query;
    if (event.query.length > 1) {
      this.loader = true;
      var url = this.base_path_service.base_path_api() + "team/team_search/?value=" + event.query + "&tournament=" + this.id + "&format=json";
      this.base_path_service.GetRequest(url).subscribe(res => {
        this.loader = false;
        this.equipoList = [];
        this.equipoFilteredList = res[0].json;
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
      });
    }
  }
  getEquipo(event) {
    this.equipoList.push(event);
  }

  addEquipo(team_id) {
    this.loader = true;
    let obj = {
      "team": team_id,
      "tournament": parseInt(this.id)
    }
    let url = this.base_path_service.base_path_api() + "tournament/team_request/?format=json"
    this.base_path_service.PostRequest(url, obj).subscribe(res => {
      this.loader = false;
      if (res[0].status == 201) {
        this.msgs = [];
        this.msgs.push({ severity: 'info', summary: 'solicitud enviada', detail: '' });
        this.API_getEquipoList();
      }
    }, err => {
      this.loader = false;
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
    })
  }



  rectangleCropImage() {

    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 1200;
    this.cropperSettings.height = 250;
    this.cropperSettings.keepAspect = false;

    this.cropperSettings.croppedWidth = 1200;
    this.cropperSettings.croppedHeight = 250;

    this.cropperSettings.canvasWidth = 350;
    this.cropperSettings.canvasHeight = 300;

    this.cropperSettings.minWidth = 100;
    this.cropperSettings.minHeight = 100;

    this.cropperSettings.rounded = false;
    this.cropperSettings.minWithRelativeToResolution = false;

    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings.noFileInput = true;

    this.data = {};
  }

  circleCropImage() {

    this.cropperSettings2 = new CropperSettings();
    this.cropperSettings2.width = 200;
    this.cropperSettings2.height = 200;
    this.cropperSettings2.keepAspect = false;

    this.cropperSettings2.croppedWidth = 200;
    this.cropperSettings2.croppedHeight = 200;

    this.cropperSettings2.canvasWidth = 350;
    this.cropperSettings2.canvasHeight = 300;

    this.cropperSettings2.minWidth = 100;
    this.cropperSettings2.minHeight = 100;

    this.cropperSettings2.rounded = true;
    this.cropperSettings2.minWithRelativeToResolution = true;

    this.cropperSettings2.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings2.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings2.noFileInput = true;

    this.data2 = {};
  }

  fileChangeListener(event, type?: string) {

    if (type == "cover") {
      this.cover = true;
    } else {
      this.cover = false;
    }

    var image: any = new Image();
    var file: File = event.target.files[0];
    var myReader: FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
      if (that.cover == false) {
        that.logo = image.src.split(",")[1];
      } else {
        that.logo = image.src.split(",")[1];
      }
    };
    myReader.readAsDataURL(file);
    this.showDialog();
  }


  showDialog() {
    this.display = true;
  }

  save() {
    this.loader = true;

    if (this.cover == false) {
      let obj = JSON.parse(localStorage.getItem("userInfo")).token.access_token;
      let url = this.base_path_service.base_path_api() + 'tournament/image/' + this.id + '/';
      return new Promise((resolve, reject) => {
        var formData: any = new FormData();
        var xhr = new XMLHttpRequest();
        formData.append("logo", this.logo);
        xhr.onreadystatechange = () => {
          if (xhr.readyState == 4) {
            this.loader = false;
            if (xhr.status == 201) {
              this.msgs = [];
              this.msgs.push({ severity: 'info', summary: 'Actualizado Exitosamente', detail: '' });
              this.profile_pic = JSON.parse(xhr.responseText)['logo'];
              this.backdrop = false;
              this.display = false;
            } else {
              reject(xhr.response);
            }
          }
        }
        xhr.open("PUT", url, true);
        xhr.setRequestHeader("Authorization", 'Bearer ' + obj);
        xhr.send(formData);
      });
    } else {
      let obj = JSON.parse(localStorage.getItem("userInfo")).token.access_token;
      let url = this.base_path_service.base_path_api() + 'tournament/image/' + this.id + '/';
      return new Promise((resolve, reject) => {
        var formData: any = new FormData();
        var xhr = new XMLHttpRequest();
        formData.append("cover_pic", this.logo);
        xhr.onreadystatechange = () => {
          if (xhr.readyState == 4) {
            this.loader = false;
            if (xhr.status == 201) {
              this.msgs = [];
              this.msgs.push({ severity: 'info', summary: 'Actualizado Exitosamente', detail: '' });
              this.cover_pic = JSON.parse(xhr.responseText)['cover_pic'];
              this.backdrop = false;
              this.display = false;
            } else {
              reject(xhr.response);
            }
          }
        }
        xhr.open("PUT", url, true);
        xhr.setRequestHeader("Authorization", 'Bearer ' + obj);
        xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
        xhr.send(formData);
      });
    }

  }

  disable() {
    this.backdrop = false;
    this.display = false;
  }

  remove_img($event) {
  }

  /*** image cropper ***/

  /*** Message Popup ***/

  showMessagePopup() {
    this.messagePopup = true;
    this.message.value = "";
  }

  sentMessage(message) {
    this.loader = true;

    let obj = {
      "form_type": "p2tour",
      "message": message,
      "tournament": parseInt(this.id)
    }

    let url = this.base_path_service.base_path_api() + "user/message/";
    this.base_path_service.PostRequest(url, obj).subscribe(res => {
      this.loader = false;
      this.messagePopup = false;
      this.backdrop = false;
      this.msgs = [];
      this.msgs.push({ severity: 'info', summary: res[0].json, detail: '' });
    }, err => {
      this.loader = false;
      this.messagePopup = false;
      this.backdrop = false;
      this.msgs = [];
      this.msgs.push({ severity: 'info', summary: err.json(), detail: '' });
    })

  }
  /*** Message Popup ***/

  public equipo_List(event) {
    if (event.target.value == '') {
      this.API_getEquipoList();
    }
  }
  public ground_List(event) {
    if (event.target.value == '') {
      this.API_getGroundList();
    }
  }
  public referee_List(event) {
    if (event.target.value == '') {
      this.API_getRefereeList();
    }
  }
  downloadImage() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + `tournament/tournamentProfile/?id=${this.id}&format=json`;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.downloadDataInfo = res[0].json;
        this.attitude = res[0].json.organization_avg;
        this.techniq = res[0].json.fairness_avg;
        this.downloadProfile = true;
        setTimeout(() => {
          this.drawImage();
        }, 1000)

      }, err => {
        this.loader = false;

      })
  }
  downloadAsImage() {
    this.downloadImage();
  }

  drawImage() {
    let canvas = this.canvasRef.nativeElement;
    html2canvas(canvas, {
      // proxy:"https://football.innotical.com/",
      // useCORS: true,
      // allowTaint:true,
      onrendered: (canvas) => {
        this.imageUrlDownload = canvas.toDataURL("image/png");
      }
    })

  }


  addAdmin() {
    this.selected_admin = 'admin';
    let url = this.base_path_service.base_path_api() + "tournament/followList/?tournament=" + this.id + "&request_type=admin";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.admin_list = res[0].json;
        if (this.admin_list.length == 0) {
          this.notfound = true
        }
        else {
          this.notfound = false
        }
        this.add_admin = true;
      })
  }

  selectedAdminFun(event) {
    this.loader = true;
    if (event.value === undefined) {
      this.selected_admin = event
    }
    else (
      this.selected_admin = event.value
    )
    if (this.selected_admin === 'follower') {
      let url = this.base_path_service.base_path_api() + "tournament/followList/?tournament=" + this.id + "&request_type=follower";
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          this.loader = false;
          this.follower_list = res[0].json;
        })
    }
    else if (this.selected_admin === 'admin') {
      this.adminList();
    }
  }
  adminList() {
    let url = this.base_path_service.base_path_api() + "tournament/followList/?tournament=" + this.id + "&request_type=admin";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.admin_list = res[0].json;
      })
  }
  addAdminInTour(id) {
    console.log("iddddddddddddddddddddddddddd", id)
  }
  addFollowers(id, tour?) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "tournament/addAdmin/";
    let data = {
      "tournament_id": this.id,
      "user_id": id
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'success', detail: res[0].json.message });
        if (tour == 'tour') {
          this.adminList()
        }
        else {
          this.selectedAdminFun('follower')
        }
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: err.json().error });
      })

  }

  removeAdmin(id) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "tournament/removeAdmin/";
    let data = {
      "tournament": this.id,
      "admin": id
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'success', detail: res[0].json.message });
        this.adminList();
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: err.json().error });
      })

  }

  getSearchAdmin(event) {
    let url = this.base_path_service.base_path_api() + "tournament/searchList/";
    let data = {
      "tournament": this.id,
      "key": event.query,
      "request_type": this.selected_admin
    }
    if (this.selected_admin === 'admin') {
      this.base_path_service.PostRequest(url, data)
        .subscribe(res => {
          this.admin_list = [];
          this.admin_list = res[0].json;
          if (this.admin_list.length == 0) {
            this.notfound = true;
          }
        })
    }
    else if (this.selected_admin === 'follower') {
      this.base_path_service.PostRequest(url, data)
        .subscribe(res => {
          this.follower_list = [];
          this.follower_list = res[0].json;
          if (this.admin_list.length == 0) {
            this.notfound = true;
          }
        })

    }

  }

  getData(event, check) {
    if (event.target.value === '') {
      this.selectedAdminFun(check);
    }
  }

  packageSelection() {

    let url = this.base_path_service.base_path_api() + "tournament/tournamentData/?tournament_id=" + this.id;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false
        let tournament = {
          tounament_info: res[0].json
        }
        localStorage.setItem('tournament', JSON.stringify(tournament));
        this.router.navigateByUrl('dashboard/payment/' + this.id + "?tournament=" + true);
      }, err => {
        this.loader = false
      })
    // this.router.navigateByUrl('/dashboard/package/' + this.id + '?status=tournament');
  }

}