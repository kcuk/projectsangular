import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../../../GlobalService';
import { Message } from 'primeng/primeng';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  invitesSent: boolean = false;
  public activeLink: boolean = true;
  public loader: boolean = false;
  public tournamentID: any;
  public teams: Array<any> = [];
  public request: boolean;
  msgs: Message[] = [];
  public basePath;
  flag: boolean;
  admin: any;
  actionOptions: Array<any> = [];
  selectedAction: any;
  refereeList;
  teamList: any;
  action: any;
  option: any;
  referee_id: any;
  team_registrationID: any;
  selectOption: any;
  public groundList: Array<any> = [];

  constructor(public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute) {
    console.log("request=>>>>>>>>>>>>>>>>>>>>>>>>")
  }

  ngOnInit() {
    let id = this.route.parent.params.subscribe(params => {
      this.tournamentID = +params['id'];
    });
    this.basePath = this.base_path_service.image_url;
    this.actionOptions.push({ "label": localStorage.getItem('language') == 'en' ? 'Action' : "Acción", "value": "action" });
    this.actionOptions.push({ "label": localStorage.getItem('language') == 'en' ? 'Accept' : "Aceptar", "value": "accept" });
    this.actionOptions.push({ "label": localStorage.getItem('language') == 'en' ? 'Deny' : "Negar", "value": "reject" })
    this.selectOption = "action";
    /*Calling Function on Load as Requested*/
    this.received();
  }


  sent() {
    this.loader = true;
    this.refereeList = [];
    this.teamList = [];
    this.activeLink = true;
    this.flag = true;
    let url = this.base_path_service.base_path_api() + "tournament/teamRegistration/" + this.tournamentID + "/?option=invite";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.base_path_service.print(res[0].json, "sent requests")
        this.refereeList = res[0].json.referee_data;
        this.teamList = res[0].json.team_data;
        this.groundList = res[0].json.ground_data;
      },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: err.json(), detail: "" });
        })

  }
  received() {
    this.loader = true;
    this.refereeList = [];
    this.teamList = [];
    this.activeLink = false;
    this.flag = false;
    let url = this.base_path_service.base_path_api() + "tournament/teamRegistration/" + this.tournamentID + "/?option=requested";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.base_path_service.print(res[0].json, "received requests")
        this.teamList = res[0].json.team_data;
        this.refereeList = res[0].json.referee_data
      },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: err.json(), detail: "" });
        })
  }



  selectedActionFun(event, id, status) {
    if (event.value == "accept") {
      this.action = "approve"
    } else if (event.value == "reject") {
      this.action = "deny"
    }
    if (status == "team") {
      this.option = "team"
      this.team_registrationID = id;
    } else {
      this.option = "referee"
      this.referee_id = id;
    }
    this.API_requestAction();
  }

  cancelRefereeRequest(referee_id) {
    this.referee_id = referee_id;
    this.action = "delete";
    this.option = "referee";
    this.API_requestAction();
  }

  API_requestAction() {
    this.loader = true;
    let obj = {
      "tournament": this.tournamentID,
      "action": this.action,
      "option": this.option
    }

    if (this.option == "referee") {
      var url = this.base_path_service.base_path_api() + "tournament/teamRegistration/" + this.referee_id + "/";
    } else if (this.option == "team") {
      var url = this.base_path_service.base_path_api() + "tournament/teamRegistration/" + this.team_registrationID + "/";
    }
    this.base_path_service.PutRequest(url, obj)
      .subscribe(res => {
        if (res[0].status == 205) {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: res[0].json.json(), detail: "" });
          if (this.activeLink == true) {
            this.sent();
          } else {
            this.received();
          }
        }
      },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: err.json(), detail: "" });
        })
  }

  public cancelTeamRequest(id: number) {
    var url = this.base_path_service.base_path_api() + "tournament/teamRegistration/" + id + '/';
    var data = {
      "option": "team",
      "action": "deny",
      "tournament": this.tournamentID
    }
    this.base_path_service.PutRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.received()

        this.msgs.push({ severity: 'info', detail: "Cancelar correctamente" });
      }, err => {
        this.loader = false;
        this.msgs.push({ severity: 'error', detail: "error" });
      })
  }
  public cancelGroundRequest(id: number) {
    var url = this.base_path_service.base_path_api() + "tournament/teamRegistration/" + id + '/?format=json';
    var data = {
      "option": "ground",
      "action": "delete"
    }
    this.base_path_service.PutRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.received()
        this.msgs.push({ severity: 'info', detail: "Cancelar correctamente" });
      }, err => {
        this.loader = false;
        this.msgs.push({ severity: 'error', detail: "error" });
      })
  }

  public acceptTeamRequest(id: number) {
    var url = this.base_path_service.base_path_api() + "tournament/teamRegistration/" + id + "/";
    this.loader = true;
    let obj = {
      "tournament": this.tournamentID,
      "action": "approve",
      "option": "team"
    }
    this.base_path_service.PutRequest(url, obj)
      .subscribe(res => {
        if (res[0].status == 205) {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: res[0].json.json(), detail: "" });
          this.received()
        }
      },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: err.json(), detail: "" });
        })
  }

}




