import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TournamentProfileComponent } from './tournament-profile.component';
import { TopScorerComponent } from './top-scorer/top-scorer.component';
import { ShoutoutComponent } from './shoutout/shoutout.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { RequestComponent } from './request/request.component';
import { PosicionesComponent } from './posiciones/posiciones.component';
import { PointsComponent } from './points/points.component';
import { InformationComponent } from './information/information.component';
import { EditSponserComponent } from './edit-sponser/edit-sponser.component';
import { TournamentPaymentListComponent } from './tournament-payment-list/tournament-payment-list.component';
import { TransactionComponent } from './transaction/transaction.component';
import { LeagueHistoryComponent } from './league-history/league-history.component';

export const tournamentProfileRoutes: Routes = [
    {
        path: '', component: TournamentProfileComponent,
        children: [
            { path: '', component: InformationComponent },
            { path: 'information', component: InformationComponent },
            { path: 'schedule', component: ScheduleComponent },
            { path: 'request', component: RequestComponent },
            { path: 'shoutout', component: ShoutoutComponent },
            { path: 'points', component: PointsComponent },
            { path: 'posesiones', component: PosicionesComponent },
            { path: 'tournament_payment', component: TournamentPaymentListComponent },
            { path: 'tournament-team-transaction/:id', component: TransactionComponent },
            { path: 'history', component: LeagueHistoryComponent },


        ]
    },

    { path: 'edit-organization', component: EditSponserComponent },
    { path: 'edit-sponser', component: EditSponserComponent }
]

export const tournamentProfileRouting: ModuleWithProviders = RouterModule.forChild(tournamentProfileRoutes);