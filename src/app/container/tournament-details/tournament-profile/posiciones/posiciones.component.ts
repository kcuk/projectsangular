import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';
import { Message } from 'primeng/primeng';
@Component({
  selector: 'app-posiciones',
  templateUrl: './posiciones.component.html',
  styleUrls: ['./posiciones.component.css']
})
export class PosicionesComponent implements OnInit {
  status: string = "pending";
  referee: any;
  requestList: Array<any> = [];
  invitedList: any;
  cancelPopup: boolean = false;
  constructor(public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute) { }

  tournamentID: any;
  teamList: Array<any> = [];
  groundList: Array<any> = [];
  refereeList: Array<any> = [];
  msgs: Message[] = [];
  loader: boolean = false;
  basePath: any;
  reg_id: number = 0;
  admin: boolean = false;
  amount: number;
  insertAmountPopup: boolean = false;
  reg_fee: number;

  ngOnInit() {
    let id = this.route.parent.params.subscribe(params => {
      this.tournamentID = +params['id'];
    });
    this.basePath = this.base_path_service.image_url;

    this.API_getTeam();
    var destroy = this.base_path_service.pageChange.subscribe(res => {
      this.tournamentID = res[3];
      if (res[4] === "posesiones")
        this.API_getTeam();
    })
  }

  API_getReferee() {
    this.loader = true;
    this.status = "referee";
    this.teamList = [];
    this.groundList = [];
    let url = this.base_path_service.base_path_api() + "tournament/referee/?tournament=" + this.tournamentID + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.base_path_service.print(res[0].json, "referee list")
        this.refereeList = res[0].json;
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: err.json(), detail: "try again!!" });
      })
  }

  API_getTeam() {
    this.loader = true;
    this.status = "team";
    this.refereeList = [];
    this.groundList = [];
    let url = this.base_path_service.base_path_api() + "tournament/teamRegistration/" + this.tournamentID + "/?option=accepted&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.base_path_service.print(res[0].json, "team list")
        this.teamList = res[0].json.team_data;
        this.admin = res[0].json.admin;
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: err.json(), detail: "try again!!" });
      })
  }

  API_getGround() {
    this.loader = true;
    this.status = "ground";
    this.teamList = [];
    this.refereeList = [];
    let url = this.base_path_service.base_path_api() + "tournament/ground/?tournament=" + this.tournamentID + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.groundList = res[0].json;
        this.base_path_service.print(res[0].json, "ground list")
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: err.json(), detail: "try again!!" });
      })
  }
  public redirectGroundProfile(id: number,court_id) {
    localStorage.setItem(court_id,court_id)
    this.router.navigateByUrl('dashboard/ground/ground-profile/' + id);
  }
  public deleteTeamInfo() {
    let url = this.base_path_service.base_path_api() + 'tournament/teamRegistration/' + this.reg_id + "/?format=json";
    var info = {
      option: "team",
      action: 'delete',
      tournament: this.tournamentID
    }
    this.base_path_service.PutRequest(url, info)
      .subscribe(res => {
        this.API_getTeam();
        this.msgs = []
        this.msgs.push({ severity: 'info', detail: 'Cancelar correctamente' });
      }, error => {

      })
  }
  public insetAmount(reg_id) {
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentTeamPayment/?format=json';
    var info = {
      reg_id: reg_id,
      amount: this.amount
    }
    this.base_path_service.PostRequest(url, info)
      .subscribe(res => {
        this.API_getTeam();
      }, err => {

      })
  }
  public insetAmountInfo(reg_id) {
    this.amount = null;
    this.reg_fee = null;
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentTeamPayment/' + reg_id + '/?format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.amount = res[0].json.paid_amount;
        this.reg_fee = res[0].json.registration_fee;
        this.loader = false;
      }, error => {
        this.loader = false;
      })
  }
}

