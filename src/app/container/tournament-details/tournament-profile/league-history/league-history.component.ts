import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/primeng';
import { GlobalService } from '../../../../GlobalService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-league-history',
  templateUrl: './league-history.component.html',
  styleUrls: ['./league-history.component.css']
})
export class LeagueHistoryComponent implements OnInit {
  teamarray: any = [];
  selectedteam: any = "";
  msgs: Message[] = [];
  nodata: boolean = false;
  logo = this.base_path_service.image_url;
  lang = localStorage.getItem("language");
  leagu_id: number;
  historydata: any;
  loader: boolean;
  show_more: boolean = false;
  show_more_index: any;
  constructor(
    public router: Router,
    public base_path_service: GlobalService,
    public activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.parent.params.subscribe(params => {
      this.leagu_id = +params["id"];
    });
  }

  ngOnInit() {
    this.getTeamList();
    this.gethistory();
  }

  getTeamList() {
    var url = this.base_path_service.base_path_api() + "user/history_filter/?form_type=league_teams&league_id=" + this.leagu_id;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        console.log(res, "res=================");
        let teamList = res[0].json.teams;
        teamList.forEach(element => {
          this.teamarray.push({
            label: element.team_name,
            value: element.team_id
          });
        });
        console.log(this.teamarray, "team list=================");
      },
      err => {
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde"
        });
      }
    );
  }

  gethistory() {
    this.loader = true;
    var url = this.base_path_service.base_path_api() + "tournament/league_history/?league_id=" + this.leagu_id + "&team_id=" + this.selectedteam;
    this.base_path_service.GetRequest(url).subscribe(
      res => {
        this.loader = false;
        this.historydata = res[0].json;
        console.log(this.historydata, "this.historydata");
        if (this.historydata.length == 0) {
          this.nodata = true;
        } else {
          this.nodata = false;
        }
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde"
        });
      }
    );
  }

  open(index) {
    this.show_more_index = index;
    // this.showallcon = true;
    this.show_more = true;
  }

  close() {
    this.show_more = false;
    // this.showallcon = false;
  }

  teamselect(event) {
    console.log("team id ",this.selectedteam)
    this.gethistory();
  }
}
