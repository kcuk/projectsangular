import { NgModule } from '@angular/core';
import {TournamentProfileComponent} from './tournament-profile.component';
import {tournamentProfileRouting} from './tournament-profile.routes';
import {SharedModule} from './../../../shared/shared.module';
import { TopScorerComponent } from './top-scorer/top-scorer.component';
import { ShoutoutComponent } from './shoutout/shoutout.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { RequestComponent } from './request/request.component';
import { PosicionesComponent } from './posiciones/posiciones.component';
import { PointsComponent } from './points/points.component';
import { InformationComponent } from './information/information.component';
import { EditSponserComponent } from './edit-sponser/edit-sponser.component';
import { ShareButtonsModule } from 'ngx-sharebuttons';
import { TournamentPaymentListComponent } from './tournament-payment-list/tournament-payment-list.component';
import { TransactionComponent } from './transaction/transaction.component';
import { LeagueHistoryComponent } from './league-history/league-history.component';

@NgModule({
  imports: [
    tournamentProfileRouting,
    ShareButtonsModule.forRoot(),
    SharedModule.forRoot()
  ],
  declarations: [LeagueHistoryComponent,TransactionComponent,TournamentPaymentListComponent,TournamentProfileComponent, TopScorerComponent, ShoutoutComponent, ScheduleComponent, RequestComponent, PosicionesComponent, PointsComponent, InformationComponent, EditSponserComponent, TournamentPaymentListComponent, TransactionComponent, LeagueHistoryComponent]
})
export class TournamentProfileModule { }
