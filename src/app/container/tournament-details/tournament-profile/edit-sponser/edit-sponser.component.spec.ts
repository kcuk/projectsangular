import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSponserComponent } from './edit-sponser.component';

describe('EditSponserComponent', () => {
  let component: EditSponserComponent;
  let fixture: ComponentFixture<EditSponserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSponserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSponserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
