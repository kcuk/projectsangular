import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { GlobalService } from './../../../../GlobalService';
import { TranslateService } from "ng2-translate";

@Component({
  selector: 'app-edit-sponser',
  templateUrl: './edit-sponser.component.html',
  styleUrls: ['./edit-sponser.component.css']
})
export class EditSponserComponent implements OnInit {

  public sponser: Array<any> = [];
  public tournamentId: number = 0;
  public loader: boolean = false;
  public sponserId: any;
  public msgs: Array<any> = [];
  public file_src_organization: string = '';
  public organizationImage: string = "";
  public file_src_sponser: string[] = [];
  public imageBasePath: string = '';
  public buttonStatus: boolean = true;
  public organization_name: any;
  public organizationInfo: any;
  public checkStatus: boolean = false;

  constructor(private translate: TranslateService,public base_path_service: GlobalService, public route: ActivatedRoute, public router: Router ) {
if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
      console.log("lang=>>>>>>>>>>>>>>>>>>>", localStorage.getItem('language'));
    }
  }

  ngOnInit() {

    var tempVar = this.route.parent.params.subscribe(params => {
      this.tournamentId = params['id'];
    });
     var id = this.route.queryParams.subscribe(params => {
        this.sponserId = params.prop; 
       console.log("hello",this.sponserId)
    });
    this.imageBasePath = this.base_path_service.image_url;
    if (this.sponserId != 0) {
      this.sponserListInfo();
    }
   
  }

  addSponserList() {
    this.sponser.push({ pk: "" });
  }
  public sponserImageUploadEvent(event: any, index: number, input: any, id: number, name: string) {
    this.sponser[index].pic = event.target.files[0];
    if (name) {
      this.fileChange(input, 'sposer', index);
      this.uploadImage(index, 'sponser', id);
    } else {
      this.checkStatus = true;
    }
  }
  public organizationImageUploadEvent(event: any, args: any, name: string) {
    this.organizationImage = event.target.files[0];
    if (name) {
      this.fileChange(args, 'organization', 0);
      this.uploadImage(0, 'organization', 0);
    } else {
      this.checkStatus = true;
    }
  }
  public uploadImage(index: number, args: any, id: number) {
    this.uploadImageOnServer(index, args, id).then(
      (result) => { },
      (error) => { }
    );
  }

  public uploadImageOnServer(index: number, args: any, id: number) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'tournament/image/' + this.tournamentId + '/';
    var method;
    return new Promise((resolve, reject) => {

      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      if (args === 'organization') {
        method = "PUT";
        formData.append("organization_logo", this.organizationImage);
        formData.append("organization_name", this.organization_name);
      } else if (args == 'sponser' && this.sponser[index].pk != "") {
        method = "PUT"
        formData.append("name", this.sponser[index].name);
        formData.append("sponsor", id);
        formData.append("pic", this.sponser[index].pic);
      } else if (this.sponser[index].pk == "") {
        method = "POST";
        url = this.base_path_service.base_path_api() + 'tournament/image/';
        if (typeof this.sponser[index].name === 'undefined') {
          this.sponser[index].name = ""
        }
        formData.append("name", this.sponser[index].name);
        formData.append("tournament", this.tournamentId);
        formData.append("pic", this.sponser[index].pic);
      }
      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else if (xhr.status == 201) {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'info', detail: 'Actualizar correctamente' });
          } else if (xhr.status == 205) {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'info', detail: 'Actualizar correctamente' });
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open(method, url, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + JSON.parse(localStorage.getItem('userInfo')).token.access_token);
      xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
      xhr.send(formData);
    });
  }

  public fileChange(input: any, args: any, index) {
    for (var i = 0; i < input.files.length; i++) {
      var img = document.createElement("img");
      img.src = window.URL.createObjectURL(input.files[i]);
      var reader: any, target: EventTarget;
      var reader: any = new FileReader();
      reader.addEventListener("load", (event) => {
        img.src = event.target.result;
        if (args == 'organization') {
          this.file_src_organization = img.src;
        } else {
          this.file_src_sponser[index] = img.src;
        }

      }, false);
      reader.readAsDataURL(input.files[i]);
    }
  }

  public deleteSponserField(index: number, id: number) {

    if (id != null) {
      let url = this.base_path_service.base_path_api() + 'tournament/image/' + id + '/?format=json';
      this.sponser.splice(index, 1);
      this.base_path_service.DeleteRequest(url)
        .subscribe(res => {
          this.msgs.push({ severity: 'info', detail: "Borrado exitosamente" });
        }, err => {
          this.base_path_service.print(err);
          this.msgs.push({ severity: 'error', detail: "Algo mal" });
        })
    } else {
      this.sponser.splice(index, 1);
    }
  }
  public sponserListInfo() {
    var url = this.base_path_service.base_path_api() + 'tournament/sponsor/' + this.tournamentId + '/?format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.sponser = res[0].json.data;
        this.buttonStatus = res[0].json.show;
        this.organizationInfo = res[0].json.organization;
        this.organization_name = this.organizationInfo.name;
        if (this.organizationInfo.pic != "/media" && this.organizationInfo.pic) {
          this.file_src_organization = this.base_path_service.image_url + this.organizationInfo.pic;
        }
      }, err => {

      })
  }
  public backToProfile() {
    this.router.navigateByUrl('dashboard/tournaments/tournament-profile/' + this.tournamentId)
  }
}