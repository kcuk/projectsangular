import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentPaymentListComponent } from './tournament-payment-list.component';

describe('TournamentPaymentListComponent', () => {
  let component: TournamentPaymentListComponent;
  let fixture: ComponentFixture<TournamentPaymentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TournamentPaymentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentPaymentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
