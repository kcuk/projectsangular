import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
declare const html2pdf: any;


@Component({
  selector: 'app-tournament-payment-list',
  templateUrl: './tournament-payment-list.component.html',
  styleUrls: ['./tournament-payment-list.component.css']
})
export class TournamentPaymentListComponent implements OnInit {
  show_package_failed: boolean;
  isowner: boolean = false;
  show_package: boolean;
  package_details: any;
  show: boolean = true;
  tour_info: any;
  sponsers: any;
  data_not_found_pdf: boolean;
  result_pdf: any;
  index: any;
  team_id: any;
  result: any;
  data_not_found: boolean;
  id: number;
  msgs: any[] = [];
  loader: boolean = false;
  add_amount: boolean = false;
  team_name: any;
  tournament_name: any;
  pending_amount: any;
  pdf_popup: boolean = false;
  @ViewChild('layout') canvasRef;



  constructor(public base_path_service: GlobalService, public fb: FormBuilder, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      this.id = +params['id'];
    });
    this.getTournamentPaymentDetails();
  }

  getTournamentPaymentDetails() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "tournament/transactionList/?tournament=" + this.id;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.result = res[0].json.data;
        this.isowner = res[0].json.is_owner
        if (this.result.length == 0) {
          this.data_not_found = true;
        }
      }, err => {
        this.loader = false;
      })
  }

  public pdfPreview() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "tournament/registrationPdf/?tournament=" + this.id;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.result_pdf = res[0].json.data;
        this.sponsers = res[0].json.sponsors;
        if (this.sponsers.length == 0) {
          this.show = false;
        }
        console.log("sponser=>>>>>>>>>", this.sponsers)
        this.tour_info = res[0].json
        this.pdf_popup = true;
      }, err => {
        this.loader = false;
      })
  }



  downloadPdfBy() {
    const elementToPrint = this.canvasRef.nativeElement;
    html2pdf(elementToPrint, {
      margin: 0,
      filename: 'Tournament_payment_list.pdf',
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: { dpi: 192, letterRendering: true },
      jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
    });
  }

  showDetails() {
    let url = this.base_path_service.base_path_api() + "user/packageDetails/?form_type=tournament&tournament=" + this.id;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.package_details = res[0].json.data;
        this.show_package = true;
      }, err => {
        this.show_package_failed = true
      })
  }


}
