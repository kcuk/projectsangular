import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';

@Component({
  selector: 'app-top-scorer',
  templateUrl: './top-scorer.component.html',
  styleUrls: ['./top-scorer.component.css']
})
export class TopScorerComponent implements OnInit {

  public id: number = 0;
  public playerInfo: Array<any> = [];
  public scorerInfo: Array<any> = [];
  public url: string = "";
  public imageUrl: string = "";


  constructor(public base_path_service: GlobalService, public route: ActivatedRoute, public router: Router) { }
  ngOnInit() {
    this.imageUrl = this.base_path_service.image_url;
    this.url = window.location.pathname;
    this.shareInfo();
    this.route.parent.params.subscribe(params => {
      this.id = +params['id'];
    });
    this.topScorerInfo();
  }
  public backToTournamentProfile() {
    this.router.navigateByUrl('dashboard/tournament-profile/' + this.id + '/points')
  }
  public topScorerInfo() {
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentTopPlayers/?tournament='+this.id;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.scorerInfo = res[0].json;
      })
  }
  public redirectPlayerProfile(id: number) {
    this.router.navigateByUrl('dashboard/player-profile/' + id);
  }
  shareInfo() {
    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }
  shareOnFacebook() {
    window.open(this.url);
  }

}
