import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../../../GlobalService';
import { Message } from 'primeng/primeng';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {
  constructor(public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute) { }

  msgs: Message[] = [];
  loader: boolean = false;
  tournamentID: any;
  tournamentInfo: any;
  backdrop: boolean = false;
  backdrop1: boolean = false;
  visble: boolean = false;
  organization: any;
  fairness: any;
  rules_path: any;
  registerRefereePopup: boolean = false;
  no_rules: boolean = false;
  showPopup: boolean = false;
  Url: any;
  public tournamentImageInfo: Array<any> = [{ url: '', pos: 0 }, { url: '', pos: 1 }, { url: '', pos: 2 }, { url: '', pos: 3 }, { url: '', pos: 4 }]
  admin: boolean = false;
  public tournament_image: any;
  public imageBasePath: string = "";
  public preview: boolean = false;
  public previewUrl: string = "";
  public imageInfo: Array<any> = [];
  public deleteConfirmationMessage: boolean = false;
  public delImgPos: any;


  ngOnInit() {
    var tempUrl = window.location.href;
    this.base_path_service.shareTwitterInfo = "";
    this.base_path_service.shareImageInfo = "";
    this.base_path_service.shareTwitterInfo = tempUrl;
    this.imageBasePath = this.base_path_service.image_url;
    let id = this.route.parent.params.subscribe(params => {
      this.tournamentID = +params['id'];
    });
    this.API_getTournamentInformation();
  }

  API_getTournamentInformation() {
    this.loader = true;
    var url = this.base_path_service.base_path_api() + "tournament/tournamentDetail/" + this.tournamentID + "/?form_type=information&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(
      res => {
        this.loader = false;
        this.base_path_service.tournamentProfile.next("information");
        this.tournamentInfo = res[0].json;
        localStorage.setItem('tournament_name', this.tournamentInfo.tournament_name);
        var tournamentImage = res[0].json.images;
        this.imageInfo = tournamentImage;
        this.admin = res[0].json.admin;
        for (var index = 0; index < this.tournamentImageInfo.length; index++) {

          for (var i = 0; i < tournamentImage.length; i++) {
            if (this.tournamentImageInfo[index].pos == tournamentImage[i].position) {
              this.tournamentImageInfo[index].url = tournamentImage[i].image;
              this.tournamentImageInfo[index].pos = tournamentImage[i].position;
              break;
            }
          }
        }

        if (this.tournamentInfo.rule_file == null) {
          this.showPopup = true;
        } else if (this.tournamentInfo.rule_file != null) {
          this.showPopup = false;
          this.Url = this.base_path_service.image_url + res[0].json.rule_file;
        }
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
      })
  }

  showModal() {
    this.visble = true;
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "tournament/rating/" + this.tournamentID + "/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.organization = res[0].json.organization;
        this.fairness = res[0].json.fairness;
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: err.json(), detail: 'Please try again !!' });
      })
  }

  updateRating() {
    this.loader = true;
    let ratings = {
      "organization": this.organization,
      "fairness": this.fairness,
      "tournament": this.tournamentID
    }
    let url = this.base_path_service.base_path_api() + "tournament/rating/?format=json";
    this.base_path_service.PostRequest(url, ratings)
      .subscribe(res => {
        this.loader = false;
        if (res[0].status == 201) {
          this.API_getTournamentInformation();
          this.visble = false;
          this.backdrop1 = false;
        }
      },
      err => {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
      })
  }

  cancel() {
    this.visble = false;
    this.backdrop1 = false;
  }

  rules() {
    if (this.tournamentInfo.rule == null && this.tournamentInfo.rule_file == null) {
      this.no_rules = true;
      this.registerRefereePopup = true;
    } else if (this.tournamentInfo.rule != null && this.tournamentInfo.rule_file == null) {
      this.no_rules = false;
      this.registerRefereePopup = true;
    }
  }
  public tournamentPicChange($event: any, args: any, picArgs: any, index: number) {
    this.tournament_image = $event.target.files[0];
    this.tournamentImageUpload(index).then(
      (result) => {

      },
      (error) => {
        console.error(error);
      }
    );
  }
  public tournamentImageUpload(index) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'tournament/image/' + this.tournamentID + "/?format=json";
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();

      formData.append("image", this.tournament_image);
      formData.append("position", this.tournamentImageInfo[index].pos);

      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 201) {
            this.loader = false;
            this.msgs.push({ severity: 'info', detail:localStorage.getItem('language')=='en'?"Image has been loaded": "La imagen se ha cargado correctamente" });
            var tempTournamentImage = JSON.parse(xhr.response);
            for (var i = 0; i < this.tournamentImageInfo.length; i++) {
              for (var index = 0; index < tempTournamentImage.length; index++) {
                if (this.tournamentImageInfo[i].pos == tempTournamentImage[index].position) {
                  this.tournamentImageInfo[i].url = tempTournamentImage[index].image;
                  this.tournamentImageInfo[i].pos = tempTournamentImage[index].position;
                  break;
                }
              }
            }

            resolve(JSON.parse(xhr.response));
          } else if (xhr.status == 200) {
            this.loader = false;
          } else if (xhr.status == 205) {
            this.loader = false;

          } else if (xhr.status == 406) {
            this.loader = false;

          } else {
            this.loader = false;
            reject(xhr.response);
          }

        }
      }

      xhr.open("PUT", url, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + JSON.parse(localStorage.getItem('userInfo')).token.access_token);
      xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
      xhr.send(formData);
    });
  }
  public deleteTournamentImage(position) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + "tournament/image/" + this.tournamentID + "/?form_type=tourImage&position=" + position + "&format=json";
    this.base_path_service.DeleteRequest(url)
      .subscribe(res => {
        this.tournamentImageInfo[position].url = "";
        this.tournamentImageInfo[position].pos = position
        this.msgs.push({ severity: 'info', detail: "La imagen ha sido eliminada" });
        this.loader = false;
      })
  }
}