import { Component, OnInit, HostListener, Inject, AfterViewChecked } from '@angular/core';
import { Message } from 'primeng/primeng';
import { GlobalService } from './../../../GlobalService';
import { Router, ActivatedRoute } from '@angular/router';
import { DOCUMENT } from "@angular/platform-browser";
import { TranslateService } from 'ng2-translate';

declare const $: any;

@Component({
  selector: 'app-fixtures',
  templateUrl: './fixtures.component.html',
  styleUrls: ['./fixtures.component.css']
})
export class FixturesComponent implements OnInit {
  lang: string;
  obj: any;
  dates: any = [];
  date_de: any[][];
  sortList: any[];
  valid_date_group_msg: any;

  nayyar: boolean = false;
  roundID: any;
  key: string = "";  //round type
  es: any; //calendar options
  isGroup: boolean = false;
  isSE: boolean = false;
  isDE: boolean = false;
  noOfMatches: Array<any> = [];
  schedule_res: Array<any> = [];

  matches: string;
  roundName: string = '';
  tourId: any;
  ground: Array<any>;
  msgs: Message[] = [];
  fixtureDetailsIns: any;
  team_count: any;
  round_name: any;
  update_status: boolean = false;
  editPopup: boolean = false;
  loader: boolean = false;
  matchListSE: Array<any> = [];
  matchListSEInfo: Array<any> = [];
  GroupFixtureList: Array<any> = [];
  GroupFixtureListInfo: Array<any> = [];
  matchListDE: Array<any> = [];
  matchListDEInfo: Array<any> = [];
  create_se_fixture: boolean = false;
  show_create_fixture_div: boolean = false;
  create_fixture: boolean = false;
  fixtureListSE: Array<any> = [];
  fixtureListDE: Array<any> = [];
  enable_partidos_btn: boolean = true;
  fixtureDetailsSEIns;
  team1List: Array<any> = [];
  team2List: Array<any> = [];
  refereeList: Array<any> = [];
  no_teams: boolean = false;
  backdrop_div: boolean = false;
  selectedGroup: any;
  groupList: Array<any> = [];
  show_group_dropdown: boolean = false;
  page = 1;
  total_pages: any;
  groupIndex: any;
  group_obj: any;
  selectOption: any;
  is_fixture_true: boolean = false;
  display_scroll_arrow: boolean = false
  date_vlidation: any[] = [];
  date_vlidation1: any;
  valid_date: boolean = false
  showmsg: boolean = false
  valid_date_group: any[] = []
  selectedOption: any;
  check_team: boolean = false;
  ground_id: any = null
  fixture_obj: any[] = [];
  fixture_group: any;
  count: any = 0;
  refree_se_id: any = null;
  ground_se_id: any = null;
  schedule_id: any = 1;


  constructor(private translate: TranslateService, @Inject(DOCUMENT) public document: Document, public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute) {
    this.lang = localStorage.getItem('language');
    translate.use(this.lang);
    this.fixtureDetailsIns = new fixtureDetails();
    this.sortList = [];
    this.sortList.push({ "label": "Grupo", "value": "group" })
    if (this.schedule_id == 1) {
      this.sortList.push({ "label": "Fecha", "value": "team" })
    }
  }

  changedropdown(event) {
    this.sortList = [];
    this.sortList.push({ "label": "Grupo", "value": "group" })
    if (this.schedule_id == 1) {
      this.sortList.push({ "label": "Fecha", "value": "team" })
    }
  }
  ngOnInit() {
    console.log("ngOnInit")

    this.roundName = localStorage.getItem("roundName");
    this.tourId = localStorage.getItem("tid");
    let paramsSub = this.route.queryParams
      .subscribe(params => {
        this.roundID = +params['id'];
        this.key = params['group'];
        if (this.key == "group") {
          this.isGroup = true;
        } else if (this.key == "SE") {
          this.isSE = true;
          // this.create_fixture = true;
        } else if (this.key == "DE") {
          this.isDE = true;
        }
      });

    this.noOfMatches.push({ label: this.lang == "en" ? "Choose" : "Escoger", value: '' });
    this.noOfMatches.push({ label: this.lang == "en" ? "Yes" : 'Si', value: 2 })
    this.noOfMatches.push({ label: 'No', value: 1 });
    this.schedule_res.push({ label: this.lang == "en" ? "Choose" : "Escoger", value: '' })
    this.schedule_res.push({ label: 'No', value: 1 });
    this.schedule_res.push({ label: this.lang == "en" ? "Yes" : 'Si', value: 2 })

    this.es = {
      firstDayOfWeek: 1,
      dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
      dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
      dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
      monthNames: [
        this.lang == 'en' ? 'January' : "enero",
        this.lang == 'en' ? 'February' : "febrero",
        this.lang == 'en' ? 'March' : "marzo",
        this.lang == 'en' ? 'April' : "abril",
        this.lang == 'en' ? 'May' : "mayo",
        this.lang == 'en' ? 'June' : "junio",
        this.lang == 'en' ? 'July' : "julio",
        this.lang == 'en' ? 'August' : "agosto",
        this.lang == 'en' ? 'September' : "septiembre",
        this.lang == 'en' ? 'October' : "octubre",
        this.lang == 'en' ? 'November' : "noviembre",
        this.lang == 'en' ? 'December' : "diciembre"
      ],
      monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
      today: 'Hoy',
      clear: 'Borrar'
    }
    this.API_getGroundList();
    this.API_getRefereeList();
    this.API_getTeam1List();

    setTimeout(res => {

      this.API_getFixtures();
    }, 1000)
    this.selectOption = "Escoger"
  }

  ngAfterViewInit() {
    console.log("AfterViewInit")
  }


  public onScroll() {
    this.page = this.page + 1;
    if (this.page <= this.total_pages) {
      if (this.check_team) {
        this.getFixtureByTeam();
      }
      else {
        this.paginateGroupType(this.page);

      }
    }

  }

  API_getTeam1List() {
    var url = this.base_path_service.base_path_api() + "tournament/tournamentRoundTeam/" + this.roundID + "/?team=0&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        var response = res[0].json.data;
        if (res[0].json.data.length <= 1) {
          this.no_teams = false;
        } else {
          this.no_teams = true;
        }
        this.team1List = [];
        this.team1List = [{ label: "Escoger Equipo", value: '' }];
        if (res[0].json.form_type === "simple") {
          for (let i = 0; i < response.length; i++) {
            this.team1List.push({ label: response[i].team_name, value: response[i].id });
          }
        } else {
          for (let i = 0; i < response.length; i++) {
            this.team1List.push({ label: response[i].alias_name, value: response[i].id });
          }
        }
      })
  }

  API_getTeam2List(team_id) {
    var url = this.base_path_service.base_path_api() + "tournament/tournamentRoundTeam/" + this.roundID + "/?team=" + team_id + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        var response = res[0].json.data;
        this.team2List = [];
        this.team2List = [{ label: "Escoger Equipo", value: '' }];
        if (res[0].json.form_type === "simple") {
          for (let i = 0; i < response.length; i++) {
            this.team2List.push({ label: response[i].team_name, value: response[i].id });
          }
        } else {
          for (let i = 0; i < response.length; i++) {
            this.team2List.push({ label: response[i].alias_name, value: response[i].id });
          }
        }
      })
  }

  selectedTeam1Fun(event) {
    if (event) {
      this.fixtureDetailsIns.team1 = event.value;
      this.API_getTeam2List(event.value);
    }
  }

  selectedTeam2Fun(event) {
    if (event) {
      this.fixtureDetailsIns.team2 = event.value;
      this.API_getTeam1List();
    }
  }

  API_getRefereeList() {
    this.refereeList = [];
    this.refereeList = [{ label: this.lang=='en'?"Choose refree":"Escoger Árbitro", value: '' }];
    var url = this.base_path_service.base_path_api() + "tournament/referee/?form_type=simple&tournament=" + this.tourId + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(
        res => {
          var response = res[0].json;
          for (let i = 0; i < response.length; i++) {
            this.refereeList.push({ label: response[i].name, value: response[i].id });
          }

        })
  }

  selectedRefereeFun(event?, j?, i?, obj?) {
    if (event.value == '') {
      event.value = null
    }
    this.fixture_obj[i].refree = event.value;
  }


  API_getFixtures(property?: string, j?: any, i?: any, data?: any) {
    this.fixture_group = true;
    this.loader = true;
    let url = this.base_path_service.base_path + "api/tournament/fixture/?round=" + this.roundID + "&page=" + this.page + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(
        res => {
          if (res[0].status == 200) {
            this.loader = false;
            this.tourId = res[0].json.tournament_id
            this.team_count = res[0].json.number_of_match;
            this.round_name = res[0].json.round_name;
            this.total_pages = res[0].json.total_pages;
            if (res[0].json.data.length > 0) {
              this.update_status = true;
              this.groupList = [];
              if (res[0].json.group_data != null) {
                for (let i = 0; i < res[0].json.group_data.length; i++) {
                  this.groupList.push({ "label": res[0].json.group_data[i].group_name, "value": { id: res[0].json.group_data[i].id, index: i } })
                }
                this.selectedGroup = this.groupList[0].value;
                this.groupIndex = this.groupList[0].value.index;
                this.group_obj = this.groupList[0];
              }
              if (this.key == "group") {
                // if (property == "update") {
                //   this.GroupFixtureListInfo[j][i] = data;
                // } else if (property == "add") {
                //   this.GroupFixtureListInfo = [];
                //   this.GroupFixtureListInfo.push(res[0].json.data);
                // } else {
                //   this.GroupFixtureListInfo.push(res[0].json.data);
                // }
                Array.prototype.push.apply(this.GroupFixtureListInfo, res[0].json.data);
                let fixture_data = this.GroupFixtureListInfo;
                for (let i = 0; i < fixture_data.length; i++) {
                  if (fixture_data[i].date_time != null) {
                    let date = fixture_data[i].date_time
                    this.dates[i] = new Date(date)
                  }
                  else {
                    this.dates[i] = '';
                  }
                }
              } else if (this.key == "SE") {
                if (property == "update") {
                  Array.prototype.push.apply(this.matchListSEInfo, res[0].json.data);
                } else if (property == "add") {
                  this.matchListSEInfo = [];
                  Array.prototype.push.apply(this.matchListSEInfo, res[0].json.data);
                } else {
                  Array.prototype.push.apply(this.matchListSEInfo, res[0].json.data);
                  console.log("seeeeeeeeeeeeeeeeeee", this.matchListSEInfo)
                }

                let fixture_data = this.matchListSEInfo;
                for (let i = 0; i < fixture_data.length; i++) {
                  if (fixture_data[i].date_time != null) {
                    let date = fixture_data[i].date_time
                    this.dates[i] = new Date(date)
                  }
                  else {
                    this.dates[i] = '';
                  }
                }
              } else if (this.key == "DE") {
                if (property == "update") {
                  // this.matchListDEInfo[j][i] = data;
                  Array.prototype.push.apply(this.matchListDEInfo, res[0].json.data);

                } else if (property == "add") {
                  this.matchListDEInfo = [];
                  // this.matchListDEInfo.push(res[0].json.data);
                  Array.prototype.push.apply(this.matchListDEInfo, res[0].json.data);
                } else {
                  // this.matchListDEInfo.push(res[0].json.data);
                  Array.prototype.push.apply(this.matchListDEInfo, res[0].json.data);

                }
                let fixture = this.matchListDEInfo;
                console.log("fixtureeeeeeeeeeeeeee", fixture)
                for (let i = 0; i < fixture.length; i++) {
                  for (let j = 0; j < 2; j++) {
                    if (fixture[i].fixture[j].date_time != null) {
                      let date = fixture[i].fixture[j].date_time
                      fixture[i].fixture[j].date_time = new Date(date)
                    }
                    // else {
                    //   this.date_de[i][j] = '';
                    //   console.log("date nullll=>>>>>>>>>>>>>>>>>>", this.date_de)

                    // }

                  }
                }
              }
              this.is_fixture_true = res[0].json.is_fixture_created
            } else {
              this.update_status = false;
            }
          } else {
            this.loader = false;
          }
        },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
        })
  }

  createFixture(x?) {
    this.show_group_dropdown = true;
    this.loader = true;
    if (this.key === "group") {
      let url = this.base_path_service.base_path + "api/tournament/tournamentRound/?format=json";
      let data = { "round": this.roundID, "form_type": "match", "no_of_match": this.matches, "tournament": parseInt(this.tourId) };
      this.base_path_service.PostRequest(url, data)
        .subscribe(res => {
          if (res[0].status == 201) {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'info', summary: 'Status', detail: 'Creado correctamente' });
            this.GroupFixtureListInfo = [];
            this.page = 1
            this.API_getFixtures();
          }
        }, err => {
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: err.json(), detail: '' });
        });
    } else if (this.key === 'SE') {
      this.create_se_fixture = true;
    }
  }

  API_getGroundList() {
    let grounds;
    this.ground = [];
    this.ground = [{ label:this.lang=='en'?"Choose field": "Escoger Cancha", value: '' }];
    let url = this.base_path_service.base_path_api() + "tournament/ground/?form_type=simple&tournament=" + this.tourId + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        grounds = res[0].json;
        for (let i = 0; i < grounds.length; i++) {
          this.ground.push({ label: grounds[i].name, value: grounds[i].id });
        }
      });
  }

  backToTournamentRouting() {
    this.router.navigateByUrl('dashboard/tournament-profile/' + this.tourId);
  }
  updateFixture(data, i) {
    this.count = 0;
    console.log("updateeeeeeeeeeeeee", data, "iiiiiiiiiiiii", i)
    this.fixture_obj[i] = new fixtureDetails();
    data.flag = true;
    if (data.referee == null) {
      data.ref = true;
      data.referee_status = false;
      this.fixture_obj[i].refree = null;

    } else {
      data.ref = false;
      data.referee_status = true;
      this.fixture_obj[i].refree = data.referee.id;
    }

    if (data.ground == null) {
      data.gflag = true;
      data.ground_status = false;
      this.fixture_obj[i].ground = null;
    } else {
      data.gflag = false;
      data.ground_status = true;
      this.fixture_obj[i].ground = data.ground.id;
    }

    if (data.date_time != null) {
      this.fixture_obj[i].date_time = this.dates[i];
    } else {
      this.fixture_obj[i].date_time = null;
    }
    console.log("dataaaaaaaaaaaaaaaaa", this.fixture_obj[i])
  }

  saveFixture(data, property, i) {
    this.loader = true;
    console.log("iiiiiiiiiiiiiii", data, "dateeeeeeeeeeeeeeeee saveeeeeeeee", this.fixture_obj[i].date_time)
    // if (data.is_accepted_by_ground === 'accepted') {
    //   this.fixture_obj[i].ground = "";
    // }
    // if (data.is_accepted_by_referee === 'accepted') {
    //   this.fixture_obj[i].refree = "";
    // }
    if (this.fixture_obj[i].date_time !== null) {
      console.log("date nulllllllllllllllllll")
      // let temp = (this.fixture_obj[i].date_time).toString().split("T");
      // if (temp[0].length != 10) {
      //   this.datepicker(data.date_time, i);
      // }
      if (this.count == 0) {
        this.datepicker(this.fixture_obj[i].date_time, i)
      }
      this.valid_date_group[i] = true
      var obj = {
        "fixture": parseInt(data.fixture_id),
        "date_time": this.fixture_obj[i].date_time,
        "refree": this.fixture_obj[i].refree,
        "ground": this.fixture_obj[i].ground
      }
      var url = this.base_path_service.base_path_api() + "tournament/fixture/" + this.roundID + "/?format=json";
      this.base_path_service.PutRequest(url, obj)
        .subscribe(
          res => {
            this.loader = false;
            if (res[0].status == 205) {
              data.flag = false;
              this.msgs = [];
              this.msgs.push({ severity: 'info', summary: 'Status', detail: 'Actualizado correctamente' });

            }
          },
          err => {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: err.json(), detail: 'Inténtalo de nuevo !!' });
          })
    }
    else {
      this.loader = false;
      console.log("elseeeeeeeeeeeeeeeeeeeee")
      this.valid_date_group[i] = false
    }
  }


  datepicker(event, i?, j?) {
    this.count = 1;
    console.log("event=<<<<<<<<<<<<<<", event)
    if (i != null) {
      this.valid_date_group[i] = true
    }
    this.valid_date = true
    let validTime = event.toString().split(" ")[4];
    let finalTime = validTime.split(":");
    var validDate;
    let month = parseInt(event.getMonth());
    month = month + 1
    if (event.getMonth() > 8) {
      if (event.getDate() > 9) {
        validDate = event.getFullYear() + "-" + month + "-" + event.getDate();
      } else {
        validDate = event.getFullYear() + "-" + month + "-" + '0' + event.getDate();
      }

    } else {
      if (event.getDate() > 9) {
        validDate = event.getFullYear() + "-" + '0' + month + "-" + event.getDate();
      } else {
        validDate = event.getFullYear() + "-" + '0' + month + "-" + '0' + event.getDate();
      }
    }
    let time_zone = new Date().toString().split(' ')[5]
    if (i != undefined) {
      this.fixture_obj[i].date_time = validDate + "T" + finalTime[0] + ":" + finalTime[1] + time_zone;
    }
    else {
      this.fixtureDetailsIns.date_time = validDate + "T" + finalTime[0] + ":" + finalTime[1] + time_zone;
    }
    // console.log("fixtureeeeeeeeeeeeeeeee datepickerrrrrrrrrr", this.fixture_obj[i].date_time)
  }
  selectedGroundFun(event?, j?, i?, obj?) {
    this.fixture_obj[i].ground = event.value;
  }


  dialog() {
    this.editPopup = true;
    this.backdrop_div = true;
  }

  editFixture() {
    this.editPopup = false;
    this.update_status = false;
    this.backdrop_div = false;
  }

  addPartidos() {
    if (this.key == "SE") {
      this.fixtureListSE.push(new fixtureDetails());
      this.fixtureDetailsIns = new fixtureDetails();
      this.enable_partidos_btn = false;
    } else {
      this.fixtureListDE.push(new fixtureDetails());
      this.enable_partidos_btn = false;
    }
  }


  saveSeFixture(property?: any, data?) {
    console.log("dataaaaaaaaaaaaaa adataaaaaaaaa", data)
    console.log("fieldssssssssss adataaaaaaaaa", this.fixtureDetailsIns)
    if (this.valid_date) {
      this.loader = true;
      this.update_status = false;
      this.fixtureDetailsIns.round = parseInt(this.roundID);
      this.fixtureDetailsIns.ground = this.ground_se_id
      this.fixtureDetailsIns.refree = this.refree_se_id
      var url = this.base_path_service.base_path_api() + "tournament/fixture/?format=json"
      this.base_path_service.PostRequest(url, this.fixtureDetailsIns)
        .subscribe(
          res => {
            this.enable_partidos_btn = true;
            this.fixtureListSE = [];
            this.API_getFixtures(property);
            this.API_getTeam1List();
            this.msgs = [];
            this.msgs.push({ severity: 'info', summary: "", detail: 'Creado con éxito !!' });
          },
          err => {
            this.loader = false;
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: err.json(), detail: 'Inténtalo de nuevo !!' });
          })
    }
    else {
      this.showmsg = true
    }
  }

  updateSeFixture() {

  }


  saveDeTeams(add?: any) {
    this.loader = true;
    let teamObj = {
      "round": parseInt(this.roundID),
      "team1": this.fixtureDetailsIns.team1,
      "team2": this.fixtureDetailsIns.team2
    }

    var url = this.base_path_service.base_path_api() + "tournament/fixture/?format=json";
    this.base_path_service.PostRequest(url, teamObj)
      .subscribe(
        res => {
          this.loader = false;
          this.API_getFixtures(add);
          this.API_getTeam1List();
          this.enable_partidos_btn = true;
          this.fixtureListDE = [];
        },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: err.json(), detail: 'Inténtalo de nuevo !!' });
        })

  }

  saveDeFixture(data, update?: any, j?, i?, k?) {
    this.loader = true;
    let ind: number = -1;
    data.fixture.forEach(element => {
      ind++;
      console.log("date nulllllllllllllllll", data.fixture[ind].date_time)
      if (data.fixture[ind].date_time == null) {

      }
      else {
        data.fixture[ind].date_time = this.datepickerDE(data.fixture[ind].date_time)
      }
      // if (element.ground != null || element.referee != null) {
      //   if (element.date_time == null) {
      //     element.show = true
      //     this.date_vlidation[ind] = true
      //   }
      //   else {
      //     this.date_vlidation[ind] = false
      //     element.show = false
      //   }
      // }
      // else {
      //   this.date_vlidation[ind] = false
      //   element.show = false
      // }

    });

    // if (!this.date_vlidation[0]) {
    //   if (!this.date_vlidation[1]) {

    var url = this.base_path_service.base_path_api() + "tournament/fixture/" + this.roundID + "/?format=json";
    this.base_path_service.PutRequest(url, data)
      .subscribe(
        res => {
          this.loader = true;
          if (res[0].status == 205) {
            data.flag = false
            this.msgs = [];
            this.msgs.push({ severity: 'info', summary: 'Status', detail: 'Actualizado correctamente' });
            this.matchListDEInfo = []
            this.API_getFixtures();
          }
        },
        err => {
          this.loader = true;
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: err.json(), detail: 'Inténtalo de nuevo !!' });
        })
    //   }
    // }

  }

  editDeFixture(match, i) {
    match.flag = true;
    this.fixtureDetailsIns[i] = new fixtureDetails();
  }

  datepickerDE(event, data?) {
    console.log("event=>>>>>>>>>>>>>>>>>", event);

    let validTime = event.toString().split(" ")[4];
    let finalTime = validTime.split(":");
    var validDate;
    let month = parseInt(event.getMonth());
    month = month + 1
    if (event.getMonth() > 8) {
      if (event.getDate() > 9) {
        validDate = event.getFullYear() + "-" + month + "-" + event.getDate();
      } else {
        validDate = event.getFullYear() + "-" + month + "-" + '0' + event.getDate();
      }

    } else {
      if (event.getDate() > 9) {
        validDate = event.getFullYear() + "-" + '0' + month + "-" + event.getDate();
      } else {
        validDate = event.getFullYear() + "-" + '0' + month + "-" + '0' + event.getDate();
      }

    }
    let time_zone = new Date().toString().split(' ')[5]
    let date_time = validDate + "T" + finalTime[0] + ":" + finalTime[1] + time_zone;
    console.log("after date", date_time)
    return date_time
  }

  selectedDERefereeFun(data, event) {
    data.referee = event.value;
  }

  selectedDEGroundFun(data, event) {
    data.ground = event.value;
  }
  public selectGroupFromDropdown(event) {
    this.page = 1;
    this.GroupFixtureListInfo = [];
    for (let i in this.groupList) {
      if (this.groupList[i].value == this.selectedGroup) {
        this.groupIndex = i;
      }
    }
    if (this.check_team) {
      this.ground_id = event.value.id;
      this.getFixtureByTeam();
    }
    else {
      this.selectedGroupFun(event)
    }
  }

  selectedGroupFun(event) {
    this.loader = true;
    this.group_obj = event;
    var url;
    if (this.key === "group") {
      this.selectedGroup = event.value;
      url = this.base_path_service.base_path + "api/tournament/fixture/?round=" + this.roundID + "&group=" + event.value.id + "&page=" + this.page + "&format=json";
    } else if (this.key === "SE" || this.key === "DE") {
      url = this.base_path_service.base_path + "api/tournament/fixture/?round=" + this.roundID + "&page=" + this.page + "&format=json";
    }
    this.base_path_service.GetRequest(url)
      .subscribe(
        res => {
          if (res[0].status == 200) {
            this.loader = false;
            this.total_pages = res[0].json.total_pages;
            if (res[0].json.data.length > 0) {
              if (this.key == 'group') {
                let length = this.GroupFixtureListInfo.length
                console.log("length=?>>>>>>>>>>>>>>>>>>", length)
                Array.prototype.push.apply(this.GroupFixtureListInfo, res[0].json.data);
                console.log("check dataaaaaaaaaaaaaaa", this.GroupFixtureListInfo)
                let fixture_data = this.GroupFixtureListInfo;
                for (let i = length; i < fixture_data.length; i++) {
                  if (fixture_data[i].date_time != null) {
                    let date = fixture_data[i].date_time
                    this.dates[i] = new Date(date)
                  }
                  else {
                    this.dates[i] = '';
                  }
                }
              }
              if (this.key == 'SE') {
                Array.prototype.push.apply(this.matchListSEInfo, res[0].json.data);
                let length = this.GroupFixtureListInfo.length
                let fixture_data = this.matchListSEInfo;
                for (let i = length; i < fixture_data.length; i++) {
                  if (fixture_data[i].date_time != null) {
                    let date = fixture_data[i].date_time
                    this.dates[i] = new Date(date)
                  }
                  else {
                    this.dates[i] = '';
                  }
                }
              }
              if (this.key == 'DE') {
                // this.matchListDEInfo.push(res[0].json.data);
                let length = this.matchListDEInfo.length
                Array.prototype.push.apply(this.matchListDEInfo, res[0].json.data);
                let fixture = this.matchListDEInfo;
                console.log("fixtureeeeeeeeeeeeeee", fixture)
                for (let i = length; i < fixture.length; i++) {
                  for (let j = 0; j < 2; j++) {
                    if (fixture[i].fixture[j].date_time != null) {
                      let date = fixture[i].fixture[j].date_time
                      fixture[i].fixture[j].date_time = new Date(date)
                    }
                  }
                }
              }
            }
          } else {
            this.loader = false;
          }
        },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: err.json(), detail: 'Inténtalo de nuevo !!' });
        })
  }

  paginateGroupType(page) {

    this.fixtureListDE = [];
    this.enable_partidos_btn = true;
    this.fixtureListSE = [];
    this.page = page;
    this.selectedGroupFun(this.group_obj);
  }

  previousGroup() {
    if (this.groupIndex > 0) {
      this.GroupFixtureListInfo = [];
      this.groupIndex = --this.groupIndex;
      this.page = 1;
      if (this.check_team) {
        this.ground_id = this.groupList[this.groupIndex].value.id;
        this.selectedGroup = this.groupList[this.groupIndex].value;
        this.getFixtureByTeam();
      }
      else {
        for (let i = 0; i < this.groupList.length; i++) {
          if (i === this.groupIndex) {
            this.group_obj = this.groupList[i];
            this.selectedGroup = this.groupList[i].value;
            this.selectedGroupFun(this.groupList[i]);
          }
        }
      }
    }
  }

  nextGroup() {
    if (this.groupIndex < this.groupList.length - 1) {
      this.GroupFixtureListInfo = [];
      this.groupIndex = ++this.groupIndex;
      this.page = 1;
      if (this.check_team) {
        this.ground_id = this.groupList[this.groupIndex].value.id;
        this.selectedGroup = this.groupList[this.groupIndex].value;
        this.getFixtureByTeam();
      }
      else {
        for (let i = 0; i < this.groupList.length; i++) {
          if (i === this.groupIndex) {
            this.group_obj = this.groupList[i];
            this.selectedGroup = this.groupList[i].value;
            this.selectedGroupFun(this.groupList[i]);
          }
        }
      }
    }
  }

  selectedSortingOptionFun(event) {
    this.page = 1;
    this.GroupFixtureListInfo = [];
    this.selectOption = event.value;
    if (event.value === "team") {
      this.check_team = true
      this.getFixtureByTeam();
    }
    else {
      this.check_team = false
      this.API_getFixtures();
    }
  }


  getFixtureByTeam(group_id?) {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'tournament/tournamentRound/' + this.roundID + '/?sort=team_match&key_type=edit&sub_group=' + this.ground_id + '&page=' + this.page + "&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.fixture_group = res[0].json.success;
        if (this.fixture_group === undefined) {
          this.fixture_group = true
        }
        Array.prototype.push.apply(this.GroupFixtureListInfo, res[0].json.data);
        let fixture_data = this.GroupFixtureListInfo;
        for (let i = 0; i < fixture_data.length; i++) {
          if (fixture_data[i].date_time != null) {
            let date = fixture_data[i].date_time
            this.dates[i] = new Date(date)
          }
          else {
            this.dates[i] = '';
          }
        }
        // this.GroupFixtureListInfo.push(res[0].json.data);
        this.total_pages = res[0].json.total_pages
        this.groupList = [];
        for (let group = 0; group < res[0].json.sub_group.length; group++) {
          this.groupList.push({ "label": res[0].json.sub_group[group].name, "value": { id: res[0].json.sub_group[group].id, index: group } })
        }
      },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', detail: 'error' });
        })
  }

  changeDate(date_time) {
    console.log("okkkkkkkkkkkkkkkkkkkkk")
  }

}

class fixtureDetails {
  "round": string = "";
  "date_time": string = "";
  "refree": string = "";
  "ground": string = "";
  "team1": string = "";
  "team2": string = "";
}

