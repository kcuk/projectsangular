import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms'
import { GlobalService } from './../../../GlobalService';
import { Router, ActivatedRoute } from '@angular/router';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { Message } from 'primeng/primeng';
import { TranslateService } from "ng2-translate";

@Component({
  selector: 'app-create-tournament-second',
  templateUrl: './create-tournament-second.component.html',
  styleUrls: ['./create-tournament-second.component.css']
})
export class CreateTournamentSecondComponent implements OnInit {
  checkcost: boolean;

  public loader: boolean = false;
  public tournamentDivision: SelectItem[];
  public city: SelectItem[];
  public town: SelectItem[];
  public labelof_tournament_division;
  public label_of_team;
  public numberOfTeamList: SelectItem[];
  public cityValue: any;
  public sub: any;
  public level_of_prize;
  public prizeList: SelectItem[];
  public createTournamentForm2: FormGroup;
  public divisionValidation: boolean = false;
  public tournament_name: string = '';
  public enable: boolean = false;
  public displdisplayCropperModal: boolean = false;
  public statusOfCertPeloteando: boolean = false;
  public payment_mode: Array<any> = [];
  public statusOfTeam: boolean = false;
  public uploadFile: string = "";
  public profilePic: string = '';
  public logoPic: string = "";
  public orgPic: string = "";
  public file_srcs_profile: string = "";
  public file_srcs_logo: string = "";
  public file_srcs_organization: string = "";
  public data: any;
  public data2: any;
  public display: boolean = false;
  public backdrop: boolean = false;
  public cropperSettings: CropperSettings;
  public cropperSettings2: CropperSettings;
  public picArgs: any;
  public cropPic: any;
  public groundInput: any;
  public organization_name: any;
  @ViewChild('auto') auto: any;
  @ViewChild('cropper', undefined) cropper: ImageCropperComponent;
  public postDataStatus: string = "";
  public tourInput: any;
  public disableRuleText: boolean = false;
  public disableRulePdf: boolean = false;
  public circleImg: boolean = false;
  public squareImg: boolean = false;
  public result_type: SelectItem[];

  public sponser: Array<any> = [{
    "pic": "",
    "name": ""
  }];

  public createTournamentFieldsIns = {
    "page": 2,
    "tournament_division": "",
    "registration_fee": 0,
    "max_no_of_teams": 0,
    "prize": 0,
    "logo": "",
    "cover_pic": "",
    "tournament": 0,
    "rule": '',
    "rule_file": "",
    "payment_mode": [],
    "organization_logo": "",
    "tournament_id": 0
  }
  public msgs: any = [];
  public paymentMethodLebals: SelectItem[];

  constructor(private translate: TranslateService, public base_path_service: GlobalService, public router: Router, public route: ActivatedRoute, public fb: FormBuilder) {
    this.rectangleCropImage();
    this.circleCropImage();
    if (localStorage.getItem('language') != null) {
      translate.use(localStorage.getItem('language'));
    }
    this.uploadFile = localStorage.getItem('language') == 'en' ? "Upload file" : "Subir archivo";

  }

  ngOnInit() {
    this.tournament_name = localStorage.getItem('tournament_name');
    this.sub = this.route.params.subscribe(params => {
      this.createTournamentFieldsIns.tournament_id = params['id'];
    });

    this.setLevelOfTeamDivision();
    this.formvalidation();
    this.paymentMethods();
  }



  public setLevelOfTeamDivision() {
    this.labelof_tournament_division = 'Seleccione la división del torneo';
    this.tournamentDivision = [];
    this.tournamentDivision.push({ label: localStorage.getItem('language') == 'en' ? "Select the tournament division" : 'Seleccione la división del torneo', value: null });
    this.tournamentDivision.push({ label: localStorage.getItem('language') == 'en' ? 'Recreational' : 'Recreacional', value: 'Recreacional' });
    this.tournamentDivision.push({ label: localStorage.getItem('language') == 'en' ? 'Semi competitive' : 'Semi competitivo', value: 'Semi-Competitivo' });
    this.tournamentDivision.push({ label: localStorage.getItem('language') == 'en' ? 'competitive' : 'Competitivo', value: 'Competitivo' });

    this.result_type = [];
    this.result_type.push({ label: localStorage.getItem('language') == 'en' ? "Select scoring structure" : 'Select estructura de puntaje', value: null });
    this.result_type.push({ label: localStorage.getItem('language') == 'en' ? "Normal" : 'Normal', value: 'Simple-Ponit' });
    this.result_type.push({ label: localStorage.getItem('language') == 'en' ? "Special" : 'Especial', value: 'Penalties-Point' });

  }
  public formvalidation() {
    this.createTournamentForm2 = new FormGroup({
      divisionName: new FormControl('', Validators.compose([Validators.required])),
      regFee: new FormControl('', Validators.compose([Validators.required,])),
      noOfTeam: new FormControl('', Validators.compose([Validators.required])),
      noOfGroups: new FormControl('', Validators.compose([Validators.pattern('[0-9]+')])),
      prize: new FormControl('', Validators.compose([Validators.required,])),
      checkProfessional: new FormControl('', Validators.compose([])),
      payment: new FormControl([], Validators.compose([Validators.required])),
      result_type: new FormControl('Simple-Ponit'),
      min_fee: new FormControl(0,  ),
      discount: new FormControl(0,  ),
      vat: new FormControl(0,  )
    })
  }


  public sponserImageUploadEvent(fileInput: any, index) {
    let x = fileInput.target.files[0];
    this.sponser[index].pic = x.name;
  }

  public rulesPdfUploadEvent(fileInput: any) {
    this.createTournamentFieldsIns.rule_file = fileInput.target.files[0];
    this.uploadFile = fileInput.target.files[0].name;
    this.disableRuleText = true;
  }
  public createTournament() {
    if (this.createTournamentForm2.valid && this.checkcost===false) {
      console.log("resultsssssss", this.createTournamentForm2)
      this.loader = true;
      this.postDataStatus = "POST";
      this.makeFileUploadRequest(1).then(
        (result) => {
          console.log("hiiiiiiiiiiiiiiiiiiiiii")
          this.loader = false;
          if (!this.createTournamentForm2.value.divisionName) {
            this.divisionValidation = true;
          } else {
            this.router.navigateByUrl('/dashboard/package/' + this.createTournamentFieldsIns.tournament_id + '?status=tournament');
          }
        },
        (error) => {
          this.loader = false;
        }
      );
    } else {
      console.log("erooooo")
      this.msgs = [];
      this.msgs.push({ severity: 'error', detail: 'Ingresa todos los campos obligatorios' });
    }
  }

  public makeFileUploadRequest(index) {
    var url;
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      var method;
      if (this.postDataStatus == "POST") {
        console.log("post=>>>>>>>>>>>>>>>>>>>>>>>>>")
        method = "POST";
        url = this.base_path_service.base_path_api() + 'tournament/tournamentDetail/?format=json';
        formData.append("tournament", this.createTournamentFieldsIns.tournament_id);
        formData.append("page", 2);
        formData.append("tournament_division", this.createTournamentForm2.value.divisionName);
        formData.append("registration_fee", this.createTournamentForm2.value.regFee);
        formData.append("max_no_of_teams", this.createTournamentForm2.value.noOfTeam);
        formData.append("rule_file", this.createTournamentFieldsIns.rule_file);
        formData.append("payment_mode", JSON.stringify(this.createTournamentForm2.value.payment));
        formData.append("prize", this.createTournamentForm2.value.prize);
        formData.append("result_type", this.createTournamentForm2.value.result_type);
        formData.append("minimum_fee", this.createTournamentForm2.value.min_fee ? this.createTournamentForm2.value.min_fee : 0);
        formData.append("discount", this.createTournamentForm2.value.discount ? this.createTournamentForm2.value.discount : 0);
        formData.append("vat", this.createTournamentForm2.value.vat ? this.createTournamentForm2.value.vat : 0);
      } else if (this.postDataStatus == "profile") {
        url = this.base_path_service.base_path_api() + 'tournament/image/' + this.createTournamentFieldsIns.tournament_id + '/';
        method = "PUT"
        formData.append("cover_pic", this.createTournamentFieldsIns.cover_pic);
      } else if (this.postDataStatus == "logo") {
        url = this.base_path_service.base_path_api() + 'tournament/image/' + this.createTournamentFieldsIns.tournament_id + '/';
        method = "PUT"
        formData.append("logo", this.createTournamentFieldsIns.logo);
      } else if (this.postDataStatus == "organization") {
        url = this.base_path_service.base_path_api() + 'tournament/image/' + this.createTournamentFieldsIns.tournament_id + '/';
        method = "PUT"
        formData.append("organization_logo", this.createTournamentFieldsIns.organization_logo);
        formData.append("organization_name", this.organization_name);
      } else if (this.postDataStatus == "sponser") {
        url = this.base_path_service.base_path_api() + 'tournament/image/';
        method = "POST"
        formData.append("tournament", this.createTournamentFieldsIns.tournament_id);
        formData.append("pic", this.sponser[index].pic);
        formData.append("name", this.sponser[index].name);
      }
      xhr.onreadystatechange = (res) => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else if (xhr.status == 201) {
            this.loader = false;
            let tournament_id = JSON.parse(xhr.response).tournament_id;
            let select_package = JSON.parse(xhr.response).payment_allowed
            localStorage.setItem('tournament_name', JSON.parse(xhr.response).tournament_name)
            console.log("JSON.parse(xhr.response)",JSON.parse(xhr.response))
            if (this.postDataStatus == "POST") {
              if (select_package) {
                // this.router.navigateByUrl('/dashboard/package/' + this.createTournamentFieldsIns.tournament_id + '?status=tournament');
                let url = this.base_path_service.base_path_api() + "tournament/tournamentData/?tournament_id=" + this.createTournamentFieldsIns.tournament_id;
                this.base_path_service.GetRequest(url)
                  .subscribe(res => {
                    this.loader = false
                    let tournament = {
                      tounament_info: res[0].json
                    }
                    localStorage.setItem('tournament', JSON.stringify(tournament));
                    this.router.navigateByUrl('dashboard/payment/' + this.createTournamentFieldsIns.tournament_id + "?tournament=" + true);
                  }, err => {
                    this.loader = false
                  })
              }
              else {
                this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + tournament_id)
              }
            }
          } else {
            reject(xhr.response);
          }
        }
        else{
        }
      }

      xhr.open(method, url, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + JSON.parse(localStorage.getItem('userInfo')).token.access_token);
      xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
      xhr.send(formData);
    });
  }

  public addSponserList() {
    this.sponser.push({});
  }
  public paymentMethods() {
    let url = this.base_path_service.base_path_api() + "peloteando/payment_mode/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.paymentMethodLebals = [];
        let paymentInfo = res[0].json;
        for (var i = 0; i < res[0].json.length; i++) {
          this.paymentMethodLebals.push({ label: paymentInfo[i].option, value: paymentInfo[i].id });
        }
      }, err => {
        this.base_path_service.print(err);
      })
  }

  public fileChangeListener(event: any, picArgs: any, input) {
    this.picArgs = picArgs;
    this.tourInput = input;
    if (this.picArgs == "profile") {
      this.circleImg = false;
      this.squareImg = true;
    } else {
      this.circleImg = true;
      this.squareImg = false;

    }

    var image: any = new Image();
    var file: File = event.target.files[0];
    var myReader: FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
      if (that.picArgs == "profile") {
        that.cropPic = image.src.split(",")[1];
        that.file_srcs_profile = image.src;
      } else {
        that.cropPic = image.src.split(",")[1];
        that.file_srcs_logo = image.src;
      }
    };
    myReader.readAsDataURL(file);
    this.showDialog();
  }
  public fileChange(input, args) {
    for (var i = 0; i < input.files.length; i++) {
      var img = document.createElement("img");
      img.src = window.URL.createObjectURL(input.files[i]);
      var reader: any, target: EventTarget;
      var reader: any = new FileReader();
      reader.addEventListener("load", (event) => {
        img.src = event.target.result;

        if (args == 'organization') {
          this.file_srcs_organization = img.src;
        } else if (args == 'sponser') {
          // this.file_srcs_organization=img.src;
        }

      }, false);
      reader.readAsDataURL(input.files[i]);
    }
  }
  public showDialog() {
    this.display = true;
    this.backdrop = true;
  }

  public updateCropImage() {
    this.loader = true;
    if (this.picArgs == "logo") {
      this.createTournamentFieldsIns.logo = this.cropPic;
      this.postDataStatus = "logo";
      this.display = false;
      this.backdrop = false;
      this.picArgs = "";
      this.tourInput = ""
      this.uploadImage(1);
    } else if (this.picArgs == "profile") {
      this.postDataStatus = "profile";
      this.createTournamentFieldsIns.cover_pic = this.cropPic;
      this.display = false;
      this.backdrop = false;
      this.picArgs = "";
      this.tourInput = ""
      this.uploadImage(1);
    }
  }
  public uploadImage(index) {
    this.makeFileUploadRequest(index).then(
      (result) => {

      }, (error) => {

      });
  }

  public organizationImageUpload($event: any, picArgs: any, input) {
    this.postDataStatus = "organization";
    this.createTournamentFieldsIns.organization_logo = $event.target.files[0];
    this.fileChange(input, 'organization');
    this.uploadImage(1);
  }
  public sponserImageUpload($event: any, picArgs: any, input, index) {
    this.postDataStatus = "sponser";
    this.sponser[index].pic = $event.target.files[0];
    this.fileChange(input, 'sponser');
    this.uploadImage(index);
  }
  public changeStatus() {
    if (this.createTournamentFieldsIns.rule.length == 0) {
      this.disableRulePdf = false;
    } else {
      this.disableRulePdf = true;
    }
  }
  public deletePdfFile() {
    this.disableRuleText = false;
    this.createTournamentFieldsIns.rule_file = "";
    this.uploadFile = localStorage.getItem('language') == 'en' ? "Upload file" : "Subir archivo";
  }

  public rectangleCropImage() {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 1200;
    this.cropperSettings.height = 250;
    this.cropperSettings.keepAspect = false;

    this.cropperSettings.croppedWidth = 1200;
    this.cropperSettings.croppedHeight = 250;

    this.cropperSettings.canvasWidth = 350;
    this.cropperSettings.canvasHeight = 300;

    this.cropperSettings.minWidth = 100;
    this.cropperSettings.minHeight = 100;

    this.cropperSettings.rounded = false;
    this.cropperSettings.minWithRelativeToResolution = false;

    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings.noFileInput = true;

    this.data = {};
  }
  public circleCropImage() {
    this.cropperSettings2 = new CropperSettings();
    this.cropperSettings2.width = 200;
    this.cropperSettings2.height = 200;
    this.cropperSettings2.keepAspect = false;

    this.cropperSettings2.croppedWidth = 200;
    this.cropperSettings2.croppedHeight = 200;

    this.cropperSettings2.canvasWidth = 350;
    this.cropperSettings2.canvasHeight = 300;

    this.cropperSettings2.minWidth = 100;
    this.cropperSettings2.minHeight = 100;

    this.cropperSettings2.rounded = true;
    this.cropperSettings2.minWithRelativeToResolution = true;

    this.cropperSettings2.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings2.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings2.noFileInput = true;

    this.data2 = {};
  }
  public cancelButton() {
    this.router.navigateByUrl('dashboard/tournaments');
  }

  checkCost(event) {
    if (parseInt(this.createTournamentForm2.controls['regFee'].value) < parseInt(this.createTournamentForm2.controls['min_fee'].value)) {
     this.checkcost = true
    }
    else {
      this.checkcost = false
    }

  }
}

