import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTournamentSecondComponent } from './create-tournament-second.component';

describe('CreateTournamentSecondComponent', () => {
  let component: CreateTournamentSecondComponent;
  let fixture: ComponentFixture<CreateTournamentSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTournamentSecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTournamentSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
