import { Component, OnInit, NgZone } from '@angular/core';
import { TranslateService } from "ng2-translate";
import { Router, ActivatedRoute } from "@angular/router";
import { GlobalService } from "./../../GlobalService";
import { FormBuilder, Validators } from "@angular/forms";
declare var Stripe: any;
@Component({
  selector: 'app-paymentform',
  templateUrl: './paymentform.component.html',
  styleUrls: ['./paymentform.component.css']
})
export class PaymentformComponent implements OnInit {
  card_type: any;
  tour_first_pay: string = '';
  display: boolean;
  loader: boolean;
  msgs: any[] = [];
  form: any;
  selectedValues: boolean = true;
  button_click: boolean = false;
  cardValid: boolean = true;
  expiry_date: boolean = true;
  months: any = [
    { value: '', 'label': localStorage.getItem('language') == 'es' ? 'Seleccione mes' : 'Select month' },
    { 'value': '01', 'label': "01" },
    { 'value': '02', 'label': "02" },
    { 'value': '03', 'label': "03" },
    { 'value': '04', 'label': "04" },
    { 'value': '05', 'label': "05" },
    { 'value': '06', 'label': "06" },
    { 'value': '07', 'label': "07" },
    { 'value': '08', 'label': "08" },
    { 'value': '09', 'label': "09" },
    { 'value': '10', 'label': "10" },
    { 'value': '11', 'label': "11" },
    { 'value': '12', 'label': "12" },
  ];
  years: any = [
    { value: '', 'label': localStorage.getItem('language') == 'es' ? 'Seleccione el año' : 'Select year' },
    { 'value': '2018', 'label': "2018" },
    { 'value': '2019', 'label': "2019" },
    { 'value': '2020', 'label': "2020" },
    { 'value': '2021', 'label': "2021" },
    { 'value': '2022', 'label': "2022" },
    { 'value': '2023', 'label': "2023" },
    { 'value': '2024', 'label': "2024" },
    { 'value': '2025', 'label': "2025" },
    { 'value': '2026', 'label': "2026" },
    { 'value': '2027', 'label': "2027" },
    { 'value': '2028', 'label': "2028" },
    { 'value': '2029', 'label': "2029" },
    { 'value': '2030', 'label': "2030" },
  ];
  constructor(private _zone: NgZone, private translate: TranslateService, public router: Router, public base_path_service: GlobalService, public route: ActivatedRoute, public _fb: FormBuilder) {

  }

  ngOnInit() {
    this.form = this._fb.group({
      "number": ["", Validators.compose([Validators.required,])],
      "exp_month": ["", Validators.compose([Validators.required])],
      "exp_year": ["", Validators.compose([Validators.required])],
      "cvc": ["", Validators.compose([Validators.required, Validators.maxLength(4), Validators.minLength(3)])],
      "name": ["", Validators.compose([Validators.required, Validators.maxLength(56)])],
    })
  }

 
  firstFormData() {
    this.button_click = true;
    this.loader = true;
    Stripe.setPublishableKey('pk_test_hwkRFeFpzlsKAoVVxsHSghO8');
    // Stripe.setPublishableKey('pk_live_ETEDtwyB7w42TDraGBBljZtY');
    this.form.patchValue({ number: this.form.controls.number.value.replace(/_/g, "") })
    Stripe.card.createToken(this.form.value,
      (status, response) => { //I'm using an arrow function instead of stripeResponseHandler(a function also) cos it's kickass!
        // basically you can do anything here with the reponse that has your token
        // you can hit your backend api and initialize a charge etc
        this._zone.run(() => {
          if (status == 200) {
            if (localStorage.getItem('reservation')) {
              this.payForReservation(response, this.form.value);
            }
            else if (localStorage.getItem('teamregister')) {
              this.payForTeamRegister(response, this.form.value)
            }
            else if (localStorage.getItem('groundcreation')) {
              this.payForGroundCreation(response, this.form.value)
            }
            else if (localStorage.getItem('tournamentcreation')) {
              this.payForTournamentCreation(response, this.form.value)
            }
          }
          else {
            this.showerror(response);
          }
        });
      }
    )
  }

  showerror(res) {
    this.msgs = [];
    this.msgs.push({ severity: 'error', detail: res.error.message });
    this.button_click = false;
    this.loader = false;
  }

  payForTournamentCreation(token, value) {
    let data = JSON.parse(localStorage.getItem('tournamentcreation'));
    data.token = token.id;
    // data.save_card = this.selectedValues;
    this.route.queryParams.subscribe(params => {
      console.log("params=>>>>>>>>>>>>>>", params)
      this.tour_first_pay = params['tour_first_pay'];
    });
    let url;
    if (this.tour_first_pay === 'true') {
      url = this.base_path_service.base_path_api() + "tournament/createTournamentInitialPay/"
    }
    else {
      url = this.base_path_service.base_path_api() + "tournament/createTournamentFinalPay/"
    }
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        console.log("statussssssssssssssssssssssssss", res)
        if (res[0].json.success) {
          console.log("successssssssssssssss")
          this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + data.tournament_id + '?status=1')
        }
        else {
          this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + data.tournament_id + '?status=2')
        }
      }, err => {
        this.loader = false;
        this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + data.tournament_id + '?status=2')
      })
  }

  payForGroundCreation(token, value) {
    let data = JSON.parse(localStorage.getItem('groundcreation'));
    data.token = token.id;
    // data.save_card = this.selectedValues;
    let url = this.base_path_service.base_path_api() + "ground/createGroundPay/"
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        console.log("statussssssssssssssssssssssssss", res)
        if (res[0].json.success) {
          console.log("successssssssssssssss")
          this.router.navigateByUrl('dashboard/ground/ground-profile/' + data.ground_id + '?status=1');
        }
        else {
          this.router.navigateByUrl('dashboard/ground/ground-profile/' + data.ground_id + '?status=2');
        }
      }, err => {
        this.loader = false;
        this.router.navigateByUrl('dashboard/ground/ground-profile/' + data.ground_id + '?status=2');
      })
  }


  payForTeamRegister(token, value) {
    let data = JSON.parse(localStorage.getItem('teamregister'));
    data.token = token.id;
    // data.save_card = this.selectedValues;
    console.log("dataaaaaaaaaaaa", data)
    let url = this.base_path_service.base_path_api() + "tournament/registerTeamPay/"
    this.base_path_service.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        console.log("statussssssssssssssssssssssssss", res)
        if (res[0].json.success) {
          console.log("successssssssssssssss")
          this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + data.tournament_id + '?status=1')
        }
        else {
          this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + data.tournament_id + '?status=2')
        }
      }, err => {
        this.loader = false;
        this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + data.tournament_id + '?status=2')
      })
  }

  payForReservation(token, value) {
    console.log("tokennnnnnnnnnnnnnnnn", token)
    let data = JSON.parse(localStorage.getItem('reservation'));
    let obj = {
      ground_id: data.ground_id,
      court_id: data.court_id,
      reservation_id: data.reservation_id,
      request_type: data.request_type,
      "token": token.id,
      "form_type": "direct_pay",
      save_card: true
    }
    console.log("dataaaaaaaaaaaa", data)
    let url = this.base_path_service.base_path_api() + "ground/reserveFieldPay/"
    this.base_path_service.PostRequest(url, obj)
      .subscribe(res => {
        this.loader = false;
        console.log("statussssssssssssssssssssssssss", res)
        if (res[0].json.success) {
          console.log("successssssssssssssss")
          this.router.navigateByUrl('/dashboard/ground/ground-profile/' + obj.ground_id + '/schedule?status=11')
        }
        else {
          this.router.navigateByUrl('/dashboard/ground/ground-profile/' + obj.ground_id + '/schedule?status=12')
        }
      }, err => {
        this.loader = false;
        this.router.navigateByUrl('/dashboard/ground/ground-profile/' + obj.ground_id + '/schedule?status=12')
      })
  }


  cancelButton() {
    if (localStorage.getItem('reservation')) {
      this.router.navigateByUrl('/dashboard/ground/ground-profile/' + JSON.parse(localStorage.getItem('reservation')).ground_id)
    }
    else if (localStorage.getItem('teamregister')) {
      this.router.navigateByUrl('/dashboard/tournaments/tournament-profile/' + JSON.parse(localStorage.getItem('teamregister')).tournament_id)
    }
    else if (localStorage.getItem('groundcreation')) {
      this.router.navigateByUrl('/dashboard/ground/ground-profile/' + JSON.parse(localStorage.getItem('groundcreation')).ground_id)
    }
  }

  validateCard(event) {
    let card_number = event.target.value.replace(/_/g, "")
    this.cardValid = Stripe.card.validateCardNumber(card_number);
    this.card_type = Stripe.card.cardType(card_number)
    console.log("bank card typeeeeeeeeee", Stripe.card.cardType(card_number))
  }

  validateExpireDate() {
    this.expiry_date = Stripe.card.validateExpiry(this.form.controls.exp_month.value, this.form.controls.exp_year.value);
  }
}
