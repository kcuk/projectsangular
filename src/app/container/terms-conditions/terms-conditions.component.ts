import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.css']
})
export class TermsConditionsComponent implements OnInit {

  constructor(public router:Router) { }

  ngOnInit() {
  }
  back_to_profile() {
    if (localStorage.getItem('term')) {
      this.router.navigateByUrl("/dashboard");
    }
    else {
      this.router.navigateByUrl('home');
    }
  }
}
