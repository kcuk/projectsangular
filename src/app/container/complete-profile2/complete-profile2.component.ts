import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { Message } from 'primeng/primeng';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { GlobalService } from './../../GlobalService';
import { FacebookService } from '../../services/login/facebookLogin.service';
import { LoginService } from '../../services/login/login.service';

@Component({
  selector: 'app-complete-profile2',
  templateUrl: './complete-profile2.component.html',
  styleUrls: ['./complete-profile2.component.css']
})
export class CompleteProfile2Component implements OnInit, AfterViewInit {

   loader: boolean = false;
   notification_logo: string = '';
   requestoptions: any;
   Players: Array<any> = [{}];
   grounds: Array<any> = [{}];
   Teams: Array<any> = [{}];
   player_data: any;
   ground_data: any;
   coverpic: any[];
   coverPicList;
   cover_url;
   cover_id: number = 0;
   name: any;
   player_id: any;
   team_id: any;
   id: number = null;
   playerpic: any;
   Enable: number = 0;
   Enable1: number = 0;
   backdrop: boolean = false;
   display: boolean = false;
   num: number = 0;
   logo: string = '';
   file_name: string = '';
   image_name: string = '';
   msgs: Message[] = [];
   unfollow_player_btn: boolean = false;
   unfollow_team_btn: boolean = false;
   data: any;
   showBackButton: any;
   fb_status: boolean = false;
  cropperSettings: CropperSettings;
  @ViewChild('cropper', undefined) cropper: ImageCropperComponent;
  ngAfterViewInit() {
    this.API_getPlayers();
    this.API_getTeams();
  }
  constructor(private fb: FacebookService, private login: LoginService, private router: Router, private http: Http, private base_path_service: GlobalService) {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.width = 200;
    this.cropperSettings.height = 200;
    this.cropperSettings.keepAspect = false;

    this.cropperSettings.croppedWidth = 200;
    this.cropperSettings.croppedHeight = 200;

    this.cropperSettings.canvasWidth = 300;
    this.cropperSettings.canvasHeight = 300;

    this.cropperSettings.minWidth = 100;
    this.cropperSettings.minHeight = 100;

    this.cropperSettings.rounded = true;
    this.cropperSettings.minWithRelativeToResolution = false;

    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;


    this.data = {};
  }

  ngOnInit() {
    if (localStorage.getItem("fb_status")) {
      this.image_name = this.base_path_service.base_path + localStorage.getItem('profile_pic');
      this.fb_status = true;
    }
    this.loader = true;
    this.API_getCoverImages();

    this.API_getGrounds();
    this.loader = false;
    if (JSON.parse(localStorage.getItem('userInfo'))[0].info.first_page == true) {
      this.showBackButton = true;

    } else {
      this.showBackButton = false;
    }
  }



   API_getCoverImages() {
    let url = this.base_path_service.base_path + "api/user/cover_pic/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.coverPicList = res[0].json;
        this.cover_url = this.base_path_service.image_url;
      });
  }

   API_getPlayers() {
    let url = this.base_path_service.base_path + "api/user/player/?form_type=signup&format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.Players = res[0].json;
        this.cover_url = this.base_path_service.image_url;
      });
  }

   API_getTeams() {
    let url = this.base_path_service.base_path + "api/team/profile/?format=json";
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.Teams = res[0].json;
        this.cover_url = this.base_path_service.image_url;
      });
  }

   API_getGrounds() {
    let url = this.base_path_service.base_path + 'api/ground/groundProfile/?form_type=onboarding2&format=json';
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.grounds = res[0].json;
        this.cover_url = this.base_path_service.image_url;
      });
  }

   follow_player(player: any) {
    this.loader = true;
    let url = this.base_path_service.base_path + 'api/user/profile/?format=json'
    this.player_data = {
      "form_type": "follow_player",
      "follow_player": player.id
    }
    this.base_path_service.PostRequest(url, this.player_data)
      .subscribe(
      res => {
        if (res[0].status == 200 || res[0].status == 201 || res[0].status == 205) {
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: '', detail: 'Seguido con éxito' });
          this.API_getPlayers();
        }
      },
      err => {
      })
    this.loader = false;
  }

   follow_team(team: any) {
    this.loader = true;
    let url = this.base_path_service.base_path + 'api/user/profile/?format=json'
    this.player_data = {
      "form_type": "follow_team",
      "follow_team": team.id
    }
    this.base_path_service.PostRequest(url, this.player_data)
      .subscribe(
      res => {
        if (res[0].status == 200 || res[0].status == 201 || res[0].status == 205) {
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: '', detail: 'Seguido con éxito' });
          this.API_getTeams()
        }
      },
      err => {
      })
    this.loader = false;
  }


   follow_ground(ground: any) {
    this.loader = true;
    let url = this.base_path_service.base_path + 'api/user/profile/?format=json'
    this.ground_data = {
      "form_type": "follow_ground",
      "follow_ground": ground.id
    }
    this.base_path_service.PostRequest(url, this.ground_data)
      .subscribe(
      res => {
        if (res[0].status == 200) {
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: '', detail: 'Seguido con éxito' });
          this.API_getGrounds();
        }
      },
      err => {
      })
    this.loader = false;

  }

   fileChangeEvent(fileInput: any) {
    this.notification_logo = fileInput.target.files[0];
  }

   get_cover(id: number) {
    if (this.cover_id == id) {
      this.cover_id = 0;
    } else {
      this.cover_id = id;
      this.id = id;
    }
  }


   onSubmitXhr() {
    this.loader = true;
    this.makeFileRequest().then(
      (result) => {

      },
      (error) => {

      }
    );
    this.loader = false;
  }
   makeFileRequest() {
    let obj = JSON.parse(localStorage.getItem("userInfo")).token.access_token;
    let url = this.base_path_service.base_path_api() + 'user/profile/?format=json'
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      formData.append("form_type", "on_boarding2");
      formData.append("cover_pic", this.id);
      formData.append("profile_pic", this.notification_logo);
      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            localStorage.setItem("profile_pic", JSON.parse(xhr.responseText)['profile_pic']);
            this.router.navigate(['/dashboard']);
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + obj);
      xhr.setRequestHeader("accept-language", localStorage.getItem('language') == 'en' ? 'en' : 'es');
      xhr.send(formData);
    });
  }




   unfollow_player(player: any) {
    this.loader = true;
    var url = this.base_path_service.base_path_api() + "user/player_follow/" + player.id + "/?format=json";
    this.base_path_service.DeleteRequest(url)
      .subscribe(
      res => {
        if (res[0].status == 205) {
          player.flag = false;
        }
      },
      err => {
      })
    this.loader = false;
  }

   unfollow_team(team: any) {
    this.loader = true;
    var url = this.base_path_service.base_path_api() + "user/team_follow/" + team.id + "/?format=json";
    this.base_path_service.DeleteRequest(url)
      .subscribe(
      res => {
        if (res[0].status == 205) {
          team.flag = false;
        }
      },
      err => {
      })
    this.loader = false;
  }

   fileChangeListener($event) {
    var image: any = new Image();
    var file: File = $event.target.files[0];
    this.image_name = file.name;
    var myReader: FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
      that.logo = image.src.split(",")[1];
    };
    myReader.readAsDataURL(file);
    this.showDialog();
  }

   showDialog() {
    this.display = true;
    this.backdrop = true;
  }
   save() {
    this.notification_logo = this.logo;
    this.image_name = this.file_name;
    this.display = false;
    this.backdrop = false
  }

   disable() {
    this.backdrop = false;
    this.display = false;
  }

   remove_img($event) {

  }

}
