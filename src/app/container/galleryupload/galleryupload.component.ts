import { Component, NgZone, OnInit, ViewChild } from "@angular/core";
import { ImageCropperComponent, CropperSettings } from "ng2-img-cropper";
import { GlobalService } from "../../GlobalService";
import { Http } from "@angular/http";
import { Router } from "@angular/router";
declare var $: any;

@Component({
  selector: "app-galleryupload",
  templateUrl: "./galleryupload.component.html",
  styleUrls: ["./galleryupload.component.css"]
})
export class GalleryuploadComponent implements OnInit {
  gallarydata: any;
  // heading:any;
  // description:any;
  featuredphoto = [];
  msgs: any;
  featured: boolean = true;
  featureheading: any = "";
  featuredescription: any = "";
  dialog: boolean = false;
  featuredialog: boolean = false;
  enable: boolean = false;
  coverPic: "";
  circleImg: boolean = false;
  squareImg: boolean = false;
  display: boolean = false;
  data: any;
  cropperSettings: CropperSettings;
  data2: any;
  cropperSettings2: CropperSettings;
  notification_logo: string = "";
  file_name: string;
  fileselected: boolean = false;
  image_name: string = "";
  logo = this.global.image_url;
  dataerror: boolean = false;
  boolfeature: boolean;
  @ViewChild("cropper", undefined)
  cropper: ImageCropperComponent;
  featureddata: any;
  picsdata: any;
  editdialog: boolean = false;

  totalPages: number;
  page: number = 1;
  pagecount: any;

  ptotalPages: number;
  ppage: number = 1;
  ppagecount: any;
  editId: any;
  editedimage: any;

  constructor(
    public zone: NgZone,
    public global: GlobalService,
    public router: Router
  ) {
    // this.rectangleCropImage();
    this.circleCropImage();
    this.dashboardcropImage();
    this.featuredphoto = [{ name: "fsdfsd" }, { name: "fsdfsd" }];
  }

  ngOnInit() {
    this.getpics();
    this.getfeatured();
  }
  addfeature() {
    this.featuredialog = true;
  }
  closeDialog() {
    this.featuredialog = false;
  }

  addpic() {
    this.dialog = true;
  }
  clossepic() {
    this.dialog = false;
  }

  // toggle() {
  //   this.featured = !this.featured;
  // }
  uploadfeature() {
    this.featured = true;
  }
  uploadgallery() {
    this.featured = false;
  }

  fileChangeListener(event, cover) {
    this.enable = true;
    this.image_name = event.target.files[0].name;
    this.coverPic = cover;
    if (cover == "coverPic") {
      this.circleImg = false;
      this.squareImg = true;
    } else {
      this.circleImg = true;
      this.squareImg = false;
    }
    var image: any = new Image();
    var file: File = event.target.files[0];
    var myReader: FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function(loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
      that.logo = image.src.split(",")[1];
    };
    myReader.readAsDataURL(file);
    this.showDialog();
  }
  showDialog() {
    this.display = true;
    this.enable = true;
  }
  disable() {
    this.display = false;
    this.enable = false;
  }
  save() {
    this.notification_logo = this.logo;
    this.image_name = this.file_name;
    this.display = false;
    this.enable = false;
    console.log(this.notification_logo, "file name selected");
    if (this.notification_logo != "") this.fileselected = true;
    else {
      this.fileselected = false;
    }
  }
  closeedit() {
    this.editdialog = false;
    this.featureheading = "";
    this.featuredescription = "";
    this.notification_logo = "";
  }
  edit(data) {
    this.editdialog = true;
    this.featureheading = data.headline;
    this.featuredescription = data.description;
    this.editId = data.id;
    this.editedimage = data.image;
    // this.notification_logo = data.image;

    if (data.featured_pic == true) {
      this.boolfeature = true;
    } else {
      this.boolfeature = false;
    }
  }

  editsave() {
    if (
      this.featureheading != "" ||
      (this.featureheading != undefined && this.featuredescription != "") ||
      this.featuredescription != undefined
    ) {
      let communityid = localStorage.getItem("communityid");
      let data = {
        form_type: "edit",
        image_id: this.editId,
        heading: this.featureheading,
        is_featured: this.boolfeature,
        description: this.featuredescription,
        community_id: communityid,
        image: this.notification_logo
      };
      var url = this.global.base_path + "api/user/gallery/";
      this.global.PostRequest(url, data).subscribe(
        res => {
          console.log(res, "res=================");
          let respons = res[0].json;
          this.featuredialog = false;
          this.featureheading = "";
          this.featuredescription = "";
          this.notification_logo = "";
          this.fileselected = false;
        },
        err => {
          if (err.status == 404) {
            this.msgs = [];
            this.msgs.push({
              severity: "error",
              summary: "",
              detail: "Network problem"
            });
          } else {
            this.msgs = [];
            this.msgs.push({
              severity: "error",
              summary: " ",
              detail: "some error, try again..."
            });
          }
        }
      );
    } else {
      this.dataerror = true;
    }
  }

  paginate(event) {
    this.page = event.page + 1;
    this.getfeatured();
    $("html, body").animate(
      {
        scrollTop: 0
      },
      600
    );
    return false;
  }
  getfeatured() {
    let url =
      this.global.base_path +
      "api/user/gallery/?form_type=featured&page=" +
      this.page;
    this.global.GetRequest(url).subscribe(
      res => {
        this.featureddata = res[0].json.data;
        this.totalPages = res[0].json.total_pages * 10;
        this.pagecount = res[0].json.total_pages;
      },
      err => {
        // this.loader = false;
      }
    );
  }
  paginatepic(event) {
    this.page = event.page + 1;
    this.getpics();
    $("html, body").animate(
      {
        scrollTop: 0
      },
      600
    );
    return false;
  }
  getpics() {
    let url =
      this.global.base_path +
      "api/user/gallery/?form_type=pictures&page=" +
      this.ppage;
    this.global.GetRequest(url).subscribe(
      res => {
        this.picsdata = res[0].json.data;
        this.ptotalPages = res[0].json.total_pages * 10;
        this.ppagecount = res[0].json.total_pages;
      },
      err => {
        // this.loader = false;
      }
    );
  }

  savefeature(data) {
    console.log(data, "feature geading");

    if (data == "pic") {
      this.boolfeature = false;
    } else {
      this.boolfeature = true;
    }
    if (
      this.featureheading != "" ||
      (this.featureheading != undefined && this.featuredescription != "") ||
      this.featuredescription != undefined
    ) {
      let communityid = localStorage.getItem("communityid");
      let data = {
        form_type: "create",
        heading: this.featureheading,
        is_featured: this.boolfeature,
        description: this.featuredescription,
        community_id: communityid,
        image: this.notification_logo
      };
      var url = this.global.base_path + "api/user/gallery/";
      this.global.PostRequest(url, data).subscribe(
        res => {
          console.log(res, "res=================");
          let respons = res[0].json;
          this.featuredialog = false;
          this.featureheading = "";
          this.featuredescription = "";
          this.notification_logo = "";
          this.fileselected = false;
          this.getfeatured();
          this.getpics();
        },
        err => {
          if (err.status == 404) {
            this.msgs = [];
            this.msgs.push({
              severity: "error",
              summary: "",
              detail: "Network problem"
            });
          } else {
            this.msgs = [];
            this.msgs.push({
              severity: "error",
              summary: " ",
              detail: "some error, try again..."
            });
          }
        }
      );
    } else {
      this.dataerror = true;
    }
  }

  getopenleagues() {
    let url = this.global.base_path + "api/user/gallery/";
    this.global.GetRequest(url).subscribe(
      res => {
        this.gallarydata = res[0].json;
      },
      err => {}
    );
  }

  public circleCropImage() {
    this.cropperSettings2 = new CropperSettings();
    this.cropperSettings2.width = 200;
    this.cropperSettings2.height = 200;
    this.cropperSettings2.keepAspect = false;

    this.cropperSettings2.croppedWidth = 200;
    this.cropperSettings2.croppedHeight = 200;

    this.cropperSettings2.canvasWidth = 350;
    this.cropperSettings2.canvasHeight = 300;

    this.cropperSettings2.minWidth = 100;
    this.cropperSettings2.minHeight = 100;

    this.cropperSettings2.rounded = true;
    this.cropperSettings2.minWithRelativeToResolution = true;

    this.cropperSettings2.cropperDrawSettings.strokeColor =
      "rgba(255,255,255,1)";
    this.cropperSettings2.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings2.noFileInput = true;

    this.data2 = {};
  }
  dashboardcropImage() {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 500;
    this.cropperSettings.height = 400;
    this.cropperSettings.keepAspect = false;

    this.cropperSettings.croppedWidth = 500;
    this.cropperSettings.croppedHeight = 400;

    this.cropperSettings.canvasWidth = 350;
    this.cropperSettings.canvasHeight = 400;

    this.cropperSettings.minWidth = 210;
    this.cropperSettings.minHeight = 250;

    this.cropperSettings.rounded = false;
    this.cropperSettings.minWithRelativeToResolution = false;

    this.cropperSettings.cropperDrawSettings.strokeColor =
      "rgba(255,255,255,1)";
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings.noFileInput = true;

    this.data = {};
  }
}
