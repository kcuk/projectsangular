import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryuploadComponent } from './galleryupload.component';

describe('GalleryuploadComponent', () => {
  let component: GalleryuploadComponent;
  let fixture: ComponentFixture<GalleryuploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryuploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryuploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
