import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from './../../GlobalService';
import { Http } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'app-email-verify',
  templateUrl: './email-verify.component.html',
  styleUrls: ['./email-verify.component.css']
})
export class EmailVerifyComponent implements OnInit {
  lang: string;
  uid: any;
  message:boolean=false;
  constructor(
    private translate: TranslateService,
    public http: Http, public global: GlobalService, public route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.uid = params.uid;
    });
    if (localStorage.getItem('language') != null) {
     this.translate.use(localStorage.getItem('language'));
    }
  }
  activate() {
    let url = this.global.base_path_api() + `peloteando/emailVerify/?uid=${this.uid}&format=json`;
    this.http.get(url)
      .subscribe(res => {
        if (res.json().status != 'is_activa') {
          this.router.navigateByUrl('/home');
        }else{
          this.message=true;
        }

      }, err => {
        console.log("error occurred");
      })
  }

}
