import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router'
import { GlobalService } from './../../GlobalService';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-followings',
  templateUrl: './followings.component.html',
  styleUrls: ['./followings.component.css']
})
export class FollowingsComponent implements OnInit {

   loader: boolean = false;
  id: number;
  followings: Array<any> = [{}];
  status: any;
  paramsSub: any;
  basePath: string;
  page: number = 1;
  page_no: number;
  msgs: Message[] = [];
  total_pages: number;
  no_of_player: number;

  constructor(private router: Router, private base_path_service: GlobalService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.loader = true;
    this.paramsSub = this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      this.status = params['status'];
    });
    this.followings = [];
    this.API_getPlayerFollowings();
    this.basePath = this.base_path_service.base_path;
  }

  followers(id: any, follow_status) {
    if (follow_status) {
      let url = this.base_path_service.base_path + 'api/user/player_follow/' + id + '/?format=json'
      this.base_path_service.DeleteRequest(url)
        .subscribe(res => {
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: '', detail: 'no seguir con éxito!' });
          this.API_getPlayerFollowing();
        },
        err => {
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores!' });
        });

    }
    else {
      let url = this.base_path_service.base_path + 'api/user/profile/?format=json'
      let data = { "form_type": "follow_player", "follow_player": id };
      this.base_path_service.PostRequest(url, data)
        .subscribe(res => {
          this.msgs = [];
          this.msgs.push({ severity: 'info', summary: '', detail: 'Seguido con éxito!' });
          this.API_getPlayerFollowing();
        },
        err => {
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: '', detail: 'Algunos errores!' });
        });
    }
  }

  API_getPlayerFollowings() {
    this.loader = true;
    let url = this.base_path_service.base_path_api() + 'user/player_follow/?form_type=following&player=' + this.id + '&page=' + this.page;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        this.followings = this.followings.concat(res[0].json.following);
        this.total_pages = res[0].json.total_pages;
        this.no_of_player = res[0].json.total;
        if (res[0].status == 200 || res[0].status == 201 || res[0].status == 205) {
          this.loader = false;
        }
      }, err => {
        this.loader = false;
      });
  }

  see() {
    this.page = this.page + 1;
    this.API_getPlayerFollowings();
  }
  API_getPlayerFollowing() {
    this.loader = true;
    for (let i = 1; i <= this.page; i++) {
      let url = this.base_path_service.base_path_api() + 'user/player_follow/?form_type=following&player=' + this.id + '&page=' + i;
      this.base_path_service.GetRequest(url)
        .subscribe(res => {
          if (res[0].status == 200 || res[0].status == 201 || res[0].status == 205) {
            this.followings = res[0].json.following;
            this.loader = false;
            this.total_pages = res[0].json.total_pages;
            this.no_of_player = res[0].json.total;
          }
        }, err => {
          this.loader = false;
        });
    }
  }

  player_profile() {
    this.router.navigateByUrl('/dashboard/player/player-profile/' + this.id);
  }
  redirectProfile(id:number){
    this.router.navigateByUrl('/dashboard/player/player-profile/' + id);
  }
}
