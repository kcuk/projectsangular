import { Component, ViewChild, Renderer2, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../GlobalService';
import { Http } from '@angular/http';
@Component({
  selector: 'app-email-otp-verify',
  templateUrl: './email-otp-verify.component.html',
  styleUrls: ['./email-otp-verify.component.css']
})
export class EmailOtpVerifyComponent {
  data: any;
  mask: any = '9';
  @ViewChild('input1') first: any;
  @ViewChild('input2') second: any;
  @ViewChild('input3') third: any;
  @ViewChild('input4') fourth: any;
  d1;
  d2;
  d3;
  d4;
  loader: boolean;
  count: number = 30;

  public msgs: any;
  constructor(
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    private renderer: Renderer2,
    public ngzone: NgZone,
    public router: Router,
  ) {
    this.data = JSON.parse(routes.snapshot.paramMap.get('data'));
    setInterval(() => {
      this.count--;
    }, 1000)
  }




  ngAfterViewInit() {
    setTimeout(() => {
      this.first.inputViewChild.nativeElement.focus();
    }, 500)
  }

  checkOtp(check, event) {
    console.log("event", event)
    this.ngzone.run(() => {
      if (event.keyCode >= 48 && event.keyCode <= 57) {
        if (check == 1) {
          this.second.inputViewChild.nativeElement.focus();
        }
        else if (check == 2) {
          this.third.inputViewChild.nativeElement.focus();
        }
        else if (check == 3) {
          this.fourth.inputViewChild.nativeElement.focus();
        }
      }
      else {
        return false
      }
    })
  }

  verifyOtp() {
    if (this.d1 && this.d2 && this.d3 && this.d4) {
      this.loader = true;
      let otp = this.d1 + "" + this.d2 + "" + this.d3 + "" + this.d4;
      let url = this.global.base_path_api() + "peloteando/mobile_otp/?format=json";
      let data = {
        "email": this.data.email,
        "form_type": "verify_otp",
        "register_type": "email",
        "otp": parseInt(otp)
      }
      this.global.PostRequestUnauthorised(url, data)
        .subscribe(res => {
          console.log("otp verify=>>>>>>>>>>>>>>>>>");
          this.router.navigate(['/home/signup/email-password', { data: JSON.stringify(this.data) }])
        }, err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({
            severity: 'error',
            summary: (localStorage.getItem('language') == 'es' ? "Ingrese OTP correcta" : "Enter correct OTP"),
          });
        })
    }
  }




  resendOtp() {
    if (this.count <= 0) {
      this.loader = true;
      let url = this.global.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
      let data = {
        "form_type": "send_otp",
        "email": this.data.email,
        "register_type": "email",
      }
      this.global.PostRequestUnauthorised(url, data)
        .subscribe(res => {
          this.loader = false;
          this.count = 30;
          this.msgs = [];
          this.msgs.push({
            severity: 'success',
            summary: localStorage.getItem('language') == 'es' ? "OTP enviado" : "OTP sent",
          });
        }, error => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({
            severity: 'error',
            summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
          });

        })
    }
  }

}
