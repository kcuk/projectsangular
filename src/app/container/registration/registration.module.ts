import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "./../../shared/shared.module";
import { Routes, RouterModule } from "@angular/router";
import { RegistrationComponent } from "./registration.component";
import { Onboading1Component } from "./onboading1/onboading1.component";
import { Onboading2Component } from "./onboading2/onboading2.component";
import { PhoneOtpVerifyComponent } from "./phone-otp-verify/phone-otp-verify.component";
import { EmailOtpVerifyComponent } from "./email-otp-verify/email-otp-verify.component";
import { EmailPasswordComponent } from "./email-password/email-password.component";
import { SetLocationComponent } from "./set-location/set-location.component";
import { RegistrationSuccessComponent } from "./registration-success/registration-success.component";
import { SetEmailComponent } from "./set-email/set-email.component";
import { SetPhoneComponent } from "./set-phone/set-phone.component";
import { SetPhoneOtpComponent } from "./set-phone-otp/set-phone-otp.component";
import { SetEmailOtpComponent } from "./set-email-otp/set-email-otp.component";
import { SetEmailPasswordComponent } from "./set-email-password/set-email-password.component";
import { SetUserComponent } from "./set-user/set-user.component";
import { SetGenderComponent } from "./set-gender/set-gender.component";
import { SetHeightComponent } from "./set-height/set-height.component";
import { SetDobComponent } from "./set-dob/set-dob.component";
import { SetLegComponent } from "./set-leg/set-leg.component";
import { SetPositionComponent } from "./set-position/set-position.component";
import { SetProfilePicComponent } from "./set-profile-pic/set-profile-pic.component";

import { ImageCropperModule } from "ngx-image-cropper";

export const registration_routes: Routes = [
  { path: "", component: RegistrationComponent },
  { path: "onboading1", component: Onboading1Component },
  { path: "onboading2", component: Onboading2Component },
  { path: "phone-otp", component: PhoneOtpVerifyComponent },
  { path: "email-otp", component: EmailOtpVerifyComponent },
  { path: "email-password", component: EmailPasswordComponent },
  { path: "set-location", component: SetLocationComponent },
  { path: "registration-success", component: RegistrationSuccessComponent },
  { path: "set-email", component: SetEmailComponent },
  { path: "set-phone", component: SetPhoneComponent },
  { path: "set-phone-otp", component: SetPhoneOtpComponent },
  { path: "set-email-otp", component: SetEmailOtpComponent },
  { path: "set-email-password", component: SetEmailPasswordComponent },
  { path: "set-user", component: SetUserComponent },
  { path: "set-gender", component: SetGenderComponent },
  { path: "set-height", component: SetHeightComponent },
  { path: "set-dob", component: SetDobComponent },
  { path: "set-leg", component: SetLegComponent },
  { path: "set-position", component: SetPositionComponent },
  { path: "set-profile-pic", component: SetProfilePicComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(registration_routes),
    SharedModule.forRoot(),
    ImageCropperModule
  ],
  declarations: [
    RegistrationComponent,
    Onboading1Component,
    Onboading2Component,
    EmailPasswordComponent,
    PhoneOtpVerifyComponent,
    EmailOtpVerifyComponent,
    SetLocationComponent,
    RegistrationSuccessComponent,
    SetEmailComponent,
    SetPhoneComponent,
    SetPhoneOtpComponent,
    SetEmailOtpComponent,
    SetEmailPasswordComponent,
    SetUserComponent,
    SetGenderComponent,
    SetHeightComponent,
    SetDobComponent,
    SetLegComponent,
    SetPositionComponent,
    SetProfilePicComponent
  ]
})
export class RegistrationModule {}
