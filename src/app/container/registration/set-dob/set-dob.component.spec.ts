import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetDobComponent } from './set-dob.component';

describe('SetDobComponent', () => {
  let component: SetDobComponent;
  let fixture: ComponentFixture<SetDobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetDobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetDobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
