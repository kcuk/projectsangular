import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetHeightComponent } from './set-height.component';

describe('SetHeightComponent', () => {
  let component: SetHeightComponent;
  let fixture: ComponentFixture<SetHeightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetHeightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetHeightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
