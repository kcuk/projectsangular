import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetGenderComponent } from './set-gender.component';

describe('SetGenderComponent', () => {
  let component: SetGenderComponent;
  let fixture: ComponentFixture<SetGenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetGenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetGenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
