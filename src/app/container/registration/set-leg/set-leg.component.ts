import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../../GlobalService';
import { Http } from '@angular/http';

@Component({
  selector: 'app-set-leg',
  templateUrl: './set-leg.component.html',
  styleUrls: ['./set-leg.component.css']
})
export class SetLegComponent  {
  profile_complete_percentage: any;
  loader: boolean;
  msgs: any[];
  leg: any = "Izquierda";

  constructor(
    public router: Router,
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    public zone: NgZone,
  ) {
    this.profile_complete_percentage = JSON.parse(localStorage.getItem('user_info')).profile_percentage;

   }

  
   setLeg(skip) {
    this.loader = true;
    let data = {
      "page": "leg",
      "leg": this.leg,
      "skipped": skip,
      "request_from":localStorage.getItem('checkoptionalpage')
    }
    let url = this.global.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.global.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.zone.run(() => {
          localStorage.setItem('user_info', JSON.stringify(res[0].json.data));
          let user_info = res[0].json.data;
          if (!user_info.is_position) {
            this.router.navigate(['/home/signup/set-position'])
          }
          else if (!user_info.is_profile_pic) {
            this.router.navigate(['/home/signup/set-profile-pic'])
          }
          else {
            localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
            // localStorage.removeItem('user_info');
            // this.router.navigate(['/dashboard'])
            let comdata = JSON.parse(localStorage.getItem("community_info"));
            console.log(comdata, "fgewguigewewrgwue");
            let comname = comdata.community_name;
            console.log(comname, "community name we get======================");
            this.router.navigate([comname]);
          }
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }


  skipAll() {
    this.loader = true;
    let url = this.global.base_path_api() + "peloteando/skip_details/?format=json";
    this.global.PostRequest(url, {})
      .subscribe(res => {
        this.loader = false;
        this.zone.run(()=>{
          localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
          // localStorage.removeItem('user_info');
          // this.router.navigate(['/dashboard'])
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          console.log(comdata, "fgewguigewewrgwue");
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.router.navigate([comname]);
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }
}
