import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetLegComponent } from './set-leg.component';

describe('SetLegComponent', () => {
  let component: SetLegComponent;
  let fixture: ComponentFixture<SetLegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetLegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetLegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
