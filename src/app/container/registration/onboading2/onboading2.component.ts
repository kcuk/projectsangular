import { Component, OnInit } from '@angular/core';
import { countryCode } from "./country_code"
import { GlobalService } from '../../../GlobalService';
import { Router, Routes, ActivatedRoute } from '@angular/router';
import { SelectItem, Message } from 'primeng/primeng';
import { Http } from '@angular/http';

@Component({
  selector: 'app-onboading2',
  templateUrl: './onboading2.component.html',
  styleUrls: ['./onboading2.component.css']
})
export class Onboading2Component implements OnInit {
  fullname: string;
  email_exist: boolean;
  check_email: boolean = false;
  codes: SelectItem[] = [];
  mobile: any;
  email: any;
  is_phone: boolean = true;
  lang: string;
  mask: any = '999999999999999'
  country_name: string;
  country_code: string;
  number: any;
  checkNumber: boolean = false;
  checkNumberErrorMsg: boolean = false;
  loader: boolean;
  public msgs: any;

  constructor(
    public router: Router,
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http
  ) {
    this.fullname = routes.snapshot.paramMap.get('fullname');
    this.lang = localStorage.getItem('language');
    console.log("dskhduhgfdyd", this.fullname);
  }

  ngOnInit() {
    this.country_code = localStorage.getItem("country_dial_code");
    countryCode.map((val) => {
      this.codes.push({
        label: val.name + " " + "(" + val.dial_code + ")",
        value: val.dial_code
      })
    })
  }

  validatePhone(event) {
    if (event.keyCode == 13) {
      if (this.checkNumber) {
        this.next('mobile')
      }
    }
    else {
      let result = event.target.value;
      this.number = result;
      if (this.number.length >= 8) {
        this.checkNumber = true;
        this.checkNumberErrorMsg = false;
      }
      else {
        this.checkNumber = false;
        this.checkNumberErrorMsg = false;
      }
    }
  }

  next(check) {
    if (check == 'mobile') {
      // this.checkMobile();
         this.sendOtp();
    }
    else {
      this.setEmail();
      // this.validateEmail();
    }
  }

  checkMobile() {
    this.loader = true
    let url = this.global.base_path_api() + "peloteando/verification/?mobile=" + parseInt(this.mobile) + "&country_code=" + this.country_code + "&format=json";
    this.http.get(url)
      .subscribe(res => {
        this.checkNumberErrorMsg = false;
        this.sendOtp();
      }, err => {
        this.loader = false;
        this.checkNumberErrorMsg = true;
        this.checkNumber = false;
      })
  }

  sendOtp() {
    let url = this.global.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
    let data = {
      "phone": this.mobile,
      "country_code": this.country_code,
      "form_type": "send_otp",
      "register_type": "phone",
    }
    this.global.PostRequestUnauthorised(url, data)
      .subscribe(res => {
        this.loader = false;
        console.log("otppppppppppppppppppppp");
        let data = {
          "page": "init_auth",
          "form_type": "phone",
          "full_name": this.fullname,
          "phone": this.mobile,
          "country_code": this.country_code
        }
        this.msgs = [];
        this.msgs.push({
          severity: 'success',
          summary: localStorage.getItem('language') == 'es' ? "OTP enviado" : "OTP sent",
        });
        this.router.navigate(['/home/signup/phone-otp', { data: JSON.stringify(data) }])
      }, error => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

  validate(event) {
    if (event.keyCode == 13) {
      if (this.check_email) {
        this.validateEmail()
      }
    }
    else {
      if (/[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+$/.test(this.email)) {
        this.check_email = true;
        this.email_exist = false;
      }
      else {
        this.check_email = false;
        this.email_exist = false;
      }
    }
  }

  validateEmail() {
    this.loader = true;
    let url = this.global.base_path_api() + "peloteando/verification/?email=" + this.email + "&format=json";
    this.http.get(url)
      .subscribe(res => {
        this.setEmail();
      }, err => {
        this.loader = false;
        this.email_exist = true;
        this.check_email = false;
      })
  }


  setEmail() {
    let url = this.global.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
    let data = {
      "form_type": "send_otp",
      "email": this.email,
      "register_type": "email",
    }
    this.global.PostRequestUnauthorised(url, data)
      .subscribe(res => {
        this.loader = false;
        let data = {
          "page": "init_auth",
          "form_type": "email",
          "full_name": this.fullname,
          "email": this.email,
        }
        this.msgs = [];
        this.msgs.push({
          severity: 'success',
          summary: localStorage.getItem('language') == 'es' ? "OTP enviado" : "OTP sent",
        });
        this.router.navigate( ['/home/signup/email-otp', { data: JSON.stringify(data) }])
      }, error => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

  termCondition(){
    this.router.navigateByUrl('/home/terms')

  }
  privacyPolicy(){
    this.router.navigateByUrl('/home/privacy-policy')

  }

}
