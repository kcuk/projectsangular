import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Onboading2Component } from './onboading2.component';

describe('Onboading2Component', () => {
  let component: Onboading2Component;
  let fixture: ComponentFixture<Onboading2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Onboading2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Onboading2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
