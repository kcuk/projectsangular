import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-onboading1',
  templateUrl: './onboading1.component.html',
  styleUrls: ['./onboading1.component.css']
})
export class Onboading1Component implements OnInit {
  full_name_error: boolean;
  full_name_valid: boolean;
  lang: any = localStorage.getItem("devicelang");
  full_name: any;
  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  validateFullName(event) {
    this.full_name_error = false;
    if (event.keyCode == 13) {
      this.next();
    }
    else {
      if (event.target.value.length > 40) {
        this.full_name = this.full_name.slice(0, 40);
      }
      if (/^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$/.test(event.target.value)) {
        this.full_name_valid = true;
      }
      else {
        this.full_name_valid = false;
      }
    }
  }

  next() {
    console.log("dhujgdhdd",this.full_name)
    if (/^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$/.test(this.full_name)) {
      this.router.navigate(['/home/signup/onboading2',{fullname:this.full_name}])
    }
    else {
      this.full_name_error = true;
    }

  }

  
}
