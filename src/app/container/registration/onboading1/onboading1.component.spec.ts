import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Onboading1Component } from './onboading1.component';

describe('Onboading1Component', () => {
  let component: Onboading1Component;
  let fixture: ComponentFixture<Onboading1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Onboading1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Onboading1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
