import { Component, ViewChild, Renderer2, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../GlobalService';
import { Http } from '@angular/http';
@Component({
  selector: 'app-set-email-password',
  templateUrl: './set-email-password.component.html',
  styleUrls: ['./set-email-password.component.css']
})
export class SetEmailPasswordComponent {
  data: any;
  check_password: boolean;
  password = "";
  loader: boolean;
  public msgs: any;
  show_password:boolean=false;

  constructor(
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    private renderer: Renderer2,
    public ngzone: NgZone,
    public router: Router,
  ) {
    this.data = JSON.parse(routes.snapshot.paramMap.get('data'));
  }

  validate(event) {
    let password_length = event.target.value.length;
    if (password_length >= 6) {
      this.check_password = true;
    }
    else {
      this.check_password = false;
    }
  }

  next() {
    if (this.password.length >= 6) {
      this.getToken();
    }
    else {
      this.msgs = [];
      this.msgs.push({
        severity: 'error',
        summary: (localStorage.getItem('language') == 'es' ? "La contraseña debe ser mayor a 5 caracteres" : "Password must be greater than 5 character"),
      });

    }
  }

  getToken() {
    this.loader = true;
    this.data.password = this.password;
    let url1 = this.global.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.global.PostRequest (url1, this.data)
      .subscribe(res => {
        this.ngzone.run(() => {
          this.loader = false;
          localStorage.setItem('user_info', JSON.stringify(res[0].json.data))
          let user_info = JSON.parse(localStorage.getItem('user_info'));
          if (!user_info.is_username) {
            this.router.navigate(['/home/signup/set-user'])

          }
          else if (!user_info.is_gender) {
            this.router.navigate(['/home/signup/set-gender'])
          }
          else if (!user_info.is_height) {
            this.router.navigate(['/home/signup/set-height'])
          }
          else if (!user_info.is_dob) {
            this.router.navigate(['/home/signup/set-dob'])
          }
          else if (!user_info.is_leg) {
            this.router.navigate(['/home/signup/set-leg'])
          }
          else if (!user_info.is_position) {
            this.router.navigate(['/home/signup/set-position'])
          }
          else if (!user_info.is_profile_pic) {
            this.router.navigate(['/home/signup/set-profile-pic'])
          }
          else {
            localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
            // localStorage.removeItem('user_info');
            // this.router.navigate(['/dashboard'])
            let comdata = JSON.parse(localStorage.getItem("community_info"));
            console.log(comdata, "fgewguigewewrgwue");
            let comname = comdata.community_name;
            console.log(comname, "community name we get======================");
            this.router.navigate([comname]);
          }
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

}
