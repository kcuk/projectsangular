import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetEmailPasswordComponent } from './set-email-password.component';

describe('SetEmailPasswordComponent', () => {
  let component: SetEmailPasswordComponent;
  let fixture: ComponentFixture<SetEmailPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetEmailPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetEmailPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
