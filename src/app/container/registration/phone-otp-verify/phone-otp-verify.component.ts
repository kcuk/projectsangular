import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2,
  Renderer,
  NgZone
} from "@angular/core";
import { Route, ActivatedRoute, Router } from "@angular/router";
import { ActionSequence } from "protractor";
import { GlobalService } from "../../../GlobalService";
import { Http } from "@angular/http";
import { Message } from "primeng/primeng";
import { TranslateService } from "ng2-translate";

@Component({
  selector: "app-phone-otp-verify",
  templateUrl: "./phone-otp-verify.component.html",
  styleUrls: ["./phone-otp-verify.component.css"]
})
export class PhoneOtpVerifyComponent {
  data: any;
  mask: any = "9";
  @ViewChild("input1")
  first: any;
  @ViewChild("input2")
  second: any;
  @ViewChild("input3")
  third: any;
  @ViewChild("input4")
  fourth: any;
  d1;
  d2;
  d3;
  d4;
  loader: boolean;
  display: boolean = false;
  count: number = 30;
  displayrequest: boolean = false;

  public msgs: Message[] = [];

  constructor(
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    private renderer: Renderer2,
    public ngzone: NgZone,
    public router: Router,
    public translate: TranslateService
  ) {
    if (localStorage.getItem("language") != null) {
      console.log("language1111111111111");
      translate.use(localStorage.getItem("language"));
    }
    this.data = JSON.parse(routes.snapshot.paramMap.get("data"));
    setInterval(() => {
      this.count--;
    }, 1000);
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.first.inputViewChild.nativeElement.focus();
    }, 500);
  }

  checkOtp(check, event) {
    console.log("event", event);
    this.ngzone.run(() => {
      if (event.keyCode >= 48 && event.keyCode <= 57) {
        if (check == 1) {
          this.second.inputViewChild.nativeElement.focus();
        } else if (check == 2) {
          this.third.inputViewChild.nativeElement.focus();
        } else if (check == 3) {
          this.fourth.inputViewChild.nativeElement.focus();
        }
      } else {
        return false;
      }
    });
  }

  verifyOtp() {
    if (this.d1 && this.d2 && this.d3 && this.d4) {
      this.loader = true;
      let otp = this.d1 + "" + this.d2 + "" + this.d3 + "" + this.d4;
      let url =
        this.global.base_path_api() + "peloteando/mobile_otp/?format=json";
      let data = {
        phone: this.data.phone,
        country_code: this.data.country_code,
        form_type: "verify_otp",
        register_type: "phone",
        otp: parseInt(otp)
      };
      this.global.PostRequestUnauthorised(url, data).subscribe(
        res => {
          console.log("otp verify=>>>>>>>>>>>>>>>>>");
          this.getToken();
        },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary:
              localStorage.getItem("language") == "es"
                ? "Ingrese OTP correcta"
                : "Enter correct OTP"
          });
        }
      );
    }
  }

  getToken() {
    console.log(this.data, "tabhjfghkjk===========>>>>>>");
    let communitydata = {
      country_code: this.data.country_code,
      form_type: this.data.form_type,
      full_name: this.data.full_name,
      page: this.data.page,
      phone: this.data.phone,
      community_id: localStorage.getItem("communityid"),
      player_type: localStorage.getItem("signupcategory"),
      team_id: localStorage.getItem("signupteamid"),
      created_team: localStorage.getItem("createdteamid"),
      language: localStorage.getItem("language")
    };

    let url1 =
      this.global.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.global.PostRequestUnauthorised(url1, communitydata).subscribe(
      res => {
        this.loader = false;
        let lang = localStorage.getItem("language");
        let country = localStorage.getItem("country_code_with_name");
        let country_code = localStorage.getItem("country_dial_code");
        let distance = localStorage.getItem("distance");
        // localStorage.clear();
        localStorage.setItem("country_code_with_name", country);
        localStorage.setItem("country_dial_code", country_code);
        localStorage.setItem("distance", distance);
        localStorage.setItem("language", lang);
        let data = res[0].json;
        let expire = new Date().getTime() / 1000 + data.token.expires_in;
        localStorage.setItem("userInfo", JSON.stringify(data));
        localStorage.setItem("user_info", JSON.stringify(data.info));
        localStorage.setItem("timeout", JSON.stringify(expire));
        localStorage.setItem(
          "nick_name",
          JSON.stringify(res[0].json.info.nick_name)
        );
        localStorage.setItem("username", data.info.username);
        localStorage.setItem(
          "profile_percentage",
          data.info.profile_percentage
        );
        localStorage.setItem("checkoptionalpage", "signup");
        localStorage.setItem("last_name", res[0].json.info.last_name);
        localStorage.setItem(
          "community_info",
          JSON.stringify(data.info.community_info)
        );
        localStorage.removeItem("signupteamid");
        localStorage.removeItem("createdteamid");
        let id_com = localStorage.getItem("communityid");
        if (id_com == this.global.peloteaid) {
          let url =
            this.global.base_path_api() +
            "user/setLanguage/?language=" +
            localStorage.getItem("language");
          this.global.GetRequest(url).subscribe(res => {
            this.loader = false;
            this.router.navigate(["/home/signup/set-location"]);
          });
        } else {
          this.showDialog();
        }
      },
      err => {
        console.log(err.status, "error in signup");
        if (err.status == 401) {
          this.loader = false;
          console.log("password error==================");
        } else if (err.status == 403) {
          this.display = true;
          this.loader = false;
        } else if (err.status == 400) {
          this.displayrequest = true;
          this.loader = false;
        } else {
          this.loader = false;
          console.log(err, "error message");
        }
      }
    );
  }
  donetwo() {
    this.loader = false;
    localStorage.removeItem("user_info");
    this.router.navigate(["/home"]);
    this.display = false;
  }
  resendOtp() {
    if (this.count <= 0) {
      this.loader = true;
      let url =
        this.global.base_path_api() +
        "peloteando/mobile_otp/?language=" +
        localStorage.getItem("language") +
        "&format=json";
      let data = {
        form_type: "send_otp",
        phone: this.data.phone,
        country_code: this.data.country_code,
        register_type: "phone"
      };
      this.global.PostRequestUnauthorised(url, data).subscribe(
        res => {
          this.loader = false;
          this.count = 30;
          console.log("resnssssssssssss");
          this.msgs = [];
          this.msgs.push({
            severity: "success",
            summary:
              localStorage.getItem("language") == "es"
                ? "OTP enviado"
                : "OTP sent"
          });
        },
        err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({
            severity: "error",
            summary:
              localStorage.getItem("language") == "es"
                ? "Error, inténtalo de nuevo más tarde"
                : "Error, Try again later"
          });
        }
      );
    }
  }
  showDialog() {
    this.display = true;
  }
  done() {
    let url =
      this.global.base_path_api() +
      "user/setLanguage/?language=" +
      localStorage.getItem("language");
    this.global.GetRequest(url).subscribe(res => {
      this.loader = false;
      // this.router.navigate(["/home/signup/set-location"]);
      localStorage.removeItem("user_info");
      this.router.navigate(["/home"]);
    });
    this.display = false;
  }
  closeDialog() {
    this.display = false;
  }
}
