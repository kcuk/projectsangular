import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetPhoneComponent } from './set-phone.component';

describe('SetPhoneComponent', () => {
  let component: SetPhoneComponent;
  let fixture: ComponentFixture<SetPhoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetPhoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetPhoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
