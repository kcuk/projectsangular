import { Component, OnInit, NgZone } from '@angular/core';
import { countryCode } from "../onboading2/country_code"
import { GlobalService } from '../../../GlobalService';
import { Router, Routes, ActivatedRoute } from '@angular/router';
import { SelectItem, Message } from 'primeng/primeng';
import { Http } from '@angular/http';
@Component({
  selector: 'app-set-phone',
  templateUrl: './set-phone.component.html',
  styleUrls: ['./set-phone.component.css']
})
export class SetPhoneComponent implements OnInit {
  codes: SelectItem[] = [];
  mobile: any;
  mask: any = '999999999999999'
  country_code: string;
  checkNumber: boolean = false;
  checkNumberErrorMsg: boolean = false;
  loader: boolean;
  public msgs: any;
  number: any;
  profile_complete_percentage: any;
  constructor(
    public router: Router,
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    public zone:NgZone
  ) {
    this.profile_complete_percentage = JSON.parse(localStorage.getItem('user_info')).profile_percentage;
   }

  ngOnInit() {
    this.country_code = localStorage.getItem("country_dial_code");
    countryCode.map((val) => {
      this.codes.push({
        label: val.name + " " + "(" + val.dial_code + ")",
        value: val.dial_code
      })
    })
  }

  validatePhone(event) {
    if (event.keyCode == 13) {
      if (this.checkNumber) {
        this.next()
      }
    }
    else {
      let result = event.target.value;
      this.number = result;
      if (this.number.length >= 8) {
        this.checkNumber = true;
        this.checkNumberErrorMsg = false;
      }
      else {
        this.checkNumber = false;
        this.checkNumberErrorMsg = false;
      }
    }
  }

  next() {
    this.loader = true
    let url = this.global.base_path_api() + "peloteando/verification/?mobile=" + parseInt(this.mobile) + "&country_code=" + this.country_code + "&format=json";
    this.http.get(url)
      .subscribe(res => {
        this.checkNumberErrorMsg = false;
        this.sendOtp();
      }, err => {
        this.loader = false;
        this.checkNumberErrorMsg = true;
        this.checkNumber = false;
      })
  }

  sendOtp() {
    let url = this.global.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
    let data = {
      "phone": this.mobile,
      "country_code": this.country_code,
      "form_type": "send_otp",
      "register_type": "phone",
    }
    this.global.PostRequestUnauthorised(url, data)
      .subscribe(res => {
        this.loader = false;
        console.log("otppppppppppppppppppppp");
        let data = {
          "page": "optional",
          "form_type": "phone",
          "skipped": false,
          "phone": this.mobile,
          "country_code": this.country_code,
          "request_from":localStorage.getItem('checkoptionalpage')
        }
        this.msgs = [];
        this.msgs.push({
          severity: 'success',
          summary: localStorage.getItem('language') == 'es' ? "OTP enviado" : "OTP sent",
        });
        this.router.navigate(['/home/signup/set-phone-otp', { data: JSON.stringify(data) }])
      }, error => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

  skip() {
    this.loader = true;
    let data = {
      "page": "optional",
      "form_type": "phone",
      "skipped": true,
      "phone": this.mobile,
      "country_code": this.country_code,
      "request_from":localStorage.getItem('checkoptionalpage')
    }
    let url = this.global.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.global.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.zone.run(() => {
          localStorage.setItem('user_info', JSON.stringify(res[0].json.data));
          let user_info = res[0].json.data;
          if (!user_info.is_username) {
            this.router.navigate(['/home/signup/set-user'])
          }
          else if (!user_info.is_gender) {
            this.router.navigate(['/home/signup/set-gender'])
          }
          else if (!user_info.is_height) {
            this.router.navigate(['/home/signup/set-height'])
          }
          else if (!user_info.is_dob) {
            this.router.navigate(['/home/signup/set-dob'])
          }
          else if (!user_info.is_leg) {
            this.router.navigate(['/home/signup/set-leg'])
          }
          else if (!user_info.is_position) {
            this.router.navigate(['/home/signup/set-position'])
          }
          else if (!user_info.is_profile_pic) {
            this.router.navigate(['/home/signup/set-profile-pic'])
          }
          else {
            localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
            // localStorage.removeItem('user_info');
            // this.router.navigate(['/dashboard'])
            let comdata = JSON.parse(localStorage.getItem("community_info"));
            console.log(comdata, "fgewguigewewrgwue");
            let comname = comdata.community_name;
            console.log(comname, "community name we get======================");
            this.router.navigate([comname]);
          }
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: (localStorage.getItem('language') == 'es' ? "Ingrese OTP correcta" : "Enter correct OTP"),
        });
      })
  }


  skipAll() {
    this.loader = true;
    let url = this.global.base_path_api() + "peloteando/skip_details/?format=json";
    this.global.PostRequest(url, {})
      .subscribe(res => {
        this.loader = false;
        this.zone.run(()=>{
          localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
          // localStorage.removeItem('user_info');
          // this.router.navigate(['/dashboard'])
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          console.log(comdata, "fgewguigewewrgwue");
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.router.navigate([comname]);
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

}
