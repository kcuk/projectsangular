import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../../GlobalService';
import { Http } from '@angular/http';

@Component({
  selector: 'app-set-email',
  templateUrl: './set-email.component.html',
  styleUrls: ['./set-email.component.css']
})
export class SetEmailComponent implements OnInit {
  email_exist: boolean;
  check_email: boolean;
  loader: boolean;
  email: string;
  msgs: any[];
  profile_complete_percentage: any;

  constructor(
    public router: Router,
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    public zone: NgZone,
  ) { 
    this.profile_complete_percentage = JSON.parse(localStorage.getItem('user_info')).profile_percentage;
  }

  ngOnInit() {
  }

  validate(event) {
    if (event.keyCode == 13) {
      if (this.check_email) {
        this.validateEmail()
      }
    }
    else {
      if (/[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+$/.test(this.email)) {
        this.check_email = true;
        this.email_exist = false;
      }
      else {
        this.check_email = false;
        this.email_exist = false;
      }
    }
  }

  validateEmail() {
    this.loader = true;
    let url = this.global.base_path_api() + "peloteando/verification/?email=" + this.email + "&format=json";
    this.http.get(url)
      .subscribe(res => {
        this.setEmail();
      }, err => {
        this.loader = false;
        this.email_exist = true;
        this.check_email = false;
      })
  }


  setEmail() {
    let url = this.global.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
    let data = {
      "form_type": "send_otp",
      "email": this.email,
      "register_type": "email",
    }
    this.global.PostRequestUnauthorised(url, data)
      .subscribe(res => {
        this.loader = false;
        let data = {
          "page": "optional",
          "form_type": "email",
          "email": this.email,
          "skipped": false
        }
        this.msgs = [];
        this.msgs.push({
          severity: 'success',
          summary: localStorage.getItem('language') == 'es' ? "OTP enviado" : "OTP sent",
        });
        this.router.navigate(['/home/signup/set-email-otp', { data: JSON.stringify(data) }])
      }, error => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

  skip() {
    this.loader = true;
    let data = {
      "page": "optional",
      "form_type": "email",
      "email": this.email,
      "skipped": true,
      "request_from": localStorage.getItem('checkoptionalpage')
    }
    let url = this.global.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.global.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.zone.run(() => {
          localStorage.setItem('user_info', JSON.stringify(res[0].json.data));
          let user_info = res[0].json.data;
          if (!user_info.is_username) {
            this.router.navigate(['/home/signup/set-user'])
          }
          else if (!user_info.is_gender) {
            this.router.navigate(['/home/signup/set-gender'])
          }
          else if (!user_info.is_height) {
            this.router.navigate(['/home/signup/set-height'])
          }
          else if (!user_info.is_dob) {
            this.router.navigate(['/home/signup/set-dob'])
          }
          else if (!user_info.is_leg) {
            this.router.navigate(['/home/signup/set-leg'])
          }
          else if (!user_info.is_position) {
            this.router.navigate(['/home/signup/set-position'])
          }
          else if (!user_info.is_profile_pic) {
            this.router.navigate(['/home/signup/set-profile-pic'])
          }
          else {
            localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
            // localStorage.removeItem('user_info');
            // this.router.navigate(['/dashboard'])
            let comdata = JSON.parse(localStorage.getItem("community_info"));
            console.log(comdata, "fgewguigewewrgwue");
            let comname = comdata.community_name;
            console.log(comname, "community name we get======================");
            this.router.navigate([comname]);
          }
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: (localStorage.getItem('language') == 'es' ? "Ingrese OTP correcta" : "Enter correct OTP"),
        });
      })
  }


  skipAll() {
    this.loader = true;
    let url = this.global.base_path_api() + "peloteando/skip_details/?format=json";
    this.global.PostRequest(url, {})
      .subscribe(res => {
        this.loader = false;
        this.zone.run(()=>{
          localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
          // localStorage.removeItem('user_info');
          // this.router.navigate(['/dashboard'])
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          console.log(comdata, "fgewguigewewrgwue");
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.router.navigate([comname]);
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }
 


}
