import { Component, ViewChild, Renderer2, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../GlobalService';
import { Http } from '@angular/http';
@Component({
  selector: 'app-set-phone-otp',
  templateUrl: './set-phone-otp.component.html',
  styleUrls: ['./set-phone-otp.component.css']
})
export class SetPhoneOtpComponent {
  data: any;
  mask: any = '9';
  @ViewChild('input1') first: any;
  @ViewChild('input2') second: any;
  @ViewChild('input3') third: any;
  @ViewChild('input4') fourth: any;
  d1;
  d2;
  d3;
  d4;
  loader: boolean;
  public msgs: any;
  count:number=30;
  constructor(
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    private renderer: Renderer2,
    public ngzone: NgZone,
    public router: Router,
  ) {
    this.data = JSON.parse(routes.snapshot.paramMap.get('data'));
    setInterval(() => {
      this.count--;
    }, 1000)
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.first.inputViewChild.nativeElement.focus();
    }, 500)
  }

  checkOtp(check, event) {
    console.log("event", event)
    this.ngzone.run(() => {
      if (event.keyCode >= 48 && event.keyCode <= 57) {
        if (check == 1) {
          this.second.inputViewChild.nativeElement.focus();
        }
        else if (check == 2) {
          this.third.inputViewChild.nativeElement.focus();
        }
        else if (check == 3) {
          this.fourth.inputViewChild.nativeElement.focus();
        }
      }
      else {
        return false
      }
    })
  }

  verifyOtp() {
    if (this.d1 && this.d2 && this.d3 && this.d4) {
      this.loader = true;
      let otp = this.d1 + "" + this.d2 + "" + this.d3 + "" + this.d4;
      let url = this.global.base_path_api() + "peloteando/mobile_otp/?format=json";
      let data = {
        "phone": this.data.phone,
        "country_code": this.data.country_code,
        "form_type": "verify_otp",
        "register_type": "phone",
        "otp": parseInt(otp)
      }
      this.global.PostRequestUnauthorised(url, data)
        .subscribe(res => {
          console.log("otp verify=>>>>>>>>>>>>>>>>>");
          this.setPhone();
        }, err => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({
            severity: 'error',
            summary: (localStorage.getItem('language') == 'es' ? "Ingrese OTP correcta" : "Enter correct OTP"),
          });
        })
    }
  }

  setPhone() {
    let url1 = this.global.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.global.PostRequest(url1, this.data)
      .subscribe(res => {
        this.ngzone.run(() => {
          this.loader = false;
          localStorage.setItem('user_info', JSON.stringify(res[0].json.data))
          let user_info = JSON.parse(localStorage.getItem('user_info'));
          if (!user_info.is_username) {
            this.router.navigate(['/home/signup/set-user'])
          }
          else if (!user_info.is_gender) {
            this.router.navigate(['/home/signup/set-gender'])
          }
          else if (!user_info.is_height) {
            this.router.navigate(['/home/signup/set-height'])
          }
          else if (!user_info.is_dob) {
            this.router.navigate(['/home/signup/set-dob'])
          }
          else if (!user_info.is_leg) {
            this.router.navigate(['/home/signup/set-leg'])
          }
          else if (!user_info.is_position) {
            this.router.navigate(['/home/signup/set-position'])
          }
          else if (!user_info.is_profile_pic) {
            this.router.navigate(['/home/signup/set-profile-pic'])
          }
          else {
            localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
            // localStorage.removeItem('user_info');
            // this.router.navigate(['/dashboard'])
            let comdata = JSON.parse(localStorage.getItem("community_info"));
            console.log(comdata, "fgewguigewewrgwue");
            let comname = comdata.community_name;
            console.log(comname, "community name we get======================");
            this.router.navigate([comname]);
          }
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }



  resendOtp() {
    if (this.count <= 0) {
      this.loader = true;
      let url = this.global.base_path_api() + "peloteando/mobile_otp/?language=" + localStorage.getItem('language') + "&format=json";
      let data = {
        "phone": this.data.phone,
        "country_code": this.data.country_code,
        "form_type": "send_otp",
        "register_type": "phone",
      }
      this.global.PostRequestUnauthorised(url, data)
        .subscribe(res => {
          this.loader = false;
          this.count = 30;
          this.msgs = [];
          this.msgs.push({
            severity: 'success',
            summary: localStorage.getItem('language') == 'es' ? "OTP enviado" : "OTP sent",
          });
        }, error => {
          this.loader = false;
          this.msgs = [];
          this.msgs.push({
            severity: 'error',
            summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
          });

        })
    }
  }
}
