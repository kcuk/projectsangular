import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetPhoneOtpComponent } from './set-phone-otp.component';

describe('SetPhoneOtpComponent', () => {
  let component: SetPhoneOtpComponent;
  let fixture: ComponentFixture<SetPhoneOtpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetPhoneOtpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetPhoneOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
