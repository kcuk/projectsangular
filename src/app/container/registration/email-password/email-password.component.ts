import { Component, ViewChild, Renderer2, NgZone } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { GlobalService } from "../../../GlobalService";
import { Http } from "@angular/http";
import { TranslateService } from "ng2-translate";


@Component({
  selector: "app-email-password",
  templateUrl: "./email-password.component.html",
  styleUrls: ["./email-password.component.css"]
})
export class EmailPasswordComponent {
  data: any;
  check_password: boolean;
  password = "";
  loader: boolean;
  public msgs: any;
  show_password: boolean = false;
  display: boolean = false;
  displayrequest:boolean=false;

  constructor(
    private translate: TranslateService,
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    private renderer: Renderer2,
    public ngzone: NgZone,
    public router: Router
  ) {
    if (localStorage.getItem("language") != null) {
      translate.use(localStorage.getItem("language"));
    }
    this.data = JSON.parse(routes.snapshot.paramMap.get("data"));
  }

  validate(event) {
    let password_length = event.target.value.length;
    if (password_length >= 6) {
      this.check_password = true;
    } else {
      this.check_password = false;
    }
  }

  next() {
    if (this.password.length >= 6) {
      this.getToken();
    } else {
      this.msgs = [];
      this.msgs.push({
        severity: "error",
        summary:
          localStorage.getItem("language") == "es"
            ? "La contraseña debe ser mayor a 5 caracteres"
            : "Password must be greater than 5 character"
      });
    }
  }

  getToken() {
    this.loader = true;
    this.data.password = this.password;
    this.data.community_id = localStorage.getItem("communityid");
    this.data.player_type = localStorage.getItem("signupcategory");
    this.data.team_id = localStorage.getItem("signupteamid");
    this.data.created_team = localStorage.getItem("createdteamid");
    this.data.language = localStorage.getItem("language");
    let url1 =
      this.global.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.global.PostRequestUnauthorised(url1, this.data).subscribe(
      res => {
        this.loader = false;
        let lang = localStorage.getItem("language");
        let country = localStorage.getItem("country_code_with_name");
        let country_code = localStorage.getItem("country_dial_code");
        let distance = localStorage.getItem("distance");
        // localStorage.clear();
        localStorage.setItem("country_code_with_name", country);
        localStorage.setItem("country_dial_code", country_code);
        localStorage.setItem("distance", distance);
        localStorage.setItem("language", lang);
        let data = res[0].json;
        let expire = new Date().getTime() / 1000 + data.token.expires_in;
        localStorage.setItem("userInfo", JSON.stringify(data));
        localStorage.setItem("user_info", JSON.stringify(data.info));
        localStorage.setItem("timeout", JSON.stringify(expire));
        localStorage.setItem(
          "nick_name",
          JSON.stringify(res[0].json.info.nick_name)
        );
        localStorage.setItem("username", data.info.username);
        localStorage.setItem(
          "profile_percentage",
          data.info.profile_percentage
        );
        localStorage.setItem("checkoptionalpage", "signup");
        localStorage.setItem("last_name", res[0].json.info.last_name);
        localStorage.setItem("community_info", data.info.community_info);

        localStorage.removeItem("signupteamid");
        localStorage.removeItem("createdteamid");

        let id_com = localStorage.getItem("communityid");
        if (id_com == this.global.peloteaid) {
          let url =
            this.global.base_path_api() +
            "user/setLanguage/?language=" +
            localStorage.getItem("language");
          this.global.GetRequest(url).subscribe(res => {
            this.loader = false;
            this.router.navigate(["/home/signup/set-location"]);
          });
        } else {
          this.showDialog();
        }

        // this.showDialog();
      },
      err => {
        console.log(err.status, "error in signup");
        if (err.status == 401) {
          this.loader = false;
          console.log("password error==================");
        } else if (err.status == 403) {
          this.display = true;
          this.loader = false;
        } else if (err.status == 400) {
          this.displayrequest = true;
          this.loader = false;
        } else {
          this.loader = false;
          console.log(err, "error message");
        }
      }
    );
  }
  showDialog() {
    this.display = true;
  }
  done() {
    let url =
      this.global.base_path_api() +
      "user/setLanguage/?language=" +
      localStorage.getItem("language");
    this.global.GetRequest(url).subscribe(res => {
      this.loader = false;
      // this.router.navigate(["/home/signup/set-location"]);
      localStorage.removeItem("user_info");
      this.router.navigate(["/home"]);
    });
    this.display = false;
  }
  
  donetwo() {
    this.loader = false;
    localStorage.removeItem("user_info");
    this.router.navigate(["/home"]);
    this.display = false;
  }  
  closeDialog() {
    this.display = false;
  }
}
