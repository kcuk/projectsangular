import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../../GlobalService';
import { Http } from '@angular/http';
@Component({
  selector: 'app-set-profile-pic',
  templateUrl: './set-profile-pic.component.html',
  styleUrls: ['./set-profile-pic.component.css']
})
export class SetProfilePicComponent implements OnInit {
  crop_img: any = "";
  msgs: any[];
  profile_complete_percentage: any;
  loader: boolean;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  display: boolean = false;
  constructor(
    public router: Router,
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    public zone: NgZone,
  ) {
    this.profile_complete_percentage = JSON.parse(localStorage.getItem('user_info')).profile_percentage;
  }

  ngOnInit() {
  }

  skip() {
    this.loader = true;
    let data = {
      "page": "profile_pic",
      "skipped": true,
      "request_from": localStorage.getItem('checkoptionalpage')

    };
    let url = this.global.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.global.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.zone.run(() => {
          localStorage.setItem('user_info', JSON.stringify(res[0].json.data));
          let user_info = res[0].json.data;
          localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
          // localStorage.removeItem('user_info');
          // this.router.navigate(['/dashboard'])
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          console.log(comdata, "fgewguigewewrgwue");
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.router.navigate([comname]);
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

  fileChangeEvent(event: any): void {
    this.display = true;
    console.log("display=>>>>>>>>>>>>>>>>>>", this.display)
    this.imageChangedEvent = event;
    console.log("cropeed", this.imageChangedEvent)

  }

  imageCropped(image: string) {
    this.croppedImage = image;
    console.log("cropeed", this.croppedImage)
  }

  disable() {
    this.display = false;
    this.croppedImage = '';
  }

  save() {
    this.display = false;
    this.crop_img = this.croppedImage;
  }

  setProfile() {
    this.loader = true;
    let data = {
      "profile_raw_data": this.crop_img,
      "page": "profile_pic",
      "skipped": false,
      "request_from": localStorage.getItem('checkoptionalpage')
    };
    let url = this.global.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.global.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.zone.run(() => {
          localStorage.setItem('user_info', JSON.stringify(res[0].json.data));
          let user_info = res[0].json.data;
          localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
          // localStorage.removeItem('user_info');
          localStorage.setItem('profile_pic',res[0].json.data.profile_pic)
          // this.router.navigate(['/dashboard'])
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          console.log(comdata, "fgewguigewewrgwue");
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.router.navigate([comname]);
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

}
