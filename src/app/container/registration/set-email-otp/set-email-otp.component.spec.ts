import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetEmailOtpComponent } from './set-email-otp.component';

describe('SetEmailOtpComponent', () => {
  let component: SetEmailOtpComponent;
  let fixture: ComponentFixture<SetEmailOtpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetEmailOtpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetEmailOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
