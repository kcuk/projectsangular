import { Component, OnInit, NgZone } from "@angular/core";
import { GlobalService } from "../../../GlobalService";
import { Router } from "@angular/router";
import { Message } from "primeng/primeng";

@Component({
  selector: "app-registration-success",
  templateUrl: "./registration-success.component.html",
  styleUrls: ["./registration-success.component.css"]
})
export class RegistrationSuccessComponent implements OnInit {
  loader: boolean;
  public msgs: Message[] = [];

  constructor(
    private global: GlobalService,
    private router: Router,
    public zone: NgZone
  ) {}

  ngOnInit() {}

  completeProfile() {
    let user_info = JSON.parse(localStorage.getItem("user_info"));

    if (!user_info.is_email) {
      this.router.navigate(["/home/signup/set-email"]);
    } else if (!user_info.is_phone_no) {
      this.router.navigate(["/home/signup/set-phone"]);
    } else {
      // localStorage.removeItem("user_info");
      // this.router.navigate(["/dashboard"]);
      let comdata = JSON.parse(localStorage.getItem("community_info"));
      console.log(comdata, "fgewguigewewrgwue");
      let comname = comdata.community_name;
      console.log(comname, "community name we get======================");
      this.router.navigate([comname]);
    }

    // if (!user_info.is_email) {
    //   this.navCtrl.setRoot("SetEmailPage");
    // }
    // else if (!user_info.is_phone_no) {
    //   this.navCtrl.setRoot("SetPhonePage");
    // }

    // else if (!user_info.is_username) {
    //   this.navCtrl.setRoot("SetUserPage");
    // }
    // else if (!user_info.is_gender) {
    //   this.navCtrl.setRoot("SetGenderPage");
    // }
    // else if (!user_info.is_dob) {
    //   this.navCtrl.setRoot("SetDobPage");
    // }
    // else if (!user_info.is_leg) {
    //   this.navCtrl.setRoot("SetLegPage");
    // }
    // else if (!user_info.is_position) {
    //   this.navCtrl.setRoot("SetPositionPage");
    // }
    // else {
    //   this.navCtrl.setRoot("TabPage");
    // }
  }

  skipAll() {
    this.loader = true;
    let url =
      this.global.base_path_api() + "peloteando/skip_details/?format=json";
    this.global.PostRequest(url, {}).subscribe(
      res => {
        this.loader = false;
        // localStorage.removeItem("user_info");
        // this.router.navigateByUrl('/home')
        let comdata = JSON.parse(localStorage.getItem("community_info"));
        console.log(comdata, "fgewguigewewrgwue");
        let comname = comdata.community_name;
        console.log(comname, "community name we get======================");
        this.router.navigate([comname]);
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary:
            localStorage.getItem("language") == "es"
              ? "Error, inténtalo de nuevo más tarde"
              : "Error, Try again later"
        });
      }
    );
  }
}
