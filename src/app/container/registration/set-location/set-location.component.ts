import { Component, OnInit, NgZone } from "@angular/core";
import { GlobalService } from "../../../GlobalService";
import { Router } from "@angular/router";
import { Message } from "primeng/primeng";
declare var google;

@Component({
  selector: "app-set-location",
  templateUrl: "./set-location.component.html",
  styleUrls: ["./set-location.component.css"]
})
export class SetLocationComponent implements OnInit {
  loader: boolean;
  social: any;
  public msgs: Message[] = [];

  constructor(
    public global: GlobalService,
    public router: Router,
    public zone: NgZone
  ) {}

  ngOnInit() {}

  allowLocation() {
    this.loader = true;
    this.zone.run(() => {
      console.log("run =>>>>>>>>>>>>>>>>>>>>>>>");
      this.global.currentLocation().subscribe(
        res => {
          console.log("location=>>>>>>>>>>>>>>>>>>>>>.", res.json());
          let data = res.json();
          this.sendData(
            data.location.latitude,
            data.location.longitude,
            data.city,
            data.country.name
          );
          // var geocoder = new google.maps.Geocoder();             // create a geocoder object
          // var location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);    // turn coordinates into an object
          // new google.maps.Geocoder().geocode({ 'latLng': location }, (results, status) => {
          //   console.log("dddddddddddddddddddddddddddd")
          //   if (status == google.maps.GeocoderStatus.OK) {
          //     console.log("jjcbjbcjbckjnbc")
          //     var country = null, city = null;
          //     if (results[0]) {
          //       var c, lc, component;
          //       for (var r = 0, rl = results.length; r < rl; r += 1) {
          //         var result = results[r];
          //         if (!city && result.types[0] === 'locality') {
          //           for (c = 0, lc = result.address_components.length; c < lc; c += 1) {
          //             component = result.address_components[c];
          //             if (component.types[0] === 'locality') {
          //               city = component.long_name;
          //               break;
          //             }
          //           }
          //         }
          //         else if (!country && result.types[0] === 'country') {
          //           country = result.address_components[0].long_name;
          //         }
          //         if (city && country) {
          //           break;
          //         }
          //       }
          //       console.log("City: " + city + " Country: " + country);

          //     }
          //   }
          //   else {
          //     console.log("aaaaaaaaaaaaaaaa")
          //     this.loader = false;
          //     this.msgs = [];
          //     this.msgs.push({
          //       severity: 'error',
          //       summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
          //     });
          //   }
          // });
        },
        err => {
          console.log("error =>>>>>>>>>>>>>>", err);
        }
      );
    });
  }

  sendData(lat, long, city, country) {
    localStorage.setItem("lat", lat);
    localStorage.setItem("long", long);
    localStorage.setItem("city", city);
    localStorage.setItem("country_name", country);
    let data = {
      page: "location",
      latitude: lat,
      longitude: long,
      city: city,
      country: country,
      request_from: "signup"
    };
    let url =
      this.global.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.global.PostRequest(url, data).subscribe(
      res => {
        this.loader = false;
        localStorage.setItem('user_info', JSON.stringify(res[0].json.data))
        let user_info = JSON.parse(localStorage.getItem("user_info"));
        this.zone.run(() => {
          this.router.navigate(["/home/signup/registration-success"]);
        });
        // if (this.social) {
        //   if (!user_info.is_phone_no
        //     && !user_info.is_username
        //     && !user_info.is_gender
        //     && !user_info.is_dob
        //     && !user_info.is_leg
        //     && !user_info.is_position) {
        //     this.navCtrl.setRoot("RegistrationSuccessPage");
        //   }
        //   else if (!user_info.is_email) {
        //     this.navCtrl.setRoot("SetEmailPage");
        //   }
        //   else if (!user_info.is_phone_no) {
        //     this.navCtrl.setRoot("SetPhonePage");
        //   }
        //   else if (!user_info.is_username) {
        //     this.navCtrl.setRoot("SetUserPage");
        //   }
        //   else if (!user_info.is_gender) {
        //     this.navCtrl.setRoot("SetGenderPage");
        //   }
        //   else if (!user_info.is_dob) {
        //     this.navCtrl.setRoot("SetDobPage");
        //   }
        //   else if (!user_info.is_leg) {
        //     this.navCtrl.setRoot("SetLegPage");
        //   }
        //   else if (!user_info.is_position) {
        //     this.navCtrl.setRoot("SetPositionPage");
        //   }
        //   else {
        //     this.navCtrl.setRoot("SelectcityPage");
        //   }
        // }
        // else {
        //   this.navCtrl.setRoot("RegistrationSuccessPage");
        // }
      },
      err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: "error",
          summary:
            localStorage.getItem("language") == "es"
              ? "Error, inténtalo de nuevo más tarde"
              : "Error, Try again later"
        });
      }
    );
  }
}
