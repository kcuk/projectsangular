import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../../GlobalService';
import { Http } from '@angular/http';
@Component({
  selector: 'app-set-user',
  templateUrl: './set-user.component.html',
  styleUrls: ['./set-user.component.css']
})
export class SetUserComponent implements OnInit {
  suggestions: any;
  check_user: boolean;
  user_exist: boolean;
  user_short_length: boolean;
  tagname_valid1: boolean=true;
  loader: boolean;
  profile_complete_percentage: any;
  msgs: any[];
  user:any="";

  constructor(
    public global: GlobalService,
    public routes: ActivatedRoute,
    public http: Http,
    public zone: NgZone,
    public router: Router,
  ) {
    this.profile_complete_percentage = JSON.parse(localStorage.getItem('user_info')).profile_percentage;

  }

  ngOnInit() {
    let url = this.global.base_path_api() + "peloteando/username_suggest/?format=json";
    this.global.GetRequest(url)
      .subscribe(res => {
        this.suggestions = res[0].json.suggestions
      })
  }

  validate(event) {
    if (event.keyCode == 13) {
      if (this.check_user) {
        this.next()
      }
    }
    else {
      let user = event.target.value;
      if (user.length > 5) {
        this.check_user = true;
        this.user_exist = false;
        this.user_short_length = false;
        if (/^(\@)?[A-Za-z0-9_]+$/i.test(user)) {
          this.tagname_valid1 = true
        }
        else {
          this.tagname_valid1 = false
        }
      }
      else {
        this.tagname_valid1 = true
        this.user_exist = false;
        this.check_user = false;
        this.user_short_length = true
      }
    }
  }

  selectUser(user) {
    this.user = user;
    this.check_user = true;
    this.user_exist = false;
    this.user_short_length = false;
  }

  next() {
    this.loader = true;
    let url = this.global.base_path_api() + "peloteando/verification/?tag_name=" + this.user + "&format=json";
    this.global.GetRequest(url)
      .subscribe(res => {
        this.loader = false;
        this.userName(false)
      }, err => {
        this.loader = false;
        this.user_exist = true;
        this.check_user = false;
      })
  }

  userName(skip) {
    this.loader = true;
    let data = {
      "page": "tagname",
      "tag_name": this.user,
      "skipped": skip,
      "request_from": localStorage.getItem('checkoptionalpage')
    }
    let url = this.global.base_path_api() + "peloteando/mobile_signup/?format=json";
    this.global.PostRequest(url, data)
      .subscribe(res => {
        this.loader = false;
        this.zone.run(()=>{
          localStorage.setItem('user_info', JSON.stringify(res[0].json.data))
          let user_info = JSON.parse(localStorage.getItem('user_info'));
          console.log("info =>>>>>>>>>>>",user_info)
          if (!user_info.is_gender) {
            this.router.navigate(['/home/signup/set-gender'])
          }
          else if (!user_info.is_height) {
            this.router.navigate(['/home/signup/set-height'])
          }
          else if (!user_info.is_dob) {
            this.router.navigate(['/home/signup/set-dob'])
          }
          else if (!user_info.is_leg) {
            this.router.navigate(['/home/signup/set-leg'])
          }
          else if (!user_info.is_position) {
            this.router.navigate(['/home/signup/set-position'])
          }
          else if (!user_info.is_profile_pic) {
            this.router.navigate(['/home/signup/set-profile-pic'])
          }
          else {
            localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
            // localStorage.removeItem('user_info');
            // this.router.navigate(['/dashboard'])
            let comdata = JSON.parse(localStorage.getItem("community_info"));
            console.log(comdata, "fgewguigewewrgwue");
            let comname = comdata.community_name;
            console.log(comname, "community name we get======================");
            this.router.navigate([comname]);
          }
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

  skipAll() {
    this.loader = true;
    let url = this.global.base_path_api() + "peloteando/skip_details/?format=json";
    this.global.PostRequest(url, {})
      .subscribe(res => {
        this.loader = false;
        this.zone.run(()=>{
          localStorage.setItem('profile_percentage', JSON.parse(localStorage.getItem('user_info')).profile_percentage);
          // localStorage.removeItem('user_info');
          // this.router.navigate(['/dashboard'])
          let comdata = JSON.parse(localStorage.getItem("community_info"));
          console.log(comdata, "fgewguigewewrgwue");
          let comname = comdata.community_name;
          console.log(comname, "community name we get======================");
          this.router.navigate([comname]);
        })
      }, err => {
        this.loader = false;
        this.msgs = [];
        this.msgs.push({
          severity: 'error',
          summary: localStorage.getItem('language') == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
        });
      })
  }

}
