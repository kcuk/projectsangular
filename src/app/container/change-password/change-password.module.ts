import { NgModule } from '@angular/core';
import {changePasswordRouting } from './change-password.routes';
import {ChangePasswordComponent} from './change-password.component'
import {SharedModule} from './../../shared/shared.module';

@NgModule({
  imports: [
    changePasswordRouting,
    SharedModule.forRoot()
  ],
  declarations: [ChangePasswordComponent]
})
export class ChangePasswordModule { }
