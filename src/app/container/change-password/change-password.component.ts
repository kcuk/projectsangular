import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../GlobalService';
import {Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  userObj: User;
  showConfirmPasswordError: boolean = false;
  passwordChanged: boolean = true;
  currentPasswordError: boolean = false;
  newPasswordError: boolean = false;
  currentTime: Date;
  redirectShow;
  msgs: Message[] = [];
  prevPath: string = '';

  constructor(private global: GlobalService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.userObj = new User();
    this.userObj.old_password = '';
    this.userObj.new_password = '';
  }

  ngOnInit() {
    if (!localStorage.getItem("userInfo"))
      this.router.navigate(['/']);
    else {
      if (localStorage.getItem("cr"))
        this.prevPath = localStorage.getItem("cr");
    }
  }

  changePassword() {
    if (this.userObj.new_password === this.userObj.confirm_password) {
      let url = this.global.base_path + "api/peloteando/change_password/";
      this.global.PostRequest(url, this.userObj)
        .subscribe(res => {
          this.msgs = [];
          this.msgs.push({ severity: "info", summary: "Password", detail: 'cambiado correctamente' });
          this.passwordChanged = false;
          this.router.navigate(['/dashboard']);

        },
        err => {
          this.msgs = [];
          this.msgs.push({ severity: "error", summary: "Password", detail: 'Error...Inténtalo de nuevo!' });
          console.log("some error in change password");
        });
    }
    else {
      this.showConfirmPasswordError = true;
    }
  }

  validate(indicator, event: any) {
    if (indicator == 1) {
      if (this.userObj.old_password.length > 0 && this.userObj.old_password.length < 6)
        this.currentPasswordError = true;
      else
        this.currentPasswordError = false;
    }
    else if (indicator == 2) {
      if (this.userObj.new_password.length > 0 && this.userObj.new_password.length < 6 && indicator == 2)
        this.newPasswordError = true;
      else if (indicator == 2)
        this.newPasswordError = false;
    }
    else {
      if (this.showConfirmPasswordError)
        if (this.userObj.new_password === this.userObj.confirm_password)
          this.showConfirmPasswordError = false;

    }
    if (event.keyCode == 13) {
      this.changePassword()
    }
  }
  goTobackPage() {
    this.router.navigate([this.prevPath]);
  }
}

class User {
  old_password: string;
  new_password: string;
  confirm_password: string;

}
