
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangePasswordComponent } from './../change-password/change-password.component';

export const changePasswordRoutes = [
    { path: '', component: ChangePasswordComponent }
];

export const changePasswordRouting: ModuleWithProviders = RouterModule.forChild(changePasswordRoutes);