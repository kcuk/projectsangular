import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';


@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) { }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('userInfo')) {
            if (localStorage.getItem('Url')) {
                console.log("url+>>>>>>>>>>>>>>>>>>>>",localStorage.getItem('Url'))
                this.router.navigateByUrl(localStorage.getItem('Url'));
                localStorage.removeItem('Url');
            }
            return true;
        } else {
            localStorage.setItem('Url', state.url)
            this.router.navigate(['../home/signup']);
            return false;
        }

    }
}