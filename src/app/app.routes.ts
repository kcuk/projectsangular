import { HomeNavbarComponent } from "./container/home-navbar/home-navbar.component";
import { NavbarModule } from "./container/navbar/navbar.module";
import {
  Routes,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { CompleteProfile1Component } from "./container/complete-profile1/complete-profile1.component";
import { CompleteProfile2Component } from "./container/complete-profile2/complete-profile2.component";
import { PageNotFoundComponent } from "./container/page-not-found/page-not-found.component";
import { ChangePasswordModule } from "./container/change-password/change-password.module";
import { ResetPasswordComponent } from "./container/reset-password/reset-password.component";
import { TermsConditionsComponent } from "./container/terms-conditions/terms-conditions.component";
import { QuoteComponent } from "./container/quote/quote.component";
import { PricingInfo } from "./container/pricing_info/pricing_info.component";
import { SupportComponent } from "./container/support/support.component";
import { TalkSpecialistComponent } from "./container/talk-specialist/talk-specialist.component";
import { AboutUsComponent } from "./container/about-us/about-us.component";
import { CustomersComponent } from "./container/customers/customers.component";
import { AuthGuard } from "./auth.gurd";
import { EmailVerifyComponent } from "./container/email-verify/email-verify.component";
import { HomeComponentModule } from "./container/home/home.component.module";
import { PrivacyPolicyComponent } from "./container/privacy-policy/privacy-policy.component";
import { LoginComponent } from "./container/login/login.component";
import { SignupComponent } from "./container/signup/signup.component";
import { RegistrationModule } from "./container/registration/registration.module";
import { LocationComponent } from "./container/location/location.component";
import { CommunitySignupComponent } from "./container/community-signup/community-signup.component";

export const AppRoutes: Routes = [
  { path: "", redirectTo: "/home", pathMatch: "full" },
  {
    path: "home",
    component: HomeNavbarComponent,
    children: [
      {
        path: "",
        loadChildren:
          "./container/home/home.component.module#HomeComponentModule"
      },
      // { path: 'user', loadChildren: './container/home/home.component.module#HomeComponentModule' },
      { path: "customers", component: CustomersComponent },
      { path: "talk", component: TalkSpecialistComponent },
      { path: "about", component: AboutUsComponent },
      { path: "support", component: SupportComponent },
      { path: "pricing", component: PricingInfo },
      { path: "quote", component: QuoteComponent },
      { path: "terms", component: TermsConditionsComponent },
      { path: "privacy-policy", component: PrivacyPolicyComponent },
      {
        path: "signup",
        loadChildren:
          "./container/registration/registration.module#RegistrationModule"
      },
      {
        path: "login",
        loadChildren: "./container/new-login/new-login.module#NewLoginModule"
      },
      {
        path: "community",
        loadChildren:
          "./container/community-signup/community-signup.module#CommunitysignupModule"
      },
      { path: "location", component: LocationComponent }
    ]
  },

  { path: "email_verify", component: EmailVerifyComponent },
  { path: "reset-password", component: ResetPasswordComponent },
  {
    path: "change-password",
    loadChildren:
      "./container/change-password/change-password.module#ChangePasswordModule"
  },
  { path: "complete-profile1", component: CompleteProfile1Component },
  { path: "complete-profile2", component: CompleteProfile2Component },
  // {
  //   path: "dashboard",
  //   loadChildren: "./container/navbar/navbar.module#NavbarModule",
  //   canActivate: [AuthGuard]
  // }
  {
    path: ":name",
    loadChildren: "./container/navbar/navbar.module#NavbarModule",
    // canActivate: [AuthGuard]
  }

  // { path: '404', component: PageNotFoundComponent },
  // { path: '**', redirectTo: '/404' }
];