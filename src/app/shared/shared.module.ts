
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SliderModule,LightboxModule,OverlayPanelModule, InputMaskModule, CalendarModule, DataTableModule, GMapModule, PickListModule, RadioButtonModule, MultiSelectModule, MessagesModule, ScheduleModule, RatingModule, PaginatorModule, AutoCompleteModule, GrowlModule, CheckboxModule, ButtonModule, DropdownModule, DialogModule, InputTextModule, InputTextareaModule, GalleriaModule, CarouselModule } from 'primeng/primeng';
import { LoaderComponent } from './../container/loader/loader.component';
import { FooterComponent } from './../container/footer/footer.component';
import { HomeNavbarComponent } from './../container/home-navbar/home-navbar.component';
import { RouterModule } from '@angular/router';
import { Angulartics2Module, Angulartics2GoogleAnalytics } from 'angulartics2';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MetadataModule, MetadataLoader, MetadataStaticLoader, PageTitlePositioning } from 'ng2-metadata';
import { TranslateModule } from 'ng2-translate';
import { HttpClientModule } from '@angular/common/http';
import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';
import { TranslateService } from 'ng2-translate';
import { ProfileCompleteComponent } from '../container/profile-complete/profile-complete.component';
import { ImageCropperModule } from 'ng2-img-cropper';


@NgModule({
  imports: [MetadataModule.forRoot({
    provide: MetadataLoader,
    useFactory: (metadataFactory)
  }),
  TranslateModule.forRoot(),
  Angulartics2Module.forChild(), CommonModule, FormsModule, ButtonModule, RouterModule, DropdownModule, InputMaskModule],
  declarations: [
    LoaderComponent,
    FooterComponent,
    HomeNavbarComponent,
    ProfileCompleteComponent, 

  ],
  exports: [
    LightboxModule, 
    ConfirmDialogModule,ProfileCompleteComponent,
    OverlayPanelModule,
    InfiniteScrollModule,SliderModule,
    CalendarModule,
    InputMaskModule,
    GMapModule,
    CommonModule,
    FormsModule,
    HttpModule,ImageCropperModule,
    DataTableModule,
    ReactiveFormsModule,
    GrowlModule,
    ButtonModule,
    DropdownModule,
    DialogModule,
    InputTextModule,
    InputTextareaModule,
    GalleriaModule,
    CarouselModule,
    CheckboxModule,
    AutoCompleteModule,
    PaginatorModule,
    ScheduleModule,
    RatingModule,
    PickListModule,
    MultiSelectModule,
    RadioButtonModule,
    MessagesModule,
    LoaderComponent,
    FooterComponent,
    HomeNavbarComponent,
    TranslateModule
  ],
})
export class SharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [ConfirmationService,TranslateService]
    };
  }
}

export function metadataFactory() {
  return new MetadataStaticLoader({
    pageTitlePositioning: PageTitlePositioning.PrependPageTitle,
    pageTitleSeparator: ' - ',
    applicationName: 'Tour of (lazy/busy) heroes',
    defaults: {
      title: 'Mighty mighty mouse',
      description: 'Mighty Mouse is an animated superhero mouse character',
      'og:image': 'https://upload.wikimedia.org/wikipedia/commons/f/f8/superraton.jpg',
      'og:type': 'website',
      'og:locale': 'en_US',
      'og:locale:alternate': 'nl_NL,tr_TR'
    }
  });
}
