import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutes } from "./app.routes";
import { RouterModule } from "@angular/router";
import { GlobalService } from "./GlobalService";
import { AppComponent } from "./app.component";
import { SharedModule } from "./shared/shared.module";
import { CompleteProfile1Component } from "./container/complete-profile1/complete-profile1.component";
import { CompleteProfile2Component } from "./container/complete-profile2/complete-profile2.component";
import { FacebookService } from "./services/login/facebookLogin.service";
import { LoginService } from "./services/login/login.service";
import { AuthGuard } from "./auth.gurd";
import { PageNotFoundComponent } from "./container/page-not-found/page-not-found.component";
import { ResetPasswordComponent } from "./container/reset-password/reset-password.component";
import { TermsConditionsComponent } from "./container/terms-conditions/terms-conditions.component";
import { QuoteComponent } from "./container/quote/quote.component";
import { PricingInfo } from "./container/pricing_info/pricing_info.component";
import { SupportComponent } from "./container/support/support.component";
import { TalkSpecialistComponent } from "./container/talk-specialist/talk-specialist.component";
import { AboutUsComponent } from "./container/about-us/about-us.component";
import { CustomersComponent } from "./container/customers/customers.component";
import { Angulartics2Module, Angulartics2GoogleAnalytics } from "angulartics2";
import { EmailVerifyComponent } from "./container/email-verify/email-verify.component";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { RedirectUrlComponent } from "./redirect-url/redirect-url.component";
import { TranslateService } from "ng2-translate";
import { PrivacyPolicyComponent } from "./container/privacy-policy/privacy-policy.component";
import { LoginComponent } from "./container/login/login.component";
import { SignupComponent } from "./container/signup/signup.component";
import { LocationComponent } from "./container/location/location.component";
import { ImageCropperModule } from "ngx-image-cropper";

@NgModule({
  declarations: [
    TermsConditionsComponent,
    AppComponent,
    CompleteProfile1Component,
    CompleteProfile2Component,
    PageNotFoundComponent,
    ResetPasswordComponent,
    LoginComponent,
    QuoteComponent,
    PricingInfo,
    SupportComponent,
    TalkSpecialistComponent,
    AboutUsComponent,
    CustomersComponent,
    EmailVerifyComponent,
    RedirectUrlComponent,
    PrivacyPolicyComponent,
    SignupComponent,
    LocationComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    ImageCropperModule,
    SharedModule.forRoot(),
    RouterModule.forRoot(AppRoutes),
    Angulartics2Module.forRoot([Angulartics2GoogleAnalytics])
  ],
  providers: [
    TranslateService,
    AuthGuard,
    GlobalService,
    LoginService,
    FacebookService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
