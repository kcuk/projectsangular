importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-messaging.js');

firebase.initializeApp({
  messagingSenderId:  "328626142690"  
});

const messaging = firebase.messaging();

  messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);

    var myaction;             
    if(payload.data.button1=='' && payload.data.button2=='') {
      myaction=[]
    }
    else if(payload.data.button1!='' && payload.data.button2!='') {
      myaction= [
                  {action: 'like', title: payload.data.button1},
                  {action: 'reply', title: payload.data.button2}
                ]
    } 
    else if(payload.data.button1!='' && payload.data.button2=='') {
      myaction= [
                  {action: 'like', title: payload.data.button1}
                ]
    }  
    else if(payload.data.button1=='' && payload.data.button2!='') {
      myaction= [
                  {action: 'reply', title: payload.data.button2}
                ]
    }  

    const notificationTitle = payload.data.title;
    const notificationOptions = {
      body: payload.data.message,
      icon: payload.data.icon  ,
      actions: myaction,
      data: {
        url : payload.data.destination_url,
        camp_id : payload.data.camp_id,
        camp_report_id : payload.data.camp_report_id,
        myaction : myaction,
        button_url_like: payload.data.b_url1,
        button_url_reply: payload.data.b_url2,
      }
    }; 

    return self.registration.showNotification(notificationTitle,notificationOptions);  
   
  });


  self.addEventListener('notificationclick', function (event) {
    console.log("click event")
    console.log(event)
   
    event.waitUntil(
      fetch('https://api.epush.in/api/mgtpush/click_count/', {
        method: 'post',
        headers: {
          "Content-Type": "application/json"
        },
         body: '{"camp_id":"' + event.notification.data.camp_id + '","camp_report_id":"' + event.notification.data.camp_report_id + '"}'
      }).then(function (response) {
        console.log(JSON.stringify(response)+" count");
     })
    );

    event.notification.close();

    if (event.action === 'like') {  
      clients.openWindow(event.notification.data.button_url_like);  
    }  
    else if (event.action === 'reply') {  
      clients.openWindow(event.notification.data.button_url_reply);
    }  
    else { 
      event.waitUntil(      
          clients.matchAll({
            type: 'window'
          })
          .then(function(windowClients) {
            console.log('WindowClients', windowClients);
            for (var i = 0; i < windowClients.length; i++) {
              var client = windowClients[i];
              console.log('WindowClient', client);
              if (client.url === url && 'focus' in client) {
                return client.focus();
              }
            }
            if (clients.openWindow) {
              return clients.openWindow(event.notification.data.url);
            }
          })
      );
    }

  });



