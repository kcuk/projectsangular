import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DemoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-demo',
  templateUrl: 'demo.html',
})
export class DemoPage {
  structure: any = { lower: 20, upper: 50 };
  date: any;
  days: any;
  year: any;
  month: any;
  week_days: any = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  month_days: any = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  current_date: any;
  current_day: any;
  day_array: any[] = [];
  fulldate: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let obj = new Date();
    this.year = obj.getFullYear();
    this.month = obj.getMonth();
    this.current_day = this.week_days[obj.getDay()];
    if (this.month > 9) {
      this.month++;
      this.date = this.year + "-" + this.month;
    }
    else {
      this.month = '0' + (this.month + 1);
      this.date = this.year + "-" + this.month;
    }
    
    if (localStorage.getItem('start_date')) {
      let new_obj = new Date(localStorage.getItem('start_date'))
      this.current_date = new_obj.getDate();

      this.fulldate = this.year + "-" + this.month + "-" + this.current_date;
      console.log("current date=>>>>>>>>>>>>>.",this.current_date);
      
    }
    else {
      this.current_date = obj.getDate();
      this.fulldate = this.year + "-" + this.month + "-" + this.current_date;
    }
    let current_date_obj = new Date(this.fulldate);
    this.pushDate(current_date_obj)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DemoPage');
  }
  pushDate(obj: Date) {
    this.days = [];
    console.log("dateeeeeeeee", obj)
    let month_days = this.month_days[obj.getMonth()]
    while (obj.getDate() <= (month_days - 1)) {
      if (obj.getDate() > 9) {
        this.days.push({ day: this.week_days[obj.getDay()], date: obj.getDate() });
      }
      else {
        this.days.push({ day: this.week_days[obj.getDay()], date: '0' + obj.getDate() });
      }
      obj.setDate((obj.getDate()) + 1);
    }
    this.days.push({ day: this.week_days[obj.getDay()], date: obj.getDate() });
  }

  calendarFormat() {
    this.current_date = '01';
    this.fulldate = this.date + "-" + this.current_date;
    let obj = new Date(this.date)
    console.log("onhhhhhhhhhhh", obj)
    this.current_day = this.week_days[obj.getDay()]
    this.pushDate(obj);
  }

  date_change(date, day) {
    this.current_date = date;
    this.fulldate = this.date + "-" + this.current_date;
    console.log("date",this.fulldate);
    this.current_day = day;
    // localStorage.setItem('start_date',this.fulldate);
  }

}
