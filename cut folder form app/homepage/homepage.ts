import { Component, ViewChild, ElementRef, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  LoadingController,
  ActionSheetController,
  PopoverController
} from "ionic-angular";
import { App, MenuController } from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { GlobalService } from "../../app/global-service";
import { Events } from "ionic-angular";
import { FcmProvider } from '../../providers/fcm/fcm';

declare var google;
@IonicPage()
@Component({
  selector: "page-homepage",
  templateUrl: "homepage.html"
})
export class HomepagePage {
  show_location: string;
  internet: boolean;
  lang: any = localStorage.getItem("devicelang");
  data: any;
  city_name: string;
  country_name: string;
  team_total_page: any;
  player_total_page: any;
  upcoming_record: boolean;
  page2: number = 1;
  page1: number = 1;
  page: number = 1;
  teamcheck: boolean;
  playercheck: boolean;
  tourcheck: boolean;
  tours: any[] = [];
  teamname: any;
  up: any[]=[];
  city_id: any;
  country: string;
  upcoming: any;
  teams: any;
  players: any = [];
  tour: any;
  @ViewChild("map") mapElement: ElementRef;
  upcoming_game: boolean;
  map: any;
  img: any = this.global.imgpath;
  displayDiv: boolean = false;
  height: any;
  spinner: boolean = false;
  constructor(
    public events: Events,
    public popoverCtrl: PopoverController,
    public actionSheetCtrl: ActionSheetController,
    private global: GlobalService,
    public modalCtrl: ModalController,
    private geolocation: Geolocation,
    public navCtrl: NavController,
    public navParams: NavParams,
    public fcm: FcmProvider,
    private app: App,
    public zone:NgZone,
    menu: MenuController
  ) {

    events.subscribe("user:created", res => {
      this.ionViewWillLoad();
    });

    events.subscribe("selectcity:true", res => {
      this.ionViewWillLoad();
    });

    

  }

  ionViewWillEnter() {
    localStorage.removeItem("start_date");
    let url = this.global.basepath + "user/setLanguage/?language=" + localStorage.getItem('devicelang') + "&format=json"
    this.global.getRequest(url)
      .subscribe(res => {
      }, res => {
      })

  }
  pushPage(page) {
    this.app.getRootNav().push(page);
  }

  ionViewWillLoad() {
    this.country_name = localStorage.getItem('country_name')
    this.show_location = localStorage.getItem('city') + ", " + localStorage.getItem('country_name');
    this.internet = false;
    localStorage.removeItem("start_date");
    this.tours = [];
    this.up = [];
    this.players = [];
    this.teams = [];
    if (localStorage.getItem("user")) {
      this.upcoming_game = true;
      this.upcominggame();
    } else {
      this.upcoming_game = false;
    }
    this.tournament(this.page);
    this.player(this.page1);
    this.team(this.page2);
  }
  profilehome(pagename, id, status?) {
    console.log("page=>>>>>>>>>>>>>>>", pagename);
    if (pagename == "TournamentprofilePage") {
      localStorage.setItem("id", id);
      localStorage.setItem("status", status);
      let modal = this.modalCtrl.create(pagename, {});
      modal.present();
    } else if (pagename == "TeamdetailPage") {
      this.app.getRootNav().push(pagename, { id: id });
    } else if ((pagename = "PlayerdetailPage")) {
      this.app.getRootNav().push(pagename, { id: id });
    }
  }
  login() {
    let modal = this.modalCtrl.create("LoginPage", {
      login: true
    });
    modal.onDidDismiss(data => { });
    modal.present();
  }
  city() {
    this.app.getRootNav().push("SelectcityPage")
  }
  location() {
    this.geolocation.getCurrentPosition().then(
      position => {
        let latLng = new google.maps.LatLng(
          position.coords.latitude,
          position.coords.longitude
        );
        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(
          this.mapElement.nativeElement,
          mapOptions
        );
      },
      err => { }
    );
  }

  upcominggame() {
    let data = {
      form_type: "match",
      city: this.city_id,
      latitude: localStorage.getItem('lat'),
      longitude: localStorage.getItem('long'),
      distance: parseInt(localStorage.getItem('distance')),
    };

    let url = this.global.basepath + "user/homePage/?format=json";
    this.global.postRequestdata(url, data).subscribe(
      res => {
        this.zone.run(()=>{
          this.upcoming = res.json();
          this.up = this.upcoming.data;
          console.log("upcoming matches=>>>>>>>>>>>>>>>>>)",this.up);
          if (this.upcoming.data.length>0) {
            this.upcoming_record = true;
            this.teamname = this.upcoming.data[0].team;
          } else {
            this.upcoming_record = false;
          }
        })
      },
      error => {
        this.internet = true;
      }
    );
  }
  tournament(page, event?) {
    let data = {
      form_type: "tournament",
      city: this.city_id,
      page: page,
      latitude: localStorage.getItem('lat'),
      longitude: localStorage.getItem('long'),
      distance: parseInt(localStorage.getItem('distance')),
    };
    let url = this.global.basepath + "user/homePage/?format=json";
    this.global.postRequestdata(url, data).subscribe(
      res => {
        this.zone.run(()=>{
        this.tour = res.json();
        let arr = this.tour.data;
        if (page == 1) {
          this.tours = this.tour.data;
        } else {
          this.tours = this.tours.concat(arr);
        }
        if (this.tours.length) {
          this.tourcheck = true;
        } else {
          this.tourcheck = false;
        }
        if (event) {
          event.complete();
        }
      })
      },
      error => {
        if (event) {
          this.internet = true;

          event.complete();
        }
      }
    );
  }
  doInfinite_tour(ev, total_page) {
    console.log("tournament scrol=>>>>>>>>>>>>>>>>>>>>>>>>>>");
    this.page++;
    if (this.page <= total_page) {
      this.tournament(this.page, ev);
    } else {
      ev.enable(false);
    }
  }
  player(page, event?) {
    let data = {
      form_type: "player",
      city: this.city_id,
      page: page,
      latitude: localStorage.getItem('lat'),
      longitude: localStorage.getItem('long'),
      distance: parseInt(localStorage.getItem('distance')),
    };
    let url = this.global.basepath + "user/homePage/?format=json";
    this.global.postRequestdata(url, data).subscribe(
      res => {
        this.zone.run(()=>{
          let arr = res.json();
        this.player_total_page = arr.total_pages;
        if (page == 1) {
          this.players = arr.data;
        } else {
          this.players = this.players.concat(arr.data);
        }
        if (res.json().data.length) {
          this.playercheck = true;
        } else {
          this.playercheck = false;
        }
        if (event) {
          event.complete();
        }
      })
      },
      error => {
        if (event) {
          this.internet = true;

          event.complete();
        }
      }
    );
  }
  doInfinite_player(ev) {
    console.log("player scrol=>>>>>>>>>>>>>>>>>>>>>>>>>>");
    this.page1++;
    if (this.page1 <= this.player_total_page) {
      this.player(this.page1, ev);
    } else {
      ev.enable(false);
    }
  }
  team(page, event?) {
    let data = {
      form_type: "team",
      city: this.city_id,
      page: page,
      latitude: localStorage.getItem('lat'),
      longitude: localStorage.getItem('long'),
      distance: parseInt(localStorage.getItem('distance')),
    };
    let url = this.global.basepath + "user/homePage/?format=json";
    this.global.postRequestdata(url, data).subscribe(
      res => {
        this.zone.run(()=>{
        let arr = res.json();
        this.team_total_page = arr.total_pages;
        console.log("team page total", this.team_total_page);
        if (page == 1) {
          this.teams = res.json().data;
        } else {
          this.teams = this.teams.concat(arr.data);
        }
        if (res.json().data.length) {
          this.teamcheck = true;
        } else {
          this.teamcheck = false;
        }
        if (event) {
          event.complete();
        }
      })
      },
      error => {
        if (event) {
          this.internet = true;

          event.complete();
        }
      }
    );
  }
  doInfinite_team(ev) {
    console.log("team scrol=>>>>>>>>>>>>>>>>>>>>>>>>>>");
    this.page2++;
    if (this.page2 <= this.team_total_page) {
      this.team(this.page2, ev);
    } else {
      ev.enable(false);
    }
  }



}
