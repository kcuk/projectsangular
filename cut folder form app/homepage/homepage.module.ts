import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomepagePage } from './homepage';
import { TranslateModule } from '@ngx-translate/core';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    HomepagePage,
  ],
  imports: [
    IonicPageModule.forChild(HomepagePage),
    TranslateModule.forChild(),
     Ionic2RatingModule
    
  ],
  exports: [
    HomepagePage
  ]
})
export class HomepagePageModule {}
