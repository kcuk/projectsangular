import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComingsoonPage } from './comingsoon';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    ComingsoonPage,
  ],
  imports: [
    TranslateModule.forChild(),
    IonicPageModule.forChild(ComingsoonPage),
  ],
  exports: [
    ComingsoonPage
  ]
})
export class ComingsoonPageModule { }
