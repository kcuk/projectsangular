import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { GlobalService } from '../../../app/global-service';

@IonicPage()
@Component({
  selector: 'page-paymentrev',
  templateUrl: 'paymentrev.html',
})
export class PaymentrevPage {
  cash: any;
  vat: any;
  weekeveryday: any = "everyday";
  isrecurrent: any = false;
  recurrent_data: any;
  tilldate: string;
  // r_type: any;
  // week: any;
  main_ground: any;
  pay_optionc: boolean;
  pay_option: boolean;
  pay_method: any = [];
  res_id: number;
  pay_data_2cash: any;
  groundname: any;
  cid: any;
  pay_data: any;
  res: any;
  paymentdata: any = [];
  amount2: any;
  amount1: any;
  discount: any;
  minimum_fee: any;
  r_amount: any;
  st: number;
  et: number;
  end_time: any;
  r_data: any;
  start_time: string;
  selected_payment_option: any = 'cash';
  payment_message: string = "Su equipo registró con éxito para este torneo";
  rescheck: any = '';

  constructor(public modalCtrl: ModalController, private toast: ToastController, public navCtrl: NavController, public navParams: NavParams, private global: GlobalService) {

    if (this.navParams.get('rescheck') != '')
      this.rescheck = this.navParams.get('rescheck');
    else
      this.rescheck = '';
    // this.r_type = this.navParams.get('r_type');
    // this.week = this.navParams.get('week');
    // console.log("recurrent", this.r_type, this.week);
    this.vat = this.navParams.get('vat');
    this.discount = this.navParams.get('discount');
    this.minimum_fee = this.navParams.get('minimum_fee');
    this.r_amount = this.navParams.get('pay_ramount');
    this.cash = parseFloat(this.navParams.get('pay_ramount')).toFixed(2);
    this.groundname = this.navParams.get('g_name');
    this.main_ground = this.navParams.get('main_ground');
    this.cid = this.navParams.get('cid')
    localStorage.setItem('c_id', this.cid)
    this.pay_method = JSON.parse(localStorage.getItem('pay_method'));
    console.log("PAYMENT", this.pay_method);
    this.pay_method.forEach(element => {
      console.log("element", element);
      if (element.abbreviation == "online" || element.abbreviation == "card") {
        this.selected_payment_option = 'minimum_fee'
        this.pay_option = true;
      }
      else if (element.abbreviation == "cash") {
        this.pay_optionc = true;
      }
    });

    if (parseFloat(this.minimum_fee) < parseFloat(this.r_amount)) {
      if (this.minimum_fee == 0)
        this.amount1 = parseFloat(this.r_amount).toFixed(2);
      else
        this.amount1 = parseFloat(this.minimum_fee).toFixed(2);
    }
    else {
      this.amount1 = parseFloat(this.r_amount).toFixed(2);
    }


    if (this.discount == 0) {
      this.amount2 = parseFloat(this.r_amount).toFixed(2);
    }
    else {
      this.amount2 = this.r_amount - ((this.r_amount * this.discount) / 100);
      this.amount2 = parseFloat(this.amount2).toFixed(2);
    }
    this.start_time = localStorage.getItem('start_time');
    this.st = parseInt(localStorage.getItem('start_time'));
    this.et = this.st + 1;
    // console.log("st",this.st,this.et);
    if (this.et == 24) {
      this.end_time = "00:00:00";
      // console.log("24",this.end_time);
    }
    if (this.et <= 9) {
      this.end_time = "0" + this.et + ":00:00";
      // console.log("10",this.end_time);
    }
    if (this.et < 23 && this.et >= 10) {
      this.end_time = this.et + ":00:00";
      // console.log("23",this.end_time);
    }
  }

  ionViewWillEnter() {
    this.global.loader.dismiss();
    console.log('ionViewDidLoad PaymentrevPage');
    if (localStorage.getItem('recurrent_data')) {
      this.recurrent_data = JSON.parse(localStorage.getItem('recurrent_data'));
      console.log("recurrent_data", this.recurrent_data);
      this.isrecurrent = this.recurrent_data.r_type;
      this.weekeveryday = this.recurrent_data.weekeveryday;
    }
  }
  paid() {
    // this.global.presentLoading();
    if (localStorage.getItem('r_start_date')) {
      this.tilldate = localStorage.getItem('r_start_date');
    }
    else {
      this.tilldate = "";
    }
    // if (!localStorage.getItem('r_type')) {
    //   this.r_type = false;
    // }
    // if (this.week == true) {
    //   this.week = "everyday";
    // }
    // else {
    //   this.week = "weekly";
    // }

    if (this.selected_payment_option == 'cash') {
      this.global.presentLoading();
      let data = {
        court_name: this.groundname,
        date: localStorage.getItem('reservation_date'),
        email: null,
        form_type: "reserve",
        from_time: this.start_time,
        ground: this.cid,
        guest_name: null,
        is_existed: "false",
        is_guest: true,
        is_recurrent_reservations: this.isrecurrent,
        main_ground: this.main_ground,
        message: "",
        paid_with: this.selected_payment_option,
        phone: "",
        price: this.cash,
        recurrent_reservations_type: this.weekeveryday,
        reserved_through: null,
        till_date: this.tilldate,
        to_time: this.end_time,
        user_id: 0
      }
      let url = this.global.basepath + "scorecard/ground_request/?format=json";
      this.global.postRequestdata(url, data)
        .subscribe(res => {
          this.pay_data = res.json();
          console.log("pay_data", this.pay_data);
          this.res_id = this.pay_data.reservation_id;
          this.field_reserve();
        },
        (error) => {
          this.global.loader.dismiss();
          console.log("this is an error", error);
        });
    }
    else if (this.selected_payment_option == 'discount') {
      this.navCtrl.push('GroundPayCartPage'
        , { key: "discount", vat: this.vat, discount: this.discount, t_amount: this.r_amount, r_amount: this.amount2, main_ground: this.main_ground, ground_name: this.groundname, rescheck: this.rescheck });
    }
    else if (this.selected_payment_option == 'minimum_fee') {
      this.navCtrl.push('GroundPayCartPage'
        , { vat: this.vat, t_amount: this.r_amount, r_amount: this.amount1, main_ground: this.main_ground, ground_name: this.groundname, rescheck: this.rescheck });
    }
  }

  field_reserve() {
    let data = {
      ground_id: localStorage.getItem('g_id'),
      court_id: this.cid,
      reservation_id: this.res_id,
      "request_type": "cash",
    }
    let url = this.global.basepath + "ground/field_reserve_pay/?format=json";
    this.global.postRequestdata(url, data)
      .subscribe(res => {
        this.global.loader.dismiss();
        this.pay_data_2cash = res.json();
        let toastController = this.toast.create({
          message: this.global.lang == 'en' ? "Reservation request send successful" : "Pedido de reserva enviado exitoso",
          duration: 1000,
        });
        toastController.present();
        this.global.loader.dismiss();
        localStorage.removeItem('start_time');
        localStorage.setItem('start_date', localStorage.getItem('current_date'));
        if (this.navParams.get('rescheck') != '') {
          this.navCtrl.pop()
        }
        else {
          this.navCtrl.setRoot('FieldprofilePage');
        }
      },
      (error) => {
        this.global.loader.dismiss();
        let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })

        toastController.present();
        localStorage.removeItem('start_time');
        localStorage.setItem('start_date', localStorage.getItem('current_date'));
        if (this.navParams.get('rescheck') != '')
          this.navCtrl.setRoot('CanchaResPage')
        else
          this.navCtrl.setRoot('FieldprofilePage');
      });
  }

  // rec() {
  //   this.navCtrl.push('RecurrentPage');
  // }

  ionViewWillLeave() {
    this.global.loader.dismiss();
  }
}
