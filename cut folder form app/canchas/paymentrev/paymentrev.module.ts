import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentrevPage } from './paymentrev';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PaymentrevPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentrevPage),
    TranslateModule.forChild()
    
  ],
  exports: [
    PaymentrevPage
  ]
})
export class PaymentrevPageModule {}
