import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ModalController, LoadingController } from 'ionic-angular';
import { GlobalService } from '../../../../app/global-service';
declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-ground-pay-cart',
  templateUrl: 'ground-pay-cart.html',
})
export class GroundPayCartPage {
  [x: string]: any;
  weekeveryday: any = "everyday";
  isrecurrent: any = false;
  min: boolean;
  tmin_amount: any;
  payment_online: any;
  card: boolean[] = [];
  reservation_id: any;
  online: boolean = true;
  final_amount: any;
  t_amount: any;
  d_amount: any;
  gateway_amount: any;
  start_time: string;
  end_time: string;
  et: number;
  st: number;
  start_date: string;
  key: any;
  main_ground: any;
  ground_name: any;
  payment_cdata: any;
  payment_odata: any;
  payment_list: any;
  card_reference: any = '';
  payment_addcard: any;
  pay_url: string;
  pay_value: string = '';
  p_amount: any;
  payment_data: any[] = [];
  rescheck: any = '';
  tilldate: any;
  autoManufacturers: string = "";
  vat: any;
  vat_percentage: any;
  sub_total: any;
  pay_by_other: any;
  pay_by_other_amount: any;
  pay_by_other_regis_id: any;
  pay_by_other_vat: any;
  pay_by_other_vat_amount: any;
  gateway_vat: any;
  check_recurrent: boolean = true;

  constructor(public modalCtrl: ModalController, private toast: ToastController, public alertCtrl: AlertController, private global: GlobalService, public navCtrl: NavController, public navParams: NavParams) {
    this.pay_by_other = navParams.get('pay_by_other');
    if (localStorage.getItem('recurrent_data')) {
      this.check_recurrent = false;
    }
    else {
      this.check_recurrent = true;
    }
    if (this.pay_by_other === 'true') {
      this.pay_by_other_amount = parseFloat(navParams.get('amount')).toFixed(2);
      this.pay_by_other_vat = navParams.get('vat');
      this.pay_by_other_vat_amount = ((this.pay_by_other_amount * this.pay_by_other_vat) / 100).toFixed(2);
      this.gateway_vat = ((this.pay_by_other_amount * 12) / 100).toFixed(2);
      this.pay_by_other_regis_id = navParams.get('reservation_id');
      this.sub_total = (parseFloat(this.pay_by_other_amount) + parseFloat(this.gateway_vat) + parseFloat(this.pay_by_other_vat_amount)).toFixed(2);
      this.gateway_amount = ((this.sub_total * 1.5) / 100).toFixed(2);
      this.t_amount = (parseFloat(this.gateway_amount) + parseFloat(this.sub_total)).toFixed(2);

    }
    else {
      if (this.navParams.get('rescheck') != '')
        this.rescheck = this.navParams.get('rescheck');
      else
        this.rescheck = '';

      let amount: any = navParams.get('r_amount');
      this.p_amount = parseFloat(amount).toFixed(2);
      this.tmin_amount = parseFloat(navParams.get('t_amount')).toFixed(2);
      this.key = this.navParams.get('key');
      this.start_date = localStorage.getItem('reservation_date');
      this.main_ground = this.navParams.get('main_ground');
      this.ground_name = this.navParams.get('ground_name');
      this.vat_percentage = this.navParams.get('vat');
      let calculate_vat = (this.vat_percentage * this.p_amount) / 100;
      this.vat = calculate_vat.toFixed(2);
      this.gateway_vat = ((amount * 12) / 100).toFixed(2);
      this.sub_total = (parseFloat(this.p_amount) + parseFloat(this.vat) + parseFloat(this.gateway_vat)).toFixed(2);
      this.gateway_amount = ((this.sub_total * 1.5) / 100).toFixed(2);
      if (this.navParams.get('key')) {
        this.t_amount = (parseFloat(this.gateway_amount) + parseFloat(this.sub_total)).toFixed(2);
        this.key = "discount";
        this.discount = this.navParams.get('discount')
        this.final_amount = "00.00";
        this.min = false;
      }
      else {
        this.t_amount = (parseFloat(this.gateway_amount) + parseFloat(this.sub_total)).toFixed(2);
        this.key = "minimum_fee";
        if (this.navParams.get('r_amount') < this.navParams.get('t_amount')) {
          this.final_amount = this.tmin_amount - parseInt(this.p_amount);
          this.final_amount = parseFloat(this.final_amount).toFixed(2);
        }
        else {
          this.final_amount = "00.00"
        }
        this.min = true;
      }
      this.st = parseInt(localStorage.getItem('start_time'));
      this.start_time = (localStorage.getItem('start_time'));
      this.et = this.st + 1;
      if (this.et == 24) {
        this.end_time = "00:00";
      }
      if (this.et <= 9) {
        this.end_time = "0" + this.et + ":00";
      }
      if (this.et < 23 && this.et >= 10) {
        this.end_time = this.et + ":00";
      }
    }
    this.selected();


  }
  ionViewWillEnter() {
    if (localStorage.getItem('recurrent_data')) {
      this.recurrent_data = JSON.parse(localStorage.getItem('recurrent_data'));
      this.isrecurrent = this.recurrent_data.r_type;
      this.weekeveryday = this.recurrent_data.weekeveryday;
    }
    if (localStorage.getItem('r_start_date')) {
      this.tilldate = localStorage.getItem('r_start_date');
    }
    else {
      this.tilldate = "";
    }
  }


  selected() {
    this.global.presentLoading();
    let url = this.global.basepath + "ground/field_list_card/";
    this.global.getRequest(url)
      .subscribe(res => {
        this.global.loader.dismiss();
        this.payment_data = JSON.parse(res.json());
        if (this.payment_data.length) {
          this.card = [].fill(false, 0, this.payment_data.length);
          this.card[0] = true;
          this.card_reference = this.payment_data[0].card_reference;
          this.online = false;
          this.pay_value = ''
        }
        else {
          this.online = true;
          this.pay_value = 'online'

        }

        //SAVED CARD API
        let url1 = this.global.basepath + "ground/saveCardDetails/";
        this.global.getRequest(url1)
          .subscribe(res1 => {
          }, (error) => {  })
      }
      , (error) => {
        this.global.loader.dismiss();
      })
  }

  status(value) {
    this.pay_value = value;
    this.card_reference = '';
    this.online = true;
    if (this.payment_data.length) {
      this.card = [].fill(false, 0, this.payment_data.length);
    }
  }
  addcard() {
    this.global.presentLoading();
    let data =
      {
        success_url: "https://www.google.co.in?status=1",
        failure_url: "https://www.google.co.in?status=2"
      }
    let url = this.global.basepath + "/ground/field_add_card/";
    this.global.postRequestdata(url, data)
      .subscribe(res => {
        this.payment_addcard = res.json();
        var target = "_blank";
        var options = "location=no";
        var inAppBrowserRef;
        this.global.loader.dismiss();
        inAppBrowserRef = cordova.InAppBrowser.open(this.payment_addcard, target, options);
        inAppBrowserRef.addEventListener('loadstart', loadStartCallBack => {
          let status = loadStartCallBack.url.split('?')[1].split('&')[0]
          if (status == 'status=1') {
            inAppBrowserRef.close();
            this.selected();
            this.addcard_success();
          }
          else if (status == 'status=2') {
            inAppBrowserRef.close();
            this.addcard_failed();
          }
        });
      }
      , (error) => {
        this.global.loader.dismiss();
      })
  }

  addcard_failed() {
    let alert = this.alertCtrl.create({
      title: this.global.lang == 'en' ? 'Card not added' : "Tarjeta no agregada",
    });
    alert.present();
    setTimeout(() => {
      alert.dismiss();
    }, 2000);
  }

  addcard_success() {
    let alert = this.alertCtrl.create({
      title: this.global.lang == 'en' ? 'Card added successful' : "Tarjeta agregada exitosa",
    });
    alert.present();
    setTimeout(() => {
      alert.dismiss();
    }, 2000);
  }

  card_remove(rf, j) {
    let confirm = this.alertCtrl.create({
      title: this.global.lang == 'en' ? 'Are you sure?' : "Estas seguro",
      message: this.global.lang == 'en' ? 'You want to delete this card' : "Quieres eliminar esta carta",
      buttons: [
        { text: 'No', handler: () => {  } },
        {
          text: this.global.lang == 'en' ? 'Yes' : "Sí",
          cssClass: 'button_ok',
          handler: () => {
            this.global.presentLoading();
            let card_reference = rf;
            let data = { "card_reference": card_reference, }
            let url = this.global.basepath + "/ground/field_delete_card/";
            this.global.postRequestdata(url, data)
              .subscribe(res => {
                this.global.loader.dismiss();
                this.selected();
              }, (error) => {
                this.global.loader.dismiss();
                let toastController = this.toast.create({
                  message: this.global.lang == 'en' ? "You should have atleast 1 card" : "Deberías tener al menos 1 carta",
                  duration: 2000,
                })
                toastController.present()
              }
              )
          }
        }
      ]
    });
    confirm.present();
  }
  card_select(rf, j) {
    this.card_reference = rf;
    this.pay_value = '';
    this.online = false;
    if (this.payment_data.length) {
      this.card = [].fill(false, 0, this.payment_data.length)
      this.card[j] = true;
    }
  }

  ionViewWillLeave() {
    this.global.loader.dismiss();
  }


  pay() {
    if (this.pay_value == '' && this.card_reference == '') {
      let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Please select any payment option" : "Por favor seleccione cualquier opción de pago", duration: 2000, })
      toastController.present();
    }
    else if (this.pay_by_other === 'true') {
      let confirm = this.alertCtrl.create({
        title: this.global.lang == 'en' ? 'Are you sure?' : "Estas seguro",
        message: this.global.lang == 'en' ? 'You want to pay rest amount!' : "¡Quieres pagar la cantidad del resto!",
        buttons: [
          { text: 'No', handler: () => {  } },
          {
            text: this.global.lang == 'en' ? 'Yes' : "Sí",
            cssClass: 'button_ok',
            handler: () => {
              if (this.pay_value == "online")
                this.payByOtherOnline();
              else
                this.payByOtherCard();
            }
          }],
      })
      confirm.present();
    }
    else {
      if (this.pay_value == "online") {
        // this.pay_online();
        let confirm = this.alertCtrl.create({
          title: this.global.lang == 'en' ? 'Are you sure?' : "Estas seguro",
          message: this.global.lang == 'en' ? 'You want to make reservation' : "Quieres hacer una reserva",
          buttons: [
            { text: 'No', handler: () => {  } },
            {
              text: this.global.lang == 'en' ? 'Yes' : "Sí",
              cssClass: 'button_ok',
              handler: () => {
                this.pay_reccurrent();
              }
            }],
        })
        confirm.present();
      }
      else if (this.card_reference != '') {
        // this.pay_card();
        let confirm = this.alertCtrl.create({
          title: this.global.lang == 'en' ? 'Are you sure?' : "Estas seguro",
          message: this.global.lang == 'en' ? 'You want to make reservation' : "Quieres hacer una reserva",
          buttons: [
            { text: 'No', handler: () => {  } },
            {
              text: this.global.lang == 'en' ? 'Yes' : "Sí",
              cssClass: 'button_ok',
              handler: () => {
                this.pay_reccurrent();
              }
            }],
        })
        confirm.present();
      }
    }
  }

  pay_reccurrent() {
    let url = this.global.basepath + 'scorecard/ground_request/?format=json';
    let data = {
      court_name: this.ground_name,
      date: localStorage.getItem('reservation_date'),
      email: null,
      form_type: "reserve",
      from_time: this.start_time,
      ground: localStorage.getItem('c_id'),
      guest_name: null,
      is_existed: "false",
      is_guest: true,
      is_recurrent_reservations: this.isrecurrent,
      main_ground: this.main_ground,
      message: "",
      paid_with: this.key,
      phone: "",
      price: this.p_amount,
      recurrent_reservations_type: this.weekeveryday,
      reserved_through: null,
      till_date: this.tilldate,
      to_time: this.end_time,
      user_id: 0
    }

    this.global.postRequestdata(url, data)
      .subscribe(res => {
        this.payment_odata = res.json();
        this.reservation_id = this.payment_odata.reservation_id;
        if (this.pay_value == "online") {
          this.pay_value = '';
          this.pay_online();
        }
        else if (this.card_reference != '') {
          this.pay_card();
        }
      }, (error) => {
        this.pay_value = '';
        let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })
        toastController.present();
        localStorage.removeItem('start_time');
        localStorage.setItem('start_date', localStorage.getItem('current_date'));
        if (this.navParams.get('rescheck') != '')
          this.navCtrl.setRoot('CanchaResPage')
        else
          this.navCtrl.setRoot('FieldprofilePage');
      })

  }


  pay_online() {
    let data = {
      ground_id: localStorage.getItem('g_id'),
      court_id: localStorage.getItem('c_id'),
      reservation_id: this.reservation_id,
      request_type: this.key,
    }
    let url = this.global.basepath + "ground/field_reserve_pay/?format=json";
    this.global.postRequestdata(url, data)
      .subscribe(res => {
        this.pay_value = '';
        this.payment_odata = res.json();
        this.pay_url = this.payment_odata.header + '?application_code=' + this.payment_odata.application_code
          + '&uid=' + this.payment_odata.uid
          + '&auth_timestamp=' + this.payment_odata.auth_timestamp
          + '&auth_token=' + this.payment_odata.auth_token
          + '&dev_reference=' + this.payment_odata.dev_reference
          + '&product_description=' + this.payment_odata.product_description
          + '&product_code=' + this.payment_odata.product_code
          + '&product_amount=' + this.payment_odata.product_amount
          + '&success_url=' + "https://www.google.co.in?status=1"
          + '&failure_url=' + "https://www.google.co.in?status=2"
          + '&failure_url=' + "https://www.google.co.in/"
          + '&review_url=' + this.payment_odata.review_url
          + '&vat=' + this.payment_odata.vat
          + '&installments_type=' + this.payment_odata.installments_type
          + '&tax_percentage=' + this.payment_odata.tax_percentage
          + '&taxable_amount=' + this.payment_odata.taxable_amount
        var target = "_blank";
        var options = "location=no";
        var inAppBrowserRef;
        inAppBrowserRef = cordova.InAppBrowser.open(this.pay_url, target, options);
        inAppBrowserRef.addEventListener('loadstart', loadStartCallBack => {
          let status = loadStartCallBack.url.split('?')[1].split('&')[0]
          if (status == 'status=1') {
            inAppBrowserRef.close();
            this.global.loader.dismiss();
            this.navCtrl.setRoot('FieldprofilePage', {
              status: 'true'
            })
          }
          else if (status == 'status=2') {
            inAppBrowserRef.close();
            this.global.loader.dismiss();
            this.navCtrl.setRoot('FieldprofilePage', {
              status: 'false'
            })
          }
        });

        // new InAppBrowser().create(this.pay_url);
        // localStorage.removeItem('start_time');
        // localStorage.setItem('start_date', localStorage.getItem('current_date'));
        // if (this.navParams.get('rescheck') != '')
        //   this.navCtrl.setRoot('CanchaResPage')
        // else
        //   this.navCtrl.setRoot('FieldprofilePage');
      }
      , (error) => {
        this.pay_value = '';
        let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })
        toastController.present();
        localStorage.removeItem('start_time');
        localStorage.setItem('start_date', localStorage.getItem('current_date'));
        if (this.navParams.get('rescheck') != '')
          this.navCtrl.setRoot('CanchaResPage')
        else
          this.navCtrl.setRoot('FieldprofilePage');
      })
  }
  pay_card() {
    let saveddata = {
      ground_id: localStorage.getItem('g_id'),
      court_id: localStorage.getItem('c_id'),
      reservation_id: this.reservation_id,
      card_reference: this.card_reference,
      request_type: this.key,
    }
    let url = this.global.basepath + "ground/field_reserve_card_pay/?format=json";
    this.global.presentLoading();
    this.global.postRequestdata(url, saveddata)
      .subscribe(res => {
        localStorage.removeItem('recurrent_data');
        localStorage.removeItem('r_start_date')
        localStorage.removeItem('reservation_date')

        this.global.loader.dismiss();
        this.payment_cdata = JSON.parse(res.json());
        if (this.payment_cdata.status == "success") {
          let modal = this.modalCtrl.create('PaymentSuccessPage', { transaction_id: this.payment_cdata.transaction_id, rescheck: this.rescheck });
          modal.present();
          setTimeout(() => { modal.dismiss(); }, 5000);
          if (this.navParams.get('rescheck') != '')
            this.navCtrl.setRoot('CanchaResPage')
          else
            this.navCtrl.setRoot('FieldprofilePage');
        }
        else {
          let modal = this.modalCtrl.create('PaymentFailurePage', { transaction_id: this.payment_cdata.transaction_id, rescheck: this.rescheck });
          modal.present();
          setTimeout(() => { modal.dismiss(); }, 5000);
          if (this.navParams.get('rescheck') != '')
            this.navCtrl.setRoot('CanchaResPage')
          else
            this.navCtrl.setRoot('FieldprofilePage');
        }
      }
      , (error) => {
        localStorage.removeItem('recurrent_data');
        localStorage.removeItem('r_start_date')
        localStorage.removeItem('reservation_date')

        this.card_reference = '';
        let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })
        toastController.present();
        if (this.navParams.get('rescheck') != '')
          this.navCtrl.setRoot('CanchaResPage')
        else
          this.navCtrl.setRoot('FieldprofilePage');
      })
  }

  payByOtherOnline() {
    let data = {
      "reservation_id": this.pay_by_other_regis_id,
      "pay_type": "online",
      ground_id: localStorage.getItem('g_id')
    }

    let url = this.global.basepath + "ground/reserveElse/";
    this.global.postRequestdata(url, data)
      .subscribe(res => {
        this.pay_value = '';
        this.payment_odata = res.json();
        this.pay_url = this.payment_odata.header + '?application_code=' + this.payment_odata.application_code
          + '&uid=' + this.payment_odata.uid
          + '&auth_timestamp=' + this.payment_odata.auth_timestamp
          + '&auth_token=' + this.payment_odata.auth_token
          + '&dev_reference=' + this.payment_odata.dev_reference
          + '&product_description=' + this.payment_odata.product_description
          + '&product_code=' + this.payment_odata.product_code
          + '&product_amount=' + this.payment_odata.product_amount
          + '&success_url=' + "https://www.google.co.in?status=1"
          + '&failure_url=' + "https://www.google.co.in?status=2"
          + '&failure_url=' + "https://www.google.co.in/"
          + '&review_url=' + this.payment_odata.review_url
          + '&vat=' + this.payment_odata.vat
          + '&installments_type=' + this.payment_odata.installments_type
          + '&tax_percentage=' + this.payment_odata.tax_percentage;
        var target = "_blank";
        var options = "location=no";
        var inAppBrowserRef;
        inAppBrowserRef = cordova.InAppBrowser.open(this.pay_url, target, options);
        inAppBrowserRef.addEventListener('loadstart', loadStartCallBack => {
          let status = loadStartCallBack.url.split('?')[1].split('&')[0]
          if (status == 'status=1') {
            inAppBrowserRef.close();
            this.global.loader.dismiss();
            this.navCtrl.setRoot('FieldprofilePage', {
              status: 'true'
            })
          }
          else if (status == 'status=2') {
            inAppBrowserRef.close();
            this.global.loader.dismiss();
            this.navCtrl.setRoot('FieldprofilePage', {
              status: 'false'
            })
          }
        });

      }
      , (error) => {
        this.pay_value = '';
        let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })
        toastController.present();
      })
  }

  payByOtherCard() {
    let saveddata = {
      "reservation_id": this.pay_by_other_regis_id,
      "pay_type": "card",
      "card_reference": this.card_reference,
      ground_id: localStorage.getItem('g_id')

    }
    let url = this.global.basepath + "ground/reserveElse/";
    this.global.presentLoading();
    this.global.postRequestdata(url, saveddata)
      .subscribe(res => {
        this.global.loader.dismiss();
        this.payment_cdata = JSON.parse(res.json());
        if (this.payment_cdata.status == "success") {
          let modal = this.modalCtrl.create('PaymentSuccessPage', { transaction_id: this.payment_cdata.transaction_id });
          modal.present();
          setTimeout(() => { modal.dismiss(); }, 5000);
          this.navCtrl.setRoot('FieldprofilePage');
        }
        else {
          let modal = this.modalCtrl.create('PaymentFailurePage', { transaction_id: this.payment_cdata.transaction_id, rescheck: this.rescheck });
          modal.present();
          setTimeout(() => { modal.dismiss(); }, 5000);
          this.navCtrl.setRoot('FieldprofilePage');
        }
      }
      , (error) => {
        this.card_reference = '';
        let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })
        toastController.present();
        this.navCtrl.setRoot('FieldprofilePage');
      })
  }

}
