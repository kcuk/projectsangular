import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroundPayCartPage } from './ground-pay-cart';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    GroundPayCartPage,
  ],
  imports: [
    IonicPageModule.forChild(GroundPayCartPage),
    TranslateModule.forChild()
    
  ],
  exports: [
    GroundPayCartPage
  ]
})
export class GroundPayCartPageModule {}
