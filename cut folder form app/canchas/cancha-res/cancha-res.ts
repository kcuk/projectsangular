import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, ToastController, AlertController } from 'ionic-angular';
import { GlobalService } from "../../../app/global-service";


@IonicPage({
	segment: 'cancha-res'
}
)
@Component({
	selector: 'page-cancha-res',
	templateUrl: 'cancha-res.html',
})
export class CanchaResPage {
	pay_data_2cash: any;
	res_id: any;
	pay_data: any;
	court_data: any;
	data_display: any;
	min_date: string;
	check_recurrent: boolean = false;
	vat: any;
	from_time: any;
	to_time: string;
	et: number;
	st: number;
	court_name: any;
	main_ground: any;
	pay_method: any;
	admin: any;
	discount: any;
	minimum_fee: any;
	pay_amount: any = '';
	court_cid: string;
	court_id: string;
	fieldrecord_new: { id: string; start_date: string; };
	i: number = 1;
	new_date: string;
	old_date: string;
	all_ground: any;
	connected_ground: any;
	ground_name: any;
	fieldrecord: any;
	time: any;
	am: string = "am";
	pm: string = "pm";
	data: any;
	timeground: boolean = false;
	id: string = localStorage.getItem('g_id');
	// start_date: string = localStorage.getItem('start_date');
	samay: any;
	button_click: boolean = false;
	date: any;
	days: any;
	year: any;
	month: any;
	month_dummy: any = [1, 2, 3, 4, 5, 6, 7];
	week_days: any = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	month_days: any = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	current_date: any;
	selectd_date: any;
	current_day: any;
	day_array: any[] = [];
	fulldate: any;
	drop: boolean = false;
	display: false;
	court: any;
	rescheck: any = '';
	show: boolean = false;
	recurrent: boolean = false;
	spinner: boolean = true;
	lang: any = localStorage.getItem('devicelang');
	months: any =(this.lang == 'es'? "[ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic]":"[Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec]");

	constructor(public alertCtrl: AlertController, private toast: ToastController, public navCtrl: NavController, public navParams: NavParams, private global: GlobalService, public modalCtrl: ModalController) {
		//TO CHECK FROM CANCHA-RES PAGE
		if (this.navParams.get('cancha_res')) {
			this.show = false;
			this.rescheck = 'cancha_res'
		}
		else {
			this.show = true;
			this.rescheck = '';
		}
		if (this.navParams.get('check_recurrent')) {
			this.check_recurrent = true
		}
		else {
			this.check_recurrent = false;
		}
		if (this.lang == 'es') {
			this.week_days = ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb']
		  }
		  else {
			this.week_days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
		  }
	}


	ionViewDidLoad() {
		this.recurrent = false;
		localStorage.removeItem('recurrent_data');
		localStorage.removeItem('r_start_date')
		let obj = new Date();
		this.year = obj.getFullYear();
		this.month = obj.getMonth();
		this.current_day = this.week_days[obj.getDay()];
		this.current_date = obj.getDate();
		if (this.current_date < 10) {
			this.current_date = '0' + this.current_date
		}
		if (this.month > 8) {
			this.month++;
		}
		else {
			this.month = '0' + (this.month + 1);
		}
		this.min_date = this.year + "-" + this.month + "-" + this.current_date;
		let new_obj = new Date(localStorage.getItem('start_date'))
		console.log("reservation dateee=>>>>>>>", new_obj)
		if (new Date().getMonth() == new_obj.getMonth() && new Date().getFullYear() == new_obj.getFullYear()) {
			this.selectd_date = new_obj.getDate();
			console.log("reservation if dateee=>>>>>>>", this.selectd_date)

			this.date = this.year + "-" + this.month;
			this.fulldate = this.date + "-" + this.selectd_date;
			console.log("reservation if dateee=>>>>>>>", this.fulldate)

			this.pushDate(new Date(this.date + "-" + this.current_date));
		}
		else {
			this.selectd_date = new_obj.getDate();

			this.year = new_obj.getFullYear();
			this.month = new_obj.getMonth();
			if (this.month > 8) {
				this.month++;
			}
			else {
				this.month = '0' + (this.month + 1);
			}
			this.date = this.year + "-" + this.month;
			this.fulldate = this.date + "-" + this.selectd_date;
			console.log("reservation elseeeee dateee=>>>>>>>", this.fulldate)
			this.pushDate(new Date(this.date))
		}
		this.canchasres();
	}

	canchasres($event?) {
		if (!$event) { this.spinner = true }
		this.fieldrecord = {
			id: this.id,
			start_date: this.fulldate,
		}
		let url = this.global.basepath + "ground/reserveschedule/?format=json";
		this.global.postRequestdata(url, this.fieldrecord)
			.subscribe(res => {
				this.spinner = false
				this.data = res.json();
				this.data_display = res.json()
				if (this.data.ground_data.field_format && this.data.ground_data.type_of_field) {
					if (this.lang == 'en') {
						this.court = this.data.ground_data.ground_name + ',' + this.data.ground_data.field_format + "," + this.data.ground_data.type_of_field_en;
					}
					else {
						this.court = this.data.ground_data.ground_name + ',' + this.data.ground_data.field_format + "," + this.data.ground_data.type_of_field;
					}
				}
				else if (this.data.ground_data.field_format) {
					this.court = this.data.ground_data.field_format
				}
				else if (this.data.ground_data.type_of_field) {
					if (this.lang == 'en') {
						this.court = this.data.ground_data.type_of_field_en;
					}
					else {
						this.court = this.data.ground_data.type_of_field;
					}
				}
				this.main_ground = this.data.main_ground;
				this.vat = this.data.vat;
				this.discount = this.data.discount;
				this.minimum_fee = this.data.minimum_fee;
				this.pay_method = JSON.stringify(this.data.payment_methods);
				localStorage.setItem('pay_method', this.pay_method);
				let payment_check_online: any = JSON.parse(localStorage.getItem('pay_method'));
				payment_check_online.forEach(element => {
					if (element.abbreviation == "online" || element.abbreviation == "card") {
						this.check_recurrent = true;
					}
					// else if (element.abbreviation == "cash") {
					//   this.pay_cash = true;
					// }
				});
				this.court_id = this.data.ground_data.id;
				console.log("court idddddd", this.court_id, "iddddddddddddddddddd", this.id)
				this.all_ground = this.court;
				this.ground_name = this.data.ground_data.ground_name;
				if (!this.data.connected_grounds.length) {
					this.drop = true;
				}
				else {
					this.drop = false;
				}
				this.court_data = this.court
			},
				(error) => {
					this.spinner = false
				});
	}

	groundid(id, y) {
		this.court_id = id;
		this.ground_name = y;
		this.option();
	}

	option() {
		// this.court_name = gn;
		// console.log("event", e, this.court_name);
		this.fieldrecord = {
			id: this.id,
			court: this.court_id,
			start_date: this.fulldate,
		}
		let url = this.global.basepath + "ground/reserveschedule/?format=json";
		this.global.postRequestdata(url, this.fieldrecord)
			.subscribe(res => {
				this.spinner = false
				this.data_display = null;
				this.data_display = res.json();
				this.discount = this.data.discount;
				this.minimum_fee = this.data.minimum_fee;
				this.admin = this.data.admin;
			},
				(error) => {
					this.spinner = false
				});
	}

	pay() {
		let data = {
			court_name: this.ground_name,
			date: this.fulldate,
			email: null,
			form_type: "reserve",
			from_time: this.from_time,
			ground: this.court_id,
			guest_name: null,
			is_existed: "false",
			is_guest: true,
			is_recurrent_reservations: false,
			main_ground: this.data.main_ground,
			message: "",
			paid_with: "cash",
			phone: "",
			price: this.pay_amount,
			recurrent_reservations_type: "everyday",
			reserved_through: null,
			till_date: "",
			to_time: this.to_time,
			user_id: 0
		}
		if (this.pay_amount === 0) {
			this.global.presentLoading();
			let url = this.global.basepath + "scorecard/ground_request/?format=json";
			this.global.postRequestdata(url, data)
				.subscribe(res => {
					this.global.loader.dismiss();
					let toastController = this.toast.create({
						message: this.global.lang == 'en' ? "Request sent successfully" : 'Solicitud enviada con éxito',
						duration: 3000,
					})
					toastController.present();
					this.navCtrl.pop();
				},
					(error) => {
						this.global.loader.dismiss();
						let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })
						toastController.present();
						this.navCtrl.pop();
					});
		}
		else if (this.pay_amount === '') {
			let toast = this.toast.create({
				message: this.global.lang == 'en' ? 'Please select time slot' : "Seleccione el intervalo de tiempo",
				duration: 2000
			})
			toast.present();
		}
		else {
			if (!this.recurrent) {
				this.global.presentLoading();
				let url = this.global.basepath + "scorecard/ground_request/?format=json";
				this.global.postRequestdata(url, data)
					.subscribe(res => {
						this.pay_data = res.json();
						console.log("pay_data", this.pay_data);
						this.res_id = this.pay_data.reservation_id;
						this.field_reserve();
					},
						(error) => {
							this.global.loader.dismiss();
							let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })
							toastController.present();
							this.navCtrl.pop();
						});
			}
			else {
				this.navCtrl.push('RecurrentPage', { reservation_date: this.fulldate, g_name: this.ground_name, cid: this.court_id, main_ground: this.data.main_ground, rescheck: this.rescheck, pay_ramount: this.pay_amount })
			}
			// localStorage.setItem('reservation_date', this.fulldate)
			// let id;
			// if (this.court_cid)
			// 	id = this.court_cid;
			// else
			// 	id = this.court_id;
			// if (!this.recurrent)
			// 	this.navCtrl.push('PaymentrevPage',
			// 		{
			// 			vat: this.vat,
			// 			discount: this.discount,
			// 			minimum_fee: this.minimum_fee,
			// 			pay_ramount: this.pay_amount,
			// 			g_name: this.ground_name = this.ground_name,
			// 			cid: id,
			// 			main_ground: this.main_ground,
			// 			rescheck: this.rescheck
			// 		});
			// else
			// 	this.navCtrl.push('RecurrentPage', { vat: this.vat, discount: this.discount, minimum_fee: this.minimum_fee, pay_ramount: this.pay_amount, g_name: this.ground_name = this.ground_name, cid: id, main_ground: this.main_ground, rescheck: this.rescheck })
		}
	}

	field_reserve() {
		let data = {
			ground_id: this.id,
			court_id: this.court_id,
			reservation_id: this.res_id,
			"request_type": "cash",
		}
		let url = this.global.basepath + "ground/reserveFieldPay/?format=json";
		this.global.postRequestdata(url, data)
			.subscribe(res => {
				this.pay_data_2cash = res.json();
				let toastController = this.toast.create({
					message: this.global.lang == 'en' ? "Reservation request send successful" : "Pedido de reserva enviado exitoso",
					duration: 1000,
				});
				toastController.present();
				this.global.loader.dismiss();
				localStorage.removeItem('start_time');
				localStorage.setItem('start_date', localStorage.getItem('current_date'));
				if (this.navParams.get('rescheck') != '') {
					this.navCtrl.pop()
				}
				else {
					this.navCtrl.setRoot('FieldprofilePage');
				}
			},
				(error) => {
					this.global.loader.dismiss();
					let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })
					toastController.present();
					localStorage.removeItem('start_time');
					localStorage.setItem('start_date', localStorage.getItem('current_date'));
					if (this.navParams.get('rescheck') != '')
						this.navCtrl.setRoot('CanchaResPage')
					else
						this.navCtrl.setRoot('FieldprofilePage');
				});
	}

	pushDate(obj: Date) {
		this.days = [];
		let month_days = this.month_days[obj.getMonth()]
		while (obj.getDate() <= (month_days - 1)) {
			if (obj.getDate() > 9) {
				this.days.push({ day: this.week_days[obj.getDay()], date: obj.getDate() });
			}
			else {
				this.days.push({ day: this.week_days[obj.getDay()], date: '0' + obj.getDate() });
			}
			obj.setDate((obj.getDate()) + 1);
		}
		this.days.push({ day: this.week_days[obj.getDay()], date: obj.getDate() });
	}

	calendarFormat() {
		let obj = new Date(this.date);
		if (new Date().getMonth() == obj.getMonth() && new Date().getFullYear() == obj.getFullYear()) {
			this.selectd_date = this.current_date;
			this.fulldate = this.date + "-" + this.selectd_date;
			this.pushDate(new Date(this.fulldate));
			this.date_change()
		}
		else {
			this.selectd_date = '01';
			this.fulldate = this.date + "-" + this.selectd_date;
			this.pushDate(obj);
			this.date_change()
		}
	}

	date_change(date?, day?) {
		if (date != undefined) {
			this.selectd_date = date;
			this.fulldate = this.date + "-" + this.selectd_date;
			console.log("change date=>>>>>>>>>.", this.fulldate)
		}
		this.button_click = false;
		this.current_day = day;
		if (this.court_cid) {
			this.fieldrecord = {
				id: this.data.ground_id,
				court: this.court_cid,
				start_date: this.fulldate,
			}
		}
		else {
			this.fieldrecord = {
				id: this.data.ground_id,
				court: this.court_id,
				start_date: this.fulldate,
			}
		}
		// this.fieldrecord = {
		//   id: this.id,
		//   start_date: this.fulldate,
		// }
		let url = this.global.basepath + "ground/reserveschedule/?format=json";
		this.global.postRequestdata(url, this.fieldrecord)
			.subscribe(res => {
				this.data_display = res.json();
				this.main_ground = this.data.main_ground;
				this.discount = this.data.discount;
				this.minimum_fee = this.data.minimum_fee;
				this.pay_method = JSON.stringify(this.data.payment_methods);
				localStorage.setItem('pay_method', this.pay_method);
				this.court_id = this.data.ground_data.id;
				// this.ground_name = this.data.ground_data.ground_name;
			},
				(error) => {
				});
		// localStorage.setItem('start_date', this.fulldate);
	}

	gettime(tt, amount, reserve, queue_count) {
		this.from_time = tt.slice(0, 5)
		this.st = parseInt(tt);
		this.et = this.st + 1;
		if (this.et == 24) {
			this.to_time = "00:00";
		}
		if (this.et <= 9) {
			this.to_time = "0" + this.et + ":00";
		}
		if (this.et < 23 && this.et >= 10) {
			this.to_time = this.et + ":00";
		}
		this.button_click = true;
		if (reserve && queue_count < 2) {
			let confirm = this.alertCtrl.create({
				title: 'Are you Sure?',
				message: 'You want to add yourself in the queue',
				buttons: [
					{ text: 'No', handler: () => { } },
					{
						text: 'Yes',
						cssClass: 'button_ok',
						handler: () => {
							let id;
							if (this.court_cid)
								id = this.court_cid;
							else
								id = this.court_id;
							this.spinner = true
							let data = {
								"ground": id,
								"date": this.fulldate,
								"from_time": this.from_time,
								"to_time": this.to_time,
								"message": ""
							}
							let url = this.global.basepath + "scorecard/addQueue/";
							this.global.postRequestdata(url, data)
								.subscribe(res => {
									this.spinner = false
									let toastController = this.toast.create({
										message: this.global.lang == 'en' ? "Request sent successfully" : 'Solicitud enviada con éxito',
										duration: 3000,
									})
									toastController.present();
									this.navCtrl.pop();
								}, error => {
									this.spinner = false
									let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })
									toastController.present();
									this.navCtrl.pop();
								})
						}
					}
				]
			});
			confirm.present();
		}
		else if (reserve && queue_count >= 2) {
			let toastController = this.toast.create({
				message: this.global.lang == 'en' ? "Reservation already full" : "Reserva ya completa",
				duration: 3000,
			})
			toastController.present();
		}
		else {
			this.pay_amount = amount;
			localStorage.setItem('start_time', tt);
		}
	}


	// getgroundtime(x) {
	//   console.log("ground_time", x);

	//   if (this.button_click) {
	//     this.button_click = false;
	//   }
	//   else {
	//     this.button_click = true

	//   }
	// }

	pop() {
		this.navCtrl.pop();
	}


	ionViewWillLeave() {
		this.spinner = false
	}

	back() {
		this.navCtrl.pop();
	}

}