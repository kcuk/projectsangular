import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CanchaResPage } from './cancha-res';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    CanchaResPage,
  ],
  imports: [
    IonicPageModule.forChild(CanchaResPage),
    TranslateModule.forChild()

  ],
  exports: [
    CanchaResPage
  ]
})
export class CanchaResPageModule {}
