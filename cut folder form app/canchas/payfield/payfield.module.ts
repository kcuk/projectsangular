import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PayfieldPage } from './payfield';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    PayfieldPage,
  ],
  imports: [
    IonicPageModule.forChild(PayfieldPage),
    TranslateModule.forChild()
    
  ],
  exports: [
    PayfieldPage
  ]
})
export class PayfieldPageModule {}
