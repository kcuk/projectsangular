import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalService } from './../../../app/global-service';
import { Http } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-payfield',
  templateUrl: 'payfield.html',
})
export class PayfieldPage {
  data: any;
  amount: any;
  form: FormGroup;
  g_id: any;
  spinner: boolean = true;
  constructor(private toast: ToastController, public viewCtrl: ViewController,
    public navCtrl: NavController, public navParams: NavParams,
    private fb: FormBuilder, private global: GlobalService, public alertCtrl: AlertController) {
    this.form = this.fb.group({
      "amount": ["", Validators.compose([Validators.required])],
      "id": ["", Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]+[a-zA-Z]+[-]+[0-9]+')])]
    })
    this.g_id = localStorage.getItem('g_id');
  }

  fetch_amount(event) {
    this.form.patchValue({ 'amount': '' });
    if (event.target.value.length == 7 && this.form.controls.id.valid) {
              this.spinner = true
      let url = this.global.basepath + "ground/reserveElse/?reservation_id=" + event.target.value + "&ground_id=" + this.g_id;
      this.global.getRequest(url)
        .subscribe(res => {
                  this.spinner = false;
          this.data = res.json();
          this.amount = res.json().pending_amount;
          this.form.patchValue({ 'amount': this.amount });
        }, err => {
                  this.spinner = false;
          let toast = this.toast.create({
            message: this.global.lang == 'en' ? 'Wrong ID' : "ID incorecto",
            duration: 2000
          })
          toast.present();
        })
    }
    else {
              this.spinner = false;
    }
  }

  makePayment() {
    this.navCtrl.push('GroundPayCartPage', {
      pay_by_other: 'true',
      reservation_id: this.form.value.id,
      amount: this.form.value.amount,
      vat: this.data.vat,
    })
  }

}
