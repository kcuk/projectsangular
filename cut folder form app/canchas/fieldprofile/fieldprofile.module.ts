import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FieldprofilePage } from './fieldprofile';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    FieldprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(FieldprofilePage),
    TranslateModule.forChild()

  ],
  exports: [
    FieldprofilePage
  ]
})
export class FieldprofilePageModule {}
