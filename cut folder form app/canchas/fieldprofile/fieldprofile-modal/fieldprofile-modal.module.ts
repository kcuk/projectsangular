import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FieldprofileModalPage } from './fieldprofile-modal';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FieldprofileModalPage,
  ],
  imports: [
    IonicPageModule.forChild(FieldprofileModalPage),
    TranslateModule.forChild()

  ],
  exports: [
    FieldprofileModalPage
  ]
})
export class FieldprofileModalPageModule { }
