import { SocialSharing } from '@ionic-native/social-sharing';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { Component, ViewChild } from '@angular/core';
import * as html2canvas from 'html2canvas';
import { GlobalService } from "../../../../app/global-service";

@IonicPage()
@Component({
  selector: 'page-fieldprofile-modal',
  templateUrl: 'fieldprofile-modal.html',
})
export class FieldprofileModalPage {
  id: any;
  fields: any;
  @ViewChild('layout') canvasRef;
  public imageUrl: any;
  path: any = this.global.imgpath;
  spinner: boolean = true;


  constructor(private toast: ToastController, public navCtrl: NavController, public navParams: NavParams, private global: GlobalService, public viewCtrl: ViewController, private socialSharing: SocialSharing) {
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad FieldprofileModalPage');
    this.details()
  }

  details() {
            this.spinner = true
    this.id = localStorage.getItem('g_id')
    console.log("fieldprofile", this.id);
    let url = this.global.basepath + "ground/soccerGroundProfile/?id=" + this.id + "&format=json";
    this.global.getRequest(url)
      .subscribe(res => {
        this.fields = res.json();
        setTimeout(() => { this.drawImage(); }, 500)
      }),
      (error) => {
        let toastController = this.toast.create({ message: "Error, Try again later", duration: 2000, })
        toastController.present();
        this.navCtrl.pop();
                this.spinner = false
        console.log("this is an error", error);
      };
  }

  drawImage() {
    let canvas = this.canvasRef.nativeElement;
    html2canvas(canvas, {
      onrendered: (canvas) => {
        this.imageUrl = canvas.toDataURL("image/png");
        setTimeout(res => {
                  this.spinner = false
        }, 2000)
        console.log("converted image is ", this.imageUrl);
      }
    })

  }
  shareviawhatsapp() {
            this.spinner = true
    this.socialSharing.shareViaWhatsApp("Pelotea app", this.imageUrl, "https://pelotea.com/ground-profile/" + this.id)
      .then(res => {         this.spinner = false ;console.log("respose of share is ", res); })
      .catch(err => {         this.spinner = false ;console.log(err, "some error"); });
  }
  shareviafb() {
            this.spinner = true
    this.socialSharing.shareViaFacebook("Pelotea app", this.imageUrl, "https://pelotea.com/ground-profile/" + this.id)
      .then(res => {         this.spinner = false ;console.log("respose of share is ", res); })
      .catch(err => {         this.spinner = false ;console.log(err, "some error"); });
  }
  cancel() {
    this.viewCtrl.dismiss();
  }
  ionViewWillLeave() {
            this.spinner = false
  }
}
