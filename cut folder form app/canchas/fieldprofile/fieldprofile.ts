import { PhotoViewer } from "@ionic-native/photo-viewer";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  AlertController,
  ToastController
} from "ionic-angular";
import { GlobalService } from "../../../app/global-service";
import { FileOpener } from "@ionic-native/file-opener";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { CallNumber } from "@ionic-native/call-number";

@IonicPage()
@Component({
  selector: "page-fieldprofile",
  templateUrl: "fieldprofile.html"
})
export class FieldprofilePage {
  pay_cash: boolean;
  online_card: boolean = false;
  pay_method: any;
  admin: any;
  fileurl: any;
  call_number: any;
  downloadurl: any;
  download: boolean;
  internet: boolean = false;
  fc: any;
  gc: any;
  unfollow: any;
  ground_id: any;
  follow: any;
  photo: any;
  sponser: any[];
  fields: any;
  id: any;
  path: any = this.global.imgpath;
  icon: boolean = false;
  spinner: boolean = true;
  fileTransfer: FileTransferObject = this.transfer.create();
  constructor(
    private toast: ToastController,
    private callNumber: CallNumber,
    public alertCtrl: AlertController,
    private transfer: FileTransfer,
    private file: File,
    public navCtrl: NavController,
    public navParams: NavParams,
    private global: GlobalService,
    public modalCtrl: ModalController,
    private photoViewer: PhotoViewer
  ) { }

  // ionViewWillLoad() {
  //   console.log("ionViewWillEnter fielddddddddddddddddd");
  //   let payment_status = this.navParams.get("status");
  //   if (payment_status == "true") {
  //     this.pay_success();
  //   } else if (payment_status == "false") {
  //     this.pay_failed();
  //   }
  // }
  // pay_failed() {
  //   let alert = this.alertCtrl.create({
  //     title: this.global.lang == "en" ? "Payment failed!" : "Pago fallido"
  //   });
  //   alert.present();
  //   setTimeout(() => {
  //     alert.dismiss();
  //   }, 2000);
  // }

  // pay_success() {
  //   let alert = this.alertCtrl.create({
  //     title: this.global.lang == "en" ? "Payment successful!" : "Pago exitoso!"
  //   });
  //   alert.present();
  //   setTimeout(() => {
  //     alert.dismiss();
  //   }, 2000);
  // }

  ionViewWillLoad() {
    this.internet = false;

    this.fielddata();
  }
 
  showimage(imageurl, imagename) {
    this.photoViewer.show(imageurl, imagename);
  }
  fielddata($event?) {
    if (!$event) {
              this.spinner = true;
    }
    if (this.navParams.get("id")) {
      this.id = this.navParams.get("id");
    } else {
      this.id = localStorage.getItem("g_id");
    }
    console.log("fieldprofile", this.id);
    let url =
      this.global.basepath +
      "ground/grounddetail/?id=" +
      this.id +
      "&format=json";
    this.global.getRequest(url).subscribe(
      res => {
                this.spinner = false
        this.fields = res.json();
        // console.log(this.fields, "id");
        this.sponser = this.fields.Sponsers;
        console.log("sponser is ", this.sponser);
        this.photo = this.fields.Photo_Gallery;
        this.call_number = res.json().Ground_Contact;
        this.call_number = res.json().country_code + this.call_number;
        console.log("contact", this.call_number);
        this.follow = this.fields.follow;
        this.admin = this.fields.admin;
        this.pay_method = JSON.stringify(this.fields.payment_methods);
        localStorage.setItem("pay_method", this.pay_method);
        let payment_check_online: any = JSON.parse(
          localStorage.getItem("pay_method")
        );
        payment_check_online.forEach(element => {
          console.log("element", element);
          if (
            element.abbreviation == "online" ||
            element.abbreviation == "card"
          ) {
            this.online_card = true;
          }
          // else if (element.abbreviation == "cash") {
          //   this.pay_cash = true;
          // }
        });
        this.fc = this.fields.Ground_Followers;
        if (this.fields.Ground_Rules == null) {
          this.download = false;
        } else if (this.fields.Ground_Rules != null) {
          this.download = true;
          this.downloadurl =
            this.global.downloadpath + this.fields.Ground_Rules;
          this.fileurl = this.fields.Ground_Rules.slice(7);
        }
      },
      error => {
                this.spinner = false
        this.internet = true;
        console.log("this is an error", error);
      }
    );
  }

  back() {
    this.navCtrl.pop();
  }

  mk() {
    if (localStorage.getItem("user")) {
      // if (!this.fields.is_peloteando_certified&&this.admin) {
      //   let toastController = this.toast.create({ message: "Reservation Not allowed", duration: 2000, })
      //   toastController.present();
      // }
      // else {
      //   this.navCtrl.push('CanchaResPage');
      // }
      if (this.fields.is_peloteando_certified) {
        if (!this.admin) {
          this.navCtrl.push("CanchaResPage", {
            check_recurrent: this.online_card
          });
        } else {
          let toastController = this.toast.create({
            message: "Reservation Not allowed",
            duration: 2000
          });
          toastController.present();
        }
      } else {
        let toastController = this.toast.create({
          message: "Reservation Not allowed",
          duration: 2000
        });
        toastController.present();
      }
    } else {
      let modal = this.modalCtrl.create("LoginPage", {
        moadal_name: "field_profile"
      });
      modal.present();
    }
  }
  share() {
    let modal = this.modalCtrl.create("FieldprofileModalPage");
    modal.onDidDismiss(data => { });
    modal.present();
  }
  message() {
    if (localStorage.getItem("user")) {
      if (this.fields.admin) {
        let toastController = this.toast.create({
          message: "Not allowed",
          duration: 2000
        });
        toastController.present();
      } else {
        this.navCtrl.push("ComposemessagePage", {
          id: this.id,
          name: this.fields.Ground_Name,
          logo: this.fields.Profile_Pic,
          ground: "ground"
        });
      }
    } else {
      let modal = this.modalCtrl.create("LoginPage", {
        moadal_name: "field_profile"
      });
      modal.present();
    }
  }

  pay() {
    if (localStorage.getItem("user")) {
      console.log("certifieddddddddddddd", this.online_card);

      if (this.fields.is_peloteando_certified && this.online_card) {
        if (!this.admin) {
          this.navCtrl.push("PayfieldPage");
        } else {
          let toastController = this.toast.create({
            message: "Reservation Not allowed",
            duration: 2000
          });
          toastController.present();
        }
      } else {
        let toastController = this.toast.create({
          message: "Reservation Not allowed",
          duration: 2000
        });
        toastController.present();
      }
    } else {
      let modal = this.modalCtrl.create("LoginPage", {
        moadal_name: "field_profile"
      });
      modal.present();
    }
  }
  folow() {
    let data = {
      ground_id: this.id
    };
    if (localStorage.getItem("user")) {
              this.spinner = true

      let url = this.global.basepath + "ground/followground/?format=json";
      this.global.postRequestdata(url, data).subscribe(res => {
        this.follow = res.json();
        console.log("follow is", this.follow);
        this.follow = true;
        this.fields.Ground_Followers++;
                this.spinner = false
      }),
        err => {
                  this.spinner = false
        };
    } else {
      let modal = this.modalCtrl.create("LoginPage", {
        moadal_name: "field_profile"
      });
      modal.present();
    }
  }

  unfolow() {
    if (localStorage.getItem("user")) {

      if (this.admin) {
        let toastController = this.toast.create({
          message: "Not Allowed",
          duration: 2000
        });
        toastController.present();
      } else {
                this.spinner = true

        let url =
          this.global.basepath +
          "ground/unfollowground/" +
          this.id +
          "/?format=json";
        this.global.getdelete(url).subscribe(res => {
          this.unfollow = res.json();
          //  console.log("unfollow is",this.unfollow);
          this.follow = false;
          this.fields.Ground_Followers--;
                  this.spinner = false
        }),
          err => {
                    this.spinner = false
          };
      }
    } else {
      let modal = this.modalCtrl.create("LoginPage", {
        id: this.id
      });
      modal.onDidDismiss(data => {
        console.log("ok");
      });
      modal.present();
    }
  }

  // files() {
  //   // var ref = window.open(this.downloadurl, '_blank', 'location=no');

  //   // cordova.plugins.fileOpener2.open(this.downloadurl, 'application/pdf',{
  //   //     error :function(e) {
  //   //         console.log('Error status: ' + e.status + ' - Error message: ' + e.message);
  //   //     },
  //   //     success : function () {
  //   //         console.log('file opened successfully');
  //   //     }
  //   // })

  //   console.log("download", this.downloadurl);
  //   this.fileTransfer.download(this.downloadurl, this.createDir() + this.fileurl).then((entry) => {
  //     console.log('download complete: ' + entry.toURL());
  //     let alert = this.alertCtrl.create({
  //       title: 'Downloaded',
  //     })
  //     alert.present();
  //     setTimeout(() => {
  //       alert.dismiss();
  //     }, 2000);
  //   }, (error) => {
  //     console.log("error downloading", error);
  //     let alert = this.alertCtrl.create({
  //       title: "File Format Incorrect"
  //     })
  //     alert.present();
  //     setTimeout(() => {
  //       alert.dismiss();
  //     }, 2000);
  //   });
  // }

  files() {
            this.spinner = true;
    this.file
      .createDir(this.file.documentsDirectory, "pelotea", true)
      .then(res => {
        console.log("responseeeeeeeeeeeeeee", res);
        this.fileTransfer
          .download(
            this.downloadurl,
            this.file.documentsDirectory + "pelotea" + this.fileurl
          )
          .then(
            entry => {
                      this.spinner = false
              console.log(
                "download complete: " + entry.toURL(),
                "entryyyyyyyyyyyyyy",
                entry
              );
              let alert = this.alertCtrl.create({
                title: this.global.lang == "en" ? "Downloaded" : "Descargado"
              });
              alert.present();
              setTimeout(() => {
                alert.dismiss();
              }, 2000);
            },
            error => {
                      this.spinner = false
              console.log("error downloading", error);
              let alert = this.alertCtrl.create({
                title:
                  this.global.lang == "en"
                    ? "File format incorrect"
                    : "Formato de archivo incorrecto"
              });
              alert.present();
              setTimeout(() => {
                alert.dismiss();
              }, 2000);
            }
          );
      })
      .catch(err => {
                this.spinner = false
        console.log(
          "someerror in creating directory",
          err,
          "file path",
          this.file.documentsDirectory
        );
        return err;
      });
  }

  ground_call() {
    if (localStorage.getItem("user")) {
      if (this.call_number) {
        this.callNumber
          .callNumber(this.call_number, true)
          .then(() => console.log("Launched dialer!"))
          .catch(() => console.log("Error launching dialer"));
        let toastController = this.toast.create({
          message:
            this.global.lang == "en"
              ? "Redirecting to dialer.."
              : "Redirigir a marcador ...",
          duration: 1000,
          position: "center"
        });
        toastController.present();
      } else {
        let toastController = this.toast.create({
          message: "Invalid Number",
          duration: 1000,
          position: "center"
        });
        toastController.present();
      }
    } else {
      let modal = this.modalCtrl.create("LoginPage", {
        id: this.id
      });
      modal.onDidDismiss(data => {
        console.log("ok");
      });
      modal.present();
    }
  }
  followerslist() {
    if (localStorage.getItem("user")) {
      this.navCtrl.push("GroundfollowerPage");
    } else {
      let modal = this.modalCtrl.create("LoginPage", {
        moadal_name: "field_profile"
      });
      modal.present();
    }
  }

  ionViewWillLeave() {
            this.spinner = false
  }
}
