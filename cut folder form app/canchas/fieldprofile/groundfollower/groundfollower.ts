import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalService } from '../../../../app/global-service';

@IonicPage()
@Component({
  selector: 'page-groundfollower',
  templateUrl: 'groundfollower.html',
})
export class GroundfollowerPage {
  lang: string;
  id: string;
  result: any[] = [];
  total_page: number;
  cover_pic: any;
  page: number = 1;
  datacheck: boolean = false;
  internet: boolean = false;
  spinner: boolean = true;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private global: GlobalService) {
    this.lang = localStorage.getItem("devicelang")
  }

  ionViewWillEnter() {
    this.cover_pic=this.global.imgpath;
    this.id = localStorage.getItem('g_id');
    console.log('ionViewDidLoad GroundfollowerPage ');
    this.getPlayersList(this.page);
  }
  getPlayersList(page, $event?) {
    this.internet = false;
    if (!$event) {         this.spinner = true }
    let url = this.global.basepath + "ground/groundProfile/?form_type=follower_list&ground=" + this.id + "&page=" + page + "&format=json";
    this.global.getRequest(url)
      .subscribe(res => {
                this.spinner = false
        this.total_page = res.json().total_pages;
        let arr = res.json().player_list;
        if (!arr.length)
        this.datacheck = true;
        else
        this.datacheck = false;
        if (page == 1) {
          this.result = res.json().player_list;
        }
        else {
          this.result = this.result.concat(arr);
        }
        if ($event) {$event.complete();}
        console.log("data",arr,res.json(),"RESULT",this.result);
        this.cover_pic = this.global.imgpath;
      }, err => {
                this.spinner = false
        this.internet = true;
        this.result = []
        if ($event) {$event.complete();}
      })
  }
  doInfinite($event) {
    this.page++;
    if (this.total_page >= this.page)
      this.getPlayersList(this.page, $event)
    else
      $event.complete();
  }
  followprofile(id){
    console.log("pp",id);
    this.navCtrl.push("PlayerdetailPage",{id:id});
  }
  ionViewWillLeave() {
            this.spinner = false
    setTimeout(() => {
      this.result = [];
    }
      , 500)
  }

}
