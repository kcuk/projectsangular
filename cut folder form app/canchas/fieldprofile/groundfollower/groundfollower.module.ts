import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroundfollowerPage } from './groundfollower';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    GroundfollowerPage,
  ],
  imports: [
    IonicPageModule.forChild(GroundfollowerPage),
    TranslateModule.forChild()

  ],
  exports: [
    GroundfollowerPage
  ]
})
export class GroundfollowerPageModule { }
