import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CanchasPage } from './canchas';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    CanchasPage,
  ],
  imports: [
    IonicPageModule.forChild(CanchasPage),
    TranslateModule.forChild()

  ],
  exports: [
    CanchasPage
  ]
})
export class CanchasPageModule {}
