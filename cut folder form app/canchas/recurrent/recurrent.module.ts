import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecurrentPage } from './recurrent';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    RecurrentPage,
  ],
  imports: [
    IonicPageModule.forChild(RecurrentPage),
    TranslateModule.forChild()
    
  ],
  exports: [
    RecurrentPage
  ]
})
export class RecurrentPageModule {}
