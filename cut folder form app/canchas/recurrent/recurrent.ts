import { GlobalService } from './../../../app/global-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-recurrent',
  templateUrl: 'recurrent.html',
})
export class RecurrentPage {
  pay_data_2cash: any;
  res_id: any;
  pay_data: any;
  spinner: boolean;
  min_date: string;
  new_obj: any;
  weekeveryday: any = "everyday";
  recurrent_data: any;
  selectd_date: any;
  ioc: boolean = true;
  samay: any[] = [];
  b: string;
  week: string[];
  button: boolean = false;
  data = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
  date: any;
  days: any;
  year: any;
  month: any;
  week_days: any ;
  month_days: any = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  current_date: any;
  current_day: any;
  day_array: any[] = [];
  fulldate: any;
  check_min_date: any;
  lang: any = localStorage.getItem('devicelang');
	months: any =(this.lang == 'es'? "[ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic]":"[Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec]");  constructor(private toast: ToastController, private global: GlobalService, public navCtrl: NavController, public navParams: NavParams) {
    if (this.lang == 'es') {
      this.week_days = ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb']
    }
    else {
      this.week_days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    }
    let getDate = navParams.get('reservation_date')
    console.log("getdate=>>>>>>>>>>>>>", getDate)
    let recurrent_obj = new Date(getDate);
    console.log("date= recurrent_obj>>>>>>>>>>>>>", recurrent_obj)
    this.year = recurrent_obj.getFullYear();
    this.month = recurrent_obj.getMonth();
    this.current_date = recurrent_obj.getDate();
    if (this.current_date < 10) {
      this.current_date = '0' + this.current_date
    }
    if (this.month > 8) {
      this.month++;
    }
    else {
      this.month = '0' + (this.month + 1);
    }
    this.date = this.year + "-" + this.month;
    this.fulldate = this.year + "-" + this.month + "-" + this.current_date;
    this.min_date = this.year + "-" + this.month + "-" + this.current_date;
    this.pushDate(new Date(this.fulldate));
    console.log("fulldate=>>>>>>>>>>>>>>>>", this.fulldate)
    this.selectd_date = this.current_date
    localStorage.setItem('r_start_date', this.fulldate);

    // let obj = new Date();
    // this.year = obj.getFullYear();
    // this.month = obj.getMonth();
    // this.current_day = this.week_days[obj.getDay()];
    // this.current_date = '01';
    // // if (this.current_date < 10) {
    // //   this.current_date = '0' + this.current_date
    // //   console.log(this.current_date, "c_dateeeeeeeeee")
    // // }
    // if (this.month > 8) {
    //   this.month++;
    //   // this.date = this.year + "-" + this.month;
    // }

    // else {
    //   this.month = '0' + (this.month + 1);
    //   // this.date = this.year + "-" + this.month;
    // }
    // // this.fulldate = this.year + "-" + this.month + "-" + this.current_date;
    // this.min_date = this.year + "-" + this.month + "-" + this.current_date;
    // let current_date_obj = new Date(this.fulldate);
    // if (localStorage.getItem('start_date')) {
    //   this.new_obj = new Date(localStorage.getItem('start_date'))
    //   this.selectd_date = this.new_obj.getDate();
    //   this.check_min_date = this.new_obj;
    //   console.log("selectd_date=>>>>>>>>>>>>>.", this.selectd_date, current_date_obj);
    //   this.pushDate(current_date_obj);
    //   localStorage.setItem('r_start_date', localStorage.getItem('start_date'));
    // }

    // else {
    //   localStorage.setItem('r_start_date', this.fulldate);
    //   this.pushDate(current_date_obj)
    // }
  }



  pay() {

    // this.recurrent_data = {
    //   r_type: true,
    //   weekeveryday:
    // }
    // localStorage.setItem('recurrent_data', JSON.stringify(this.recurrent_data))
    // this.navCtrl.push('PaymentrevPage', { vat: this.navParams.get('vat'),
    //  discount: this.navParams.get('discount'),
    //   minimum_fee: this.navParams.get('minimum_fee'), pay_ramount: this.navParams.get('pay_ramount'), 
    //   g_name: this.navParams.get('g_name'), cid: this.navParams.get('cid'), main_ground: this.navParams.get('main_ground'),
    //    rescheck: this.navParams.get('rescheck') });
    // }
    let data = {
      court_name: this.navParams.get('g_name'),
      date: this.navParams.get('reservation_date'),
      email: null,
      form_type: "reserve",
      from_time: localStorage.getItem('start_time'),
      ground: this.navParams.get('cid'),
      guest_name: null,
      is_existed: "false",
      is_guest: true,
      is_recurrent_reservations: true,
      main_ground: this.navParams.get('main_ground'),
      message: "",
      paid_with: "cash",
      phone: "",
      price: this.navParams.get('pay_ramount'),
      recurrent_reservations_type: this.weekeveryday,
      reserved_through: null,
      till_date: this.fulldate,
      // to_time: this.to_time,
      user_id: 0
    }

    this.global.presentLoading();
    let url = this.global.basepath + "scorecard/ground_request/?format=json";
    this.global.postRequestdata(url, data)
      .subscribe(res => {
        this.pay_data = res.json();
        console.log("pay_data", this.pay_data);
        this.res_id = this.pay_data.reservation_id;
        this.field_reserve();
      },
        (error) => {
          this.global.loader.dismiss();
          let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })
          toastController.present();
          this.navCtrl.pop();
        });

  }

  field_reserve() {
    let data = {
      ground_id: parseInt(localStorage.getItem('g_id')),
      court_id: this.navParams.get('cid'),
      reservation_id: this.res_id,
      "request_type": "cash",
    }
    let url = this.global.basepath + "ground/reserveFieldPay/?format=json";
    this.global.postRequestdata(url, data)
      .subscribe(res => {
        this.global.loader.dismiss();
        this.pay_data_2cash = res.json();
        let toastController = this.toast.create({
          message: this.global.lang == 'en' ? "Reservation request send successful" : "Pedido de reserva enviado exitoso",
          duration: 1000,
        });
        toastController.present();
        localStorage.removeItem('start_time');
        localStorage.setItem('start_date', localStorage.getItem('current_date'));
        if (this.navParams.get('rescheck') != '') {
          this.navCtrl.pop()
        }
        else {
          this.navCtrl.setRoot('FieldprofilePage');
        }
      },
        (error) => {
          this.global.loader.dismiss();
          let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde", duration: 2000, })
          toastController.present();
          localStorage.removeItem('start_time');
          localStorage.setItem('start_date', localStorage.getItem('current_date'));
          if (this.navParams.get('rescheck') != '')
            this.navCtrl.setRoot('CanchaResPage')
          else
            this.navCtrl.setRoot('FieldprofilePage');
        });
  }


  day(i) {
    this.b = i;
    console.log(this.b);
    if (this.button) {
      this.button = false;
    }
    else {
      this.button = true;

    }
  }
  icon() {
    this.ioc = true;
    this.weekeveryday = "everyday";
    console.log(this.ioc, this.weekeveryday)
  }
  icon1() {
    this.ioc = false;
    this.weekeveryday = "once_a_day";
    console.log(this.ioc, this.weekeveryday);
  }

  pushDate(obj: Date) {
    this.days = [];
    let month_days = this.month_days[obj.getMonth()]
    while (obj.getDate() <= (month_days - 1)) {
      if (obj.getDate() > 9) {
        this.days.push({ day: this.week_days[obj.getDay()], date: obj.getDate() });
      }
      else {
        this.days.push({ day: this.week_days[obj.getDay()], date: '0' + obj.getDate() });
      }
      obj.setDate((obj.getDate()) + 1);
    }
    this.days.push({ day: this.week_days[obj.getDay()], date: obj.getDate() });
    console.log("dates=>>>>>>>>>>>", this.days)
  }

  calendarFormat() {
    console.log("caledereeeeeeeeeeeeeeeeeeeeeeeeeeeeee")
    let obj = new Date(this.date);
    if (this.month == (obj.getMonth() + 1) && this.year == obj.getFullYear()) {
      this.selectd_date = this.current_date;
      this.fulldate = this.date + "-" + this.selectd_date;
      localStorage.setItem('r_start_date', this.fulldate);
      this.pushDate(new Date(this.fulldate));
    }
    else {
      let month_days = this.month_days[obj.getMonth()]
      if (this.selectd_date <= month_days) {
      }
      else {
        this.selectd_date = month_days
      }
      this.fulldate = this.date + "-" + this.selectd_date;
      localStorage.setItem('r_start_date', this.fulldate);
      this.pushDate(new Date(this.date));

    }

    // let date = new Date(localStorage.getItem('r_start_date')).getDate();
    // let fulldate = this.date + "-" + date;
    // localStorage.setItem('r_start_date', fulldate);
    // this.current_date = '01';
    // this.fulldate = this.date + "-" + this.current_date;
    // let obj = new Date(this.date);
    // this.current_day = this.week_days[obj.getDay()]
    // localStorage.setItem('date', this.date);
    // this.pushDate(obj);
  }

  date_change(date, day) {
    this.selectd_date = date;
    this.fulldate = this.date + "-" + this.selectd_date;
    this.current_day = day;
    localStorage.setItem('r_start_date', this.fulldate);
  }

}