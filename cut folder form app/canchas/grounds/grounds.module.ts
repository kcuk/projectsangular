import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroundsPage } from './grounds';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    GroundsPage,
  ],
  imports: [
    IonicPageModule.forChild(GroundsPage),
    TranslateModule.forChild()

  ],
  exports: [
    GroundsPage
  ]
})
export class GroundsPageModule { }
