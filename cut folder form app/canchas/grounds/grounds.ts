import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  Platform
} from "ionic-angular";
import { GlobalService } from "../../../app/global-service";
import { Events } from "ionic-angular";
@IonicPage()
@Component({
  selector: "page-grounds",
  templateUrl: "grounds.html"
})
export class GroundsPage {
  lang: string;
  city_id: any;
  country_id: any;
  min_date: string;
  public unregisterBackButtonAction: any;
  new_date: Date;
  filldrop: boolean = false;
  alert: any;
  fav: boolean = false;
  type: any = { futsal: false, Cs: false, Cn: false };
  modality: any = {
    all: false,
    V5: false,
    V6: false,
    V7: false,
    V8: false,
    V9: false,
    V11: false
  };
  rating: any = { r1: false, r2: false, r3: false, r4: false, r5: false };
  testRadioOpen: boolean;
  check: boolean = false;
  testRadioResult: any;
  city: any;
  canchas: any;
  place: any;
  country: any[];
  val: any;
  items: string[];
  country_name: any;
  button_info: any;
  date: any;
  days: any;
  year: any;
  month: any;
  week_days: any = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  month_days: any = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  current_date: any;
  current_day: any;
  selectd_date: any;
  day_array: any[] = [];
  fulldate: any;
  spinner: boolean = true;
  constructor(
    public events: Events,
    public platform: Platform,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private global: GlobalService
  ) {
    this.country_id = localStorage.getItem("country_id");
    this.lang = localStorage.getItem("devicelang");
    if (this.global.lang == "en") {
      this.week_days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    } else {
      this.week_days = ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"];
    }
    // let obj = new Date();
    // this.year = obj.getFullYear();
    // this.month = obj.getMonth();
    // this.current_day = this.week_days[obj.getDay()];
    // this.current_date = obj.getDate();
    // if (this.current_date < 10) {
    //   this.current_date = '0' + this.current_date
    // }
    // if (this.month > 8) {
    //   this.month++;
    // }
    // else {
    //   this.month = '0' + (this.month + 1);
    // }
    // this.min_date = this.year + "-" + this.month + "-" + this.current_date;
    // let new_obj = new Date(localStorage.getItem('start_date'))
    // if (new Date().getMonth() == new_obj.getMonth() && new Date().getFullYear() == new_obj.getFullYear()) {
    //   console.log("month equalssssssssssssss", new Date().getMonth(), new_obj.getMonth())
    //   this.selectd_date = new_obj.getDate();
    //   this.date = this.year + "-" + this.month;
    //   this.fulldate = this.date + "-" + this.selectd_date;
    //   this.pushDate(new Date(this.date + "-" + this.current_date));
    // }
    // else {
    //   console.log("month not equalssssssssssssss", new Date().getMonth(), new_obj.getMonth())
    //   this.selectd_date = new_obj.getDate();
    //   this.year = new_obj.getFullYear();
    //   this.month = new_obj.getMonth();
    //   if (this.month > 8) {
    //     this.month++;
    //   }
    //   else {
    //     this.month = '0' + (this.month + 1);
    //   }
    //   this.date = this.year + "-" + this.month;
    //   console.log("dateeeeeeeeeeeeeeee", this.date)
    //   this.fulldate = this.date + "-" + this.selectd_date;
    //   this.pushDate(new Date(this.date))
    // }
  }

  ionViewDidEnter() {
    if (localStorage.getItem("custom_time")) {
      this.testRadioResult = localStorage.getItem("custom_time").slice(0, 5);
    }
    if (localStorage.getItem("city")) {
      this.place = localStorage.getItem("city");
    }
    if (localStorage.getItem("city_id")) {
      this.city_id = localStorage.getItem("city_id");
    }

    // if (localStorage.getItem('date')) {
    //   this.date = localStorage.getItem('date');
    // }
    if (localStorage.getItem("time_format")) {
      this.button_info = localStorage.getItem("time_format");
    }
    if (localStorage.getItem("custom")) {
      this.testRadioResult = localStorage.getItem("custom");
    }
    if (localStorage.getItem("g_type")) {
      this.type = JSON.parse(localStorage.getItem("g_type"));
    }
    if (localStorage.getItem("filldrop")) {
      this.filldrop = JSON.parse(localStorage.getItem("filldrop"));
    }
    if (localStorage.getItem("g_modality")) {
      this.modality = JSON.parse(localStorage.getItem("g_modality"));
    }
    if (localStorage.getItem("g_rating")) {
      this.rating = JSON.parse(localStorage.getItem("g_rating"));
    }
    if (localStorage.getItem("favourite")) {
      this.fav = JSON.parse(localStorage.getItem("favourite"));
    }
  }

  search(val) {
    if (val == " ") {
      val = null;
    }
    let url =
      this.global.basepath +
      "team/location/?country=" +
      this.country_id +
      "&city=" +
      val;
    this.global.getRequest(url).subscribe(res => {
      this.country = res.json();
    });
  }

  getItems(ev: any) {
    this.val = ev.target.value;
    if (this.val) {
      this.search(this.val);
    }
  }

  count(city, id) {
    this.place = city;
    this.city_id = id;
    this.country = [];
  }

  ground() {
    if (this.place != "") {
      localStorage.setItem("city", this.place);
    }
    localStorage.setItem("city_id", this.city_id);
    // localStorage.setItem('start_date', this.fulldate);
    setTimeout(res => {
      this.events.publish("user:created");
    }, 1000);
    localStorage.setItem("g_type", JSON.stringify(this.type));
    localStorage.setItem("g_rating", JSON.stringify(this.rating));
    localStorage.setItem("g_modality", JSON.stringify(this.modality));
    if (this.button_info != undefined) {
      localStorage.setItem("time_format", this.button_info);
    }
    localStorage.setItem("week_day", this.current_day);
    localStorage.setItem("favourite", JSON.stringify(this.fav));
    this.navCtrl.pop();
  }

  // pushDate(obj: Date) {
  //   this.days = [];
  //   let month_days = this.month_days[obj.getMonth()]
  //   while (obj.getDate() <= (month_days - 1)) {
  //     if (obj.getDate() > 9) {
  //       this.days.push({ day: this.week_days[obj.getDay()], date: obj.getDate() });
  //     }
  //     else {
  //       this.days.push({ day: this.week_days[obj.getDay()], date: '0' + obj.getDate() });
  //     }
  //     obj.setDate((obj.getDate()) + 1);
  //   }
  //   this.days.push({ day: this.week_days[obj.getDay()], date: obj.getDate() });
  // }

  // calendarFormat() {
  //   console.log("groundddddddddddddddddddddddddddd")
  //   let obj = new Date(this.date);
  //   if (new Date().getMonth() == obj.getMonth() && new Date().getFullYear() == obj.getFullYear()) {
  //     this.selectd_date = this.current_date;
  //     this.fulldate = this.date + "-" + this.selectd_date;
  //     this.pushDate(new Date(this.fulldate));
  //   }
  //   else {
  //     this.selectd_date = '01';
  //     this.fulldate = this.date + "-" + this.selectd_date;
  //     this.pushDate(obj);
  //   }
  // }

  // updateCucumber() {
  // }

  // date_change(date, day) {
  //   this.selectd_date = date;
  //   this.fulldate = this.date + "-" + this.selectd_date;
  //   if (day) {
  //     this.current_day = day;
  //   }
  //   else {
  //     this.current_day = null;
  //   }
  // }
  getMorn() {
    if (this.button_info) {
      this.testRadioResult = "";
      localStorage.removeItem("custom_time");
    }
  }
  test() {}

  showRadio(tt) {
    this.alert = this.alertCtrl.create();
    this.alert.setTitle(
      this.lang == "en"
        ? "Select reservation time"
        : "Seleccionar la hora de reserva"
    );

    this.alert.addInput({
      type: "radio",
      label: "00:00 hrs",
      value: "00:00",
      checked: this.check
    });

    this.alert.addInput({
      type: "radio",
      label: "01:00 hrs",
      value: "01:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "02:00 hrs",
      value: "02:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "03:00 hrs",
      value: "03:00",
      checked: this.check
    });

    this.alert.addInput({
      type: "radio",
      label: "04:00 hrs",
      value: "04:00",
      checked: this.check
    });

    this.alert.addInput({
      type: "radio",
      label: "05:00 hrs",
      value: "05:00",
      checked: this.check
    });

    this.alert.addInput({
      type: "radio",
      label: "06:00 hrs",
      value: "06:00",
      checked: this.check
    });

    this.alert.addInput({
      type: "radio",
      label: "07:00 hrs",
      value: "07:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "08:00 hrs",
      value: "08:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "09:00 hrs",
      value: "09:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "10:00 hrs",
      value: "10:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "11:00 hrs",
      value: "11:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "12:00 hrs",
      value: "12:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "13:00 hrs",
      value: "13:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "14:00 hrs",
      value: "14:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "15:00 hrs",
      value: "15:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "16:00 hrs",
      value: "16:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "17:00 hrs",
      value: "17:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "18:00 hrs",
      value: "18:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "19:00 hrs",
      value: "19:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "20:00 hrs",
      value: "20:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "21:00 hrs",
      value: "21:00",
      checked: this.check
    });
    this.alert.addInput({
      type: "radio",
      label: "22:00 hrs",
      value: "22:00",
      checked: this.check
    });

    this.alert.addInput({
      type: "radio",
      label: "23:00 hrs",
      value: "23:00",
      checked: this.check
    });

    this.alert.addButton({
      text: this.lang == "en" ? "Cancel" : "Cancelar",
      handler: data => {
        this.testRadioOpen = false;
      }
    });

    this.alert.addButton({
      text: this.lang == "en" ? "Done" : "Hecho",
      cssClass: "button_ok",
      handler: data => {
        this.testRadioOpen = false;
        this.testRadioResult = data;
        localStorage.setItem("custom_time", data + ":00");
      }
    });

    this.alert.present().then(() => {
      this.testRadioOpen = true;
    });
  }
  filter() {
    if (this.filldrop) {
      this.filldrop = false;
    } else {
      this.filldrop = true;
    }
    localStorage.setItem("filldrop", JSON.stringify(this.filldrop));
  }
  reset() {
    this.type = { futsal: false, Cs: false, Cn: false };
    this.modality = {
      all: false,
      v4: false,
      V5: false,
      V6: false,
      V7: false,
      V8: false,
      V9: false,
      V11: false
    };
    this.rating = { r1: false, r2: false, r3: false, r4: false };
    this.fav = false;
    this.button_info = undefined;
    this.testRadioResult = "";
    localStorage.removeItem("g_modality");
    localStorage.removeItem("g_type");
    localStorage.removeItem("g_rating");
    localStorage.removeItem("time_format");
    localStorage.removeItem("filldrop");
    localStorage.removeItem("custom_time");
    localStorage.removeItem("week_day");
  }
  // ionViewWillLeave() {
  //   this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  //           this.spinner = false
  // }
  // initializeBackButtonCustomHandler(): void {
  //   this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
  //       this.alert.dismiss()
  //   )
  // }
}
