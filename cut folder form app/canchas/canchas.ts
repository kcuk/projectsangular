import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { GlobalService } from "../../app/global-service";

@IonicPage()
@Component({
  selector: 'page-canchas',
  templateUrl: 'canchas.html',
})
export class CanchasPage {
  english: any;
  admin: any;
  searchQuery: string;
  no_data: boolean;
  type: any[] = [];
  modality: any[] = [];
  filter_rating: any[] = [];
  field_format: any = [];
  rating: any = [];
  internet: boolean = false;
  grass_type: any = [];
  time_custom: string;
  tabbar: any;
  search_value: any;
  val: any;
  slot: any;
  week_day: string;
  time_format: string;
  start_date: string;
  city: string;
  country: string;
  groundsdata: any;
  id: any;
  time: any;
  t: any;
  am: string = "am";
  pm: string = "pm";
  canchas: any[] = [];
  totalPages: any;
  page: number = 1;
  date: any;
  days: any;
  year: any;
  month: any;
  week_days: any = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  month_days: any = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  current_date: any;
  selectd_date: any;
  current_day: any;
  day_array: any[] = [];
  fulldate: any;
  items: string[];
  min_date: any;
  img: any = this.global.imgpath;
  check_month: boolean = true;
  spinner: boolean = true;
  lang: any = localStorage.getItem('devicelang');
	months: any =(this.lang == 'es'? "[ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic]":"[Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec]");
  constructor(public modalCtrl: ModalController, private toast: ToastController, public navCtrl: NavController, public navParams: NavParams, private global: GlobalService) {
    if (this.lang == 'es') {
      this.week_days = ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb']
    }
    else {
      this.week_days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    }
  }

  ionViewDidEnter() {
    // this.min_date = new Date()
    // FOR DEFAULT DATE
    let obj = new Date();
    this.year = obj.getFullYear();
    this.month = obj.getMonth();
    this.current_day = this.week_days[obj.getDay()];
    this.current_date = obj.getDate();
    if (this.current_date < 10) {
      this.current_date = '0' + this.current_date
    }
    if (this.month > 8) {
      this.month++;
      // this.date = this.year + "-" + this.month;
    }
    else {
      this.month = '0' + (this.month + 1);
      // this.date = this.year + "-" + this.month;
    }
    // this.fulldate = this.year + "-" + this.month + "-" + this.current_date;
    this.min_date = this.year + "-" + this.month + "-" + this.current_date;
    // let current_date_obj = new Date(this.fulldate);
    if (localStorage.getItem('start_date')) {
      let new_obj = new Date(localStorage.getItem('start_date'))
      if (new Date().getMonth() == new_obj.getMonth() && new Date().getFullYear() == new_obj.getFullYear()) {
        this.check_month = true;
        this.selectd_date = new_obj.getDate();
        this.date = this.year + "-" + this.month;
        this.fulldate = this.date + "-" + this.current_date;
        this.pushDate(new Date(this.fulldate));
      }
      else {
        this.selectd_date = new_obj.getDate();
        this.year = new_obj.getFullYear();
        this.month = new_obj.getMonth();
        if (this.month > 8) {
          this.month++;
        }
        else {
          this.month = '0' + (this.month + 1);
        }
        let date = this.year + "-" + this.month
        if (this.date == date) {
          this.check_month = true;
        }
        else {
          this.check_month = false;
        }
        this.date = date;
        console.log("helllllllllo", this.date)

        this.pushDate(new Date(this.date))
      }
      // let new_obj = new Date(localStorage.getItem('start_date'))
      // this.selectd_date = new_obj.getDate();
      // this.pushDate(current_date_obj)
    }
    else {
      this.fulldate = this.year + "-" + this.month + "-" + this.current_date;
      this.date = this.year + "-" + this.month
      localStorage.setItem('start_date', this.fulldate);
      this.selectd_date = this.current_date;
      this.pushDate(new Date(this.fulldate))
    }

    this.searchQuery = "";

    if (localStorage.getItem('g_type')) {
      this.type = JSON.parse(localStorage.getItem('g_type'));
    }
    if (localStorage.getItem('g_rating')) {
      this.filter_rating = JSON.parse(localStorage.getItem('g_rating'));
    }
    if (localStorage.getItem('g_modality')) {
      this.modality = JSON.parse(localStorage.getItem('g_modality'));
    }

    Object.keys(this.filter_rating).filter((key) => {
      if (this.filter_rating[key] == true) {
        this.rating.push(key);
      }

    });

    Object.keys(this.modality).filter((key) => {
      if (this.modality[key] == true) {
        this.field_format.push(key);
      }
    });

    Object.keys(this.type).filter((key) => {
      if (this.type[key] == true) {
        if (key == 'Cs') {
          this.grass_type.push('Césped sintético');
        }
        if (key == 'Cn') {
          this.grass_type.push('Césped natural');
        }
        if (key == 'futsal') {
          this.grass_type.push('Salón');
        }
      }
    });
    if (this.check_month) {
      this.canchaname(1, null);
    }
  }

  canchaname(page, val?, event?) {
    if (!event) {
             this.spinner = true
    }
    this.country = localStorage.getItem('country_id');
    this.city = localStorage.getItem('city_id');
    this.start_date = localStorage.getItem('start_date');
    if (localStorage.getItem('time_format')) {
      this.time_format = localStorage.getItem('time_format');
    }
    else {
      this.time_format = null
    }
    if (localStorage.getItem('week_day') != undefined) {
      this.week_day = localStorage.getItem('week_day');
    }
    this.time_custom = localStorage.getItem('custom_time');

    if (val == "undefined")
      val = null;
    if (this.search_value == "undefined")
      this.search_value = null;
    if (this.time_custom == "undefined")
      this.time_custom = null;
    this.internet = false;
    let data = {
      country: this.country,
      city: this.city,
      start_date: this.start_date,
      time_format: this.time_format,
      week_day: this.week_day,
      page: page,
      time_custom: this.time_custom,
      search_key: val,
      field_format: this.field_format,
      grass_type: this.grass_type,
      favourite: JSON.parse(localStorage.getItem('favourite')),
      latitude: localStorage.getItem('lat'),
      longitude: localStorage.getItem('long'),
      distance: parseInt(localStorage.getItem('distance')),
    }
    let url = this.global.basepath + "ground/groundfilter/?page=" + page + "&format=json";
    this.global.postRequestdata(url, data)
      .subscribe(res => {
                this.spinner = false;
        this.no_data = true;
        let arr = res.json().data;
        if (page == 1) {
          this.no_data = false;
          this.canchas = res.json().data;
        }
        else {
          this.no_data = false;
          this.canchas = this.canchas.concat(arr);
        }
        this.totalPages = res.json().total_pages;
        if (event) {
          if (event != 'search') {
            event.complete();
          }
        }
      },
      (error) => {
                this.spinner = false;
        this.internet = true;
        this.canchas = [];
        if (event) {
          if (event != 'search') {
            event.complete();
          }
        }
      })
  }




  doInfinite(event) {
    this.page++;
    if (this.page <= this.totalPages) {
      this.canchaname(this.page, null, event);
    }
    else {
      event.enable(false);
    }
  }

  revuser(id) {
    localStorage.setItem('g_id', id);
    let modal = this.modalCtrl.create('FieldprofilePage', {
      id: id,
    });
    modal.onDidDismiss(data => { console.log("dismis=>>>>>>>>>>>>>>>>>>>>>>") });
    modal.present();
  }

  modify() {
    this.navCtrl.push('GroundsPage');
  }

  field(id, certified, admin) {
    if (localStorage.getItem('user')) {
      if (certified && !admin) {
        localStorage.setItem('g_id', id);
        let modal = this.modalCtrl.create('CanchaResPage', {
          id: id, cancha_res: "cancha_res"
        });
        modal.onDidDismiss(data => { console.log("dismis=>>>>>>>>>>>>>>>>>>>>>>") });
        modal.present();
      }
      else {
        let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Reservation not allowed" : "Reserva no permitida", duration: 2000, })
        toastController.present();
      }
    }
    else {
      let modal = this.modalCtrl.create('LoginPage', { id: this.id });
      modal.onDidDismiss(data => {
      });
      modal.present();
    }
  }

  pushDate(obj: Date) {
    this.days = [];
    let month_days = this.month_days[obj.getMonth()]
    while (obj.getDate() <= (month_days - 1)) {
      if (obj.getDate() > 9) {
        this.days.push({ day: this.week_days[obj.getDay()], date: obj.getDate() });
      }
      else {
        this.days.push({ day: this.week_days[obj.getDay()], date: '0' + obj.getDate() });
      }
      obj.setDate((obj.getDate()) + 1);
    }
    this.days.push({ day: this.week_days[obj.getDay()], date: obj.getDate() });
  }

  calendarFormat() {
    // this.current_day = this.week_days[obj.getDay()]
    // this.current_date = '01';
    console.log("caledereeeeeeeeeeeeeeeeeeeeeeeeeeeeee")
    let obj = new Date(this.date);
    if (new Date().getMonth() == obj.getMonth() && new Date().getFullYear() == obj.getFullYear()) {
      this.selectd_date = this.current_date;
      this.fulldate = this.date + "-" + this.selectd_date;
      localStorage.setItem('start_date', this.fulldate);
      this.pushDate(new Date(this.fulldate));
      this.page = 1;
      this.canchaname(this.page)
    }
    else {
      if (this.check_month) {
        this.selectd_date = '01';
      }

      this.fulldate = this.date + "-" + this.selectd_date;
      localStorage.setItem('start_date', this.fulldate);
      this.pushDate(obj);
      this.page = 1;
      this.canchaname(this.page)
    }

  }

  date_change(date, day) {
    // this.current_day = day;
    this.selectd_date = date;
    this.fulldate = this.date + "-" + this.selectd_date;
    localStorage.setItem('start_date', this.fulldate);
    this.page = 1;
    this.canchaname(this.page)
  }

  getItems(ev) {
    this.search_value = ev.target.value;
    this.page = 1;
    if (this.search_value) {
      if (this.search_value.length > 1) {
        this.canchas = [];
        this.canchaname(this.page, this.search_value, "search");
                this.spinner = false;
      }
    }
    else {
      this.canchaname(this.page, null);
      //         this.spinner = false;
    }
  }
  ionViewWillLeave() {
    this.rating = [];
    this.field_format = [];
    this.grass_type = [];
            this.spinner = false;
  }
}
