import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-organizationprofile',
  templateUrl: 'organizationprofile.html',
})
export class OrganizationprofilePage {
  organizationp:any[]=[
    {name:"Organization Name",city:"Quito",country:"Ecuador"},
  ];
  dataorg:any[]=[
    {tournament_name:"Tournament Name",organiser_name:"Organiser Name",city:"Quito",country:"Ecuador",no_of_teams:"08",status:"Registeration Open"},
    {tournament_name:"Tournament Name",organiser_name:"Organiser Name",city:"Quito",country:"Ecuador",no_of_teams:"08",status:"Ongoing"},
    {tournament_name:"Tournament Name",organiser_name:"Organiser Name",city:"Quito",country:"Ecuador",no_of_teams:"08",status:"Completed"}];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrganizationprofilePage');
  }
  back() {
    this.navCtrl.pop();
  }
}
