import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganizationprofilePage } from './organizationprofile';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    OrganizationprofilePage,
  ],
  imports: [
    TranslateModule.forChild(),
    IonicPageModule.forChild(OrganizationprofilePage),
  ],
  exports: [
    OrganizationprofilePage
  ]
})
export class OrganizationprofilePageModule { }
