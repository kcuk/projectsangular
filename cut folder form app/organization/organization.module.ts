import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganizationPage } from './organization';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    OrganizationPage,
  ],
  imports: [
    TranslateModule.forChild(),
    IonicPageModule.forChild(OrganizationPage),
  ],
  exports: [
    OrganizationPage
  ]
})
export class OrganizationPageModule { }
