import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-organization',
  templateUrl: 'organization.html',
})
export class OrganizationPage {
  organization:any=[
    {name:"Organization Name",city:"Quito",country:"Ecuador"},
    {name:"Organization Name",city:"Quito",country:"Ecuador"},
    {name:"Organization Name",city:"Quito",country:"Ecuador"},
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OrganizationPage');
  }
  profile(){
    // this.navCtrl.push("OrganizationprofilePage");
    let modal = this.modalCtrl.create('OrganizationprofilePage', {
      // id: id,
    });
    modal.onDidDismiss(data => {
      console.log("ok")
    });
    modal.present();
  }
  // ionViewWillLeave() {
  //   this.global.loader.dismiss();
  // }
}
