import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ViewController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalService } from './../../app/global-service'
import { Http, Headers } from '@angular/http';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook'
@IonicPage()
@Component({
  selector: 'page-loginmodal',
  templateUrl: 'loginmodal.html',
})
export class LoginmodalPage {
  form: FormGroup;
  loginVAlid: boolean = false;
  loader: any;
  valid: boolean = true;
  constructor(public viewCtrl: ViewController, private loadingCtrl: LoadingController, private FB: Facebook, private http: Http, public navCtrl: NavController, public navParams: NavParams, private fb: FormBuilder, private global: GlobalService) {

    this.form = this.fb.group({
      "user": ["", Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(30)])],
      "pass": ["", Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(15)])],
    })

  }

  login() {
    let data = {
      form_type:'email'
    }
    this.global.presentLoading();
    let url = this.global.basepath + "peloteando/app_login/?format=json";
    this.global.loginValid(url, this.form.value,data)
      .subscribe(res => {
        this.global.loader.dismiss();
        let data = res.json()[0]
        let userInfo = {
          "token": data.token.access_token,
          "expire_date": data.token.expires_in
        }
        let expire = (new Date().getTime() / 1000) + data.token.expires_in;
        localStorage.setItem("user", JSON.stringify(userInfo))
        localStorage.setItem("expire", JSON.stringify(expire))
        this.viewCtrl.dismiss();
      }, err => {
        this.global.loader.dismiss();
      });
  }

  signup() {
    localStorage.clear();
    this.navCtrl.setRoot('SignupmodalPage')
  }

  cancel(){
    this.viewCtrl.dismiss();
  }

}
