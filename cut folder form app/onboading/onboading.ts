import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';

/**
 * Generated class for the OnboadingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-onboading',
  templateUrl: 'onboading.html',
})
export class OnboadingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


  goToLogin(){
    this.navCtrl.setRoot('SignupPage');
  }

  next(slide, index) {
    console.log("slideeeeeee",slide)
    slide.slideTo(index, 1000)
  }
}
