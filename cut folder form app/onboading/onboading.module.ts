import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnboadingPage } from './onboading';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    OnboadingPage,
  ],
  imports: [
    IonicPageModule.forChild(OnboadingPage),
    TranslateModule.forChild(),

  ],
  exports: [
    OnboadingPage
  ]
})
export class OnboadingPageModule { }
