import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComposemessagePage } from './composemessage';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    ComposemessagePage,
  ],
  imports: [
    TranslateModule.forChild(),
    IonicPageModule.forChild(ComposemessagePage),
  ],
  exports: [
    ComposemessagePage
  ]
})
export class ComposemessagePageModule { }
