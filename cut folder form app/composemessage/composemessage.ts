import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { GlobalService } from "../../app/global-service";

@IonicPage()
@Component({
  selector: 'page-composemessage',
  templateUrl: 'composemessage.html',
})
export class ComposemessagePage {
  datenew = new Date();
  message: string;
  id: any;
  name: any;
  logo: any;
  image: any;
  obj: object;
  constructor(private toast: ToastController, public navCtrl: NavController, public navParams: NavParams, private global: GlobalService) {
    this.image = this.global.imgpath;
  }

  ionViewWillEnter() {
    this.id = this.navParams.get('id');
    this.name = this.navParams.get('name');
    this.logo = this.navParams.get('logo');
    console.log('ionViewDidLoad ComposemessagePage');
  }
  send() {
    if (this.message ? this.message.trim() : false) {
      console.log("message is", this.message);
      let url = this.global.basepath + "user/message/?format=json";

      if (this.navParams.get('reply')) {
        this.obj = { "form_type": "reply", "msg_id": this.id, "message": this.message.trim() }
      }

      else if (this.navParams.get('tournament')) {
        this.obj = { "form_type": "p2tour", "tournament": this.id, "message": this.message.trim() }
      }

      else if (this.navParams.get('player')) {
        this.obj = { "form_type": "p2p", "player": this.id, "message": this.message.trim() }
      }

      else if (this.navParams.get('team')) {
        this.obj = { "form_type": "p2t", "team": this.id, "message": this.message.trim() }
      }

      else if (this.navParams.get('ground')) {
        this.obj = { "form_type": "p2g", "ground": this.id, "message": this.message.trim() }
      }

      this.global.postRequestdata(url, this.obj)
        .subscribe(res => {
          console.log("send message response", res);
          if (this.navParams.get('reply')) {
            let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Message sent" : "Mensaje enviado", duration: 2000, })
            toastController.present();
            this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3));
            this.message = '';
          }
          else {
            let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Message sent" : "Mensaje enviado", duration: 2000, })
            toastController.present();
            this.navCtrl.pop();
            this.message = '';
          }
        },
        (error) => {
          console.log("error in message send", error);
          if (this.navParams.get('reply')) {
            let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Failed" : "Ha fallado", duration: 2000, })
            toastController.present();
            this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3));
          }
          else {
            let toastController = this.toast.create({ message: this.global.lang == 'en' ? "Failed" : "Ha fallado", duration: 2000, })
            toastController.present();
            this.navCtrl.pop();
          }
        })
    }
    else {
      let toastController = this.toast.create({
        message: this.global.lang == 'en' ? "Please enter message" : "Por favor ingrese el mensaje",
        duration: 1000,
      })
      toastController.present();
      this.message = '';
    }
  }

}
