import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController
} from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GlobalService } from "../../../app/global-service";

@IonicPage()
@Component({
  selector: "page-onBoading1",
  templateUrl: "onBoading1.html"
})
export class OnBoading1Page {
  code: any;
  country_name: any;
  data: any;
  current_date: any;
  city: any;
  country: any;
  email: any;
  signup_res: any;
  signup_details: any = [];
  form: FormGroup;
  length: any;
  lang: any = localStorage.getItem("devicelang");
  constructor(
    private toast: ToastController,
    private global: GlobalService,
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder) {
    this.form = this.fb.group({
      full_name: ["", Validators.compose([Validators.required, Validators.maxLength(40), Validators.pattern("^[a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*( [a-zA-ZÀ-ÿ-z_]([-']?[a-zA-ZÀ-ÿ-z_]+)*)+$")])]
    })
  }

  onBoading1(value) {
    this.navCtrl.push("OnBoading2Page",
      { full_name: value.full_name },
      { animate: true, animation: "ios-transition", direction: "forward",  }
    );
    // let url = this.global.basepath + "peloteando/mobile_signup/?format=json";
    // let data = {
    //   "page": "init_auth",
    //   "full_name": value.full_name,
    //   "phone": "",
    //   "country_code": ""
    // }
    // this.global.postRequestsignup(url, data)
    //   .subscribe(res => {
    //     console.log("response =>>>>>>>>>>>>>.", res)
    //   }, error => {

    //   })
  }


}
