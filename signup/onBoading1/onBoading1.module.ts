import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnBoading1Page } from './onBoading1';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    OnBoading1Page,
  ],
  imports: [
    TranslateModule.forChild(),
    IonicPageModule.forChild(OnBoading1Page),
  ],
  exports: [
    OnBoading1Page
  ]
})
export class OnBoading1PageModule { }
