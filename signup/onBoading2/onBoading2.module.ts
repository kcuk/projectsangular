import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnBoading2Page } from './onBoading2';
import { TranslateModule } from '@ngx-translate/core';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    OnBoading2Page,
  ],
  imports: [
    TranslateModule.forChild(),
    IonicPageModule.forChild(OnBoading2Page),
    BrMaskerModule
  ],
  exports: [
    OnBoading2Page
  ]
})
export class OnBoading2PageModule { }
