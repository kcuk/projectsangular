import { Component, Attribute } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalService } from '../../../app/global-service';
import { TextMaskModule } from 'angular2-text-mask';
import { AlertController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-onBoading2',
  templateUrl: 'onBoading2.html',
})
export class OnBoading2Page {
  number: any;
  form: FormGroup;
  country_name = "Unites States +1";
  country_code = "+1";
  fullName: any;
  masks: any;
  checkNumber: boolean = false;
  checkNumberErrorMsg: boolean = true;
  constructor(
    private toast: ToastController,
    private global: GlobalService,
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    private alertctrl: AlertController) {
    this.fullName = navParams.get('full_name');
    this.form = this.fb.group({
      phone: ["", Validators.compose([Validators.required])]
    })
    this.masks = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  }

  onBoading2(value) {
    let alert = this.alertctrl.create({
      title: this.global.lang == "es" ? "Estas seguro?" : 'Are you sure?',
      message: localStorage.getItem('devicelang') == 'es' ? 'Te vamos a enviar un código de verificación a ' : 'We will send a verification code to ' + this.country_code + " " + this.form.controls.phone.value,
      buttons: [
        {
          text: localStorage.getItem('devicelang') == 'es' ? "Editar" : 'Edit',
          role: 'cancel',
          handler: () => { }
        },
        {
          text: 'OK',
          handler: () => {
            let url = this.global.basepath + "peloteando/mobile_otp/?format=json";
            let data = {
              "phone": this.number,
              "country_code": this.country_code,
              "form_type": "send_otp"
            }
            this.global.postRequestsignup(url, data)
              .subscribe(res => {
                let toastController = this.toast.create({
                  message: this.global.lang == 'es' ? "OTP enviado" : "OTP sent",
                  duration: 2000,
                })
                toastController.present();
                let data = {
                  "page": "init_auth",
                  "full_name": this.fullName,
                  "phone": this.form.controls.phone.value,
                  "country_code": this.country_code
                }
                this.navCtrl.push("OtpVerificationPage", {
                  data: data
                })
              }, error => {
                let toastController = this.toast.create({
                  message: this.global.lang == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
                  duration: 2000,
                })
                toastController.present();
              })
          }
        }
      ]
    });
    alert.present();
  }

  selectCountry_code() {
    this.navCtrl.push('CountryCodePage', {
      callback: this.myCallbackFunction
    });
  }

  myCallbackFunction = (country_code_obj) => {
    return new Promise((resolve, reject) => {
      this.country_code = country_code_obj.dial_code;
      this.country_name = country_code_obj.name + " " + country_code_obj.dial_code;
      this.form.patchValue({ phone: "" })
      resolve();
    });
  }

  validatePhone(event) {
    let result = event.target.value;
    this.number = result.replace(/[- )(]/g, '');
    if (this.number.length >= 8) {
      let url = this.global.basepath + "peloteando/verification/?mobile=" + this.number + "&country_code=" + this.country_code + "&format=json";
      this.global.getRequest(url)
        .subscribe(res => {
          this.checkNumber = true;
          this.checkNumberErrorMsg = true;
        }, err => {
          this.checkNumber = false;
          this.checkNumberErrorMsg = false;
        })
    }
    else {
      this.checkNumber = false;
      this.checkNumberErrorMsg = true;
    }
  }


}
