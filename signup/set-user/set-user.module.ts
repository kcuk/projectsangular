import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetUserPage } from './set-user';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SetUserPage,
  ],
  imports: [
    IonicPageModule.forChild(SetUserPage),TranslateModule
  ],
})
export class SetUserPageModule {}
