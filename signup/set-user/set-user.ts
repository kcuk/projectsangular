import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ToastController, AlertController } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GlobalService } from "../../../app/global-service";
import { Http } from "@angular/http";

@IonicPage()
@Component({
  selector: 'page-set-user',
  templateUrl: 'set-user.html',
})
export class SetUserPage {
  suggestions: any = [];
  user_short_length: boolean;
  check_user: boolean = false;
  username: any;
  user_exist: boolean = false;

  constructor(
    public global: GlobalService,
    public toast: ToastController,
    public alertctrl: AlertController,
    public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillLoad() {
    let url = this.global.basepath + "peloteando/username_suggest/?format=json";
    this.global.getRequest(url)
      .subscribe(res => {
        this.suggestions = res.json().suggestions
      })
  }

  validate(event) {
    let user = event.target.value;
    if (user.length > 4) {
      let url = this.global.basepath + "peloteando/verification/?tag_name=" + user + "&format=json";
      this.global.getRequest(url)
        .subscribe(res => {
          this.check_user = true;
          this.user_short_length = false;
          this.user_exist = false;
        }, err => {
          this.check_user = false;
          this.user_short_length = false;
          this.user_exist = true;
        })
    }
    else {
      this.user_exist = false;
      this.check_user = false;
      this.user_short_length = true
    }
  }

  selectUser(user) {
    this.username = user;
    this.check_user = true;
    this.user_exist = false;
    this.user_short_length = false;
  }

  userName(skip) {
    if (!skip) {
      this.global.presentLoading();
    }
    let data = {
      "page": "tagname",
      "tag_name": this.username,
      "skipped": skip
    }
    let url = this.global.basepath + "peloteando/mobile_signup/?format=json";
    this.global.postRequestdata(url, data)
      .subscribe(res => {
        if (!skip) {
          this.global.loader.dismiss();
        }
        localStorage.setItem('user_info', JSON.stringify(res.json().data))
        this.navCtrl.setRoot('SetPositionPage')
      }, err => {
        if (!skip) {
          this.global.loader.dismiss();
        }
        let toastController = this.toast.create({
          message: this.global.lang == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
          duration: 2000,
        })
        toastController.present();
      })
  }

}
