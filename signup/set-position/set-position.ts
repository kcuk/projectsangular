import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ToastController, AlertController } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GlobalService } from "../../../app/global-service";
import { Http } from "@angular/http";
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { ThrowStmt } from "@angular/compiler/src/output/output_ast";

@IonicPage()
@Component({
  selector: 'page-set-position',
  templateUrl: 'set-position.html',
})
export class SetPositionPage {
  position: any = "1";
  skip: boolean = false;
  constructor(public global: GlobalService,
    public toast: ToastController,
    public alertctrl: AlertController,
    public navCtrl: NavController, public navParams: NavParams,
    private nativeGeocoder: NativeGeocoder,
    private geolocation: Geolocation) {
  }

  setPosition(skip) {
    this.skip = skip;
    this.getLocation();
  }

  getLocation() {
    this.geolocation.getCurrentPosition().then((res) => {
      console.log("type of", res);
      this.nativeGeocoder.reverseGeocode(res.coords.latitude, res.coords.longitude).then((result) => {
        console.log("type of", typeof result, "resultes=>>>>>>>>>>>", result);
        this.sendData(res.coords.latitude, res.coords.longitude, result);
      }).catch((error: any) => {
        console.log("catchjjjjjjjjjjjjj", error)
        let toastController = this.toast.create({
          message: this.global.lang == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
          duration: 2000,
        })
        toastController.present();
      })
    }).catch((error) => {
      this.setLocation();
    })
  }

  sendData(lat, long, location) {
    localStorage.setItem('location',location)
    this.global.presentLoading();
    let data = {
      "page": "location",
      "latitude": lat,
      "longitude": -long
    }
    let url = this.global.basepath + "peloteando/mobile_signup/?format=json";
    this.global.postRequestdata(url, data)
      .subscribe(res => {
        let data = {
          "page": "position",
          "position": parseInt(this.position),
          "skipped": this.skip
        }
        this.global.postRequestdata(url, data)
          .subscribe(res => {
            this.global.loader.dismiss();
            localStorage.setItem('user_info', JSON.stringify(res.json().data))
            this.navCtrl.setRoot('TabPage')
          }, err => {
            this.global.loader.dismiss();
            let toastController = this.toast.create({
              message: this.global.lang == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
              duration: 2000,
            })
            toastController.present();
          })
      }, err => {
        let toastController = this.toast.create({
          message: this.global.lang == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
          duration: 2000,
        })
        toastController.present();
      })

  }


  setLocation() {
    let alert = this.alertctrl.create({
      title: this.global.lang == "es" ? 'Permitir que "Pelotea" acceda a su ubicación mientras usa la aplicación?' : 'Allow "Pelotea" to access your location while you use the app?',
      message: localStorage.getItem('devicelang') == 'es' ? 'Pelotea usa esto para ayudar a las personas a encontrar y conectarse con otros jugadores, equipos y ligas en su área.' : "Pelotea uses this to help people find and connect with other players, teams and leagues in your area.",
      buttons: [
        {
          text: localStorage.getItem('devicelang') == 'es' ? "No permitir" : "Don't Allow",
          role: 'cancel',
          handler: () => { }
        },
        {
          text: this.global.lang == 'es' ? "Permitir" : "Allow",
          handler: () => {
            this.getLocation();
          }
        }
      ]
    });
    alert.present();
  }


}
