import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetPositionPage } from './set-position';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SetPositionPage,
  ],
  imports: [
    IonicPageModule.forChild(SetPositionPage),TranslateModule
  ],
})
export class SetPositionPageModule {}
