import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ToastController } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GlobalService } from "./../../app/global-service";
import { Http } from "@angular/http";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";
declare var ReconnectingWebSocket: any;
import { GooglePlus } from '@ionic-native/google-plus';

@IonicPage()
@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})

export class SignupPage {

  emailExist: boolean = false;
  data: any;
  code: any;
  form: FormGroup;
  country_code: any = "+593";
  length: number = 9;

  constructor(

    private FB: Facebook,
    private toast: ToastController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    private global: GlobalService,
    private googlePlus: GooglePlus) { }



  signUp() {
    this.navCtrl.push('OnBoading1Page',{},
      { animate: true, animation: "ios-transition", direction: "forward", }
    )
  }


  fbSignUp() {
    this.global.presentLoading();
    this.FB.login(["public_profile", "user_friends", "email"])
      .then((afterlogin: FacebookLoginResponse) => {
        try {
          console.log("tryyyyyyyyyyyyyy");
          this.FB.api(
            "/" +
            afterlogin.authResponse.userID +
            "?fields=id,name,email,picture",
            []
          ).then(res => {
            console.log("connected now", res);
            let url = this.global.basepath + "peloteando/fb_login/?format=json";
            let data = {
              email: res.email,
              access_token: afterlogin.authResponse.accessToken
            };
            this.global.postRequestsignup(url, data).subscribe(
              res => {
                this.global.loader.dismiss();
                let info = res.json()[0].info;
                localStorage.setItem("signup_profile1", JSON.stringify(info));
                localStorage.setItem("country_name", info.country_name);
                localStorage.setItem("country_id", info.country_id);
                console.log("info=>>>>>>>>>>>>>>>>>>", info);
                let userInfo = {
                  token: res.json()[0].token.access_token,
                  expire_date: res.json()[0].token.expires_in
                };
                let expire =
                  new Date().getTime() / 1000 + res.json()[0].token.expires_in;
                localStorage.setItem("user", JSON.stringify(userInfo));
                localStorage.setItem("expire", JSON.stringify(expire));
                localStorage.setItem(
                  "logintoken",
                  res.json()[0].token.access_token
                );
                localStorage.setItem("loginid", res.json()[0].info.user_id);
                localStorage.setItem("loginname", res.json()[0].info.username);
                setTimeout(() => {
                  let user_id = localStorage.getItem("logintoken");
                  let data_to_be_send = { action: "login", user: user_id };
                  setTimeout(res => {
                    this.global.socket.send(JSON.stringify(data_to_be_send));
                  }, 1000);

                  this.global.socket.onerror(close => {
                    let socket = new ReconnectingWebSocket(this.global.ws_path);
                    socket.send(JSON.stringify(data_to_be_send));
                  });
                }, 500);
                if (info.first_page == true) {
                  this.navCtrl.push("SignupdetailPage");
                } else if (info.second_page == true) {
                  this.navCtrl.push("EditplayerPage");
                } else if (
                  info.first_page == false &&
                  info.second_page == false
                ) {
                  localStorage.removeItem("signup_profile1");
                  this.navCtrl.setRoot("SelectcityPage", {
                    country_name: info.country_name,
                    country_id: info.country_id
                  });
                }
              },
              err => {
                let toastController = this.toast.create({
                  message:
                    this.global.lang == "en"
                      ? "Error,Try again later"
                      : "Error, inténtalo de nuevo más tarde",
                  duration: 2000
                });
                toastController.present();
              }
            );
          });
        } catch (error) {
          this.global.loader.dismiss();
          console.log("errorrrrrrrrrrrrrrrrrrrrrrrrrrrr");
        }
      })
      .catch(e => {
        this.global.loader.dismiss();
        console.log("could not logged in" + e);
      });
  }


  googleLogin() {
    this.global.presentLoading();
    this.googlePlus.login({ scope: '', webClientId: this.global.googleWebClientId })
      .then(res => {
        console.log(res);
        let url = this.global.basepath + "peloteando/gmail_login/?format=json";
        let data = {
          id_token: res.idToken
        };
        this.global.postRequestsignup(url, data).subscribe(res => {
          this.global.loader.dismiss();
          let info = res.json().info;
          let userInfo = {
            token: res.json().token.access_token,
            expire_date: res.json().token.expires_in
          };
          let expire =
            new Date().getTime() / 1000 + res.json().token.expires_in;
          localStorage.setItem("user", JSON.stringify(userInfo));
          localStorage.setItem("signup_profile1", JSON.stringify(info));
          localStorage.setItem("country_name", info.country_name);
          localStorage.setItem("country_id", info.country_id);
          localStorage.setItem("expire", JSON.stringify(expire));
          localStorage.setItem("logintoken", res.json().token.access_token);
          localStorage.setItem("loginid", res.json().info.user_id);
          localStorage.setItem("loginname", res.json().info.username);
          setTimeout(() => {
            let user_id = localStorage.getItem("logintoken");
            let data_to_be_send = { action: "login", user: user_id };
            setTimeout(res => {
              this.global.socket.send(JSON.stringify(data_to_be_send));
            }, 1000);
            this.global.socket.onerror(close => {
              let socket = new ReconnectingWebSocket(this.global.ws_path);
              socket.send(JSON.stringify(data_to_be_send));
            });
          }, 500);

          if (info.first_page == true) {
            this.navCtrl.push("SignupdetailPage");
          } else if (info.second_page == true) {
            this.navCtrl.push("EditplayerPage");
          } else if (
            info.first_page == false &&
            info.second_page == false
          ) {
            localStorage.removeItem("signup_profile1");
            this.navCtrl.setRoot("SelectcityPage", {
              country_name: info.country_name,
              country_id: info.country_id
            });
          }
        }, err => {
          this.global.loader.dismiss();
          let toastController = this.toast.create({
            message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde",
            duration: 2000,
          })
          toastController.present();
        });
      })
      .catch(e => {
        this.global.loader.dismiss();
        let toastController = this.toast.create({
          message: this.global.lang == 'en' ? "Error,Try again later" : "Error, inténtalo de nuevo más tarde",
          duration: 2000,
        })
        toastController.present();
      });
  }


}
