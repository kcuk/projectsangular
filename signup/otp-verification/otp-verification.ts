import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ToastController, AlertController } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GlobalService } from "../../../app/global-service";
import { Http } from "@angular/http";

@IonicPage()

@Component({
  selector: 'page-otp-verification',
  templateUrl: 'otp-verification.html',
})
export class OtpVerificationPage {

  verifyOtpDigiit: boolean;
  sign_up_data: any;
  otp: any;
  constructor(private toast: ToastController,
    private global: GlobalService,
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    private alertctrl: AlertController) {
    console.log("params=>>>>>>>>>>>>>>>>", navParams)
    this.sign_up_data = this.navParams.get('data')
    console.log(" this.sign_up_dat=>>>>>>>>>>>>>>>>", this.sign_up_data)
  }

  validate(event) {
    if (event.target.value.length > 7) {
      this.verifyOtpDigiit = true;
    }
    else {
      this.verifyOtpDigiit = false;
    }
  }

  verifyOtp() {
    this.global.presentLoading();
    console.log("otp=>>>>>>>>>>>>", this.otp)
    let sent_otp = this.otp.replace(/[- )(]/g, '')
    console.log("sent_otp=>>>>>>>>>>>>", sent_otp)
    let url = this.global.basepath + "peloteando/mobile_otp/?format=json";
    this.sign_up_data.phone = this.sign_up_data.phone.replace(/[- )(]/g, '');
    let data = {
      "phone": this.sign_up_data.phone,
      "country_code": this.sign_up_data.country_code,
      "form_type": "verify_otp",
      "otp": parseInt(sent_otp)
    }
    this.global.postRequestsignup(url, data)
      .subscribe(res => {
        console.log("respnseeeeeeeeeee", res);
        let url1 = this.global.basepath + "peloteando/mobile_signup/?format=json";
        this.global.postRequestsignup(url1, this.sign_up_data)
          .subscribe(res => {
            this.global.loader.dismiss();
            console.log("sent_otp=>>>>>>>>>>>>", res.json());
            let data = res.json();
            let userInfo = {
              token: data.token.token.access_token,
              expire_date: data.token.token.expires_in
            };
            let expire = new Date().getTime() / 1000 + data.token.token.expires_in;
            localStorage.setItem("user", JSON.stringify(userInfo));
            localStorage.setItem("expire", JSON.stringify(expire));
            localStorage.setItem("logintoken", userInfo.token);
            localStorage.setItem("loginid", data.data.profile_id);
            localStorage.setItem("loginname", data.data.username);
            localStorage.setItem('user_info',JSON.stringify(data.data))
            this.navCtrl.push("SetPasswordPage");
          }, err => {
            this.global.loader.dismiss();
            let toastController = this.toast.create({
              message: this.global.lang == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
              duration: 2000,
            })
            toastController.present();
          })
      }, err => {
        this.global.loader.dismiss();
        let toastController = this.toast.create({
          message: this.global.lang == 'es' ? "Ingrese OTP correcta" : "Enter correct OTP",
          duration: 2000,
        })
        toastController.present();
      })
  }


}
