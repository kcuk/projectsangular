import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtpVerificationPage } from './otp-verification';
import { TranslateModule } from '@ngx-translate/core';
import { BrMaskerModule } from 'brmasker-ionic-3';
@NgModule({
  declarations: [
    OtpVerificationPage,
  ],
  imports: [
    IonicPageModule.forChild(OtpVerificationPage),TranslateModule,BrMaskerModule
  ],
})
export class OtpVerificationPageModule {}
