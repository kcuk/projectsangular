import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CountryCodePage } from './countryCode';
import { TranslateModule } from '@ngx-translate/core';
import { IonAlphaScrollModule } from 'ionic2-alpha-scroll';


@NgModule({
  declarations: [
    CountryCodePage,
  ],
  imports: [
    TranslateModule.forChild(),
    IonicPageModule.forChild(CountryCodePage),
    IonAlphaScrollModule
  ],
  exports: [
    CountryCodePage
  ]
})
export class CountryCodePageModule { }
