import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ToastController, AlertController } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GlobalService } from "../../../app/global-service";
import { Http } from "@angular/http";

@IonicPage()
@Component({
  selector: 'page-set-password',
  templateUrl: 'set-password.html',
})
export class SetPasswordPage {
  password: any = "";
  show_password: boolean = false;
  check_password: boolean = false;
  constructor(
    public global: GlobalService,
    public toast:ToastController,
    public navCtrl: NavController, public navParams: NavParams) {
  }

  validate(event) {
    let password_length = event.target.value.length;
    if (password_length >= 6) {
      this.check_password = true;
    }
    else {
      this.check_password = false;
    }
  }

  setPassword() {
    this.global.presentLoading();
    let url = this.global.basepath + "peloteando/mobile_signup/?format=json";
    let data = {
      "page": "password",
      "password": this.password
    }
    this.global.postRequestdata(url, data)
      .subscribe(res => {
        this.global.loader.dismis();
        console.log("responseeeeeeeeeeeeeeeeeee", res);
        localStorage.setItem('user_info', JSON.stringify(res.json().data))
        this.navCtrl.setRoot('SetEmailPage');
      }, err => {
        this.global.loader.dismiss();
        let toastController = this.toast.create({
          message: this.global.lang == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
          duration: 2000,
        })
        toastController.present();
      })
  }

}
