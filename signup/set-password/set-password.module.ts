import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetPasswordPage } from './set-password';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SetPasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(SetPasswordPage),TranslateModule
  ],
})
export class SetPasswordPageModule {}
