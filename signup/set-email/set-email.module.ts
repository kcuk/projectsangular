import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetEmailPage } from './set-email';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SetEmailPage,
  ],
  imports: [
    IonicPageModule.forChild(SetEmailPage),TranslateModule
  ],
})
export class SetEmailPageModule {}
