import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ToastController, AlertController } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GlobalService } from "../../../app/global-service";
import { Http } from "@angular/http";

@IonicPage()
@Component({
  selector: 'page-set-email',
  templateUrl: 'set-email.html',
})
export class SetEmailPage {
  email: any;
  email_exist: boolean;
  check_email: boolean = false;
  skip: boolean = false;
  constructor(
    public global: GlobalService,
    public toast: ToastController,
    public alertctrl: AlertController,
    public navCtrl: NavController, public navParams: NavParams) {
  }

  validate(event) {
    let email = event.target.value;
    if (/[a-zA-ZÀ-ÿ-z_.0-9]+@[a-zA-ZÀ-ÿ]+[.][a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ.]+$/.test(email)) {
      let url = this.global.basepath + "peloteando/verification/?email=" + email + "&format=json";
      this.global.getRequest(url)
        .subscribe(res => {
          this.email_exist = false;
          this.check_email = true;
        }, err => {
          this.email_exist = true;
          this.check_email = false;
        })
    }
    else {
      this.email_exist = false;
      this.check_email = false;
    }
  }

  skipEmail() {
    let alert = this.alertctrl.create({
      title: this.global.lang == "es" ? "Estas seguro?" : 'Are you sure?',
      message: localStorage.getItem('devicelang') == 'es' ? 'Es importante que ingreses tu email para mantener tu cuenta segura y facilitar el inicio de sesión.' : "It's important to add your email to keep your account secure and make login easier.",
      buttons: [
        {
          text: localStorage.getItem('devicelang') == 'es' ? "Cancelar" : 'Cancel',
          role: 'cancel',
          handler: () => { }
        },
        {
          text: this.global.lang == 'es' ? "Si, seguro" : "I'm sure",
          handler: () => {
            this.setEmail(true)
          }
        }
      ]
    });
    alert.present();
  }

  setEmail(skip) {
    if (!skip) {
      this.global.presentLoading();
    }
    let data = {
      "page": "email",
      "email": this.email,
      "skipped": skip
    }
    let url = this.global.basepath + "peloteando/mobile_signup/?format=json";
    this.global.postRequestdata(url, data)
      .subscribe(res => {
        if (!skip) {
          this.global.loader.dismiss();
        }
        localStorage.setItem('user_info', JSON.stringify(res.json().data))
        this.navCtrl.setRoot('SetUserPage')
      }, err => {
        if (!skip) {
          this.global.loader.dismiss();
        }
        let toastController = this.toast.create({
          message: this.global.lang == 'es' ? "Error, inténtalo de nuevo más tarde" : "Error, Try again later",
          duration: 2000,
        })
        toastController.present();
      })
  }

}
